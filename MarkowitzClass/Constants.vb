Module Constants
	' **********************************************************************
	' Constants and Enumerations used by the Optimiser
	'
	' **********************************************************************

	Friend Const EPSILON As Double = 0.000000001
	Friend Const INFINITY As Double = CDbl(Single.MaxValue)	' 3.4E38
	Friend Const INVALID As Double = Double.NaN

	Friend Const FILENAME_DATEFORMAT As String = "yyyyMMdd_HHmmss"

	'Enumerate "directions"
	Friend Enum Direction As Integer
		Lower = (-1)
		Higher = 1
	End Enum

	'Enumerate variable stress
	Friend Enum VariableStress As Integer
		vsIn = 0
		vsUp = 1
		vsLo = 2
	End Enum

	'Enumerate simplex error codes
	Friend Enum SimplexErrorCode As Integer
		SimplexOK = 0
		SimplexInfeasible = (-1)
		SimplexUnboundedE = (-2)
		SimplexDegenerate = (-3)
	End Enum


	'
#Region " Optimiser Public classes"

	Friend Class Cset
		' **********************************************************************
		' Replacement for the original 'CSet' class.
		'
		' This class is designed to hold a sorted list of Instruments which are 
		' either 'In' or 'Out' of the optimised portfolio.
		' **********************************************************************

		Private Items As ArrayList

		Public Sub New()
			Items = New ArrayList()
		End Sub

		Public Sub New(ByVal MaxCount As Integer)

			Items = New ArrayList(MaxCount + 1)

		End Sub

		Public ReadOnly Property Count() As Integer
			Get
				Return Items.Count
			End Get
		End Property

		Public Function Member(ByVal Index As Integer) As Integer
			If (Index > 0) AndAlso (Index <= Items.Count) Then
				Return CInt(Items(Index - 1))
			Else
				Return 0
			End If
		End Function

		Public Sub Add(ByVal Member As Integer)
			Try
				If Not Items.Contains(Member) Then
					Items.Add(Member)

					Items.Sort()
				End If
			Catch ex As Exception
			End Try
		End Sub

		Public Sub Delete(ByVal Member As Integer)
			Try
				If Items.Contains(Member) Then
					Items.Remove(Member)
				End If
			Catch ex As Exception
			End Try
		End Sub

		Public Function Position(ByVal Member As Integer) As Integer
			If Items.Contains(Member) Then
				Return Items.IndexOf(Member) + 1
			Else
				Return 0
			End If
		End Function

	End Class

	Friend Class CLAIterationVariables
		' **********************************************************************
		' Class to hold persistent variables in the CLA Iteration process.
		'
		' This class is designed to replace the use of Static variables in the 
		' Iteration() function.
		'
		' **********************************************************************

		Public InDirection As Direction
		Public OutDirection As Direction
		Public jMaxA As Integer
		Public jMaxB As Integer
		Public lambdaA As Double
		Public lambdaB As Double

		' Public Problemj As Integer					 'The security causing the problem :- Appears to do nothing !!
		Public jChange As Integer					 'Security coming in or out on any iteration
		Public jLast As Integer						 'The last one to come in or out
		Public jLastButOne As Integer			 'The one to come in or out before the last one

		Public Sub New()

			Reset()

		End Sub

		Public Sub New(ByVal SourceObject As CLAIterationVariables)
			Try

				Reset()

				If (SourceObject IsNot Nothing) Then
					InDirection = SourceObject.InDirection
					OutDirection = SourceObject.OutDirection
					jMaxA = SourceObject.jMaxA
					jMaxB = SourceObject.jMaxB
					lambdaA = SourceObject.lambdaA
					lambdaB = SourceObject.lambdaB
					jChange = SourceObject.jChange
					jLast = SourceObject.jLast
					jLastButOne = SourceObject.jLastButOne
				End If

			Catch ex As Exception
			End Try

		End Sub


		Public Sub Reset()

			Try

				InDirection = 0
				OutDirection = 0
				jMaxA = 0
				jMaxB = 0
				lambdaA = 0
				lambdaB = 0

				'Variables to keep track of securities coming in or out of the solution in the CLA.  Used to spot loops - in which case
				'mu is peturbed

				'Problemj = Integer.MaxValue				'The security causing the problem :- Appears to do nothing !!
				jChange = (Integer.MaxValue - 1)	'Security coming in or out on any iteration
				jLast = jChange - 1								'The last one to come in or out
				jLastButOne = jLast - 1						'The one to come in or out before the last one

			Catch ex As Exception
			End Try

		End Sub

	End Class

	'Public Class Cset_Org
	'	Private Items() As Integer
	'	Private mCount As Integer

	'	Public Sub New(ByVal MaxCount As Integer)
	'		ReDim Items(MaxCount)
	'		mCount = 0
	'	End Sub

	'	Public ReadOnly Property Count() As Integer
	'		Get
	'			Count = mCount
	'			Return Count
	'		End Get

	'	End Property

	'	Private Sub Initialize(ByVal MaxCount As Integer)
	'		ReDim Items(MaxCount)
	'		mCount = 0
	'	End Sub

	'	Public Sub Resize(ByVal MaxCount As Integer)
	'		ReDim Preserve Items(MaxCount)
	'	End Sub

	'	Public Function Member(ByVal Index As Integer) As Integer
	'		Member = Items(Index)
	'	End Function

	'	Public Sub Add(ByVal Member As Integer)
	'		Dim i As Integer
	'		For i = mCount + 1 To 2 Step -1
	'			If Member > Items(i - 1) Then
	'				Exit For
	'			Else
	'				Items(i) = Items(i - 1)
	'			End If
	'		Next i
	'		Items(i) = Member
	'		mCount = mCount + 1
	'	End Sub

	'	Private Sub DeleteByIndex(ByVal Index As Integer)
	'		Dim i As Integer
	'		mCount = mCount - 1
	'		For i = Index To mCount
	'			Items(i) = Items(i + 1)
	'		Next i
	'	End Sub

	'	Public Sub Delete(ByVal Member As Integer)
	'		Dim i As Integer
	'		i = Position(Member)
	'		DeleteByIndex(i)
	'	End Sub

	'	Public Function Position(ByVal Member As Integer) As Integer
	'		Dim i As Integer
	'		For i = 1 To mCount
	'			If Items(i) = Member Then Exit For
	'		Next i
	'		Position = i
	'	End Function

	'End Class



#End Region




End Module

