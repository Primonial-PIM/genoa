Imports RenaissanceGlobals

Partial Public Class OptimiserClass


	Private Function MarginalsIteration(ByVal CLA_Count As Integer, Optional ByVal MoveBestToLimit As Boolean = False, Optional ByVal MaxMoveCount As Integer = 1, Optional ByVal SingularMethod As Boolean = False, Optional ByVal SaveCP As Boolean = True) As Double
		' ****************************************************************************************
		'
		' Assumes SUM(Weight) = 100%
		' ****************************************************************************************
		Dim MarginalStep As Double = 0.001

		Dim RVal As Double = 0
		Static TempWeight() As Double

		If (MarginalsConstraintArray Is Nothing) Then
			Return (-1)
		End If

		Try
			Dim Initial_ExpectedReturn As Double
			Dim Initial_Variance As Double
			Dim This_ExpectedReturn As Double
			Dim This_Variance As Double
			Dim Best_ExpectedReturn As Double = 0
			Dim Best_Variance As Double = Double.MaxValue
			Dim ExpectedReturn As Double = 0
			Dim Variance As Double = Double.MaxValue

			Dim IN_Counter As Integer
			Dim OUT_Counter As Integer

			Dim Best_IN_Counter As Integer = (-1)
			Dim Best_OUT_Counter As Integer = (-1)
			Dim Temp_IN_Weight As Double
			Dim Temp_OUT_Weight As Double

			Dim IterationCount As Integer = 0
			Dim ReturnCalcCount As Integer = 0
			Dim CheckMarginalsCount As Integer = 0

			If (TempWeight Is Nothing) OrElse (TempWeight.Length <> PortfolioWeight.Length) Then
				TempWeight = Array.CreateInstance(GetType(Double), PortfolioWeight.Length)
			End If
			Array.Copy(PortfolioWeight, TempWeight, PortfolioWeight.Length)

			ReturnRiskReturn(TempWeight, Initial_ExpectedReturn, Initial_Variance)

			If (CLA_Count > 1) Then

				If (SingularMethod) Then
			


				Else

					Try


						For IN_Counter = 1 To (NumberOfRealSecurities)

							For OUT_Counter = 1 To (NumberOfRealSecurities)

								If (IN_Counter <> OUT_Counter) Then
									IterationCount += 1

									If (TempWeight(IN_Counter) < UpperLimit(IN_Counter)) AndAlso (TempWeight(OUT_Counter) > LowerLimit(OUT_Counter)) Then

										Temp_IN_Weight = TempWeight(IN_Counter)
										Temp_OUT_Weight = TempWeight(OUT_Counter)

										Try
											TempWeight(IN_Counter) += MarginalStep
											TempWeight(OUT_Counter) -= MarginalStep

											ReturnRiskReturn(TempWeight, This_ExpectedReturn, This_Variance)

											ReturnCalcCount += 1

											If (This_Variance < Initial_Variance) AndAlso (This_ExpectedReturn > Best_ExpectedReturn) Then

												CheckMarginalsCount += 1

												If CheckMarginalsLimits(TempWeight) Then

													Best_Variance = This_Variance
													Best_ExpectedReturn = This_ExpectedReturn
													Best_IN_Counter = IN_Counter
													Best_OUT_Counter = OUT_Counter
												End If

											End If

										Catch ex As Exception
										Finally

											TempWeight(IN_Counter) = Temp_IN_Weight
											TempWeight(OUT_Counter) = Temp_OUT_Weight
										End Try

									End If

								End If

							Next

						Next

						' Debug.Print("IterationCount : " & IterationCount.ToString & ", " & ReturnCalcCount.ToString & ", " & CheckMarginalsCount.ToString & " - Best In/Out : " & Best_IN_Counter.ToString & ", " & Best_OUT_Counter.ToString)

					Catch ex As Exception
					End Try

				End If

			Else
				' for CLA_Count = 1, just save the portfolio.

				Variance = Initial_Variance
				ExpectedReturn = Initial_ExpectedReturn
				Initial_ExpectedReturn = 999

			End If


			' Set Return Values


			If (Best_IN_Counter >= 0) Then

				If (MoveBestToLimit) AndAlso (MaxMoveCount > 1) Then
					Dim MoveCount As Integer = 0

					PortfolioWeight(Best_IN_Counter) += MarginalStep
					PortfolioWeight(Best_OUT_Counter) -= MarginalStep

					Temp_IN_Weight = PortfolioWeight(Best_IN_Counter)
					Temp_OUT_Weight = PortfolioWeight(Best_OUT_Counter)

					ReturnRiskReturn(PortfolioWeight, Best_ExpectedReturn, Best_Variance)

					' Debug.Print("    Variance 001 " & (Best_Variance * 1000.0#).ToString("#,##0.00000000"))

					For MoveCount = 2 To MaxMoveCount
						If (PortfolioWeight(Best_IN_Counter) < UpperLimit(Best_IN_Counter)) AndAlso (PortfolioWeight(Best_OUT_Counter) > LowerLimit(Best_OUT_Counter)) Then
							PortfolioWeight(Best_IN_Counter) += MarginalStep
							PortfolioWeight(Best_OUT_Counter) -= MarginalStep

							ReturnRiskReturn(PortfolioWeight, This_ExpectedReturn, This_Variance)

							' Debug.Print("    Variance " & MoveCount.ToString("000") & " " & (This_Variance * 1000.0#).ToString("#,##0.00000000"))

							If (This_Variance > Best_Variance) Then
								'' Reverse the last, failed, increment.
								PortfolioWeight(Best_IN_Counter) -= MarginalStep
								PortfolioWeight(Best_OUT_Counter) += MarginalStep

								' Debug.Print("    Variance loop EXIT")
								Exit For
							End If

							Best_Variance = This_Variance
						End If
					Next

					If (CheckMarginalsLimits(PortfolioWeight) = False) Then
						PortfolioWeight(Best_IN_Counter) = Temp_IN_Weight
						PortfolioWeight(Best_OUT_Counter) = Temp_OUT_Weight

						' Debug.Print("    Variance Marginals Check FAILED")
					End If

					'While CheckMarginalsLimits(PortfolioWeight) AndAlso (MoveCount <= MaxMoveCount)
					'	PortfolioWeight(Best_IN_Counter) += MarginalStep
					'	PortfolioWeight(Best_OUT_Counter) -= MarginalStep

					'	MoveCount += 1
					'End While

					'' Reverse the last, failed, increment.

					'PortfolioWeight(Best_IN_Counter) -= MarginalStep
					'PortfolioWeight(Best_OUT_Counter) += MarginalStep

					ReturnRiskReturn(PortfolioWeight, ExpectedReturn, Variance)

				Else

					PortfolioWeight(Best_IN_Counter) += MarginalStep
					PortfolioWeight(Best_OUT_Counter) -= MarginalStep

					Variance = Best_Variance
					ExpectedReturn = Best_ExpectedReturn

				End If

			Else
				If (CLA_Count > 1) Then
					Variance = Initial_Variance
					ExpectedReturn = Initial_ExpectedReturn
				End If
			End If

			' Debug.Print("Initial / Final Variance " & (Initial_Variance * 1000.0#).ToString("#,##0.00000000") & ", " & (Variance * 1000.0#).ToString("#,##0.00000000"))

			Try
				' Save Corner portfolio details to Output arrays.

				' OutputCornerPortfolio(pCLA_Count, ExpectedReturn, Variance, LambdaE)
				' Move inline - NPP 02 Oct 2007 - 

				RVal = Math.Abs(Initial_ExpectedReturn - ExpectedReturn)

				If (SaveCP) Then
					Dim OutX_Array() As Double

					OutCPNum(CLA_Count) = CLA_Count
					OutE(CLA_Count) = ExpectedReturn
					OutSD(CLA_Count) = Variance ^ (0.5)
					OutLambdaE(CLA_Count) = RVal

					If (OutX(CLA_Count) IsNot Nothing) AndAlso (OutX(CLA_Count).Length = (NumberOfRealSecurities + 1)) Then
						Array.Copy(PortfolioWeight, OutX(CLA_Count), OutX(CLA_Count).Length)
					Else
						OutX_Array = System.Array.CreateInstance(GetType(Double), NumberOfRealSecurities + 1)
						Array.Copy(PortfolioWeight, OutX_Array, OutX_Array.Length)
						OutX(CLA_Count) = OutX_Array
					End If
				End If

			Catch ex As Exception
			End Try


		Catch ex As Exception

		End Try

		Return RVal

	End Function

	Private Sub ReturnRiskReturn(ByVal pWeights() As Double, ByRef pReturn As Double, ByRef pVariance As Double)
		' ****************************************************************************************
		'
		'
		' Assumes Weights are Base 1, having come out of the optimiser.
		' ****************************************************************************************
		Dim RVal_Return As Double
		Dim RVal_Variance As Double

		Try
			Dim j As Integer
			Dim k As Integer

			RVal_Return = 0
			RVal_Variance = 0

			For j = 1 To NumberOfRealSecurities
				If (pWeights(j) <> 0.0#) Then
					RVal_Return += (ExpectedReturns(j) * pWeights(j))
					RVal_Variance += (CovarianceMatrix(j, j) * pWeights(j) * pWeights(j))

					For k = 1 To j - 1
						If (pWeights(k) <> 0.0#) Then
							RVal_Variance += (2.0# * CovarianceMatrix(j, k) * pWeights(j) * pWeights(k))
						End If
					Next k
				End If
			Next j

		Catch ex As Exception
			RVal_Return = 0
			RVal_Variance = 0
		End Try

		pReturn = RVal_Return
		pVariance = RVal_Variance

	End Sub

	Private Function CheckMarginalsLimits(ByVal pWeights() As Double) As Boolean
		' ****************************************************************************************
		' Returns True for OK, False for Fail.
		'
		' Assumes Weights are Base 1, having come out of the optimiser.
		' ****************************************************************************************

		Dim RVal As Boolean = False

		Try
			Dim ConstraintCounter As Integer
			Dim InstrumentCounter As Integer
			Dim SumWeight As Double
			Dim ConstraintFailed As Boolean
			Dim ConstraintTypeColumn As Integer
			Dim ConstraintLimitColumn As Integer

			ConstraintTypeColumn = _InstrumentNumerator.Count
			ConstraintLimitColumn = ConstraintTypeColumn + 1

			For ConstraintCounter = 1 To (MarginalsConstraintArray.GetLength(0) - 1)
				SumWeight = 0.0
				ConstraintFailed = False

				' Sum MatchingInstruments

				For InstrumentCounter = 0 To (MarginalsConstraintArray.GetLength(1) - 3)	' (-3 to Strip off Constraint Type and Constraint weight columns) '   InstrumentNumerator.Count - 1)
					SumWeight += pWeights(InstrumentCounter + 1) * MarginalsConstraintArray(ConstraintCounter, InstrumentCounter)
				Next

				SumWeight = Math.Round(SumWeight, 8)

				If (MarginalsConstraintArray(ConstraintCounter, ConstraintTypeColumn) = ConstraintType.ctGreaterThan) Then
					If (SumWeight < MarginalsConstraintArray(ConstraintCounter, ConstraintLimitColumn)) AndAlso (Math.Abs((SumWeight - MarginalsConstraintArray(ConstraintCounter, ConstraintLimitColumn))) > 0.00000001) Then
						ConstraintFailed = True
					End If
				ElseIf (MarginalsConstraintArray(ConstraintCounter, ConstraintTypeColumn) = ConstraintType.ctLessThan) Then
					If (SumWeight > MarginalsConstraintArray(ConstraintCounter, ConstraintLimitColumn)) AndAlso (Math.Abs((SumWeight - MarginalsConstraintArray(ConstraintCounter, ConstraintLimitColumn))) > 0.00000001) Then
						ConstraintFailed = True
					End If
				ElseIf (MarginalsConstraintArray(ConstraintCounter, ConstraintTypeColumn) = ConstraintType.ctEqualTo) Then
					If (Math.Abs(SumWeight - MarginalsConstraintArray(ConstraintCounter, ConstraintLimitColumn)) > 0.0000001) Then
						' Allow for imprecision of Double type numbers.
						ConstraintFailed = True
					End If
				End If

				If (ConstraintFailed) Then
					Exit For
				End If
			Next

			RVal = Not ConstraintFailed

		Catch ex As Exception
		End Try

		Return RVal

	End Function

End Class
