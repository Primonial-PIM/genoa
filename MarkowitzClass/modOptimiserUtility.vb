Imports System.IO

Partial Public Class OptimiserClass
	' **********************************************************************
	'
	'
	' **********************************************************************


	Private Function IsUp(ByVal j As Integer) As Boolean
		' **********************************************************************
		'Return True if variable j is Out at upper limit
		' **********************************************************************

		Try

			If (State(j) = VariableStress.vsUp) Then
				Return True
			Else
				Return False
			End If

		Catch ex As Exception
		End Try

		Return False

		' IsUp = (State(j) = vsUP)
	End Function

	Private Function IsLo(ByVal j As Integer) As Boolean
		' **********************************************************************
		'Return True if variable j is Out at lower limit
		' **********************************************************************

		Try

			If (State(j) = VariableStress.vsLo) Then
				Return True
			Else
				Return False
			End If

		Catch ex As Exception
		End Try

		Return False

		' IsLo = (State(j) = vsLo)
	End Function

	Private Sub GoIn(ByVal jIn As Integer)
		' **********************************************************************
		'<U1> Variable jIn goes IN
		' **********************************************************************

		Try

			OutVars.Delete(jIn)
			InVars.Add(jIn)
			State(jIn) = VariableStress.vsIn

		Catch ex As Exception
		End Try
	End Sub

	Private Sub GoOut(ByVal jOut As Integer, ByVal OutDirection As Direction)
		' **********************************************************************
		'<U2> Variable jOut goes OUT
		' **********************************************************************

		Try

			InVars.Delete(jOut)

			'add to out set if security or slack variable (not ABV)

			If jOut <= NumberOfRealSecurities + NumberOfSlackVariables Then
				OutVars.Add(jOut)
			End If

			If OutDirection = Direction.Higher Then
				State(jOut) = VariableStress.vsUp
			Else
				State(jOut) = VariableStress.vsLo
			End If

		Catch ex As Exception
		End Try

	End Sub

	Public Function ExtractPortfolio(ByVal pTargetIsStdDev As Boolean, ByVal pTargetSd As Double, ByVal pTargetReturn As Double, Optional ByVal pOptimiserSnapshot As OptimiserDataClass = Nothing) As Double()
		' *****************************************************************************************
		' Return an array of portfolio weights targeting either the given Std Deviation or Expected return.
		'
		' *****************************************************************************************

		Dim i, j As Integer
		Dim LowWeight As Double
		Dim HighWeight As Double
		Dim found As Boolean

		Dim RVal(-1) As Double
		Dim OptimiserSnapshot As OptimiserDataClass
		Dim UpperWeights() As Double
		Dim LowerWeights() As Double

		Dim TargetValue As Double
		Dim OptimiserTargetArray() As Double

		Try
			If (pOptimiserSnapshot Is Nothing) OrElse (pOptimiserSnapshot.ResultsSet = False) Then
				OptimiserSnapshot = Me.GetOptimisationData
			Else
				OptimiserSnapshot = pOptimiserSnapshot
			End If

			If (OptimiserSnapshot.NumberOfRealSecurities <= 0) OrElse (OptimiserSnapshot.OptimiserCompleted = False) Then
				Return RVal
			End If

			RVal = Array.CreateInstance(GetType(Double), OptimiserSnapshot.NumberOfRealSecurities + 1)

			If (pTargetIsStdDev) Then
				TargetValue = pTargetSd
				OptimiserTargetArray = OptimiserSnapshot.CP_StandardDeviation
			Else
				TargetValue = pTargetReturn
				OptimiserTargetArray = OptimiserSnapshot.CP_ExpectedReturn
			End If

			found = False
			If TargetValue = OptimiserTargetArray(1) Then	' OutSD(1) Then
				For j = 1 To OptimiserSnapshot.NumberOfRealSecurities
					RVal(j) = OptimiserSnapshot.CP_WeightingArray(1)(j)
				Next
			Else
				For i = 2 To OptimiserSnapshot.CornerPortfolioCount
					UpperWeights = OptimiserSnapshot.CP_WeightingArray(i - 1)
					LowerWeights = OptimiserSnapshot.CP_WeightingArray(i)

					If (OptimiserTargetArray(i) <= TargetValue) And (Not found) Then
						found = True
						LowWeight = (OptimiserTargetArray(i - 1) - TargetValue) / (OptimiserTargetArray(i - 1) - OptimiserTargetArray(i))
						HighWeight = 1 - LowWeight
						For j = 1 To OptimiserSnapshot.NumberOfRealSecurities
							RVal(j) = (LowWeight * LowerWeights(j)) + (HighWeight * UpperWeights(j))
						Next
					End If
				Next
			End If

		Catch ex As Exception
		End Try

		Return RVal

		'found = False
		'If Sd = OutSD(1) Then
		'	For j = 1 To NumSecs
		'		LocalOut100(PortfolioIndex, j) = OutX(j)
		'	Next
		'Else
		'	For i = 2 To CLAcount
		'		If (OutSD(i) <= Sd) And (Not found) Then
		'			found = True
		'			LowWeight = (OutSD(i - 1) - Sd) / (OutSD(i - 1) - OutSD(i))
		'			HighWeight = 1 - LowWeight
		'			For j = 1 To NumSecs
		'				LocalOut100(PortfolioIndex, j) = LowWeight * OutX((i - 1) * NumSecs + j) + HighWeight * OutX((i - 2) * NumSecs + j)
		'			Next
		'		End If
		'	Next
		'End If

	End Function

	Public Sub ExportOptimisationParameters(Optional ByVal pOptimiserSnapshot As OptimiserDataClass = Nothing, Optional ByVal pFileName As String = "")
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************
		Dim OptimiserSnapshot As OptimiserDataClass

		Dim FP As Integer = FreeFile()
		Dim FileName As String = pFileName
		Dim RowCount As Integer
		Dim ColCount As Integer
		Dim ThisPrintString As String
		Dim PertracIDs() As Integer

		Try
			If (pOptimiserSnapshot Is Nothing) OrElse (pOptimiserSnapshot.ResultsSet = False) Then
				OptimiserSnapshot = Me.GetOptimisationData
			Else
				OptimiserSnapshot = pOptimiserSnapshot
			End If

		Catch ex As Exception

			Exit Sub
		End Try


		Try
			Dim thisFilePath As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)

			If (FileName.Length <= 0) Then
				FileName = "OptimiserParametersV2_" & Now.ToString(FILENAME_DATEFORMAT) & ".txt"
			End If

			FileName = Path.Combine(thisFilePath, FileName)

			FileOpen(FP, FileName, OpenMode.Output)

			PertracIDs = InstrumentIDs(OptimiserSnapshot)

			PrintLine(FP, "Genoa V2 - Optimiser Parameters")
			PrintLine(FP, "")

			'_OptimiserInitialised = False
			PrintLine(FP, "OptimiserInitialised = " & OptimiserSnapshot.OptimiserInitialised.ToString)

			'_OptimiserCompleted = False
			PrintLine(FP, "OptimiserCompleted = " & OptimiserSnapshot.OptimiserCompleted.ToString)

			'_ELambdaE = 0.000001 ' Default Value.
			PrintLine(FP, "ELambdaE = " & OptimiserSnapshot.ELambdaE.ToString("#,##0.0000########"))

			'_MaxCPs = 100
			PrintLine(FP, "MaxCPs = " & OptimiserSnapshot.MaxCPs.ToString("#,##0"))

			'OutCPNum
			PrintLine(FP, "Actual CPs = " & OptimiserSnapshot.CornerPortfolioCount.ToString("#,##0"))

			'_SimplexWasDegenerate = False
			PrintLine(FP, "SimplexWasDegenerate = " & OptimiserSnapshot.SimplexWasDegenerate.ToString)

			'_NumberOfRealSecurities = 0
			PrintLine(FP, "NumberOfRealSecurities = " & OptimiserSnapshot.NumberOfRealSecurities.ToString)

			'_NumberOfSlackVariables = 0
			PrintLine(FP, "NumberOfSlackVariables = " & OptimiserSnapshot.NumberOfSlackVariables.ToString)

			'_NumberOfConstraints = 0
			PrintLine(FP, "NumberOfConstraints = " & OptimiserSnapshot.NumberOfConstraints.ToString)

			'_VariableCount = 0
			PrintLine(FP, "VariableCount = " & OptimiserSnapshot.VariableCount.ToString)

			'SaveDebugInfo = 0
			PrintLine(FP, "SaveDebugInfo = " & OptimiserSnapshot.SaveDebugInfo.ToString)

			'_StatusString = ""
			PrintLine(FP, "StatusString = '" & OptimiserSnapshot.StatusString & "'")

			'_StatusString = ""
			PrintLine(FP, "Flag_NeverAllowZeroWeightedStocksBackIn = '" & Flag_NeverAllowZeroWeightedStocksBackIn.ToString & "'")

			If (StockBannedList IsNot Nothing) AndAlso (StockBannedList.Count > 0) Then
				Dim ArrayCounter As Integer

				PrintLine(FP, "Banned List :")

				For ArrayCounter = 0 To (StockBannedList.Count - 1)
					If (StockBannedList(ArrayCounter) >= PertracIDs.Length) Then
						PrintLine(FP, Chr(9) & "Instrument #" & CInt(StockBannedList(ArrayCounter)).ToString("0000") & Chr(9) & "<Slack Variable>")
					Else
						PrintLine(FP, Chr(9) & "Instrument #" & CInt(StockBannedList(ArrayCounter)).ToString("0000") & Chr(9) & "Pertrac : " & PertracIDs(StockBannedList(ArrayCounter)).ToString)
					End If
				Next

			Else
				PrintLine(FP, "No Instruments currently in the Banned List.")
			End If

			'ExpectedReturns = Nothing
			PrintLine(FP, "")
			PrintLine(FP, "ExpectedReturns / Std Error")

			If (OptimiserSnapshot.ExpectedReturns Is Nothing) Then
				PrintLine(FP, "<Nothing>")

			ElseIf (OptimiserSnapshot.ExpectedReturns.Length <= 0) Then
				PrintLine(FP, "Empty")

			Else
				For RowCount = 1 To (OptimiserSnapshot.ExpectedReturns.Length - 1)
					PrintLine(FP, RowCount.ToString("00000") & ") " & Chr(9) & OptimiserSnapshot.ExpectedReturns(RowCount).ToString("#,##0.0000%") & Chr(9) & OptimiserSnapshot.StdErrors(RowCount).ToString("#,##0.00000000"))
				Next
			End If

			' LowerLimit 

			PrintLine(FP, "")
			PrintLine(FP, "LowerLimit")

			If (OptimiserSnapshot.LowerLimit Is Nothing) Then
				PrintLine(FP, "<Nothing>")

			ElseIf (OptimiserSnapshot.LowerLimit.Length <= 0) Then
				PrintLine(FP, "Empty")

			Else
				For RowCount = 1 To (OptimiserSnapshot.LowerLimit.Length - 1)
					PrintLine(FP, RowCount.ToString("00000") & ") " & Chr(9) & OptimiserSnapshot.LowerLimit(RowCount).ToString("#,##0.0000%"))
				Next
			End If

			'UpperLimit = Nothing
			PrintLine(FP, "")
			PrintLine(FP, "UpperLimit")

			If (OptimiserSnapshot.UpperLimit Is Nothing) Then
				PrintLine(FP, "<Nothing>")

			ElseIf (OptimiserSnapshot.UpperLimit.Length <= 0) Then
				PrintLine(FP, "Empty")

			Else
				For RowCount = 1 To (OptimiserSnapshot.UpperLimit.Length - 1)
					PrintLine(FP, RowCount.ToString("00000") & ") " & Chr(9) & OptimiserSnapshot.UpperLimit(RowCount).ToString("#,##0.0000%"))
				Next
			End If

			'ConstraintCoefficient
			'ConstraintLimit

			PrintLine(FP, "")
			PrintLine(FP, "ConstraintCoefficient / ConstraintLimit")

			If (OptimiserSnapshot.ConstraintLimit Is Nothing) OrElse (OptimiserSnapshot.ConstraintCoefficient Is Nothing) Then
				PrintLine(FP, "<Nothing>")

			ElseIf (OptimiserSnapshot.ConstraintLimit.Length <= 0) OrElse (OptimiserSnapshot.ConstraintCoefficient.GetLength(0) <= 0) OrElse (OptimiserSnapshot.ConstraintCoefficient.GetLength(1) <= 0) Then
				PrintLine(FP, "Empty")

			Else
				ThisPrintString = Chr(9) & Chr(9) & Chr(9) & Chr(9)
				For ColCount = 1 To (OptimiserSnapshot.ConstraintCoefficient.GetUpperBound(1))
					If ColCount <= OptimiserSnapshot.NumberOfRealSecurities Then
						ThisPrintString &= ColCount.ToString("00000") & Chr(9)
					ElseIf ColCount <= (OptimiserSnapshot.NumberOfRealSecurities + OptimiserSnapshot.NumberOfSlackVariables) Then
						ThisPrintString &= "S" & (ColCount - (OptimiserSnapshot.NumberOfRealSecurities)).ToString("00000") & Chr(9)
					Else
						ThisPrintString &= "" & (ColCount - (OptimiserSnapshot.NumberOfRealSecurities + OptimiserSnapshot.NumberOfSlackVariables)).ToString("00000") & Chr(9)
					End If
				Next
				ThisPrintString &= "Limit"

				PrintLine(FP, ThisPrintString)

				For RowCount = 1 To (OptimiserSnapshot.ConstraintLimit.Length - 1)
					ThisPrintString = Chr(9) & Chr(9) & Chr(9) & "(" & RowCount.ToString("00000") & ")" & Chr(9)

					For ColCount = 1 To (OptimiserSnapshot.ConstraintCoefficient.GetUpperBound(1))
						ThisPrintString &= OptimiserSnapshot.ConstraintCoefficient(RowCount, ColCount).ToString("#,##0.0000") & Chr(9)
					Next
					ThisPrintString &= OptimiserSnapshot.ConstraintLimit(RowCount).ToString("#,##0.00######")

					PrintLine(FP, ThisPrintString)
				Next
			End If

			'CovarianceMatrix 
			PrintLine(FP, "")
			PrintLine(FP, "CovarianceMatrix")

			If (OptimiserSnapshot.CovarianceMatrix Is Nothing) Then
				PrintLine(FP, "<Nothing>")

			ElseIf (OptimiserSnapshot.CovarianceMatrix.GetLength(0) <= 0) OrElse (OptimiserSnapshot.CovarianceMatrix.GetLength(1) <= 0) Then
				PrintLine(FP, "Empty")

			Else
				ThisPrintString = Chr(9) & Chr(9) & Chr(9) & Chr(9)
				For ColCount = 1 To (OptimiserSnapshot.CovarianceMatrix.GetUpperBound(1))
					If ColCount <= OptimiserSnapshot.NumberOfRealSecurities Then
						ThisPrintString &= ColCount.ToString("00000") & Chr(9)
					Else
						ThisPrintString &= "S" & (ColCount - OptimiserSnapshot.NumberOfRealSecurities).ToString("00000") & Chr(9)
					End If
				Next

				PrintLine(FP, ThisPrintString)

				For RowCount = 1 To (OptimiserSnapshot.CovarianceMatrix.GetUpperBound(0))
					ThisPrintString = Chr(9) & Chr(9) & Chr(9) & RowCount.ToString("00000") & Chr(9)

					For ColCount = 1 To (OptimiserSnapshot.CovarianceMatrix.GetUpperBound(1))
						ThisPrintString &= OptimiserSnapshot.CovarianceMatrix(RowCount, ColCount).ToString("#,##0.0000######") & Chr(9)
					Next

					PrintLine(FP, ThisPrintString)
				Next
			End If

			PrintLine(FP, "")
			PrintLine(FP, "Results : ")
			PrintLine(FP, "")

			If (OptimiserSnapshot.CornerPortfolioCount > 0) AndAlso (OptimiserSnapshot.NumberOfRealSecurities > 0) Then
				'Dim PertracIDs(Math.Max(OptimiserSnapshot.NumberOfRealSecurities, OptimiserSnapshot.InstrumentNumerator.Count)) As Integer
				'Dim InstrumentKeys(OptimiserSnapshot.InstrumentNumerator.Count - 1) As Integer
				'OptimiserSnapshot.InstrumentNumerator.Keys.CopyTo(InstrumentKeys, 0)

				'For RowCount = 0 To (InstrumentKeys.Length - 1)
				'	PertracIDs(OptimiserSnapshot.InstrumentNumerator(InstrumentKeys(RowCount))) = InstrumentKeys(RowCount)
				'Next

				ThisPrintString = "CP#" & Chr(9) & "Expected Return" & Chr(9) & "Std Dev" & Chr(9) & "LambdaE" & Chr(9)
				For RowCount = 1 To OptimiserSnapshot.NumberOfRealSecurities
					ThisPrintString &= PertracIDs(RowCount).ToString & Chr(9)
				Next
				PrintLine(FP, ThisPrintString)

				For RowCount = 1 To OptimiserSnapshot.CornerPortfolioCount
					ThisPrintString = OptimiserSnapshot.CP_Number(RowCount).ToString & Chr(9) & OptimiserSnapshot.CP_ExpectedReturn(RowCount).ToString & Chr(9) & OptimiserSnapshot.CP_StandardDeviation(RowCount).ToString & Chr(9) & OptimiserSnapshot.CP_LambdaE(RowCount).ToString & Chr(9)

					Dim ThisWeightingArray() As Double
					ThisWeightingArray = OptimiserSnapshot.CP_WeightingArray(RowCount)

					If (ThisWeightingArray IsNot Nothing) AndAlso (ThisWeightingArray.Length > 0) Then

						For ColCount = 1 To (ThisWeightingArray.Length - 1)
							ThisPrintString &= ThisWeightingArray(ColCount).ToString & Chr(9)
						Next

					End If
					PrintLine(FP, ThisPrintString)

				Next

			Else

				PrintLine(FP, "No Results : # CP = 0 or # Securities = 0")

			End If

			If (OptimiserSnapshot.SaveDebugInfo) Then
				If (OptimiserSnapshot.CornerPortfolioCount > 0) AndAlso (OptimiserSnapshot.NumberOfRealSecurities > 0) Then
					PrintLine(FP, "DEBUG Info :-")

					For RowCount = 0 To OptimiserSnapshot.CornerPortfolioCount
						PrintLine(FP, "")
						PrintLine(FP, RowCount.ToString & ")")

						If (Debug_StaticVars(RowCount) IsNot Nothing) Then
							ThisPrintString = ""
							ThisPrintString = "InDirection=" & Debug_StaticVars(RowCount).InDirection.ToString & ", " & "OutDirection=" & Debug_StaticVars(RowCount).OutDirection.ToString & ", " & "jMaxA=" & Debug_StaticVars(RowCount).jMaxA.ToString & ", " & "jMaxB=" & Debug_StaticVars(RowCount).jMaxB.ToString & ", " & "lambdaA=" & Debug_StaticVars(RowCount).lambdaA.ToString & ", " & "lambdaB=" & Debug_StaticVars(RowCount).lambdaB.ToString & ", " & "jChange=" & Debug_StaticVars(RowCount).jChange.ToString & ", " & "jLast=" & Debug_StaticVars(RowCount).jLast.ToString & ", " & "jLastButOne=" & Debug_StaticVars(RowCount).jLastButOne.ToString
							PrintLine(FP, ThisPrintString)
						End If

						ThisPrintString = "InVars" & Chr(9)
						If (Debug_InVars(RowCount) IsNot Nothing) AndAlso (Debug_InVars(RowCount).Length > 0) Then
							For ColCount = 1 To (Debug_InVars(RowCount).Length - 1)
								If (Debug_InVars(RowCount)(ColCount) = 1) Then
									ThisPrintString &= "X" & Chr(9)
								Else
									ThisPrintString &= "_" & Chr(9)
								End If
							Next
						End If
						PrintLine(FP, ThisPrintString)

						ThisPrintString = "OutVars" & Chr(9)
						If (Debug_OutVars(RowCount) IsNot Nothing) AndAlso (Debug_OutVars(RowCount).Length > 0) Then
							For ColCount = 1 To (Debug_OutVars(RowCount).Length - 1)
								If (Debug_OutVars(RowCount)(ColCount) = 1) Then
									ThisPrintString &= "X" & Chr(9)
								Else
									ThisPrintString &= "_" & Chr(9)
								End If
							Next
						End If
						PrintLine(FP, ThisPrintString)

						ThisPrintString = "AlphaV" & Chr(9)
						If (Debug_AlphaV(RowCount) IsNot Nothing) AndAlso (Debug_AlphaV(RowCount).Length > 0) Then
							For ColCount = 1 To (Debug_AlphaV(RowCount).Length - 1)
								ThisPrintString &= Debug_AlphaV(RowCount)(ColCount).ToString & Chr(9)
							Next
						End If
						PrintLine(FP, ThisPrintString)

						ThisPrintString = "BetaV" & Chr(9)
						If (Debug_BetaV(RowCount) IsNot Nothing) AndAlso (Debug_BetaV(RowCount).Length > 0) Then
							For ColCount = 1 To (Debug_BetaV(RowCount).Length - 1)
								ThisPrintString &= Debug_BetaV(RowCount)(ColCount).ToString & Chr(9)
							Next
						End If
						PrintLine(FP, ThisPrintString)

						ThisPrintString = "BBar" & Chr(9)
						If (Debug_BBar(RowCount) IsNot Nothing) AndAlso (Debug_BBar(RowCount).Length > 0) Then
							For ColCount = 1 To (Debug_BBar(RowCount).Length - 1)
								ThisPrintString &= Debug_BBar(RowCount)(ColCount).ToString & Chr(9)
							Next
						End If
						PrintLine(FP, ThisPrintString)

						ThisPrintString = "ExpectedReturns" & Chr(9)
						If (Debug_ExpectedReturns(RowCount) IsNot Nothing) AndAlso (Debug_ExpectedReturns(RowCount).Length > 0) Then
							For ColCount = 1 To (Debug_ExpectedReturns(RowCount).Length - 1)
								ThisPrintString &= Debug_ExpectedReturns(RowCount)(ColCount).ToString & Chr(9)
							Next
						End If
						PrintLine(FP, ThisPrintString)

						ThisPrintString = "PortfolioWeight" & Chr(9)
						If (Debug_PortfolioWeight(RowCount) IsNot Nothing) AndAlso (Debug_PortfolioWeight(RowCount).Length > 0) Then
							For ColCount = 1 To (Debug_PortfolioWeight(RowCount).Length - 1)
								ThisPrintString &= Debug_PortfolioWeight(RowCount)(ColCount).ToString & Chr(9)
							Next
						End If
						PrintLine(FP, ThisPrintString)
						PrintLine(FP, "")


						If (Debug_Mi(RowCount) IsNot Nothing) Then
							Dim ThisMIArray(,) As Double = Debug_Mi(RowCount)
							Dim MiRowCount As Integer

							For MiRowCount = 1 To (ThisMIArray.GetUpperBound(0))
								ThisPrintString = "Mi " & MiRowCount.ToString("000") & Chr(9)

								For ColCount = 1 To (ThisMIArray.GetUpperBound(1))
									ThisPrintString &= ThisMIArray(MiRowCount, ColCount).ToString("#,##0.0000######") & Chr(9)
								Next

								PrintLine(FP, ThisPrintString)
							Next

							PrintLine(FP, "")
						End If

					Next

				End If
			End If


		Catch ex As Exception
		Finally
			Try
				FileClose(FP)
			Catch ex As Exception
			End Try
		End Try


	End Sub


End Class
