Imports System.Data.SqlClient
Imports System.IO
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass
Imports RenaissanceStatFunctions
Imports RenaissanceUtilities.DatePeriodFunctions
Imports MarkowitzClass

Public Class frmOptimise

	Inherits System.Windows.Forms.Form
	Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

  Private Sub New()
    MyBase.New()

    'This call is required by the Windows Form Designer.
    InitializeComponent()

    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents editAuditID As System.Windows.Forms.TextBox
  Friend WithEvents btnNavFirst As System.Windows.Forms.Button
  Friend WithEvents btnNavPrev As System.Windows.Forms.Button
  Friend WithEvents btnNavNext As System.Windows.Forms.Button
  Friend WithEvents btnLast As System.Windows.Forms.Button
  Friend WithEvents btnCancel As System.Windows.Forms.Button
  Friend WithEvents btnSave As System.Windows.Forms.Button
  Friend WithEvents Combo_SelectGroupID As System.Windows.Forms.ComboBox
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
  Friend WithEvents Label6 As System.Windows.Forms.Label
  Friend WithEvents Combo_SelectGroup As System.Windows.Forms.ComboBox
  Friend WithEvents Grid_Returns As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents StatusStrip_GroupReturns As System.Windows.Forms.StatusStrip
  Friend WithEvents StatusLabel_Optimisation As System.Windows.Forms.ToolStripStatusLabel
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents Label_GroupStartDate As System.Windows.Forms.Label
  Friend WithEvents Label_GroupEndDate As System.Windows.Forms.Label
  Friend WithEvents Label4 As System.Windows.Forms.Label
  Friend WithEvents GroupBox_Actions As System.Windows.Forms.GroupBox
  Friend WithEvents Btn_SetAllBenchmarkIndex As System.Windows.Forms.Button
  Friend WithEvents Btn_SetSingleBenchmarkIndex As System.Windows.Forms.Button
  Friend WithEvents Label3 As System.Windows.Forms.Label
  Friend WithEvents Combo_BenckmarkIndex As FCP_TelerikControls.FCP_RadComboBox
  Friend WithEvents Split_GroupReturns As System.Windows.Forms.SplitContainer
  Friend WithEvents Chart_VAMI As Dundas.Charting.WinControl.Chart
  Friend WithEvents Btn_ShowCarts As System.Windows.Forms.Button
  Friend WithEvents Chart_MonthlyReturns As Dundas.Charting.WinControl.Chart
  Friend WithEvents Label_ChartStock As System.Windows.Forms.Label
  Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
  Friend WithEvents Tab_Instruments As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Marginals As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Results As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Covariance As System.Windows.Forms.TabPage
  Friend WithEvents Tab_Constraints As System.Windows.Forms.TabPage
  Friend WithEvents Tab_OptimisationIO As System.Windows.Forms.TabPage
  Friend WithEvents Label5 As System.Windows.Forms.Label
  Friend WithEvents List_CovarianceMatrices As System.Windows.Forms.ListBox
  Friend WithEvents Split_Covariance As System.Windows.Forms.SplitContainer
  Friend WithEvents Radio_Covariance_UseSavedMatrix As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Covariance_UseLiveMatrix As System.Windows.Forms.RadioButton
  Friend WithEvents Panel_CovarianceMatrixSource As System.Windows.Forms.Panel
  Friend WithEvents Label9 As System.Windows.Forms.Label
  Friend WithEvents Label8 As System.Windows.Forms.Label
  Friend WithEvents Label7 As System.Windows.Forms.Label
  Friend WithEvents Label_MatrixName As System.Windows.Forms.Label
  Friend WithEvents Label_MatrixLamda As System.Windows.Forms.Label
  Friend WithEvents Label_MatrixDateTo As System.Windows.Forms.Label
  Friend WithEvents Label_MatrixDateFrom As System.Windows.Forms.Label
  Friend WithEvents Label_MatrixStatus As System.Windows.Forms.Label
  Friend WithEvents Label13 As System.Windows.Forms.Label
  Friend WithEvents Grid_Covariance As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents Label_HoldingReturn As System.Windows.Forms.Label
  Friend WithEvents Label11 As System.Windows.Forms.Label
  Friend WithEvents Label_HoldingRisk As System.Windows.Forms.Label
  Friend WithEvents Label14 As System.Windows.Forms.Label
  Friend WithEvents Label_HoldingValue As System.Windows.Forms.Label
  Friend WithEvents Label16 As System.Windows.Forms.Label
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents Grid_Marginals As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents GroupBox5 As System.Windows.Forms.GroupBox
  Friend WithEvents GroupBox4 As System.Windows.Forms.GroupBox
  Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
  Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
  Friend WithEvents Radio_ImproveReturn As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_ImproveTargetRiskReturn As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_ImproveSharpeRatio As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_ImproveRisk As System.Windows.Forms.RadioButton
  Friend WithEvents Label10 As System.Windows.Forms.Label
  Friend WithEvents Edit_Marginals_TradeSize As RenaissanceControls.NumericTextBox
  Friend WithEvents Check_Marginals_SellAll As System.Windows.Forms.CheckBox
  Friend WithEvents Check_Marginals_Sell As System.Windows.Forms.CheckBox
  Friend WithEvents Check_Marginals_Buy As System.Windows.Forms.CheckBox
  Friend WithEvents GroupBox6 As System.Windows.Forms.GroupBox
  Friend WithEvents Label17 As System.Windows.Forms.Label
  Friend WithEvents Label_MarginalsReturnTarget As System.Windows.Forms.Label
  Friend WithEvents Edit_Marginals_RiskLimit As RenaissanceControls.PercentageTextBox
  Friend WithEvents Label_MarginalsRiskLimit As System.Windows.Forms.Label
  Friend WithEvents Check_Marginals_SubstituteTradesOnly As System.Windows.Forms.CheckBox
  Friend WithEvents Check_Marginals_ContraTrade As System.Windows.Forms.CheckBox
  Friend WithEvents Edit_Marginals_RiskFreeRate As RenaissanceControls.PercentageTextBox
  Friend WithEvents Edit_Marginals_ReturnTarget As RenaissanceControls.PercentageTextBox
  Friend WithEvents Check_Marginals_ApplyBounds As System.Windows.Forms.CheckBox
  Friend WithEvents Combo_Marginals_ContraInstrument As System.Windows.Forms.ComboBox
  Friend WithEvents Check_Marginals_ExcludeCustomLimitBreaks As System.Windows.Forms.CheckBox
  Friend WithEvents Check_Marginals_ExcludeLiquidityBreaks As System.Windows.Forms.CheckBox
  Friend WithEvents Check_Marginals_ExcludeSectorBreaks As System.Windows.Forms.CheckBox
  Friend WithEvents Check_Marginals_ExcludeInstrumentBreaks As System.Windows.Forms.CheckBox
  Friend WithEvents Label18 As System.Windows.Forms.Label
  Friend WithEvents Panel3 As System.Windows.Forms.Panel
  Friend WithEvents Label19 As System.Windows.Forms.Label
  Friend WithEvents Label_NewHoldingValue As System.Windows.Forms.Label
  Friend WithEvents Label21 As System.Windows.Forms.Label
  Friend WithEvents Label22 As System.Windows.Forms.Label
  Friend WithEvents Label_NewHoldingReturn As System.Windows.Forms.Label
  Friend WithEvents Label_NewHoldingRisk As System.Windows.Forms.Label
  Friend WithEvents Label25 As System.Windows.Forms.Label
  Friend WithEvents Label20 As System.Windows.Forms.Label
  Friend WithEvents Constraints_ComboConstraintsGoup As System.Windows.Forms.ComboBox
  Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
  Friend WithEvents Constraints_TabConstraints As System.Windows.Forms.TabControl
  Friend WithEvents Tab_Template As System.Windows.Forms.TabPage
  Friend WithEvents Constraint_Button_AllActive As System.Windows.Forms.Button
  Friend WithEvents Constraint_Button_NoneActive As System.Windows.Forms.Button
  Friend WithEvents Label23 As System.Windows.Forms.Label
  Friend WithEvents Constraint_Button_CancelGroup As System.Windows.Forms.Button
  Friend WithEvents Constraint_Button_SaveGroup As System.Windows.Forms.Button
  Friend WithEvents Panel5 As System.Windows.Forms.Panel
  Friend WithEvents Label24 As System.Windows.Forms.Label
  Friend WithEvents Panel4 As System.Windows.Forms.Panel
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  Friend WithEvents Constraints_Grid_Defined As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents Constraint_Button_NewGroup As System.Windows.Forms.Button
  Friend WithEvents Label26 As System.Windows.Forms.Label
  Friend WithEvents Panel2 As System.Windows.Forms.Panel
  Friend WithEvents Constraint_Button_DeleteGroup As System.Windows.Forms.Button
  Friend WithEvents Constraint_Button_RenameGroup As System.Windows.Forms.Button
  Friend WithEvents Constraints_Grid_Template As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents Constraint_Button_NewGroup_Copy As System.Windows.Forms.Button
  Friend WithEvents Chart_Results As Dundas.Charting.WinControl.Chart
  Friend WithEvents Results_Btn_ClearResults As System.Windows.Forms.Button
  Friend WithEvents Results_Btn_PlotRiskReturn As System.Windows.Forms.Button
  Friend WithEvents Button_Marginals_Trade As System.Windows.Forms.Button
  Friend WithEvents Btn_RunOptimisation As System.Windows.Forms.Button
  Friend WithEvents Radio_Covariance_EditCovMatrix As System.Windows.Forms.RadioButton
  Friend WithEvents Button_SaveAsCovMatrix As System.Windows.Forms.Button
  Friend WithEvents Button_SaveCovMatrix As System.Windows.Forms.Button
  Friend WithEvents Grid_OptimisationResults As C1.Win.C1FlexGrid.C1FlexGrid
  Friend WithEvents Optimisation_CpLimit As RenaissanceControls.NumericTextBox
  Friend WithEvents Label27 As System.Windows.Forms.Label
  Friend WithEvents Label28 As System.Windows.Forms.Label
  Friend WithEvents Optimisation_ELambdaE As RenaissanceControls.NumericTextBox
  Friend WithEvents Optimisation_Check_SaveResultsToFile As System.Windows.Forms.CheckBox
  Friend WithEvents GroupBox7 As System.Windows.Forms.GroupBox
  Friend WithEvents Label31 As System.Windows.Forms.Label
  Friend WithEvents Results_Text_MinTradeSize As RenaissanceControls.NumericTextBox
  Friend WithEvents Label29 As System.Windows.Forms.Label
  Friend WithEvents Results_Text_TargetHolding As RenaissanceControls.NumericTextBox
  Friend WithEvents Label30 As System.Windows.Forms.Label
  Friend WithEvents Results_Text_TargetVolatility As RenaissanceControls.PercentageTextBox
  Friend WithEvents Results_Btn_RoundTrades As System.Windows.Forms.Button
  Friend WithEvents Results_Btn_CaptureNewHoldings As System.Windows.Forms.Button
  Friend WithEvents Label32 As System.Windows.Forms.Label
  Friend WithEvents Results_Text_TargetReturn As RenaissanceControls.PercentageTextBox
  Friend WithEvents Results_Radio_TargetReturn As System.Windows.Forms.RadioButton
  Friend WithEvents Results_Radio_TargetVolatility As System.Windows.Forms.RadioButton
  Friend WithEvents CheckBox_ZeroWeightOptimiserFix As System.Windows.Forms.CheckBox
  Friend WithEvents checkbox_UseMarginalsIteration As System.Windows.Forms.CheckBox
  Friend WithEvents Edit_MarginalsMoveCount As RenaissanceControls.NumericTextBox
  Friend WithEvents Label33 As System.Windows.Forms.Label
  Friend WithEvents Check_Covariance_BackfillData As System.Windows.Forms.CheckBox
  Friend WithEvents CheckBox_RunUnconstrained As System.Windows.Forms.CheckBox
  Friend WithEvents Button_Marginals_TradeTop1 As System.Windows.Forms.Button
  Friend WithEvents GroupBox8 As System.Windows.Forms.GroupBox
  Friend WithEvents Label34 As System.Windows.Forms.Label
  Friend WithEvents Results_Radio_VAR As System.Windows.Forms.RadioButton
  Friend WithEvents Results_Radio_Volatility As System.Windows.Forms.RadioButton
  Friend WithEvents Label12 As System.Windows.Forms.Label
  Friend WithEvents Results_Text_ConfidenceLevel As RenaissanceControls.PercentageTextBox
  Friend WithEvents Label15 As System.Windows.Forms.Label
  Friend WithEvents Label35 As System.Windows.Forms.Label
  Friend WithEvents Results_Text_ConfidencePeriods As RenaissanceControls.NumericTextBox
  Friend WithEvents Label36 As System.Windows.Forms.Label
  Friend WithEvents Results_Radio_vsPortfolioValue As System.Windows.Forms.RadioButton
  Friend WithEvents Results_Radio_vsNewHolding As System.Windows.Forms.RadioButton
  Friend WithEvents Results_Text_PortfolioValue As RenaissanceControls.NumericTextBox
  Friend WithEvents Label37 As System.Windows.Forms.Label
  Friend WithEvents Results_Check_ShowVsPortfolio As System.Windows.Forms.CheckBox
  Friend WithEvents Panel_PortfolioValues As System.Windows.Forms.Panel
  Friend WithEvents Radio_ImproveWorstDrawDown As System.Windows.Forms.RadioButton
  Friend WithEvents Label38 As System.Windows.Forms.Label
  Friend WithEvents Label_HoldingDrawDown As System.Windows.Forms.Label
  Friend WithEvents Label40 As System.Windows.Forms.Label
  Friend WithEvents Label_NewHoldingDrawDown As System.Windows.Forms.Label
  Friend WithEvents Radio_ImproveStdError As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_ImproveLeastSquares As System.Windows.Forms.RadioButton
  Friend WithEvents Label39 As System.Windows.Forms.Label
  Friend WithEvents Combo_MarginalIndex As FCP_TelerikControls.FCP_RadComboBox
  Friend WithEvents btn_ShowGroupChart As System.Windows.Forms.Button
  Friend WithEvents Check_Marginals_AnalysisPeriod As System.Windows.Forms.CheckBox
  Friend WithEvents Edit_Marginals_AnalysisPeriod As RenaissanceControls.NumericTextBox
  Friend WithEvents btnClose As System.Windows.Forms.Button
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim ChartArea10 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend10 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series19 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series20 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea11 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend11 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series21 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series22 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea12 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend12 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series23 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series24 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmOptimise))
    Me.editAuditID = New System.Windows.Forms.TextBox
    Me.btnNavFirst = New System.Windows.Forms.Button
    Me.btnNavPrev = New System.Windows.Forms.Button
    Me.btnNavNext = New System.Windows.Forms.Button
    Me.btnLast = New System.Windows.Forms.Button
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.Combo_SelectGroupID = New System.Windows.Forms.ComboBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.btnClose = New System.Windows.Forms.Button
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Label6 = New System.Windows.Forms.Label
    Me.Combo_SelectGroup = New System.Windows.Forms.ComboBox
    Me.Grid_Returns = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.StatusStrip_GroupReturns = New System.Windows.Forms.StatusStrip
    Me.StatusLabel_Optimisation = New System.Windows.Forms.ToolStripStatusLabel
    Me.Label2 = New System.Windows.Forms.Label
    Me.Label_GroupStartDate = New System.Windows.Forms.Label
    Me.Label_GroupEndDate = New System.Windows.Forms.Label
    Me.Label4 = New System.Windows.Forms.Label
    Me.GroupBox_Actions = New System.Windows.Forms.GroupBox
    Me.Btn_SetAllBenchmarkIndex = New System.Windows.Forms.Button
    Me.Btn_SetSingleBenchmarkIndex = New System.Windows.Forms.Button
    Me.Label3 = New System.Windows.Forms.Label
    Me.Combo_BenckmarkIndex = New FCP_TelerikControls.FCP_RadComboBox
    Me.Split_GroupReturns = New System.Windows.Forms.SplitContainer
    Me.Label_ChartStock = New System.Windows.Forms.Label
    Me.Chart_MonthlyReturns = New Dundas.Charting.WinControl.Chart
    Me.Chart_VAMI = New Dundas.Charting.WinControl.Chart
    Me.Btn_ShowCarts = New System.Windows.Forms.Button
    Me.TabControl1 = New System.Windows.Forms.TabControl
    Me.Tab_Instruments = New System.Windows.Forms.TabPage
    Me.btn_ShowGroupChart = New System.Windows.Forms.Button
    Me.Tab_Marginals = New System.Windows.Forms.TabPage
    Me.Grid_Marginals = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.GroupBox5 = New System.Windows.Forms.GroupBox
    Me.Button_Marginals_TradeTop1 = New System.Windows.Forms.Button
    Me.Button_Marginals_Trade = New System.Windows.Forms.Button
    Me.GroupBox4 = New System.Windows.Forms.GroupBox
    Me.Check_Marginals_ExcludeCustomLimitBreaks = New System.Windows.Forms.CheckBox
    Me.Check_Marginals_ExcludeLiquidityBreaks = New System.Windows.Forms.CheckBox
    Me.Check_Marginals_ExcludeSectorBreaks = New System.Windows.Forms.CheckBox
    Me.Check_Marginals_ExcludeInstrumentBreaks = New System.Windows.Forms.CheckBox
    Me.GroupBox3 = New System.Windows.Forms.GroupBox
    Me.Check_Marginals_ApplyBounds = New System.Windows.Forms.CheckBox
    Me.Edit_Marginals_RiskFreeRate = New RenaissanceControls.PercentageTextBox
    Me.Edit_Marginals_ReturnTarget = New RenaissanceControls.PercentageTextBox
    Me.Label17 = New System.Windows.Forms.Label
    Me.Label_MarginalsReturnTarget = New System.Windows.Forms.Label
    Me.Edit_Marginals_RiskLimit = New RenaissanceControls.PercentageTextBox
    Me.Label_MarginalsRiskLimit = New System.Windows.Forms.Label
    Me.GroupBox2 = New System.Windows.Forms.GroupBox
    Me.Check_Marginals_AnalysisPeriod = New System.Windows.Forms.CheckBox
    Me.Edit_Marginals_AnalysisPeriod = New RenaissanceControls.NumericTextBox
    Me.Combo_MarginalIndex = New FCP_TelerikControls.FCP_RadComboBox
    Me.Label39 = New System.Windows.Forms.Label
    Me.Radio_ImproveStdError = New System.Windows.Forms.RadioButton
    Me.Radio_ImproveLeastSquares = New System.Windows.Forms.RadioButton
    Me.Radio_ImproveWorstDrawDown = New System.Windows.Forms.RadioButton
    Me.Radio_ImproveTargetRiskReturn = New System.Windows.Forms.RadioButton
    Me.Radio_ImproveSharpeRatio = New System.Windows.Forms.RadioButton
    Me.Radio_ImproveRisk = New System.Windows.Forms.RadioButton
    Me.Radio_ImproveReturn = New System.Windows.Forms.RadioButton
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.Combo_Marginals_ContraInstrument = New System.Windows.Forms.ComboBox
    Me.Check_Marginals_SubstituteTradesOnly = New System.Windows.Forms.CheckBox
    Me.Check_Marginals_ContraTrade = New System.Windows.Forms.CheckBox
    Me.GroupBox6 = New System.Windows.Forms.GroupBox
    Me.Check_Marginals_Buy = New System.Windows.Forms.CheckBox
    Me.Check_Marginals_Sell = New System.Windows.Forms.CheckBox
    Me.Check_Marginals_SellAll = New System.Windows.Forms.CheckBox
    Me.Edit_Marginals_TradeSize = New RenaissanceControls.NumericTextBox
    Me.Label10 = New System.Windows.Forms.Label
    Me.Tab_Results = New System.Windows.Forms.TabPage
    Me.GroupBox8 = New System.Windows.Forms.GroupBox
    Me.Panel_PortfolioValues = New System.Windows.Forms.Panel
    Me.Label36 = New System.Windows.Forms.Label
    Me.Results_Text_PortfolioValue = New RenaissanceControls.NumericTextBox
    Me.Results_Radio_vsPortfolioValue = New System.Windows.Forms.RadioButton
    Me.Label37 = New System.Windows.Forms.Label
    Me.Results_Radio_vsNewHolding = New System.Windows.Forms.RadioButton
    Me.Results_Check_ShowVsPortfolio = New System.Windows.Forms.CheckBox
    Me.Label35 = New System.Windows.Forms.Label
    Me.Results_Text_ConfidencePeriods = New RenaissanceControls.NumericTextBox
    Me.Label34 = New System.Windows.Forms.Label
    Me.Results_Radio_VAR = New System.Windows.Forms.RadioButton
    Me.Results_Radio_Volatility = New System.Windows.Forms.RadioButton
    Me.Label12 = New System.Windows.Forms.Label
    Me.Results_Text_ConfidenceLevel = New RenaissanceControls.PercentageTextBox
    Me.Label15 = New System.Windows.Forms.Label
    Me.GroupBox7 = New System.Windows.Forms.GroupBox
    Me.Results_Radio_TargetReturn = New System.Windows.Forms.RadioButton
    Me.Results_Radio_TargetVolatility = New System.Windows.Forms.RadioButton
    Me.Label32 = New System.Windows.Forms.Label
    Me.Results_Text_TargetReturn = New RenaissanceControls.PercentageTextBox
    Me.Results_Btn_RoundTrades = New System.Windows.Forms.Button
    Me.Results_Btn_CaptureNewHoldings = New System.Windows.Forms.Button
    Me.Label31 = New System.Windows.Forms.Label
    Me.Results_Text_MinTradeSize = New RenaissanceControls.NumericTextBox
    Me.Label29 = New System.Windows.Forms.Label
    Me.Results_Text_TargetHolding = New RenaissanceControls.NumericTextBox
    Me.Label30 = New System.Windows.Forms.Label
    Me.Results_Text_TargetVolatility = New RenaissanceControls.PercentageTextBox
    Me.Results_Btn_PlotRiskReturn = New System.Windows.Forms.Button
    Me.Results_Btn_ClearResults = New System.Windows.Forms.Button
    Me.Chart_Results = New Dundas.Charting.WinControl.Chart
    Me.Tab_Covariance = New System.Windows.Forms.TabPage
    Me.Split_Covariance = New System.Windows.Forms.SplitContainer
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Label_MatrixStatus = New System.Windows.Forms.Label
    Me.Label13 = New System.Windows.Forms.Label
    Me.Label_MatrixLamda = New System.Windows.Forms.Label
    Me.Label_MatrixDateTo = New System.Windows.Forms.Label
    Me.Label_MatrixDateFrom = New System.Windows.Forms.Label
    Me.Label9 = New System.Windows.Forms.Label
    Me.Label8 = New System.Windows.Forms.Label
    Me.Label7 = New System.Windows.Forms.Label
    Me.Label_MatrixName = New System.Windows.Forms.Label
    Me.Panel_CovarianceMatrixSource = New System.Windows.Forms.Panel
    Me.Check_Covariance_BackfillData = New System.Windows.Forms.CheckBox
    Me.Button_SaveAsCovMatrix = New System.Windows.Forms.Button
    Me.Button_SaveCovMatrix = New System.Windows.Forms.Button
    Me.Radio_Covariance_EditCovMatrix = New System.Windows.Forms.RadioButton
    Me.Radio_Covariance_UseSavedMatrix = New System.Windows.Forms.RadioButton
    Me.Radio_Covariance_UseLiveMatrix = New System.Windows.Forms.RadioButton
    Me.Label5 = New System.Windows.Forms.Label
    Me.List_CovarianceMatrices = New System.Windows.Forms.ListBox
    Me.Grid_Covariance = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Tab_Constraints = New System.Windows.Forms.TabPage
    Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
    Me.Panel5 = New System.Windows.Forms.Panel
    Me.Constraints_Grid_Defined = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Constraint_Button_AllActive = New System.Windows.Forms.Button
    Me.Constraint_Button_NoneActive = New System.Windows.Forms.Button
    Me.Label24 = New System.Windows.Forms.Label
    Me.Label23 = New System.Windows.Forms.Label
    Me.Panel4 = New System.Windows.Forms.Panel
    Me.Constraint_Button_NewGroup_Copy = New System.Windows.Forms.Button
    Me.Constraint_Button_DeleteGroup = New System.Windows.Forms.Button
    Me.Constraint_Button_RenameGroup = New System.Windows.Forms.Button
    Me.Constraint_Button_NewGroup = New System.Windows.Forms.Button
    Me.Label26 = New System.Windows.Forms.Label
    Me.Constraint_Button_CancelGroup = New System.Windows.Forms.Button
    Me.Constraints_ComboConstraintsGoup = New System.Windows.Forms.ComboBox
    Me.Label20 = New System.Windows.Forms.Label
    Me.Constraint_Button_SaveGroup = New System.Windows.Forms.Button
    Me.Constraints_TabConstraints = New System.Windows.Forms.TabControl
    Me.Tab_Template = New System.Windows.Forms.TabPage
    Me.Constraints_Grid_Template = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Tab_OptimisationIO = New System.Windows.Forms.TabPage
    Me.CheckBox_RunUnconstrained = New System.Windows.Forms.CheckBox
    Me.Edit_MarginalsMoveCount = New RenaissanceControls.NumericTextBox
    Me.Label33 = New System.Windows.Forms.Label
    Me.checkbox_UseMarginalsIteration = New System.Windows.Forms.CheckBox
    Me.CheckBox_ZeroWeightOptimiserFix = New System.Windows.Forms.CheckBox
    Me.Optimisation_Check_SaveResultsToFile = New System.Windows.Forms.CheckBox
    Me.Label28 = New System.Windows.Forms.Label
    Me.Optimisation_ELambdaE = New RenaissanceControls.NumericTextBox
    Me.Label27 = New System.Windows.Forms.Label
    Me.Optimisation_CpLimit = New RenaissanceControls.NumericTextBox
    Me.Grid_OptimisationResults = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Btn_RunOptimisation = New System.Windows.Forms.Button
    Me.Label_HoldingReturn = New System.Windows.Forms.Label
    Me.Label11 = New System.Windows.Forms.Label
    Me.Label_HoldingRisk = New System.Windows.Forms.Label
    Me.Label14 = New System.Windows.Forms.Label
    Me.Label_HoldingValue = New System.Windows.Forms.Label
    Me.Label16 = New System.Windows.Forms.Label
    Me.Panel2 = New System.Windows.Forms.Panel
    Me.Label38 = New System.Windows.Forms.Label
    Me.Label_HoldingDrawDown = New System.Windows.Forms.Label
    Me.Label18 = New System.Windows.Forms.Label
    Me.Panel3 = New System.Windows.Forms.Panel
    Me.Label40 = New System.Windows.Forms.Label
    Me.Label_NewHoldingDrawDown = New System.Windows.Forms.Label
    Me.Label19 = New System.Windows.Forms.Label
    Me.Label_NewHoldingValue = New System.Windows.Forms.Label
    Me.Label21 = New System.Windows.Forms.Label
    Me.Label22 = New System.Windows.Forms.Label
    Me.Label_NewHoldingReturn = New System.Windows.Forms.Label
    Me.Label_NewHoldingRisk = New System.Windows.Forms.Label
    Me.Label25 = New System.Windows.Forms.Label
    CType(Me.Grid_Returns, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.StatusStrip_GroupReturns.SuspendLayout()
    Me.GroupBox_Actions.SuspendLayout()
    CType(Me.Combo_BenckmarkIndex, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Split_GroupReturns.Panel1.SuspendLayout()
    Me.Split_GroupReturns.Panel2.SuspendLayout()
    Me.Split_GroupReturns.SuspendLayout()
    CType(Me.Chart_MonthlyReturns, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Chart_VAMI, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.TabControl1.SuspendLayout()
    Me.Tab_Instruments.SuspendLayout()
    Me.Tab_Marginals.SuspendLayout()
    CType(Me.Grid_Marginals, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.GroupBox5.SuspendLayout()
    Me.GroupBox4.SuspendLayout()
    Me.GroupBox3.SuspendLayout()
    Me.GroupBox2.SuspendLayout()
    CType(Me.Combo_MarginalIndex, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.GroupBox1.SuspendLayout()
    Me.GroupBox6.SuspendLayout()
    Me.Tab_Results.SuspendLayout()
    Me.GroupBox8.SuspendLayout()
    Me.Panel_PortfolioValues.SuspendLayout()
    Me.GroupBox7.SuspendLayout()
    CType(Me.Chart_Results, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Covariance.SuspendLayout()
    Me.Split_Covariance.Panel1.SuspendLayout()
    Me.Split_Covariance.Panel2.SuspendLayout()
    Me.Split_Covariance.SuspendLayout()
    Me.Panel1.SuspendLayout()
    Me.Panel_CovarianceMatrixSource.SuspendLayout()
    CType(Me.Grid_Covariance, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_Constraints.SuspendLayout()
    Me.SplitContainer1.Panel1.SuspendLayout()
    Me.SplitContainer1.Panel2.SuspendLayout()
    Me.SplitContainer1.SuspendLayout()
    Me.Panel5.SuspendLayout()
    CType(Me.Constraints_Grid_Defined, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel4.SuspendLayout()
    Me.Constraints_TabConstraints.SuspendLayout()
    Me.Tab_Template.SuspendLayout()
    CType(Me.Constraints_Grid_Template, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Tab_OptimisationIO.SuspendLayout()
    CType(Me.Grid_OptimisationResults, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel2.SuspendLayout()
    Me.Panel3.SuspendLayout()
    Me.SuspendLayout()
    '
    'editAuditID
    '
    Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.editAuditID.Enabled = False
    Me.editAuditID.Location = New System.Drawing.Point(1023, 32)
    Me.editAuditID.Name = "editAuditID"
    Me.editAuditID.Size = New System.Drawing.Size(50, 20)
    Me.editAuditID.TabIndex = 9
    '
    'btnNavFirst
    '
    Me.btnNavFirst.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavFirst.Location = New System.Drawing.Point(861, 31)
    Me.btnNavFirst.Name = "btnNavFirst"
    Me.btnNavFirst.Size = New System.Drawing.Size(40, 23)
    Me.btnNavFirst.TabIndex = 2
    Me.btnNavFirst.Text = "<<"
    '
    'btnNavPrev
    '
    Me.btnNavPrev.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavPrev.Location = New System.Drawing.Point(903, 31)
    Me.btnNavPrev.Name = "btnNavPrev"
    Me.btnNavPrev.Size = New System.Drawing.Size(35, 23)
    Me.btnNavPrev.TabIndex = 3
    Me.btnNavPrev.Text = "<"
    '
    'btnNavNext
    '
    Me.btnNavNext.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavNext.Location = New System.Drawing.Point(941, 31)
    Me.btnNavNext.Name = "btnNavNext"
    Me.btnNavNext.Size = New System.Drawing.Size(35, 23)
    Me.btnNavNext.TabIndex = 4
    Me.btnNavNext.Text = ">"
    '
    'btnLast
    '
    Me.btnLast.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnLast.Location = New System.Drawing.Point(977, 31)
    Me.btnLast.Name = "btnLast"
    Me.btnLast.Size = New System.Drawing.Size(40, 23)
    Me.btnLast.TabIndex = 5
    Me.btnLast.Text = ">>"
    '
    'btnCancel
    '
    Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(175, 502)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 5
    Me.btnCancel.Text = "Cancel"
    '
    'btnSave
    '
    Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(3, 502)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 28)
    Me.btnSave.TabIndex = 4
    Me.btnSave.Text = "Save"
    '
    'Combo_SelectGroupID
    '
    Me.Combo_SelectGroupID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SelectGroupID.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectGroupID.Location = New System.Drawing.Point(383, 33)
    Me.Combo_SelectGroupID.Name = "Combo_SelectGroupID"
    Me.Combo_SelectGroupID.Size = New System.Drawing.Size(472, 21)
    Me.Combo_SelectGroupID.TabIndex = 1
    '
    'Label1
    '
    Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label1.Location = New System.Drawing.Point(304, 35)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(73, 14)
    Me.Label1.TabIndex = 18
    Me.Label1.Text = "Select Group"
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(259, 502)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 6
    Me.btnClose.Text = "Close"
    '
    'RootMenu
    '
    Me.RootMenu.AllowMerge = False
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(1077, 24)
    Me.RootMenu.TabIndex = 10
    Me.RootMenu.Text = "MenuStrip1"
    '
    'Label6
    '
    Me.Label6.AutoSize = True
    Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label6.Location = New System.Drawing.Point(6, 36)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(85, 13)
    Me.Label6.TabIndex = 100
    Me.Label6.Text = "Group Collection"
    '
    'Combo_SelectGroup
    '
    Me.Combo_SelectGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectGroup.Location = New System.Drawing.Point(100, 33)
    Me.Combo_SelectGroup.Name = "Combo_SelectGroup"
    Me.Combo_SelectGroup.Size = New System.Drawing.Size(198, 21)
    Me.Combo_SelectGroup.TabIndex = 0
    '
    'Grid_Returns
    '
    Me.Grid_Returns.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Returns.ColumnInfo = "10,0,0,0,0,85,Columns:0{Width:170;}" & Global.Microsoft.VisualBasic.ChrW(9)
    Me.Grid_Returns.Location = New System.Drawing.Point(0, 0)
    Me.Grid_Returns.Name = "Grid_Returns"
    Me.Grid_Returns.Rows.Count = 2
    Me.Grid_Returns.Rows.DefaultSize = 17
    Me.Grid_Returns.Size = New System.Drawing.Size(719, 422)
    Me.Grid_Returns.TabIndex = 0
    '
    'StatusStrip_GroupReturns
    '
    Me.StatusStrip_GroupReturns.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusLabel_Optimisation})
    Me.StatusStrip_GroupReturns.Location = New System.Drawing.Point(0, 662)
    Me.StatusStrip_GroupReturns.Name = "StatusStrip_GroupReturns"
    Me.StatusStrip_GroupReturns.Size = New System.Drawing.Size(1077, 22)
    Me.StatusStrip_GroupReturns.TabIndex = 103
    Me.StatusStrip_GroupReturns.Text = "StatusStrip1"
    '
    'StatusLabel_Optimisation
    '
    Me.StatusLabel_Optimisation.Name = "StatusLabel_Optimisation"
    Me.StatusLabel_Optimisation.Size = New System.Drawing.Size(11, 17)
    Me.StatusLabel_Optimisation.Text = "."
    '
    'Label2
    '
    Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label2.Location = New System.Drawing.Point(6, 3)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(94, 19)
    Me.Label2.TabIndex = 104
    Me.Label2.Text = "Group Start Date"
    '
    'Label_GroupStartDate
    '
    Me.Label_GroupStartDate.BackColor = System.Drawing.SystemColors.Window
    Me.Label_GroupStartDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_GroupStartDate.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_GroupStartDate.Location = New System.Drawing.Point(106, 3)
    Me.Label_GroupStartDate.Name = "Label_GroupStartDate"
    Me.Label_GroupStartDate.Size = New System.Drawing.Size(129, 19)
    Me.Label_GroupStartDate.TabIndex = 0
    '
    'Label_GroupEndDate
    '
    Me.Label_GroupEndDate.BackColor = System.Drawing.SystemColors.Window
    Me.Label_GroupEndDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_GroupEndDate.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_GroupEndDate.Location = New System.Drawing.Point(106, 25)
    Me.Label_GroupEndDate.Name = "Label_GroupEndDate"
    Me.Label_GroupEndDate.Size = New System.Drawing.Size(129, 19)
    Me.Label_GroupEndDate.TabIndex = 1
    '
    'Label4
    '
    Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label4.Location = New System.Drawing.Point(6, 26)
    Me.Label4.Name = "Label4"
    Me.Label4.Size = New System.Drawing.Size(94, 18)
    Me.Label4.TabIndex = 106
    Me.Label4.Text = "Group End Date"
    '
    'GroupBox_Actions
    '
    Me.GroupBox_Actions.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox_Actions.Controls.Add(Me.Btn_SetAllBenchmarkIndex)
    Me.GroupBox_Actions.Controls.Add(Me.Btn_SetSingleBenchmarkIndex)
    Me.GroupBox_Actions.Controls.Add(Me.Label3)
    Me.GroupBox_Actions.Controls.Add(Me.Combo_BenckmarkIndex)
    Me.GroupBox_Actions.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.GroupBox_Actions.Location = New System.Drawing.Point(241, 3)
    Me.GroupBox_Actions.Name = "GroupBox_Actions"
    Me.GroupBox_Actions.Size = New System.Drawing.Size(820, 67)
    Me.GroupBox_Actions.TabIndex = 3
    Me.GroupBox_Actions.TabStop = False
    Me.GroupBox_Actions.Text = "Set Benchmark Index"
    '
    'Btn_SetAllBenchmarkIndex
    '
    Me.Btn_SetAllBenchmarkIndex.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Btn_SetAllBenchmarkIndex.Location = New System.Drawing.Point(317, 39)
    Me.Btn_SetAllBenchmarkIndex.Name = "Btn_SetAllBenchmarkIndex"
    Me.Btn_SetAllBenchmarkIndex.Size = New System.Drawing.Size(188, 23)
    Me.Btn_SetAllBenchmarkIndex.TabIndex = 2
    Me.Btn_SetAllBenchmarkIndex.Text = "Set As Benckmark - ALL Lines"
    '
    'Btn_SetSingleBenchmarkIndex
    '
    Me.Btn_SetSingleBenchmarkIndex.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Btn_SetSingleBenchmarkIndex.Location = New System.Drawing.Point(123, 39)
    Me.Btn_SetSingleBenchmarkIndex.Name = "Btn_SetSingleBenchmarkIndex"
    Me.Btn_SetSingleBenchmarkIndex.Size = New System.Drawing.Size(188, 23)
    Me.Btn_SetSingleBenchmarkIndex.TabIndex = 1
    Me.Btn_SetSingleBenchmarkIndex.Text = "Set As Benckmark - This Line Only"
    '
    'Label3
    '
    Me.Label3.AutoSize = True
    Me.Label3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label3.Location = New System.Drawing.Point(8, 17)
    Me.Label3.Name = "Label3"
    Me.Label3.Size = New System.Drawing.Size(89, 13)
    Me.Label3.TabIndex = 102
    Me.Label3.Text = "Select Instrument"
    '
    'Combo_BenckmarkIndex
    '
    Me.Combo_BenckmarkIndex.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_BenckmarkIndex.Location = New System.Drawing.Point(123, 14)
    Me.Combo_BenckmarkIndex.MasternameCollection = Nothing
    Me.Combo_BenckmarkIndex.Name = "Combo_BenckmarkIndex"
    '
    '
    '
    Me.Combo_BenckmarkIndex.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
    Me.Combo_BenckmarkIndex.SelectNoMatch = False
    Me.Combo_BenckmarkIndex.Size = New System.Drawing.Size(689, 20)
    Me.Combo_BenckmarkIndex.TabIndex = 0
    Me.Combo_BenckmarkIndex.ThemeName = "ControlDefault"
    '
    'Split_GroupReturns
    '
    Me.Split_GroupReturns.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Split_GroupReturns.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Split_GroupReturns.Location = New System.Drawing.Point(3, 73)
    Me.Split_GroupReturns.Margin = New System.Windows.Forms.Padding(0)
    Me.Split_GroupReturns.Name = "Split_GroupReturns"
    '
    'Split_GroupReturns.Panel1
    '
    Me.Split_GroupReturns.Panel1.Controls.Add(Me.Grid_Returns)
    '
    'Split_GroupReturns.Panel2
    '
    Me.Split_GroupReturns.Panel2.Controls.Add(Me.Label_ChartStock)
    Me.Split_GroupReturns.Panel2.Controls.Add(Me.Chart_MonthlyReturns)
    Me.Split_GroupReturns.Panel2.Controls.Add(Me.Chart_VAMI)
    Me.Split_GroupReturns.Size = New System.Drawing.Size(1061, 426)
    Me.Split_GroupReturns.SplitterDistance = 725
    Me.Split_GroupReturns.SplitterWidth = 3
    Me.Split_GroupReturns.TabIndex = 3
    '
    'Label_ChartStock
    '
    Me.Label_ChartStock.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Label_ChartStock.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_ChartStock.Location = New System.Drawing.Point(3, 0)
    Me.Label_ChartStock.Name = "Label_ChartStock"
    Me.Label_ChartStock.Size = New System.Drawing.Size(323, 15)
    Me.Label_ChartStock.TabIndex = 1
    '
    'Chart_MonthlyReturns
    '
    Me.Chart_MonthlyReturns.Anchor = CType((System.Windows.Forms.AnchorStyles.Left Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_MonthlyReturns.BackColor = System.Drawing.Color.Azure
    Me.Chart_MonthlyReturns.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_MonthlyReturns.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_MonthlyReturns.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_MonthlyReturns.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_MonthlyReturns.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea10.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea10.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea10.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea10.AxisX.LabelStyle.Format = "Y"
    ChartArea10.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea10.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea10.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea10.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea10.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea10.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea10.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea10.AxisY.LabelStyle.Format = "P0"
    ChartArea10.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea10.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea10.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea10.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea10.AxisY.StartFromZero = False
    ChartArea10.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea10.BackColor = System.Drawing.Color.Transparent
    ChartArea10.BorderColor = System.Drawing.Color.DimGray
    ChartArea10.Name = "Default"
    Me.Chart_MonthlyReturns.ChartAreas.Add(ChartArea10)
    Legend10.BackColor = System.Drawing.Color.Transparent
    Legend10.BorderColor = System.Drawing.Color.Transparent
    Legend10.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend10.DockToChartArea = "Default"
    Legend10.Enabled = False
    Legend10.Name = "Default"
    Me.Chart_MonthlyReturns.Legends.Add(Legend10)
    Me.Chart_MonthlyReturns.Location = New System.Drawing.Point(0, 213)
    Me.Chart_MonthlyReturns.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_MonthlyReturns.MinimumSize = New System.Drawing.Size(150, 150)
    Me.Chart_MonthlyReturns.Name = "Chart_MonthlyReturns"
    Me.Chart_MonthlyReturns.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series19.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series19.BorderWidth = 2
    Series19.ChartType = "Line"
    Series19.CustomAttributes = "LabelStyle=Top"
    Series19.Name = "Series1"
    Series19.ShadowOffset = 1
    Series19.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series19.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series20.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series20.BorderWidth = 2
    Series20.ChartType = "Line"
    Series20.CustomAttributes = "LabelStyle=Top"
    Series20.Name = "Series2"
    Series20.ShadowOffset = 1
    Series20.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series20.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_MonthlyReturns.Series.Add(Series19)
    Me.Chart_MonthlyReturns.Series.Add(Series20)
    Me.Chart_MonthlyReturns.Size = New System.Drawing.Size(327, 188)
    Me.Chart_MonthlyReturns.TabIndex = 2
    Me.Chart_MonthlyReturns.Text = "Chart2"
    '
    'Chart_VAMI
    '
    Me.Chart_VAMI.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_VAMI.BackColor = System.Drawing.Color.Azure
    Me.Chart_VAMI.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_VAMI.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_VAMI.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_VAMI.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_VAMI.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea11.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea11.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea11.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea11.AxisX.LabelStyle.Format = "Y"
    ChartArea11.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea11.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea11.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea11.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea11.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea11.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea11.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea11.AxisY.LabelStyle.Format = "N0"
    ChartArea11.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea11.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea11.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea11.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea11.AxisY.StartFromZero = False
    ChartArea11.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea11.BackColor = System.Drawing.Color.Transparent
    ChartArea11.BorderColor = System.Drawing.Color.DimGray
    ChartArea11.Name = "Default"
    Me.Chart_VAMI.ChartAreas.Add(ChartArea11)
    Legend11.BackColor = System.Drawing.Color.Transparent
    Legend11.BorderColor = System.Drawing.Color.Transparent
    Legend11.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend11.DockToChartArea = "Default"
    Legend11.Enabled = False
    Legend11.Name = "Default"
    Me.Chart_VAMI.Legends.Add(Legend11)
    Me.Chart_VAMI.Location = New System.Drawing.Point(1, 16)
    Me.Chart_VAMI.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_VAMI.MinimumSize = New System.Drawing.Size(150, 150)
    Me.Chart_VAMI.Name = "Chart_VAMI"
    Me.Chart_VAMI.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series21.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series21.BorderWidth = 2
    Series21.ChartType = "Line"
    Series21.CustomAttributes = "LabelStyle=Top"
    Series21.Name = "Series1"
    Series21.ShadowOffset = 1
    Series21.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series21.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series22.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series22.BorderWidth = 2
    Series22.ChartType = "Line"
    Series22.CustomAttributes = "LabelStyle=Top"
    Series22.Name = "Series2"
    Series22.ShadowOffset = 1
    Series22.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series22.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_VAMI.Series.Add(Series21)
    Me.Chart_VAMI.Series.Add(Series22)
    Me.Chart_VAMI.Size = New System.Drawing.Size(327, 174)
    Me.Chart_VAMI.TabIndex = 0
    Me.Chart_VAMI.Text = "Chart2"
    '
    'Btn_ShowCarts
    '
    Me.Btn_ShowCarts.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Btn_ShowCarts.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Btn_ShowCarts.Location = New System.Drawing.Point(733, 502)
    Me.Btn_ShowCarts.Name = "Btn_ShowCarts"
    Me.Btn_ShowCarts.Size = New System.Drawing.Size(113, 28)
    Me.Btn_ShowCarts.TabIndex = 2
    Me.Btn_ShowCarts.Text = "Hide Charts"
    '
    'TabControl1
    '
    Me.TabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.TabControl1.Controls.Add(Me.Tab_Instruments)
    Me.TabControl1.Controls.Add(Me.Tab_Marginals)
    Me.TabControl1.Controls.Add(Me.Tab_Results)
    Me.TabControl1.Controls.Add(Me.Tab_Covariance)
    Me.TabControl1.Controls.Add(Me.Tab_Constraints)
    Me.TabControl1.Controls.Add(Me.Tab_OptimisationIO)
    Me.TabControl1.Location = New System.Drawing.Point(2, 97)
    Me.TabControl1.Name = "TabControl1"
    Me.TabControl1.SelectedIndex = 0
    Me.TabControl1.Size = New System.Drawing.Size(1075, 562)
    Me.TabControl1.TabIndex = 8
    '
    'Tab_Instruments
    '
    Me.Tab_Instruments.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_Instruments.Controls.Add(Me.btn_ShowGroupChart)
    Me.Tab_Instruments.Controls.Add(Me.Split_GroupReturns)
    Me.Tab_Instruments.Controls.Add(Me.Btn_ShowCarts)
    Me.Tab_Instruments.Controls.Add(Me.Label_GroupStartDate)
    Me.Tab_Instruments.Controls.Add(Me.GroupBox_Actions)
    Me.Tab_Instruments.Controls.Add(Me.Label2)
    Me.Tab_Instruments.Controls.Add(Me.Label_GroupEndDate)
    Me.Tab_Instruments.Controls.Add(Me.Label4)
    Me.Tab_Instruments.Controls.Add(Me.btnSave)
    Me.Tab_Instruments.Controls.Add(Me.btnCancel)
    Me.Tab_Instruments.Controls.Add(Me.btnClose)
    Me.Tab_Instruments.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Instruments.Name = "Tab_Instruments"
    Me.Tab_Instruments.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Instruments.Size = New System.Drawing.Size(1067, 536)
    Me.Tab_Instruments.TabIndex = 0
    Me.Tab_Instruments.Text = "Instruments"
    '
    'btn_ShowGroupChart
    '
    Me.btn_ShowGroupChart.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btn_ShowGroupChart.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btn_ShowGroupChart.Location = New System.Drawing.Point(611, 502)
    Me.btn_ShowGroupChart.Name = "btn_ShowGroupChart"
    Me.btn_ShowGroupChart.Size = New System.Drawing.Size(113, 28)
    Me.btn_ShowGroupChart.TabIndex = 107
    Me.btn_ShowGroupChart.Text = "Show Weighted Group Chart"
    '
    'Tab_Marginals
    '
    Me.Tab_Marginals.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_Marginals.Controls.Add(Me.Grid_Marginals)
    Me.Tab_Marginals.Controls.Add(Me.GroupBox5)
    Me.Tab_Marginals.Controls.Add(Me.GroupBox4)
    Me.Tab_Marginals.Controls.Add(Me.GroupBox3)
    Me.Tab_Marginals.Controls.Add(Me.GroupBox2)
    Me.Tab_Marginals.Controls.Add(Me.GroupBox1)
    Me.Tab_Marginals.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Marginals.Name = "Tab_Marginals"
    Me.Tab_Marginals.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Marginals.Size = New System.Drawing.Size(1067, 536)
    Me.Tab_Marginals.TabIndex = 1
    Me.Tab_Marginals.Text = "Marginals"
    '
    'Grid_Marginals
    '
    Me.Grid_Marginals.AllowEditing = False
    Me.Grid_Marginals.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Marginals.ColumnInfo = "10,0,0,0,0,85,Columns:"
    Me.Grid_Marginals.Location = New System.Drawing.Point(3, 153)
    Me.Grid_Marginals.Name = "Grid_Marginals"
    Me.Grid_Marginals.Rows.DefaultSize = 17
    Me.Grid_Marginals.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.RowRange
    Me.Grid_Marginals.Size = New System.Drawing.Size(1059, 380)
    Me.Grid_Marginals.TabIndex = 0
    '
    'GroupBox5
    '
    Me.GroupBox5.Controls.Add(Me.Button_Marginals_TradeTop1)
    Me.GroupBox5.Controls.Add(Me.Button_Marginals_Trade)
    Me.GroupBox5.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.GroupBox5.Location = New System.Drawing.Point(915, 3)
    Me.GroupBox5.Name = "GroupBox5"
    Me.GroupBox5.Size = New System.Drawing.Size(146, 144)
    Me.GroupBox5.TabIndex = 5
    Me.GroupBox5.TabStop = False
    Me.GroupBox5.Text = "Calculate"
    '
    'Button_Marginals_TradeTop1
    '
    Me.Button_Marginals_TradeTop1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Button_Marginals_TradeTop1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_Marginals_TradeTop1.Location = New System.Drawing.Point(6, 66)
    Me.Button_Marginals_TradeTop1.Name = "Button_Marginals_TradeTop1"
    Me.Button_Marginals_TradeTop1.Size = New System.Drawing.Size(134, 39)
    Me.Button_Marginals_TradeTop1.TabIndex = 1
    Me.Button_Marginals_TradeTop1.Text = "Trade and ReCalculate (Top Trade)"
    '
    'Button_Marginals_Trade
    '
    Me.Button_Marginals_Trade.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Button_Marginals_Trade.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_Marginals_Trade.Location = New System.Drawing.Point(6, 20)
    Me.Button_Marginals_Trade.Name = "Button_Marginals_Trade"
    Me.Button_Marginals_Trade.Size = New System.Drawing.Size(134, 39)
    Me.Button_Marginals_Trade.TabIndex = 0
    Me.Button_Marginals_Trade.Text = "Trade and ReCalculate (Selected)"
    '
    'GroupBox4
    '
    Me.GroupBox4.Controls.Add(Me.Check_Marginals_ExcludeCustomLimitBreaks)
    Me.GroupBox4.Controls.Add(Me.Check_Marginals_ExcludeLiquidityBreaks)
    Me.GroupBox4.Controls.Add(Me.Check_Marginals_ExcludeSectorBreaks)
    Me.GroupBox4.Controls.Add(Me.Check_Marginals_ExcludeInstrumentBreaks)
    Me.GroupBox4.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.GroupBox4.Location = New System.Drawing.Point(766, 3)
    Me.GroupBox4.Name = "GroupBox4"
    Me.GroupBox4.Size = New System.Drawing.Size(143, 144)
    Me.GroupBox4.TabIndex = 4
    Me.GroupBox4.TabStop = False
    Me.GroupBox4.Text = "Exclude"
    '
    'Check_Marginals_ExcludeCustomLimitBreaks
    '
    Me.Check_Marginals_ExcludeCustomLimitBreaks.AutoSize = True
    Me.Check_Marginals_ExcludeCustomLimitBreaks.Location = New System.Drawing.Point(6, 89)
    Me.Check_Marginals_ExcludeCustomLimitBreaks.Name = "Check_Marginals_ExcludeCustomLimitBreaks"
    Me.Check_Marginals_ExcludeCustomLimitBreaks.Size = New System.Drawing.Size(121, 17)
    Me.Check_Marginals_ExcludeCustomLimitBreaks.TabIndex = 3
    Me.Check_Marginals_ExcludeCustomLimitBreaks.Text = "Custom Limit Breaks"
    Me.Check_Marginals_ExcludeCustomLimitBreaks.UseVisualStyleBackColor = True
    '
    'Check_Marginals_ExcludeLiquidityBreaks
    '
    Me.Check_Marginals_ExcludeLiquidityBreaks.AutoSize = True
    Me.Check_Marginals_ExcludeLiquidityBreaks.Location = New System.Drawing.Point(6, 66)
    Me.Check_Marginals_ExcludeLiquidityBreaks.Name = "Check_Marginals_ExcludeLiquidityBreaks"
    Me.Check_Marginals_ExcludeLiquidityBreaks.Size = New System.Drawing.Size(100, 17)
    Me.Check_Marginals_ExcludeLiquidityBreaks.TabIndex = 2
    Me.Check_Marginals_ExcludeLiquidityBreaks.Text = "Liquidity Breaks"
    Me.Check_Marginals_ExcludeLiquidityBreaks.UseVisualStyleBackColor = True
    '
    'Check_Marginals_ExcludeSectorBreaks
    '
    Me.Check_Marginals_ExcludeSectorBreaks.AutoSize = True
    Me.Check_Marginals_ExcludeSectorBreaks.Location = New System.Drawing.Point(6, 43)
    Me.Check_Marginals_ExcludeSectorBreaks.Name = "Check_Marginals_ExcludeSectorBreaks"
    Me.Check_Marginals_ExcludeSectorBreaks.Size = New System.Drawing.Size(93, 17)
    Me.Check_Marginals_ExcludeSectorBreaks.TabIndex = 1
    Me.Check_Marginals_ExcludeSectorBreaks.Text = "Sector Breaks"
    Me.Check_Marginals_ExcludeSectorBreaks.UseVisualStyleBackColor = True
    '
    'Check_Marginals_ExcludeInstrumentBreaks
    '
    Me.Check_Marginals_ExcludeInstrumentBreaks.AutoSize = True
    Me.Check_Marginals_ExcludeInstrumentBreaks.Location = New System.Drawing.Point(6, 20)
    Me.Check_Marginals_ExcludeInstrumentBreaks.Name = "Check_Marginals_ExcludeInstrumentBreaks"
    Me.Check_Marginals_ExcludeInstrumentBreaks.Size = New System.Drawing.Size(111, 17)
    Me.Check_Marginals_ExcludeInstrumentBreaks.TabIndex = 0
    Me.Check_Marginals_ExcludeInstrumentBreaks.Text = "Instrument Breaks"
    Me.Check_Marginals_ExcludeInstrumentBreaks.UseVisualStyleBackColor = True
    '
    'GroupBox3
    '
    Me.GroupBox3.Controls.Add(Me.Check_Marginals_ApplyBounds)
    Me.GroupBox3.Controls.Add(Me.Edit_Marginals_RiskFreeRate)
    Me.GroupBox3.Controls.Add(Me.Edit_Marginals_ReturnTarget)
    Me.GroupBox3.Controls.Add(Me.Label17)
    Me.GroupBox3.Controls.Add(Me.Label_MarginalsReturnTarget)
    Me.GroupBox3.Controls.Add(Me.Edit_Marginals_RiskLimit)
    Me.GroupBox3.Controls.Add(Me.Label_MarginalsRiskLimit)
    Me.GroupBox3.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.GroupBox3.Location = New System.Drawing.Point(589, 3)
    Me.GroupBox3.Name = "GroupBox3"
    Me.GroupBox3.Size = New System.Drawing.Size(171, 144)
    Me.GroupBox3.TabIndex = 3
    Me.GroupBox3.TabStop = False
    Me.GroupBox3.Text = "Subject To"
    '
    'Check_Marginals_ApplyBounds
    '
    Me.Check_Marginals_ApplyBounds.Location = New System.Drawing.Point(4, 87)
    Me.Check_Marginals_ApplyBounds.Name = "Check_Marginals_ApplyBounds"
    Me.Check_Marginals_ApplyBounds.Size = New System.Drawing.Size(163, 34)
    Me.Check_Marginals_ApplyBounds.TabIndex = 0
    Me.Check_Marginals_ApplyBounds.Text = "Apply Upper / Lower Bounds"
    Me.Check_Marginals_ApplyBounds.UseVisualStyleBackColor = True
    '
    'Edit_Marginals_RiskFreeRate
    '
    Me.Edit_Marginals_RiskFreeRate.Location = New System.Drawing.Point(107, 64)
    Me.Edit_Marginals_RiskFreeRate.Name = "Edit_Marginals_RiskFreeRate"
    Me.Edit_Marginals_RiskFreeRate.RenaissanceTag = Nothing
    Me.Edit_Marginals_RiskFreeRate.Size = New System.Drawing.Size(53, 20)
    Me.Edit_Marginals_RiskFreeRate.TabIndex = 5
    Me.Edit_Marginals_RiskFreeRate.Text = "0.00%"
    Me.Edit_Marginals_RiskFreeRate.TextFormat = "#,##0.00%"
    Me.Edit_Marginals_RiskFreeRate.Value = 0
    '
    'Edit_Marginals_ReturnTarget
    '
    Me.Edit_Marginals_ReturnTarget.Location = New System.Drawing.Point(107, 41)
    Me.Edit_Marginals_ReturnTarget.Name = "Edit_Marginals_ReturnTarget"
    Me.Edit_Marginals_ReturnTarget.RenaissanceTag = Nothing
    Me.Edit_Marginals_ReturnTarget.Size = New System.Drawing.Size(53, 20)
    Me.Edit_Marginals_ReturnTarget.TabIndex = 3
    Me.Edit_Marginals_ReturnTarget.Text = "0.00%"
    Me.Edit_Marginals_ReturnTarget.TextFormat = "#,##0.00%"
    Me.Edit_Marginals_ReturnTarget.Value = 0
    '
    'Label17
    '
    Me.Label17.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label17.Location = New System.Drawing.Point(5, 67)
    Me.Label17.Name = "Label17"
    Me.Label17.Size = New System.Drawing.Size(92, 15)
    Me.Label17.TabIndex = 4
    Me.Label17.Text = "Risk Free Rate"
    '
    'Label_MarginalsReturnTarget
    '
    Me.Label_MarginalsReturnTarget.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_MarginalsReturnTarget.Location = New System.Drawing.Point(5, 44)
    Me.Label_MarginalsReturnTarget.Name = "Label_MarginalsReturnTarget"
    Me.Label_MarginalsReturnTarget.Size = New System.Drawing.Size(97, 15)
    Me.Label_MarginalsReturnTarget.TabIndex = 2
    Me.Label_MarginalsReturnTarget.Text = "Return Target (>=)"
    '
    'Edit_Marginals_RiskLimit
    '
    Me.Edit_Marginals_RiskLimit.Location = New System.Drawing.Point(107, 18)
    Me.Edit_Marginals_RiskLimit.Name = "Edit_Marginals_RiskLimit"
    Me.Edit_Marginals_RiskLimit.RenaissanceTag = Nothing
    Me.Edit_Marginals_RiskLimit.Size = New System.Drawing.Size(53, 20)
    Me.Edit_Marginals_RiskLimit.TabIndex = 1
    Me.Edit_Marginals_RiskLimit.Text = "100.00%"
    Me.Edit_Marginals_RiskLimit.TextFormat = "#,##0.00%"
    Me.Edit_Marginals_RiskLimit.Value = 1
    '
    'Label_MarginalsRiskLimit
    '
    Me.Label_MarginalsRiskLimit.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_MarginalsRiskLimit.Location = New System.Drawing.Point(5, 21)
    Me.Label_MarginalsRiskLimit.Name = "Label_MarginalsRiskLimit"
    Me.Label_MarginalsRiskLimit.Size = New System.Drawing.Size(80, 15)
    Me.Label_MarginalsRiskLimit.TabIndex = 0
    Me.Label_MarginalsRiskLimit.Text = "Risk Limit (<=)"
    '
    'GroupBox2
    '
    Me.GroupBox2.Controls.Add(Me.Check_Marginals_AnalysisPeriod)
    Me.GroupBox2.Controls.Add(Me.Edit_Marginals_AnalysisPeriod)
    Me.GroupBox2.Controls.Add(Me.Combo_MarginalIndex)
    Me.GroupBox2.Controls.Add(Me.Label39)
    Me.GroupBox2.Controls.Add(Me.Radio_ImproveStdError)
    Me.GroupBox2.Controls.Add(Me.Radio_ImproveLeastSquares)
    Me.GroupBox2.Controls.Add(Me.Radio_ImproveWorstDrawDown)
    Me.GroupBox2.Controls.Add(Me.Radio_ImproveTargetRiskReturn)
    Me.GroupBox2.Controls.Add(Me.Radio_ImproveSharpeRatio)
    Me.GroupBox2.Controls.Add(Me.Radio_ImproveRisk)
    Me.GroupBox2.Controls.Add(Me.Radio_ImproveReturn)
    Me.GroupBox2.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.GroupBox2.Location = New System.Drawing.Point(293, 3)
    Me.GroupBox2.Name = "GroupBox2"
    Me.GroupBox2.Size = New System.Drawing.Size(290, 144)
    Me.GroupBox2.TabIndex = 2
    Me.GroupBox2.TabStop = False
    Me.GroupBox2.Text = "Improve"
    '
    'Check_Marginals_AnalysisPeriod
    '
    Me.Check_Marginals_AnalysisPeriod.AutoSize = True
    Me.Check_Marginals_AnalysisPeriod.Location = New System.Drawing.Point(42, 107)
    Me.Check_Marginals_AnalysisPeriod.Name = "Check_Marginals_AnalysisPeriod"
    Me.Check_Marginals_AnalysisPeriod.Size = New System.Drawing.Size(140, 17)
    Me.Check_Marginals_AnalysisPeriod.TabIndex = 9
    Me.Check_Marginals_AnalysisPeriod.Text = "Analysis period (Months)"
    Me.Check_Marginals_AnalysisPeriod.UseVisualStyleBackColor = True
    '
    'Edit_Marginals_AnalysisPeriod
    '
    Me.Edit_Marginals_AnalysisPeriod.Enabled = False
    Me.Edit_Marginals_AnalysisPeriod.Location = New System.Drawing.Point(188, 105)
    Me.Edit_Marginals_AnalysisPeriod.Name = "Edit_Marginals_AnalysisPeriod"
    Me.Edit_Marginals_AnalysisPeriod.RenaissanceTag = Nothing
    Me.Edit_Marginals_AnalysisPeriod.Size = New System.Drawing.Size(67, 20)
    Me.Edit_Marginals_AnalysisPeriod.TabIndex = 10
    Me.Edit_Marginals_AnalysisPeriod.Text = "60"
    Me.Edit_Marginals_AnalysisPeriod.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.Edit_Marginals_AnalysisPeriod.TextFormat = "#,##0"
    Me.Edit_Marginals_AnalysisPeriod.Value = 60
    '
    'Combo_MarginalIndex
    '
    Me.Combo_MarginalIndex.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_MarginalIndex.Location = New System.Drawing.Point(43, 81)
    Me.Combo_MarginalIndex.MasternameCollection = Nothing
    Me.Combo_MarginalIndex.Name = "Combo_MarginalIndex"
    '
    '
    '
    Me.Combo_MarginalIndex.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
    Me.Combo_MarginalIndex.SelectNoMatch = False
    Me.Combo_MarginalIndex.Size = New System.Drawing.Size(244, 20)
    Me.Combo_MarginalIndex.TabIndex = 8
    Me.Combo_MarginalIndex.ThemeName = "ControlDefault"
    '
    'Label39
    '
    Me.Label39.AutoSize = True
    Me.Label39.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label39.Location = New System.Drawing.Point(21, 83)
    Me.Label39.Name = "Label39"
    Me.Label39.Size = New System.Drawing.Size(18, 13)
    Me.Label39.TabIndex = 7
    Me.Label39.Text = "vs"
    '
    'Radio_ImproveStdError
    '
    Me.Radio_ImproveStdError.AutoSize = True
    Me.Radio_ImproveStdError.Location = New System.Drawing.Point(134, 61)
    Me.Radio_ImproveStdError.Name = "Radio_ImproveStdError"
    Me.Radio_ImproveStdError.Size = New System.Drawing.Size(93, 17)
    Me.Radio_ImproveStdError.TabIndex = 6
    Me.Radio_ImproveStdError.TabStop = True
    Me.Radio_ImproveStdError.Text = "Standard Error"
    Me.Radio_ImproveStdError.UseVisualStyleBackColor = True
    '
    'Radio_ImproveLeastSquares
    '
    Me.Radio_ImproveLeastSquares.AutoSize = True
    Me.Radio_ImproveLeastSquares.Location = New System.Drawing.Point(7, 61)
    Me.Radio_ImproveLeastSquares.Name = "Radio_ImproveLeastSquares"
    Me.Radio_ImproveLeastSquares.Size = New System.Drawing.Size(93, 17)
    Me.Radio_ImproveLeastSquares.TabIndex = 5
    Me.Radio_ImproveLeastSquares.TabStop = True
    Me.Radio_ImproveLeastSquares.Text = "Least Squares"
    Me.Radio_ImproveLeastSquares.UseVisualStyleBackColor = True
    '
    'Radio_ImproveWorstDrawDown
    '
    Me.Radio_ImproveWorstDrawDown.AutoSize = True
    Me.Radio_ImproveWorstDrawDown.Location = New System.Drawing.Point(177, 40)
    Me.Radio_ImproveWorstDrawDown.Name = "Radio_ImproveWorstDrawDown"
    Me.Radio_ImproveWorstDrawDown.Size = New System.Drawing.Size(107, 17)
    Me.Radio_ImproveWorstDrawDown.TabIndex = 4
    Me.Radio_ImproveWorstDrawDown.TabStop = True
    Me.Radio_ImproveWorstDrawDown.Text = "Worst Drawdown"
    Me.Radio_ImproveWorstDrawDown.UseVisualStyleBackColor = True
    '
    'Radio_ImproveTargetRiskReturn
    '
    Me.Radio_ImproveTargetRiskReturn.AutoSize = True
    Me.Radio_ImproveTargetRiskReturn.Location = New System.Drawing.Point(7, 40)
    Me.Radio_ImproveTargetRiskReturn.Name = "Radio_ImproveTargetRiskReturn"
    Me.Radio_ImproveTargetRiskReturn.Size = New System.Drawing.Size(123, 17)
    Me.Radio_ImproveTargetRiskReturn.TabIndex = 3
    Me.Radio_ImproveTargetRiskReturn.TabStop = True
    Me.Radio_ImproveTargetRiskReturn.Text = "Target Risk / Return"
    Me.Radio_ImproveTargetRiskReturn.UseVisualStyleBackColor = True
    '
    'Radio_ImproveSharpeRatio
    '
    Me.Radio_ImproveSharpeRatio.AutoSize = True
    Me.Radio_ImproveSharpeRatio.Enabled = False
    Me.Radio_ImproveSharpeRatio.Location = New System.Drawing.Point(177, 19)
    Me.Radio_ImproveSharpeRatio.Name = "Radio_ImproveSharpeRatio"
    Me.Radio_ImproveSharpeRatio.Size = New System.Drawing.Size(87, 17)
    Me.Radio_ImproveSharpeRatio.TabIndex = 2
    Me.Radio_ImproveSharpeRatio.TabStop = True
    Me.Radio_ImproveSharpeRatio.Text = "Sharpe Ratio"
    Me.Radio_ImproveSharpeRatio.UseVisualStyleBackColor = True
    '
    'Radio_ImproveRisk
    '
    Me.Radio_ImproveRisk.AutoSize = True
    Me.Radio_ImproveRisk.Location = New System.Drawing.Point(103, 19)
    Me.Radio_ImproveRisk.Name = "Radio_ImproveRisk"
    Me.Radio_ImproveRisk.Size = New System.Drawing.Size(46, 17)
    Me.Radio_ImproveRisk.TabIndex = 1
    Me.Radio_ImproveRisk.TabStop = True
    Me.Radio_ImproveRisk.Text = "Risk"
    Me.Radio_ImproveRisk.UseVisualStyleBackColor = True
    '
    'Radio_ImproveReturn
    '
    Me.Radio_ImproveReturn.AutoSize = True
    Me.Radio_ImproveReturn.Location = New System.Drawing.Point(7, 19)
    Me.Radio_ImproveReturn.Name = "Radio_ImproveReturn"
    Me.Radio_ImproveReturn.Size = New System.Drawing.Size(57, 17)
    Me.Radio_ImproveReturn.TabIndex = 0
    Me.Radio_ImproveReturn.TabStop = True
    Me.Radio_ImproveReturn.Text = "Return"
    Me.Radio_ImproveReturn.UseVisualStyleBackColor = True
    '
    'GroupBox1
    '
    Me.GroupBox1.Controls.Add(Me.Combo_Marginals_ContraInstrument)
    Me.GroupBox1.Controls.Add(Me.Check_Marginals_SubstituteTradesOnly)
    Me.GroupBox1.Controls.Add(Me.Check_Marginals_ContraTrade)
    Me.GroupBox1.Controls.Add(Me.GroupBox6)
    Me.GroupBox1.Controls.Add(Me.Edit_Marginals_TradeSize)
    Me.GroupBox1.Controls.Add(Me.Label10)
    Me.GroupBox1.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(285, 144)
    Me.GroupBox1.TabIndex = 1
    Me.GroupBox1.TabStop = False
    Me.GroupBox1.Text = "Trade Details"
    '
    'Combo_Marginals_ContraInstrument
    '
    Me.Combo_Marginals_ContraInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_Marginals_ContraInstrument.Enabled = False
    Me.Combo_Marginals_ContraInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_Marginals_ContraInstrument.Location = New System.Drawing.Point(103, 94)
    Me.Combo_Marginals_ContraInstrument.Name = "Combo_Marginals_ContraInstrument"
    Me.Combo_Marginals_ContraInstrument.Size = New System.Drawing.Size(179, 21)
    Me.Combo_Marginals_ContraInstrument.TabIndex = 4
    '
    'Check_Marginals_SubstituteTradesOnly
    '
    Me.Check_Marginals_SubstituteTradesOnly.AutoSize = True
    Me.Check_Marginals_SubstituteTradesOnly.Location = New System.Drawing.Point(9, 119)
    Me.Check_Marginals_SubstituteTradesOnly.Name = "Check_Marginals_SubstituteTradesOnly"
    Me.Check_Marginals_SubstituteTradesOnly.Size = New System.Drawing.Size(169, 17)
    Me.Check_Marginals_SubstituteTradesOnly.TabIndex = 5
    Me.Check_Marginals_SubstituteTradesOnly.Text = "Look for Substitute trades only"
    Me.Check_Marginals_SubstituteTradesOnly.UseVisualStyleBackColor = True
    '
    'Check_Marginals_ContraTrade
    '
    Me.Check_Marginals_ContraTrade.AutoSize = True
    Me.Check_Marginals_ContraTrade.Location = New System.Drawing.Point(9, 96)
    Me.Check_Marginals_ContraTrade.Name = "Check_Marginals_ContraTrade"
    Me.Check_Marginals_ContraTrade.Size = New System.Drawing.Size(88, 17)
    Me.Check_Marginals_ContraTrade.TabIndex = 3
    Me.Check_Marginals_ContraTrade.Text = "Contra Trade"
    Me.Check_Marginals_ContraTrade.UseVisualStyleBackColor = True
    '
    'GroupBox6
    '
    Me.GroupBox6.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox6.Controls.Add(Me.Check_Marginals_Buy)
    Me.GroupBox6.Controls.Add(Me.Check_Marginals_Sell)
    Me.GroupBox6.Controls.Add(Me.Check_Marginals_SellAll)
    Me.GroupBox6.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.GroupBox6.Location = New System.Drawing.Point(3, 40)
    Me.GroupBox6.Name = "GroupBox6"
    Me.GroupBox6.Size = New System.Drawing.Size(279, 50)
    Me.GroupBox6.TabIndex = 2
    Me.GroupBox6.TabStop = False
    '
    'Check_Marginals_Buy
    '
    Me.Check_Marginals_Buy.AutoSize = True
    Me.Check_Marginals_Buy.Location = New System.Drawing.Point(6, 10)
    Me.Check_Marginals_Buy.Name = "Check_Marginals_Buy"
    Me.Check_Marginals_Buy.Size = New System.Drawing.Size(76, 17)
    Me.Check_Marginals_Buy.TabIndex = 0
    Me.Check_Marginals_Buy.Text = "Allow BUY"
    Me.Check_Marginals_Buy.UseVisualStyleBackColor = True
    '
    'Check_Marginals_Sell
    '
    Me.Check_Marginals_Sell.AutoSize = True
    Me.Check_Marginals_Sell.Location = New System.Drawing.Point(103, 10)
    Me.Check_Marginals_Sell.Name = "Check_Marginals_Sell"
    Me.Check_Marginals_Sell.Size = New System.Drawing.Size(80, 17)
    Me.Check_Marginals_Sell.TabIndex = 1
    Me.Check_Marginals_Sell.Text = "Allow SELL"
    Me.Check_Marginals_Sell.UseVisualStyleBackColor = True
    '
    'Check_Marginals_SellAll
    '
    Me.Check_Marginals_SellAll.AutoSize = True
    Me.Check_Marginals_SellAll.Location = New System.Drawing.Point(6, 31)
    Me.Check_Marginals_SellAll.Name = "Check_Marginals_SellAll"
    Me.Check_Marginals_SellAll.Size = New System.Drawing.Size(125, 17)
    Me.Check_Marginals_SellAll.TabIndex = 2
    Me.Check_Marginals_SellAll.Text = "SELL Whole Holding"
    Me.Check_Marginals_SellAll.UseVisualStyleBackColor = True
    '
    'Edit_Marginals_TradeSize
    '
    Me.Edit_Marginals_TradeSize.Location = New System.Drawing.Point(88, 18)
    Me.Edit_Marginals_TradeSize.Name = "Edit_Marginals_TradeSize"
    Me.Edit_Marginals_TradeSize.RenaissanceTag = Nothing
    Me.Edit_Marginals_TradeSize.Size = New System.Drawing.Size(138, 20)
    Me.Edit_Marginals_TradeSize.TabIndex = 1
    Me.Edit_Marginals_TradeSize.Text = "0.00"
    Me.Edit_Marginals_TradeSize.TextFormat = "#,##0.00"
    Me.Edit_Marginals_TradeSize.Value = 0
    '
    'Label10
    '
    Me.Label10.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label10.Location = New System.Drawing.Point(6, 21)
    Me.Label10.Name = "Label10"
    Me.Label10.Size = New System.Drawing.Size(76, 15)
    Me.Label10.TabIndex = 0
    Me.Label10.Text = "Trade Size"
    '
    'Tab_Results
    '
    Me.Tab_Results.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_Results.Controls.Add(Me.GroupBox8)
    Me.Tab_Results.Controls.Add(Me.GroupBox7)
    Me.Tab_Results.Controls.Add(Me.Results_Btn_PlotRiskReturn)
    Me.Tab_Results.Controls.Add(Me.Results_Btn_ClearResults)
    Me.Tab_Results.Controls.Add(Me.Chart_Results)
    Me.Tab_Results.ForeColor = System.Drawing.Color.Transparent
    Me.Tab_Results.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Results.Name = "Tab_Results"
    Me.Tab_Results.Size = New System.Drawing.Size(1067, 536)
    Me.Tab_Results.TabIndex = 2
    Me.Tab_Results.Text = "Results"
    '
    'GroupBox8
    '
    Me.GroupBox8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox8.Controls.Add(Me.Panel_PortfolioValues)
    Me.GroupBox8.Controls.Add(Me.Results_Check_ShowVsPortfolio)
    Me.GroupBox8.Controls.Add(Me.Label35)
    Me.GroupBox8.Controls.Add(Me.Results_Text_ConfidencePeriods)
    Me.GroupBox8.Controls.Add(Me.Label34)
    Me.GroupBox8.Controls.Add(Me.Results_Radio_VAR)
    Me.GroupBox8.Controls.Add(Me.Results_Radio_Volatility)
    Me.GroupBox8.Controls.Add(Me.Label12)
    Me.GroupBox8.Controls.Add(Me.Results_Text_ConfidenceLevel)
    Me.GroupBox8.Controls.Add(Me.Label15)
    Me.GroupBox8.Location = New System.Drawing.Point(822, 180)
    Me.GroupBox8.Name = "GroupBox8"
    Me.GroupBox8.Size = New System.Drawing.Size(237, 180)
    Me.GroupBox8.TabIndex = 4
    Me.GroupBox8.TabStop = False
    Me.GroupBox8.Text = "Chart Axis"
    '
    'Panel_PortfolioValues
    '
    Me.Panel_PortfolioValues.Controls.Add(Me.Label36)
    Me.Panel_PortfolioValues.Controls.Add(Me.Results_Text_PortfolioValue)
    Me.Panel_PortfolioValues.Controls.Add(Me.Results_Radio_vsPortfolioValue)
    Me.Panel_PortfolioValues.Controls.Add(Me.Label37)
    Me.Panel_PortfolioValues.Controls.Add(Me.Results_Radio_vsNewHolding)
    Me.Panel_PortfolioValues.Enabled = False
    Me.Panel_PortfolioValues.Location = New System.Drawing.Point(30, 117)
    Me.Panel_PortfolioValues.Name = "Panel_PortfolioValues"
    Me.Panel_PortfolioValues.Size = New System.Drawing.Size(204, 52)
    Me.Panel_PortfolioValues.TabIndex = 130
    '
    'Label36
    '
    Me.Label36.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label36.ForeColor = System.Drawing.Color.Black
    Me.Label36.Location = New System.Drawing.Point(24, 5)
    Me.Label36.Name = "Label36"
    Me.Label36.Size = New System.Drawing.Size(131, 13)
    Me.Label36.TabIndex = 131
    Me.Label36.Text = "vs 'New Holding' value"
    '
    'Results_Text_PortfolioValue
    '
    Me.Results_Text_PortfolioValue.Location = New System.Drawing.Point(110, 28)
    Me.Results_Text_PortfolioValue.Name = "Results_Text_PortfolioValue"
    Me.Results_Text_PortfolioValue.RenaissanceTag = Nothing
    Me.Results_Text_PortfolioValue.Size = New System.Drawing.Size(91, 20)
    Me.Results_Text_PortfolioValue.TabIndex = 127
    Me.Results_Text_PortfolioValue.Text = "1,000,000"
    Me.Results_Text_PortfolioValue.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.Results_Text_PortfolioValue.TextFormat = "#,##0"
    Me.Results_Text_PortfolioValue.Value = 1000000
    '
    'Results_Radio_vsPortfolioValue
    '
    Me.Results_Radio_vsPortfolioValue.AutoSize = True
    Me.Results_Radio_vsPortfolioValue.Location = New System.Drawing.Point(4, 31)
    Me.Results_Radio_vsPortfolioValue.Name = "Results_Radio_vsPortfolioValue"
    Me.Results_Radio_vsPortfolioValue.Size = New System.Drawing.Size(14, 13)
    Me.Results_Radio_vsPortfolioValue.TabIndex = 130
    Me.Results_Radio_vsPortfolioValue.TabStop = True
    Me.Results_Radio_vsPortfolioValue.UseVisualStyleBackColor = True
    '
    'Label37
    '
    Me.Label37.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label37.ForeColor = System.Drawing.Color.Black
    Me.Label37.Location = New System.Drawing.Point(24, 31)
    Me.Label37.Name = "Label37"
    Me.Label37.Size = New System.Drawing.Size(83, 13)
    Me.Label37.TabIndex = 128
    Me.Label37.Text = "Portfolio Value :"
    '
    'Results_Radio_vsNewHolding
    '
    Me.Results_Radio_vsNewHolding.AutoSize = True
    Me.Results_Radio_vsNewHolding.Location = New System.Drawing.Point(4, 5)
    Me.Results_Radio_vsNewHolding.Name = "Results_Radio_vsNewHolding"
    Me.Results_Radio_vsNewHolding.Size = New System.Drawing.Size(14, 13)
    Me.Results_Radio_vsNewHolding.TabIndex = 129
    Me.Results_Radio_vsNewHolding.TabStop = True
    Me.Results_Radio_vsNewHolding.UseVisualStyleBackColor = True
    '
    'Results_Check_ShowVsPortfolio
    '
    Me.Results_Check_ShowVsPortfolio.AutoSize = True
    Me.Results_Check_ShowVsPortfolio.Enabled = False
    Me.Results_Check_ShowVsPortfolio.ForeColor = System.Drawing.Color.Black
    Me.Results_Check_ShowVsPortfolio.Location = New System.Drawing.Point(6, 97)
    Me.Results_Check_ShowVsPortfolio.Name = "Results_Check_ShowVsPortfolio"
    Me.Results_Check_ShowVsPortfolio.Size = New System.Drawing.Size(179, 17)
    Me.Results_Check_ShowVsPortfolio.TabIndex = 126
    Me.Results_Check_ShowVsPortfolio.Text = "Show as Cash vs Portfolio Value"
    Me.Results_Check_ShowVsPortfolio.UseVisualStyleBackColor = True
    '
    'Label35
    '
    Me.Label35.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label35.ForeColor = System.Drawing.Color.Black
    Me.Label35.Location = New System.Drawing.Point(63, 71)
    Me.Label35.Name = "Label35"
    Me.Label35.Size = New System.Drawing.Size(74, 13)
    Me.Label35.TabIndex = 123
    Me.Label35.Text = "Time Periods :"
    Me.Label35.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Results_Text_ConfidencePeriods
    '
    Me.Results_Text_ConfidencePeriods.Location = New System.Drawing.Point(140, 68)
    Me.Results_Text_ConfidencePeriods.Name = "Results_Text_ConfidencePeriods"
    Me.Results_Text_ConfidencePeriods.RenaissanceTag = Nothing
    Me.Results_Text_ConfidencePeriods.Size = New System.Drawing.Size(91, 20)
    Me.Results_Text_ConfidencePeriods.TabIndex = 122
    Me.Results_Text_ConfidencePeriods.Text = "1"
    Me.Results_Text_ConfidencePeriods.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.Results_Text_ConfidencePeriods.TextFormat = "#,##0"
    Me.Results_Text_ConfidencePeriods.Value = 1
    '
    'Label34
    '
    Me.Label34.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label34.ForeColor = System.Drawing.Color.Black
    Me.Label34.Location = New System.Drawing.Point(63, 45)
    Me.Label34.Name = "Label34"
    Me.Label34.Size = New System.Drawing.Size(74, 13)
    Me.Label34.TabIndex = 121
    Me.Label34.Text = "Confidence :"
    Me.Label34.TextAlign = System.Drawing.ContentAlignment.TopRight
    '
    'Results_Radio_VAR
    '
    Me.Results_Radio_VAR.AutoSize = True
    Me.Results_Radio_VAR.Location = New System.Drawing.Point(5, 45)
    Me.Results_Radio_VAR.Name = "Results_Radio_VAR"
    Me.Results_Radio_VAR.Size = New System.Drawing.Size(14, 13)
    Me.Results_Radio_VAR.TabIndex = 120
    Me.Results_Radio_VAR.TabStop = True
    Me.Results_Radio_VAR.UseVisualStyleBackColor = True
    '
    'Results_Radio_Volatility
    '
    Me.Results_Radio_Volatility.AutoSize = True
    Me.Results_Radio_Volatility.Location = New System.Drawing.Point(5, 19)
    Me.Results_Radio_Volatility.Name = "Results_Radio_Volatility"
    Me.Results_Radio_Volatility.Size = New System.Drawing.Size(14, 13)
    Me.Results_Radio_Volatility.TabIndex = 119
    Me.Results_Radio_Volatility.TabStop = True
    Me.Results_Radio_Volatility.UseVisualStyleBackColor = True
    '
    'Label12
    '
    Me.Label12.AutoSize = True
    Me.Label12.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label12.ForeColor = System.Drawing.Color.Black
    Me.Label12.Location = New System.Drawing.Point(25, 45)
    Me.Label12.Name = "Label12"
    Me.Label12.Size = New System.Drawing.Size(29, 13)
    Me.Label12.TabIndex = 118
    Me.Label12.Text = "VAR"
    '
    'Results_Text_ConfidenceLevel
    '
    Me.Results_Text_ConfidenceLevel.Location = New System.Drawing.Point(140, 42)
    Me.Results_Text_ConfidenceLevel.Name = "Results_Text_ConfidenceLevel"
    Me.Results_Text_ConfidenceLevel.RenaissanceTag = Nothing
    Me.Results_Text_ConfidenceLevel.Size = New System.Drawing.Size(91, 20)
    Me.Results_Text_ConfidenceLevel.TabIndex = 116
    Me.Results_Text_ConfidenceLevel.Text = "95.00%"
    Me.Results_Text_ConfidenceLevel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.Results_Text_ConfidenceLevel.TextFormat = "#,##0.00%"
    Me.Results_Text_ConfidenceLevel.Value = 0.95
    '
    'Label15
    '
    Me.Label15.AutoSize = True
    Me.Label15.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label15.ForeColor = System.Drawing.Color.Black
    Me.Label15.Location = New System.Drawing.Point(25, 19)
    Me.Label15.Name = "Label15"
    Me.Label15.Size = New System.Drawing.Size(45, 13)
    Me.Label15.TabIndex = 117
    Me.Label15.Text = "Volatility"
    '
    'GroupBox7
    '
    Me.GroupBox7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox7.Controls.Add(Me.Results_Radio_TargetReturn)
    Me.GroupBox7.Controls.Add(Me.Results_Radio_TargetVolatility)
    Me.GroupBox7.Controls.Add(Me.Label32)
    Me.GroupBox7.Controls.Add(Me.Results_Text_TargetReturn)
    Me.GroupBox7.Controls.Add(Me.Results_Btn_RoundTrades)
    Me.GroupBox7.Controls.Add(Me.Results_Btn_CaptureNewHoldings)
    Me.GroupBox7.Controls.Add(Me.Label31)
    Me.GroupBox7.Controls.Add(Me.Results_Text_MinTradeSize)
    Me.GroupBox7.Controls.Add(Me.Label29)
    Me.GroupBox7.Controls.Add(Me.Results_Text_TargetHolding)
    Me.GroupBox7.Controls.Add(Me.Label30)
    Me.GroupBox7.Controls.Add(Me.Results_Text_TargetVolatility)
    Me.GroupBox7.Location = New System.Drawing.Point(822, 3)
    Me.GroupBox7.Name = "GroupBox7"
    Me.GroupBox7.Size = New System.Drawing.Size(237, 171)
    Me.GroupBox7.TabIndex = 1
    Me.GroupBox7.TabStop = False
    Me.GroupBox7.Text = "Select Portfolio"
    '
    'Results_Radio_TargetReturn
    '
    Me.Results_Radio_TargetReturn.AutoSize = True
    Me.Results_Radio_TargetReturn.Location = New System.Drawing.Point(5, 46)
    Me.Results_Radio_TargetReturn.Name = "Results_Radio_TargetReturn"
    Me.Results_Radio_TargetReturn.Size = New System.Drawing.Size(14, 13)
    Me.Results_Radio_TargetReturn.TabIndex = 115
    Me.Results_Radio_TargetReturn.TabStop = True
    Me.Results_Radio_TargetReturn.UseVisualStyleBackColor = True
    '
    'Results_Radio_TargetVolatility
    '
    Me.Results_Radio_TargetVolatility.AutoSize = True
    Me.Results_Radio_TargetVolatility.Location = New System.Drawing.Point(5, 20)
    Me.Results_Radio_TargetVolatility.Name = "Results_Radio_TargetVolatility"
    Me.Results_Radio_TargetVolatility.Size = New System.Drawing.Size(14, 13)
    Me.Results_Radio_TargetVolatility.TabIndex = 114
    Me.Results_Radio_TargetVolatility.TabStop = True
    Me.Results_Radio_TargetVolatility.UseVisualStyleBackColor = True
    '
    'Label32
    '
    Me.Label32.AutoSize = True
    Me.Label32.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label32.ForeColor = System.Drawing.Color.Black
    Me.Label32.Location = New System.Drawing.Point(25, 46)
    Me.Label32.Name = "Label32"
    Me.Label32.Size = New System.Drawing.Size(73, 13)
    Me.Label32.TabIndex = 113
    Me.Label32.Text = "Target Return"
    '
    'Results_Text_TargetReturn
    '
    Me.Results_Text_TargetReturn.Location = New System.Drawing.Point(140, 43)
    Me.Results_Text_TargetReturn.Name = "Results_Text_TargetReturn"
    Me.Results_Text_TargetReturn.RenaissanceTag = Nothing
    Me.Results_Text_TargetReturn.Size = New System.Drawing.Size(91, 20)
    Me.Results_Text_TargetReturn.TabIndex = 1
    Me.Results_Text_TargetReturn.Text = "5.00%"
    Me.Results_Text_TargetReturn.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.Results_Text_TargetReturn.TextFormat = "#,##0.00%"
    Me.Results_Text_TargetReturn.Value = 0.05
    '
    'Results_Btn_RoundTrades
    '
    Me.Results_Btn_RoundTrades.ForeColor = System.Drawing.Color.Black
    Me.Results_Btn_RoundTrades.Location = New System.Drawing.Point(140, 125)
    Me.Results_Btn_RoundTrades.Name = "Results_Btn_RoundTrades"
    Me.Results_Btn_RoundTrades.Size = New System.Drawing.Size(91, 36)
    Me.Results_Btn_RoundTrades.TabIndex = 5
    Me.Results_Btn_RoundTrades.Text = "Round Trades"
    Me.Results_Btn_RoundTrades.UseVisualStyleBackColor = True
    '
    'Results_Btn_CaptureNewHoldings
    '
    Me.Results_Btn_CaptureNewHoldings.ForeColor = System.Drawing.Color.Black
    Me.Results_Btn_CaptureNewHoldings.Location = New System.Drawing.Point(6, 125)
    Me.Results_Btn_CaptureNewHoldings.Name = "Results_Btn_CaptureNewHoldings"
    Me.Results_Btn_CaptureNewHoldings.Size = New System.Drawing.Size(123, 36)
    Me.Results_Btn_CaptureNewHoldings.TabIndex = 4
    Me.Results_Btn_CaptureNewHoldings.Text = "Capture New Holdings"
    Me.Results_Btn_CaptureNewHoldings.UseVisualStyleBackColor = True
    '
    'Label31
    '
    Me.Label31.AutoSize = True
    Me.Label31.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label31.ForeColor = System.Drawing.Color.Black
    Me.Label31.Location = New System.Drawing.Point(9, 98)
    Me.Label31.Name = "Label31"
    Me.Label31.Size = New System.Drawing.Size(78, 13)
    Me.Label31.TabIndex = 109
    Me.Label31.Text = "Min Trade Size"
    '
    'Results_Text_MinTradeSize
    '
    Me.Results_Text_MinTradeSize.Location = New System.Drawing.Point(140, 95)
    Me.Results_Text_MinTradeSize.Name = "Results_Text_MinTradeSize"
    Me.Results_Text_MinTradeSize.RenaissanceTag = Nothing
    Me.Results_Text_MinTradeSize.Size = New System.Drawing.Size(91, 20)
    Me.Results_Text_MinTradeSize.TabIndex = 3
    Me.Results_Text_MinTradeSize.Text = "1"
    Me.Results_Text_MinTradeSize.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.Results_Text_MinTradeSize.TextFormat = "#,##0"
    Me.Results_Text_MinTradeSize.Value = 1
    '
    'Label29
    '
    Me.Label29.AutoSize = True
    Me.Label29.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label29.ForeColor = System.Drawing.Color.Black
    Me.Label29.Location = New System.Drawing.Point(9, 72)
    Me.Label29.Name = "Label29"
    Me.Label29.Size = New System.Drawing.Size(95, 13)
    Me.Label29.TabIndex = 107
    Me.Label29.Text = "Total New Holding"
    '
    'Results_Text_TargetHolding
    '
    Me.Results_Text_TargetHolding.Location = New System.Drawing.Point(140, 69)
    Me.Results_Text_TargetHolding.Name = "Results_Text_TargetHolding"
    Me.Results_Text_TargetHolding.RenaissanceTag = Nothing
    Me.Results_Text_TargetHolding.Size = New System.Drawing.Size(91, 20)
    Me.Results_Text_TargetHolding.TabIndex = 2
    Me.Results_Text_TargetHolding.Text = "0"
    Me.Results_Text_TargetHolding.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.Results_Text_TargetHolding.TextFormat = "#,##0"
    Me.Results_Text_TargetHolding.Value = 0
    '
    'Label30
    '
    Me.Label30.AutoSize = True
    Me.Label30.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label30.ForeColor = System.Drawing.Color.Black
    Me.Label30.Location = New System.Drawing.Point(25, 20)
    Me.Label30.Name = "Label30"
    Me.Label30.Size = New System.Drawing.Size(79, 13)
    Me.Label30.TabIndex = 105
    Me.Label30.Text = "Target Volatility"
    '
    'Results_Text_TargetVolatility
    '
    Me.Results_Text_TargetVolatility.Location = New System.Drawing.Point(140, 17)
    Me.Results_Text_TargetVolatility.Name = "Results_Text_TargetVolatility"
    Me.Results_Text_TargetVolatility.RenaissanceTag = Nothing
    Me.Results_Text_TargetVolatility.Size = New System.Drawing.Size(91, 20)
    Me.Results_Text_TargetVolatility.TabIndex = 0
    Me.Results_Text_TargetVolatility.Text = "5.00%"
    Me.Results_Text_TargetVolatility.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.Results_Text_TargetVolatility.TextFormat = "#,##0.00%"
    Me.Results_Text_TargetVolatility.Value = 0.05
    '
    'Results_Btn_PlotRiskReturn
    '
    Me.Results_Btn_PlotRiskReturn.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Results_Btn_PlotRiskReturn.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Results_Btn_PlotRiskReturn.Location = New System.Drawing.Point(822, 481)
    Me.Results_Btn_PlotRiskReturn.Name = "Results_Btn_PlotRiskReturn"
    Me.Results_Btn_PlotRiskReturn.Size = New System.Drawing.Size(237, 23)
    Me.Results_Btn_PlotRiskReturn.TabIndex = 2
    Me.Results_Btn_PlotRiskReturn.Text = "Plot Risk / Return"
    '
    'Results_Btn_ClearResults
    '
    Me.Results_Btn_ClearResults.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Results_Btn_ClearResults.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Results_Btn_ClearResults.Location = New System.Drawing.Point(822, 510)
    Me.Results_Btn_ClearResults.Name = "Results_Btn_ClearResults"
    Me.Results_Btn_ClearResults.Size = New System.Drawing.Size(237, 23)
    Me.Results_Btn_ClearResults.TabIndex = 3
    Me.Results_Btn_ClearResults.Text = "Clear Chart"
    '
    'Chart_Results
    '
    Me.Chart_Results.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Results.BackColor = System.Drawing.Color.Azure
    Me.Chart_Results.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Results.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Results.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Results.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Results.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    Me.Chart_Results.BorderSkin.PageColor = System.Drawing.Color.AliceBlue
    ChartArea12.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea12.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea12.AxisX.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea12.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea12.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea12.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea12.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea12.AxisX.Title = "Volatility"
    ChartArea12.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea12.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
                Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
    ChartArea12.AxisY.LabelStyle.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
    ChartArea12.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea12.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea12.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea12.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea12.AxisY.Title = "Return"
    ChartArea12.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea12.BackColor = System.Drawing.Color.Transparent
    ChartArea12.BorderColor = System.Drawing.Color.DimGray
    ChartArea12.Name = "Default"
    Me.Chart_Results.ChartAreas.Add(ChartArea12)
    Legend12.BackColor = System.Drawing.Color.Transparent
    Legend12.BorderColor = System.Drawing.Color.Transparent
    Legend12.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend12.DockToChartArea = "Default"
    Legend12.Enabled = False
    Legend12.Name = "Default"
    Me.Chart_Results.Legends.Add(Legend12)
    Me.Chart_Results.Location = New System.Drawing.Point(1, 1)
    Me.Chart_Results.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Results.MinimumSize = New System.Drawing.Size(150, 150)
    Me.Chart_Results.Name = "Chart_Results"
    Me.Chart_Results.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series23.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series23.ChartType = "Point"
    Series23.CustomAttributes = "LabelStyle=Top"
    Series23.MarkerBorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
    Series23.Name = "Series1"
    Series23.ShadowOffset = 1
    Series23.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series23.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series24.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series24.ChartType = "Point"
    Series24.CustomAttributes = "LabelStyle=Top"
    Series24.MarkerBorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
    Series24.Name = "Series2"
    Series24.ShadowOffset = 1
    Series24.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series24.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Results.Series.Add(Series23)
    Me.Chart_Results.Series.Add(Series24)
    Me.Chart_Results.Size = New System.Drawing.Size(812, 534)
    Me.Chart_Results.TabIndex = 0
    Me.Chart_Results.Text = "Chart2"
    '
    'Tab_Covariance
    '
    Me.Tab_Covariance.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_Covariance.Controls.Add(Me.Split_Covariance)
    Me.Tab_Covariance.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Covariance.Name = "Tab_Covariance"
    Me.Tab_Covariance.Size = New System.Drawing.Size(1067, 536)
    Me.Tab_Covariance.TabIndex = 3
    Me.Tab_Covariance.Text = "Covariance"
    '
    'Split_Covariance
    '
    Me.Split_Covariance.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Split_Covariance.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Split_Covariance.Location = New System.Drawing.Point(1, 1)
    Me.Split_Covariance.Margin = New System.Windows.Forms.Padding(1)
    Me.Split_Covariance.Name = "Split_Covariance"
    '
    'Split_Covariance.Panel1
    '
    Me.Split_Covariance.Panel1.Controls.Add(Me.Panel1)
    Me.Split_Covariance.Panel1.Controls.Add(Me.Panel_CovarianceMatrixSource)
    Me.Split_Covariance.Panel1.Controls.Add(Me.Label5)
    Me.Split_Covariance.Panel1.Controls.Add(Me.List_CovarianceMatrices)
    '
    'Split_Covariance.Panel2
    '
    Me.Split_Covariance.Panel2.Controls.Add(Me.Grid_Covariance)
    Me.Split_Covariance.Size = New System.Drawing.Size(1064, 534)
    Me.Split_Covariance.SplitterDistance = 327
    Me.Split_Covariance.TabIndex = 0
    '
    'Panel1
    '
    Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel1.Controls.Add(Me.Label_MatrixStatus)
    Me.Panel1.Controls.Add(Me.Label13)
    Me.Panel1.Controls.Add(Me.Label_MatrixLamda)
    Me.Panel1.Controls.Add(Me.Label_MatrixDateTo)
    Me.Panel1.Controls.Add(Me.Label_MatrixDateFrom)
    Me.Panel1.Controls.Add(Me.Label9)
    Me.Panel1.Controls.Add(Me.Label8)
    Me.Panel1.Controls.Add(Me.Label7)
    Me.Panel1.Controls.Add(Me.Label_MatrixName)
    Me.Panel1.Location = New System.Drawing.Point(5, 405)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(273, 119)
    Me.Panel1.TabIndex = 3
    '
    'Label_MatrixStatus
    '
    Me.Label_MatrixStatus.BackColor = System.Drawing.SystemColors.Window
    Me.Label_MatrixStatus.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_MatrixStatus.Location = New System.Drawing.Point(85, 90)
    Me.Label_MatrixStatus.Name = "Label_MatrixStatus"
    Me.Label_MatrixStatus.Size = New System.Drawing.Size(181, 19)
    Me.Label_MatrixStatus.TabIndex = 8
    Me.Label_MatrixStatus.Text = " "
    '
    'Label13
    '
    Me.Label13.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label13.Location = New System.Drawing.Point(3, 91)
    Me.Label13.Name = "Label13"
    Me.Label13.Size = New System.Drawing.Size(76, 14)
    Me.Label13.TabIndex = 4
    Me.Label13.Text = "Matrix Status"
    '
    'Label_MatrixLamda
    '
    Me.Label_MatrixLamda.BackColor = System.Drawing.SystemColors.Window
    Me.Label_MatrixLamda.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_MatrixLamda.Location = New System.Drawing.Point(85, 67)
    Me.Label_MatrixLamda.Name = "Label_MatrixLamda"
    Me.Label_MatrixLamda.Size = New System.Drawing.Size(181, 19)
    Me.Label_MatrixLamda.TabIndex = 7
    Me.Label_MatrixLamda.Text = " "
    '
    'Label_MatrixDateTo
    '
    Me.Label_MatrixDateTo.BackColor = System.Drawing.SystemColors.Window
    Me.Label_MatrixDateTo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_MatrixDateTo.Location = New System.Drawing.Point(85, 44)
    Me.Label_MatrixDateTo.Name = "Label_MatrixDateTo"
    Me.Label_MatrixDateTo.Size = New System.Drawing.Size(181, 19)
    Me.Label_MatrixDateTo.TabIndex = 6
    Me.Label_MatrixDateTo.Text = " "
    '
    'Label_MatrixDateFrom
    '
    Me.Label_MatrixDateFrom.BackColor = System.Drawing.SystemColors.Window
    Me.Label_MatrixDateFrom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_MatrixDateFrom.Location = New System.Drawing.Point(85, 21)
    Me.Label_MatrixDateFrom.Name = "Label_MatrixDateFrom"
    Me.Label_MatrixDateFrom.Size = New System.Drawing.Size(181, 19)
    Me.Label_MatrixDateFrom.TabIndex = 5
    Me.Label_MatrixDateFrom.Text = " "
    '
    'Label9
    '
    Me.Label9.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label9.Location = New System.Drawing.Point(3, 68)
    Me.Label9.Name = "Label9"
    Me.Label9.Size = New System.Drawing.Size(76, 14)
    Me.Label9.TabIndex = 3
    Me.Label9.Text = "Lamda"
    '
    'Label8
    '
    Me.Label8.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label8.Location = New System.Drawing.Point(3, 45)
    Me.Label8.Name = "Label8"
    Me.Label8.Size = New System.Drawing.Size(76, 14)
    Me.Label8.TabIndex = 2
    Me.Label8.Text = "Date To"
    '
    'Label7
    '
    Me.Label7.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label7.Location = New System.Drawing.Point(3, 22)
    Me.Label7.Name = "Label7"
    Me.Label7.Size = New System.Drawing.Size(76, 14)
    Me.Label7.TabIndex = 1
    Me.Label7.Text = "Date From"
    '
    'Label_MatrixName
    '
    Me.Label_MatrixName.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_MatrixName.Location = New System.Drawing.Point(3, 3)
    Me.Label_MatrixName.Name = "Label_MatrixName"
    Me.Label_MatrixName.Size = New System.Drawing.Size(263, 14)
    Me.Label_MatrixName.TabIndex = 0
    Me.Label_MatrixName.Text = "."
    '
    'Panel_CovarianceMatrixSource
    '
    Me.Panel_CovarianceMatrixSource.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel_CovarianceMatrixSource.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel_CovarianceMatrixSource.Controls.Add(Me.Check_Covariance_BackfillData)
    Me.Panel_CovarianceMatrixSource.Controls.Add(Me.Button_SaveAsCovMatrix)
    Me.Panel_CovarianceMatrixSource.Controls.Add(Me.Button_SaveCovMatrix)
    Me.Panel_CovarianceMatrixSource.Controls.Add(Me.Radio_Covariance_EditCovMatrix)
    Me.Panel_CovarianceMatrixSource.Controls.Add(Me.Radio_Covariance_UseSavedMatrix)
    Me.Panel_CovarianceMatrixSource.Controls.Add(Me.Radio_Covariance_UseLiveMatrix)
    Me.Panel_CovarianceMatrixSource.Location = New System.Drawing.Point(5, 5)
    Me.Panel_CovarianceMatrixSource.Name = "Panel_CovarianceMatrixSource"
    Me.Panel_CovarianceMatrixSource.Size = New System.Drawing.Size(314, 130)
    Me.Panel_CovarianceMatrixSource.TabIndex = 0
    '
    'Check_Covariance_BackfillData
    '
    Me.Check_Covariance_BackfillData.AutoSize = True
    Me.Check_Covariance_BackfillData.Location = New System.Drawing.Point(23, 26)
    Me.Check_Covariance_BackfillData.Name = "Check_Covariance_BackfillData"
    Me.Check_Covariance_BackfillData.Size = New System.Drawing.Size(120, 17)
    Me.Check_Covariance_BackfillData.TabIndex = 1
    Me.Check_Covariance_BackfillData.Text = "Backfill Price history"
    Me.Check_Covariance_BackfillData.UseVisualStyleBackColor = True
    '
    'Button_SaveAsCovMatrix
    '
    Me.Button_SaveAsCovMatrix.Enabled = False
    Me.Button_SaveAsCovMatrix.Location = New System.Drawing.Point(100, 96)
    Me.Button_SaveAsCovMatrix.Name = "Button_SaveAsCovMatrix"
    Me.Button_SaveAsCovMatrix.Size = New System.Drawing.Size(71, 25)
    Me.Button_SaveAsCovMatrix.TabIndex = 5
    Me.Button_SaveAsCovMatrix.Text = "Save As..."
    Me.Button_SaveAsCovMatrix.UseVisualStyleBackColor = True
    '
    'Button_SaveCovMatrix
    '
    Me.Button_SaveCovMatrix.Enabled = False
    Me.Button_SaveCovMatrix.Location = New System.Drawing.Point(23, 96)
    Me.Button_SaveCovMatrix.Name = "Button_SaveCovMatrix"
    Me.Button_SaveCovMatrix.Size = New System.Drawing.Size(71, 25)
    Me.Button_SaveCovMatrix.TabIndex = 4
    Me.Button_SaveCovMatrix.Text = "Save"
    Me.Button_SaveCovMatrix.UseVisualStyleBackColor = True
    '
    'Radio_Covariance_EditCovMatrix
    '
    Me.Radio_Covariance_EditCovMatrix.AutoSize = True
    Me.Radio_Covariance_EditCovMatrix.Location = New System.Drawing.Point(3, 72)
    Me.Radio_Covariance_EditCovMatrix.Name = "Radio_Covariance_EditCovMatrix"
    Me.Radio_Covariance_EditCovMatrix.Size = New System.Drawing.Size(131, 17)
    Me.Radio_Covariance_EditCovMatrix.TabIndex = 3
    Me.Radio_Covariance_EditCovMatrix.Text = "Edit Covariance Matrix"
    Me.Radio_Covariance_EditCovMatrix.UseVisualStyleBackColor = True
    '
    'Radio_Covariance_UseSavedMatrix
    '
    Me.Radio_Covariance_UseSavedMatrix.AutoSize = True
    Me.Radio_Covariance_UseSavedMatrix.Location = New System.Drawing.Point(3, 51)
    Me.Radio_Covariance_UseSavedMatrix.Name = "Radio_Covariance_UseSavedMatrix"
    Me.Radio_Covariance_UseSavedMatrix.Size = New System.Drawing.Size(166, 17)
    Me.Radio_Covariance_UseSavedMatrix.TabIndex = 2
    Me.Radio_Covariance_UseSavedMatrix.Text = "Use Saved Covariance Matrix"
    Me.Radio_Covariance_UseSavedMatrix.UseVisualStyleBackColor = True
    '
    'Radio_Covariance_UseLiveMatrix
    '
    Me.Radio_Covariance_UseLiveMatrix.AutoSize = True
    Me.Radio_Covariance_UseLiveMatrix.Checked = True
    Me.Radio_Covariance_UseLiveMatrix.Location = New System.Drawing.Point(3, 3)
    Me.Radio_Covariance_UseLiveMatrix.Name = "Radio_Covariance_UseLiveMatrix"
    Me.Radio_Covariance_UseLiveMatrix.Size = New System.Drawing.Size(155, 17)
    Me.Radio_Covariance_UseLiveMatrix.TabIndex = 0
    Me.Radio_Covariance_UseLiveMatrix.TabStop = True
    Me.Radio_Covariance_UseLiveMatrix.Text = "Use Live Covariance Matrix"
    Me.Radio_Covariance_UseLiveMatrix.UseVisualStyleBackColor = True
    '
    'Label5
    '
    Me.Label5.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label5.Location = New System.Drawing.Point(7, 138)
    Me.Label5.Name = "Label5"
    Me.Label5.Size = New System.Drawing.Size(141, 14)
    Me.Label5.TabIndex = 1
    Me.Label5.Text = "Saved Covariance Matrices"
    '
    'List_CovarianceMatrices
    '
    Me.List_CovarianceMatrices.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.List_CovarianceMatrices.FormattingEnabled = True
    Me.List_CovarianceMatrices.Location = New System.Drawing.Point(5, 155)
    Me.List_CovarianceMatrices.Name = "List_CovarianceMatrices"
    Me.List_CovarianceMatrices.Size = New System.Drawing.Size(314, 238)
    Me.List_CovarianceMatrices.Sorted = True
    Me.List_CovarianceMatrices.TabIndex = 2
    '
    'Grid_Covariance
    '
    Me.Grid_Covariance.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Covariance.ColumnInfo = "1,1,0,0,0,85,Columns:0{Width:101;Name:""Col_Name"";AllowDragging:False;AllowEditing" & _
        ":False;Style:""Format:""""0000"""";DataType:System.String;Font:Microsoft Sans Serif, " & _
        "8.25pt;TextAlign:LeftCenter;"";}" & Global.Microsoft.VisualBasic.ChrW(9)
    Me.Grid_Covariance.Location = New System.Drawing.Point(5, 5)
    Me.Grid_Covariance.Margin = New System.Windows.Forms.Padding(1)
    Me.Grid_Covariance.Name = "Grid_Covariance"
    Me.Grid_Covariance.Rows.Count = 1
    Me.Grid_Covariance.Rows.DefaultSize = 17
    Me.Grid_Covariance.Size = New System.Drawing.Size(719, 523)
    Me.Grid_Covariance.TabIndex = 0
    '
    'Tab_Constraints
    '
    Me.Tab_Constraints.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_Constraints.Controls.Add(Me.SplitContainer1)
    Me.Tab_Constraints.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Constraints.Name = "Tab_Constraints"
    Me.Tab_Constraints.Size = New System.Drawing.Size(1067, 536)
    Me.Tab_Constraints.TabIndex = 4
    Me.Tab_Constraints.Text = "Constraints"
    '
    'SplitContainer1
    '
    Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
    Me.SplitContainer1.Name = "SplitContainer1"
    '
    'SplitContainer1.Panel1
    '
    Me.SplitContainer1.Panel1.Controls.Add(Me.Panel5)
    Me.SplitContainer1.Panel1.Controls.Add(Me.Panel4)
    Me.SplitContainer1.Panel1.Controls.Add(Me.Constraint_Button_CancelGroup)
    Me.SplitContainer1.Panel1.Controls.Add(Me.Constraints_ComboConstraintsGoup)
    Me.SplitContainer1.Panel1.Controls.Add(Me.Label20)
    Me.SplitContainer1.Panel1.Controls.Add(Me.Constraint_Button_SaveGroup)
    '
    'SplitContainer1.Panel2
    '
    Me.SplitContainer1.Panel2.Controls.Add(Me.Constraints_TabConstraints)
    Me.SplitContainer1.Size = New System.Drawing.Size(1067, 536)
    Me.SplitContainer1.SplitterDistance = 350
    Me.SplitContainer1.TabIndex = 0
    '
    'Panel5
    '
    Me.Panel5.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel5.Controls.Add(Me.Constraints_Grid_Defined)
    Me.Panel5.Controls.Add(Me.Constraint_Button_AllActive)
    Me.Panel5.Controls.Add(Me.Constraint_Button_NoneActive)
    Me.Panel5.Controls.Add(Me.Label24)
    Me.Panel5.Controls.Add(Me.Label23)
    Me.Panel5.Location = New System.Drawing.Point(5, 32)
    Me.Panel5.Name = "Panel5"
    Me.Panel5.Size = New System.Drawing.Size(340, 410)
    Me.Panel5.TabIndex = 1
    '
    'Constraints_Grid_Defined
    '
    Me.Constraints_Grid_Defined.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Constraints_Grid_Defined.AutoGenerateColumns = False
    Me.Constraints_Grid_Defined.AutoResize = False
    Me.Constraints_Grid_Defined.ColumnInfo = resources.GetString("Constraints_Grid_Defined.ColumnInfo")
    Me.Constraints_Grid_Defined.ExtendLastCol = True
    Me.Constraints_Grid_Defined.Location = New System.Drawing.Point(1, 20)
    Me.Constraints_Grid_Defined.Name = "Constraints_Grid_Defined"
    Me.Constraints_Grid_Defined.Rows.Count = 1
    Me.Constraints_Grid_Defined.Rows.DefaultSize = 17
    Me.Constraints_Grid_Defined.Size = New System.Drawing.Size(335, 359)
    Me.Constraints_Grid_Defined.TabIndex = 1
    '
    'Constraint_Button_AllActive
    '
    Me.Constraint_Button_AllActive.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Constraint_Button_AllActive.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Constraint_Button_AllActive.Location = New System.Drawing.Point(136, 385)
    Me.Constraint_Button_AllActive.Name = "Constraint_Button_AllActive"
    Me.Constraint_Button_AllActive.Size = New System.Drawing.Size(45, 20)
    Me.Constraint_Button_AllActive.TabIndex = 3
    Me.Constraint_Button_AllActive.Text = "All"
    '
    'Constraint_Button_NoneActive
    '
    Me.Constraint_Button_NoneActive.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Constraint_Button_NoneActive.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Constraint_Button_NoneActive.Location = New System.Drawing.Point(187, 385)
    Me.Constraint_Button_NoneActive.Name = "Constraint_Button_NoneActive"
    Me.Constraint_Button_NoneActive.Size = New System.Drawing.Size(45, 20)
    Me.Constraint_Button_NoneActive.TabIndex = 4
    Me.Constraint_Button_NoneActive.Text = "None"
    '
    'Label24
    '
    Me.Label24.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label24.Location = New System.Drawing.Point(2, 2)
    Me.Label24.Name = "Label24"
    Me.Label24.Size = New System.Drawing.Size(253, 15)
    Me.Label24.TabIndex = 0
    Me.Label24.Text = "Defined / Applied Constraint sets"
    '
    'Label23
    '
    Me.Label23.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Label23.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label23.Location = New System.Drawing.Point(3, 389)
    Me.Label23.Name = "Label23"
    Me.Label23.Size = New System.Drawing.Size(91, 14)
    Me.Label23.TabIndex = 2
    Me.Label23.Text = "Active Constraints"
    '
    'Panel4
    '
    Me.Panel4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel4.Controls.Add(Me.Constraint_Button_NewGroup_Copy)
    Me.Panel4.Controls.Add(Me.Constraint_Button_DeleteGroup)
    Me.Panel4.Controls.Add(Me.Constraint_Button_RenameGroup)
    Me.Panel4.Controls.Add(Me.Constraint_Button_NewGroup)
    Me.Panel4.Controls.Add(Me.Label26)
    Me.Panel4.Location = New System.Drawing.Point(5, 448)
    Me.Panel4.Name = "Panel4"
    Me.Panel4.Size = New System.Drawing.Size(340, 54)
    Me.Panel4.TabIndex = 2
    '
    'Constraint_Button_NewGroup_Copy
    '
    Me.Constraint_Button_NewGroup_Copy.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Constraint_Button_NewGroup_Copy.Location = New System.Drawing.Point(74, 19)
    Me.Constraint_Button_NewGroup_Copy.Name = "Constraint_Button_NewGroup_Copy"
    Me.Constraint_Button_NewGroup_Copy.Size = New System.Drawing.Size(65, 23)
    Me.Constraint_Button_NewGroup_Copy.TabIndex = 4
    Me.Constraint_Button_NewGroup_Copy.Text = "New (Copy)"
    '
    'Constraint_Button_DeleteGroup
    '
    Me.Constraint_Button_DeleteGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Constraint_Button_DeleteGroup.Location = New System.Drawing.Point(216, 19)
    Me.Constraint_Button_DeleteGroup.Name = "Constraint_Button_DeleteGroup"
    Me.Constraint_Button_DeleteGroup.Size = New System.Drawing.Size(65, 23)
    Me.Constraint_Button_DeleteGroup.TabIndex = 3
    Me.Constraint_Button_DeleteGroup.Text = "Delete"
    '
    'Constraint_Button_RenameGroup
    '
    Me.Constraint_Button_RenameGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Constraint_Button_RenameGroup.Location = New System.Drawing.Point(145, 19)
    Me.Constraint_Button_RenameGroup.Name = "Constraint_Button_RenameGroup"
    Me.Constraint_Button_RenameGroup.Size = New System.Drawing.Size(65, 23)
    Me.Constraint_Button_RenameGroup.TabIndex = 2
    Me.Constraint_Button_RenameGroup.Text = "Rename"
    '
    'Constraint_Button_NewGroup
    '
    Me.Constraint_Button_NewGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Constraint_Button_NewGroup.Location = New System.Drawing.Point(3, 19)
    Me.Constraint_Button_NewGroup.Name = "Constraint_Button_NewGroup"
    Me.Constraint_Button_NewGroup.Size = New System.Drawing.Size(65, 23)
    Me.Constraint_Button_NewGroup.TabIndex = 1
    Me.Constraint_Button_NewGroup.Text = "New"
    '
    'Label26
    '
    Me.Label26.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label26.Location = New System.Drawing.Point(2, 1)
    Me.Label26.Name = "Label26"
    Me.Label26.Size = New System.Drawing.Size(253, 15)
    Me.Label26.TabIndex = 0
    Me.Label26.Text = "Constriant Group management"
    '
    'Constraint_Button_CancelGroup
    '
    Me.Constraint_Button_CancelGroup.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Constraint_Button_CancelGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Constraint_Button_CancelGroup.Location = New System.Drawing.Point(96, 509)
    Me.Constraint_Button_CancelGroup.Name = "Constraint_Button_CancelGroup"
    Me.Constraint_Button_CancelGroup.Size = New System.Drawing.Size(81, 23)
    Me.Constraint_Button_CancelGroup.TabIndex = 4
    Me.Constraint_Button_CancelGroup.Text = "Cancel"
    '
    'Constraints_ComboConstraintsGoup
    '
    Me.Constraints_ComboConstraintsGoup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Constraints_ComboConstraintsGoup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Constraints_ComboConstraintsGoup.Location = New System.Drawing.Point(102, 7)
    Me.Constraints_ComboConstraintsGoup.Name = "Constraints_ComboConstraintsGoup"
    Me.Constraints_ComboConstraintsGoup.Size = New System.Drawing.Size(243, 21)
    Me.Constraints_ComboConstraintsGoup.TabIndex = 0
    '
    'Label20
    '
    Me.Label20.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label20.Location = New System.Drawing.Point(8, 11)
    Me.Label20.Name = "Label20"
    Me.Label20.Size = New System.Drawing.Size(91, 15)
    Me.Label20.TabIndex = 20
    Me.Label20.Text = "Constraint Group"
    '
    'Constraint_Button_SaveGroup
    '
    Me.Constraint_Button_SaveGroup.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Constraint_Button_SaveGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Constraint_Button_SaveGroup.Location = New System.Drawing.Point(9, 509)
    Me.Constraint_Button_SaveGroup.Name = "Constraint_Button_SaveGroup"
    Me.Constraint_Button_SaveGroup.Size = New System.Drawing.Size(81, 23)
    Me.Constraint_Button_SaveGroup.TabIndex = 3
    Me.Constraint_Button_SaveGroup.Text = "Save"
    '
    'Constraints_TabConstraints
    '
    Me.Constraints_TabConstraints.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Constraints_TabConstraints.Controls.Add(Me.Tab_Template)
    Me.Constraints_TabConstraints.Location = New System.Drawing.Point(2, 2)
    Me.Constraints_TabConstraints.Name = "Constraints_TabConstraints"
    Me.Constraints_TabConstraints.SelectedIndex = 0
    Me.Constraints_TabConstraints.Size = New System.Drawing.Size(711, 534)
    Me.Constraints_TabConstraints.TabIndex = 0
    '
    'Tab_Template
    '
    Me.Tab_Template.Controls.Add(Me.Constraints_Grid_Template)
    Me.Tab_Template.Location = New System.Drawing.Point(4, 22)
    Me.Tab_Template.Name = "Tab_Template"
    Me.Tab_Template.Padding = New System.Windows.Forms.Padding(3)
    Me.Tab_Template.Size = New System.Drawing.Size(703, 508)
    Me.Tab_Template.TabIndex = 0
    Me.Tab_Template.Text = "Template"
    Me.Tab_Template.UseVisualStyleBackColor = True
    '
    'Constraints_Grid_Template
    '
    Me.Constraints_Grid_Template.AllowAddNew = True
    Me.Constraints_Grid_Template.AllowDelete = True
    Me.Constraints_Grid_Template.AllowDragging = C1.Win.C1FlexGrid.AllowDraggingEnum.None
    Me.Constraints_Grid_Template.AllowSorting = C1.Win.C1FlexGrid.AllowSortingEnum.None
    Me.Constraints_Grid_Template.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Constraints_Grid_Template.AutoGenerateColumns = False
    Me.Constraints_Grid_Template.AutoResize = False
    Me.Constraints_Grid_Template.ColumnInfo = resources.GetString("Constraints_Grid_Template.ColumnInfo")
    Me.Constraints_Grid_Template.KeyActionTab = C1.Win.C1FlexGrid.KeyActionEnum.MoveAcross
    Me.Constraints_Grid_Template.Location = New System.Drawing.Point(3, 3)
    Me.Constraints_Grid_Template.Name = "Constraints_Grid_Template"
    Me.Constraints_Grid_Template.Rows.Count = 1
    Me.Constraints_Grid_Template.Rows.DefaultSize = 17
    Me.Constraints_Grid_Template.Size = New System.Drawing.Size(697, 502)
    Me.Constraints_Grid_Template.TabIndex = 0
    '
    'Tab_OptimisationIO
    '
    Me.Tab_OptimisationIO.BackColor = System.Drawing.SystemColors.Control
    Me.Tab_OptimisationIO.Controls.Add(Me.CheckBox_RunUnconstrained)
    Me.Tab_OptimisationIO.Controls.Add(Me.Edit_MarginalsMoveCount)
    Me.Tab_OptimisationIO.Controls.Add(Me.Label33)
    Me.Tab_OptimisationIO.Controls.Add(Me.checkbox_UseMarginalsIteration)
    Me.Tab_OptimisationIO.Controls.Add(Me.CheckBox_ZeroWeightOptimiserFix)
    Me.Tab_OptimisationIO.Controls.Add(Me.Optimisation_Check_SaveResultsToFile)
    Me.Tab_OptimisationIO.Controls.Add(Me.Label28)
    Me.Tab_OptimisationIO.Controls.Add(Me.Optimisation_ELambdaE)
    Me.Tab_OptimisationIO.Controls.Add(Me.Label27)
    Me.Tab_OptimisationIO.Controls.Add(Me.Optimisation_CpLimit)
    Me.Tab_OptimisationIO.Controls.Add(Me.Grid_OptimisationResults)
    Me.Tab_OptimisationIO.Controls.Add(Me.Btn_RunOptimisation)
    Me.Tab_OptimisationIO.Location = New System.Drawing.Point(4, 22)
    Me.Tab_OptimisationIO.Name = "Tab_OptimisationIO"
    Me.Tab_OptimisationIO.Size = New System.Drawing.Size(1067, 536)
    Me.Tab_OptimisationIO.TabIndex = 5
    Me.Tab_OptimisationIO.Text = "Optimisation Input/Output"
    '
    'CheckBox_RunUnconstrained
    '
    Me.CheckBox_RunUnconstrained.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.CheckBox_RunUnconstrained.Location = New System.Drawing.Point(172, 23)
    Me.CheckBox_RunUnconstrained.Name = "CheckBox_RunUnconstrained"
    Me.CheckBox_RunUnconstrained.Size = New System.Drawing.Size(163, 18)
    Me.CheckBox_RunUnconstrained.TabIndex = 3
    Me.CheckBox_RunUnconstrained.Text = "Run Unconstrained."
    Me.CheckBox_RunUnconstrained.UseVisualStyleBackColor = True
    '
    'Edit_MarginalsMoveCount
    '
    Me.Edit_MarginalsMoveCount.Location = New System.Drawing.Point(512, 41)
    Me.Edit_MarginalsMoveCount.Name = "Edit_MarginalsMoveCount"
    Me.Edit_MarginalsMoveCount.RenaissanceTag = Nothing
    Me.Edit_MarginalsMoveCount.Size = New System.Drawing.Size(28, 20)
    Me.Edit_MarginalsMoveCount.TabIndex = 7
    Me.Edit_MarginalsMoveCount.Text = "5"
    Me.Edit_MarginalsMoveCount.TextFormat = "#,##0"
    Me.Edit_MarginalsMoveCount.Value = 5
    '
    'Label33
    '
    Me.Label33.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label33.Location = New System.Drawing.Point(386, 44)
    Me.Label33.Name = "Label33"
    Me.Label33.Size = New System.Drawing.Size(122, 15)
    Me.Label33.TabIndex = 6
    Me.Label33.Text = "Marginals Move Count"
    '
    'checkbox_UseMarginalsIteration
    '
    Me.checkbox_UseMarginalsIteration.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.checkbox_UseMarginalsIteration.Location = New System.Drawing.Point(363, 23)
    Me.checkbox_UseMarginalsIteration.Name = "checkbox_UseMarginalsIteration"
    Me.checkbox_UseMarginalsIteration.Size = New System.Drawing.Size(163, 18)
    Me.checkbox_UseMarginalsIteration.TabIndex = 5
    Me.checkbox_UseMarginalsIteration.Text = "Use Marginals Iteration"
    Me.checkbox_UseMarginalsIteration.UseVisualStyleBackColor = True
    '
    'CheckBox_ZeroWeightOptimiserFix
    '
    Me.CheckBox_ZeroWeightOptimiserFix.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.CheckBox_ZeroWeightOptimiserFix.Location = New System.Drawing.Point(363, 5)
    Me.CheckBox_ZeroWeightOptimiserFix.Name = "CheckBox_ZeroWeightOptimiserFix"
    Me.CheckBox_ZeroWeightOptimiserFix.Size = New System.Drawing.Size(163, 18)
    Me.CheckBox_ZeroWeightOptimiserFix.TabIndex = 4
    Me.CheckBox_ZeroWeightOptimiserFix.Text = "Apply Zero Weight Fix"
    Me.CheckBox_ZeroWeightOptimiserFix.UseVisualStyleBackColor = True
    '
    'Optimisation_Check_SaveResultsToFile
    '
    Me.Optimisation_Check_SaveResultsToFile.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Optimisation_Check_SaveResultsToFile.Location = New System.Drawing.Point(172, 5)
    Me.Optimisation_Check_SaveResultsToFile.Name = "Optimisation_Check_SaveResultsToFile"
    Me.Optimisation_Check_SaveResultsToFile.Size = New System.Drawing.Size(163, 18)
    Me.Optimisation_Check_SaveResultsToFile.TabIndex = 2
    Me.Optimisation_Check_SaveResultsToFile.Text = "Save Results to File"
    Me.Optimisation_Check_SaveResultsToFile.UseVisualStyleBackColor = True
    '
    'Label28
    '
    Me.Label28.AutoSize = True
    Me.Label28.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label28.Location = New System.Drawing.Point(8, 36)
    Me.Label28.Name = "Label28"
    Me.Label28.Size = New System.Drawing.Size(59, 13)
    Me.Label28.TabIndex = 103
    Me.Label28.Text = "ELambdaE"
    '
    'Optimisation_ELambdaE
    '
    Me.Optimisation_ELambdaE.Location = New System.Drawing.Point(74, 33)
    Me.Optimisation_ELambdaE.Name = "Optimisation_ELambdaE"
    Me.Optimisation_ELambdaE.RenaissanceTag = Nothing
    Me.Optimisation_ELambdaE.Size = New System.Drawing.Size(74, 20)
    Me.Optimisation_ELambdaE.TabIndex = 1
    Me.Optimisation_ELambdaE.Text = "0.0000001"
    Me.Optimisation_ELambdaE.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.Optimisation_ELambdaE.TextFormat = "#,##0.0#########"
    Me.Optimisation_ELambdaE.Value = 0.0000001
    '
    'Label27
    '
    Me.Label27.AutoSize = True
    Me.Label27.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label27.Location = New System.Drawing.Point(8, 10)
    Me.Label27.Name = "Label27"
    Me.Label27.Size = New System.Drawing.Size(52, 13)
    Me.Label27.TabIndex = 101
    Me.Label27.Text = "Max CPs."
    '
    'Optimisation_CpLimit
    '
    Me.Optimisation_CpLimit.Location = New System.Drawing.Point(74, 7)
    Me.Optimisation_CpLimit.Name = "Optimisation_CpLimit"
    Me.Optimisation_CpLimit.RenaissanceTag = Nothing
    Me.Optimisation_CpLimit.Size = New System.Drawing.Size(74, 20)
    Me.Optimisation_CpLimit.TabIndex = 0
    Me.Optimisation_CpLimit.Text = "100"
    Me.Optimisation_CpLimit.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
    Me.Optimisation_CpLimit.TextFormat = "#,##0"
    Me.Optimisation_CpLimit.Value = 100
    '
    'Grid_OptimisationResults
    '
    Me.Grid_OptimisationResults.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_OptimisationResults.ColumnInfo = resources.GetString("Grid_OptimisationResults.ColumnInfo")
    Me.Grid_OptimisationResults.Location = New System.Drawing.Point(3, 65)
    Me.Grid_OptimisationResults.Name = "Grid_OptimisationResults"
    Me.Grid_OptimisationResults.Rows.Count = 1
    Me.Grid_OptimisationResults.Rows.DefaultSize = 17
    Me.Grid_OptimisationResults.Size = New System.Drawing.Size(1059, 468)
    Me.Grid_OptimisationResults.TabIndex = 9
    '
    'Btn_RunOptimisation
    '
    Me.Btn_RunOptimisation.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Btn_RunOptimisation.Location = New System.Drawing.Point(871, 9)
    Me.Btn_RunOptimisation.Name = "Btn_RunOptimisation"
    Me.Btn_RunOptimisation.Size = New System.Drawing.Size(188, 50)
    Me.Btn_RunOptimisation.TabIndex = 8
    Me.Btn_RunOptimisation.Text = "Run Optimisation"
    Me.Btn_RunOptimisation.UseVisualStyleBackColor = True
    '
    'Label_HoldingReturn
    '
    Me.Label_HoldingReturn.BackColor = System.Drawing.SystemColors.Window
    Me.Label_HoldingReturn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_HoldingReturn.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_HoldingReturn.Location = New System.Drawing.Point(240, 4)
    Me.Label_HoldingReturn.Name = "Label_HoldingReturn"
    Me.Label_HoldingReturn.Size = New System.Drawing.Size(50, 19)
    Me.Label_HoldingReturn.TabIndex = 4
    Me.Label_HoldingReturn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Label11
    '
    Me.Label11.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label11.Location = New System.Drawing.Point(192, 5)
    Me.Label11.Name = "Label11"
    Me.Label11.Size = New System.Drawing.Size(42, 19)
    Me.Label11.TabIndex = 3
    Me.Label11.Text = "Return"
    Me.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label_HoldingRisk
    '
    Me.Label_HoldingRisk.BackColor = System.Drawing.SystemColors.Window
    Me.Label_HoldingRisk.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_HoldingRisk.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_HoldingRisk.Location = New System.Drawing.Point(138, 4)
    Me.Label_HoldingRisk.Name = "Label_HoldingRisk"
    Me.Label_HoldingRisk.Size = New System.Drawing.Size(50, 19)
    Me.Label_HoldingRisk.TabIndex = 2
    Me.Label_HoldingRisk.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Label14
    '
    Me.Label14.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label14.Location = New System.Drawing.Point(101, 5)
    Me.Label14.Name = "Label14"
    Me.Label14.Size = New System.Drawing.Size(31, 19)
    Me.Label14.TabIndex = 1
    Me.Label14.Text = "Risk"
    Me.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label_HoldingValue
    '
    Me.Label_HoldingValue.BackColor = System.Drawing.SystemColors.Window
    Me.Label_HoldingValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_HoldingValue.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_HoldingValue.Location = New System.Drawing.Point(333, 4)
    Me.Label_HoldingValue.Name = "Label_HoldingValue"
    Me.Label_HoldingValue.Size = New System.Drawing.Size(87, 19)
    Me.Label_HoldingValue.TabIndex = 6
    Me.Label_HoldingValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Label16
    '
    Me.Label16.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label16.Location = New System.Drawing.Point(294, 5)
    Me.Label16.Name = "Label16"
    Me.Label16.Size = New System.Drawing.Size(33, 19)
    Me.Label16.TabIndex = 5
    Me.Label16.Text = "Value"
    Me.Label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Panel2
    '
    Me.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel2.Controls.Add(Me.Label38)
    Me.Panel2.Controls.Add(Me.Label_HoldingDrawDown)
    Me.Panel2.Controls.Add(Me.Label18)
    Me.Panel2.Controls.Add(Me.Label_HoldingValue)
    Me.Panel2.Controls.Add(Me.Label11)
    Me.Panel2.Controls.Add(Me.Label16)
    Me.Panel2.Controls.Add(Me.Label_HoldingReturn)
    Me.Panel2.Controls.Add(Me.Label_HoldingRisk)
    Me.Panel2.Controls.Add(Me.Label14)
    Me.Panel2.Location = New System.Drawing.Point(6, 60)
    Me.Panel2.Name = "Panel2"
    Me.Panel2.Size = New System.Drawing.Size(525, 31)
    Me.Panel2.TabIndex = 6
    '
    'Label38
    '
    Me.Label38.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label38.Location = New System.Drawing.Point(426, 5)
    Me.Label38.Name = "Label38"
    Me.Label38.Size = New System.Drawing.Size(42, 19)
    Me.Label38.TabIndex = 7
    Me.Label38.Text = "Draw Dn"
    Me.Label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label_HoldingDrawDown
    '
    Me.Label_HoldingDrawDown.BackColor = System.Drawing.SystemColors.Window
    Me.Label_HoldingDrawDown.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_HoldingDrawDown.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_HoldingDrawDown.Location = New System.Drawing.Point(474, 4)
    Me.Label_HoldingDrawDown.Name = "Label_HoldingDrawDown"
    Me.Label_HoldingDrawDown.Size = New System.Drawing.Size(44, 19)
    Me.Label_HoldingDrawDown.TabIndex = 8
    Me.Label_HoldingDrawDown.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Label18
    '
    Me.Label18.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label18.Location = New System.Drawing.Point(3, 5)
    Me.Label18.Name = "Label18"
    Me.Label18.Size = New System.Drawing.Size(99, 19)
    Me.Label18.TabIndex = 0
    Me.Label18.Text = "'Holding' Portfolio"
    Me.Label18.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'Panel3
    '
    Me.Panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel3.Controls.Add(Me.Label40)
    Me.Panel3.Controls.Add(Me.Label_NewHoldingDrawDown)
    Me.Panel3.Controls.Add(Me.Label19)
    Me.Panel3.Controls.Add(Me.Label_NewHoldingValue)
    Me.Panel3.Controls.Add(Me.Label21)
    Me.Panel3.Controls.Add(Me.Label22)
    Me.Panel3.Controls.Add(Me.Label_NewHoldingReturn)
    Me.Panel3.Controls.Add(Me.Label_NewHoldingRisk)
    Me.Panel3.Controls.Add(Me.Label25)
    Me.Panel3.Location = New System.Drawing.Point(537, 60)
    Me.Panel3.Name = "Panel3"
    Me.Panel3.Size = New System.Drawing.Size(535, 31)
    Me.Panel3.TabIndex = 7
    '
    'Label40
    '
    Me.Label40.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label40.Location = New System.Drawing.Point(435, 5)
    Me.Label40.Name = "Label40"
    Me.Label40.Size = New System.Drawing.Size(42, 19)
    Me.Label40.TabIndex = 9
    Me.Label40.Text = "Draw Dn"
    Me.Label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label_NewHoldingDrawDown
    '
    Me.Label_NewHoldingDrawDown.BackColor = System.Drawing.SystemColors.Window
    Me.Label_NewHoldingDrawDown.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_NewHoldingDrawDown.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_NewHoldingDrawDown.Location = New System.Drawing.Point(483, 4)
    Me.Label_NewHoldingDrawDown.Name = "Label_NewHoldingDrawDown"
    Me.Label_NewHoldingDrawDown.Size = New System.Drawing.Size(44, 19)
    Me.Label_NewHoldingDrawDown.TabIndex = 10
    Me.Label_NewHoldingDrawDown.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Label19
    '
    Me.Label19.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label19.Location = New System.Drawing.Point(3, 5)
    Me.Label19.Name = "Label19"
    Me.Label19.Size = New System.Drawing.Size(113, 19)
    Me.Label19.TabIndex = 0
    Me.Label19.Text = "'New Holding' Portfolio"
    Me.Label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
    '
    'Label_NewHoldingValue
    '
    Me.Label_NewHoldingValue.BackColor = System.Drawing.SystemColors.Window
    Me.Label_NewHoldingValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_NewHoldingValue.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_NewHoldingValue.Location = New System.Drawing.Point(343, 4)
    Me.Label_NewHoldingValue.Name = "Label_NewHoldingValue"
    Me.Label_NewHoldingValue.Size = New System.Drawing.Size(87, 19)
    Me.Label_NewHoldingValue.TabIndex = 6
    Me.Label_NewHoldingValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Label21
    '
    Me.Label21.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label21.Location = New System.Drawing.Point(204, 5)
    Me.Label21.Name = "Label21"
    Me.Label21.Size = New System.Drawing.Size(42, 19)
    Me.Label21.TabIndex = 3
    Me.Label21.Text = "Return"
    Me.Label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label22
    '
    Me.Label22.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label22.Location = New System.Drawing.Point(304, 5)
    Me.Label22.Name = "Label22"
    Me.Label22.Size = New System.Drawing.Size(33, 19)
    Me.Label22.TabIndex = 5
    Me.Label22.Text = "Value"
    Me.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'Label_NewHoldingReturn
    '
    Me.Label_NewHoldingReturn.BackColor = System.Drawing.SystemColors.Window
    Me.Label_NewHoldingReturn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_NewHoldingReturn.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_NewHoldingReturn.Location = New System.Drawing.Point(252, 4)
    Me.Label_NewHoldingReturn.Name = "Label_NewHoldingReturn"
    Me.Label_NewHoldingReturn.Size = New System.Drawing.Size(50, 19)
    Me.Label_NewHoldingReturn.TabIndex = 4
    Me.Label_NewHoldingReturn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Label_NewHoldingRisk
    '
    Me.Label_NewHoldingRisk.BackColor = System.Drawing.SystemColors.Window
    Me.Label_NewHoldingRisk.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Label_NewHoldingRisk.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label_NewHoldingRisk.Location = New System.Drawing.Point(152, 4)
    Me.Label_NewHoldingRisk.Name = "Label_NewHoldingRisk"
    Me.Label_NewHoldingRisk.Size = New System.Drawing.Size(50, 19)
    Me.Label_NewHoldingRisk.TabIndex = 2
    Me.Label_NewHoldingRisk.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
    '
    'Label25
    '
    Me.Label25.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Label25.Location = New System.Drawing.Point(113, 5)
    Me.Label25.Name = "Label25"
    Me.Label25.Size = New System.Drawing.Size(33, 19)
    Me.Label25.TabIndex = 1
    Me.Label25.Text = "Risk"
    Me.Label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight
    '
    'frmOptimise
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(1077, 684)
    Me.Controls.Add(Me.Panel3)
    Me.Controls.Add(Me.Panel2)
    Me.Controls.Add(Me.TabControl1)
    Me.Controls.Add(Me.StatusStrip_GroupReturns)
    Me.Controls.Add(Me.btnNavFirst)
    Me.Controls.Add(Me.btnNavPrev)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.btnNavNext)
    Me.Controls.Add(Me.Combo_SelectGroup)
    Me.Controls.Add(Me.btnLast)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Combo_SelectGroupID)
    Me.Controls.Add(Me.editAuditID)
    Me.Controls.Add(Me.RootMenu)
    Me.MainMenuStrip = Me.RootMenu
    Me.MinimumSize = New System.Drawing.Size(938, 339)
    Me.Name = "frmOptimise"
    Me.Text = "Group Optimisation"
    CType(Me.Grid_Returns, System.ComponentModel.ISupportInitialize).EndInit()
    Me.StatusStrip_GroupReturns.ResumeLayout(False)
    Me.StatusStrip_GroupReturns.PerformLayout()
    Me.GroupBox_Actions.ResumeLayout(False)
    Me.GroupBox_Actions.PerformLayout()
    CType(Me.Combo_BenckmarkIndex, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Split_GroupReturns.Panel1.ResumeLayout(False)
    Me.Split_GroupReturns.Panel2.ResumeLayout(False)
    Me.Split_GroupReturns.ResumeLayout(False)
    CType(Me.Chart_MonthlyReturns, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Chart_VAMI, System.ComponentModel.ISupportInitialize).EndInit()
    Me.TabControl1.ResumeLayout(False)
    Me.Tab_Instruments.ResumeLayout(False)
    Me.Tab_Marginals.ResumeLayout(False)
    CType(Me.Grid_Marginals, System.ComponentModel.ISupportInitialize).EndInit()
    Me.GroupBox5.ResumeLayout(False)
    Me.GroupBox4.ResumeLayout(False)
    Me.GroupBox4.PerformLayout()
    Me.GroupBox3.ResumeLayout(False)
    Me.GroupBox3.PerformLayout()
    Me.GroupBox2.ResumeLayout(False)
    Me.GroupBox2.PerformLayout()
    CType(Me.Combo_MarginalIndex, System.ComponentModel.ISupportInitialize).EndInit()
    Me.GroupBox1.ResumeLayout(False)
    Me.GroupBox1.PerformLayout()
    Me.GroupBox6.ResumeLayout(False)
    Me.GroupBox6.PerformLayout()
    Me.Tab_Results.ResumeLayout(False)
    Me.GroupBox8.ResumeLayout(False)
    Me.GroupBox8.PerformLayout()
    Me.Panel_PortfolioValues.ResumeLayout(False)
    Me.Panel_PortfolioValues.PerformLayout()
    Me.GroupBox7.ResumeLayout(False)
    Me.GroupBox7.PerformLayout()
    CType(Me.Chart_Results, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Covariance.ResumeLayout(False)
    Me.Split_Covariance.Panel1.ResumeLayout(False)
    Me.Split_Covariance.Panel2.ResumeLayout(False)
    Me.Split_Covariance.ResumeLayout(False)
    Me.Panel1.ResumeLayout(False)
    Me.Panel_CovarianceMatrixSource.ResumeLayout(False)
    Me.Panel_CovarianceMatrixSource.PerformLayout()
    CType(Me.Grid_Covariance, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_Constraints.ResumeLayout(False)
    Me.SplitContainer1.Panel1.ResumeLayout(False)
    Me.SplitContainer1.Panel2.ResumeLayout(False)
    Me.SplitContainer1.ResumeLayout(False)
    Me.Panel5.ResumeLayout(False)
    CType(Me.Constraints_Grid_Defined, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel4.ResumeLayout(False)
    Me.Constraints_TabConstraints.ResumeLayout(False)
    Me.Tab_Template.ResumeLayout(False)
    CType(Me.Constraints_Grid_Template, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Tab_OptimisationIO.ResumeLayout(False)
    Me.Tab_OptimisationIO.PerformLayout()
    CType(Me.Grid_OptimisationResults, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel2.ResumeLayout(False)
    Me.Panel3.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
	Private WithEvents _MainForm As GenoaMain
	Private WithEvents MarginalsTimer As System.Windows.Forms.Timer

	' Timer Locals
	Private In_MarginalsTimer_Tick As Boolean
	Private _Update_MarginalsTimer As Boolean
	Private _Update_MarginalsTime As Date

	' Form ToolTip
	Private FormTooltip As New ToolTip()
	Dim FieldsMenu As ToolStripMenuItem = Nothing
  Dim ReportsMenu As ToolStripMenuItem = Nothing
  Dim CustomFieldsMenu As ToolStripMenuItem = Nothing

  Private Update_GroupToLoad As Boolean = False
  Private _Update_ChartsToUpdate As Boolean = False
  Private Update_ChartsToUpdateTime As Date

	Private GetVeniceHoldingsValueDate As Date
	Private GetVeniceHoldingsDatePicker As ToolStripControlHost
	Private GetVeniceHoldingsLookthroughCheckboxChecked As Boolean
	Private GetVeniceHoldingsLookthroughCheckbox As ToolStripControlHost

	' Form Constants, specific to the table being updated.

	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  Private GenericUpdateObject As RenaissanceGlobals.RenaissanceTimerUpdateClass

	' Form Locals, initialised on 'New' defining what standard data items to use
	Private THIS_TABLENAME As String
	Private THIS_ADAPTORNAME As String
	Private THIS_DATASETNAME As String


	' The standard ChangeID for this form. e.g. tblGroupList
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	Private THIS_FORM_SelectingCombo As ComboBox
	Private THIS_FORM_NewMoveToControl As Control

	' Form Specific Order fields
	Private THIS_FORM_SelectBy As String
	Private THIS_FORM_OrderBy As String

	Private THIS_FORM_ValueMember As String

	' Form specific Permissioning variables
	Private THIS_FORM_PermissionArea As String
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
	Private THIS_FORM_FormID As GenoaFormID

	' Data Structures

	Private myDataset As RenaissanceDataClass.DSGroupList			 ' Form Specific !!!!
	Private myTable As RenaissanceDataClass.DSGroupList.tblGroupListDataTable
	Private myConnection As SqlConnection
	Private myAdaptor As SqlDataAdapter

	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

	' Active Element.

	Private SortedRows() As DataRow
	Private SelectBySortedRows() As DataRow
	Private thisDataRow As RenaissanceDataClass.DSGroupList.tblGroupListRow		' Form Specific !!!!
	Private thisAuditID As Integer
	Private thisPosition As Integer
	Private _IsOverCancelButton As Boolean
	Private _InUse As Boolean
  Private InFormUpdate_Tick As Boolean

	Private SelectedGroupID As Integer = 0
	Private DSSelectedGroup As New RenaissanceDataClass.DSGroupsAggregated
	Private LoadedAggregatedGroupListID As Integer = 0

	Private PertracGridCombo As New RadC1GridCombo ' CustomC1Combo
	Private SectorCombo As New CustomC1Combo

	Private InstrumentsGridContextMenuStrip As ContextMenuStrip
	Private InstrumentsGridClickMouseRow As Integer = 0
	Private InstrumentsGridClickMouseCol As Integer = 0

	Private CurrentConstraintGroupID As Integer

  Private _DefaultStatsDatePeriod As DealingPeriod = DealingPeriod.Monthly

	' Form Status Flags

	Private FormIsValid As Boolean
	Private InstrumentsGridChanged As Boolean
	Private ConstrainsGridChanged As Boolean
	Private _FormOpenFailed As Boolean
	Private InPaint As Boolean
	Private Suppress_RiskAndReturn As Boolean

	' User Permission Flags

	Private HasReadPermission As Boolean
	Private HasUpdatePermission As Boolean
	Private HasInsertPermission As Boolean
	Private HasDeletePermission As Boolean

	' "Charts" Management Arrays

	Private RiskReturnChartArrayList As New ArrayList
	Private RiskReturnChartPoints As New ArrayList
	Private OptimisationArrayLists As New ArrayList	' Of ArrayList
  Private GroupChartArrayLists As New ArrayList ' Of ArrayList

	' Scaling factor hash table, used to pass to CovarianceGrid calculation function
  Private ScalingFactorHash As Dictionary(Of Integer, Double)

	' Dictionary of Integer keys and boolean values relating to Custom Field IDs. A 'True' value
	' indicates which Custom Fields are to appear on the results grid.

	Private Grid_CustomFields_Display_Dictionary As New Dictionary(Of Integer, Boolean)

	' Private CustomField_DataCache As New Dictionary(Of ULong, Object)

	Friend CustomFieldDataCache As CustomFieldDataCacheClass	' New

	' Temporary Grid for Marginals calculation

	Private Grid_CalculateMarginals As New C1.Win.C1FlexGrid.C1FlexGrid

	' Queue Object, Used as a store of unused Constraint Tabs

	Dim ConstraintsTabPageStack As New Stack(Of TabPage)
	Dim TemplateConstraintsTabPage As TabPage = Nothing
	Dim tbl_CurrentConstraints As DSGenoaConstraintItems.tblGenoaConstraintItemsDataTable = Nothing
	Dim tbl_CurrentConstraintsGroup As Integer = 0

	' Class for the management and calculation of Optimisation Constraint array
	' This class will also test constraints for the Marginals class.

	Private ConstraintWorker As ConstraintWorkerClass

	' This Form's Optimisation Object

	Private thisOptimiser As OptimiserClass

	' Cache object for 'Get positions from Venice' functionality
	Dim tblVeniceHoldings As DataTable = Nothing
	Dim tblVeniceHoldings_FundID As Integer = (-1)

	Private CalculateCapFromNewPercent As Boolean = False	' "GroupNewPercent" or "GroupPercent"

  ' Group ID to use for Marginals Drawdown calculations

  Private Marginals_Drawdown_DynamicGroupID As Integer
  Private Holding_DynamicGroupID As Integer
  Private NewHolding_DynamicGroupID As Integer

#End Region

#Region " Local Class and Enumeration Definitions"

	Private Enum TmpMarginalsCols As Integer
		PertracID = 0
		Holding = 1
		Trade = 2
		Weighting = 3
		InstrumentReturn = 4

		MAX_Count = 4
	End Enum

	Private Enum MarginalTradeType As Integer
		Buy = 1
		Sell = 2
	End Enum

	Private Class ConstraintFieldGridComparer
		Implements IComparer

		Public Function RowCompare(ByVal x As C1.Win.C1FlexGrid.Row, ByVal y As C1.Win.C1FlexGrid.Row) As Integer
			' ***********************************************************
			' Custom Comparer class designed to sort the Custom fields grid
			' on the Genoa Constraints Tab.
			'
			'It is Intended that fields that are Defined and Applied will appear at the top of
			' the grid, rows then being sorted by field Name.
			'
			' ***********************************************************

			Try
				If (CBool(x.Item("Col_IsDefined")) = CBool(y.Item("Col_IsDefined"))) Then
					' If (CBool(x.Item("Col_IsApplied")) = CBool(y.Item("Col_IsApplied"))) Then
					Return CStr(x.Item("Col_CustomName")).CompareTo(CStr(y.Item("Col_CustomName")))
					'Else
					'	If (CBool(x.Item("Col_IsApplied"))) Then
					'		Return (-1)
					'	Else
					'		Return 1
					'	End If
					'End If
				Else
					If (CBool(x.Item("Col_IsDefined"))) Then
						Return (-1)
					Else
						Return 1
					End If
				End If
			Catch ex As Exception
			End Try

			Return 0
		End Function

		Public Function Compare(ByVal x As Object, ByVal y As Object) As Integer Implements System.Collections.IComparer.Compare
			Try
				Return RowCompare(x, y)
			Catch ex As Exception
				Return 0
			End Try

		End Function

	End Class

	Friend Class MarginalsGridDetails

		Public GridClickMouseRow As Integer
		Public GridClickMouseCol As Integer

		Public Sub New()
			GridClickMouseRow = 0
			GridClickMouseCol = 0
		End Sub

	End Class

	Friend Class ConstraintsTabDetails
		Public ConstraintsGroupID As Integer
		Public CustomFieldID As Integer
		Public CustomFieldDataType As Integer
		Public TabText As String
		Public ConstraintsGrid As C1.Win.C1FlexGrid.C1FlexGrid
		Public GridClickMouseRow As Integer
		Public GridClickMouseCol As Integer

		Public Sub New()
			ConstraintsGroupID = 0
			CustomFieldID = 0
			CustomFieldDataType = RenaissanceDataType.NumericType
			TabText = ""
			GridClickMouseRow = 0
			GridClickMouseCol = 0
			ConstraintsGrid = Nothing
		End Sub

		Public Sub New(ByVal pConstraintsGroupID As Integer, ByVal pCustomFieldID As Integer, ByVal pCustomFieldDataType As Integer, ByVal pTabText As String, ByVal pConstraintsGrid As C1.Win.C1FlexGrid.C1FlexGrid)
			Me.New()

			Me.ConstraintsGroupID = pConstraintsGroupID
			Me.CustomFieldID = pCustomFieldID
			Me.CustomFieldDataType = pCustomFieldDataType
			Me.TabText = pTabText
			Me.ConstraintsGrid = pConstraintsGrid
		End Sub

	End Class

	Private Class MarginalTrade

		Private _PertracID As Integer
		Private _ContraID As Integer
		Private _TradeSize As Double
		Private _TradeDirection As String

		Private Sub New()
		End Sub

		Public Sub New(ByVal pPertracID As Integer, ByVal pContraID As Integer, ByVal pTradeSize As Double, ByVal pTradeDirection As String)

			Try
				_PertracID = pPertracID
				_ContraID = pContraID
				_TradeSize = pTradeSize
				_TradeDirection = pTradeDirection
			Catch ex As Exception
			End Try

		End Sub

		Public Sub New(ByRef pMarginalsRow As C1.Win.C1FlexGrid.Row)

			Try
				If (pMarginalsRow IsNot Nothing) Then
					_PertracID = CInt(pMarginalsRow("PertracID"))
					_ContraID = CInt(pMarginalsRow("ContraID"))
					_TradeSize = CDbl(pMarginalsRow("TradeAmount"))
					_TradeDirection = CStr(pMarginalsRow("BuySell"))
				End If
			Catch ex As Exception
			End Try

		End Sub

		Public ReadOnly Property PertracID() As Integer
			Get
				Return _PertracID
			End Get
		End Property

		Public ReadOnly Property ContraID() As Integer
			Get
				Return _ContraID
			End Get
		End Property

		Public ReadOnly Property TradeSize() As Double
			Get
				Return _TradeSize
			End Get
		End Property

		Public ReadOnly Property TradeDirection() As String
			Get
				Return _TradeDirection
			End Get
		End Property

		Public ReadOnly Property PertracTrade() As Double
			Get
				If (_TradeDirection.ToUpper.Trim.StartsWith("B")) Then
					' Buy

					Return (_TradeSize)
				Else
					' Sell

					Return (_TradeSize * (-1.0#))
				End If
			End Get
		End Property

		Public ReadOnly Property ContraTrade() As Double
			Get
				If (_TradeDirection.ToUpper.Trim.StartsWith("B")) Then
					' Buy

					Return (_TradeSize * (-1.0#))
				Else
					' Sell

					Return (_TradeSize)
				End If
			End Get
		End Property

	End Class

	Private Class DecimalPoint

		Public X As Double
		Public Y As Double

		Public Sub New(ByVal pX As Double, ByVal pY As Double)
			X = pX
			Y = pY
		End Sub

	End Class

#End Region

#Region " Form 'Properties' "

	Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

	Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return _IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			_IsOverCancelButton = Value
		End Set
	End Property

	Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

	Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
		Get
			Return _InUse
		End Get
	End Property

	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

	Private Property UpdateMarginalsFlag() As Boolean
		Get
			Return _Update_MarginalsTimer
		End Get
		Set(ByVal value As Boolean)
			If value Then
				_Update_MarginalsTime = Now
				_Update_MarginalsTimer = True
				If (MarginalsTimer.Interval <> 200) Then
					MarginalsTimer.Interval = 200
				End If
			End If
		End Set
	End Property

  Public Property DefaultStatsDatePeriod() As DealingPeriod
    Get
      Return _DefaultStatsDatePeriod
    End Get
    Set(ByVal value As DealingPeriod)
      _DefaultStatsDatePeriod = value

      StatusStrip_GroupReturns.Text = "Data Period set to " & _DefaultStatsDatePeriod.ToString
      StatusLabel_Optimisation.Text = "Data Period set to " & _DefaultStatsDatePeriod.ToString
    End Set
  End Property

  Private Property Update_ChartsToUpdate() As Boolean
    Get
      Return _Update_ChartsToUpdate
    End Get
    Set(ByVal value As Boolean)
      _Update_ChartsToUpdate = value

      If value Then
        Update_ChartsToUpdateTime = Now()
      End If
    End Set
  End Property
#End Region

	Public Sub New(ByVal pMainForm As GenoaMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		If (Me.IsDisposed) Then Exit Sub

		_MainForm = pMainForm
		AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True
    DefaultStatsDatePeriod = MainForm.DefaultStatsDatePeriod

		' ******************************************************
		' Set pointer to Custom Field Data Cache
		' ******************************************************

		CustomFieldDataCache = MainForm.CustomFieldDataCache

		' ******************************************************
		' Initialise new Constraint Worker
		' ******************************************************

		ConstraintWorker = New ConstraintWorkerClass(Me)

		' ******************************************************
		' Set Optimiser instance
		' ******************************************************

		thisOptimiser = New OptimiserClass()

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectGroupID

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "GroupListName"
		THIS_FORM_OrderBy = "GroupListName"

		THIS_FORM_ValueMember = "GroupListID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = GenoaFormID.frmOptimise

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblGroupList	 ' This Defines the Form Data !!! 

		PertracGridCombo.MasternameCollection = MainForm.MasternameDictionary

		' MainForm.SetTblGenericCombo(PertracGridCombo, RenaissanceGlobals.RenaissanceStandardDatasets.Mastername, "Mastername", "ID", "", False, True, True, 0, "")

		'AddHandler PertracGridCombo.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler PertracGridCombo.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		'AddHandler PertracGridCombo.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		'AddHandler PertracGridCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		' Form Control Changed events

		AddHandler Combo_SelectGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_SelectGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_SelectGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_SelectGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_SelectGroupID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_SelectGroupID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_SelectGroupID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_SelectGroupID.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged

    'AddHandler Combo_BenckmarkIndex.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_BenckmarkIndex.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    'AddHandler Combo_BenckmarkIndex.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    'AddHandler Combo_BenckmarkIndex.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    'AddHandler Combo_MarginalIndex.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_MarginalIndex.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    'AddHandler Combo_MarginalIndex.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    'AddHandler Combo_MarginalIndex.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler Constraints_ComboConstraintsGoup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		'AddHandler Constraints_ComboConstraintsGoup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Constraints_ComboConstraintsGoup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Constraints_ComboConstraintsGoup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		' Marginals Tab, Control events

		AddHandler Edit_Marginals_TradeSize.ValueChanged, AddressOf MarginalsControlChanged
		AddHandler Check_Marginals_Buy.CheckedChanged, AddressOf MarginalsControlChanged
		AddHandler Check_Marginals_Sell.CheckedChanged, AddressOf MarginalsControlChanged
		AddHandler Check_Marginals_SellAll.CheckedChanged, AddressOf MarginalsControlChanged
		AddHandler Check_Marginals_ContraTrade.CheckedChanged, AddressOf MarginalsControlChanged
		AddHandler Check_Marginals_SubstituteTradesOnly.CheckedChanged, AddressOf MarginalsControlChanged
		AddHandler Combo_Marginals_ContraInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_Marginals_ContraInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_Marginals_ContraInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_Marginals_ContraInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
		AddHandler Combo_Marginals_ContraInstrument.SelectedIndexChanged, AddressOf MarginalsControlChanged
    AddHandler Combo_BenckmarkIndex.SelectedIndexChanged, AddressOf MarginalsControlChanged
    AddHandler Radio_ImproveRisk.CheckedChanged, AddressOf MarginalsControlChanged
		AddHandler Radio_ImproveReturn.CheckedChanged, AddressOf MarginalsControlChanged
		AddHandler Radio_ImproveSharpeRatio.CheckedChanged, AddressOf MarginalsControlChanged
		AddHandler Radio_ImproveTargetRiskReturn.CheckedChanged, AddressOf MarginalsControlChanged
    AddHandler Radio_ImproveWorstDrawDown.CheckedChanged, AddressOf MarginalsControlChanged
    AddHandler Radio_ImproveLeastSquares.CheckedChanged, AddressOf MarginalsControlChanged
    AddHandler Radio_ImproveStdError.CheckedChanged, AddressOf MarginalsControlChanged

    AddHandler Edit_Marginals_RiskLimit.ValueChanged, AddressOf MarginalsControlChanged
		AddHandler Edit_Marginals_ReturnTarget.ValueChanged, AddressOf MarginalsControlChanged
    AddHandler Edit_Marginals_RiskFreeRate.ValueChanged, AddressOf MarginalsControlChanged
    AddHandler Edit_Marginals_AnalysisPeriod.ValueChanged, AddressOf MarginalsControlChanged

		AddHandler Check_Marginals_ApplyBounds.CheckedChanged, AddressOf MarginalsControlChanged
		AddHandler Check_Marginals_ExcludeInstrumentBreaks.CheckedChanged, AddressOf MarginalsControlChanged
		AddHandler Check_Marginals_ExcludeSectorBreaks.CheckedChanged, AddressOf MarginalsControlChanged
		AddHandler Check_Marginals_ExcludeLiquidityBreaks.CheckedChanged, AddressOf MarginalsControlChanged
		AddHandler Check_Marginals_ExcludeCustomLimitBreaks.CheckedChanged, AddressOf MarginalsControlChanged
    AddHandler Check_Marginals_AnalysisPeriod.CheckedChanged, AddressOf MarginalsControlChanged



		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(Genoa_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, Genoa_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		' NPP 7 Sep 2007
		' DSSelectedGroup = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupsAggregated, False)

		MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)
		Me.RootMenu.PerformLayout()

		GetVeniceHoldingsValueDate = RenaissanceGlobals.Globals.Renaissance_BaseDate
		GetVeniceHoldingsDatePicker = New ToolStripControlHost(GetNewCmsDateTimePicker)
		GetVeniceHoldingsLookthroughCheckboxChecked = False
		GetVeniceHoldingsLookthroughCheckbox = New ToolStripControlHost(GetNewCmsCheckbox)

		Try
			InPaint = True

			' Initialise Tabs

			Call InitialiseInstrumentsGrid()
			Call InitialiseCovarianceGrid()
			Call InitialiseMarginalsGrid()
			Call InitialiseOptimisationReturnsGrid()

			InstrumentsGridContextMenuStrip = New ContextMenuStrip
			AddHandler InstrumentsGridContextMenuStrip.Opening, AddressOf cms_Opening_ReturnsGrid
			InstrumentsGridContextMenuStrip.Items.Add(New ToolStripMenuItem(" "))
			Grid_Returns.ContextMenuStrip = InstrumentsGridContextMenuStrip

			Call SetGroupCombo()
			Call SetBenchmarkIndexCombo()

			' Initialise Covariance Tab

			Call Set_ListCovarianceMatrix()

		Catch ex As Exception
		Finally
			InPaint = False
			Suppress_RiskAndReturn = False
		End Try

		RiskReturnChartArrayList.Add(Chart_Results)

    Marginals_Drawdown_DynamicGroupID = MainForm.UniqueGenoaNumber
    Holding_DynamicGroupID = MainForm.UniqueGenoaNumber
    NewHolding_DynamicGroupID = MainForm.UniqueGenoaNumber

		' 
    ReportsMenu = SetGroupReportingMenu(RootMenu)
    FieldsMenu = SetFieldSelectMenu(RootMenu)
		CustomFieldsMenu = SetCustomFieldSelectMenu(RootMenu)

		' *********************************************
		' Set 'Copy' Menus for the form charts
		' *********************************************

		MainForm.AddCopyMenuToChart(Chart_VAMI)
		MainForm.AddCopyMenuToChart(Chart_MonthlyReturns)
		MainForm.AddCopyMenuToChart(Chart_Results)

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
	Public Sub ResetForm() Implements StandardGenoaForm.ResetForm
		THIS_FORM_SelectBy = "GroupListName"
		THIS_FORM_OrderBy = "GroupListName"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

	Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
    ' ***************************************************************************************
    '
    '
    ' ***************************************************************************************

    Try
      Me.Cursor = Cursors.WaitCursor

      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
      _FormOpenFailed = False
      _InUse = True

      ' Initialise Timer

      If (MarginalsTimer IsNot Nothing) Then
        Try
          MarginalsTimer.Stop()
        Catch ex As Exception
        Finally
          MarginalsTimer = Nothing
        End Try
      End If
      Try
        MarginalsTimer = New System.Windows.Forms.Timer
        _Update_MarginalsTimer = False
        In_MarginalsTimer_Tick = False
        MarginalsTimer.Interval = 200

        AddHandler MarginalsTimer.Tick, AddressOf MarginalsTimer_Tick
        MarginalsTimer.Start()
      Catch ex As Exception
      End Try

      ' Initialise Data structures. Connection, Adaptor and Dataset.

      ConstraintWorker.InvalidateInstruments()
      ConstraintWorker.InvalidateConstraintDetails()
      ConstraintWorker.InvalidateCustomFieldData()

      If Not (MainForm Is Nothing) Then
        FormIsValid = True
      Else
        MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      If (myConnection Is Nothing) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      If (myAdaptor Is Nothing) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      If (myDataset Is Nothing) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      ' Initialse form

      InPaint = True
      IsOverCancelButton = False

      GenericUpdateObject = MainForm.AddFormUpdate(Me, AddressOf FormUpdate_Tick)

      ' Check User permissions
      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

        FormIsValid = False
        _FormOpenFailed = True
        Exit Sub
      End If

      ' Build Sorted data list from which this form operates
      If (Combo_SelectGroup.Items.Count > 0) Then
        Combo_SelectGroup.SelectedIndex = 0
      End If

      ' Initialise Instruments / Returns Grid.
      ResetInstrumentsGrid()
      ResetMarginalsGrid()

      ' Initialise Constraints TabControl

      If (TemplateConstraintsTabPage Is Nothing) Then
        If (Constraints_TabConstraints.TabPages.ContainsKey("Tab_Template")) Then
          TemplateConstraintsTabPage = Constraints_TabConstraints.TabPages("Tab_Template")
          Constraints_TabConstraints.TabPages.Remove(TemplateConstraintsTabPage)
        End If
      End If

      If Constraints_TabConstraints.TabPages.Count > 0 Then
        While Constraints_TabConstraints.TabPages.Count > 0
          If Not ConstraintsTabPageStack.Contains(Constraints_TabConstraints.TabPages(0)) Then
            ConstraintsTabPageStack.Push(Constraints_TabConstraints.TabPages(0))
          End If

          Constraints_TabConstraints.TabPages.RemoveAt(0)
        End While
      End If

      Call SetSortedRows()

			InstrumentsGridChanged = False
			ConstrainsGridChanged = False

      ' General :


      ' Instruments :

      If Combo_BenckmarkIndex.Items.Count > 0 Then
        Combo_BenckmarkIndex.SelectedIndex = 0
      End If

      ' Marginals :

      If (Not Radio_ImproveReturn.Checked) Then
        Radio_ImproveReturn.Checked = True
      End If

      If Combo_MarginalIndex.Items.Count > 0 Then
        Combo_MarginalIndex.SelectedIndex = 0
      End If

      Check_Marginals_Buy.Checked = True
      Check_Marginals_Sell.Checked = True
      Check_Marginals_SellAll.Checked = False
      Edit_Marginals_TradeSize.Value = 0
      Edit_Marginals_ReturnTarget.Value = 0
      Edit_Marginals_RiskLimit.Value = 0

      ' Results 

      Results_Radio_TargetVolatility.Checked = True

      ' Constraints
      ' Set TemplateConstraintsTabPage, is becomes the pre-defined Template Tab page, which is then removed
      ' from the control.

      Call SetConstraintSetsCombo()

      ' Covariance :

      Check_Covariance_BackfillData.Checked = False
      If (Radio_Covariance_UseLiveMatrix.Checked) Then
        Radio_Covariance_UseLiveMatrix_CheckedChanged(Radio_Covariance_UseLiveMatrix, New EventArgs)
      Else
        Radio_Covariance_UseLiveMatrix.Checked = True
      End If

      ' Results

      FormatPointChart(Me.Chart_Results)

      ' End. 

      ' Display initial record.

      thisPosition = 0
      If THIS_FORM_SelectingCombo.Items.Count > 0 Then
        Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
        thisDataRow = SortedRows(thisPosition)
        Call LoadInstrumentGrid(thisDataRow)
      Else
        Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
        Call LoadInstrumentGrid(Nothing)
      End If

      ClearResultsCharts()
      Call Split_GroupReturns_Resize(Nothing, New EventArgs)

    Catch ex As Exception
    Finally
      InPaint = False
      Me.Cursor = Cursors.Default
    End Try

    Results_Radio_Volatility.Checked = True
    Results_Radio_vsNewHolding.Checked = True


  End Sub

	Private Sub frmEntity_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		' ***************************************************************************************
		'
		'
		' ***************************************************************************************

		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False
    MainForm.RemoveFormUpdate(Me) ' Remove Form Update reference, will be re-established in Load() if this form is cached.

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (InstrumentsGridChanged) Then
				Call SaveInstrumentGrid()
			End If
			If (ConstrainsGridChanged) Then
				Call SaveConstraintsGrid()
			End If

			HideForm = True
			If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
			Catch ex As Exception
			End Try

			Try
				RemoveHandler PertracGridCombo.SelectedValueChanged, AddressOf Me.IndexComboChanged
				'RemoveHandler PertracGridCombo.LostFocus, AddressOf Me.Grid_Returns_LostFocus
			Catch ex As Exception
			End Try

			Try
				Dim ThisPage As TabPage

				While (ConstraintsTabPageStack.Count > 0)
					ThisPage = ConstraintsTabPageStack.Pop

					If (ThisPage IsNot Nothing) Then
						DestructConstraintsTabPage(ThisPage)
					End If
				End While

				ConstraintsTabPageStack.Clear()

				While (Constraints_TabConstraints.TabPages.Count > 0)
					ThisPage = Constraints_TabConstraints.TabPages(0)
					Constraints_TabConstraints.TabPages.RemoveAt(0)

					If (ThisPage IsNot Nothing) Then
						DestructConstraintsTabPage(ThisPage)
					End If
				End While
			Catch ex As Exception
			End Try

			Try
				TemplateConstraintsTabPage = Nothing
			Catch ex As Exception
			End Try

			Try
				' Form Control Changed events

				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
				RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_SelectGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_SelectGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_SelectGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_SelectGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_SelectGroupID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_SelectGroupID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_SelectGroupID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_SelectGroupID.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        'RemoveHandler Combo_BenckmarkIndex.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_BenckmarkIndex.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        'RemoveHandler Combo_BenckmarkIndex.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        'RemoveHandler Combo_BenckmarkIndex.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        'RemoveHandler Combo_MarginalIndex.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_MarginalIndex.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        'RemoveHandler Combo_MarginalIndex.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        'RemoveHandler Combo_MarginalIndex.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Constraints_ComboConstraintsGoup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Constraints_ComboConstraintsGoup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Constraints_ComboConstraintsGoup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Constraints_ComboConstraintsGoup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				'RemoveHandler PertracGridCombo.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler PertracGridCombo.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				'RemoveHandler PertracGridCombo.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				'RemoveHandler PertracGridCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Edit_Marginals_TradeSize.ValueChanged, AddressOf MarginalsControlChanged
				RemoveHandler Check_Marginals_Buy.CheckedChanged, AddressOf MarginalsControlChanged
				RemoveHandler Check_Marginals_Sell.CheckedChanged, AddressOf MarginalsControlChanged
				RemoveHandler Check_Marginals_SellAll.CheckedChanged, AddressOf MarginalsControlChanged
				RemoveHandler Check_Marginals_ContraTrade.CheckedChanged, AddressOf MarginalsControlChanged
				RemoveHandler Check_Marginals_SubstituteTradesOnly.CheckedChanged, AddressOf MarginalsControlChanged
				RemoveHandler Combo_Marginals_ContraInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_Marginals_ContraInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_Marginals_ContraInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_Marginals_ContraInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				RemoveHandler Combo_Marginals_ContraInstrument.SelectedIndexChanged, AddressOf MarginalsControlChanged
        RemoveHandler Combo_BenckmarkIndex.SelectedIndexChanged, AddressOf MarginalsControlChanged
        RemoveHandler Radio_ImproveRisk.CheckedChanged, AddressOf MarginalsControlChanged
				RemoveHandler Radio_ImproveReturn.CheckedChanged, AddressOf MarginalsControlChanged
				RemoveHandler Radio_ImproveSharpeRatio.CheckedChanged, AddressOf MarginalsControlChanged
				RemoveHandler Radio_ImproveTargetRiskReturn.CheckedChanged, AddressOf MarginalsControlChanged
				RemoveHandler Edit_Marginals_RiskLimit.ValueChanged, AddressOf MarginalsControlChanged
				RemoveHandler Edit_Marginals_ReturnTarget.ValueChanged, AddressOf MarginalsControlChanged
				RemoveHandler Edit_Marginals_RiskFreeRate.ValueChanged, AddressOf MarginalsControlChanged
				RemoveHandler Check_Marginals_ApplyBounds.CheckedChanged, AddressOf MarginalsControlChanged
				RemoveHandler Check_Marginals_ExcludeInstrumentBreaks.CheckedChanged, AddressOf MarginalsControlChanged
				RemoveHandler Check_Marginals_ExcludeSectorBreaks.CheckedChanged, AddressOf MarginalsControlChanged
				RemoveHandler Check_Marginals_ExcludeLiquidityBreaks.CheckedChanged, AddressOf MarginalsControlChanged
				RemoveHandler Check_Marginals_ExcludeCustomLimitBreaks.CheckedChanged, AddressOf MarginalsControlChanged

			Catch ex As Exception
			End Try


		End If

	End Sub

  Private Function FormUpdate_Tick() As Boolean
    ' *******************************************************************************
    '
    ' Callback process for the Generic Form Update Process.
    '
    ' Function MUST return True / False on Update.
    ' True will clear the update request. False will not.
    '
    ' Motivation :
    ' Sometimes, when charting parameters may be quickly updated, one wants to defer
    ' the chart update until the update has finished.
    '
    ' *******************************************************************************
    Dim RVal As Boolean = False

    Try

      If (Me.IsDisposed) Then
        Return True
      End If

      If (InFormUpdate_Tick) OrElse (Not _InUse) Then
        Return False
        Exit Function
      End If

    Catch ex As Exception
    End Try

    Try
      InFormUpdate_Tick = True

      ' Update Form :

      ' Don't react to changes made in paint routines etc.
      If Not InPaint Then

        If (Update_GroupToLoad) Then
          Try
            Update_GroupToLoad = False

            If (InstrumentsGridChanged = True) Then
              Call SaveInstrumentGrid()
            End If
            If (ConstrainsGridChanged = True) Then
              Call SaveConstraintsGrid()

            End If

            ' Find the correct data row, then show it...
            thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

            ' Double check
            If thisPosition > 0 Then
              THIS_FORM_SelectingCombo.Tag = thisPosition
            End If

            If (thisPosition >= 0) Then
              thisDataRow = Me.SortedRows(thisPosition)
            Else
              thisDataRow = Nothing
            End If

            Try
              Me.Cursor = Cursors.WaitCursor

              Grid_Returns.Sort(C1.Win.C1FlexGrid.SortFlags.Ascending, -1)
              Call LoadInstrumentGrid(thisDataRow)

              ClearResultsCharts()

            Catch ex As Exception
              MainForm.LogError(Me.Name & ", FormUpdate_Tick", LOG_LEVELS.Error, "", ex.Message, ex.StackTrace)
            Finally
              Me.Cursor = Cursors.Default
            End Try

          Catch ex As Exception
          End Try

          RVal = True

        ElseIf (Update_ChartsToUpdate) Then
          If (Now() - Update_ChartsToUpdateTime).TotalSeconds >= 1.0# Then

            Update_ChartsToUpdate = False

            Try
              Me.Cursor = Cursors.WaitCursor

              Call Update_Group_Percentages(True)
              Call Update_Group_RiskAndReturn(True)
              Call UpdateMarginalsGrid()
              Call CalculateConstraintExposures()


            Catch ex As Exception
              MainForm.LogError(Me.Name & ", FormUpdate_Tick", LOG_LEVELS.Error, "", ex.Message, ex.StackTrace)
            Finally
              RVal = True
              Me.Cursor = Cursors.Default
            End Try

          Else

            RVal = False
          End If

        End If
      End If


    Catch ex As Exception
      MainForm.LogError(Me.Name & ", FormUpdate_Tick", LOG_LEVELS.Error, "", ex.Message, ex.StackTrace)
      RVal = False
    Finally
      InFormUpdate_Tick = False
    End Try

    Return RVal

  End Function

	Private Sub frmOptimise_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.VisibleChanged
		' ***************************************************************************************************
		'
		'
		' ***************************************************************************************************

		Try
			If (MarginalsTimer IsNot Nothing) AndAlso (Me.Created) AndAlso (Not Me.IsDisposed) Then
				If (Me.Visible) Then
					MarginalsTimer.Start()
				Else
					MarginalsTimer.Stop()
				End If
			End If
		Catch ex As Exception
		End Try

	End Sub

	Private Sub MarginalsTimer_Tick(ByVal Sender As Object, ByVal e As EventArgs)
		' *******************************************************************************
		' Tick event handler to control update of Marginals.
		'
		' *******************************************************************************

		If (Me.IsDisposed) Then
			Try
				MarginalsTimer.Stop()
				Exit Sub
			Catch ex As Exception
			End Try
		End If

		' Alter update period depending on whether or not this form is the active form.

		If (ActiveForm Is Nothing) OrElse (ActiveForm IsNot Me) Then
			If (MarginalsTimer.Interval < 5000) Then
				MarginalsTimer.Interval = 5000
			End If
		Else
			If (MarginalsTimer.Interval > 200) Then
				MarginalsTimer.Interval = 200
			End If
		End If

		' No Update required or already running ?
		Try
			If (In_MarginalsTimer_Tick) OrElse (Not _Update_MarginalsTimer) Then
				Exit Sub
			End If
		Catch ex As Exception
		End Try

		' Sufficient Time elapsed since update request?
		If (Now - _Update_MarginalsTime).TotalSeconds < 0.2 Then
			Exit Sub
		End If


		' OK, run Marginals process as appropriate.

		Try
			_Update_MarginalsTimer = False

			In_MarginalsTimer_Tick = True

			' Grid_Marginals.DataSource = CalculateMarginals(Grid_Marginals.DataSource)
			Try
				If (Grid_Marginals.DataSource Is Nothing) Then
					Call InitialiseMarginalsGrid()
					Call ResetMarginalsGrid()
				End If
			Catch ex As NullReferenceException
				Call InitialiseMarginalsGrid()
				Call ResetMarginalsGrid()
			Catch ex As Exception
			End Try

			CalculateMarginals(Grid_Marginals.DataSource)
			Grid_Marginals.Cols("ContraID").Visible = False
			FormatMarginalsGridRows()

		Catch ex As Exception
		Finally
			In_MarginalsTimer_Tick = False
		End Try

	End Sub

#End Region

	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		' Routine to handle changes / updates to tables by this and other windows.
		' If this, or any other, form posts a change to a table, then it will invoke an update event 
		' detailing what tables have been altered.
		' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
		' the 'VeniceAutoUpdate' event of the main Venice form.
		' Each form may them react as appropriate to changes in any table that might impact it.
		'
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint


		Try
			InPaint = True

			KnowledgeDateChanged = False
			SetButtonStatus_Flag = False

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
				KnowledgeDateChanged = True
				RefreshForm = True
			End If

			' ****************************************************************
			' Check for changes relevant to this form
			' ****************************************************************


			' Changes to the KnowledgeDate :-
			If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
				SetButtonStatus_Flag = True
			End If

			' Changes to the tblUserPermissions table :-
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

				' Check ongoing permissions.

				Call CheckPermissions()
				If (HasReadPermission = False) Then
					Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

					FormIsValid = False
					Me.Close()
					Exit Sub
				End If

				SetButtonStatus_Flag = True

			End If

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList) = True) Or KnowledgeDateChanged Then
				Call SetGroupCombo()
			End If

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupItems) = True) Or KnowledgeDateChanged Then
				ConstraintWorker.InvalidateInstruments()
				RefreshForm = True
			End If

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupItemData) = True) Or KnowledgeDateChanged Then
				RefreshForm = True
			End If

			' tblCovarianceList
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCovarianceList) = True) Or KnowledgeDateChanged Then
				Call Set_ListCovarianceMatrix()

				If (Radio_Covariance_UseSavedMatrix.Checked) Then
					Call List_CovarianceMatrices_SelectedIndexChanged(Nothing, New EventArgs)
				End If
			End If

			' tblGenoaConstraintList
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGenoaConstraintList) = True) Or KnowledgeDateChanged Then
				Call SetConstraintSetsCombo()

				ConstraintWorker.InvalidateConstraintDetails()
			End If

      ' Venice tblTransaction - Used for the Venice Holdings cache.

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Or KnowledgeDateChanged Then
				If (tblVeniceHoldings IsNot Nothing) Then
					Try
						tblVeniceHoldings.Clear()
					Catch ex As Exception
					Finally
						tblVeniceHoldings = Nothing
					End Try
				End If
			End If

			' MasterName
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Mastername) = True) OrElse _
             (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCTA_Simulation)) Then

        PertracGridCombo.MasternameCollection = MainForm.MasternameDictionary

        ' MainForm.SetTblGenericCombo(PertracGridCombo, RenaissanceGlobals.RenaissanceStandardDatasets.Mastername, "Mastername", "ID", "", False, True, True, 0, "")
      End If

      ' Performance (Pertrac)

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Performance)) OrElse _
          (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCTA_Simulation)) Then

        If (Radio_Covariance_UseLiveMatrix.Checked) Then
          CalculateLiveCovarianceMatrix(DefaultStatsDatePeriod)
          UpdateMarginalsGrid()
        End If

        Try
          ' Update charts.

          Dim ThisPertracID As Integer
          Dim ThisScalingFactor As Double
          Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
          Dim Col_GroupScalingFactor As Integer = Grid_Returns.Cols("GroupScalingFactor").SafeIndex
          Dim ThisDatePeriod As DealingPeriod

          ThisPertracID = Grid_Returns.Item(Grid_Returns.Row, Col_GroupPertracCode)
          ThisScalingFactor = CDbl(Grid_Returns.Item(Grid_Returns.Row, Col_GroupScalingFactor))

          ' ThisSeries = Chart_VAMI.Series.Count - 1
          ThisDatePeriod = MainForm.PertracData.GetPertracDataPeriod(ThisPertracID)

          Set_LineChart(MainForm, ThisDatePeriod, ThisPertracID, Chart_VAMI, 0, "", MainForm.StatFunctions.AnnualPeriodCount(ThisDatePeriod), 1, ThisScalingFactor, Now.AddMonths(-36), Now())
          Set_LineChart_RateReturnsLine(MainForm, Chart_VAMI, 1, 0, CDbl(Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols("GroupExpectedReturn").SafeIndex)), MainForm.StatFunctions.AnnualPeriodCount(ThisDatePeriod), "")

          'ThisSeries = Chart_MonthlyReturns.Series.Count - 1

          Set_RollingReturnChart(MainForm, ThisDatePeriod, ThisPertracID, Chart_MonthlyReturns, 0, "", MainForm.StatFunctions.AnnualPeriodCount(ThisDatePeriod), 1, ThisScalingFactor, Now.AddMonths(-36), Now())
          Set_LineChart_StaticReturnsLine(MainForm, Chart_MonthlyReturns, 1, 0, CDbl(Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols("GroupExpectedReturn").SafeIndex)) * 100, "")
        Catch ex As Exception
        End Try

      End If

			' Check to see if the local AggregatedGroups table needs to refresh.

      Try
        Dim ThisChangeID As RenaissanceChangeID

        For Each ThisChangeID In e.TablesChanged
          If RenaissanceStandardDatasets.tblGroupsAggregated.ISChangeIDToNote(ThisChangeID) Then
            LoadedAggregatedGroupListID = 0
            Exit For
          ElseIf RenaissanceStandardDatasets.tblGroupsAggregated.IsChangeIDToTriggerUpdate(ThisChangeID) Then
            LoadedAggregatedGroupListID = 0
            Exit For
          End If
        Next
      Catch ex As Exception
      End Try

			' tblGenoaConstraintItems

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGenoaConstraintItems) = True) Or KnowledgeDateChanged Then
        Try
          If (tbl_CurrentConstraints IsNot Nothing) Then
            SyncLock tbl_CurrentConstraints
              tbl_CurrentConstraints.Clear()
            End SyncLock
          Else
            tbl_CurrentConstraints = New DSGenoaConstraintItems.tblGenoaConstraintItemsDataTable
          End If
        Catch ex As Exception
        End Try
        tbl_CurrentConstraintsGroup = 0

        Try
          If (ConstrainsGridChanged = False) Then
            If (Me.Constraints_ComboConstraintsGoup.SelectedIndex > 0) AndAlso (IsNumeric(Constraints_ComboConstraintsGoup.SelectedValue)) Then
              LoadConstraintsGroup(CInt(Constraints_ComboConstraintsGoup.SelectedValue), True, False)
            End If
          End If
        Catch ex As Exception
        End Try

        ConstraintWorker.InvalidateConstraintDetails()
      End If

			' tblPertracCustomFields

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPertracCustomFields) = True) Or KnowledgeDateChanged Then
				Dim ExistingSelectedFieldIDs() As Integer
				Dim FieldKeys() As Integer

				' Update Grid_CustomFields_Display_Dictionary for new or removed Custom fields

				If (Grid_CustomFields_Display_Dictionary.Count > 0) Then
					Dim FieldCounter As Integer

					ReDim FieldKeys(Grid_CustomFields_Display_Dictionary.Count - 1)
					ReDim ExistingSelectedFieldIDs(Grid_CustomFields_Display_Dictionary.Count - 1)

					Grid_CustomFields_Display_Dictionary.Keys.CopyTo(FieldKeys, 0)

					For FieldCounter = 0 To (FieldKeys.Length - 1)
						ExistingSelectedFieldIDs(FieldCounter) = (-1)

						If Grid_CustomFields_Display_Dictionary(FieldKeys(FieldCounter)) Then
							ExistingSelectedFieldIDs(FieldCounter) = FieldKeys(FieldCounter)
						End If
					Next

					Grid_CustomFields_Display_Dictionary.Clear()
					RootMenu.Items.Remove(CustomFieldsMenu)
					CustomFieldsMenu = SetCustomFieldSelectMenu(RootMenu)

					For FieldCounter = 0 To (ExistingSelectedFieldIDs.Length - 1)
						If (ExistingSelectedFieldIDs(FieldCounter) >= 0) Then
							SetCustomMenuChecked(ExistingSelectedFieldIDs(FieldCounter).ToString)
						End If
					Next

				Else
					Grid_CustomFields_Display_Dictionary.Clear()
					RootMenu.Items.Remove(CustomFieldsMenu)
					CustomFieldsMenu = SetCustomFieldSelectMenu(RootMenu)
				End If

				' Update Constraints Field-Select Grid

				Call SetConstraints_FieldSelectGrid()
			End If

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPertracCustomFieldData) = True) Or KnowledgeDateChanged Then

				' Cache object is now cleared in the Mainform.Main_RaiseEvent procedure.
				' CustomField_DataCache.Clear()

				If (InstrumentsGridChanged = False) Then
					Dim ColCount As Integer

					For ColCount = 2 To (Grid_Returns.Cols.Count - 1)
						If (Grid_Returns.Cols(ColCount).Name.StartsWith("Custom_")) Then
							PaintCustomFieldColumn(Grid_Returns.Cols(ColCount).Name)

							Application.DoEvents()
						End If
					Next
				End If

				ConstraintWorker.InvalidateCustomFieldData()

			End If

			' ****************************************************************
			' Changes to the Main FORM table :-
			' ****************************************************************

			If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then
				RefreshForm = True

				' Re-Set Controls etc.
				Call SetSortedRows()

				' Move again to the correct item
				thisPosition = Get_Position(thisAuditID)

				' Validate current position.
				If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
					thisDataRow = Me.SortedRows(thisPosition)
				Else
					If (Me.SortedRows.GetLength(0) > 0) Then
						thisPosition = 0
						thisDataRow = Me.SortedRows(thisPosition)
						InstrumentsGridChanged = False
					Else
						thisDataRow = Nothing
					End If
				End If

				' Set SelectingCombo Index.
				If (Me.THIS_FORM_SelectingCombo.Items.Count <= 0) Then
					Try
						Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
					Catch ex As Exception
					End Try
				ElseIf (thisPosition < 0) Then
					Try
						MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
					Catch ex As Exception
					End Try
				Else
					Try
						Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
					Catch ex As Exception
					End Try
				End If

			End If

		Catch ex As Exception
		Finally

			InPaint = OrgInPaint

		End Try

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

    ' Retrieve form data if appropriate, i.e. this data item is not already being edited.

		If (RefreshForm = True) Then

			If (InstrumentsGridChanged = False) Then
        LoadInstrumentGrid(thisDataRow)  ' Includes a call to 'SetButtonStatus()'
			End If

			If (ConstrainsGridChanged = False) Then
				If (Constraints_ComboConstraintsGoup.SelectedIndex >= 0) AndAlso (IsNumeric(Constraints_ComboConstraintsGoup.SelectedValue)) Then
					LoadConstraintsGroup(CInt(Constraints_ComboConstraintsGoup.SelectedValue))
				End If
			End If

		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
	Private Sub SetSortedRows()

		Dim OrgInPaint As Boolean

		Dim thisrow As DataRow
		Dim thisDrowDownWidth As Integer
		Dim SizingBitmap As Bitmap
		Dim SizingGraphics As Graphics
		Dim SelectString As String = "RN >= 0"

		' Form Specific Selection Combo :-
		If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

		' Code is pretty Generic from here on...

		' Set paint local so that changes to the selection combo do not trigger form updates.

		OrgInPaint = InPaint
		InPaint = True

		If (Me.Combo_SelectGroup.SelectedIndex > 0) Then
			SelectString = "GroupGroup='" & Combo_SelectGroup.SelectedValue.ToString & "'"
		End If

		' Get selected Row sets, or exit if no data is present.
		If myDataset Is Nothing Then
			ReDim SortedRows(0)
			ReDim SelectBySortedRows(0)
		Else
			SortedRows = myTable.Select(SelectString, THIS_FORM_OrderBy)
			SelectBySortedRows = myTable.Select(SelectString, THIS_FORM_SelectBy)
		End If

		' Set Combo data source
		THIS_FORM_SelectingCombo.DataSource = SortedRows
		THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
		THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

		' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

		thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
		For Each thisrow In SortedRows

			' Compute the string dimensions in the given font
			SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
			SizingGraphics = Graphics.FromImage(SizingBitmap)
			Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
			If (stringSize.Width) > thisDrowDownWidth Then
				thisDrowDownWidth = CInt(stringSize.Width)
			End If
		Next

		THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

		If (THIS_FORM_SelectingCombo.Items.Count > 0) AndAlso (THIS_FORM_SelectingCombo.SelectedIndex < 0) Then
			THIS_FORM_SelectingCombo.SelectedIndex = 0
		End If

		InPaint = OrgInPaint
	End Sub


	' Check User permissions
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
	Private Sub Set_InstrumentsGridChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		Try
			If InPaint = False Then
				If (Me.HasUpdatePermission) Or (Me.HasInsertPermission) Then
					InstrumentsGridChanged = True
					Me.btnSave.Enabled = True
					Me.btnCancel.Enabled = True
				End If
			End If
		Catch ex As Exception
		End Try

	End Sub

	' 
	Private Sub Set_ConstrainsGridChanged()
		Try
			If InPaint = False Then
				ConstraintWorker.InvalidateConstraintDetails()

				If (Me.HasUpdatePermission) Or (Me.HasInsertPermission) Then
					ConstrainsGridChanged = True
					Me.Constraint_Button_SaveGroup.Enabled = True
					Me.Constraint_Button_CancelGroup.Enabled = True
				End If
			End If
		Catch ex As Exception
		End Try

	End Sub

  Private Sub MarginalsControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    Try
      Call UpdateMarginalsGrid()
    Catch ex As Exception
    End Try

  End Sub

	Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Select Menu
		' Event association is made during the dynamic menu creation

		Try
			THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

			Call SetSortedRows()
		Catch ex As Exception
		End Try

	End Sub

	Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Order Menu
		' Event association is made during the dynamic menu creation

		Try
			THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

			Call SetSortedRows()
		Catch ex As Exception
		End Try

	End Sub


	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation

		Try
			Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
				Case 0 ' This Record
					If (thisAuditID >= 0) Then
						Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID)
					End If

				Case 1 ' All Records
					Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset)

			End Select
		Catch ex As Exception
		End Try

	End Sub

#End Region

#Region " Menu Code"

	Private Function SetFieldSelectMenu(ByRef RootMenu As MenuStrip) As ToolStripMenuItem
		' ***************************************************************************************
		' Build the FieldSelect Menu.
		'
		' Add an item for each field in the tblGroupsAggregated table, when selected the item
		' will show or hide the associated field on the grid.
		' ***************************************************************************************

		Dim TransactionTable As DataTable = DSSelectedGroup.tblGroupsAggregated
		Dim ColumnNames(-1) As String
		Dim ColumnCount As Integer

		Dim FieldsMenu As New ToolStripMenuItem("Grid &Fields")
		FieldsMenu.Name = "Menu_GridField"

		Dim newMenuItem As ToolStripMenuItem

		ReDim ColumnNames(TransactionTable.Columns.Count - 1)
		For ColumnCount = 0 To (TransactionTable.Columns.Count - 1)
			ColumnNames(ColumnCount) = TransactionTable.Columns(ColumnCount).ColumnName
			If (ColumnNames(ColumnCount).StartsWith("Group")) Then
				ColumnNames(ColumnCount) = ColumnNames(ColumnCount).Substring(5) & "."
			End If
		Next
		Array.Sort(ColumnNames)

		For ColumnCount = 0 To (ColumnNames.Length - 1)
			newMenuItem = FieldsMenu.DropDownItems.Add(ColumnNames(ColumnCount), Nothing, AddressOf Me.ShowGridField)

			If (ColumnNames(ColumnCount).EndsWith(".")) Then
				newMenuItem.Name = "Menu_GridField_Group" & ColumnNames(ColumnCount).Substring(0, ColumnNames(ColumnCount).Length - 1)
			Else
				newMenuItem.Name = "Menu_GridField_" & ColumnNames(ColumnCount)
			End If
		Next

		RootMenu.Items.Add(FieldsMenu)
		Return FieldsMenu

	End Function

  Private Function SetGroupReportingMenu(ByRef RootMenu As MenuStrip) As ToolStripMenuItem
    ' ***************************************************************************************
    ' Build the FieldSelect Menu.
    '
    ' Add an item for each field in the tblGroupsAggregated table, when selected the item
    ' will show or hide the associated field on the grid.
    ' ***************************************************************************************

    Dim ReportsMenu As New ToolStripMenuItem("Group &Reports")
    ReportsMenu.Name = "Menu_GridField"

    Dim newMenuItem As ToolStripMenuItem

    newMenuItem = ReportsMenu.DropDownItems.Add("Group Report", Nothing, AddressOf Me.ShowGroupReport)
    newMenuItem.Name = "Menu_GroupReport"

    RootMenu.Items.Add(ReportsMenu)
    Return ReportsMenu

  End Function

  Private Function SetCustomFieldSelectMenu(ByRef RootMenu As MenuStrip) As ToolStripMenuItem
    ' ***************************************************************************************
    ' Build the FieldSelect Menu.
    '
    ' Add an item for each field in the tblPertracCustomFields table, when selected the item
    ' will show or hide the associated field on the grid.
    ' ***************************************************************************************

    Dim FieldsTable As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsDataTable
    Dim SelectedRows() As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow
    Dim FieldsRow As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow
    Dim FieldNames(-1) As String
    Dim RowCount As Integer

    FieldsTable = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPertracCustomFields).Tables(0)
    SelectedRows = FieldsTable.Select("True", "FieldName")
    Grid_CustomFields_Display_Dictionary.Clear()

    Dim FieldsMenu As New ToolStripMenuItem("Custom &Fields")
    FieldsMenu.Name = "Menu_CustomFields"

    Dim newMenuItem As ToolStripMenuItem

    ReDim FieldNames(SelectedRows.Length - 1)
    For RowCount = 0 To (SelectedRows.Length - 1)
      FieldsRow = SelectedRows(RowCount)

      FieldNames(RowCount) = FieldsRow.FieldName

      newMenuItem = FieldsMenu.DropDownItems.Add(FieldsRow.FieldName, Nothing, AddressOf Me.ShowGridField)
      newMenuItem.Name = "Menu_CustomField_" & FieldsRow.FieldID.ToString

      Grid_CustomFields_Display_Dictionary.Add(FieldsRow.FieldID, False)
    Next

    RootMenu.Items.Add(FieldsMenu)
    Return FieldsMenu

  End Function

	Private Sub SetCustomMenuChecked(ByRef pFieldID As String)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim FieldID As Integer

		If IsNumeric(pFieldID) = False Then
			Exit Sub
		End If

		FieldID = CInt(pFieldID)

		Try
			CType(CustomFieldsMenu.DropDownItems("Menu_CustomField_" & FieldID.ToString), ToolStripMenuItem).Checked = True
		Catch ex As Exception
		End Try
		Grid_CustomFields_Display_Dictionary(FieldID) = True

	End Sub

  Private Sub ShowGridField(ByVal sender As Object, ByVal e As EventArgs)
    ' ***************************************************************************************
    ' Callback function for the Grid Fields Menu items.
    '
    ' ***************************************************************************************

    Try
      If TypeOf (sender) Is ToolStripMenuItem Then
        Dim thisMenuItem As ToolStripMenuItem
        Dim FieldName As String

        thisMenuItem = CType(sender, ToolStripMenuItem)
        thisMenuItem.Checked = Not thisMenuItem.Checked
        FieldName = thisMenuItem.Name

        If (FieldName.StartsWith("Menu_CustomField_")) Then
          Dim FieldID As Integer

          FieldID = CInt(FieldName.Substring(17))

          If Grid_CustomFields_Display_Dictionary.ContainsKey(FieldID) Then
            Grid_CustomFields_Display_Dictionary.Item(FieldID) = thisMenuItem.Checked
          Else
            Grid_CustomFields_Display_Dictionary.Add(FieldID, thisMenuItem.Checked)
          End If

          Dim ThisColumnName As String
          ThisColumnName = "Custom_" & FieldID.ToString

          If (thisMenuItem.Checked = False) Then
            If Grid_Returns.Cols.Contains(ThisColumnName) Then
              Grid_Returns.Cols.Remove(ThisColumnName)
            End If
          Else
            CreateCustomFieldColumn(FieldID)
            PaintCustomFieldColumn(ThisColumnName)
          End If

        Else
          If (FieldName.StartsWith("Menu_GridField_")) Then
            FieldName = FieldName.Substring(15)
          End If

          If (Grid_Returns.Cols.Contains(FieldName)) Then
            If (thisMenuItem.Checked) Then
              Grid_Returns.Cols(FieldName).Visible = True
            Else
              Grid_Returns.Cols(FieldName).Visible = False
            End If
          End If

        End If
      End If
    Catch ex As Exception

    End Try
  End Sub

	Sub cms_Opening_ReturnsGrid(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		' Acquire references to the owning control and item.
		Dim c As Control = InstrumentsGridContextMenuStrip.SourceControl
		Dim tsi As ToolStripDropDownItem = InstrumentsGridContextMenuStrip.OwnerItem

		' Clear the ContextMenuStrip control's 
		' Items collection.
		InstrumentsGridContextMenuStrip.Items.Clear()

		InstrumentsGridClickMouseCol = Grid_Returns.MouseCol

		' Check the source control first.
		Dim Col_GroupSector As Integer = Grid_Returns.Cols("GroupSector").SafeIndex
		Dim Col_GroupIndexCode As Integer = Grid_Returns.Cols("GroupIndexCode").SafeIndex
		Dim Col_Beta As Integer = Grid_Returns.Cols("GroupBeta").SafeIndex
		Dim Col_Alpha As Integer = Grid_Returns.Cols("GroupAlpha").SafeIndex
		Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
		Dim Col_GroupTrade As Integer = Grid_Returns.Cols("GroupTrade").SafeIndex
		Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
		Dim Col_GroupLiquidity As Integer = Grid_Returns.Cols("GroupLiquidity").SafeIndex
    Dim Col_GroupExpectedReturn As Integer = Grid_Returns.Cols("GroupExpectedReturn").SafeIndex
    Dim Col_GroupUpperBound As Integer = Grid_Returns.Cols("GroupUpperBound").SafeIndex
    Dim Col_GroupLowerBound As Integer = Grid_Returns.Cols("GroupLowerBound").SafeIndex

		Dim IsCustomField As Boolean = False

		' Add custom item (Form)

		e.Cancel = False

		InstrumentsGridContextMenuStrip.Items.Add("Show Standard Return Columns", Nothing, AddressOf Menu_SetStandardReturnColumns)
		InstrumentsGridContextMenuStrip.Items.Add("Show Standard Optimiser Columns", Nothing, AddressOf Menu_SetStandardOptimiserColumns)

		InstrumentsGridContextMenuStrip.Items.Add(New ToolStripSeparator)

		If Grid_Returns.Cols(Grid_Returns.MouseCol).Name.StartsWith("Custom_") Then
			IsCustomField = True
		End If
		If (IsCustomField) Then
			If (Grid_Returns.MouseRow > 0) Then
				InstrumentsGridClickMouseRow = Grid_Returns.MouseRow
				InstrumentsGridContextMenuStrip.Items.Add("Delete Group Custom Field Item.", Nothing, AddressOf DeleteGroupCustomFieldData)
				InstrumentsGridContextMenuStrip.Items.Add("Copy Group Custom Field Item from Group Zero.", Nothing, AddressOf GetGroupZeroCustomFieldData)

				' Set Cancel to false. 
				' It is optimized to true based on empty entry.
				e.Cancel = False
			End If
		Else
			Select Case Grid_Returns.MouseCol
				Case Col_GroupSector
					If (Grid_Returns.MouseRow > 0) Then
						InstrumentsGridClickMouseRow = Grid_Returns.MouseRow
						InstrumentsGridContextMenuStrip.Items.Add("Get Sector From Venice", Nothing, AddressOf GetVeniceSector)
						InstrumentsGridContextMenuStrip.Items.Add("Get Selected Sectors From Venice", Nothing, AddressOf GetSelectedVeniceSectors)
						InstrumentsGridContextMenuStrip.Items.Add("Get ALL Sectors From Venice", Nothing, AddressOf GetAllVeniceSectors)
						InstrumentsGridContextMenuStrip.Items.Add("Get Sector From Pertrac", Nothing, AddressOf GetPertracSector)
						InstrumentsGridContextMenuStrip.Items.Add("Get Selected Sectors From Pertrac", Nothing, AddressOf GetSelectedPertracSectors)
						InstrumentsGridContextMenuStrip.Items.Add("Get ALL Sectors From Pertrac", Nothing, AddressOf GetAllPertracSectors)

						' Set Cancel to false. 
						' It is optimized to true based on empty entry.
						e.Cancel = False
					End If

				Case Col_GroupLiquidity
					If (Grid_Returns.MouseRow > 0) Then
						InstrumentsGridClickMouseRow = Grid_Returns.MouseRow
						InstrumentsGridContextMenuStrip.Items.Add("Get Liquidity From Venice", Nothing, AddressOf GetVeniceLiquidity)
						InstrumentsGridContextMenuStrip.Items.Add("Get Selected Liquidities From Venice", Nothing, AddressOf GetSelectedVeniceLiquidities)
						InstrumentsGridContextMenuStrip.Items.Add("Get ALL Liquidities From Venice", Nothing, AddressOf GetAllVeniceLiquidities)

						' Set Cancel to false. 
						' It is optimized to true based on empty entry.
						e.Cancel = False
					End If

				Case Col_GroupIndexCode
					If (Grid_Returns.MouseRow > 0) Then
						InstrumentsGridClickMouseRow = Grid_Returns.MouseRow

						InstrumentsGridContextMenuStrip.Items.Add("Set Benchmark Index, This Row only.", Nothing, AddressOf Menu_SetBenchmarkSingleRow)
						InstrumentsGridContextMenuStrip.Items.Add("Set Benchmark Index, All Rows.", Nothing, AddressOf Menu_SetBenchmarkAllRows)

						' Set Cancel to false. 
						' It is optimized to true based on empty entry.
						e.Cancel = False
					End If

				Case Col_Beta
					If (Grid_Returns.MouseRow > 0) Then
						InstrumentsGridClickMouseRow = Grid_Returns.MouseRow

						InstrumentsGridContextMenuStrip.Items.Add("Set THIS Expected Beta = Index Beta", Nothing, AddressOf SetThisExpectedBeta)
						InstrumentsGridContextMenuStrip.Items.Add("Set ALL Expected Beta = Index Beta", Nothing, AddressOf SetAllExpectedBeta)

						e.Cancel = False
					End If

				Case Col_Alpha
					If (Grid_Returns.MouseRow > 0) Then
						InstrumentsGridClickMouseRow = Grid_Returns.MouseRow

						InstrumentsGridContextMenuStrip.Items.Add("Set THIS Expected Alpha = Index Alpha", Nothing, AddressOf SetThisExpectedAlpha)
						InstrumentsGridContextMenuStrip.Items.Add("Set ALL Expected Alpha = Index Alpha", Nothing, AddressOf SetAllExpectedAlpha)

						e.Cancel = False
					End If

				Case Col_GroupHolding, Col_GroupTrade
					If (Grid_Returns.MouseRow > 0) Then
						InstrumentsGridClickMouseRow = Grid_Returns.MouseRow

						InstrumentsGridContextMenuStrip.Items.Add("Set THIS 'Trade' to Zero", Nothing, AddressOf SetThisTradeZero)
						InstrumentsGridContextMenuStrip.Items.Add("Set ALL 'Trade' to Zero", Nothing, AddressOf SetAllTradeZero)
						InstrumentsGridContextMenuStrip.Items.Add("Set THIS 'Holding' = 'New Holding'", Nothing, AddressOf SetThisHolding)
						InstrumentsGridContextMenuStrip.Items.Add("Set ALL 'Holding' = 'New Holding'", Nothing, AddressOf SetALLHolding)

						InstrumentsGridContextMenuStrip.Items.Add(New ToolStripSeparator)
						InstrumentsGridContextMenuStrip.Items.Add("Value Date :")
						InstrumentsGridContextMenuStrip.Items.Add(GetVeniceHoldingsDatePicker)
						InstrumentsGridContextMenuStrip.Items.Add(GetVeniceHoldingsLookthroughCheckbox)

						Dim thisMenu As ToolStripMenuItem
						thisMenu = New System.Windows.Forms.ToolStripMenuItem("Get THIS 'Holding' from Venice")
						PopulateGetHoldingMenu(thisMenu, SelectionEnum.SingleRow, False)
						InstrumentsGridContextMenuStrip.Items.Add(thisMenu)

						thisMenu = New System.Windows.Forms.ToolStripMenuItem("Get Selected 'Holding's from Venice")
						PopulateGetHoldingMenu(thisMenu, SelectionEnum.MultipleRows, False)
						InstrumentsGridContextMenuStrip.Items.Add(thisMenu)

						thisMenu = New System.Windows.Forms.ToolStripMenuItem("Get ALL 'Holding's from Venice")
						PopulateGetHoldingMenu(thisMenu, SelectionEnum.AllRows, False)
						InstrumentsGridContextMenuStrip.Items.Add(thisMenu)

						thisMenu = Nothing
						e.Cancel = False
					End If

				Case Col_GroupNewHolding
					If (Grid_Returns.MouseRow > 0) Then
						InstrumentsGridClickMouseRow = Grid_Returns.MouseRow

						InstrumentsGridContextMenuStrip.Items.Add("Set THIS 'Trade' to Zero", Nothing, AddressOf SetThisTradeZero)
						InstrumentsGridContextMenuStrip.Items.Add("Set ALL 'Trade' to Zero", Nothing, AddressOf SetAllTradeZero)
						InstrumentsGridContextMenuStrip.Items.Add("Set THIS 'NewHolding' = 'Holding'", Nothing, AddressOf SetThisNewHolding)
						InstrumentsGridContextMenuStrip.Items.Add("Set ALL 'NewHolding' = 'Holding'", Nothing, AddressOf SetALLNewHolding)

						InstrumentsGridContextMenuStrip.Items.Add(New ToolStripSeparator)
						InstrumentsGridContextMenuStrip.Items.Add("Value Date :")
						InstrumentsGridContextMenuStrip.Items.Add(GetVeniceHoldingsDatePicker)
						InstrumentsGridContextMenuStrip.Items.Add(GetVeniceHoldingsLookthroughCheckbox)

						Dim thisMenu As ToolStripMenuItem
						thisMenu = New System.Windows.Forms.ToolStripMenuItem("Get THIS 'NewHolding' from Venice")
						PopulateGetHoldingMenu(thisMenu, SelectionEnum.SingleRow, True)
						InstrumentsGridContextMenuStrip.Items.Add(thisMenu)

						thisMenu = New System.Windows.Forms.ToolStripMenuItem("Get Selected 'NewHolding's from Venice")
						PopulateGetHoldingMenu(thisMenu, SelectionEnum.MultipleRows, True)
						InstrumentsGridContextMenuStrip.Items.Add(thisMenu)

						thisMenu = New System.Windows.Forms.ToolStripMenuItem("Get ALL 'NewHolding's from Venice")
						PopulateGetHoldingMenu(thisMenu, SelectionEnum.AllRows, True)
						InstrumentsGridContextMenuStrip.Items.Add(thisMenu)

						thisMenu = Nothing
						e.Cancel = False
					End If

        Case Col_GroupLowerBound
          Dim ThisMenuItem As ToolStripMenuItem
          InstrumentsGridClickMouseRow = Grid_Returns.MouseRow

          ThisMenuItem = New ToolStripMenuItem("Set ALL Lower Bounds = THIS Lower Bound", Nothing, AddressOf SetLowerBounds)
          ThisMenuItem.Tag = "1Yr"
          InstrumentsGridContextMenuStrip.Items.Add(ThisMenuItem)

        Case Col_GroupUpperBound
          Dim ThisMenuItem As ToolStripMenuItem
          InstrumentsGridClickMouseRow = Grid_Returns.MouseRow

          ThisMenuItem = New ToolStripMenuItem("Set ALL Upper Bounds = THIS Upper Bound", Nothing, AddressOf SetUpperBounds)
          ThisMenuItem.Tag = "1Yr"
          InstrumentsGridContextMenuStrip.Items.Add(ThisMenuItem)

        Case Col_GroupExpectedReturn

          Dim ThisMenuItem As ToolStripMenuItem
          InstrumentsGridClickMouseRow = Grid_Returns.MouseRow

          ThisMenuItem = New ToolStripMenuItem("Set THIS Expected Return to 1Yr Return", Nothing, AddressOf SetTHISExpectedReturn)
          ThisMenuItem.Tag = "1Yr"
          InstrumentsGridContextMenuStrip.Items.Add(ThisMenuItem)
          ThisMenuItem = New ToolStripMenuItem("Set ALL Expected Return to 1Yr Return", Nothing, AddressOf SetALLExpectedReturn)
          ThisMenuItem.Tag = "1Yr"
          InstrumentsGridContextMenuStrip.Items.Add(ThisMenuItem)

          ThisMenuItem = New ToolStripMenuItem("Set THIS Expected Return to 3Yr Return", Nothing, AddressOf SetTHISExpectedReturn)
          ThisMenuItem.Tag = "3Yr"
          InstrumentsGridContextMenuStrip.Items.Add(ThisMenuItem)
          ThisMenuItem = New ToolStripMenuItem("Set ALL Expected Return to 3Yr Return", Nothing, AddressOf SetALLExpectedReturn)
          ThisMenuItem.Tag = "3Yr"
          InstrumentsGridContextMenuStrip.Items.Add(ThisMenuItem)

          ThisMenuItem = New ToolStripMenuItem("Set THIS Expected Return to 5Yr Return", Nothing, AddressOf SetTHISExpectedReturn)
          ThisMenuItem.Tag = "5Yr"
          InstrumentsGridContextMenuStrip.Items.Add(ThisMenuItem)
          ThisMenuItem = New ToolStripMenuItem("Set ALL Expected Return to 5Yr Return", Nothing, AddressOf SetALLExpectedReturn)
          ThisMenuItem.Tag = "5Yr"
          InstrumentsGridContextMenuStrip.Items.Add(ThisMenuItem)

          ThisMenuItem = New ToolStripMenuItem("Set THIS Expected Return to ITD Return", Nothing, AddressOf SetTHISExpectedReturn)
          ThisMenuItem.Tag = "ITD"
          InstrumentsGridContextMenuStrip.Items.Add(ThisMenuItem)
          ThisMenuItem = New ToolStripMenuItem("Set ALL Expected Return to ITD Return", Nothing, AddressOf SetALLExpectedReturn)
          ThisMenuItem.Tag = "ITD"
          InstrumentsGridContextMenuStrip.Items.Add(ThisMenuItem)


        Case Else
          '	e.Cancel = True

      End Select
		End If

	End Sub

  Private Sub SetTHISExpectedReturn(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim Col_GroupExpectedReturn As Integer = Grid_Returns.Cols("GroupExpectedReturn").SafeIndex
    Dim Col_GroupIndexE As Integer = Grid_Returns.Cols("GroupIndexE").SafeIndex
    Dim Col_GroupBetaE As Integer = Grid_Returns.Cols("GroupBetaE").SafeIndex
    Dim Col_GroupAlphaE As Integer = Grid_Returns.Cols("GroupAlphaE").SafeIndex
    Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
    Dim Col_GroupIndexCode As Integer = Grid_Returns.Cols("GroupIndexCode").SafeIndex
    Dim ThisPertracID As UInteger
    Dim BackFillPertracID As UInteger
    Dim ThisSimpleStats As RenaissanceStatFunctions.StatFunctions.SeriesStatsClass
    Dim ThisTag As String = ""
    Dim TargetReturn As Double = 0
    Dim ThisIndexExpectedBeta As Double
    Dim ThisIndexExpectedReturn As Double

    If (Me.Grid_Returns.Rows.Count > 1) AndAlso (InstrumentsGridClickMouseRow > 0) Then

      Try

        ThisPertracID = CUInt(Math.Max(CInt(Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupPertracCode)), 0))
        BackFillPertracID = CUInt(Math.Max(CInt(Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupIndexCode)), 0))

        ThisSimpleStats = MainForm.StatFunctions.GetSimpleStats(DefaultStatsDatePeriod, MainForm.StatFunctions.CombinedStatsID(ThisPertracID, BackFillPertracID), False, 0.0#, 1.0#)

        Try
          If CType(sender, ToolStripMenuItem).Tag IsNot Nothing Then
            ThisTag = CType(sender, ToolStripMenuItem).Tag.ToString.ToUpper
          Else
            ThisTag = ""
          End If
        Catch ex As Exception
        End Try

        Select Case ThisTag

          Case "1YR"
            TargetReturn = ThisSimpleStats.AnnualisedReturn.M12

          Case "3YR"
            TargetReturn = ThisSimpleStats.AnnualisedReturn.M36

          Case "5YR"
            TargetReturn = ThisSimpleStats.AnnualisedReturn.M60

          Case "ITD"
            TargetReturn = ThisSimpleStats.AnnualisedReturn.ITD

          Case Else

            TargetReturn = ThisSimpleStats.AnnualisedReturn.ITD

        End Select

        ThisIndexExpectedReturn = CDbl(Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupIndexE))
        ThisIndexExpectedBeta = CDbl(Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupBetaE))

        Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupAlphaE) = TargetReturn - (ThisIndexExpectedBeta * ThisIndexExpectedReturn)

      Catch ex As Exception
      End Try

      Call Update_Group_RiskAndReturn(True)
      Call UpdateMarginalsGrid()

    End If

  End Sub

  Private Sub SetALLExpectedReturn(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim Col_GroupExpectedReturn As Integer = Grid_Returns.Cols("GroupExpectedReturn").SafeIndex
    Dim Col_GroupIndexE As Integer = Grid_Returns.Cols("GroupIndexE").SafeIndex
    Dim Col_GroupBetaE As Integer = Grid_Returns.Cols("GroupBetaE").SafeIndex
    Dim Col_GroupAlphaE As Integer = Grid_Returns.Cols("GroupAlphaE").SafeIndex
    Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
    Dim Col_GroupIndexCode As Integer = Grid_Returns.Cols("GroupIndexCode").SafeIndex
    Dim ThisPertracID As UInteger
    Dim BackFillPertracID As UInteger
    Dim ThisSimpleStats As RenaissanceStatFunctions.StatFunctions.SeriesStatsClass
    Dim ThisTag As String = ""
    Dim TargetReturn As Double = 0
    Dim ThisIndexExpectedBeta As Double
    Dim ThisIndexExpectedReturn As Double

    If (Me.Grid_Returns.Rows.Count > 1) Then

      Try

        If (Me.Grid_Returns.Rows.Count > 1) Then
          Dim ThisGridRow As Integer

          For ThisGridRow = 1 To (Me.Grid_Returns.Rows.Count - 1)

            ThisPertracID = CUInt(Math.Max(CInt(Grid_Returns.Item(ThisGridRow, Col_GroupPertracCode)), 0))
            BackFillPertracID = CUInt(Math.Max(CInt(Grid_Returns.Item(ThisGridRow, Col_GroupIndexCode)), 0))

            ThisSimpleStats = MainForm.StatFunctions.GetSimpleStats(DefaultStatsDatePeriod, MainForm.StatFunctions.CombinedStatsID(ThisPertracID, BackFillPertracID), False, 0.0#, 1.0#)

            Try
              If CType(sender, ToolStripMenuItem).Tag IsNot Nothing Then
                ThisTag = CType(sender, ToolStripMenuItem).Tag.ToString.ToUpper
              Else
                ThisTag = ""
              End If
            Catch ex As Exception
            End Try

            Select Case ThisTag

              Case "1YR"
                TargetReturn = ThisSimpleStats.AnnualisedReturn.M12

              Case "3YR"
                TargetReturn = ThisSimpleStats.AnnualisedReturn.M36

              Case "5YR"
                TargetReturn = ThisSimpleStats.AnnualisedReturn.M60

              Case "ITD"
                TargetReturn = ThisSimpleStats.AnnualisedReturn.ITD

              Case Else

                TargetReturn = ThisSimpleStats.AnnualisedReturn.ITD

            End Select

            ThisIndexExpectedReturn = CDbl(Grid_Returns.Item(ThisGridRow, Col_GroupIndexE))
            ThisIndexExpectedBeta = CDbl(Grid_Returns.Item(ThisGridRow, Col_GroupBetaE))

            Grid_Returns.Item(ThisGridRow, Col_GroupAlphaE) = TargetReturn - (ThisIndexExpectedBeta * ThisIndexExpectedReturn)
            'Application.DoEvents()

          Next

        End If

      Catch ex As Exception
      End Try

      Call Update_Group_RiskAndReturn(True)
      Call UpdateMarginalsGrid()

    End If
  End Sub

	Private Function GetNewCmsDateTimePicker() As System.Windows.Forms.DateTimePicker
		' ************************************************************************
		' Create the 'Value Date' DT Picker used on the grid context menu.
		'
		' ************************************************************************

		Dim RVal As New System.Windows.Forms.DateTimePicker

		Try
			RVal.Size = New System.Drawing.Size(200, 20)

			RVal.Value = GetVeniceHoldingsValueDate
			AddHandler RVal.ValueChanged, AddressOf CmsDateTimePicker_ValueChanged

		Catch ex As Exception
		End Try

		Return RVal

	End Function

	Private Sub CmsDateTimePicker_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' ************************************************************************
		' ValueChanged callback for the Get Holdings Value Date DT Picker.
		'
		' ************************************************************************

		Try
			GetVeniceHoldingsValueDate = CType(sender, System.Windows.Forms.DateTimePicker).Value

			' Clear holdings cache if 'ValueDate' changes

			If (tblVeniceHoldings IsNot Nothing) Then
				tblVeniceHoldings.Clear()
			End If
		Catch ex As Exception
		Finally
			tblVeniceHoldings = Nothing
		End Try

	End Sub

	Private Function GetNewCmsCheckbox() As System.Windows.Forms.CheckBox
		' ************************************************************************
		' Create the 'Lookthrough' Checkbox used on the grid context menu.
		'
		' ************************************************************************

		Dim RVal As New System.Windows.Forms.CheckBox

		Try
			RVal.Text = "Lookthrough holdings"
			RVal.Size = New System.Drawing.Size(200, 20)
			RVal.CheckAlign = ContentAlignment.MiddleLeft
			RVal.AutoCheck = True
			RVal.Checked = GetVeniceHoldingsLookthroughCheckboxChecked
			RVal.BackColor = Color.Transparent
			AddHandler RVal.CheckedChanged, AddressOf CmsCheckbox_CheckedChanged

		Catch ex As Exception
		End Try

		Return RVal

	End Function

	Private Sub CmsCheckbox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' ************************************************************************
		' CheckedChanged callback for the Get Holdings 'Lookthrough' checkbox.
		'
		' ************************************************************************

		Try
			GetVeniceHoldingsLookthroughCheckboxChecked = CType(sender, System.Windows.Forms.CheckBox).Checked

			' Clear holdings cache if 'Lookthrough' changes

			If (tblVeniceHoldings IsNot Nothing) Then
				tblVeniceHoldings.Clear()
			End If
		Catch ex As Exception
		Finally
			tblVeniceHoldings = Nothing
		End Try

	End Sub

#End Region

#Region " Reports Code"

  Private Sub ShowGroupReport(ByVal sender As Object, ByVal e As EventArgs)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************


		Try
			Me.Cursor = Cursors.WaitCursor

			' 1) Create  a Data Table reflecting the contents of the Grid.

			Dim ReportTable As New DataTable
			Dim ThisReportTableRow As DataRow
			Dim ThisTableColumn As DataColumn
			Dim ThisGridColumn As C1.Win.C1FlexGrid.Column

			For Each ThisGridColumn In Me.Grid_Returns.Cols
				ThisTableColumn = New DataColumn(ThisGridColumn.Name, ThisGridColumn.DataType)
				ReportTable.Columns.Add(ThisTableColumn)
			Next

			' 2) populate the data table with the Grid data

			Dim GridRow As Integer
			Dim GridColumn As Integer

			For GridRow = 1 To (Me.Grid_Returns.Rows.Count - 1)
				ThisReportTableRow = ReportTable.NewRow

				For GridColumn = 0 To (Me.Grid_Returns.Cols.Count - 1)
					ThisReportTableRow(Grid_Returns.Cols(GridColumn).Name) = Grid_Returns.Item(GridRow, GridColumn)
				Next

				ReportTable.Rows.Add(ThisReportTableRow)
			Next

			MainForm.MainReportHandler.DisplayReport("rptOptimiserGroupReport", ReportTable, 0, Now.Date, MainForm.Main_Knowledgedate, Nothing)

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error running the Group Report", ex.StackTrace, True)
		Finally
			Me.Cursor = Cursors.Default
		End Try

  End Sub

#End Region

#Region " Set Form Combos (Form Specific Code) "

	Private Sub SetGroupCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_SelectGroup, _
		RenaissanceStandardDatasets.tblGroupList, _
		"GroupGroup", _
		"GroupGroup", _
		"", True, True, True, "", "All Groups")		' 

	End Sub

	Private Sub SetBenchmarkIndexCombo()

    Combo_BenckmarkIndex.MasternameCollection = MainForm.MasternameDictionary
    Combo_BenckmarkIndex.SelectNoMatch = False

    Combo_MarginalIndex.MasternameCollection = MainForm.MasternameDictionary
    Combo_MarginalIndex.SelectNoMatch = False

	End Sub

	Private Sub SetConstraintSetsCombo()
		' ***************************************************************
		'
		' ***************************************************************

		Call MainForm.SetTblGenericCombo( _
		Me.Constraints_ComboConstraintsGoup, _
		RenaissanceStandardDatasets.tblGenoaConstraintList, _
		"ConstraintGroupName", _
		"ConstraintGroupID", _
		"", False, True, True, 0, "")		' 

	End Sub

	Private Sub SetConstraints_FieldSelectGrid()
		' ***************************************************************
		'
		' ***************************************************************

		Dim thisDataset As DSPertracCustomFields
		Dim thisSortedRows() As DSPertracCustomFields.tblPertracCustomFieldsRow = Nothing
		Dim thisRow As DSPertracCustomFields.tblPertracCustomFieldsRow
		Dim GridRowCounter As Integer
		Dim FieldRowCounter As Integer
		Dim thisTab As System.Windows.Forms.TabPage
		Dim Col_CustomFieldID As Integer = Constraints_Grid_Defined.Cols("Col_CustomID").SafeIndex
		Dim ThisCustomFieldID As Integer

		thisDataset = MainForm.Load_Table(RenaissanceStandardDatasets.tblPertracCustomFields, False)
		If Not (thisDataset Is Nothing) Then
			thisSortedRows = thisDataset.Tables(0).Select("FieldIsOptimisable<>0", "FieldName")
		End If

		If (thisSortedRows Is Nothing) OrElse (thisSortedRows.Length <= 0) Then
			' Clear Fields Grid, Clear superflous Constraint Tabs

			' Remove Grid Rows (Except the Two Special Cases)

			For GridRowCounter = (Constraints_Grid_Defined.Rows.Count - 1) To 1 Step -1
				ThisCustomFieldID = CInt(Constraints_Grid_Defined.Item(GridRowCounter, Col_CustomFieldID))

				If Not ((ThisCustomFieldID = SpecialConstraintFields.Sector) Or (ThisCustomFieldID = SpecialConstraintFields.Liquidity)) Then
					Try
						Constraints_Grid_Defined.Rows.Remove(GridRowCounter)
					Catch ex As Exception
					End Try
				End If

			Next

			' Clear Tab Pages

			Dim TabCounter As Integer

			For TabCounter = (Constraints_TabConstraints.TabCount - 1) To 0 Step -1
				If (TabCounter > 0) Then
					thisTab = Constraints_TabConstraints.TabPages(TabCounter)

					If (thisTab.Name <> "Tab_Sector") AndAlso (thisTab.Name <> "Tab_Liquidity") Then
						Constraints_TabConstraints.TabPages.RemoveAt(TabCounter)

						' Save This TabPage for later

						If (thisTab.Tag IsNot Nothing) AndAlso (TypeOf thisTab.Tag Is ConstraintsTabDetails) Then
							CType(thisTab.Tag, ConstraintsTabDetails).ConstraintsGrid.Rows.Count = 1
						End If

						ConstraintsTabPageStack.Push(thisTab)
					End If

				End If
			Next

		Else
			' Add New / Delete Missing grid rows as necessary
			Dim FoundFlag As Boolean = False

			' Check For Deleted Fields

			For GridRowCounter = (Constraints_Grid_Defined.Rows.Count - 1) To 1 Step -1
				ThisCustomFieldID = CInt(Constraints_Grid_Defined.Item(GridRowCounter, Col_CustomFieldID))

				If Not ((ThisCustomFieldID = SpecialConstraintFields.Sector) Or (ThisCustomFieldID = SpecialConstraintFields.Liquidity)) Then
					' Check Existence of CustomFieldID, if it is not one of the special cases.

					FoundFlag = False
					For FieldRowCounter = 0 To (thisSortedRows.Length - 1)
						thisRow = thisSortedRows(FieldRowCounter)

						If (thisRow.FieldID = ThisCustomFieldID) Then
							FoundFlag = True
							Exit For
						End If
					Next

					If (FoundFlag = False) Then
						' The CustomField represented by the current GridRow does not exist in the Custom Field Table.
						' either it has been deleted, or IsOptimisable has been set to false.
						' In Either case, remove it from the Grid and ,if necessary, from the TabControl

						Try
							Constraints_Grid_Defined.Rows.Remove(GridRowCounter)
						Catch ex As Exception
						End Try

						Try
							thisTab = Constraints_TabConstraints.TabPages("Tab_" & ThisCustomFieldID.ToString)

							If (thisTab IsNot Nothing) Then

								' Remove TabPage

								Constraints_TabConstraints.TabPages.Remove(thisTab)

								' Save This TabPage for later

								If (thisTab.Tag IsNot Nothing) AndAlso (TypeOf thisTab.Tag Is ConstraintsTabDetails) Then
									CType(thisTab.Tag, ConstraintsTabDetails).ConstraintsGrid.Rows.Count = 1
								End If

								ConstraintsTabPageStack.Push(thisTab)

							End If

						Catch ex As Exception
						End Try

					End If ' FoundFlag = True
				End If ' Not 'Special' field

			Next

			' Add New Fields

			For FieldRowCounter = (-2) To (thisSortedRows.Length - 1)
				Try

					If (FieldRowCounter < 0) Then
						ThisCustomFieldID = FieldRowCounter
          Else
            ThisCustomFieldID = thisSortedRows(FieldRowCounter).FieldID
					End If

          'If (FieldRowCounter <> 0) Then
          FoundFlag = False

          For GridRowCounter = 1 To (Constraints_Grid_Defined.Rows.Count - 1)
            Try
              If CInt(Constraints_Grid_Defined.Item(GridRowCounter, Col_CustomFieldID)) = ThisCustomFieldID Then
                FoundFlag = True
                Exit For
              End If
            Catch ex As Exception
            End Try
          Next

          If (FoundFlag) Then
            ' Check Grid and Tab Details

            Dim GridRow As C1.Win.C1FlexGrid.Row = Nothing
            Dim ThisDataType As RenaissanceDataType
            Dim DataTypeChanged As Boolean = False
            Dim FieldNameChanged As Boolean = False

            Try
              GridRow = Constraints_Grid_Defined.Rows(GridRowCounter)

              If (ThisCustomFieldID < 0) Then
                If CStr(Nz(GridRow("Col_CustomName"), "")) <> CType(FieldRowCounter, SpecialConstraintFields).ToString Then
                  FieldNameChanged = True
                  GridRow("Col_CustomName") = CType(FieldRowCounter, SpecialConstraintFields).ToString
                End If


                If (FieldRowCounter = SpecialConstraintFields.Liquidity) Then
                  '		GridRow("Col_CustomFieldDataType") = RenaissanceDataType.NumericType
                  ThisDataType = RenaissanceDataType.NumericType
                Else
                  ThisDataType = RenaissanceDataType.TextType
                End If
              Else
                If (CStr(Nz(GridRow("Col_CustomName"), "")) <> thisSortedRows(FieldRowCounter).FieldName) Then
                  FieldNameChanged = True
                  GridRow("Col_CustomName") = thisSortedRows(FieldRowCounter).FieldName
                End If

                ThisDataType = thisSortedRows(FieldRowCounter).FieldDataType
              End If

              If CInt(GridRow("Col_CustomFieldDataType")) <> ThisDataType Then
                GridRow("Col_CustomFieldDataType") = CInt(ThisDataType)
                DataTypeChanged = True
              End If

            Catch ex As Exception
            End Try

            Try
              If ((DataTypeChanged) Or (FieldNameChanged)) AndAlso (CBool(GridRow("Col_IsDefined"))) Then
                Dim TabName As String
                Dim ThisTabPage As TabPage

                If (ThisCustomFieldID < 0) Then
                  TabName = "Tab_" & CType(ThisCustomFieldID, SpecialConstraintFields).ToString
                Else
                  TabName = "Tab_" & ThisCustomFieldID.ToString
                End If

                If Me.Constraints_TabConstraints.TabPages.ContainsKey(TabName) Then
                  ThisTabPage = Constraints_TabConstraints.TabPages(TabName)

                  If (DataTypeChanged) Then
                    If (ThisTabPage.Tag IsNot Nothing) Then
                      Dim thisConstraintsTabDetails As ConstraintsTabDetails

                      thisConstraintsTabDetails = CType(ThisTabPage.Tag, ConstraintsTabDetails)

                      If (thisConstraintsTabDetails.CustomFieldDataType <> ThisDataType) Then
                        thisConstraintsTabDetails.CustomFieldDataType = ThisDataType

                        If Not Type.Equals(thisConstraintsTabDetails.ConstraintsGrid.Cols("Col_Value").DataType, DefaultDataTypeType(ThisDataType)) Then
                          thisConstraintsTabDetails.ConstraintsGrid.Cols("Col_Value").DataType = DefaultDataTypeType(ThisDataType)

                          Dim ConstraintsGridCounter As Integer
                          Dim ConstraintsGridRow As C1.Win.C1FlexGrid.Row

                          For ConstraintsGridCounter = 1 To (thisConstraintsTabDetails.ConstraintsGrid.Cols.Count - 1)
                            Try
                              ConstraintsGridRow = thisConstraintsTabDetails.ConstraintsGrid.Rows(ConstraintsGridCounter)

                              If (ConstraintsGridRow("Col_Value") IsNot Nothing) Then
                                ConstraintsGridRow("Col_Value") = ConvertValue(ConstraintsGridRow("Col_Value"), thisConstraintsTabDetails.ConstraintsGrid.Cols("Col_Value").DataType)
                              End If
                            Catch ex As Exception
                            End Try
                          Next

                        End If

                        thisConstraintsTabDetails.ConstraintsGrid.Cols("Col_Value").Format = DefaultDataTypeFormat(ThisDataType)
                      End If

                    End If
                  End If

                  If (FieldNameChanged) Then
                    ThisTabPage.Text = Nz(GridRow("Col_CustomName"), "").ToString
                  End If

                End If
              End If
            Catch ex As Exception

            End Try

          Else

            ' Add Row Grid Row
            Dim NewGridRow As C1.Win.C1FlexGrid.Row

            NewGridRow = Constraints_Grid_Defined.Rows.Add()

            NewGridRow(Col_CustomFieldID) = ThisCustomFieldID
            NewGridRow("Col_IsDefined") = False
            NewGridRow("Col_IsApplied") = False

            If (ThisCustomFieldID < 0) Then
              NewGridRow("Col_CustomName") = CType(FieldRowCounter, SpecialConstraintFields).ToString

              If (FieldRowCounter = SpecialConstraintFields.Liquidity) Then
                NewGridRow("Col_CustomFieldDataType") = CInt(RenaissanceDataType.NumericType)
              Else
                NewGridRow("Col_CustomFieldDataType") = CInt(RenaissanceDataType.TextType)
              End If
            Else
              NewGridRow("Col_CustomName") = thisSortedRows(FieldRowCounter).FieldName
              NewGridRow("Col_CustomFieldDataType") = thisSortedRows(FieldRowCounter).FieldDataType
            End If
          End If
          'End If

        Catch ex As Exception
        End Try

      Next

    End If

    ' Sort
    Constraints_Grid_Defined.Sort(New ConstraintFieldGridComparer)


	End Sub

#End Region

#Region "Instrument Grid : Load and Save / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

  Private Sub LoadInstrumentGrid(ByRef ThisRow As RenaissanceDataClass.DSGroupList.tblGroupListRow)
    ' *****************************************************************************************
    ' Routine to populate form Controls given a data row.
    ' Form is cleared for invalid Datarows or Invalid Form.
    ' *****************************************************************************************

    Dim OrgInpaint As Boolean

    ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
    OrgInpaint = InPaint

    Try
      InPaint = True

      Grid_Returns.Enabled = False
      Me.Cursor = Cursors.WaitCursor

      If (ThisRow Is Nothing) OrElse (FormIsValid = False) OrElse (Me.InUse = False) OrElse (IsNumeric(Combo_SelectGroupID.SelectedValue) = False) Then
        ' Bad / New Datarow - Clear Form.
        Try

          thisAuditID = (-1)

          Me.editAuditID.Text = ""
          Me.Label_GroupStartDate.Text = ""
          Me.Label_GroupEndDate.Text = ""

          Me.Grid_Returns.Rows.Count = 1

          Me.btnCancel.Enabled = False
          Me.THIS_FORM_SelectingCombo.Enabled = True

          Me.Combo_Marginals_ContraInstrument.DataSource = Nothing

          ' Set Empty constraints Set
          If (Constraints_ComboConstraintsGoup.Items.Count > 0) AndAlso (Constraints_ComboConstraintsGoup.SelectedIndex <> 0) Then
            Constraints_ComboConstraintsGoup.SelectedIndex = 0
          End If
          LoadConstraintsGroup(0)

          ' Set Live Covariance Matrix
          Check_Covariance_BackfillData.Checked = False
          If (Radio_Covariance_UseLiveMatrix.Checked) Then
            CalculateLiveCovarianceMatrix(DefaultStatsDatePeriod)
          Else
            Me.Radio_Covariance_UseLiveMatrix.Checked = True
          End If

        Catch ex As Exception
        End Try

      Else
        Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
        Dim Col_GroupTrade As Integer = Grid_Returns.Cols("GroupTrade").SafeIndex
        Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex

        Dim ThisHolding As Double
        Dim ThisTrade As Double
        Dim ThisNewHolding As Double

        ' Populate Form with given data.

        Try
          Dim GroupListID As Integer
          Dim RowCount As Integer
          Dim ColCount As Integer
          Dim ColOrdinals(Grid_Returns.Cols.Count - 1) As Integer
          Dim SelectedRows() As DSGroupsAggregated.tblGroupsAggregatedRow
          Dim ThisGroupRow As DSGroupsAggregated.tblGroupsAggregatedRow

          thisAuditID = thisDataRow.AuditID

          Me.editAuditID.Text = thisDataRow.AuditID.ToString

          GroupListID = CInt(Combo_SelectGroupID.SelectedValue)
          Me.Label_GroupStartDate.Text = ""
          Me.Label_GroupEndDate.Text = ""

          If Not (LoadedAggregatedGroupListID = GroupListID) Then
            LoadSelectedAggregatedGroup(GroupListID, DSSelectedGroup.tblGroupsAggregated)
          End If
          SelectedRows = DSSelectedGroup.tblGroupsAggregated.Select("GroupListID=" & GroupListID.ToString, "PertracName")

          Grid_Returns.Rows.Count = (1 + SelectedRows.Length)

          ' Get Ordinals

          ColOrdinals(0) = (-1)
          ColOrdinals(1) = (-1)
          For ColCount = 2 To (Grid_Returns.Cols.Count - 1)
            ColOrdinals(ColCount) = (-1)
            Try
              If DSSelectedGroup.tblGroupsAggregated.Columns.Contains(Grid_Returns.Cols(ColCount).Name) Then
                ColOrdinals(ColCount) = DSSelectedGroup.tblGroupsAggregated.Columns.IndexOf(Grid_Returns.Cols(ColCount).Name)
              End If
            Catch ex As Exception
            End Try
          Next

          ' Load pertrac Data

          Try
            If (SelectedRows.Length > 0) Then
              Dim SelectedPertracIDs(SelectedRows.Length - 1) As Integer

              MainForm.GenoaStatusLabel.Text = "Loading Pertrac Returns"
              MainForm.GenoaStatusStrip.Refresh()
              Application.DoEvents()

              For RowCount = 1 To (SelectedRows.Length)
                ThisGroupRow = SelectedRows(RowCount - 1)

                SelectedPertracIDs(RowCount - 1) = ThisGroupRow.GroupPertracCode
              Next

              MainForm.PertracData.LoadPertracTables(SelectedPertracIDs)

            End If
          Catch ex As Exception
            Call MainForm.LogError(Me.Name & ", LoadInstrumentGrid", LOG_LEVELS.Error, ex.Message, "Error loading Pertrac returns.", ex.StackTrace, True)
          Finally
            MainForm.GenoaStatusLabel.Text = ""
            MainForm.GenoaStatusStrip.Refresh()
            Application.DoEvents()
          End Try

          ' Get Common StatsDatePeriod.

          If (SelectedRows.Length > 0) Then

            Dim newStatsDatePeriod As DealingPeriod = DealingPeriod.Daily
            Dim IDArray((SelectedRows.Length * 2) - 1) As Integer
            Dim PeriodArray() As DealingPeriod

            Dim thisIndex As Integer = 0

            For Each ThisGroupRow In SelectedRows
              IDArray((thisIndex * 2)) = ThisGroupRow.GroupPertracCode
              IDArray((thisIndex * 2) + 1) = ThisGroupRow.GroupIndexCode
              thisIndex += 1
            Next

            ' Remove Duplicates, simply to improve performance in the call to GetPertracDataPeriods()

            Array.Sort(IDArray)
            For thisIndex = 1 To (IDArray.Length - 1)
              If (IDArray(thisIndex) > 0) AndAlso (IDArray(thisIndex) = IDArray(thisIndex - 1)) Then
                IDArray(thisIndex - 1) = 0
              End If
            Next

            ' Get Array of Native Date Periods for these instruments

            PeriodArray = MainForm.PertracData.GetPertracDataPeriods(IDArray)

            ' Resolve Coarsest Date Period, including INDEX IDs, thus dont use PertracData.GetPertracDataPeriod().

            ' Me.DefaultStatsDatePeriod = MainForm.PertracData.GetPertracDataPeriod(RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(GroupListID, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Group_Mean))

            For thisIndex = 0 To (IDArray.Length - 1)
              If (IDArray(thisIndex) > 0) Then
                If (MainForm.StatFunctions.AnnualPeriodCount(PeriodArray(thisIndex)) < MainForm.StatFunctions.AnnualPeriodCount(newStatsDatePeriod)) Then
                  newStatsDatePeriod = PeriodArray(thisIndex)
                End If
              End If
            Next

            Me.DefaultStatsDatePeriod = newStatsDatePeriod

          End If

          ' Show Data

          For RowCount = 1 To (SelectedRows.Length)
            ThisGroupRow = SelectedRows(RowCount - 1)

            If (StatusLabel_Optimisation.Visible) Then
              StatusLabel_Optimisation.Text = "Working on " & ThisGroupRow.PertracName
              StatusStrip_GroupReturns.Refresh()
              ' Application.DoEvents()
            Else
              MainForm.GenoaStatusLabel.Text = "Working on " & ThisGroupRow.PertracName
              MainForm.GenoaStatusStrip.Refresh()
              Application.DoEvents()
            End If

            If (RowCount = 1) Then
              If IsDate(ThisGroupRow.GroupDateFrom) Then
                Me.Label_GroupStartDate.Text = ThisGroupRow.GroupDateFrom.ToString(DISPLAYMEMBER_DATEFORMAT)
              End If
              If IsDate(ThisGroupRow.GroupDateTo) Then
                Me.Label_GroupEndDate.Text = ThisGroupRow.GroupDateTo.ToString(DISPLAYMEMBER_DATEFORMAT)
              End If
            End If

            Grid_Returns.Item(RowCount, 0) = "" ' Col_Updated
            Grid_Returns.Item(RowCount, 1) = "" ' Col_CustomUpdated
            For ColCount = 2 To (Grid_Returns.Cols.Count - 1)
              If (ColOrdinals(ColCount) >= 0) Then
                Grid_Returns.Item(RowCount, ColCount) = ThisGroupRow(ColOrdinals(ColCount))
              Else
                Grid_Returns.Item(RowCount, ColCount) = Nz(Nothing, Grid_Returns.Cols(ColCount).DataType)
              End If
            Next

            ' Update Trade Column to reflect Holding and NewHolding

            ThisHolding = CDbl(Grid_Returns.Item(RowCount, Col_GroupHolding))
            ThisTrade = CDbl(Grid_Returns.Item(RowCount, Col_GroupTrade))
            ThisNewHolding = CDbl(Grid_Returns.Item(RowCount, Col_GroupNewHolding))

            If Math.Abs(ThisHolding - (ThisNewHolding - ThisTrade)) > GENOA_RoundingError Then
              ' New Holding has changed

              Grid_Returns.Item(RowCount, Col_GroupTrade) = Math.Round(ThisNewHolding - ThisHolding, GENOA_HoldingPrecision)

            End If

            ' Set Alpha And Beta

            GetAlphaBetaAndStdError(ThisGroupRow.GroupPertracCode, ThisGroupRow.GroupIndexCode, RowCount)

          Next

          ' Paint Custom Fields

          For ColCount = 2 To (Grid_Returns.Cols.Count - 1)
            If (Grid_Returns.Cols(ColCount).Name.StartsWith("Custom_")) Then
              PaintCustomFieldColumn(Grid_Returns.Cols(ColCount).Name)
            End If
          Next


          ' Load appropriate constraints Set
          If (thisDataRow.DefaultConstraintGroup <= 0) Then
            If (Constraints_ComboConstraintsGoup.Items.Count > 0) AndAlso (Constraints_ComboConstraintsGoup.SelectedIndex <> 0) Then
              Constraints_ComboConstraintsGoup.SelectedIndex = 0
            End If

            LoadConstraintsGroup(0)
          Else
            Try
              LoadConstraintsGroup(0)

              Constraints_ComboConstraintsGoup.SelectedValue = thisDataRow.DefaultConstraintGroup

              Constraints_ComboConstraintsGoup.Tag = Constraints_ComboConstraintsGoup.SelectedIndex
              LoadConstraintsGroup(thisDataRow.DefaultConstraintGroup)

            Catch ex As Exception
              LoadConstraintsGroup(0)
            End Try
          End If

          ' If the Grid was sorted (not by Name) prior to Loading the portfolio, then the NewPercent column may at this point be out of order.
          ' This will cause the Risk & Return calculations to be inaccurate and these may be calculated during the Covariance Matrix update.
          ' So...

          Call Update_Group_Percentages(True)

          ' Refresh ScalingFactorHash

          Call SetScalingFactorHash()

          ' Load Covariance Matrix

          If thisDataRow.DefaultCovarianceMatrix > 0 Then
            Radio_Covariance_UseSavedMatrix.Checked = True
            List_CovarianceMatrices.SelectedValue = thisDataRow.DefaultCovarianceMatrix
          ElseIf (Radio_Covariance_UseLiveMatrix.Checked) Then
            CalculateLiveCovarianceMatrix(DefaultStatsDatePeriod)
          Else
            Radio_Covariance_UseLiveMatrix.Checked = True
          End If

          ' Set Buttons

          Me.btnCancel.Enabled = False
          Me.THIS_FORM_SelectingCombo.Enabled = True

          ' Set values on Marginals Contra Instruments Combo.

          MainForm.SetTblGenericCombo(Me.Combo_Marginals_ContraInstrument, SelectedRows, "PertracName", "GroupPertracCode", "", False, True, True, 0, "")

        Catch ex As Exception
          Grid_Returns.Rows.Count = 1

          Call MainForm.LogError(Me.Name & ", LoadInstrumentGrid", LOG_LEVELS.Error, ex.Message, "Error Showing Data", ex.StackTrace, True)
          Call LoadInstrumentGrid(Nothing)

        Finally
          StatusLabel_Optimisation.Text = ""
          If (StatusLabel_Optimisation.Visible = False) Then
            MainForm.GenoaStatusLabel.Text = ""
            MainForm.GenoaStatusStrip.Refresh()
          End If

        End Try

      End If


      ' Restore Grid Sort order ?
      Try
        If (Grid_Returns.SortColumn IsNot Nothing) Then
          Grid_Returns.Sort(C1.Win.C1FlexGrid.SortFlags.UseColSort, Grid_Returns.SortColumn.SafeIndex)
        End If
      Catch ex As Exception
      End Try

      Call Update_Group_Percentages(True)

      ' Allow Field events to trigger before 'InPaint' Is re-set. 
      ' (Should) Prevent Validation errors during Form Draw.
      Application.DoEvents()


    Catch ex As Exception
    Finally

      Grid_Returns.Enabled = True
      Me.Cursor = Cursors.Default

      ' Restore 'Paint' flag.
      InPaint = OrgInpaint
      InstrumentsGridChanged = False
      Me.btnSave.Enabled = False

    End Try

    Update_Group_RiskAndReturn(False)

    ConstraintWorker.InvalidateInstruments()

    UpdateMarginalsGrid()

    CalculateConstraintExposures()

    ' As it says on the can....

    Call SetButtonStatus()

  End Sub

	Private Sub LoadSelectedAggregatedGroup(ByVal GroupListID As Integer, ByRef tblGroupsAggregated As DSGroupsAggregated.tblGroupsAggregatedDataTable)
		' *************************************************************
		'
		' *************************************************************

		Try
			' Exit if Table is Null.

			If (tblGroupsAggregated Is Nothing) Then
				Exit Sub
			End If

			' Clear existing Rows

			If (tblGroupsAggregated.Rows.Count > 0) Then
				tblGroupsAggregated.Rows.Clear()
			End If

			' OK, Load data.
			Dim SelectCommand As New SqlCommand

			' Set Select Command
			SelectCommand.CommandText = "[adp_tblGroupsAggregated_SelectCommand]"
			SelectCommand.CommandType = System.Data.CommandType.StoredProcedure

			Try
				SelectCommand.Connection = myConnection
				SelectCommand.Parameters.Add( _
				New SqlParameter("@RETURN_VALUE", System.Data.SqlDbType.Int, 4, System.Data.ParameterDirection.ReturnValue, False, CType(0, Byte), CType(0, Byte), "", System.Data.DataRowVersion.Current, Nothing))
				SelectCommand.Parameters.Add(New SqlParameter("@GroupListID", System.Data.SqlDbType.Int)).Value = GroupListID
				SelectCommand.Parameters.Add(New SqlParameter("@Knowledgedate", System.Data.SqlDbType.DateTime)).Value = MainForm.Main_Knowledgedate

				SyncLock SelectCommand.Connection
					tblGroupsAggregated.Load(SelectCommand.ExecuteReader)
				End SyncLock

				LoadedAggregatedGroupListID = GroupListID

			Catch ex As Exception
				If (tblGroupsAggregated.Rows.Count > 0) Then
					tblGroupsAggregated.Rows.Clear()
				End If

				LoadedAggregatedGroupListID = 0
			Finally
				SelectCommand.Connection = Nothing
			End Try

		Catch ex As Exception
			If (tblGroupsAggregated.Rows.Count > 0) Then
				tblGroupsAggregated.Rows.Clear()
			End If

			LoadedAggregatedGroupListID = 0
		End Try
	End Sub

	Private Function SaveInstrumentGrid(Optional ByVal pConfirm As Boolean = True, Optional ByVal pDiscardChanges As Boolean = True) As System.Windows.Forms.DialogResult
		' *************************************************************
		'
		' *************************************************************

		Dim ErrMessage As String
		Dim ErrFlag As Boolean
		Dim ErrStack As String
		Dim ProtectedItem As Boolean = False
    Dim UpdateDetailsGroupItems As String = ""
    Dim UpdateDetailsDataItems As String = ""

		ErrMessage = ""
		ErrStack = ""

		' *************************************************************
		'
		' *************************************************************

		If (InstrumentsGridChanged = False) Or (FormIsValid = False) Or (Me.IsDisposed) Then
			Return Windows.Forms.DialogResult.OK
			Exit Function
		End If

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) And (Me.HasInsertPermission = False) Then
			MainForm.LogError(Me.Name & ", SaveInstrumentGrid()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", SaveInstrumentGrid()", LOG_LEVELS.Warning, "", "Changes to Genoa are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm) AndAlso (InstrumentsGridChanged) Then
			Dim UserAnswer As System.Windows.Forms.DialogResult

      UserAnswer = MessageBox.Show("Save Instrument Changes ?", "Save Instrument Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

			If UserAnswer <> Windows.Forms.DialogResult.Yes Then
				If (pDiscardChanges) And (UserAnswer = Windows.Forms.DialogResult.No) Then
					InstrumentsGridChanged = False
				End If

				Return UserAnswer
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim UpdateRows(0) As DataRow

		' *************************************************************
		' Clear focus from the Grid / Set selected cell to origin
		' *************************************************************

		Grid_Returns.FinishEditing()
		Grid_Returns.Select(0, 0, 0, 0, False)

		' *************************************************************
		' Process Update(s)
		' *************************************************************

		Dim ItemsUpdated As Boolean = False
		Dim ItemsDataUpdated As Boolean = False
		Dim CustomFieldDataUpdated As Boolean = False

		Try
			InPaint = True

			' *************************************************************
			' Static fields.
			'
			' Work through the DataGrid, Checking and updating those records
			' that appear to have changed.
			' *************************************************************

			Dim GroupItemsAdaptor As SqlDataAdapter = MainForm.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblGroupItems.Adaptorname, Genoa_CONNECTION, RenaissanceStandardDatasets.tblGroupItems.TableName)
			Dim GroupItemsDataAdaptor As SqlDataAdapter = MainForm.MainDataHandler.Get_Adaptor(RenaissanceStandardDatasets.tblGroupItemData.Adaptorname, Genoa_CONNECTION, RenaissanceStandardDatasets.tblGroupItemData.TableName)

			Dim ItemsDS As DSGroupItems = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupItems)
			Dim ItemsDataDS As DSGroupItemData = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupItemData)
			Dim ItemsTbl As DSGroupItems.tblGroupItemsDataTable = Nothing
			Dim ItemsDataTbl As DSGroupItemData.tblGroupItemDataDataTable = Nothing
			Dim SelectedItem() As DSGroupItems.tblGroupItemsRow
			Dim SelectedItemData() As DSGroupItemData.tblGroupItemDataRow
			Dim GridRow As Integer


			If (ItemsDS IsNot Nothing) Then
				ItemsTbl = ItemsDS.tblGroupItems
			End If
			If (ItemsDataDS IsNot Nothing) Then
				ItemsDataTbl = ItemsDataDS.tblGroupItemData
			End If

			If (ItemsTbl IsNot Nothing) AndAlso (ItemsDataTbl IsNot Nothing) AndAlso (GroupItemsAdaptor IsNot Nothing) AndAlso (GroupItemsDataAdaptor IsNot Nothing) Then
				Dim Col_GroupListID As Integer = Grid_Returns.Cols("GroupListID").SafeIndex
				Dim Col_GroupItemID As Integer = Grid_Returns.Cols("GroupItemID").SafeIndex

				Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
				Dim Col_GroupIndexCode As Integer = Grid_Returns.Cols("GroupIndexCode").SafeIndex
				Dim Col_GroupAlpha As Integer = Grid_Returns.Cols("GroupAlpha").SafeIndex
				Dim Col_GroupBeta As Integer = Grid_Returns.Cols("GroupBeta").SafeIndex
				Dim Col_GroupIndexE As Integer = Grid_Returns.Cols("GroupIndexE").SafeIndex
				Dim Col_GroupBetaE As Integer = Grid_Returns.Cols("GroupBetaE").SafeIndex
				Dim Col_GroupAlphaE As Integer = Grid_Returns.Cols("GroupAlphaE").SafeIndex
				Dim Col_GroupExpectedReturn As Integer = Grid_Returns.Cols("GroupExpectedReturn").SafeIndex
				Dim Col_GroupStdErr As Integer = Grid_Returns.Cols("GroupStdErr").SafeIndex
				Dim Col_GroupScalingFactor As Integer = Grid_Returns.Cols("GroupScalingFactor").SafeIndex
				Dim Col_GroupSector As Integer = Grid_Returns.Cols("GroupSector").SafeIndex

				Dim Col_GroupLiquidity As Integer = Grid_Returns.Cols("GroupLiquidity").SafeIndex
				Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
				Dim Col_GroupTrade As Integer = Grid_Returns.Cols("GroupTrade").SafeIndex
				Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
				Dim Col_GroupPercent As Integer = Grid_Returns.Cols("GroupPercent").SafeIndex
				Dim Col_GroupUpperBound As Integer = Grid_Returns.Cols("GroupUpperBound").SafeIndex
				Dim Col_GroupLowerBound As Integer = Grid_Returns.Cols("GroupLowerBound").SafeIndex
				Dim Col_GroupFloor As Integer = Grid_Returns.Cols("GroupFloor").SafeIndex
				Dim Col_GroupCap As Integer = Grid_Returns.Cols("GroupCap").SafeIndex
				Dim Col_GroupTradeSize As Integer = Grid_Returns.Cols("GroupTradeSize").SafeIndex

				Dim ThisListID As Integer
				Dim ThisItemID As Integer
				Dim ThisPertracID As Integer
				Dim ThisIndexCode As Integer
				Dim ThisSector As String

				Dim ThisItemAlpha As Double
				Dim ThisItemBeta As Double
				Dim ThisItemIndexE As Double
				Dim ThisItemBetaE As Double
				Dim ThisItemAlphaE As Double
				Dim ThisExpectedReturn As Double
				Dim ThisStdErr As Double
				Dim ThisScalingFactor As Double

				SyncLock ItemsTbl
					SyncLock ItemsDataTbl

						For GridRow = 1 To (Grid_Returns.Rows.Count - 1)
							Try
								ThisListID = CInt(Grid_Returns.Item(GridRow, Col_GroupListID))
								ThisItemID = CInt(Grid_Returns.Item(GridRow, Col_GroupItemID))
								ThisPertracID = CInt(Grid_Returns.Item(GridRow, Col_GroupPertracCode))

								' Update tables table

								SelectedItem = ItemsTbl.Select("GroupItemID=" & ThisItemID.ToString)
								If (SelectedItem.Length > 0) Then
									ThisSector = CStr(Grid_Returns.Item(GridRow, Col_GroupSector))
									ThisIndexCode = CInt(Grid_Returns.Item(GridRow, Col_GroupIndexCode))

									' 1st, check the GroupItems table

									If (SelectedItem(0).GroupIndexCode <> ThisIndexCode) OrElse (SelectedItem(0).GroupSector <> ThisSector) Then
										SelectedItem(0).GroupIndexCode = ThisIndexCode
										SelectedItem(0).GroupSector = ThisSector
										GroupItemsAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
										GroupItemsAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

                    UpdateDetailsGroupItems &= SelectedItem(0).GroupItemID.ToString & ","
										Call MainForm.AdaptorUpdate(Me.Name, GroupItemsAdaptor, New DataRow() {SelectedItem(0)})
										ItemsUpdated = True
									End If

									' 2nd, check the GroupItemsData table

									ThisItemAlpha = CDbl(Grid_Returns.Item(GridRow, Col_GroupAlpha))
									ThisItemBeta = CDbl(Grid_Returns.Item(GridRow, Col_GroupBeta))
									ThisItemIndexE = CDbl(Grid_Returns.Item(GridRow, Col_GroupIndexE))
									ThisItemBetaE = CDbl(Grid_Returns.Item(GridRow, Col_GroupBetaE))
									ThisItemAlphaE = CDbl(Grid_Returns.Item(GridRow, Col_GroupAlphaE))
									ThisExpectedReturn = CDbl(Grid_Returns.Item(GridRow, Col_GroupExpectedReturn))
									ThisStdErr = CDbl(Grid_Returns.Item(GridRow, Col_GroupStdErr))
									ThisScalingFactor = CDbl(Grid_Returns.Item(GridRow, Col_GroupScalingFactor))

									SelectedItemData = ItemsDataTbl.Select("GroupItemID=" & ThisItemID.ToString)

									If (SelectedItemData.Length > 0) Then

										If Not (SelectedItemData(0).GroupPertracCode = ThisPertracID) Then
											SelectedItemData(0).GroupPertracCode = ThisPertracID
										End If

										If Not (SelectedItemData(0).GroupLiquidity = CInt(Grid_Returns.Item(GridRow, Col_GroupLiquidity))) Then
											SelectedItemData(0).GroupLiquidity = CInt(Grid_Returns.Item(GridRow, Col_GroupLiquidity))
										End If

										If Not (SelectedItemData(0).GroupHolding = CDbl(Grid_Returns.Item(GridRow, Col_GroupHolding))) Then
											SelectedItemData(0).GroupHolding = CDbl(Grid_Returns.Item(GridRow, Col_GroupHolding))
										End If

										'If Not (SelectedItemData(0).GroupTrade = CDbl(Grid_Returns.Item(GridRow, Col_GroupTrade))) Then
										'	SelectedItemData(0).GroupTrade = CDbl(Grid_Returns.Item(GridRow, Col_GroupTrade))
										'End If

										If Not (SelectedItemData(0).GroupNewHolding = CDbl(Grid_Returns.Item(GridRow, Col_GroupNewHolding))) Then
											SelectedItemData(0).GroupNewHolding = CDbl(Grid_Returns.Item(GridRow, Col_GroupNewHolding))
										End If

										If Not (SelectedItemData(0).GroupPercent = CDbl(Grid_Returns.Item(GridRow, Col_GroupPercent))) Then
											SelectedItemData(0).GroupPercent = CDbl(Grid_Returns.Item(GridRow, Col_GroupPercent))
										End If

										If Not (SelectedItemData(0).GroupUpperBound = CDbl(Grid_Returns.Item(GridRow, Col_GroupUpperBound))) Then
											SelectedItemData(0).GroupUpperBound = CDbl(Grid_Returns.Item(GridRow, Col_GroupUpperBound))
										End If

										If Not (SelectedItemData(0).GroupLowerBound = CDbl(Grid_Returns.Item(GridRow, Col_GroupLowerBound))) Then
											SelectedItemData(0).GroupLowerBound = CDbl(Grid_Returns.Item(GridRow, Col_GroupLowerBound))
										End If

										If Not (SelectedItemData(0).GroupFloor = CBool(Grid_Returns.Item(GridRow, Col_GroupFloor))) Then
											SelectedItemData(0).GroupFloor = CBool(Grid_Returns.Item(GridRow, Col_GroupFloor))
										End If

										If Not (SelectedItemData(0).GroupCap = CBool(Grid_Returns.Item(GridRow, Col_GroupCap))) Then
											SelectedItemData(0).GroupCap = CBool(Grid_Returns.Item(GridRow, Col_GroupCap))
										End If

										If Not (SelectedItemData(0).GroupTradeSize = CDbl(Grid_Returns.Item(GridRow, Col_GroupTradeSize))) Then
											SelectedItemData(0).GroupTradeSize = CDbl(Grid_Returns.Item(GridRow, Col_GroupTradeSize))
										End If

										If Not (SelectedItemData(0).GroupAlpha = ThisItemAlpha) Then
											SelectedItemData(0).GroupAlpha = ThisItemAlpha
										End If

										If Not (SelectedItemData(0).GroupBeta = ThisItemBeta) Then
											SelectedItemData(0).GroupBeta = ThisItemBeta
										End If

										If Not (SelectedItemData(0).GroupIndexE = ThisItemIndexE) Then
											SelectedItemData(0).GroupIndexE = ThisItemIndexE
										End If

										If Not (SelectedItemData(0).GroupBetaE = ThisItemBetaE) Then
											SelectedItemData(0).GroupBetaE = ThisItemBetaE
										End If

										If Not (SelectedItemData(0).GroupAlphaE = ThisItemAlphaE) Then
											SelectedItemData(0).GroupAlphaE = ThisItemAlphaE
										End If

										If Not (SelectedItemData(0).GroupExpectedReturn = ThisExpectedReturn) Then
											SelectedItemData(0).GroupExpectedReturn = ThisExpectedReturn
										End If

										If Not (SelectedItemData(0).GroupStdErr = ThisStdErr) Then
											SelectedItemData(0).GroupStdErr = ThisStdErr
										End If

										If Not (SelectedItemData(0).GroupScalingFactor = ThisScalingFactor) Then
											SelectedItemData(0).GroupScalingFactor = ThisScalingFactor
										End If

										If (SelectedItemData(0).RowState And DataRowState.Modified) = DataRowState.Modified Then
											GroupItemsDataAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
											GroupItemsDataAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

											Call MainForm.AdaptorUpdate(Me.Name, GroupItemsDataAdaptor, New DataRow() {SelectedItemData(0)})
                      UpdateDetailsDataItems &= SelectedItem(0).GroupItemID.ToString & ","

											ItemsDataUpdated = True
										End If

									Else
										' Add new Data Item row.

										Dim NewDataItemRow As DSGroupItemData.tblGroupItemDataRow

										NewDataItemRow = ItemsDataTbl.NewtblGroupItemDataRow

										NewDataItemRow.GroupItemID = ThisItemID
										NewDataItemRow.GroupPertracCode = ThisPertracID

										NewDataItemRow.GroupLiquidity = CInt(Grid_Returns.Item(GridRow, Col_GroupLiquidity))
										NewDataItemRow.GroupHolding = CDbl(Grid_Returns.Item(GridRow, Col_GroupHolding))
										'NewDataItemRow.GroupTrade = CDbl(Grid_Returns.Item(GridRow, Col_GroupTrade))
										NewDataItemRow.GroupNewHolding = CDbl(Grid_Returns.Item(GridRow, Col_GroupNewHolding))
										NewDataItemRow.GroupPercent = CDbl(Grid_Returns.Item(GridRow, Col_GroupPercent))
										NewDataItemRow.GroupUpperBound = CDbl(Grid_Returns.Item(GridRow, Col_GroupUpperBound))
										NewDataItemRow.GroupLowerBound = CDbl(Grid_Returns.Item(GridRow, Col_GroupLowerBound))
										NewDataItemRow.GroupFloor = CBool(Grid_Returns.Item(GridRow, Col_GroupFloor))
										NewDataItemRow.GroupCap = CBool(Grid_Returns.Item(GridRow, Col_GroupCap))
										NewDataItemRow.GroupTradeSize = CDbl(Grid_Returns.Item(GridRow, Col_GroupTradeSize))

										NewDataItemRow.GroupAlpha = ThisItemAlpha
										NewDataItemRow.GroupBeta = ThisItemBeta
										NewDataItemRow.GroupIndexE = ThisItemIndexE
										NewDataItemRow.GroupBetaE = ThisItemBetaE
										NewDataItemRow.GroupAlphaE = ThisItemAlphaE
										NewDataItemRow.GroupExpectedReturn = ThisExpectedReturn
										NewDataItemRow.GroupStdErr = ThisStdErr
										NewDataItemRow.GroupScalingFactor = ThisScalingFactor

										ItemsDataTbl.Rows.Add(NewDataItemRow)

										GroupItemsDataAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
										GroupItemsDataAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

										Call MainForm.AdaptorUpdate(Me.Name, GroupItemsDataAdaptor, New DataRow() {NewDataItemRow})

                    UpdateDetailsDataItems &= ThisItemID.ToString & ","

										ItemsDataUpdated = True

									End If

								Else
									' Item Does not exist...
								End If

							Catch ex As Exception
								ErrMessage = ex.Message
								ErrFlag = True
								ErrStack = ex.StackTrace

								Exit For
							End Try
						Next
					End SyncLock
				End SyncLock

			End If

		Catch ex As Exception
			ErrMessage = ex.Message
			ErrFlag = True
			ErrStack = ex.StackTrace
		Finally
			InPaint = False
		End Try


		' *************************************************************
		' Custom fields.
		'
		' Work through the DataGrid, Checking and updating those records
		' that appear to have changed.
		' *************************************************************
		Dim GroupListID As Integer
		Dim CustomFieldDataUpdateString As String = ""

		If (ErrFlag = False) Then
			Dim ColCount As Integer
			Dim RowCount As Integer
			Dim ColumnName As String
			Dim CustomFieldID As Integer
			Dim Col_Is_Custom As Boolean
			Dim PertracID As Integer

			Dim Changed_Ordinal As Integer = Grid_Returns.Cols.IndexOf("Col_CustomUpdated")
			Dim ID_Ordinal As Integer = Grid_Returns.Cols.IndexOf("GroupPertracCode")

			Dim thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow = Nothing
			Dim ThisConnection As SqlConnection = Nothing
			Dim AdpCustomData As New SqlDataAdapter
			Dim DSCustomData As RenaissanceDataClass.DSPertracCustomFieldData
			Dim tblCustomData As RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataDataTable
			Dim ThisRow As RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataRow
			Dim ThisCustomFieldDataUpdated As Boolean = False

			Dim ThisGridItem As Object

			Try
				ThisConnection = MainForm.GetGenoaConnection
				MainForm.MainAdaptorHandler.Set_AdaptorCommands(ThisConnection, AdpCustomData, RenaissanceStandardDatasets.tblPertracCustomFieldData.TableName)
				AdpCustomData.InsertCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate
				AdpCustomData.UpdateCommand.Parameters("@KnowledgeDate").Value = MainForm.Main_Knowledgedate

				If (Combo_SelectGroupID.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_SelectGroupID.SelectedValue)) Then
					GroupListID = CInt(Combo_SelectGroupID.SelectedValue)
				Else
					GroupListID = (0)
				End If

				For ColCount = 2 To (Grid_Returns.Cols.Count - 1)
					If (Grid_Returns.Cols(ColCount).Name.StartsWith("Custom_")) Then

						ColumnName = Grid_Returns.Cols(ColCount).Name
						CustomFieldID = (-1)
						Col_Is_Custom = False
						thisCustomFieldDefinition = Nothing

						If ColumnName.StartsWith("Custom_") AndAlso (IsNumeric(ColumnName.Substring(7))) Then
							CustomFieldID = CInt(ColumnName.Substring(7))
							thisCustomFieldDefinition = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, CustomFieldID, "")

							' Only allow the update of 'Custom' fields
							If (thisCustomFieldDefinition IsNot Nothing) Then
								Select Case CType(thisCustomFieldDefinition.CustomFieldType, PertracCustomFieldTypes)
									Case PertracCustomFieldTypes.CustomBoolean, PertracCustomFieldTypes.CustomDate, PertracCustomFieldTypes.CustomNumeric, PertracCustomFieldTypes.CustomString
										Col_Is_Custom = True

									Case Else
										Col_Is_Custom = False

								End Select
							End If
						End If

						If (Col_Is_Custom) Then
							' ********************************************************
							' When Saving Custom field data, rather than get the existing data and 
							' update the datarows as appropriate, I have chosen to simply create
							' a new table and add all changes as new rows. The underlying INSERT command
							' has been written to check for existing records and INSERT / UPDATE as appropriate.
							' 
							' This approach has been taken as I believe it should be faster, especially for 
							' mass updates.
							' ********************************************************

							DSCustomData = New RenaissanceDataClass.DSPertracCustomFieldData
							tblCustomData = DSCustomData.tblPertracCustomFieldData

							For RowCount = 1 To (Grid_Returns.Rows.Count - 1)
								If (CStr(Grid_Returns.Item(RowCount, Changed_Ordinal)) = "x") Then
									Grid_Returns.Item(RowCount, Changed_Ordinal) = ""

									PertracID = CInt(Grid_Returns.Item(RowCount, ID_Ordinal))

									ThisRow = tblCustomData.NewtblPertracCustomFieldDataRow

									ThisRow.FieldDataID = 0
									ThisRow.CustomFieldID = CustomFieldID
									ThisRow.PertracID = PertracID
									ThisRow.GroupID = GroupListID
									ThisRow.FieldNumericData = 0
									ThisRow.FieldTextData = ""
									ThisRow.FieldDateData = Renaissance_BaseDate
									ThisRow.FieldBooleanData = False
									ThisRow.UpdateRequired = False
									ThisRow.LatestReturnDate = Renaissance_BaseDate

									Try
										ThisGridItem = Grid_Returns.Item(RowCount, ColCount)

										If (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.TextType) Then
											If (ThisGridItem IsNot Nothing) Then
												ThisRow.FieldTextData = ThisGridItem.ToString
											End If

										ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.NumericType) Then
											If IsNumeric(ThisGridItem) Then
												ThisRow.FieldNumericData = CDbl(ThisGridItem)
											End If

										ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.DateType) Then
											If IsDate(ThisGridItem) Then
												ThisRow.FieldDateData = CDate(ThisGridItem)
											End If

										ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.BooleanType) Then
											ThisRow.FieldBooleanData = CBool(ThisGridItem)

										End If
									Catch ex As Exception
									End Try

									If (ThisRow.RowState And DataRowState.Detached) Then
										tblCustomData.Rows.Add(ThisRow)
									End If

									ThisCustomFieldDataUpdated = True

								End If

							Next

							If (ThisCustomFieldDataUpdated) Then
								CustomFieldDataUpdated = True

								MainForm.AdaptorUpdate(Me.Name, AdpCustomData, tblCustomData)

								If (CustomFieldDataUpdateString.Length > 0) Then
									CustomFieldDataUpdateString &= ","
								End If
								CustomFieldDataUpdateString &= (CustomFieldID.ToString & "." & GroupListID.ToString)

								CustomFieldDataCache.ClearDataCacheGroupData(CustomFieldID, GroupListID)
							End If

							tblCustomData = Nothing
							DSCustomData = Nothing

						End If
					End If
				Next

			Catch ex As Exception
				ErrMessage = ex.Message
				ErrFlag = True
				ErrStack = ex.StackTrace
			Finally
				If (ThisConnection IsNot Nothing) Then
					Try
						ThisConnection.Close()
					Catch ex As Exception
					End Try
					ThisConnection = Nothing
				End If
			End Try

		End If


		' *************************************************************
		' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
		' *************************************************************

		If (ErrFlag = True) Then
			Call MainForm.LogError(Me.Name & ", SaveInstrumentGrid", 0, ErrMessage, "Error Saving Data", ErrStack, True)
		End If

		' Finish off

		InstrumentsGridChanged = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		' Propagate changes
		Try
			Dim UpdateMessage As New RenaissanceGlobals.RenaissanceUpdateEventArgs

			If ItemsUpdated Then
				UpdateMessage.TableChanged(RenaissanceStandardDatasets.tblGroupItems.ChangeID) = True
        UpdateMessage.UpdateDetail(RenaissanceStandardDatasets.tblGroupItems.ChangeID) = UpdateDetailsGroupItems
      End If

			If ItemsDataUpdated Then
				UpdateMessage.TableChanged(RenaissanceStandardDatasets.tblGroupItemData.ChangeID) = True
        UpdateMessage.UpdateDetail(RenaissanceStandardDatasets.tblGroupItemData.ChangeID) = UpdateDetailsDataItems
      End If

      If (CustomFieldDataUpdated) Then
        UpdateMessage.TableChanged(RenaissanceStandardDatasets.tblPertracCustomFieldData.ChangeID) = True
        UpdateMessage.UpdateDetail(RenaissanceStandardDatasets.tblPertracCustomFieldData.ChangeID) = CustomFieldDataUpdateString
      End If

			If (ItemsUpdated) OrElse (ItemsDataUpdated) OrElse (CustomFieldDataUpdated) Then
				Call MainForm.Main_RaiseEvent(UpdateMessage)
			End If
		Catch ex As Exception
		End Try

		Return Windows.Forms.DialogResult.OK

	End Function

	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
		 ((Me.HasUpdatePermission) Or (Me.HasInsertPermission)) Then


		Else


		End If

	End Sub

	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub

	Private Sub Combo_SelectGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectGroup.SelectedIndexChanged
		' ********************************************************************************************
		' This form allows the user to display only those Entities from a given group.
		' This combo controls this.
		'
		' ********************************************************************************************

		If InPaint Then
			Exit Sub
		End If

		If (InstrumentsGridChanged = True) Then
			Call SaveInstrumentGrid()
		End If
		If (ConstrainsGridChanged = True) Then
			Call SaveConstraintsGrid()
		End If

		Call SetSortedRows()

		thisPosition = 0
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		If (thisPosition >= 0) Then
			thisDataRow = Me.SortedRows(thisPosition)
		Else
			thisDataRow = Nothing
		End If

		Grid_Returns.Sort(C1.Win.C1FlexGrid.SortFlags.Ascending, -1)
    Call LoadInstrumentGrid(thisDataRow)

	End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

	Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' ********************************************************************************************
		' Selection Combo. SelectedItem changed.
		'
		' ********************************************************************************************

    If (GenericUpdateObject IsNot Nothing) Then

      GenericUpdateObject.FormToUpdate = True
      Update_GroupToLoad = True

    Else

      Update_GroupToLoad = True
      FormUpdate_Tick()

    End If

	End Sub

	Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
		' ********************************************************************************************
		' 'Previous' Button.
		' ********************************************************************************************

		If (InstrumentsGridChanged = True) Then
			Call SaveInstrumentGrid()
		End If
		If (ConstrainsGridChanged = True) Then
			Call SaveConstraintsGrid()
		End If

		If thisPosition > 0 Then thisPosition -= 1
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		Exit Sub

		If myTable.Rows.Count > thisPosition Then
			thisDataRow = myTable.Rows(thisPosition)
      Call LoadInstrumentGrid(thisDataRow)
		Else
      Call LoadInstrumentGrid(Nothing)
		End If

	End Sub

	Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
		' ********************************************************************************************
		' 'Next' Button.
		' ********************************************************************************************

		If (InstrumentsGridChanged = True) Then
			Call SaveInstrumentGrid()
		End If
		If (ConstrainsGridChanged = True) Then
			Call SaveConstraintsGrid()
		End If

		If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		Exit Sub

		If myTable.Rows.Count > thisPosition Then
			thisDataRow = myTable.Rows(thisPosition)
      Call LoadInstrumentGrid(thisDataRow)
		Else
      Call LoadInstrumentGrid(Nothing)
		End If

	End Sub

	Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
		' ********************************************************************************************
		' 'First' Button.
		' ********************************************************************************************

		If (InstrumentsGridChanged = True) Then
			Call SaveInstrumentGrid()
		End If
		If (ConstrainsGridChanged = True) Then
			Call SaveConstraintsGrid()
		End If

		thisPosition = 0
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

    If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
      Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
    Else
      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    End If

	End Sub

	Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
		' ********************************************************************************************
		' 'Last' Button.
		' ********************************************************************************************

		If (InstrumentsGridChanged = True) Then
			Call SaveInstrumentGrid()
		End If
		If (ConstrainsGridChanged = True) Then
			Call SaveConstraintsGrid()
		End If

		thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

	End Sub

	Private Function Get_Position(ByVal pAuditID As Integer) As Integer
		' ********************************************************************************************
		' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
		' AudidID.
		' ********************************************************************************************

		Dim MinIndex As Integer
		Dim MaxIndex As Integer
		Dim CurrentMin As Integer
		Dim CurrentMax As Integer
		Dim searchDataRow As DataRow

		Try
			' SortedRows exists ?

			If SortedRows Is Nothing Then
				Return (-1)
				Exit Function
			End If

			' Return (-1) if there are no rows in the 'SortedRows'

			If (SortedRows.GetLength(0) <= 0) Then
				Return (-1)
				Exit Function
			End If


			' Use a modified Search moving outwards from the last returned value to locate the required value.
			' Reflecting the fact that for the most part One looks for closely located records.

			MinIndex = 0
			MaxIndex = SortedRows.GetLength(0) - 1


			' Check First and Last records (Incase 'First' or 'Last' was pressed).

			searchDataRow = SortedRows(MinIndex)

			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
				Return MinIndex
				Exit Function
			End If

			searchDataRow = SortedRows(MaxIndex)
			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
				Return MaxIndex
				Exit Function
			End If

			' now search outwards from the last returned value.

			If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
				CurrentMin = thisPosition
				CurrentMax = CurrentMin + 1
			Else
				CurrentMin = CInt((MinIndex + MaxIndex) / 2)
				CurrentMax = CurrentMin + 1
			End If

			While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
				If (CurrentMin >= MinIndex) Then
					searchDataRow = SortedRows(CurrentMin)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
						Return CurrentMin
						Exit Function
					End If

					CurrentMin -= 1
				End If

				If (CurrentMax <= MaxIndex) Then
					searchDataRow = SortedRows(CurrentMax)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
						Return CurrentMax
						Exit Function
					End If

					CurrentMax += 1
				End If

			End While

			Return (-1)
			Exit Function

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
			Return (-1)
			Exit Function
		End Try

	End Function

#End Region

#Region " Form User Control Event Code (inc Grid Events)."

	' ---------------------------------------------------------------------------------
	' frmOptimise
	' ---------------------------------------------------------------------------------



	' ---------------------------------------------------------------------------------
	' Tab Instruments
	' ---------------------------------------------------------------------------------

	Private Sub InitialiseInstrumentsGrid()
		' *****************************************************************************************
		' One-Offinitialisation of the Instruments / returns Grid
		'
		' Add the Update flag Columns and establish custon grid styles.
		'
		' *****************************************************************************************

		Try
			Grid_Returns.Cols.Count = 0
			Grid_Returns.AutoGenerateColumns = True
			Grid_Returns.DataSource = New DSGroupsAggregated.tblGroupsAggregatedDataTable
			Grid_Returns.AutoGenerateColumns = False
			Grid_Returns.DataSource = Nothing

			' 
			Grid_Returns.Cols.Count += 1
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).Name = "GroupNewPercent"
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).DataType = GetType(Double)
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).Width = 30
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).Move(0)
			Grid_Returns.Cols(0).AllowEditing = False
			Grid_Returns.Cols(0).AllowMerging = False
			Grid_Returns.Cols(0).AllowResizing = True
			Grid_Returns.Cols(0).AllowDragging = True
			Grid_Returns.Cols(0).Caption = "NewPercent"
			' 
			Grid_Returns.Cols.Count += 1
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).Name = "Col_Updated"
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).DataType = GetType(String)
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).Width = 10
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).Move(0)
			Grid_Returns.Cols(0).AllowEditing = False
			Grid_Returns.Cols(0).AllowMerging = False
			Grid_Returns.Cols(0).AllowResizing = False
			Grid_Returns.Cols(0).AllowDragging = False
			Grid_Returns.Cols(0).Caption = ""

			Grid_Returns.Cols.Count += 1
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).Name = "Col_CustomUpdated"
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).DataType = GetType(String)
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).Width = 10
			Grid_Returns.Cols(Grid_Returns.Cols.Count - 1).Move(1)
			Grid_Returns.Cols(1).AllowEditing = False
			Grid_Returns.Cols(1).AllowMerging = False
			Grid_Returns.Cols(1).AllowResizing = False
			Grid_Returns.Cols(1).AllowDragging = False
			Grid_Returns.Cols(1).Caption = ""
		Catch ex As Exception
		End Try

		Dim ThisStyle As C1.Win.C1FlexGrid.CellStyle

		Try

			ThisStyle = Me.Grid_Returns.Styles("Genoa_PlainEdit")
			If (ThisStyle Is Nothing) Then
				ThisStyle = Me.Grid_Returns.Styles.Add("Genoa_PlainEdit")
			End If
			ThisStyle.ForeColor = Color.Black
			ThisStyle.Format = ""
			ThisStyle.Font = New Font(ThisStyle.Font, FontStyle.Regular)

		Catch ex As Exception

		End Try

	End Sub

	Private Sub ResetInstrumentsGrid()
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			Dim thisCol As C1.Win.C1FlexGrid.Column
			Dim thisColID As Integer

			Grid_Returns.Rows.Count = 1

			For thisColID = 2 To (Grid_Returns.Cols.Count - 1) ' Exclude Col_Update, Col_Custom_Update
				thisCol = Grid_Returns.Cols(thisColID)

				thisCol.Visible = False
				thisCol.AllowEditing = False
				thisCol.AllowMerging = False
				thisCol.Caption = thisCol.Name
				thisCol.Width = 80

				If thisCol.Caption.StartsWith("Group") AndAlso (thisCol.Caption.Length > 5) Then
					thisCol.Caption = thisCol.Caption.Substring(5)
				End If
			Next


			Grid_Returns.Cols("GroupIndexCode").AllowEditing = True
			Grid_Returns.Cols("GroupIndexE").AllowEditing = True
			Grid_Returns.Cols("GroupBetaE").AllowEditing = True
			Grid_Returns.Cols("GroupAlphaE").AllowEditing = True

			Grid_Returns.Cols("GroupSector").AllowEditing = True
			Grid_Returns.Cols("GroupLiquidity").AllowEditing = True
			Grid_Returns.Cols("GroupHolding").AllowEditing = True
			Grid_Returns.Cols("GroupTrade").AllowEditing = True
			Grid_Returns.Cols("GroupNewHolding").AllowEditing = True
			Grid_Returns.Cols("GroupPercent").AllowEditing = False
			Grid_Returns.Cols("GroupNewPercent").AllowEditing = False
			Grid_Returns.Cols("GroupUpperBound").AllowEditing = True
			Grid_Returns.Cols("GroupLowerBound").AllowEditing = True
			Grid_Returns.Cols("GroupFloor").AllowEditing = True
			Grid_Returns.Cols("GroupCap").AllowEditing = True
			Grid_Returns.Cols("GroupTradeSize").AllowEditing = True
			Grid_Returns.Cols("GroupScalingFactor").AllowEditing = True

			Grid_Returns.Cols("PertracName").Width = 200
			Grid_Returns.Cols("IndexName").Width = 200
			Grid_Returns.Cols("GroupIndexCode").Width = 200
			Grid_Returns.Cols("GroupPercent").Width = 60
			Grid_Returns.Cols("GroupNewPercent").Width = 60
			Grid_Returns.Cols("GroupHolding").Width = 100
			Grid_Returns.Cols("GroupNewHolding").Width = 100
			Grid_Returns.Cols("GroupFloor").Width = 40
			Grid_Returns.Cols("GroupCap").Width = 40

			Grid_Returns.Cols("GroupBeta").Format = "#,##0.0000"
			Grid_Returns.Cols("GroupAlpha").Format = "#,##0.0000%"
			Grid_Returns.Cols("GroupIndexE").Format = "#,##0.0000%"
			Grid_Returns.Cols("GroupBetaE").Format = "#,##0.0000"
			Grid_Returns.Cols("GroupAlphaE").Format = "#,##0.0000%"
			Grid_Returns.Cols("GroupExpectedReturn").Format = "#,##0.0000%"
			Grid_Returns.Cols("GroupStdErr").Format = "#,##0.0000"
			Grid_Returns.Cols("GroupScalingFactor").Format = "#,##0.00%"

			Grid_Returns.Cols("GroupLiquidity").Format = "#,##0"
			Grid_Returns.Cols("GroupHolding").Format = "#,##0"
			Grid_Returns.Cols("GroupTrade").Format = "#,##0"
			Grid_Returns.Cols("GroupNewHolding").Format = "#,##0"
			Grid_Returns.Cols("GroupPercent").Format = "#,##0.00%"
			Grid_Returns.Cols("GroupNewPercent").Format = "#,##0.00%"
			Grid_Returns.Cols("GroupUpperBound").Format = "#,##0.00%"
			Grid_Returns.Cols("GroupLowerBound").Format = "#,##0.00%"
			Grid_Returns.Cols("GroupTradeSize").Format = "#,##0"

			Grid_Returns.Cols("GroupIndexE").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupBetaE").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupAlphaE").Style.BackColor = Color.Honeydew

			Grid_Returns.Cols("GroupSector").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupLiquidity").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupHolding").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupTrade").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupNewHolding").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupUpperBound").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupLowerBound").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupFloor").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupCap").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupTradeSize").Style.BackColor = Color.Honeydew
			Grid_Returns.Cols("GroupScalingFactor").Style.BackColor = Color.Honeydew

			' Grid_Returns.Cols("GroupIndexCode").DataMap = MainForm.GetTblLookupCollection(RenaissanceGlobals.RenaissanceStandardDatasets.Mastername, "Mastername", "ID", "", False, True, True, 0, "")
			Grid_Returns.Cols("GroupIndexCode").DataMap = MainForm.PertracLookupCollection
			' Grid_Returns.Cols("GroupSector").DataMap = MainForm.GetTblLookupCollection(RenaissanceStandardDatasets.tblGroupItems, "GroupSector", "GroupSector", "", True, True, True, "", "")

			If (Grid_Returns.Rows(0).Height > 0) Then
				Grid_Returns.Rows.DefaultSize = Grid_Returns.Rows(0).Height
			End If

			Try
				PertracGridCombo.Height = Grid_Returns.Rows.DefaultSize

				Try
					RemoveHandler PertracGridCombo.SelectedValueChanged, AddressOf Me.IndexComboChanged
					'RemoveHandler PertracGridCombo.LostFocus, AddressOf Me.Grid_Returns_LostFocus
				Catch ex As Exception
				End Try
				AddHandler PertracGridCombo.SelectedValueChanged, AddressOf Me.IndexComboChanged
				'AddHandler PertracGridCombo.LostFocus, AddressOf Me.Grid_Returns_LostFocus

				Grid_Returns.Cols("GroupIndexCode").Editor = PertracGridCombo
			Catch ex As Exception
			End Try

			' Sector Combo
			Try
				If Not (TypeOf Grid_Returns.Cols("GroupSector").Editor Is CustomC1Combo) Then
					SectorCombo.DropDownStyle = ComboBoxStyle.DropDown
					SectorCombo.Height = Grid_Returns.Rows.DefaultSize
					MainForm.SetTblGenericCombo(SectorCombo, RenaissanceStandardDatasets.tblGroupItems, "GroupSector", "GroupSector", "", True, True, True, "", "")

					AddHandler SectorCombo.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
					AddHandler SectorCombo.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
					AddHandler SectorCombo.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
					AddHandler SectorCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

					'AddHandler SectorCombo.SelectedValueChanged, AddressOf Me.SectorComboChanged
					AddHandler SectorCombo.LostFocus, AddressOf SectorComboChanged
					' AddHandler SectorCombo.KeyPress, AddressOf Me.SectorComboChanged

					Grid_Returns.Cols("GroupSector").Editor = SectorCombo
				End If

			Catch ex As Exception

			End Try

			SetGrid_StandardOptimiserColumns()

		Catch ex As Exception
		End Try

	End Sub

	Private Sub Btn_ShowCarts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_ShowCarts.Click
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			Split_GroupReturns.Panel2Collapsed = Not Split_GroupReturns.Panel2Collapsed

			If Split_GroupReturns.Panel2Collapsed Then
				Btn_ShowCarts.Text = "Show Charts"
			Else
				Btn_ShowCarts.Text = "Hide Charts"
				Split_GroupReturns_Resize(Split_GroupReturns, New EventArgs)
			End If
		Catch ex As Exception
		End Try
	End Sub

  Private Sub btn_ShowGroupChart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_ShowGroupChart.Click
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      Dim newChartForm As frmViewChart

      newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
      newChartForm.GenoaParentForm = Me
      newChartForm.UpdateChartSub = AddressOf Me.PaintGroupReturnChart
      newChartForm.FormChart = Chart_VAMI ' Just for basic formatting.
      newChartForm.ParentChartList = GroupChartArrayLists
      newChartForm.Text = "Weighted Portfolio (NewHolding) VAMI"

      SyncLock GroupChartArrayLists
        GroupChartArrayLists.Add(newChartForm.DisplayedChart)
      End SyncLock

      For Each ThisSeries As Dundas.Charting.WinControl.Series In CType(newChartForm.DisplayedChart, Dundas.Charting.WinControl.Chart).Series
        ThisSeries.ChartArea = ""
      Next
      PaintGroupReturnChart(newChartForm.DisplayedChart)

    Catch ex As Exception
    End Try

  End Sub

	Private Sub Btn_SetSingleBenchmarkIndex_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_SetSingleBenchmarkIndex.Click
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (Me.Grid_Returns.Rows.Count > 1) AndAlso (Grid_Returns.Row > 0) Then
      SetSingleBenchmarkIndex(Grid_Returns.Row)
		End If

		Call Set_InstrumentsGridChanged(sender, New EventArgs)

	End Sub

  Private Sub SetSingleBenchmarkIndex(ByVal ReturnsRow As Integer)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim SelectedIndex As Integer
    Dim ThisItem As Object
    Dim PertracID As Integer = 0
    Dim Col_GroupBetaE As Integer = Grid_Returns.Cols("GroupBetaE").SafeIndex

    Try
      If (Combo_BenckmarkIndex.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_BenckmarkIndex.SelectedValue)) Then
        SelectedIndex = CInt(Combo_BenckmarkIndex.SelectedValue)

        If (Me.Grid_Returns.Rows.Count > 1) AndAlso (ReturnsRow > 0) Then
          Grid_Returns.Item(ReturnsRow, Grid_Returns.Cols("GroupIndexCode").SafeIndex) = SelectedIndex

          ThisItem = Grid_Returns.Item(ReturnsRow, Grid_Returns.Cols.IndexOf("GroupPertracCode"))
          If (ThisItem IsNot Nothing) AndAlso (IsNumeric(ThisItem)) Then
            PertracID = CInt(ThisItem)
          End If

          GetAlphaBetaAndStdError(PertracID, SelectedIndex, ReturnsRow)

          Grid_Returns.Item(ReturnsRow, Col_GroupBetaE) = 0

        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

	Private Sub Btn_SetAllBenchmarkIndex_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_SetAllBenchmarkIndex.Click
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

    SetAllBenchmarkIndexRows()

		Call Set_InstrumentsGridChanged(sender, New EventArgs)

	End Sub

  Private Sub SetAllBenchmarkIndexRows()
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim SelectedIndex As Integer
    Dim GridRow As Integer
    Dim Col_GroupIndexCode As Integer = Grid_Returns.Cols("GroupIndexCode").SafeIndex
    Dim Col_GroupIndexE As Integer = Grid_Returns.Cols("GroupIndexE").SafeIndex
    Dim ExistingReturn As Double = 0

    Try

      If (Combo_BenckmarkIndex.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_BenckmarkIndex.SelectedValue)) Then
        SelectedIndex = CInt(Combo_BenckmarkIndex.SelectedValue)

        If (SelectedIndex > 0) Then
          For GridRow = 1 To (Me.Grid_Returns.Rows.Count - 1)
            Try
              If (CInt(Grid_Returns.Item(GridRow, Col_GroupIndexCode)) = SelectedIndex) AndAlso (IsNumeric(Grid_Returns.Item(GridRow, Col_GroupIndexE))) Then
                ExistingReturn = Grid_Returns.Item(GridRow, Col_GroupIndexE)
                Exit For
              End If
            Catch ex As Exception
            End Try
          Next
        End If

        For GridRow = 1 To (Grid_Returns.Rows.Count - 1)
          If (IsNumeric(Grid_Returns.Item(GridRow, Col_GroupIndexCode)) = False) OrElse CInt(Grid_Returns.Item(GridRow, Col_GroupIndexCode)) <> SelectedIndex Then
            SetSingleBenchmarkIndex(GridRow)
            Grid_Returns.Item(GridRow, Col_GroupIndexE) = ExistingReturn
          End If
        Next
      End If

    Catch ex As Exception
    End Try
  End Sub

	Private Sub Menu_SetBenchmarkSingleRow(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (Me.Grid_Returns.Rows.Count > 1) AndAlso (InstrumentsGridClickMouseRow > 0) Then
      SetSingleBenchmarkIndex(InstrumentsGridClickMouseRow)
		End If

		Call Set_InstrumentsGridChanged(sender, New EventArgs)
	End Sub

	Private Sub Menu_SetStandardReturnColumns(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Call SetGrid_StandardReturnColumns()
	End Sub

	Private Sub Menu_SetStandardOptimiserColumns(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Call SetGrid_StandardOptimiserColumns()
	End Sub

	Private Sub Menu_SetBenchmarkAllRows(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

    SetAllBenchmarkIndexRows()

		Call Set_InstrumentsGridChanged(sender, New EventArgs)
	End Sub

	Private Sub SetThisExpectedBeta(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (Me.Grid_Returns.Rows.Count > 1) AndAlso (InstrumentsGridClickMouseRow > 0) Then
			Dim Col_BetaE As Integer = Grid_Returns.Cols("GroupBetaE").SafeIndex
			Dim Col_Beta As Integer = Grid_Returns.Cols("GroupBeta").SafeIndex

			Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_BetaE) = Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_Beta)

			Call Set_InstrumentsGridChanged(sender, New EventArgs)
		End If
	End Sub

	Private Sub SetAllExpectedBeta(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			Suppress_RiskAndReturn = True

			If (Me.Grid_Returns.Rows.Count > 1) Then
				Dim Col_BetaE As Integer = Grid_Returns.Cols("GroupBetaE").SafeIndex
				Dim Col_Beta As Integer = Grid_Returns.Cols("GroupBeta").SafeIndex
				Dim RowCount As Integer

				For RowCount = 1 To (Me.Grid_Returns.Rows.Count - 1)
					Grid_Returns.Item(RowCount, Col_BetaE) = Grid_Returns.Item(RowCount, Col_Beta)
				Next

				Call Set_InstrumentsGridChanged(sender, New EventArgs)

			End If

		Catch ex As Exception
		Finally
			Suppress_RiskAndReturn = False
		End Try

		Try
			Call Update_Group_RiskAndReturn(True)
		Catch ex As Exception
		End Try

	End Sub

	Private Sub SetThisExpectedAlpha(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (Me.Grid_Returns.Rows.Count > 1) AndAlso (InstrumentsGridClickMouseRow > 0) Then
			Dim Col_AlphaE As Integer = Grid_Returns.Cols("GroupAlphaE").SafeIndex
			Dim Col_Alpha As Integer = Grid_Returns.Cols("GroupAlpha").SafeIndex

			Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_AlphaE) = Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_Alpha)

			Call Set_InstrumentsGridChanged(sender, New EventArgs)
		End If
	End Sub

	Private Sub SetAllExpectedAlpha(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************
		Try

			If (Me.Grid_Returns.Rows.Count > 1) Then
				Dim Col_AlphaE As Integer = Grid_Returns.Cols("GroupAlphaE").SafeIndex
				Dim Col_Alpha As Integer = Grid_Returns.Cols("GroupAlpha").SafeIndex
				Dim RowCount As Integer

				Suppress_RiskAndReturn = True

				For RowCount = 1 To (Me.Grid_Returns.Rows.Count - 1)
					Grid_Returns.Item(RowCount, Col_AlphaE) = Grid_Returns.Item(RowCount, Col_Alpha)
				Next

				Call Set_InstrumentsGridChanged(sender, New EventArgs)
			End If

		Catch ex As Exception
		Finally
			Suppress_RiskAndReturn = False
		End Try

		Try
			Call Update_Group_RiskAndReturn(True)
		Catch ex As Exception
		End Try

	End Sub

	Private Sub SetThisTradeZero(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (Me.Grid_Returns.Rows.Count > 1) AndAlso (InstrumentsGridClickMouseRow > 0) Then
			Dim Col_GroupTrade As Integer = Grid_Returns.Cols("GroupTrade").SafeIndex

			Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupTrade) = 0

			Call Set_InstrumentsGridChanged(sender, New EventArgs)
		End If
	End Sub

	Private Sub SetAllTradeZero(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************
		Try

			If (Me.Grid_Returns.Rows.Count > 1) Then
				Dim Col_GroupTrade As Integer = Grid_Returns.Cols("GroupTrade").SafeIndex
				Dim RowCount As Integer

				Suppress_RiskAndReturn = True

				For RowCount = 1 To (Me.Grid_Returns.Rows.Count - 1)
					Grid_Returns.Item(RowCount, Col_GroupTrade) = 0
				Next

				Call Set_InstrumentsGridChanged(sender, New EventArgs)
			End If

		Catch ex As Exception
		Finally
			Suppress_RiskAndReturn = False
		End Try

		Try
			Call Update_Group_RiskAndReturn(True)
		Catch ex As Exception
		End Try

	End Sub

  Private Sub SetLowerBounds(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************
    Try

      If (Me.Grid_Returns.Rows.Count > 1) Then
        Dim Col_GroupLowerBound As Integer = Grid_Returns.Cols("GroupLowerBound").SafeIndex
        Dim RowCount As Integer

        Suppress_RiskAndReturn = True

        For RowCount = 1 To (Me.Grid_Returns.Rows.Count - 1)
          If (RowCount <> InstrumentsGridClickMouseRow) Then
            Grid_Returns.Item(RowCount, Col_GroupLowerBound) = Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupLowerBound)
          End If
        Next

        Call Set_InstrumentsGridChanged(sender, New EventArgs)
      End If

    Catch ex As Exception
    Finally
      Suppress_RiskAndReturn = False
    End Try

    Try
      Call Update_Group_RiskAndReturn(True)
    Catch ex As Exception
    End Try

  End Sub

  Private Sub SetUpperBounds(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************
    Try

      If (Me.Grid_Returns.Rows.Count > 1) Then
        Dim Col_GroupUpperBound As Integer = Grid_Returns.Cols("GroupUpperBound").SafeIndex
        Dim RowCount As Integer

        Suppress_RiskAndReturn = True

        For RowCount = 1 To (Me.Grid_Returns.Rows.Count - 1)
          If (RowCount <> InstrumentsGridClickMouseRow) Then
            Grid_Returns.Item(RowCount, Col_GroupUpperBound) = Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupUpperBound)
          End If
        Next

        Call Set_InstrumentsGridChanged(sender, New EventArgs)
      End If

    Catch ex As Exception
    Finally
      Suppress_RiskAndReturn = False
    End Try

    Try
      Call Update_Group_RiskAndReturn(True)
    Catch ex As Exception
    End Try

  End Sub

	Private Sub SetThisHolding(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (Me.Grid_Returns.Rows.Count > 1) AndAlso (InstrumentsGridClickMouseRow > 0) Then
			Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
			Dim Col_GroupTrade As Integer = Grid_Returns.Cols("GroupTrade").SafeIndex
			Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex

			Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupHolding) = Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupNewHolding)
			Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupTrade) = 0

			Call Set_InstrumentsGridChanged(sender, New EventArgs)
		End If
	End Sub

	Private Sub SetALLHolding(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************
		Try

			If (Me.Grid_Returns.Rows.Count > 1) Then
				Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
				Dim Col_GroupTrade As Integer = Grid_Returns.Cols("GroupTrade").SafeIndex
				Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
				Dim RowCount As Integer

				Suppress_RiskAndReturn = True

				For RowCount = 1 To (Me.Grid_Returns.Rows.Count - 1)
					Grid_Returns.Item(RowCount, Col_GroupHolding) = Grid_Returns.Item(RowCount, Col_GroupNewHolding)
					Grid_Returns.Item(RowCount, Col_GroupTrade) = 0
				Next

				Call Set_InstrumentsGridChanged(sender, New EventArgs)
			End If

		Catch ex As Exception
		Finally
			Suppress_RiskAndReturn = False
		End Try

		Try
			Call Update_Group_RiskAndReturn(True)
		Catch ex As Exception
		End Try

	End Sub

	Private Sub SetThisNewHolding(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (Me.Grid_Returns.Rows.Count > 1) AndAlso (InstrumentsGridClickMouseRow > 0) Then
			Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
			Dim Col_GroupTrade As Integer = Grid_Returns.Cols("GroupTrade").SafeIndex
			Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex

			Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupNewHolding) = Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupHolding)
			Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupTrade) = 0

			Call Set_InstrumentsGridChanged(sender, New EventArgs)
		End If
	End Sub

	Private Sub SetALLNewHolding(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************
		Try

			If (Me.Grid_Returns.Rows.Count > 1) Then
				Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
				Dim Col_GroupTrade As Integer = Grid_Returns.Cols("GroupTrade").SafeIndex
				Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
				Dim RowCount As Integer

				Suppress_RiskAndReturn = True

				For RowCount = 1 To (Me.Grid_Returns.Rows.Count - 1)
					Grid_Returns.Item(RowCount, Col_GroupNewHolding) = Grid_Returns.Item(RowCount, Col_GroupHolding)
					Grid_Returns.Item(RowCount, Col_GroupTrade) = 0
				Next

				Call Set_InstrumentsGridChanged(sender, New EventArgs)
			End If

		Catch ex As Exception
		Finally
			Suppress_RiskAndReturn = False
		End Try

		Try
			Call Update_Group_RiskAndReturn(True)
		Catch ex As Exception
		End Try

	End Sub

	Private Sub PopulateGetHoldingMenu(ByVal TargetMenuItem As ToolStripMenuItem, ByVal HoldingSelection As SelectionEnum, ByVal NewHolding As Boolean)	 ' = SelectionEnum.SingleRow
		' *****************************************************************************************
		' Populate this menu with possible Venice Funds.
		'
		' *****************************************************************************************

		Dim thisFundsDS As RenaissanceDataClass.DSFund
		Dim thisFundView As DataView

		Dim FundCounter As Integer
		Dim ThisFund As DSFund.tblFundRow

		If TargetMenuItem.HasDropDownItems Then
			TargetMenuItem.DropDownItems.Clear()
		End If

		thisFundsDS = MainForm.Load_Table(RenaissanceStandardDatasets.tblFund)
		thisFundView = New DataView(thisFundsDS.tblFund, "True", "FundName", DataViewRowState.CurrentRows)

		' Get appropriate callback
		Dim MenuCallback As System.EventHandler


		If (NewHolding) Then

			Select Case HoldingSelection
				Case SelectionEnum.MultipleRows
					MenuCallback = AddressOf GetSelectionNewHoldingFromVenice

				Case SelectionEnum.AllRows
					MenuCallback = AddressOf GetAllNewHoldingFromVenice

				Case Else
					MenuCallback = AddressOf GetSingleNewHoldingFromVenice

			End Select

		Else

			Select Case HoldingSelection
				Case SelectionEnum.MultipleRows
					MenuCallback = AddressOf GetSelectionHoldingFromVenice

				Case SelectionEnum.AllRows
					MenuCallback = AddressOf GetAllHoldingFromVenice

				Case Else
					MenuCallback = AddressOf GetSingleHoldingFromVenice

			End Select

		End If

		For FundCounter = 0 To (thisFundView.Count - 1)
			ThisFund = thisFundView.Item(FundCounter).Row

			TargetMenuItem.DropDownItems.Add(ThisFund.FundName, Nothing, MenuCallback).Tag = ThisFund.FundID

		Next

	End Sub

	Private Sub GetSingleHoldingFromVenice(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		' OK the Line to fetch is 'InstrumentsGridClickMouseRow'

		Try
			If (InstrumentsGridClickMouseRow <= 0) OrElse (InstrumentsGridClickMouseRow >= Me.Grid_Returns.Rows.Count) Then
				Exit Sub
			End If
		Catch ex As Exception
			Exit Sub
		End Try

		Dim FundID As Integer = 0
		Dim PertracID As Integer = 0
		Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Dim HoldingValue As Double = 0

		Try
			Grid_Returns.Cursor = Cursors.WaitCursor

			Application.DoEvents()

			FundID = CInt(CType(sender, ToolStripItem).Tag)
			PertracID = CInt(Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupPertracCode))

			If (Me.tblVeniceHoldings Is Nothing) OrElse (tblVeniceHoldings_FundID <> FundID) Then
				tblVeniceHoldings = GetVeniceHoldingsData(myConnection, FundID, GetVeniceHoldingsValueDate, GetVeniceHoldingsLookthroughCheckboxChecked, MainForm.Main_Knowledgedate)
				tblVeniceHoldings_FundID = FundID
			End If

			HoldingValue = Math.Round(GetAggregatedFundHolding(myConnection, tblVeniceHoldings, PertracID, MainForm.Main_Knowledgedate))

		Catch ex As Exception
		Finally
			Grid_Returns.Cursor = Cursors.Default
		End Try

		Try
			Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupHolding) = HoldingValue
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetSelectionHoldingFromVenice(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (Not Grid_Returns.Selection.IsValid) Then
			Exit Sub
		End If

		Dim FundID As Integer = 0
		Dim PertracID As Integer = 0
		Dim InstrumentCounter As Integer
		Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Dim HoldingValue As Double = 0

		Try
			Grid_Returns.Cursor = Cursors.WaitCursor
			Suppress_RiskAndReturn = True

			FundID = CInt(CType(sender, ToolStripItem).Tag)

			For InstrumentCounter = Grid_Returns.Selection.r1 To Grid_Returns.Selection.r2
				Application.DoEvents()

				If (InstrumentCounter > 0) Then
					Try
						PertracID = CInt(Grid_Returns.Item(InstrumentCounter, Col_GroupPertracCode))

						If (Me.tblVeniceHoldings Is Nothing) OrElse (tblVeniceHoldings_FundID <> FundID) Then
							tblVeniceHoldings = GetVeniceHoldingsData(myConnection, FundID, GetVeniceHoldingsValueDate, GetVeniceHoldingsLookthroughCheckboxChecked, MainForm.Main_Knowledgedate)
							tblVeniceHoldings_FundID = FundID
						End If

						HoldingValue = Math.Round(GetAggregatedFundHolding(myConnection, tblVeniceHoldings, PertracID, MainForm.Main_Knowledgedate))

					Catch ex As Exception
						HoldingValue = 0
					End Try

					Grid_Returns.Item(InstrumentCounter, Col_GroupHolding) = HoldingValue
				End If
			Next
		Catch ex As Exception
		Finally
			Grid_Returns.Cursor = Cursors.Default
			Suppress_RiskAndReturn = False
		End Try

		Try
			Call Update_Group_RiskAndReturn(True)
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetAllHoldingFromVenice(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim FundID As Integer = 0
		Dim PertracID As Integer = 0
		Dim InstrumentCounter As Integer
		Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Dim HoldingValue As Double = 0

		Try
			Grid_Returns.Cursor = Cursors.WaitCursor
			Suppress_RiskAndReturn = True

			FundID = CInt(CType(sender, ToolStripItem).Tag)

			For InstrumentCounter = 1 To (Me.Grid_Returns.Rows.Count - 1)
				Try
					Application.DoEvents()

					PertracID = CInt(Grid_Returns.Item(InstrumentCounter, Col_GroupPertracCode))

					If (Me.tblVeniceHoldings Is Nothing) OrElse (tblVeniceHoldings_FundID <> FundID) Then
						tblVeniceHoldings = GetVeniceHoldingsData(myConnection, FundID, GetVeniceHoldingsValueDate, GetVeniceHoldingsLookthroughCheckboxChecked, MainForm.Main_Knowledgedate)
						tblVeniceHoldings_FundID = FundID
					End If

					HoldingValue = Math.Round(GetAggregatedFundHolding(myConnection, tblVeniceHoldings, PertracID, MainForm.Main_Knowledgedate))

				Catch ex As Exception
					HoldingValue = 0
				End Try

				Grid_Returns.Item(InstrumentCounter, Col_GroupHolding) = HoldingValue
			Next
		Catch ex As Exception
		Finally
			Suppress_RiskAndReturn = False
			Grid_Returns.Cursor = Cursors.Default
		End Try

		Try
			Call Update_Group_RiskAndReturn(True)
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetSingleNewHoldingFromVenice(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		' OK the Line to fetch is 'InstrumentsGridClickMouseRow'

		Try
			If (InstrumentsGridClickMouseRow <= 0) OrElse (InstrumentsGridClickMouseRow >= Me.Grid_Returns.Rows.Count) Then
				Exit Sub
			End If
		Catch ex As Exception
			Exit Sub
		End Try

		Dim FundID As Integer = 0
		Dim PertracID As Integer = 0
		Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Dim HoldingValue As Double = 0

		Try
			Grid_Returns.Cursor = Cursors.WaitCursor

			Application.DoEvents()

			FundID = CInt(CType(sender, ToolStripItem).Tag)
			PertracID = CInt(Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupPertracCode))

			If (Me.tblVeniceHoldings Is Nothing) OrElse (tblVeniceHoldings_FundID <> FundID) Then
				tblVeniceHoldings = GetVeniceHoldingsData(myConnection, FundID, GetVeniceHoldingsValueDate, GetVeniceHoldingsLookthroughCheckboxChecked, MainForm.Main_Knowledgedate)
				tblVeniceHoldings_FundID = FundID
			End If

			HoldingValue = Math.Round(GetAggregatedFundHolding(myConnection, tblVeniceHoldings, PertracID, MainForm.Main_Knowledgedate))

		Catch ex As Exception
		Finally
			Grid_Returns.Cursor = Cursors.Default
		End Try

		Try
			Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupNewHolding) = HoldingValue
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetSelectionNewHoldingFromVenice(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (Not Grid_Returns.Selection.IsValid) Then
			Exit Sub
		End If

		Dim FundID As Integer = 0
		Dim PertracID As Integer = 0
		Dim InstrumentCounter As Integer
		Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Dim HoldingValue As Double = 0

		Try
			Grid_Returns.Cursor = Cursors.WaitCursor
			Suppress_RiskAndReturn = True

			FundID = CInt(CType(sender, ToolStripItem).Tag)

			For InstrumentCounter = Grid_Returns.Selection.r1 To Grid_Returns.Selection.r2
				Application.DoEvents()

				If (InstrumentCounter > 0) Then
					Try

						PertracID = CInt(Grid_Returns.Item(InstrumentCounter, Col_GroupPertracCode))

						If (Me.tblVeniceHoldings Is Nothing) OrElse (tblVeniceHoldings_FundID <> FundID) Then
							tblVeniceHoldings = GetVeniceHoldingsData(myConnection, FundID, GetVeniceHoldingsValueDate, GetVeniceHoldingsLookthroughCheckboxChecked, MainForm.Main_Knowledgedate)
							tblVeniceHoldings_FundID = FundID
						End If

						HoldingValue = Math.Round(GetAggregatedFundHolding(myConnection, tblVeniceHoldings, PertracID, MainForm.Main_Knowledgedate))

					Catch ex As Exception
						HoldingValue = 0
					End Try

					Grid_Returns.Item(InstrumentCounter, Col_GroupNewHolding) = HoldingValue
				End If
			Next
		Catch ex As Exception
		Finally
			Grid_Returns.Cursor = Cursors.Default
			Suppress_RiskAndReturn = False
		End Try

		Try
			Call Update_Group_RiskAndReturn(True)
		Catch ex As Exception
		End Try

	End Sub

	Private Sub GetAllNewHoldingFromVenice(ByVal sender As Object, ByVal e As System.EventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim FundID As Integer = 0
		Dim PertracID As Integer = 0
		Dim InstrumentCounter As Integer
		Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Dim HoldingValue As Double = 0

		Try
			Grid_Returns.Cursor = Cursors.WaitCursor
			Suppress_RiskAndReturn = True

			FundID = CInt(CType(sender, ToolStripItem).Tag)

			For InstrumentCounter = 1 To (Me.Grid_Returns.Rows.Count - 1)
				Try
					Me.Cursor = Cursors.WaitCursor
					Application.DoEvents()

					PertracID = CInt(Grid_Returns.Item(InstrumentCounter, Col_GroupPertracCode))

					If (Me.tblVeniceHoldings Is Nothing) OrElse (tblVeniceHoldings_FundID <> FundID) Then
						tblVeniceHoldings = GetVeniceHoldingsData(myConnection, FundID, GetVeniceHoldingsValueDate, GetVeniceHoldingsLookthroughCheckboxChecked, MainForm.Main_Knowledgedate)
						tblVeniceHoldings_FundID = FundID
					End If

					HoldingValue = Math.Round(GetAggregatedFundHolding(myConnection, tblVeniceHoldings, PertracID, MainForm.Main_Knowledgedate))

				Catch ex As Exception
					HoldingValue = 0
				Finally
					Me.Cursor = Cursors.Default
				End Try

				Grid_Returns.Item(InstrumentCounter, Col_GroupNewHolding) = HoldingValue
			Next
		Catch ex As Exception
		Finally
			Suppress_RiskAndReturn = False
			Grid_Returns.Cursor = Cursors.Default
		End Try

		Try
			Call Update_Group_RiskAndReturn(True)
		Catch ex As Exception
		End Try

	End Sub


	Private Sub Grid_Returns_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Returns.KeyDown
		' *************************************************************************************
		'
		'
		'
		' *************************************************************************************
		Try
			If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), False, False)
			End If
		Catch ex As Exception
		End Try
	End Sub

	Private Sub Grid_Returns_CellChanged(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_Returns.CellChanged
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

    If InPaint Then
      Exit Sub
    End If

		If (e.Row <= 0) OrElse (e.Col <= 1) Then
			Exit Sub
		End If

		Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Dim Col_GroupIndexCode As Integer = Grid_Returns.Cols("GroupIndexCode").SafeIndex
		' Dim Col_GroupAlpha As Integer = Grid_Returns.Cols("GroupAlpha").SafeIndex ' Not Used
		' Dim Col_GroupBeta As Integer = Grid_Returns.Cols("GroupBeta").SafeIndex   ' Not Used
		Dim Col_GroupIndexE As Integer = Grid_Returns.Cols("GroupIndexE").SafeIndex
		Dim Col_GroupBetaE As Integer = Grid_Returns.Cols("GroupBetaE").SafeIndex
		Dim Col_GroupAlphaE As Integer = Grid_Returns.Cols("GroupAlphaE").SafeIndex
		Dim Col_GroupExpectedReturn As Integer = Grid_Returns.Cols("GroupExpectedReturn").SafeIndex
		Dim Col_GroupStdErr As Integer = Grid_Returns.Cols("GroupStdErr").SafeIndex
		Dim Col_GroupScalingFactor As Integer = Grid_Returns.Cols("GroupScalingFactor").SafeIndex
		Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
		Dim Col_GroupTrade As Integer = Grid_Returns.Cols("GroupTrade").SafeIndex
		Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
		Dim Col_GroupPercent As Integer = Grid_Returns.Cols("GroupPercent").SafeIndex
		Dim Col_GroupNewPercent As Integer = Grid_Returns.Cols("GroupNewPercent").SafeIndex
		Dim Col_GroupSector As Integer = Grid_Returns.Cols("GroupSector").SafeIndex
		Dim Col_GroupLiquidity As Integer = Grid_Returns.Cols("GroupLiquidity").SafeIndex
		Dim Col_GroupCap As Integer = Grid_Returns.Cols("GroupCap").SafeIndex
		Dim Col_GroupFloor As Integer = Grid_Returns.Cols("GroupFloor").SafeIndex
		Dim Col_GroupUpperBound As Integer = Grid_Returns.Cols("GroupUpperBound").SafeIndex
		Dim Col_GroupLowerBound As Integer = Grid_Returns.Cols("GroupLowerBound").SafeIndex

		Dim ThisPertracCode As Integer
		Dim ThisIndexCode As Integer
		Dim ThisIndexExpectedBeta As Double
		Dim ThisIndexExpectedAlpha As Double
		Dim ThisIndexExpectedReturn As Double
		Dim RowCounter As Integer

		Try

			Select Case e.Col

				Case Col_GroupIndexE
					' Propagate Index Return 

					If Me.Grid_Returns.Row = e.Row Then	' Prevent recursive loop....
						ThisIndexCode = CInt(Grid_Returns.Item(e.Row, Col_GroupIndexCode))
						ThisIndexExpectedReturn = CDbl(Grid_Returns.Item(e.Row, Col_GroupIndexE))
						If (ThisIndexCode > 0) Then
							For RowCounter = 1 To (Me.Grid_Returns.Rows.Count - 1)
								If (RowCounter <> e.Row) Then
									Try
										If (CInt(Grid_Returns.Item(RowCounter, Col_GroupIndexCode)) = ThisIndexCode) AndAlso (IsNumeric(Grid_Returns.Item(RowCounter, Col_GroupIndexE))) AndAlso (CDbl(Grid_Returns.Item(RowCounter, Col_GroupIndexE)) <> ThisIndexExpectedReturn) Then
											Grid_Returns.Item(RowCounter, Col_GroupIndexE) = ThisIndexExpectedReturn
										End If
									Catch ex As Exception
									End Try
								End If
							Next
						End If
					End If

					ThisIndexExpectedReturn = CDbl(Grid_Returns.Item(e.Row, Col_GroupIndexE))
					ThisIndexExpectedAlpha = CDbl(Grid_Returns.Item(e.Row, Col_GroupAlphaE))
					ThisIndexExpectedBeta = CDbl(Grid_Returns.Item(e.Row, Col_GroupBetaE))

					Grid_Returns.Item(e.Row, Col_GroupExpectedReturn) = (ThisIndexExpectedBeta * ThisIndexExpectedReturn) + ThisIndexExpectedAlpha

          ThisPertracCode = CInt(Grid_Returns.Item(e.Row, Col_GroupPertracCode))

          Set_LineChart_RateReturnsLine(MainForm, Chart_VAMI, 1, 0, CDbl(Grid_Returns.Item(e.Row, Col_GroupExpectedReturn)), MainForm.StatFunctions.AnnualPeriodCount(MainForm.PertracData.GetPertracDataPeriod(ThisPertracCode)), "")
					Set_LineChart_StaticReturnsLine(MainForm, Chart_MonthlyReturns, 1, 0, CDbl(Grid_Returns.Item(e.Row, Col_GroupExpectedReturn)) * 100, "")

				Case Col_GroupBetaE, Col_GroupAlphaE
					' Re-Calculate results Columns

					ThisIndexExpectedReturn = CDbl(Grid_Returns.Item(e.Row, Col_GroupIndexE))
					ThisIndexExpectedAlpha = CDbl(Grid_Returns.Item(e.Row, Col_GroupAlphaE))
					ThisIndexExpectedBeta = CDbl(Grid_Returns.Item(e.Row, Col_GroupBetaE))

					Grid_Returns.Item(e.Row, Col_GroupExpectedReturn) = (ThisIndexExpectedBeta * ThisIndexExpectedReturn) + ThisIndexExpectedAlpha

          ThisPertracCode = CInt(Grid_Returns.Item(e.Row, Col_GroupPertracCode))

          Set_LineChart_RateReturnsLine(MainForm, Chart_VAMI, 1, 0, CDbl(Grid_Returns.Item(e.Row, Col_GroupExpectedReturn)), MainForm.StatFunctions.AnnualPeriodCount(MainForm.PertracData.GetPertracDataPeriod(ThisPertracCode)), "")
					Set_LineChart_StaticReturnsLine(MainForm, Chart_MonthlyReturns, 1, 0, CDbl(Grid_Returns.Item(e.Row, Col_GroupExpectedReturn)) * 100, "")

				Case Col_GroupHolding

					Dim ThisHolding As Double = CDbl(Grid_Returns.Item(e.Row, Col_GroupHolding))
					Dim ThisTrade As Double = CDbl(Grid_Returns.Item(e.Row, Col_GroupTrade))
					Dim ThisNewHolding As Double = CDbl(Grid_Returns.Item(e.Row, Col_GroupNewHolding))

					If Math.Abs(ThisHolding - (ThisNewHolding - ThisTrade)) > GENOA_RoundingError Then
						' Holding has changed

						Grid_Returns.Item(e.Row, Col_GroupNewHolding) = Math.Round(ThisHolding + ThisTrade, GENOA_HoldingPrecision)

            If (GenericUpdateObject IsNot Nothing) Then
              GenericUpdateObject.FormToUpdate = True
              Update_ChartsToUpdate = True
            Else
              Update_ChartsToUpdate = True
              FormUpdate_Tick()
            End If

            'Call Update_Group_Percentages(True)
            'Call Update_Group_RiskAndReturn(True)
            'Call UpdateMarginalsGrid()
            'Call CalculateConstraintExposures()

					End If

				Case Col_GroupTrade

					Dim ThisHolding As Double = CDbl(Grid_Returns.Item(e.Row, Col_GroupHolding))
					Dim ThisTrade As Double = CDbl(Grid_Returns.Item(e.Row, Col_GroupTrade))
					Dim ThisNewHolding As Double = CDbl(Grid_Returns.Item(e.Row, Col_GroupNewHolding))

					If Math.Abs(ThisHolding - (ThisNewHolding - ThisTrade)) > GENOA_RoundingError Then
						' Trade has changed

						Grid_Returns.Item(e.Row, Col_GroupNewHolding) = Math.Round(ThisHolding + ThisTrade, GENOA_HoldingPrecision)

            If (GenericUpdateObject IsNot Nothing) Then
              GenericUpdateObject.FormToUpdate = True
              Update_ChartsToUpdate = True
            Else
              Update_ChartsToUpdate = True
              FormUpdate_Tick()
            End If

            'Call Update_Group_Percentages(True)
            'Call Update_Group_RiskAndReturn(True)
            'Call UpdateMarginalsGrid()
            'Call CalculateConstraintExposures()

					End If

				Case Col_GroupNewHolding

					Dim ThisHolding As Double = CDbl(Grid_Returns.Item(e.Row, Col_GroupHolding))
					Dim ThisTrade As Double = CDbl(Grid_Returns.Item(e.Row, Col_GroupTrade))
					Dim ThisNewHolding As Double = CDbl(Grid_Returns.Item(e.Row, Col_GroupNewHolding))

					If Math.Abs(ThisHolding - (ThisNewHolding - ThisTrade)) > GENOA_RoundingError Then
						' New Holding has changed

						Grid_Returns.Item(e.Row, Col_GroupTrade) = Math.Round(ThisNewHolding - ThisHolding, GENOA_HoldingPrecision)

            If (GenericUpdateObject IsNot Nothing) Then
              GenericUpdateObject.FormToUpdate = True
              Update_ChartsToUpdate = True
            Else
              Update_ChartsToUpdate = True
              FormUpdate_Tick()
            End If

            'Call Update_Group_Percentages(True)
            'Call Update_Group_RiskAndReturn(True)
            'Call UpdateMarginalsGrid()
            'Call CalculateConstraintExposures()

					End If

				Case Col_GroupSector

					Grid_Returns.Item(e.Row, e.Col) = SectorCombo.SelectedValue
					ConstraintWorker.InvalidateCustomFieldData()
					Call UpdateMarginalsGrid()

				Case Col_GroupLiquidity

					ConstraintWorker.InvalidateCustomFieldData()
					Call UpdateMarginalsGrid()

				Case Col_GroupIndexCode

					If Me.Grid_Returns.Row = e.Row Then	' Prevent recursive loop....
						ThisIndexCode = CInt(Grid_Returns.Item(e.Row, Col_GroupIndexCode))

						If (ThisIndexCode > 0) Then
							For RowCounter = 1 To (Me.Grid_Returns.Rows.Count - 1)
								If (RowCounter <> e.Row) Then
									Try
										If (CInt(Grid_Returns.Item(RowCounter, Col_GroupIndexCode)) = ThisIndexCode) AndAlso (IsNumeric(Grid_Returns.Item(RowCounter, Col_GroupIndexE))) Then
											Grid_Returns.Item(e.Row, Col_GroupIndexE) = Grid_Returns.Item(RowCounter, Col_GroupIndexE)
											Exit For
										End If
									Catch ex As Exception
									End Try
								End If
							Next
						End If
					End If

				Case Col_GroupExpectedReturn

          If (GenericUpdateObject IsNot Nothing) Then
            GenericUpdateObject.FormToUpdate = True
            Update_ChartsToUpdate = True
          Else
            Update_ChartsToUpdate = True
            FormUpdate_Tick()
          End If

          'Call Update_Group_RiskAndReturn(True)
          'Call UpdateMarginalsGrid()

				Case Col_GroupCap
					Try
						If CBool(Grid_Returns.Item(e.Row, e.Col)) Then
							If CalculateCapFromNewPercent Then
								Grid_Returns.Item(e.Row, Col_GroupUpperBound) = Grid_Returns.Item(e.Row, Col_GroupNewPercent)
							Else
								Grid_Returns.Item(e.Row, Col_GroupUpperBound) = Grid_Returns.Item(e.Row, Col_GroupPercent)
							End If
							ConstraintWorker.InvalidateConstraintDetails()
						End If
					Catch ex As Exception
					End Try

				Case Col_GroupFloor
					Try
						If CBool(Grid_Returns.Item(e.Row, e.Col)) Then
							If CalculateCapFromNewPercent Then
								Grid_Returns.Item(e.Row, Col_GroupLowerBound) = Grid_Returns.Item(e.Row, Col_GroupNewPercent)
							Else
								Grid_Returns.Item(e.Row, Col_GroupLowerBound) = Grid_Returns.Item(e.Row, Col_GroupPercent)
							End If
							ConstraintWorker.InvalidateConstraintDetails()
						End If
					Catch ex As Exception
					End Try

				Case Col_GroupLowerBound
					Try
						ConstraintWorker.InvalidateConstraintDetails()
						Call UpdateMarginalsGrid()
					Catch ex As Exception
					End Try

				Case Col_GroupUpperBound
					Try
						ConstraintWorker.InvalidateConstraintDetails()
						Call UpdateMarginalsGrid()
					Catch ex As Exception
					End Try

				Case Col_GroupScalingFactor
					' Scaling factor changed.
					' 
					ThisPertracCode = CInt(Grid_Returns.Item(e.Row, Col_GroupPertracCode))

					If (ScalingFactorHash.ContainsKey(ThisPertracCode)) Then
						If (Math.Abs(ScalingFactorHash(ThisPertracCode) - CDbl(Grid_Returns.Item(e.Row, Col_GroupScalingFactor))) > EPSILON) Then
							ScalingFactorHash(ThisPertracCode) = CDbl(Grid_Returns.Item(e.Row, Col_GroupScalingFactor))

							If (Radio_Covariance_UseLiveMatrix.Checked) Then
                CalculateLiveCovarianceMatrix(DefaultStatsDatePeriod)
							End If

							' Call Update_Group_Percentages(True)   ' Not Needed
							' Call Update_Group_RiskAndReturn(True) ' Performed in CalculateLiveCovarianceMatrix()

							Call UpdateMarginalsGrid()
							Results_RefreshRiskReturnCharts()

              Dim ThisDatePeriod As DealingPeriod
              ThisDatePeriod = MainForm.PertracData.GetPertracDataPeriod(ThisPertracCode)

              Set_LineChart(MainForm, ThisDatePeriod, ThisPertracCode, Chart_VAMI, 0, "", MainForm.StatFunctions.AnnualPeriodCount(ThisDatePeriod), 1, ScalingFactorHash(ThisPertracCode), Now.AddMonths(-36), Now())
              Set_LineChart_RateReturnsLine(MainForm, Chart_VAMI, 1, 0, CDbl(Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols("GroupExpectedReturn").SafeIndex)), MainForm.StatFunctions.AnnualPeriodCount(ThisDatePeriod), "")
              Set_RollingReturnChart(MainForm, ThisDatePeriod, ThisPertracCode, Chart_MonthlyReturns, 0, "", MainForm.StatFunctions.AnnualPeriodCount(ThisDatePeriod), 1, ScalingFactorHash(ThisPertracCode), Now.AddMonths(-36), Now())
              Set_LineChart_StaticReturnsLine(MainForm, Chart_MonthlyReturns, 1, 0, CDbl(Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols("GroupExpectedReturn").SafeIndex)) * 100, "")

						End If
					End If

				Case Else

			End Select

		Catch ex As Exception
			Grid_Returns.Item(e.Row, Col_GroupExpectedReturn) = 0
			Grid_Returns.Item(e.Row, Col_GroupStdErr) = 0
		Finally
			If Not (InstrumentsGridChanged) Then
				Call Set_InstrumentsGridChanged(Grid_Returns, New EventArgs)
			End If

			If (Grid_Returns.Cols(e.Col).Name.StartsWith("Custom_")) Then
				Grid_Returns.Item(e.Row, 1) = "x"
			Else
				Grid_Returns.Item(e.Row, 0) = "x"
			End If
		End Try

	End Sub

	Private Sub Grid_Returns_EnterCell(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Returns.EnterCell
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Static Dim lastPertracID As Integer

		Dim ThisPertracID As Integer
		Dim ThisScalingFactor As Double

		If (InPaint = False) AndAlso (Grid_Returns.Row >= 0) AndAlso (Grid_Returns.Col >= 0) Then
			Try
				Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
				Dim Col_GroupSector As Integer = Grid_Returns.Cols("GroupSector").SafeIndex
				Dim Col_GroupCap As Integer = Grid_Returns.Cols("GroupCap").SafeIndex
				Dim Col_GroupFloor As Integer = Grid_Returns.Cols("GroupFloor").SafeIndex
				Dim Col_GroupUpperBound As Integer = Grid_Returns.Cols("GroupUpperBound").SafeIndex
				Dim Col_GroupLowerBound As Integer = Grid_Returns.Cols("GroupLowerBound").SafeIndex
				Dim Col_GroupScalingFactor As Integer = Grid_Returns.Cols("GroupScalingFactor").SafeIndex

				If (IsNumeric(Grid_Returns.Item(Grid_Returns.Row, Col_GroupPertracCode))) Then
					ThisPertracID = Grid_Returns.Item(Grid_Returns.Row, Col_GroupPertracCode)
					ThisScalingFactor = CDbl(Grid_Returns.Item(Grid_Returns.Row, Col_GroupScalingFactor))

          If (ThisPertracID <> lastPertracID) Then

            Dim ThisDatePeriod As DealingPeriod = MainForm.PertracData.GetPertracDataPeriod(ThisPertracID)

            ' Dim ThisSeries As Integer

            lastPertracID = ThisPertracID
            Me.Label_ChartStock.Text = Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols("PertracName").SafeIndex).ToString

            ' ThisSeries = Chart_VAMI.Series.Count - 1
            Set_LineChart(MainForm, ThisDatePeriod, ThisPertracID, Chart_VAMI, 0, "", MainForm.StatFunctions.AnnualPeriodCount(ThisDatePeriod), 1, ThisScalingFactor, Now.AddMonths(-36), Now())
            Set_LineChart_RateReturnsLine(MainForm, Chart_VAMI, 1, 0, CDbl(Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols("GroupExpectedReturn").SafeIndex)), MainForm.StatFunctions.AnnualPeriodCount(ThisDatePeriod), "")

            'ThisSeries = Chart_MonthlyReturns.Series.Count - 1

            Set_RollingReturnChart(MainForm, ThisDatePeriod, ThisPertracID, Chart_MonthlyReturns, 0, "", MainForm.StatFunctions.AnnualPeriodCount(ThisDatePeriod), 1, ThisScalingFactor, Now.AddMonths(-36), Now())
            Set_LineChart_StaticReturnsLine(MainForm, Chart_MonthlyReturns, 1, 0, CDbl(Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols("GroupExpectedReturn").SafeIndex)) * 100, "")

          End If
				End If

				Select Case Grid_Returns.Col
					Case Col_GroupSector
						Me.SectorCombo.Text = Grid_Returns.Item(Grid_Returns.Row, Col_GroupSector)
						Me.SectorCombo.SelectedValue = Grid_Returns.Item(Grid_Returns.Row, Col_GroupSector)

					Case Col_GroupUpperBound
						' Disable Editing of UpperBound Column if 'Cap' is Set
						Try
							If CBool(Grid_Returns.Item(Grid_Returns.Row, Col_GroupCap)) Then
								Grid_Returns.Cols(Grid_Returns.Col).AllowEditing = False
							End If
						Catch ex As Exception
						End Try

					Case Col_GroupLowerBound
						' Disable editing of LowerBound if 'Floor' is Set
						Try
							If CBool(Grid_Returns.Item(Grid_Returns.Row, Col_GroupFloor)) Then
								Grid_Returns.Cols(Grid_Returns.Col).AllowEditing = False
							End If
						Catch ex As Exception
						End Try

				End Select

				If (Grid_Returns.Cols(Grid_Returns.Col).AllowEditing) Then
					Grid_Returns.SetCellStyle(Grid_Returns.Row, Grid_Returns.Col, Grid_Returns.Styles("Genoa_PlainEdit"))
				End If

			Catch ex As Exception

			End Try
		End If

	End Sub

	Private Sub Grid_Returns_LeaveCell(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Returns.LeaveCell
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If InPaint Then
			Exit Sub
		End If

		Dim Col_GroupSector As Integer = Grid_Returns.Cols("GroupSector").SafeIndex
		Dim Col_GroupUpperBound As Integer = Grid_Returns.Cols("GroupUpperBound").SafeIndex
		Dim Col_GroupLowerBound As Integer = Grid_Returns.Cols("GroupLowerBound").SafeIndex

		Try

			Select Case Grid_Returns.Col

				Case Col_GroupSector

					If (SectorCombo.SelectedValue IsNot Nothing) AndAlso (Grid_Returns.Row > 0) Then
						If (Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Col) Is Nothing) OrElse (CStr(Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Col)) <> SectorCombo.SelectedValue.ToString) Then
							Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Col) = SectorCombo.SelectedValue.ToString
						End If
					End If

				Case Col_GroupUpperBound, Col_GroupLowerBound
					Grid_Returns.Cols(Grid_Returns.Col).AllowEditing = True

				Case Else

			End Select

			If (Grid_Returns.Row > 0) AndAlso (Grid_Returns.Cols(Grid_Returns.Col).AllowEditing) Then
				Grid_Returns.SetCellStyle(Grid_Returns.Row, Grid_Returns.Col, Grid_Returns.Cols(Grid_Returns.Col).Style)
			End If

		Catch ex As Exception
		End Try

	End Sub

	Private Sub Grid_Returns_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Returns.LostFocus
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (Me.Created) AndAlso (Me.Grid_Returns.Focused = False) Then

			If (Grid_Returns.Cols.Count > 1) AndAlso (Grid_Returns.Col = Grid_Returns.Cols("GroupIndexCode").SafeIndex) Then
				Dim ThisCol As Integer = Grid_Returns.Col

				Grid_Returns.Col = 0
				Grid_Returns.Col = ThisCol
			End If
		End If
	End Sub

	Private Sub Grid_Returns_ChangeEdit(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Returns.ChangeEdit
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (InPaint = False) Then
			If (Grid_Returns.Row > 0) AndAlso (Grid_Returns.Col > 1) Then
				Call Set_InstrumentsGridChanged(Grid_Returns, New EventArgs)

				If (Grid_Returns.Cols(Grid_Returns.Col).Name.StartsWith("Custom_")) Then
					Grid_Returns.Item(Grid_Returns.Row, 1) = "x"
				Else
					Grid_Returns.Item(Grid_Returns.Row, 0) = "x"
				End If
			End If
		End If
	End Sub

	Private Sub Grid_Returns_BeforeSort(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.SortColEventArgs) Handles Grid_Returns.BeforeSort
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			Grid_Returns.Col = Grid_Returns.Cols("GroupPertracCode").SafeIndex
		Catch ex As Exception
		End Try

	End Sub

	Private Sub Update_Group_Percentages(ByVal UpdateCapsAndFloors As Boolean)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
		Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
		Dim Col_GroupCap As Integer = Grid_Returns.Cols("GroupCap").SafeIndex
		Dim Col_GroupFloor As Integer = Grid_Returns.Cols("GroupFloor").SafeIndex
		Dim Col_GroupUpperBound As Integer = Grid_Returns.Cols("GroupUpperBound").SafeIndex
		Dim Col_GroupLowerBound As Integer = Grid_Returns.Cols("GroupLowerBound").SafeIndex
		Dim Col_GroupPercent As Integer = Grid_Returns.Cols("GroupPercent").SafeIndex
		Dim Col_GroupNewPercent As Integer = Grid_Returns.Cols("GroupNewPercent").SafeIndex


		Try
			Dim SumValue As Double
			Dim FormatString As String

			' 'Holdings' Value

			If CalculateCapFromNewPercent Then
				SumValue = Update_Grid_Percentages(Grid_Returns, Col_GroupHolding, Col_GroupPercent, 0, 0, 0, 0, False)
			Else
				SumValue = Update_Grid_Percentages(Grid_Returns, Col_GroupHolding, Col_GroupPercent, Col_GroupFloor, Col_GroupCap, Col_GroupLowerBound, Col_GroupUpperBound, UpdateCapsAndFloors)
			End If

			FormatString = DefaultNumericFormat(SumValue)

			Me.Label_HoldingValue.Text = SumValue.ToString(FormatString)
			Grid_Returns.Cols("GroupHolding").Format = FormatString
			Grid_Returns.Cols("GroupTrade").Format = FormatString
			Grid_Returns.Cols("GroupNewHolding").Format = FormatString

			' 'NewHoldings' Value

			If CalculateCapFromNewPercent Then
				SumValue = Update_Grid_Percentages(Grid_Returns, Col_GroupNewHolding, Col_GroupNewPercent, Col_GroupFloor, Col_GroupCap, Col_GroupLowerBound, Col_GroupUpperBound, UpdateCapsAndFloors)
			Else
				SumValue = Update_Grid_Percentages(Grid_Returns, Col_GroupNewHolding, Col_GroupNewPercent, 0, 0, 0, 0, False)
			End If

			FormatString = DefaultNumericFormat(SumValue)

			Me.Label_NewHoldingValue.Text = SumValue.ToString(FormatString)

		Catch ex As Exception
		End Try

	End Sub

	'Private Function Update_Grid_Percentages(ByRef UpdateGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByRef pHoldingCol As Integer, ByRef pPercentagesCol As Integer, ByVal pFloorCol As Integer, ByVal pCapCol As Integer, ByVal pLowerLimitCol As Integer, ByVal pUpperLimitCol As Integer, ByVal UpdateCapsAndFloors As Boolean, Optional ByVal NoTitleRow As Boolean = False) As Double
	'	' *****************************************************************************************
	'	'
	'	'
	'	' *****************************************************************************************

	'	Dim RowCounter As Integer
	'	Dim SumOfholding As Double = 0
	'	Dim StartRow As Integer = 1

	'	Try
	'		If (NoTitleRow) Then
	'			StartRow = 0
	'		End If

	'		For RowCounter = StartRow To (UpdateGrid.Rows.Count - 1)
	'			If IsNumeric(UpdateGrid.Item(RowCounter, pHoldingCol)) Then
	'				SumOfholding += CDbl(UpdateGrid.Item(RowCounter, pHoldingCol))
	'			End If
	'		Next

	'		For RowCounter = StartRow To (UpdateGrid.Rows.Count - 1)
	'			If IsNumeric(UpdateGrid.Item(RowCounter, pHoldingCol)) Then
	'				If (SumOfholding = 0) Then
	'					UpdateGrid.Item(RowCounter, pPercentagesCol) = 0

	'					If UpdateCapsAndFloors Then
	'						Try
	'							If CBool(UpdateGrid.Item(RowCounter, pCapCol)) AndAlso (CDbl(UpdateGrid.Item(RowCounter, pUpperLimitCol)) <> 0) Then
	'								UpdateGrid.Item(RowCounter, pUpperLimitCol) = 0
	'							End If
	'							If CBool(UpdateGrid.Item(RowCounter, pFloorCol)) AndAlso (CDbl(UpdateGrid.Item(RowCounter, pFloorCol)) <> 0) Then
	'								UpdateGrid.Item(RowCounter, pLowerLimitCol) = 0
	'							End If
	'						Catch ex As Exception
	'						End Try
	'					End If

	'				Else
	'					UpdateGrid.Item(RowCounter, pPercentagesCol) = CDbl(UpdateGrid.Item(RowCounter, pHoldingCol)) / SumOfholding

	'					If UpdateCapsAndFloors Then
	'						Try
	'							If CBool(UpdateGrid.Item(RowCounter, pCapCol)) AndAlso (CDbl(UpdateGrid.Item(RowCounter, pUpperLimitCol)) <> CDbl(UpdateGrid.Item(RowCounter, pPercentagesCol))) Then
	'								UpdateGrid.Item(RowCounter, pUpperLimitCol) = UpdateGrid.Item(RowCounter, pPercentagesCol)
	'							End If
	'							If CBool(UpdateGrid.Item(RowCounter, pFloorCol)) AndAlso (CDbl(UpdateGrid.Item(RowCounter, pLowerLimitCol)) <> CDbl(UpdateGrid.Item(RowCounter, pPercentagesCol))) Then
	'								UpdateGrid.Item(RowCounter, pLowerLimitCol) = UpdateGrid.Item(RowCounter, pPercentagesCol)
	'							End If
	'						Catch ex As Exception
	'						End Try
	'					End If
	'				End If
	'			End If
	'		Next

	'	Catch ex As Exception
	'	End Try

	'	Return SumOfholding

	'End Function

	Private Sub Update_Group_RiskAndReturn(ByVal UpdatePlotPortfolio As Boolean)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			' Exit if Form creation is not yet complete or the form has been closed.

			If (Not Me.Created) OrElse (Me.IsDisposed) Then
				Exit Sub
			End If

			If (Suppress_RiskAndReturn) Then
				Exit Sub
			End If

			' OK...

			Label_HoldingRisk.Text = ""
			Label_HoldingReturn.Text = ""

			Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
			Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
			Dim Col_GroupPercent As Integer = Grid_Returns.Cols("GroupPercent").SafeIndex
			Dim Col_GroupNewPercent As Integer = Grid_Returns.Cols("GroupNewPercent").SafeIndex
			Dim Col_GroupReturn As Integer = Grid_Returns.Cols("GroupExpectedReturn").SafeIndex
			Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
			' 
			Dim RowCounter As Integer = 0
			Dim SumOfholding As Double = 0

			' Calculate Risk and Return

			Dim ReturnValue As Double
			Dim RiskValue As Double
      Dim DrawDownValue As Double

			ReturnValue = CalculateGroupReturn(Me.Grid_Returns, Col_GroupPercent, Col_GroupReturn)
			RiskValue = CalculateGroupRisk(Me.Grid_Returns, Me.Grid_Covariance, Col_GroupPertracCode, Col_GroupPercent)
      DrawDownValue = CalculateGroupWorstDrawdown(DefaultStatsDatePeriod, Holding_DynamicGroupID, Me.Grid_Returns, Col_GroupPertracCode, Col_GroupPercent, False)

      Label_HoldingReturn.Text = ReturnValue.ToString("#,##0.00%")
			Label_HoldingRisk.Text = RiskValue.ToString("#,##0.00%")
      Label_HoldingDrawDown.Text = DrawDownValue.ToString("#,##0.00%")

			Label_NewHoldingReturn.Text = CalculateGroupReturn(Me.Grid_Returns, Col_GroupNewPercent, Col_GroupReturn).ToString("#,##0.00%")
			Label_NewHoldingRisk.Text = CalculateGroupRisk(Me.Grid_Returns, Me.Grid_Covariance, Col_GroupPertracCode, Col_GroupNewPercent).ToString("#,##0.00%")
      Label_NewHoldingDrawDown.Text = CalculateGroupWorstDrawdown(DefaultStatsDatePeriod, NewHolding_DynamicGroupID, Me.Grid_Returns, Col_GroupPertracCode, Col_GroupNewPercent, False).ToString("#,##0.00%")

			If (UpdatePlotPortfolio) Then
				RiskReturnChartPoints.Add(New DecimalPoint(CDbl(ConvertValue(Label_NewHoldingRisk.Text, GetType(Double))), CDbl(ConvertValue(Label_NewHoldingReturn.Text, GetType(Double)))))

				If (Results_Check_ShowVsPortfolio.Enabled) AndAlso (Results_Check_ShowVsPortfolio.Checked) AndAlso (Me.Results_Radio_vsNewHolding.Checked) Then
					ClearOptimisationChartLines()
					PaintResultsCharts()
					Results_RefreshRiskReturnCharts()
				Else
					PaintResultsCharts()
        End If

        PaintGroupReturnChart()

      End If

		Catch ex As Exception
		End Try

	End Sub

	Public Function CalculateGroupReturn(ByRef pReturnsGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal pWeightCol As Integer, ByVal pReturnCol As Integer, Optional ByVal NoTitleRow As Boolean = False) As Double
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim RVal As Double = 0
		Dim SumReturn As Double = 0
		Dim StartRow As Integer = 1

		' Validate - Check given Columns are valid.

		Try
			If (pWeightCol < 0) OrElse (pWeightCol >= pReturnsGrid.Cols.Count) Then
				Return RVal
			End If

			If (pReturnCol < 0) OrElse (pReturnCol >= pReturnsGrid.Cols.Count) Then
				Return RVal
			End If
		Catch ex As Exception
		End Try

		' Calculate Group Return 

		Try
			Dim ThisReturn As Double
			Dim ThisWeight As Double
			Dim RowCount As Integer

			If (NoTitleRow) Then
				StartRow = 0
			End If

			For RowCount = StartRow To (pReturnsGrid.Rows.Count - 1)
				If (IsNumeric(pReturnsGrid.Item(RowCount, pWeightCol)) AndAlso IsNumeric(pReturnsGrid.Item(RowCount, pReturnCol))) Then
					ThisReturn = CDbl(pReturnsGrid.Item(RowCount, pReturnCol))
					ThisWeight = CDbl(pReturnsGrid.Item(RowCount, pWeightCol))

					SumReturn += (ThisReturn * ThisWeight)
				End If
			Next

			RVal = SumReturn

		Catch ex As Exception
		End Try

		' Return

		Return RVal

	End Function

	Public Function CalculateGroupRisk(ByRef pReturnsGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByRef pCovarianceGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal pReturnsPertracIDCol As Integer, ByVal pReturnsWeightCol As Integer, Optional ByVal NoReturnsTitleRow As Boolean = False, Optional ByVal NoCoVarianceTitleRow As Boolean = False) As Double
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim RVal As Double = 0
		Dim SumRisk As Double = 0
		Dim RowIndex As Integer
		Dim ColIndex As Integer
		Dim ThisPertracID As Integer
		Dim ReturnsStartRow As Integer = 1
		Dim CovarianceStartRow As Integer = 1

		Dim WeightsArray() As Double
		Dim PertracIDsArray() As Integer
		Dim CovarianceRowArray() As Integer
		Dim IntermediateRiskArray() As Double

		' Validate - Check given Columns are valid, Check Returns Grid has some rows.

		Try
			If (pReturnsPertracIDCol < 0) OrElse (pReturnsPertracIDCol >= pReturnsGrid.Cols.Count) Then
				Return RVal
			End If

			If (pReturnsWeightCol < 0) OrElse (pReturnsWeightCol >= pReturnsGrid.Cols.Count) Then
				Return RVal
			End If

			If (pReturnsGrid.Rows.Count <= 1) Then
				Return RVal
			End If
		Catch ex As Exception
		End Try

		If (NoReturnsTitleRow) Then
			ReturnsStartRow = 0
		End If
		If (NoCoVarianceTitleRow) Then
			CovarianceStartRow = 0
		End If
		Try

			' Get Pertrac Hash.

			Dim CovariancePertracHash As Dictionary(Of Integer, Integer)

			If (TypeOf pCovarianceGrid.Tag Is Dictionary(Of Integer, Integer)) Then
				CovariancePertracHash = CType(pCovarianceGrid.Tag, Dictionary(Of Integer, Integer))
			Else
				CovariancePertracHash = New Dictionary(Of Integer, Integer)

				For RowIndex = CovarianceStartRow To (pCovarianceGrid.Rows.Count - 1)
					If IsNumeric(pCovarianceGrid.Item(RowIndex, pCovarianceGrid.Cols.Count - 1)) Then
						Try
							CovariancePertracHash.Add(CInt(pCovarianceGrid.Item(RowIndex, pCovarianceGrid.Cols.Count - 1)), RowIndex)
						Catch ex As Exception
						End Try
					End If
				Next

				Grid_Covariance.Tag = CovariancePertracHash

			End If

			' Dimension Arrays

			WeightsArray = Array.CreateInstance(GetType(Double), pReturnsGrid.Rows.Count - ReturnsStartRow)
			PertracIDsArray = Array.CreateInstance(GetType(Integer), WeightsArray.Length)
			CovarianceRowArray = Array.CreateInstance(GetType(Integer), WeightsArray.Length)
			IntermediateRiskArray = Array.CreateInstance(GetType(Double), WeightsArray.Length)

			' Get Weights
			Dim ReturnsGridRow As Integer

			For RowIndex = 0 To (WeightsArray.Length - 1)
				ReturnsGridRow = RowIndex + ReturnsStartRow

				PertracIDsArray(RowIndex) = (-1)
				CovarianceRowArray(RowIndex) = (-1)

				If IsNumeric(pReturnsGrid.Item(ReturnsGridRow, pReturnsPertracIDCol)) Then
					ThisPertracID = CInt(pReturnsGrid.Item(ReturnsGridRow, pReturnsPertracIDCol))
					PertracIDsArray(RowIndex) = ThisPertracID

					If IsNumeric(CDbl(pReturnsGrid.Item(ReturnsGridRow, pReturnsWeightCol))) Then
						WeightsArray(RowIndex) = CDbl(pReturnsGrid.Item(ReturnsGridRow, pReturnsWeightCol))
					Else
						WeightsArray(RowIndex) = 0
					End If

					If CovariancePertracHash.ContainsKey(ThisPertracID) Then
						CovarianceRowArray(RowIndex) = CovariancePertracHash(ThisPertracID)
					End If
				End If
			Next

			For RowIndex = 0 To (WeightsArray.Length - 1)
				IntermediateRiskArray(RowIndex) = 0
			Next

			For RowIndex = 0 To (WeightsArray.Length - 1)

				If (PertracIDsArray(RowIndex) >= 0) AndAlso (CovarianceRowArray(RowIndex) >= 0) Then

					For ColIndex = 0 To (WeightsArray.Length - 1)

						If (CovarianceRowArray(ColIndex) >= 0) Then

							If IsNumeric(pCovarianceGrid.Item(CovarianceRowArray(RowIndex), CovarianceRowArray(ColIndex))) Then

								IntermediateRiskArray(ColIndex) += (WeightsArray(RowIndex) * CDbl(pCovarianceGrid.Item(CovarianceRowArray(RowIndex), CovarianceRowArray(ColIndex))))

							End If

						End If
					Next

				End If
			Next

			For RowIndex = 0 To (IntermediateRiskArray.Length - 1)
				SumRisk += (WeightsArray(RowIndex) * IntermediateRiskArray(RowIndex))
			Next

      RVal = (SumRisk ^ 0.5) * MainForm.StatFunctions.Sqrt12(DefaultStatsDatePeriod)

		Catch ex As Exception
		End Try

		Return RVal

	End Function

  Public Function CalculateGroupWorstDrawdown(ByVal GroupDataPeriod As DealingPeriod, ByRef pReturnsGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal pReturnsPertracIDCol As Integer, ByVal pReturnsWeightCol As Integer, ByVal NoReturnsTitleRow As Boolean) As Double

    Return CalculateGroupWorstDrawdown(GroupDataPeriod, Marginals_Drawdown_DynamicGroupID, pReturnsGrid, pReturnsPertracIDCol, pReturnsWeightCol, NoReturnsTitleRow)

  End Function

  Public Function CalculateGroupWorstDrawdown(ByVal GroupDataPeriod As DealingPeriod, ByVal pGroupID As Integer, ByRef pReturnsGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal pReturnsPertracIDCol As Integer, ByVal pReturnsWeightCol As Integer, ByVal NoReturnsTitleRow As Boolean) As Double
    ' *****************************************************************************************
    '
    ' 1) Get Group members and weights from the given grid.
    ' 2) Set members to the Given Group ID
    ' 3) Calculate Drawdown on derived Group.
    '
    ' The ability to specify a GRoupID is provided as a performance enhancing move. Specifically
    ' It means that 'Holding' vs 'NewHolding' Drawdown calculations will not constantly conflict.
    '
    ' *****************************************************************************************

    Dim RowIndex As Integer
    Dim ThisPertracID As Integer
    Dim ReturnsStartRow As Integer = 1
    Dim WeightsArray() As Double
    Dim PertracIDsArray() As Integer
    'Dim DrawDowns() As RenaissanceStatFunctions.StatFunctions.DrawDownInstanceClass

    Try

      Try
        If (pReturnsPertracIDCol < 0) OrElse (pReturnsPertracIDCol >= pReturnsGrid.Cols.Count) Then
          Return 0.0#
        End If

        If (pReturnsWeightCol < 0) OrElse (pReturnsWeightCol >= pReturnsGrid.Cols.Count) Then
          Return 0.0#
        End If

        If (pReturnsGrid.Rows.Count <= 1) Then
          Return 0.0#
        End If
      Catch ex As Exception
      End Try

      If (NoReturnsTitleRow) Then
        ReturnsStartRow = 0
      End If

      ' Dimension Arrays

      WeightsArray = Array.CreateInstance(GetType(Double), pReturnsGrid.Rows.Count - ReturnsStartRow)
      PertracIDsArray = Array.CreateInstance(GetType(Integer), WeightsArray.Length)

      ' Get Pertrac IDs and Weights

      Dim ReturnsGridRow As Integer

      For RowIndex = 0 To (WeightsArray.Length - 1)
        ReturnsGridRow = RowIndex + ReturnsStartRow

        PertracIDsArray(RowIndex) = (-1)

        If IsNumeric(pReturnsGrid.Item(ReturnsGridRow, pReturnsPertracIDCol)) Then
          ThisPertracID = CInt(pReturnsGrid.Item(ReturnsGridRow, pReturnsPertracIDCol))
          PertracIDsArray(RowIndex) = ThisPertracID

          If IsNumeric(CDbl(pReturnsGrid.Item(ReturnsGridRow, pReturnsWeightCol))) Then
            WeightsArray(RowIndex) = CDbl(pReturnsGrid.Item(ReturnsGridRow, pReturnsWeightCol))
          Else
            WeightsArray(RowIndex) = 0
          End If
        End If

      Next

      ' Set Group Members

      MainForm.StatFunctions.SetDynamicGroupMembers(pGroupID, PertracIDsArray, WeightsArray)

      ' Get Drawdown details

      Dim GroupPertracID As UInteger = RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(pGroupID, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Weighted)

      Dim NAVSeries() As Double
      Dim MaxNAV As Double
      Dim ThisNAV As Double
      Dim ThisDrawDown As Double
      Dim Index As Integer
      Dim MaxDrawDown As Double = 0.0#

      NAVSeries = MainForm.StatFunctions.NAVSeries(GroupDataPeriod, GroupPertracID, False, MainForm.StatFunctions.AnnualPeriodCount(GroupDataPeriod), 1.0#, False, Renaissance_BaseDate, Renaissance_EndDate_Data, 1.0#)

      If (NAVSeries IsNot Nothing) AndAlso (NAVSeries.Length > 1) Then
        MaxNAV = NAVSeries(0)

        For Index = 1 To (NAVSeries.Length - 1)
          ThisNAV = NAVSeries(Index)

          If (ThisNAV < MaxNAV) Then
            ThisDrawDown = ((ThisNAV / MaxNAV) - 1.0#) * (-1.0#)

            If (ThisDrawDown > MaxDrawDown) Then
              MaxDrawDown = ThisDrawDown
            End If
          Else
            MaxNAV = ThisNAV
          End If
        Next

      End If

      Return MaxDrawDown

      'DrawDowns = MainForm.StatFunctions.GetDrawDownDetails(GroupDataPeriod, GroupPertracID, False, Renaissance_BaseDate, Renaissance_EndDate_Data, 1.0#)

      'If (DrawDowns IsNot Nothing) AndAlso (DrawDowns.Length > 0) Then

      '  Return DrawDowns(0).DrawDown

      'End If

    Catch ex As Exception

    End Try

    Return 0.0#

  End Function

  Public Function CalculateLeastSquares(ByVal GroupDataPeriod As DealingPeriod, ByRef pReturnsGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal pReturnsPertracIDCol As Integer, ByVal pReturnsWeightCol As Integer, ByVal NoReturnsTitleRow As Boolean, ByVal ImproveVsInstrument_Dates() As Date, ByVal ImproveVsInstrument_NAVs() As Double, ByVal NormaliseSeries As Boolean, ByVal UseAnalysisPeriod As Boolean, ByVal AnalysisMonths As Integer) As Double

    Return CalculateLeastSquares(GroupDataPeriod, Marginals_Drawdown_DynamicGroupID, pReturnsGrid, pReturnsPertracIDCol, pReturnsWeightCol, NoReturnsTitleRow, ImproveVsInstrument_Dates, ImproveVsInstrument_NAVs, NormaliseSeries, UseAnalysisPeriod, AnalysisMonths)

  End Function

  Public Function CalculateLeastSquares(ByVal GroupDataPeriod As DealingPeriod, ByVal pGroupID As Integer, ByRef pReturnsGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal pReturnsPertracIDCol As Integer, ByVal pReturnsWeightCol As Integer, ByVal NoReturnsTitleRow As Boolean, ByVal Benchmark_Dates() As Date, ByVal Benchmark_NAVs() As Double, ByVal NormaliseSeries As Boolean, ByVal UseAnalysisPeriod As Boolean, ByVal AnalysisMonths As Integer) As Double
    ' *****************************************************************************************
    '
    ' 1) Get Group members and weights from the given grid.
    ' 2) Set members to the Given Group ID
    ' 3) Calculate Least Squares Vs Given Series on derived Group.
    '
    ' The ability to specify a GroupID is provided as a performance enhancing move. Specifically
    ' It means that 'Holding' vs 'NewHolding' Drawdown calculations will not constantly conflict.
    '
    ' *****************************************************************************************

    Dim RowIndex As Integer
    Dim ThisPertracID As Integer
    Dim ReturnsStartRow As Integer = 1
    Dim WeightsArray() As Double
    Dim PertracIDsArray() As Integer

    Try

      Try
        If (Benchmark_Dates Is Nothing) OrElse (Benchmark_Dates.Length <= 0) Then
          Return 0.0#
        End If

        If (Benchmark_NAVs Is Nothing) OrElse (Benchmark_NAVs.Length <= 0) Then
          Return 0.0#
        End If

        If (pReturnsPertracIDCol < 0) OrElse (pReturnsPertracIDCol >= pReturnsGrid.Cols.Count) Then
          Return 0.0#
        End If

        If (pReturnsWeightCol < 0) OrElse (pReturnsWeightCol >= pReturnsGrid.Cols.Count) Then
          Return 0.0#
        End If

        If (pReturnsGrid.Rows.Count <= 1) Then
          Return 0.0#
        End If
      Catch ex As Exception
      End Try

      If (NoReturnsTitleRow) Then
        ReturnsStartRow = 0
      End If

      ' Dimension Arrays

      WeightsArray = Array.CreateInstance(GetType(Double), pReturnsGrid.Rows.Count - ReturnsStartRow)
      PertracIDsArray = Array.CreateInstance(GetType(Integer), WeightsArray.Length)

      ' Get Pertrac IDs and Weights

      Dim ReturnsGridRow As Integer

      For RowIndex = 0 To (WeightsArray.Length - 1)
        ReturnsGridRow = RowIndex + ReturnsStartRow

        PertracIDsArray(RowIndex) = (-1)

        If IsNumeric(pReturnsGrid.Item(ReturnsGridRow, pReturnsPertracIDCol)) Then
          ThisPertracID = CInt(pReturnsGrid.Item(ReturnsGridRow, pReturnsPertracIDCol))
          PertracIDsArray(RowIndex) = ThisPertracID

          If IsNumeric(CDbl(pReturnsGrid.Item(ReturnsGridRow, pReturnsWeightCol))) Then
            WeightsArray(RowIndex) = CDbl(pReturnsGrid.Item(ReturnsGridRow, pReturnsWeightCol))
          Else
            WeightsArray(RowIndex) = 0
          End If
        End If

      Next

      ' Set Group Members

      MainForm.StatFunctions.SetDynamicGroupMembers(pGroupID, PertracIDsArray, WeightsArray)

      ' Get Group Series

      Dim GroupPertracID As UInteger = RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(pGroupID, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Weighted)

      Dim GroupDates() As Date
      Dim GroupNAVs() As Double

      GroupDates = MainForm.StatFunctions.DateSeries(GroupDataPeriod, GroupPertracID, False, MainForm.StatFunctions.AnnualPeriodCount(DefaultStatsDatePeriod), 1.0#, False, Benchmark_Dates(0), Renaissance_EndDate_Data)
      GroupNAVs = MainForm.StatFunctions.NAVSeries(GroupDataPeriod, GroupPertracID, False, MainForm.StatFunctions.AnnualPeriodCount(DefaultStatsDatePeriod), 1.0#, False, Benchmark_Dates(0), Renaissance_EndDate_Data)

      If (GroupDates IsNot Nothing) AndAlso (GroupNAVs IsNot Nothing) AndAlso (GroupDates.Length > 0) AndAlso (GroupNAVs.Length > 0) Then

        Dim GroupIndex As Integer = GetPriceIndex(GroupDataPeriod, GroupDates(0), MaxFunctions.MAX(Benchmark_Dates(0), GroupDates(0)))
        Dim BenchmarkIndex As Integer = GetPriceIndex(GroupDataPeriod, Benchmark_Dates(0), MaxFunctions.MAX(Benchmark_Dates(0), GroupDates(0)))
        Dim SumOfSquares As Double

        If (GroupIndex < 0) Then
          BenchmarkIndex += Math.Abs(GroupIndex)
          GroupIndex = 0
        End If
        If (BenchmarkIndex < 0) Then
          GroupIndex += Math.Abs(BenchmarkIndex)
          BenchmarkIndex = 0
        End If

        If (GroupIndex < GroupDates.Length) AndAlso (BenchmarkIndex < Benchmark_Dates.Length) Then

          If (UseAnalysisPeriod) Then
            SumOfSquares = MainForm.StatFunctions.LeastSquares(GroupNAVs, Benchmark_NAVs, GroupIndex, BenchmarkIndex, MainForm.StatFunctions.GetPeriodsFromMonth(GroupDataPeriod, AnalysisMonths), NormaliseSeries)
          Else
            SumOfSquares = MainForm.StatFunctions.LeastSquares(GroupNAVs, Benchmark_NAVs, GroupIndex, BenchmarkIndex, CInt(Math.Max(GroupDates.Length, Benchmark_Dates.Length)), NormaliseSeries)
          End If

          Return SumOfSquares

        End If

        'Dim GroupIndex As Integer = Math.Max(0, GetPriceIndex(GroupDataPeriod, GroupDates(0), MaxFunctions.MAX(Benchmark_Dates(0), GroupDates(0))))
        'Dim BenchmarkIndex As Integer = Math.Max(0, GetPriceIndex(GroupDataPeriod, Benchmark_Dates(0), MaxFunctions.MAX(Benchmark_Dates(0), GroupDates(0))))
        'Dim LastGroupIndex As Integer = (GroupDates.Length - 1)
        'Dim LastBenchmarkIndex As Integer = (Benchmark_Dates.Length - 1)
        'Dim GroupDivisor As Double = 1.0#
        'Dim BenchmarkDivisor As Double = 1.0#
        'Dim IndexCounter As Double = 0.0#
        'Dim SumOfSquares As Double = 0.0#

        'If (GroupIndex <= LastGroupIndex) AndAlso (BenchmarkIndex <= LastBenchmarkIndex) Then

        '  GroupDivisor = GroupNAVs(GroupIndex)
        '  BenchmarkDivisor = Benchmark_NAVs(BenchmarkIndex)

        '  While (GroupIndex < LastGroupIndex) And (BenchmarkIndex < LastBenchmarkIndex)

        '    If (NormaliseSeries) Then
        '      SumOfSquares += ((GroupNAVs(GroupIndex) / GroupDivisor) - (Benchmark_NAVs(BenchmarkIndex) / BenchmarkDivisor)) ^ 2.0#
        '    Else
        '      SumOfSquares += (GroupNAVs(GroupIndex) - Benchmark_NAVs(BenchmarkIndex)) ^ 2.0#
        '    End If

        '    IndexCounter += 1.0#
        '    GroupIndex += 1
        '    BenchmarkIndex += 1
        '  End While

        '  SumOfSquares /= IndexCounter

        '  Return SumOfSquares

        'End If

      End If

    Catch ex As Exception
    End Try

    Return 0.0#

  End Function

  Public Function CalculateStdError(ByVal GroupDataPeriod As DealingPeriod, ByRef pReturnsGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal pReturnsPertracIDCol As Integer, ByVal pReturnsWeightCol As Integer, ByVal NoReturnsTitleRow As Boolean, ByVal Benchmark_Dates() As Date, ByVal Benchmark_Returns() As Double, ByVal UseAnalysisPeriod As Boolean, ByVal AnalysisMonths As Integer) As Double

    Return CalculateStdError(GroupDataPeriod, Marginals_Drawdown_DynamicGroupID, pReturnsGrid, pReturnsPertracIDCol, pReturnsWeightCol, NoReturnsTitleRow, Benchmark_Dates, Benchmark_Returns, UseAnalysisPeriod, AnalysisMonths)

  End Function

  Public Function CalculateStdError(ByVal GroupDataPeriod As DealingPeriod, ByVal pGroupID As Integer, ByRef pReturnsGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal pReturnsPertracIDCol As Integer, ByVal pReturnsWeightCol As Integer, ByVal NoReturnsTitleRow As Boolean, ByVal Benchmark_Dates() As Date, ByVal Benchmark_Returns() As Double, ByVal UseAnalysisPeriod As Boolean, ByVal AnalysisMonths As Integer) As Double
    ' *****************************************************************************************
    '
    ' 1) Get Group members and weights from the given grid.
    ' 2) Set members to the Given Group ID
    ' 3) Calculate Least Squares Vs Given Series on derived Group.
    '
    ' The ability to specify a GroupID is provided as a performance enhancing move. Specifically
    ' It means that 'Holding' vs 'NewHolding' Drawdown calculations will not constantly conflict.
    '
    ' *****************************************************************************************

    Dim RowIndex As Integer
    Dim ThisPertracID As Integer
    Dim ReturnsStartRow As Integer = 1
    Dim WeightsArray() As Double
    Dim PertracIDsArray() As Integer

    Try

      Try
        If (Benchmark_Dates Is Nothing) OrElse (Benchmark_Dates.Length <= 0) Then
          Return 0.0#
        End If

        If (Benchmark_Returns Is Nothing) OrElse (Benchmark_Returns.Length <= 0) Then
          Return 0.0#
        End If

        If (pReturnsPertracIDCol < 0) OrElse (pReturnsPertracIDCol >= pReturnsGrid.Cols.Count) Then
          Return 0.0#
        End If

        If (pReturnsWeightCol < 0) OrElse (pReturnsWeightCol >= pReturnsGrid.Cols.Count) Then
          Return 0.0#
        End If

        If (pReturnsGrid.Rows.Count <= 1) Then
          Return 0.0#
        End If
      Catch ex As Exception
      End Try

      If (NoReturnsTitleRow) Then
        ReturnsStartRow = 0
      End If

      ' Dimension Arrays

      WeightsArray = Array.CreateInstance(GetType(Double), pReturnsGrid.Rows.Count - ReturnsStartRow)
      PertracIDsArray = Array.CreateInstance(GetType(Integer), WeightsArray.Length)

      ' Get Pertrac IDs and Weights

      Dim ReturnsGridRow As Integer

      For RowIndex = 0 To (WeightsArray.Length - 1)
        ReturnsGridRow = RowIndex + ReturnsStartRow

        PertracIDsArray(RowIndex) = (-1)

        If IsNumeric(pReturnsGrid.Item(ReturnsGridRow, pReturnsPertracIDCol)) Then
          ThisPertracID = CInt(pReturnsGrid.Item(ReturnsGridRow, pReturnsPertracIDCol))
          PertracIDsArray(RowIndex) = ThisPertracID

          If IsNumeric(CDbl(pReturnsGrid.Item(ReturnsGridRow, pReturnsWeightCol))) Then
            WeightsArray(RowIndex) = CDbl(pReturnsGrid.Item(ReturnsGridRow, pReturnsWeightCol))
          Else
            WeightsArray(RowIndex) = 0
          End If
        End If

      Next

      ' Set Group Members

      MainForm.StatFunctions.SetDynamicGroupMembers(pGroupID, PertracIDsArray, WeightsArray)

      ' Get Group Series

      Dim GroupPertracID As UInteger = RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(pGroupID, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Weighted)

      Dim GroupDates() As Date
      Dim GroupReturns() As Double

      GroupDates = MainForm.StatFunctions.DateSeries(GroupDataPeriod, GroupPertracID, False, MainForm.StatFunctions.AnnualPeriodCount(DefaultStatsDatePeriod), 1.0#, False, Benchmark_Dates(0), Renaissance_EndDate_Data)
      GroupReturns = MainForm.StatFunctions.ReturnSeries(GroupDataPeriod, GroupPertracID, False, MainForm.StatFunctions.AnnualPeriodCount(DefaultStatsDatePeriod), 1.0#, False, Benchmark_Dates(0), Renaissance_EndDate_Data)

      If (GroupDates IsNot Nothing) AndAlso (GroupReturns IsNot Nothing) AndAlso (GroupDates.Length > 0) AndAlso (GroupReturns.Length > 0) Then

        Dim GroupIndex As Integer = GetPriceIndex(GroupDataPeriod, GroupDates(0), MaxFunctions.MAX(Benchmark_Dates(0), GroupDates(0)))
        Dim BenchmarkIndex As Integer = GetPriceIndex(GroupDataPeriod, Benchmark_Dates(0), MaxFunctions.MAX(Benchmark_Dates(0), GroupDates(0)))
        Dim SumOfSquares As Double

        If (GroupIndex < 0) Then
          BenchmarkIndex += Math.Abs(GroupIndex)
          GroupIndex = 0
        End If
        If (BenchmarkIndex < 0) Then
          GroupIndex += Math.Abs(BenchmarkIndex)
          BenchmarkIndex = 0
        End If

        If (GroupIndex < GroupDates.Length) AndAlso (BenchmarkIndex < Benchmark_Dates.Length) Then

          If (UseAnalysisPeriod) Then
            SumOfSquares = MainForm.StatFunctions.LeastSquares(GroupReturns, Benchmark_Returns, GroupIndex, BenchmarkIndex, MainForm.StatFunctions.GetPeriodsFromMonth(GroupDataPeriod, AnalysisMonths), False)
          Else
            SumOfSquares = MainForm.StatFunctions.LeastSquares(GroupReturns, Benchmark_Returns, GroupIndex, BenchmarkIndex, CInt(Math.Max(GroupDates.Length, Benchmark_Dates.Length)), False)
          End If

          Return SumOfSquares

        End If
      End If

    Catch ex As Exception

    End Try

    Return 0.0#

  End Function

  Public Function CalculateConstraintExposures(Optional ByVal pForceUpdate As Boolean = False) As Boolean
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If (Me.IsDisposed) Or (Not Me.Created) Then
        Return True
      End If

      If (Constraints_TabConstraints.Visible = False) And (pForceUpdate = False) Then
        Return True
      End If

      ' 

      Dim DefinedGrid As C1.Win.C1FlexGrid.C1FlexGrid = Constraints_Grid_Defined

      ' Get Instrument Weights

      Dim InstrumentWeights() As Double
      InstrumentWeights = ConstraintWorker.InstrumentWeights(Grid_Returns)

      ' Constraint Grid Columns

      Dim Col_CustomFieldID As Integer = DefinedGrid.Cols("Col_CustomID").SafeIndex
      Dim Col_CustomFieldName As Integer = DefinedGrid.Cols("Col_CustomName").SafeIndex
      Dim Col_IsDefined As Integer = DefinedGrid.Cols("Col_IsDefined").SafeIndex

      Try
        Dim GridRowCounter As Integer
        Dim thisGridRow As C1.Win.C1FlexGrid.Row
        Dim thisConstraintsGridRow As C1.Win.C1FlexGrid.Row
        Dim ConstraintsGridRowCounter As Integer

        Dim ThisCustomFieldID As Integer
        Dim ConstraintsTab As TabPage
        Dim TabName As String
        Dim ConstraintsGrid As C1.Win.C1FlexGrid.C1FlexGrid

        ' Cycle through the Constraints defined on the Instruments Grid.
        ' InstrumentsGrid

        For GridRowCounter = 1 To (DefinedGrid.Rows.Count - 1)
          Try

            thisGridRow = DefinedGrid.Rows(GridRowCounter)

            If (Not thisGridRow.IsNew) AndAlso CBool(thisGridRow(Col_IsDefined)) Then

              ' OK, Resolve and Get TabPage

              ThisCustomFieldID = CInt(thisGridRow(Col_CustomFieldID))

              ' Get TabName

              If (ThisCustomFieldID < 0) Then
                TabName = "Tab_" & CType(ThisCustomFieldID, SpecialConstraintFields).ToString
              Else
                TabName = "Tab_" & ThisCustomFieldID.ToString
              End If

              ConstraintsTab = Nothing

              If Me.Constraints_TabConstraints.TabPages.ContainsKey(TabName) Then
                ConstraintsTab = Me.Constraints_TabConstraints.TabPages(TabName)
              End If

              If (ConstraintsTab IsNot Nothing) AndAlso (ConstraintsTab Is Constraints_TabConstraints.SelectedTab) Then
                Dim thisConstraintsTabDetails As frmOptimise.ConstraintsTabDetails

                thisConstraintsTabDetails = ConstraintsTab.Tag
                ConstraintsGrid = thisConstraintsTabDetails.ConstraintsGrid

                Dim Col_LessThan As Integer = ConstraintsGrid.Cols("Col_LessThan").SafeIndex
                Dim Col_Equal As Integer = ConstraintsGrid.Cols("Col_Equal").SafeIndex
                Dim Col_GreaterThan As Integer = ConstraintsGrid.Cols("Col_GreaterThan").SafeIndex

                For ConstraintsGridRowCounter = 1 To (ConstraintsGrid.Rows.Count - 1)
                  Try
                    thisConstraintsGridRow = ConstraintsGrid.Rows(ConstraintsGridRowCounter)

                    If (Not thisConstraintsGridRow.IsNew) Then
                      ' If LessThan, Equal and GreaterThan are all set, Discard the constraint
                      ' as this will equait to another 'Sum(Weight) = 1' constraint.

                      If Not (CBool(thisConstraintsGridRow(Col_LessThan)) And CBool(thisConstraintsGridRow(Col_Equal)) And CBool(thisConstraintsGridRow(Col_GreaterThan))) Then

                        CalculateConstraintExposures(ConstraintsGrid, thisConstraintsGridRow, pForceUpdate, thisConstraintsTabDetails.ConstraintsGroupID, thisConstraintsTabDetails.CustomFieldID, "", thisConstraintsTabDetails.CustomFieldDataType, InstrumentWeights)

                      End If

                    End If
                  Catch ex As Exception
                  End Try
                Next

              End If

            End If

          Catch ex As Exception

          End Try
        Next

      Catch ex As Exception

      End Try

    Catch ex As Exception

    End Try

  End Function

  Public Function CalculateConstraintExposures(ByRef ConstraintsGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByRef pConstraintRow As C1.Win.C1FlexGrid.Row, ByVal pForceUpdate As Boolean, Optional ByVal ConstraintGroupID As Integer = 0, Optional ByVal ThisCustomFieldID As Integer = 0, Optional ByVal ThisCustomFieldName As String = "", Optional ByVal ThisCustomFieldDataType As Integer = (-1), Optional ByVal pInstrumentWeights() As Double = Nothing) As Boolean
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim InstrumentWeights() As Double
    Dim thisConstraintsTabDetails As ConstraintsTabDetails = Nothing

    Try
      If (Me.IsDisposed) Or (Not Me.Created) Then
        Return True
      End If

      If (Constraints_TabConstraints.Visible = False) And (pForceUpdate = False) Then
        Return True
      End If

      If (pConstraintRow Is Nothing) Then
        Return True
      End If

      ' Get Constraints Tab Details.

      Try
        If (ConstraintsGrid.Tag IsNot Nothing) AndAlso (TypeOf (ConstraintsGrid.Tag) Is ConstraintsTabDetails) Then
          thisConstraintsTabDetails = CType(ConstraintsGrid.Tag, ConstraintsTabDetails)
        End If
      Catch ex As Exception
      End Try

      ' Validate Optional parameter values

      If (ThisCustomFieldID = 0) Then
        If (thisConstraintsTabDetails IsNot Nothing) Then
          ThisCustomFieldID = thisConstraintsTabDetails.CustomFieldID
        Else
          Return False
        End If
      End If

      If (ThisCustomFieldDataType < 0) Then
        If (thisConstraintsTabDetails IsNot Nothing) Then
          ThisCustomFieldDataType = thisConstraintsTabDetails.CustomFieldDataType
        Else
          Return False
        End If
      End If

      ' Get InstrumentWeights, if necessary

      If (pInstrumentWeights Is Nothing) Then

        InstrumentWeights = ConstraintWorker.InstrumentWeights(Grid_Returns)

      Else
        InstrumentWeights = pInstrumentWeights
      End If

      ' Get Constraint Weighting array

      Dim ConstraintArray() As Double
      ConstraintArray = ConstraintWorker.ReturnMarginalsConstraintArrayFromConstraintGridRow(ConstraintsGrid, pConstraintRow, ConstraintGroupID, ThisCustomFieldID, ThisCustomFieldName, ThisCustomFieldDataType)

      ' Calculate Current exposure.
      Dim CurrentWeight As Double = 0
      Dim InstrumentCounter As Integer

      Try
        If (ConstraintArray IsNot Nothing) AndAlso (InstrumentWeights IsNot Nothing) Then
          For InstrumentCounter = 0 To (InstrumentWeights.Length - 1)
            If (InstrumentCounter < ConstraintArray.Length) Then
              CurrentWeight += (InstrumentWeights(InstrumentCounter) * ConstraintArray(InstrumentCounter))
            End If
          Next
        End If

      Catch ex As Exception
      End Try

      ' Set Value

      pConstraintRow("Col_Exposure") = CurrentWeight

    Catch ex As Exception
      Return False
    End Try

    Return True

  End Function

  Private Sub IndexComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    If (sender.Focused) AndAlso (InPaint = False) AndAlso (Me.IsDisposed = False) AndAlso (Grid_Returns.Row > 0) Then
      Set_InstrumentsGridChanged(sender, e)
      Grid_Returns.Item(Grid_Returns.Row, 0) = "x"

      Dim PertracID As Integer = 0
      Dim IndexID As Integer = 0

      Dim Col_GroupIndexCode As Integer = Grid_Returns.Cols("GroupIndexCode").SafeIndex
      Dim Col_GroupIndexE As Integer = Grid_Returns.Cols("GroupIndexE").SafeIndex

      Dim ThisItem As Object

      Try

        ThisItem = Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols.IndexOf("GroupPertracCode"))
        If (ThisItem IsNot Nothing) AndAlso (IsNumeric(ThisItem)) Then
          PertracID = CInt(ThisItem)
        End If

        If (TypeOf sender Is ComboBox) Then
          ThisItem = CType(sender, ComboBox).SelectedValue
        ElseIf (TypeOf sender Is FCP_TelerikControls.FCP_RadComboBox) Then
          ThisItem = CType(sender, FCP_TelerikControls.FCP_RadComboBox).SelectedValue
        End If

        If (ThisItem IsNot Nothing) AndAlso (IsNumeric(ThisItem)) Then
          IndexID = CInt(ThisItem)
        End If

        ' Set Alpha and Beta

        GetAlphaBetaAndStdError(PertracID, IndexID, Grid_Returns.Row)

        ' Set Index Expected return to match Index return for an existing line with the same Index (if such exists).

        Grid_Returns.Item(Grid_Returns.Row, Col_GroupIndexCode) = IndexID

        'If (IndexID > 0) Then
        '	For RowCounter = 1 To (Me.Grid_Returns.Rows.Count - 1)
        '		If (RowCounter <> Grid_Returns.Row) Then
        '			Try
        '				If (CInt(Grid_Returns.Item(RowCounter, Col_GroupIndexCode)) = IndexID) AndAlso (IsNumeric(Grid_Returns.Item(RowCounter, Col_GroupIndexE))) Then
        '					Grid_Returns.Item(Grid_Returns.Row, Col_GroupIndexE) = Grid_Returns.Item(RowCounter, Col_GroupIndexE)
        '					Exit For
        '				End If
        '			Catch ex As Exception
        '			End Try
        '		End If
        '	Next
        'End If

      Catch ex As Exception
        Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols("GroupAlpha").SafeIndex) = 0
        Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Cols("GroupBeta").SafeIndex) = 0
      End Try

    End If
  End Sub

  Private Sub SectorComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    If (InPaint = False) AndAlso (Me.IsDisposed = False) AndAlso (Grid_Returns.Row > 0) Then
      Dim SectorCombo As CustomC1Combo

      Try
        If Not (TypeOf sender Is CustomC1Combo) Then
          Exit Sub
        End If

        SectorCombo = CType(sender, CustomC1Combo)

        If (SectorCombo.SelectedIndex < 0) Then
          Dim DTable As DataTable = SectorCombo.DataSource
          Dim NewRow As DataRow = DTable.NewRow

          NewRow("DM") = SectorCombo.Text
          NewRow("VM") = SectorCombo.Text

          DTable.Rows.Add(NewRow)
          SectorCombo.SelectedIndex = SectorCombo.Items.Count - 1
          SectorCombo.Text = SectorCombo.SelectedValue.ToString
        End If

        Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Col) = SectorCombo.SelectedValue.ToString
      Catch ex As Exception
        Grid_Returns.Item(Grid_Returns.Row, Grid_Returns.Col) = ""
      End Try

    End If

  End Sub

  Private Sub GetAlphaBetaAndStdError(ByVal PertracID As Integer, ByVal ReferenceID As Integer, ByVal GridRow As Integer)

    Dim StartDate As Date
    Dim EndDate As Date

    Dim ThisItem As Object

    Try
      ThisItem = Grid_Returns.Item(GridRow, Grid_Returns.Cols.IndexOf("GroupDateFrom"))
      If (ThisItem IsNot Nothing) AndAlso (IsDate(ThisItem)) Then
        StartDate = CDate(ThisItem)
      Else
        ReferenceID = 0
      End If

      ThisItem = Grid_Returns.Item(GridRow, Grid_Returns.Cols.IndexOf("GroupDateTo"))
      If (ThisItem IsNot Nothing) AndAlso (IsDate(ThisItem)) Then
        EndDate = CDate(ThisItem)
      Else
        ReferenceID = 0
      End If

      If (PertracID > 0) AndAlso (ReferenceID > 0) Then
        Dim thisStatObject As StatFunctions.ComparisonStatsClass

        thisStatObject = MainForm.StatFunctions.GetComparisonStatsItem(MainForm.PertracData.GetPertracDataPeriod(PertracID, ReferenceID), CULng(PertracID), ReferenceID, False, CULng(PertracID), StartDate, EndDate, 1, 999, -1)

        Grid_Returns.Item(GridRow, Grid_Returns.Cols("GroupAlpha").SafeIndex) = thisStatObject.Alpha(PertracID)
        Grid_Returns.Item(GridRow, Grid_Returns.Cols("GroupBeta").SafeIndex) = thisStatObject.Beta(PertracID)
        Grid_Returns.Item(GridRow, Grid_Returns.Cols("GroupStdErr").SafeIndex) = thisStatObject.StandardError(PertracID)
      Else
        Grid_Returns.Item(GridRow, Grid_Returns.Cols("GroupAlpha").SafeIndex) = 0
        Grid_Returns.Item(GridRow, Grid_Returns.Cols("GroupBeta").SafeIndex) = 0
        Grid_Returns.Item(GridRow, Grid_Returns.Cols("GroupStdErr").SafeIndex) = 0
      End If

    Catch ex As Exception
      Grid_Returns.Item(GridRow, Grid_Returns.Cols("GroupAlpha").SafeIndex) = 0
      Grid_Returns.Item(GridRow, Grid_Returns.Cols("GroupBeta").SafeIndex) = 0
      Grid_Returns.Item(GridRow, Grid_Returns.Cols("GroupStdErr").SafeIndex) = 0
    End Try

  End Sub

  Private Sub Split_GroupReturns_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Split_GroupReturns.Resize
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      Me.Chart_MonthlyReturns.Top = ((Split_GroupReturns.Panel2.Height - Label_ChartStock.Height) / 2) + Label_ChartStock.Height
      Me.Chart_MonthlyReturns.Height = (Split_GroupReturns.Panel2.Height - Label_ChartStock.Height) / 2
      Me.Chart_VAMI.Height = (Split_GroupReturns.Panel2.Height - Label_ChartStock.Height) / 2

    Catch ex As Exception

    End Try
  End Sub

  Private Sub GetVeniceSector(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      GetVeniceSectors(SelectionEnum.SingleRow)
    Catch ex As Exception
    End Try

  End Sub

  Private Sub GetSelectedVeniceSectors(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      GetVeniceSectors(SelectionEnum.MultipleRows)
    Catch ex As Exception
    End Try

  End Sub

  Private Sub GetAllVeniceSectors(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      GetVeniceSectors(SelectionEnum.AllRows)
    Catch ex As Exception
    End Try

  End Sub

  Private Sub GetVeniceSectors(Optional ByVal SelectionType As SelectionEnum = SelectionEnum.SingleRow)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim ThisPertracID As Integer
    Dim VeniceSector As String
    Dim VeniceFundType As Integer
    Dim GridRow As Integer
    Dim StartRow As Integer = 0
    Dim EndRow As Integer = 0

    Select Case SelectionType

      Case SelectionEnum.MultipleRows
        If (Grid_Returns.Selection.IsValid) Then
          StartRow = Grid_Returns.Selection.r1
          EndRow = Grid_Returns.Selection.r2
        End If

      Case SelectionEnum.AllRows
        StartRow = 1
        EndRow = Grid_Returns.Rows.Count - 1

      Case Else
        StartRow = InstrumentsGridClickMouseRow
        EndRow = InstrumentsGridClickMouseRow

    End Select

    Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
    Dim Col_GroupSector As Integer = Grid_Returns.Cols("GroupSector").SafeIndex

    If (Me.Created) AndAlso (InPaint = False) Then
      Try
        For GridRow = StartRow To EndRow
          If (GridRow > 0) AndAlso (GridRow < Grid_Returns.Rows.Count) Then

            Try

              ThisPertracID = Grid_Returns.Item(GridRow, Col_GroupPertracCode)
              VeniceFundType = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, 0, "InstrumentFundType", "InstrumentID", "InstrumentPertracCode=" & ThisPertracID.ToString)

              If (ThisPertracID <= 0) OrElse (VeniceFundType <= 0) Then
                Grid_Returns.Item(GridRow, Col_GroupSector) = ""
              Else

                VeniceSector = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblFundType, VeniceFundType, "FundTypeDescription")

                CheckSectorComboContains(VeniceSector)

                If (Grid_Returns.Row = GridRow) Then
                  Me.SectorCombo.SelectedValue = VeniceSector
                  Me.SectorCombo.Text = VeniceSector
                End If

                Try
                  InPaint = True
                  Grid_Returns.Item(GridRow, Col_GroupSector) = VeniceSector
                Catch ex As Exception
                Finally
                  InPaint = False
                End Try

              End If

            Catch ex As Exception
              Grid_Returns.Item(GridRow, Col_GroupSector) = ""
            Finally
              Grid_Returns.Item(GridRow, 0) = "x"
            End Try

          End If
        Next

      Catch ex As Exception
      Finally
        Call Set_InstrumentsGridChanged(Grid_Returns, New EventArgs)
      End Try
    End If

  End Sub

  Private Sub GetPertracSector(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      GetPertracSectors(SelectionEnum.SingleRow)
    Catch ex As Exception
    End Try

  End Sub

  Private Sub GetSelectedPertracSectors(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      GetPertracSectors(SelectionEnum.MultipleRows)
    Catch ex As Exception
    End Try

  End Sub

  Private Sub GetAllPertracSectors(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      GetPertracSectors(SelectionEnum.AllRows)
    Catch ex As Exception
    End Try

  End Sub

  Private Sub GetPertracSectors(Optional ByVal SelectionType As SelectionEnum = SelectionEnum.SingleRow)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim ThisPertracID As Integer
    Dim PertracSector As String
    Dim TempValue As Object
    Dim GridRow As Integer
    Dim StartRow As Integer = 0
    Dim EndRow As Integer = 0

    Select Case SelectionType

      Case SelectionEnum.MultipleRows
        If (Grid_Returns.Selection.IsValid) Then
          StartRow = Grid_Returns.Selection.r1
          EndRow = Grid_Returns.Selection.r2
        End If

      Case SelectionEnum.AllRows
        StartRow = 1
        EndRow = Grid_Returns.Rows.Count - 1

      Case Else
        StartRow = InstrumentsGridClickMouseRow
        EndRow = InstrumentsGridClickMouseRow

    End Select

    Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
    Dim Col_GroupSector As Integer = Grid_Returns.Cols("GroupSector").SafeIndex

    If (Me.Created) AndAlso (InPaint = False) Then
      Try
        For GridRow = StartRow To EndRow
          If (GridRow > 0) AndAlso (GridRow < Grid_Returns.Rows.Count) Then
            Try

              ThisPertracID = Grid_Returns.Item(GridRow, Col_GroupPertracCode)
              TempValue = MainForm.PertracData.GetInformationValue(ThisPertracID, PertracInformationFields.Strategy).ToString()

              If (ThisPertracID <= 0) OrElse (TempValue Is Nothing) Then
                Grid_Returns.Item(GridRow, Col_GroupSector) = ""
              Else

                PertracSector = CStr(TempValue)

                CheckSectorComboContains(PertracSector)

                If (Grid_Returns.Row = GridRow) Then
                  Me.SectorCombo.SelectedValue = PertracSector
                  Me.SectorCombo.Text = PertracSector
                End If

                Try
                  InPaint = True
                  Grid_Returns.Item(GridRow, Col_GroupSector) = PertracSector
                Catch ex As Exception
                Finally
                  InPaint = False
                End Try

              End If

            Catch ex As Exception
              Grid_Returns.Item(GridRow, Col_GroupSector) = ""

            Finally
              Grid_Returns.Item(GridRow, 0) = "x"

            End Try
          End If
        Next

      Catch ex As Exception
      Finally
        Call Set_InstrumentsGridChanged(Grid_Returns, New EventArgs)
      End Try
    End If
  End Sub

  Private Sub GetVeniceLiquidity(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      GetVeniceLiquidities(SelectionEnum.SingleRow)
    Catch ex As Exception
    End Try

  End Sub

  Private Sub GetSelectedVeniceLiquidities(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      GetVeniceLiquidities(SelectionEnum.MultipleRows)
    Catch ex As Exception
    End Try

  End Sub

  Private Sub GetAllVeniceLiquidities(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      GetVeniceLiquidities(SelectionEnum.AllRows)
    Catch ex As Exception
    End Try

  End Sub

  Private Sub GetVeniceLiquidities(Optional ByVal SelectionType As SelectionEnum = SelectionEnum.SingleRow)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim ThisPertracID As Integer
    Dim VeniceDealingPeriod As RenaissanceGlobals.DealingPeriod
    Dim VeniceDealingDays As Integer
    Dim GridRow As Integer
    Dim StartRow As Integer = 0
    Dim EndRow As Integer = 0

    Select Case SelectionType

      Case SelectionEnum.MultipleRows
        If (Grid_Returns.Selection.IsValid) Then
          StartRow = Grid_Returns.Selection.r1
          EndRow = Grid_Returns.Selection.r2
        End If

      Case SelectionEnum.AllRows
        StartRow = 1
        EndRow = Grid_Returns.Rows.Count - 1

      Case Else
        StartRow = InstrumentsGridClickMouseRow
        EndRow = InstrumentsGridClickMouseRow

    End Select

    Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
    Dim Col_GroupLiquidity As Integer = Grid_Returns.Cols("GroupLiquidity").SafeIndex

    If (Me.Created) AndAlso (InPaint = False) Then
      Try
        For GridRow = StartRow To EndRow
          If (GridRow > 0) AndAlso (GridRow < Grid_Returns.Rows.Count) Then

            Try

              ThisPertracID = Grid_Returns.Item(GridRow, Col_GroupPertracCode)
              VeniceDealingPeriod = CType(LookupTableValue(MainForm, RenaissanceStandardDatasets.tblInstrument, 0, "InstrumentDealingPeriod", "InstrumentID", "InstrumentPertracCode=" & ThisPertracID.ToString), RenaissanceGlobals.DealingPeriod)

              If (ThisPertracID <= 0) OrElse (VeniceDealingPeriod <= 0) Then
                Grid_Returns.Item(GridRow, Col_GroupLiquidity) = 0
              Else

                VeniceDealingDays = 0
                Select Case VeniceDealingPeriod
                  Case DealingPeriod.Daily
                    VeniceDealingDays = 1

                  Case DealingPeriod.Weekly
                    VeniceDealingDays = 7

                  Case DealingPeriod.Fortnightly
                    VeniceDealingDays = 14

                  Case DealingPeriod.Monthly
                    VeniceDealingDays = 30

                  Case DealingPeriod.Quarterly
                    VeniceDealingDays = 90

                  Case DealingPeriod.SemiAnnually
                    VeniceDealingDays = 180

                  Case DealingPeriod.Annually
                    VeniceDealingDays = 365

                End Select

                Try
                  InPaint = True
                  Grid_Returns.Item(GridRow, Col_GroupLiquidity) = VeniceDealingDays
                Catch ex As Exception
                Finally
                  InPaint = False
                End Try

              End If

            Catch ex As Exception
              Grid_Returns.Item(GridRow, Col_GroupLiquidity) = 0
            Finally
              Grid_Returns.Item(GridRow, 0) = "x"
            End Try

          End If
        Next

      Catch ex As Exception
      Finally
        Call Set_InstrumentsGridChanged(Grid_Returns, New EventArgs)
      End Try
    End If

  End Sub

  Private Sub DeleteGroupCustomFieldData(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim ThisPertracID As Integer
    Dim ThisGroupListID As Integer
    Dim ThisCustomFieldID As Integer
    Dim ThisColumnName As String

    Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex

    If (Me.Created) AndAlso (InPaint = False) AndAlso (InstrumentsGridClickMouseRow >= 0) AndAlso (InstrumentsGridClickMouseCol >= 2) Then
      If MessageBox.Show("Are you sure that you want to delete this Custom Field Data Item ?", "Delete ?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) <> Windows.Forms.DialogResult.Yes Then
        Exit Sub
      End If

      Try
        ThisPertracID = Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupPertracCode)
        ThisColumnName = Grid_Returns.Cols(InstrumentsGridClickMouseCol).Name

        If (ThisColumnName.StartsWith("Custom_") = False) OrElse (IsNumeric(ThisColumnName.Substring(7)) = False) Then
          Exit Sub
        End If
        If (Combo_SelectGroupID.SelectedValue Is Nothing) OrElse (IsNumeric(Combo_SelectGroupID.SelectedValue) = False) Then
          Exit Sub
        End If

        ThisCustomFieldID = CInt(ThisColumnName.Substring(7))
        ThisGroupListID = CInt(Combo_SelectGroupID.SelectedValue)

        Dim tmpCommand As New SqlCommand

        Try
          tmpCommand.CommandType = CommandType.StoredProcedure
          tmpCommand.CommandText = "adp_tblPertracCustomFieldData_DeleteCommand"
          tmpCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = ThisGroupListID
          tmpCommand.Parameters.Add("@CustomFieldID", SqlDbType.Int).Value = ThisCustomFieldID
          tmpCommand.Parameters.Add("@PertracID", SqlDbType.Int).Value = ThisPertracID

          tmpCommand.Connection = MainForm.GetGenoaConnection
          tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

          tmpCommand.ExecuteNonQuery()
        Catch ex As Exception
          MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Selecting from Custom Field Data" & vbCrLf & tmpCommand.CommandText, ex.StackTrace, True)
        Finally
          Try
            If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
              tmpCommand.Connection.Close()
              tmpCommand.Connection = Nothing
            End If
          Catch ex As Exception
          End Try
        End Try


        CustomFieldDataCache.ClearDataCacheGroupData(ThisCustomFieldID, ThisGroupListID)

        Try
          InPaint = True

          ' Return Data for Group Zero, hiving just deleted the data for this group!

          Grid_Returns.Item(InstrumentsGridClickMouseRow, InstrumentsGridClickMouseCol) = CustomFieldDataCache.GetDataPoint(ThisCustomFieldID, 0, ThisPertracID)

          Application.DoEvents()
        Catch ex As Exception
        Finally
          InPaint = False
        End Try

      Catch ex As Exception
      End Try

      MainForm.ReloadTable(RenaissanceChangeID.tblPertracCustomFieldData, False)

      Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblPertracCustomFieldData, (ThisCustomFieldID.ToString & "." & ThisGroupListID.ToString)))

    End If

  End Sub

  Private Sub GetGroupZeroCustomFieldData(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim ThisPertracID As Integer
    Dim ThisCustomFieldID As Integer
    Dim ThisColumnName As String

    Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex

    If (Me.Created) AndAlso (InPaint = False) AndAlso (InstrumentsGridClickMouseRow >= 0) AndAlso (InstrumentsGridClickMouseCol >= 2) Then
      Try
        ThisPertracID = Grid_Returns.Item(InstrumentsGridClickMouseRow, Col_GroupPertracCode)
        ThisColumnName = Grid_Returns.Cols(InstrumentsGridClickMouseCol).Name

        If (ThisColumnName.StartsWith("Custom_") = False) OrElse (IsNumeric(ThisColumnName.Substring(7)) = False) Then
          Exit Sub
        End If
        If (Combo_SelectGroupID.SelectedValue Is Nothing) OrElse (IsNumeric(Combo_SelectGroupID.SelectedValue) = False) Then
          Exit Sub
        End If

        ThisCustomFieldID = CInt(ThisColumnName.Substring(7))

        Grid_Returns.Item(InstrumentsGridClickMouseRow, InstrumentsGridClickMouseCol) = CustomFieldDataCache.GetDataPoint(ThisCustomFieldID, 0, ThisPertracID)

      Catch ex As Exception
      End Try
    End If

  End Sub

  Private Sub CheckSectorComboContains(ByVal pSectorName As String)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim ThisTable As DataTable
    Dim ThisRow As DataRow
    Dim RowCount As Integer

    ThisTable = SectorCombo.DataSource
    For RowCount = 0 To (ThisTable.Rows.Count - 1)
      ThisRow = ThisTable.Rows(RowCount)

      If ThisRow("VM") = pSectorName Then
        Exit Sub
      End If
    Next

    ThisRow = ThisTable.NewRow
    ThisRow("DM") = pSectorName
    ThisRow("VM") = pSectorName
    ThisTable.Rows.Add(ThisRow)

  End Sub

  Private Sub SetGrid_StandardReturnColumns()
    ' ***************************************************************************
    '
    '
    ' ***************************************************************************

    Dim thisColID As Integer
    Dim thisCol As C1.Win.C1FlexGrid.Column

    Grid_Returns.Cols("Col_Updated").Move(0)
    Grid_Returns.Cols("Col_CustomUpdated").Move(1)
    Grid_Returns.Cols("PertracName").Move(2)
    Grid_Returns.Cols("GroupIndexCode").Move(3)
    Grid_Returns.Cols("GroupBeta").Move(4)
    Grid_Returns.Cols("GroupAlpha").Move(5)
    Grid_Returns.Cols("GroupIndexE").Move(6)
    Grid_Returns.Cols("GroupBetaE").Move(7)
    Grid_Returns.Cols("GroupAlphaE").Move(8)
    Grid_Returns.Cols("GroupExpectedReturn").Move(9)
    Grid_Returns.Cols("GroupStdErr").Move(10)

    For thisColID = 0 To 10
      Grid_Returns.Cols(thisColID).Visible = True
    Next
    For thisColID = 11 To (Grid_Returns.Cols.Count - 1)
      Grid_Returns.Cols(thisColID).Visible = False
    Next

    If (FieldsMenu IsNot Nothing) Then
      Dim ThisFieldMenu As ToolStripMenuItem

      For thisColID = 2 To (Grid_Returns.Cols.Count - 1) ' Exclude Col_Update and Col_CustomUpdate
        thisCol = Grid_Returns.Cols(thisColID)
        Try
          ThisFieldMenu = CType(FieldsMenu.DropDownItems("Menu_GridField_" & thisCol.Name), ToolStripMenuItem)

          If (ThisFieldMenu IsNot Nothing) Then
            ThisFieldMenu.Checked = thisCol.Visible
          End If
        Catch ex As Exception
        End Try
      Next
    End If

    Try
      ' Clear Custom Field Checks
      Dim ThisDropDownItem As ToolStripDropDownItem

      For Each ThisDropDownItem In CustomFieldsMenu.DropDownItems
        CType(ThisDropDownItem, ToolStripMenuItem).Checked = False
      Next
    Catch ex As Exception
    End Try
  End Sub

  Private Sub SetGrid_StandardOptimiserColumns()
    ' ***************************************************************************
    '
    '
    ' ***************************************************************************

    Dim thisColID As Integer
    Dim thisCol As C1.Win.C1FlexGrid.Column

    Grid_Returns.Cols("Col_Updated").Move(0)
    Grid_Returns.Cols("Col_CustomUpdated").Move(1)
    Grid_Returns.Cols("PertracName").Move(2)
    Grid_Returns.Cols("GroupSector").Move(3)
    Grid_Returns.Cols("GroupHolding").Move(4)
    Grid_Returns.Cols("GroupTrade").Move(5)
    Grid_Returns.Cols("GroupNewHolding").Move(6)
    ' Grid_Returns.Cols("GroupTradeSize").Move(7)
    Grid_Returns.Cols("GroupPercent").Move(7)
    Grid_Returns.Cols("GroupNewPercent").Move(8)
    Grid_Returns.Cols("GroupFloor").Move(9)
    Grid_Returns.Cols("GroupCap").Move(10)
    Grid_Returns.Cols("GroupExpectedReturn").Move(11)
    Grid_Returns.Cols("GroupLowerBound").Move(12)
    Grid_Returns.Cols("GroupUpperBound").Move(13)
    Grid_Returns.Cols("GroupLiquidity").Move(14)

    For thisColID = 0 To 14
      Grid_Returns.Cols(thisColID).Visible = True
    Next
    For thisColID = 15 To (Grid_Returns.Cols.Count - 1)
      Grid_Returns.Cols(thisColID).Visible = False
    Next

    If (FieldsMenu IsNot Nothing) Then
      Dim ThisFieldMenu As ToolStripMenuItem

      For thisColID = 2 To (Grid_Returns.Cols.Count - 1) ' Exclude Col_Update, Col_CustomUpdate
        thisCol = Grid_Returns.Cols(thisColID)
        Try
          ThisFieldMenu = CType(FieldsMenu.DropDownItems("Menu_GridField_" & thisCol.Name), ToolStripMenuItem)
          If (ThisFieldMenu IsNot Nothing) Then
            ThisFieldMenu.Checked = thisCol.Visible
          End If
        Catch ex As Exception
        End Try
      Next
    End If

    Try
      ' Clear Custom Field Checks
      Dim ThisDropDownItem As ToolStripDropDownItem

      For Each ThisDropDownItem In CustomFieldsMenu.DropDownItems
        CType(ThisDropDownItem, ToolStripMenuItem).Checked = False
      Next
    Catch ex As Exception
    End Try

  End Sub

  ' ---------------------------------------------------------------------------------
  ' Tab Marginals
  ' ---------------------------------------------------------------------------------

  Private Sub InitialiseMarginalsGrid()
    ' *****************************************************************************************
    ' One-Off initialisation of the Marginals Grid
    '
    ' Add the Update flag Columns and establish custon grid styles.
    '
    ' *****************************************************************************************

    Try

      Grid_Marginals.Cols.Count = 0
      Grid_Marginals.AutoGenerateColumns = True
      Grid_Marginals.DataSource = New DSMarginals.tblMarginalsDataTable
      Grid_Marginals.AutoGenerateColumns = False

      Dim thisCol As C1.Win.C1FlexGrid.Column
      Dim thisColID As Integer

      For thisColID = 0 To (Grid_Marginals.Cols.Count - 1) '
        thisCol = Grid_Marginals.Cols(thisColID)

        thisCol.Caption = thisCol.Name
      Next

      Grid_Marginals.Cols("ContraID").Visible = False
      Grid_Marginals.Cols("WorstDrawdown").Visible = False
      Grid_Marginals.Cols("WorstDrawdown").Format = "#,##0.00%"
    Catch ex As Exception
    End Try

    ' Add cms handler

    Grid_Marginals.ContextMenuStrip = New ContextMenuStrip
    Grid_Marginals.ContextMenuStrip.Tag = New MarginalsGridDetails
    AddHandler Grid_Marginals.ContextMenuStrip.Opening, AddressOf Grid_Marginals_cms_Opening
    Grid_Marginals.ContextMenuStrip.Items.Add(New ToolStripMenuItem(" "))

    ' Set Positive / negative display styles

    Dim ThisStyle As C1.Win.C1FlexGrid.CellStyle

    Try

      ThisStyle = Me.Grid_Marginals.Styles("Positive")
      If (ThisStyle Is Nothing) Then
        ThisStyle = Me.Grid_Marginals.Styles.Add("Positive")
      End If
      ThisStyle.ForeColor = Color.Black

      ThisStyle = Me.Grid_Marginals.Styles("Negative")
      If (ThisStyle Is Nothing) Then
        ThisStyle = Me.Grid_Marginals.Styles.Add("Negative")
      End If
      ThisStyle.ForeColor = Color.Red

    Catch ex As Exception

    End Try

  End Sub

  Private Sub ResetMarginalsGrid()
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    If (Me.IsDisposed) Then
      Exit Sub
    End If

    Try
      Dim thisCol As C1.Win.C1FlexGrid.Column
      Dim thisColID As Integer

      For thisColID = 0 To (Grid_Marginals.Cols.Count - 1) '
        thisCol = Grid_Marginals.Cols(thisColID)

        thisCol.Visible = True
        thisCol.AllowEditing = False
        thisCol.AllowMerging = False
        thisCol.AllowDragging = True
        thisCol.Width = 60
      Next

      ' ReSet Column order
      Try
        If (Grid_Marginals.DataSource IsNot Nothing) Then
          Dim MarginalsTbl As DSMarginals.tblMarginalsDataTable

          MarginalsTbl = CType(Grid_Marginals.DataSource, DSMarginals.tblMarginalsDataTable)

          For thisColID = 0 To (MarginalsTbl.Columns.Count - 1) '
            Grid_Marginals.Cols(MarginalsTbl.Columns(thisColID).ColumnName).Move(thisColID)
          Next

        End If
      Catch ex As Exception
      End Try

      Try
        If (Grid_Marginals.DataSource IsNot Nothing) Then
          CType(Grid_Marginals.DataSource, DataTable).Rows.Clear()
        End If
      Catch ex As Exception
      End Try

      ' Set Formats and Widths etc...

      Grid_Marginals.Cols("InstrumentName").Caption = "Name"
      Grid_Marginals.Cols("TradeAmount").Caption = "Trade"
      Grid_Marginals.Cols("NewHolding").Caption = "New Holding"
      Grid_Marginals.Cols("NewWeight").Caption = "New Weight"
      Grid_Marginals.Cols("AboveTargetReturn").Caption = "Return OK"
      Grid_Marginals.Cols("BelowTargetRisk").Caption = "Risk OK"
      Grid_Marginals.Cols("PortfolioRisk").Caption = "Risk"
      Grid_Marginals.Cols("PortfolioReturn").Caption = "Return"
      Grid_Marginals.Cols("SharpeRatio").Caption = "Sharpe"

      Grid_Marginals.Cols("PertracID").Width = 60
      Grid_Marginals.Cols("InstrumentName").Width = 200
      Grid_Marginals.Cols("BuySell").Width = 40
      Grid_Marginals.Cols("TradeAmount").Width = 100
      Grid_Marginals.Cols("NewHolding").Width = 100
      Grid_Marginals.Cols("NewWeight").Width = 50
      Grid_Marginals.Cols("LimitFlag").Width = 40
      Grid_Marginals.Cols("LimitComment").Width = 200
      Grid_Marginals.Cols("AboveTargetReturn").Width = 40
      Grid_Marginals.Cols("BelowTargetRisk").Width = 40

      Grid_Marginals.Cols("TradeAmount").Format = "#,##0.0"
      Grid_Marginals.Cols("NewHolding").Format = "#,##0.0"
      Grid_Marginals.Cols("NewWeight").Format = "#,##0.00%"
      Grid_Marginals.Cols("PortfolioRisk").Format = "#,##0.00%"
      Grid_Marginals.Cols("PortfolioReturn").Format = "#,##0.00%"
      Grid_Marginals.Cols("SharpeRatio").Format = "#,##0.00"

    Catch ex As Exception
    End Try

  End Sub

  Private Sub FormatMarginalsGridRows()
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    Try
      Dim RowCount As Integer
      Dim Col_BuySell As Integer

      Col_BuySell = Grid_Marginals.Cols("BuySell").SafeIndex

      For RowCount = 1 To (Grid_Marginals.Rows.Count - 1)
        If Grid_Marginals.Item(RowCount, Col_BuySell).ToString.ToUpper = "SELL" Then
          Grid_Marginals.Rows(RowCount).Style = Grid_Marginals.Styles("Negative")
        Else
          Grid_Marginals.Rows(RowCount).Style = Grid_Marginals.Styles("Positive")
        End If
      Next
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Check_Marginals_SellAll_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Marginals_SellAll.CheckedChanged
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    Try
      If (Check_Marginals_SellAll.Checked) Then
        Check_Marginals_Sell.Checked = True
        Check_Marginals_Sell.Enabled = False
      Else
        Check_Marginals_Sell.Enabled = True
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Check_Marginals_ContraTrade_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Marginals_ContraTrade.CheckedChanged
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    Try
      If (Check_Marginals_ContraTrade.Checked) Then
        Combo_Marginals_ContraInstrument.Enabled = True
      Else
        Combo_Marginals_ContraInstrument.Enabled = False
        If (Combo_Marginals_ContraInstrument.Items.Count > 0) Then
          Combo_Marginals_ContraInstrument.SelectedIndex = 0
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_ImproveReturn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ImproveReturn.CheckedChanged
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    Try
      If (Me.Created AndAlso (Not Me.IsDisposed)) Then
        If (Radio_ImproveReturn.Checked) Then
          ' Radio_ImproveReturn.Checked = False
          Radio_ImproveRisk.Checked = False
          Radio_ImproveSharpeRatio.Checked = False
          Radio_ImproveTargetRiskReturn.Checked = False
          Radio_ImproveWorstDrawDown.Checked = False
          Radio_ImproveLeastSquares.Checked = False
          Radio_ImproveStdError.Checked = False
          Check_Marginals_AnalysisPeriod.Enabled = False

          Label_MarginalsRiskLimit.Text = "Risk Limit (<=)"
          Label_MarginalsReturnTarget.Text = "Return Target (>=)"

          If (Radio_ImproveLeastSquares.Checked OrElse Radio_ImproveStdError.Checked) AndAlso (Combo_MarginalIndex.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_MarginalIndex.SelectedValue)) Then
            Call PaintGroupReturnChart()
          End If
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_ImproveRisk_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ImproveRisk.CheckedChanged
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    Try
      If (Me.Created AndAlso (Not Me.IsDisposed)) Then
        If (Radio_ImproveRisk.Checked) Then
          Radio_ImproveReturn.Checked = False
          ' Radio_ImproveRisk.Checked = False
          Radio_ImproveSharpeRatio.Checked = False
          Radio_ImproveTargetRiskReturn.Checked = False
          Radio_ImproveWorstDrawDown.Checked = False
          Radio_ImproveLeastSquares.Checked = False
          Radio_ImproveStdError.Checked = False
          Check_Marginals_AnalysisPeriod.Enabled = False

          Label_MarginalsRiskLimit.Text = "Risk Limit (<=)"
          Label_MarginalsReturnTarget.Text = "Return Target (>=)"
        End If

        Call PaintGroupReturnChart()
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_ImproveSharpeRatio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ImproveSharpeRatio.CheckedChanged
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    Try
      If (Radio_ImproveSharpeRatio.Checked) Then
        Radio_ImproveReturn.Checked = False
        Radio_ImproveRisk.Checked = False
        ' Radio_ImproveSharpeRatio.Checked = False
        Radio_ImproveTargetRiskReturn.Checked = False
        Radio_ImproveWorstDrawDown.Checked = False
        Radio_ImproveLeastSquares.Checked = False
        Radio_ImproveStdError.Checked = False
        Check_Marginals_AnalysisPeriod.Enabled = False

        Label_MarginalsRiskLimit.Text = "Risk Limit (<=)"
        Label_MarginalsReturnTarget.Text = "Return Target (>=)"
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_ImproveTargetRiskReturn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ImproveTargetRiskReturn.CheckedChanged
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    Try
      If (Radio_ImproveTargetRiskReturn.Checked) Then
        Radio_ImproveReturn.Checked = False
        Radio_ImproveRisk.Checked = False
        Radio_ImproveSharpeRatio.Checked = False
        ' Radio_ImproveTargetRiskReturn.Checked = False
        Radio_ImproveWorstDrawDown.Checked = False
        Radio_ImproveLeastSquares.Checked = False
        Radio_ImproveStdError.Checked = False
        Check_Marginals_AnalysisPeriod.Enabled = False

        Label_MarginalsRiskLimit.Text = "Risk Target"
        Label_MarginalsReturnTarget.Text = "Return Target"
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_ImproveWorstDrawDown_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ImproveWorstDrawDown.CheckedChanged
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    Try
      If (Grid_Marginals.Cols.Contains("WorstDrawdown")) Then
        Grid_Marginals.Cols("WorstDrawdown").Visible = Radio_ImproveWorstDrawDown.Checked
      End If

      If (Radio_ImproveWorstDrawDown.Checked) Then
        Radio_ImproveReturn.Checked = False
        Radio_ImproveRisk.Checked = False
        Radio_ImproveSharpeRatio.Checked = False
        Radio_ImproveTargetRiskReturn.Checked = False
        ' Radio_ImproveWorstDrawDown.Checked = False 
        Radio_ImproveLeastSquares.Checked = False
        Radio_ImproveStdError.Checked = False
        Check_Marginals_AnalysisPeriod.Enabled = False

        Label_MarginalsRiskLimit.Text = "Risk Limit (<=)"
        Label_MarginalsReturnTarget.Text = "Return Target (>=)"
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_ImproveLeastSquares_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ImproveLeastSquares.CheckedChanged
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    Try
      If (Radio_ImproveLeastSquares.Checked) Then
        Radio_ImproveReturn.Checked = False
        Radio_ImproveRisk.Checked = False
        Radio_ImproveSharpeRatio.Checked = False
        Radio_ImproveTargetRiskReturn.Checked = False
        Radio_ImproveWorstDrawDown.Checked = False
        ' Radio_ImproveLeastSquares.Checked = False
        Radio_ImproveStdError.Checked = False
        Check_Marginals_AnalysisPeriod.Enabled = True

        Label_MarginalsRiskLimit.Text = "Risk Target"
        Label_MarginalsReturnTarget.Text = "Return Target"
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Radio_ImproveStdError_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ImproveStdError.CheckedChanged
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    Try
      If (Radio_ImproveLeastSquares.Checked) Then
        Radio_ImproveReturn.Checked = False
        Radio_ImproveRisk.Checked = False
        Radio_ImproveSharpeRatio.Checked = False
        Radio_ImproveTargetRiskReturn.Checked = False
        Radio_ImproveWorstDrawDown.Checked = False
        Radio_ImproveLeastSquares.Checked = False
        ' Radio_ImproveStdError.Checked = False
        Check_Marginals_AnalysisPeriod.Enabled = True

        Label_MarginalsRiskLimit.Text = "Risk Target"
        Label_MarginalsReturnTarget.Text = "Return Target"
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Check_Marginals_AnalysisPeriod_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Marginals_AnalysisPeriod.CheckedChanged
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    Try
      Edit_Marginals_AnalysisPeriod.Enabled = Check_Marginals_AnalysisPeriod.Checked
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Grid_Marginals_AfterSort(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.SortColEventArgs) Handles Grid_Marginals.AfterSort
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    Try

      FormatMarginalsGridRows()

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Grid_Marginals_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Marginals.VisibleChanged
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    Try
      If Grid_Marginals.Visible Then
        UpdateMarginalsGrid(True)
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Combo_BenckmarkIndex_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_BenckmarkIndex.SelectedIndexChanged
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    Try
      If (Me.Created AndAlso (Not Me.IsDisposed)) Then
        If (Radio_ImproveLeastSquares.Checked OrElse Radio_ImproveStdError.Checked) AndAlso (Combo_MarginalIndex.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_MarginalIndex.SelectedValue)) Then
          Call PaintGroupReturnChart()
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Button_Marginals_Trade_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Marginals_Trade.Click
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    Call Marginals_TradeSelection(Nothing, New EventArgs)

    'Dim GridRowCounter As Integer
    'Dim ThisMarginalsRow As C1.Win.C1FlexGrid.Row
    'Dim SelectedRowCount As Integer = 0

    'Dim MarginalTrades(-1) As MarginalTrade

    'Try
    '	If (Not Grid_Marginals.Selection.IsValid) Then
    '		Exit Sub
    '	End If

    '	For GridRowCounter = Grid_Marginals.Selection.r1 To Grid_Marginals.Selection.r2
    '		ThisMarginalsRow = Grid_Marginals.Rows(GridRowCounter)

    '		If (Not ThisMarginalsRow.IsNew) Then
    '			SelectedRowCount += 1
    '		End If
    '	Next

    '	' Redim Arrays
    '	If (SelectedRowCount <= 0) Then
    '		Exit Sub
    '	End If

    '	ReDim MarginalTrades(SelectedRowCount - 1)

    '	' Record Trades

    '	SelectedRowCount = 0
    '	For GridRowCounter = Grid_Marginals.Selection.r1 To Grid_Marginals.Selection.r2
    '		ThisMarginalsRow = Grid_Marginals.Rows(GridRowCounter)

    '		If (Not ThisMarginalsRow.IsNew) Then
    '			MarginalTrades(SelectedRowCount) = New MarginalTrade(ThisMarginalsRow)

    '			SelectedRowCount += 1
    '		End If
    '	Next

    '	' Process Trades

    '	Try
    '		InPaint = True

    '		For SelectedRowCount = 0 To (MarginalTrades.Length - 1)
    '			ProcessMarginalsTrade(MarginalTrades(SelectedRowCount))
    '		Next

    '	Catch ex As Exception
    '	Finally
    '		InPaint = False

    '		Call Update_Group_Percentages(True, False)
    '		Call Update_Group_RiskAndReturn()
    '		Call UpdateMarginalsGrid()

    '	End Try

    '	' Update Portfolio Chart

    '	Try
    '		If (Label_NewHoldingReturn.Text.Length > 0) AndAlso (Label_NewHoldingRisk.Text.Length > 0) Then
    '			RiskReturnChartPoints.Add(New DecimalPoint(CDbl(ConvertValue(Label_NewHoldingRisk.Text, GetType(Double))), CDbl(ConvertValue(Label_NewHoldingReturn.Text, GetType(Double)))))

    '			PaintResultsCharts()
    '		End If
    '	Catch ex As Exception
    '	End Try

    'Catch ex As Exception
    'End Try

  End Sub

  Private Sub Button_Marginals_TradeTop1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_Marginals_TradeTop1.Click
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    Try
      Grid_Marginals.Select(1, 0, 1, 0)
      Call Marginals_TradeSelection(Nothing, New EventArgs)
    Catch ex As Exception
    End Try

  End Sub

  Private Sub ProcessMarginalsTrade(ByRef ThisMarginalsRow As C1.Win.C1FlexGrid.Row)
    ' ***************************************************************************************************
    ' Process Trade represented by 'ThisMarginalsRow' which must be of a grid with fields taken from
    ' tblMarginals.
    '
    ' ***************************************************************************************************

    Try

      ProcessMarginalsTrade(New MarginalTrade(ThisMarginalsRow))

    Catch ex As Exception
    End Try

  End Sub

  Private Sub ProcessMarginalsTrade(ByVal pPertracID As Integer, ByVal pContraID As Integer, ByVal pTradeSize As Double, ByVal pTradeDirection As String)
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    Try

      ProcessMarginalsTrade(New MarginalTrade(pPertracID, pContraID, pTradeSize, pTradeDirection))

    Catch ex As Exception

    End Try
  End Sub

  Private Sub ProcessMarginalsTrade(ByRef ThisMarginalTrade As MarginalTrade)
    ' ***************************************************************************************************
    ' Process Trade represented by 'ThisMarginalTrade'.
    '
    ' ***************************************************************************************************

    Dim ThisInstrumentsRow As C1.Win.C1FlexGrid.Row
    Dim InstrumentRowCounter As Integer

    Dim PertracID As Integer
    Dim ContraID As Integer
    Dim PertracTrade As Double
    Dim ContraTrade As Double

    Try
      PertracID = ThisMarginalTrade.PertracID
      ContraID = ThisMarginalTrade.ContraID
      PertracTrade = ThisMarginalTrade.PertracTrade
      ContraTrade = ThisMarginalTrade.ContraTrade

      ' OK, now Update the instruments (returns) Grid.

      Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
      Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
      Dim Col_GroupTrade As Integer = Grid_Returns.Cols("GroupTrade").SafeIndex
      Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
      Dim ThisInstrumentID As Integer

      For InstrumentRowCounter = 1 To (Grid_Returns.Rows.Count - 1)
        ThisInstrumentsRow = Grid_Returns.Rows(InstrumentRowCounter)

        If (Not ThisInstrumentsRow.IsNew) Then
          ThisInstrumentID = Nz(ThisInstrumentsRow(Col_GroupPertracCode), 0)

          If (ThisInstrumentID = PertracID) Then
            ThisInstrumentsRow(Col_GroupTrade) = CDbl(ThisInstrumentsRow(Col_GroupTrade)) + PertracTrade
            ThisInstrumentsRow(Col_GroupNewHolding) = CDbl(ThisInstrumentsRow(Col_GroupHolding)) + CDbl(ThisInstrumentsRow(Col_GroupTrade))

            If (ContraID = 0) Then
              Exit For
            End If
          ElseIf (ThisInstrumentID = ContraID) AndAlso (ContraID > 0) Then
            ThisInstrumentsRow(Col_GroupTrade) = CDbl(ThisInstrumentsRow(Col_GroupTrade)) + ContraTrade
            ThisInstrumentsRow(Col_GroupNewHolding) = CDbl(ThisInstrumentsRow(Col_GroupHolding)) + CDbl(ThisInstrumentsRow(Col_GroupTrade))
          End If
        End If
      Next

    Catch ex As Exception
    End Try

  End Sub

  ' ---------------------------------------------------------------------------------
  ' Tab Results
  ' ---------------------------------------------------------------------------------

  Private Sub Chart_Results_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Results.DoubleClick
    ' *******************************************************************************
    ' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
    ' with this chart.
    ' *******************************************************************************

    Dim newChartForm As frmViewChart

    newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
    newChartForm.GenoaParentForm = Me
    newChartForm.UpdateChartSub = AddressOf Me.PaintResultsCharts
    newChartForm.FormChart = Chart_Results
    newChartForm.ParentChartList = RiskReturnChartArrayList
    newChartForm.Text = "Chart Portfolio Risk vs Return"

    SyncLock RiskReturnChartArrayList
      RiskReturnChartArrayList.Add(newChartForm.DisplayedChart)
    End SyncLock

  End Sub

  Private Sub Chart_Results_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Chart_Results.VisibleChanged
    ' *******************************************************************************
    '
    '
    ' *******************************************************************************

    Try
      If (CType(sender, Dundas.Charting.WinControl.Chart).Visible) Then
        PaintResultsCharts(CType(sender, Dundas.Charting.WinControl.Chart))
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Results_Btn_PlotRiskReturn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Results_Btn_PlotRiskReturn.Click
    ' *****************************************************************************
    '
    '
    ' Series 0  : Portfolio Positions - Risk & Return
    ' Series 1  : Portfolio Risk/Return Walk
    ' Series 2+ : Optimisation lines.
    ' *****************************************************************************

    Dim thisChart As Dundas.Charting.WinControl.Chart

    SyncLock RiskReturnChartArrayList
      For Each thisChart In RiskReturnChartArrayList

        If (thisChart.Visible) Then
          Try

            PlotRiskReturn(thisChart)

          Catch ex As Exception
          End Try

        End If

      Next
    End SyncLock


  End Sub

  Private Sub Results_RefreshRiskReturnCharts()
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim thisChart As Dundas.Charting.WinControl.Chart

    SyncLock RiskReturnChartArrayList
      For Each thisChart In RiskReturnChartArrayList
        If (thisChart.Series(0).Points.Count > 0) Then
          PlotRiskReturn(thisChart)
        End If
      Next
    End SyncLock

  End Sub

  Private Sub Results_RefreshGroupReturnCharts()
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim thisChart As Dundas.Charting.WinControl.Chart

    SyncLock GroupChartArrayLists
      For Each thisChart In GroupChartArrayLists
        If (thisChart.Series(0).Points.Count > 0) Then
          PaintGroupReturnChart(GroupChartArrayLists)
        End If
      Next
    End SyncLock

  End Sub

  Private Sub Results_Btn_ClearResults_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Results_Btn_ClearResults.Click
    ' *****************************************************************************
    '
    ' *****************************************************************************

    ClearResultsCharts()

  End Sub

  Private Sub Results_Btn_CaptureNewHoldings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Results_Btn_CaptureNewHoldings.Click
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim TargetVolatility As Double
    Dim TargetReturn As Double
    Dim TargetStdDev As Double
    Dim TargetHolding As Double
    Dim TargetWeightings() As Double
    Dim InstrumentIDs() As Integer
    Dim NewHolding As Double

    Try
      TargetVolatility = Results_Text_TargetVolatility.Value
      TargetReturn = Results_Text_TargetReturn.Value
      TargetStdDev = TargetVolatility / MainForm.StatFunctions.Sqrt12(DefaultStatsDatePeriod)
      TargetHolding = Results_Text_TargetHolding.Value

      If (TargetHolding = 0) Then
        Try
          TargetHolding = Double.Parse(Label_NewHoldingValue.Text)
        Catch ex As Exception
        End Try
      End If
      If (TargetHolding = 0) Then
        TargetHolding = 1000000
      End If

      TargetWeightings = thisOptimiser.ExtractPortfolio(Results_Radio_TargetVolatility.Checked, TargetStdDev, TargetReturn)
      InstrumentIDs = thisOptimiser.InstrumentIDs

      Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
      Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
      Dim Col_GroupTrade As Integer = Grid_Returns.Cols("GroupTrade").SafeIndex
      Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
      Dim ThisInstrumentID As Integer
      Dim ThisInstrumentsRow As C1.Win.C1FlexGrid.Row
      Dim InstrumentRowCounter As Integer
      Dim HoldingCounter As Integer

      InPaint = True

      ' Set New Holdings 

      For InstrumentRowCounter = 1 To (Grid_Returns.Rows.Count - 1)
        ThisInstrumentsRow = Grid_Returns.Rows(InstrumentRowCounter)
        NewHolding = 0

        Try

          If (Not ThisInstrumentsRow.IsNew) Then
            ThisInstrumentID = Nz(ThisInstrumentsRow(Col_GroupPertracCode), 0)

            ' Find new holding (Defaults to Zero)

            For HoldingCounter = 1 To (InstrumentIDs.Length - 1)
              If (InstrumentIDs(HoldingCounter) = ThisInstrumentID) Then
                NewHolding = TargetWeightings(HoldingCounter) * TargetHolding

                Exit For
              End If
            Next

            ' Set New Holding 

            ThisInstrumentsRow(Col_GroupNewHolding) = NewHolding
            ThisInstrumentsRow(Col_GroupTrade) = NewHolding - CDbl(ThisInstrumentsRow(Col_GroupHolding))

          End If

        Catch ex As Exception
        End Try
      Next

    Catch ex As Exception
    Finally
      InPaint = False

      Call Update_Group_Percentages(True)
      Call Update_Group_RiskAndReturn(True)
      Call UpdateMarginalsGrid()
      Call CalculateConstraintExposures()

    End Try



  End Sub

  Private Sub Results_Btn_RoundTrades_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Results_Btn_RoundTrades.Click
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      Dim Col_GroupHolding As Integer = Grid_Returns.Cols("GroupHolding").SafeIndex
      Dim Col_GroupTrade As Integer = Grid_Returns.Cols("GroupTrade").SafeIndex
      Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
      Dim ThisInstrumentsRow As C1.Win.C1FlexGrid.Row
      Dim InstrumentRowCounter As Integer
      Dim TradeSize As Double

      TradeSize = Results_Text_MinTradeSize.Value
      If TradeSize <= 0 Then
        Exit Sub
      End If

      Suppress_RiskAndReturn = True

      For InstrumentRowCounter = 1 To (Grid_Returns.Rows.Count - 1)
        ThisInstrumentsRow = Grid_Returns.Rows(InstrumentRowCounter)

        ThisInstrumentsRow(Col_GroupTrade) = Math.Round(CDbl(ThisInstrumentsRow(Col_GroupTrade)) / TradeSize, 0) * TradeSize

      Next

    Catch ex As Exception
    Finally
      Suppress_RiskAndReturn = False

      Call Update_Group_Percentages(True)
      Call Update_Group_RiskAndReturn(True)
      Call UpdateMarginalsGrid()
      Call CalculateConstraintExposures()

    End Try


  End Sub

  Private Sub Results_Radio_TargetVolatility_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Results_Radio_TargetVolatility.CheckedChanged
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If (Results_Radio_TargetVolatility.Checked) Then
        Results_Radio_TargetReturn.Checked = False

        Results_Text_TargetVolatility.Enabled = True
        Results_Text_TargetReturn.Enabled = False

      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Results_Radio_TargetReturn_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Results_Radio_TargetReturn.CheckedChanged
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      If (Results_Radio_TargetReturn.Checked) Then
        Results_Radio_TargetVolatility.Checked = False

        Results_Text_TargetVolatility.Enabled = False
        Results_Text_TargetReturn.Enabled = True

      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub ClearResultsCharts()
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Dim X_Axis_Multiplier As Double = GetResultsMultiplier()

    Try
      If (OptimisationArrayLists IsNot Nothing) Then
        OptimisationArrayLists.Clear()
      End If

      If (RiskReturnChartPoints IsNot Nothing) Then
        RiskReturnChartPoints.Clear()

        Try
          If (Label_NewHoldingReturn.Text.Length > 0) AndAlso (Label_NewHoldingRisk.Text.Length > 0) Then
            ' Please note, this point must be added twice, I suppose that a line chart with only one point is a singularity and so does not appear?

            RiskReturnChartPoints.Add(New DecimalPoint(X_Axis_Multiplier * CDbl(ConvertValue(Label_NewHoldingRisk.Text, GetType(Double))), CDbl(ConvertValue(Label_NewHoldingReturn.Text, GetType(Double)))))
            RiskReturnChartPoints.Add(New DecimalPoint(X_Axis_Multiplier * CDbl(ConvertValue(Label_NewHoldingRisk.Text, GetType(Double))), CDbl(ConvertValue(Label_NewHoldingReturn.Text, GetType(Double)))))
          End If
        Catch ex As Exception
        End Try
      End If

      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock RiskReturnChartArrayList
        For Each thisChart In RiskReturnChartArrayList
          thisChart.Series(0).Points.Clear()
        Next
      End SyncLock

      SyncLock GroupChartArrayLists
        For Each thisChart In GroupChartArrayLists
          thisChart.Series(0).Points.Clear()
        Next
      End SyncLock

      PaintResultsCharts()

    Catch ex As Exception
    End Try

  End Sub

  Private Sub ClearOptimisationChartLines()
    ' *****************************************************************************
    '
    ' *****************************************************************************
    Dim SeriesCounter As Integer

    Try

      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock RiskReturnChartArrayList
        For Each thisChart In RiskReturnChartArrayList

          Try

            For SeriesCounter = 2 To (1 + OptimisationArrayLists.Count)
              If (thisChart.Series.Count > SeriesCounter) Then
                thisChart.Series(SeriesCounter).Points.Clear()
              End If
            Next

          Catch ex As Exception
          End Try

        Next
      End SyncLock

    Catch ex As Exception
    End Try

  End Sub

  Private Sub PlotRiskReturn(ByRef ThisChartOnly As Dundas.Charting.WinControl.Chart)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************
    Dim X_Axis_Multiplier As Double = GetResultsMultiplier()

    Try

      Dim PertracID As Integer
      Dim RowCount As Integer
      Dim ThisSeries As Dundas.Charting.WinControl.Series = ThisChartOnly.Series(0)
      Dim thisGridRow As C1.Win.C1FlexGrid.Row

      Dim Col_GroupExpectedReturn As Integer = Grid_Returns.Cols("GroupExpectedReturn").SafeIndex
      Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
      Dim Col_PertracName As Integer = Grid_Returns.Cols("PertracName").SafeIndex

      Dim CovarianceStartDate As Date
      Dim CovarianceEndDate As Date

      Dim ExpectedReturn As Double
      Dim StdDev As Double
      Dim Sqrt12 As Double = MainForm.StatFunctions.Sqrt12(DefaultStatsDatePeriod)

      ThisSeries.Points.Clear()

      CovarianceStartDate = CDate(Label_MatrixDateFrom.Text)
      CovarianceEndDate = CDate(Label_MatrixDateTo.Text)

      Dim PeriodCount As Integer = 12
      Try
        PeriodCount = GetPeriodCount(DefaultStatsDatePeriod, CovarianceStartDate, CovarianceEndDate)
      Catch ex As Exception
      End Try

      ' Get Pertrac Hash for the Covariance Grid.

      Dim CovariancePertracHash As Dictionary(Of Integer, Integer) = Nothing
      If (TypeOf Grid_Covariance.Tag Is Dictionary(Of Integer, Integer)) AndAlso (CType(Grid_Covariance.Tag, Dictionary(Of Integer, Integer)).Count = (Grid_Covariance.Rows.Count - 1)) Then

        CovariancePertracHash = CType(Grid_Covariance.Tag, Dictionary(Of Integer, Integer))

      Else
        If (CovariancePertracHash IsNot Nothing) Then
          CovariancePertracHash.Clear()
        Else
          CovariancePertracHash = New Dictionary(Of Integer, Integer)
        End If

        For RowCount = 1 To (Grid_Covariance.Rows.Count - 1)
          If IsNumeric(Grid_Covariance.Item(RowCount, Grid_Covariance.Cols.Count - 1)) Then
            Try
              CovariancePertracHash.Add(CInt(Grid_Covariance.Item(RowCount, Grid_Covariance.Cols.Count - 1)), RowCount)
            Catch ex As Exception
            End Try
          End If
        Next

        Grid_Covariance.Tag = CovariancePertracHash

      End If

      ' populate grid

      For RowCount = 1 To (Grid_Returns.Rows.Count - 1)

        thisGridRow = Grid_Returns.Rows(RowCount)

        PertracID = CInt(thisGridRow(Col_GroupPertracCode))

        Try
          ExpectedReturn = CDbl(thisGridRow(Col_GroupExpectedReturn))
        Catch ex As Exception
          ExpectedReturn = 0
        End Try

        ' Get StdDev off the Covariance Grid.
        StdDev = 0
        Try
          Dim CovRowIndex As Integer

          If CovariancePertracHash.ContainsKey(PertracID) Then
            CovRowIndex = CovariancePertracHash(PertracID)

            If IsNumeric(Me.Grid_Covariance.Item(CovRowIndex, CovRowIndex)) Then
              StdDev = Math.Sqrt(CDbl(Grid_Covariance.Item(CovRowIndex, CovRowIndex)))
            End If
          End If

        Catch ex As Exception
        End Try

        ' ThisSeries.Points.AddXY(InstrumentStats.Volatility.M36 * 100.0#, InstrumentStats.AnnualisedReturn.M36 * 100.0#)
        ThisSeries.Points.AddXY(X_Axis_Multiplier * StdDev * Sqrt12 * 100.0#, ExpectedReturn * 100.0#)
        ThisSeries.Points(ThisSeries.Points.Count - 1).ToolTip = CStr(thisGridRow(Col_PertracName))

      Next

      If (ThisSeries.Points.Count > 0) Then
        ThisSeries.ChartArea = ThisChartOnly.ChartAreas(0).Name
      Else
        ThisSeries.ChartArea = ""
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Results_Radio_Volatility_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Results_Radio_Volatility.CheckedChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try

      If (Not InPaint) Then
        If (Results_Radio_Volatility.Checked) Then
          Results_Text_ConfidenceLevel.Enabled = False
          Results_Text_ConfidencePeriods.Enabled = False
          Results_Check_ShowVsPortfolio.Enabled = False
          Results_Check_ShowVsPortfolio.Enabled = False
          Panel_PortfolioValues.Enabled = False

          ClearOptimisationChartLines()
          PaintResultsCharts()
          Results_RefreshRiskReturnCharts()
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Results_Radio_VAR_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Results_Radio_VAR.CheckedChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try

      If (Not InPaint) Then
        If (Results_Radio_VAR.Checked) Then
          Results_Text_ConfidenceLevel.Enabled = True
          Results_Text_ConfidencePeriods.Enabled = True
          Results_Check_ShowVsPortfolio.Enabled = True
          Results_Check_ShowVsPortfolio.Enabled = True

          If (Results_Check_ShowVsPortfolio.Checked) Then
            Panel_PortfolioValues.Enabled = True
          Else
            Panel_PortfolioValues.Enabled = False
          End If

          ClearOptimisationChartLines()
          PaintResultsCharts()
          Results_RefreshRiskReturnCharts()
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Results_Check_ShowVsPortfolio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Results_Check_ShowVsPortfolio.CheckedChanged
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      If (Not InPaint) Then

        If (Results_Check_ShowVsPortfolio.Checked) Then
          Panel_PortfolioValues.Enabled = True
        Else
          Panel_PortfolioValues.Enabled = False
        End If

        ClearOptimisationChartLines()
        PaintResultsCharts()
        Results_RefreshRiskReturnCharts()

      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Results_Radio_vsNewHolding_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Results_Radio_vsNewHolding.CheckedChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try

      If (Not InPaint) Then
        If (Results_Radio_vsNewHolding.Checked) Then
          Results_Text_PortfolioValue.Enabled = False

          ClearOptimisationChartLines()
          PaintResultsCharts()
          Results_RefreshRiskReturnCharts()
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Results_Radio_vsPortfolioValue_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Results_Radio_vsPortfolioValue.CheckedChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try

      If (Not InPaint) Then
        If (Results_Radio_vsPortfolioValue.Checked) Then
          ClearOptimisationChartLines()
          PaintResultsCharts()
          Results_RefreshRiskReturnCharts()

          Results_Text_PortfolioValue.Enabled = True
        Else
          Results_Text_PortfolioValue.Enabled = False
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Results_Text_PortfolioValue_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Results_Text_PortfolioValue.ValueChanged
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      If (Not InPaint) Then

        ClearOptimisationChartLines()
        PaintResultsCharts()
        Results_RefreshRiskReturnCharts()

      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Results_Text_ConfidenceLevel_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Results_Text_ConfidenceLevel.ValueChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try

      If (Not InPaint) Then

        ClearOptimisationChartLines()
        PaintResultsCharts()
        Results_RefreshRiskReturnCharts()

      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Results_Text_ConfidencePeriods_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Results_Text_ConfidencePeriods.ValueChanged
    ' *****************************************************************************
    '
    ' *****************************************************************************

    Try

      If (Not InPaint) Then

        ClearOptimisationChartLines()
        PaintResultsCharts()
        Results_RefreshRiskReturnCharts()

      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub PaintResultsCharts(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    ' Series 0  : Portfolio Positions - Risk & Return
    ' Series 1  : Portfolio Risk/Return Walk
    ' Series 2+ : Optimisation lines.
    ' *****************************************************************************

    Dim OptimisationChartPoints As ArrayList
    Dim OptimisationCounter As Integer
    Dim X_Axis_Multiplier As Double = GetResultsMultiplier()

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        Try
          ' OptimisationArrayLists

          While ThisChartOnly.Series.Count < (OptimisationArrayLists.Count + 2)
            ThisChartOnly.Series.Add("")
          End While
          While ThisChartOnly.Series.Count > (OptimisationArrayLists.Count + 2)
            ThisChartOnly.Series.RemoveAt(ThisChartOnly.Series.Count - 1)
          End While

          If (Results_Radio_VAR.Checked) Then
            If (Results_Check_ShowVsPortfolio.Checked) Then
              ThisChartOnly.ChartAreas(0).AxisX.Title = "VAR Loss (Cash)"
              ThisChartOnly.ChartAreas(0).AxisX.LabelStyle.Format = "#,##0"
            Else
              ThisChartOnly.ChartAreas(0).AxisX.Title = "VAR Loss (%)"
              ThisChartOnly.ChartAreas(0).AxisX.LabelStyle.Format = "#,##0%"
            End If
          Else
            ThisChartOnly.ChartAreas(0).AxisX.Title = "Volatility"
          End If

          OptimisationCounter = 2
          For Each OptimisationChartPoints In OptimisationArrayLists

            If (ThisChartOnly.Series(OptimisationCounter).Points.Count <= 0) AndAlso (OptimisationChartPoints.Count > 0) Then
              LoadJoinedPointSeries(ThisChartOnly.Series(OptimisationCounter), OptimisationChartPoints, X_Axis_Multiplier, True, True)
            ElseIf (OptimisationChartPoints.Count = 0) AndAlso (ThisChartOnly.Series(OptimisationCounter).Points.Count > 0) Then
              ThisChartOnly.Series(OptimisationCounter).Points.Clear()
            End If

            OptimisationCounter += 1
          Next

          ' 

          LoadJoinedPointSeries(ThisChartOnly.Series(1), RiskReturnChartPoints, X_Axis_Multiplier, True, True)

          If (ThisChartOnly.Series(1).Points.Count > 0) Then
            ThisChartOnly.Series(1).ChartArea = ThisChartOnly.ChartAreas(0).Name
            ThisChartOnly.Series(1).Points(ThisChartOnly.Series(1).Points.Count - 1).Label = "Portfolio"
          Else
            ThisChartOnly.Series(1).ChartArea = ""
          End If

        Catch ex As Exception
        End Try

      End If

    Else
      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock RiskReturnChartArrayList
        For Each thisChart In RiskReturnChartArrayList

          If (thisChart.Visible) Then
            Try

              PaintResultsCharts(thisChart)

            Catch ex As Exception
            End Try

          End If

        Next
      End SyncLock

    End If

  End Sub

  Private Sub PaintGroupReturnChart(Optional ByVal pThisChartOnly As Object = Nothing, Optional ByVal ParameterList() As String = Nothing)
    ' *****************************************************************************
    '
    ' Series 0  : Portfolio Positions - Risk & Return
    ' Series 1  : Portfolio Risk/Return Walk
    ' Series 2+ : Optimisation lines.
    ' *****************************************************************************

    If (pThisChartOnly IsNot Nothing) Then

      If (TypeOf pThisChartOnly Is Dundas.Charting.WinControl.Chart) Then

        Dim ThisChartOnly As Dundas.Charting.WinControl.Chart = CType(pThisChartOnly, Dundas.Charting.WinControl.Chart)

        Try

          Set_LineChart(MainForm, DefaultStatsDatePeriod, RenaissancePertracDataClass.PertracDataClass.SetFlagsToID(NewHolding_DynamicGroupID, RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Dynamic_Group_Weighted), pThisChartOnly, 0, "", MainForm.StatFunctions.AnnualPeriodCount(DefaultStatsDatePeriod), 1.0#, 1.0#, Now.AddMonths(-60), Now())

          If (Radio_ImproveLeastSquares.Checked OrElse Radio_ImproveStdError.Checked) AndAlso (Combo_MarginalIndex.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_MarginalIndex.SelectedValue)) Then
            Set_LineChart(MainForm, DefaultStatsDatePeriod, CInt(Combo_MarginalIndex.SelectedValue), pThisChartOnly, 1, "", MainForm.StatFunctions.AnnualPeriodCount(DefaultStatsDatePeriod), 1.0#, 1.0#, Now.AddMonths(-60), Now())
          Else
            If (ThisChartOnly.Series.Count > 1) Then
              ThisChartOnly.Series(1).ChartArea = ""
            End If
          End If

        Catch ex As Exception
        End Try

      End If

    Else
      Dim thisChart As Dundas.Charting.WinControl.Chart

      SyncLock GroupChartArrayLists
        For Each thisChart In GroupChartArrayLists

          If (thisChart.Visible) Then
            Try

              PaintGroupReturnChart(thisChart)

            Catch ex As Exception
            End Try

          End If

        Next
      End SyncLock

    End If

  End Sub

  Private Sub FormatPointChart(ByRef ThisChart As Dundas.Charting.WinControl.Chart)
    ' *****************************************************************************
    ' Enable range selection and zooming end user interface
    ' *****************************************************************************

    ThisChart.ChartAreas(0).CursorX.UserEnabled = True
    ThisChart.ChartAreas(0).CursorX.UserSelection = True
    ThisChart.ChartAreas(0).CursorY.UserEnabled = True
    ThisChart.ChartAreas(0).CursorY.UserSelection = True
    ThisChart.ChartAreas(0).AxisX.View.Zoomable = True
    ThisChart.ChartAreas(0).AxisY.View.Zoomable = True
    ThisChart.ChartAreas(0).AxisX.LabelStyle.Font = New Font("Arial", 8)
    ThisChart.ChartAreas(0).AxisY.LabelStyle.Font = New Font("Arial", 8)
    ThisChart.ChartAreas(0).AxisX.StartFromZero = True
    ThisChart.ChartAreas(0).AxisX.Minimum = 0
    ThisChart.ChartAreas(0).AxisX.Interval = 0
    ThisChart.ChartAreas(0).AxisX.LabelStyle.Format = "P0"
    ThisChart.ChartAreas(0).AxisY.LabelStyle.Format = "P0"

  End Sub

  Private Sub LoadJoinedPointSeries(ByRef ThisSeries As Dundas.Charting.WinControl.Series, ByRef ChartPoints As ArrayList, ByVal X_Axis_Multiplier As Double, ByVal Percent As Boolean, ByVal Marker As Boolean)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim PointCounter As Integer
    Dim ThisPoint As DecimalPoint
    Dim Multiplier As Double

    Try
      If Percent Then
        Multiplier = 100.0#
      Else
        Multiplier = 1.0#
      End If

      ThisSeries.Points.Clear()

      For PointCounter = 0 To (ChartPoints.Count - 1)
        ThisPoint = CType(ChartPoints(PointCounter), DecimalPoint)

        ThisSeries.Points.AddXY(X_Axis_Multiplier * ThisPoint.X * Multiplier, ThisPoint.Y * Multiplier)
        ThisSeries.Points(ThisSeries.Points.Count - 1).ToolTip = (PointCounter + 1).ToString

      Next PointCounter

      FormatSeriesLine(ThisSeries, Marker)

    Catch
    End Try

  End Sub

  Private Sub FormatSeriesLine(ByRef ThisSeries As Dundas.Charting.WinControl.Series, ByVal Marker As Boolean)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try

      ThisSeries.ChartType = "Line"
      ThisSeries.ShadowOffset = 1

      If Marker Then
        ThisSeries.MarkerStyle = Dundas.Charting.WinControl.MarkerStyle.Diamond
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Function GetResultsMultiplier() As Double
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Dim RVal As Double = 1.0#
    Dim VARMultiplier As Double
    Dim VarZScore As Double

    Try

      If (Me.Results_Radio_VAR.Checked) Then

        VarZScore = NORMINV(0.5# + (Results_Text_ConfidenceLevel.Value / 2.0#), 0, 1)
        VARMultiplier = VarZScore * Math.Sqrt(CDbl(Results_Text_ConfidencePeriods.Value)) / MainForm.StatFunctions.Sqrt12(DefaultStatsDatePeriod)

        RVal = VARMultiplier

        If (Results_Check_ShowVsPortfolio.Checked) Then
          If (Me.Results_Radio_vsNewHolding.Checked) Then
            If (IsNumeric(Label_NewHoldingValue.Text)) AndAlso (CDbl(Label_NewHoldingValue.Text) <> 0) Then
              RVal *= (Math.Abs(CDbl(Label_NewHoldingValue.Text)) / 100.0#)
            End If
          Else
            If (Results_Text_PortfolioValue.Value <> 0) Then
              RVal *= (Math.Abs(Results_Text_PortfolioValue.Value) / 100.0#)
            End If
          End If
        End If
      End If

    Catch ex As Exception
    End Try

    Return RVal

  End Function

  ' ---------------------------------------------------------------------------------
  ' Tab Covariance
  ' ---------------------------------------------------------------------------------

  Private Sub InitialiseCovarianceGrid()

    Dim ThisStyle As C1.Win.C1FlexGrid.CellStyle

    Try

      ThisStyle = Me.Grid_Covariance.Styles("Positive")
      If (ThisStyle Is Nothing) Then
        ThisStyle = Me.Grid_Covariance.Styles.Add("Positive")
      End If
      ThisStyle.ForeColor = Color.Blue
      ThisStyle.Format = "#,##0.0000"

      ThisStyle = Me.Grid_Covariance.Styles("Negative")
      If (ThisStyle Is Nothing) Then
        ThisStyle = Me.Grid_Covariance.Styles.Add("Negative")
      End If
      ThisStyle.ForeColor = Color.Red
      ThisStyle.Format = "#,##0.0000"

      ThisStyle = Me.Grid_Covariance.Styles("Black")
      If (ThisStyle Is Nothing) Then
        ThisStyle = Me.Grid_Covariance.Styles.Add("Black")
      End If
      ThisStyle.ForeColor = Color.Blue
      ThisStyle.Format = "#,##0.0000"
      ThisStyle.Font = New Font(ThisStyle.Font, FontStyle.Bold)

    Catch ex As Exception

    End Try

  End Sub

  Private Sub Radio_Covariance_UseLiveMatrix_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Covariance_UseLiveMatrix.CheckedChanged
    ' ***************************************************************************
    '
    '
    ' ***************************************************************************

    If (Radio_Covariance_UseLiveMatrix.Checked) Then
      Radio_Covariance_UseSavedMatrix.Checked = False
      Radio_Covariance_EditCovMatrix.Checked = False
      Check_Covariance_BackfillData.Enabled = True
      Button_SaveCovMatrix.Enabled = False
      Button_SaveAsCovMatrix.Enabled = False

      List_CovarianceMatrices.Enabled = False
      List_CovarianceMatrices.SelectedItems.Clear()

      List_CovarianceMatrices.SelectedIndex = (-1)
      If (List_CovarianceMatrices.SelectedIndex >= 0) Then
        List_CovarianceMatrices.SelectedIndex = (-1)
      End If

      Grid_Covariance.AllowEditing = False

      CalculateLiveCovarianceMatrix(DefaultStatsDatePeriod)
    End If

  End Sub

  Private Sub Check_Covariance_BackfillData_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Covariance_BackfillData.CheckedChanged
    ' ***************************************************************************
    '
    '
    ' ***************************************************************************

    If (Check_Covariance_BackfillData.Enabled) AndAlso (Check_Covariance_BackfillData.Focused) AndAlso (Radio_Covariance_UseLiveMatrix.Checked) Then
      Radio_Covariance_UseSavedMatrix.Checked = False
      Radio_Covariance_EditCovMatrix.Checked = False
      Button_SaveCovMatrix.Enabled = False
      Button_SaveAsCovMatrix.Enabled = False

      List_CovarianceMatrices.Enabled = False
      List_CovarianceMatrices.SelectedItems.Clear()

      List_CovarianceMatrices.SelectedIndex = (-1)
      If (List_CovarianceMatrices.SelectedIndex >= 0) Then
        List_CovarianceMatrices.SelectedIndex = (-1)
      End If

      Grid_Covariance.AllowEditing = False

      CalculateLiveCovarianceMatrix(DefaultStatsDatePeriod)
    End If

  End Sub

  Private Sub Radio_Covariance_UseSavedMatrix_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Covariance_UseSavedMatrix.CheckedChanged
    ' ***************************************************************************
    '
    '
    ' ***************************************************************************

    If (Radio_Covariance_UseSavedMatrix.Checked) Then
      Radio_Covariance_UseLiveMatrix.Checked = False
      Radio_Covariance_EditCovMatrix.Checked = False
      Check_Covariance_BackfillData.Enabled = False
      Button_SaveCovMatrix.Enabled = False
      Button_SaveAsCovMatrix.Enabled = False

      List_CovarianceMatrices.Enabled = True
      List_CovarianceMatrices.SelectedItems.Clear()
      Grid_Covariance.Rows.Count = 1
      Grid_Covariance.Cols.Count = 1

      Grid_Covariance.AllowEditing = False

      Call List_CovarianceMatrices_SelectedIndexChanged(List_CovarianceMatrices, New EventArgs)

    End If

  End Sub

  Private Sub Radio_Covariance_EditCovMatrix_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Covariance_EditCovMatrix.CheckedChanged
    ' ***************************************************************************
    '
    '
    ' ***************************************************************************

    If (Radio_Covariance_EditCovMatrix.Checked) Then
      Radio_Covariance_UseLiveMatrix.Checked = False
      Radio_Covariance_UseSavedMatrix.Checked = False
      Check_Covariance_BackfillData.Enabled = False
      Button_SaveCovMatrix.Enabled = False
      Button_SaveAsCovMatrix.Enabled = False

      List_CovarianceMatrices.Enabled = False
      ' List_CovarianceMatrices.SelectedItems.Clear()

      Grid_Covariance.AllowEditing = True

      Dim ColCount As Integer

      For ColCount = 1 To (Grid_Covariance.Cols.Count - 1)
        If (Grid_Covariance.Cols(ColCount).Visible) Then
          Grid_Covariance.Cols(ColCount).AllowEditing = True
        End If
      Next
    End If

  End Sub

  Private Sub List_CovarianceMatrices_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles List_CovarianceMatrices.SelectedIndexChanged
    ' ***************************************************************************
    '
    '
    ' ***************************************************************************

    If (Not Radio_Covariance_UseSavedMatrix.Checked) Then
      Exit Sub
    End If

    ' Is a Covariance List selected to display ?

    If (List_CovarianceMatrices.SelectedIndex < 0) OrElse (IsNumeric(List_CovarianceMatrices.SelectedValue) = False) OrElse (CInt(List_CovarianceMatrices.SelectedValue) <= 0) Then
      Grid_Covariance.Rows.Count = 1
      Grid_Covariance.Cols.Count = 1

      Me.Label_MatrixName.Text = ""
      Me.Label_MatrixDateFrom.Text = ""
      Me.Label_MatrixDateTo.Text = ""
      Me.Label_MatrixLamda.Text = ""
      Me.Label_MatrixStatus.Text = ""
    Else ' 
      Dim CurrentGroupID As Integer = (-1)

      If (Combo_SelectGroupID.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_SelectGroupID.SelectedValue)) AndAlso (CInt(Combo_SelectGroupID.SelectedValue) > 0) Then
        CurrentGroupID = CInt(Combo_SelectGroupID.SelectedValue)
      End If

      LoadCovarianceMatrix(DefaultStatsDatePeriod, CurrentGroupID, CInt(List_CovarianceMatrices.SelectedValue))
    End If

  End Sub

  Private Sub Set_ListCovarianceMatrix()
    ' ***************************************************************************************************
    ' Build the List of Saved Covariance matrices.
    '
    ' ***************************************************************************************************

    Dim TmpCombo As New ComboBox

    Call MainForm.SetTblGenericCombo( _
    TmpCombo, _
    RenaissanceStandardDatasets.tblCovarianceList, _
    "ListName", _
    "CovListID", _
    "", True, True, False)    ' 

    Try
      Dim ExistingSelectedValue As Integer = (-1)
      If (List_CovarianceMatrices.SelectedIndex >= 0) AndAlso (Radio_Covariance_UseSavedMatrix.Checked) Then
        ExistingSelectedValue = CInt(List_CovarianceMatrices.SelectedValue)
      End If

      List_CovarianceMatrices.DataSource = TmpCombo.DataSource
      List_CovarianceMatrices.DisplayMember = TmpCombo.DisplayMember
      List_CovarianceMatrices.ValueMember = TmpCombo.ValueMember

      If (ExistingSelectedValue >= 0) Then
        List_CovarianceMatrices.SelectedValue = ExistingSelectedValue
      End If

    Catch ex As Exception
    End Try

    TmpCombo = Nothing

  End Sub

  Private Sub LoadCovarianceMatrix(ByVal StatsDatePeriod As DealingPeriod, ByVal pCurrentGroupID As Integer, ByVal pCovarianceListID As Integer)
    ' ***************************************************************************************************
    ' Load a previously saved Covariance matrix, given the 'CovListID'.
    '
    ' ***************************************************************************************************

    Dim ThisCovListRow As DSCovarianceList.tblCovarianceListRow
    Dim OrgInpaint As Boolean

    ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
    OrgInpaint = InPaint

    Dim tmpCommand As New SqlCommand
    Dim ListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable = Nothing


    Try
      InPaint = True

      ThisCovListRow = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblCovarianceList, pCovarianceListID)
      If (ThisCovListRow Is Nothing) Then
        Me.Grid_Covariance.Rows.Count = 1
        Me.Grid_Covariance.Cols.Count = 1

        Exit Sub
      End If

      Label_MatrixName.Text = ThisCovListRow.ListName
      Label_MatrixDateFrom.Text = ThisCovListRow.DateFrom.ToString(DISPLAYMEMBER_DATEFORMAT)
      Label_MatrixDateTo.Text = ThisCovListRow.DateTo.ToString(DISPLAYMEMBER_DATEFORMAT)
      Label_MatrixLamda.Text = ThisCovListRow.Lamda.ToString
      Label_MatrixStatus.Text = "<Loading>"

      ' Get Covariance Data

      tmpCommand.CommandType = CommandType.StoredProcedure
      tmpCommand.CommandText = "adp_tblGroupItems_SelectGroupPertracCodes"
      tmpCommand.Connection = MainForm.GetGenoaConnection
      If (pCurrentGroupID >= 0) Then
        ' Load for stocks in the currently selected Optimisation group
        tmpCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = pCurrentGroupID
      Else
        ' Load for stocks in the Save covariance List
        tmpCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = ThisCovListRow.GroupListID
      End If
      tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
      tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      ListTable = New RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable
      ListTable.Columns.Add("ListDescription", GetType(String))
      ListTable.Columns.Add("PertracName", GetType(String))
      ListTable.Columns.Add("PertracProvider", GetType(String))
      ListTable.Columns.Add("IndexName", GetType(String))
      ListTable.Columns.Add("IndexProvider", GetType(String))

      ListTable.Load(tmpCommand.ExecuteReader)

      If (ListTable IsNot Nothing) Then
        Dim MissingInstruments As Integer

        MissingInstruments = GetFullCovarianceMatrix(MainForm, StatsDatePeriod, Grid_Covariance, ListTable, ScalingFactorHash, False, True, 1.0, ThisCovListRow.DateFrom, ThisCovListRow.DateTo, True, True, pCovarianceListID, StatusLabel_Optimisation)

        If (MissingInstruments > 0) Then
          Label_MatrixStatus.Text = MissingInstruments.ToString & " Missing Instruments"
        Else
          Label_MatrixStatus.Text = "OK"
        End If
      Else
        Me.Grid_Covariance.Rows.Count = 1
        Me.Grid_Covariance.Cols.Count = 1
      End If

      ' Allow Field events to trigger before 'InPaint' Is re-set. 
      ' (Should) Prevent Validation errors during Form Draw.
      Application.DoEvents()

      InPaint = OrgInpaint

    Catch ex As Exception
      ListTable = Nothing

    Finally
      Try
        If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
          tmpCommand.Connection.Close()
          tmpCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try

      InPaint = OrgInpaint

      Update_Group_RiskAndReturn(True)

    End Try

  End Sub

  Private Sub Grid_Covariance_EnterCell(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Covariance.EnterCell
    ' ***************************************************************************************************
    '
    ' ***************************************************************************************************

    If Me.Radio_Covariance_EditCovMatrix.Checked Then
      If (Grid_Covariance.Cols(Grid_Covariance.Col).AllowEditing) Then
        Grid_Covariance.SetCellStyle(Grid_Covariance.Row, Grid_Covariance.Col, Grid_Returns.Styles("Genoa_PlainEdit"))
      End If
    End If

  End Sub

  Private Sub Grid_Covariance_AfterEdit(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Grid_Covariance.AfterEdit
    ' ***************************************************************************************************
    ' If a Covariance grid cell is manually edited, Set the Cell format appropriately and ensure that the 
    ' Mirror Covariance item is set and formatted the same.
    '
    ' ***************************************************************************************************

    ' If 'Manual Editing' is set...

    If (Radio_Covariance_EditCovMatrix.Checked) Then

      Dim EditedCellValue As Object

      ' Check the changed cell is an active cell.

      If (e.Row > 0) AndAlso (e.Col > 0) AndAlso (Me.Grid_Covariance.Cols(e.Col).Visible) Then
        EditedCellValue = Grid_Covariance.Item(e.Row, e.Col)

        ' Check the new value is numeric.

        If (IsNumeric(EditedCellValue)) Then

          ' Set Format

          If (CDbl(EditedCellValue) < 0) Then
            Grid_Covariance.SetCellStyle(e.Row, e.Col, Grid_Covariance.Styles("Negative"))
          Else
            Grid_Covariance.SetCellStyle(e.Row, e.Col, Grid_Covariance.Styles("Positive"))
          End If

          ' Check 'Mirror' Cell.

          If (e.Row <> e.Col) Then
            Dim OtherCellValue As Object

            OtherCellValue = Grid_Covariance.Item(e.Col, e.Row)

            If Not ((IsNumeric(OtherCellValue)) AndAlso (Math.Abs(CDbl(EditedCellValue) - CDbl(OtherCellValue)) < 0.0000000001)) Then
              Grid_Covariance.Item(e.Col, e.Row) = CDbl(EditedCellValue)

              If (CDbl(EditedCellValue) < 0) Then
                Grid_Covariance.SetCellStyle(e.Col, e.Row, Grid_Covariance.Styles("Negative"))
              Else
                Grid_Covariance.SetCellStyle(e.Col, e.Row, Grid_Covariance.Styles("Positive"))
              End If

            End If
          End If
        End If
      End If

      If (((List_CovarianceMatrices.SelectedIndex >= 0) AndAlso IsNumeric(List_CovarianceMatrices.SelectedValue)) AndAlso (CInt(List_CovarianceMatrices.SelectedValue) > 0)) Then
        Me.Button_SaveCovMatrix.Enabled = True
      End If
      Me.Button_SaveAsCovMatrix.Enabled = True

    End If

  End Sub

  Private Sub Grid_Covariance_LeaveCell(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Covariance.LeaveCell
    ' ***************************************************************************************************
    '
    ' ***************************************************************************************************

    If (Radio_Covariance_EditCovMatrix.Checked) Then

      Grid_Covariance_AfterEdit(sender, New C1.Win.C1FlexGrid.RowColEventArgs(Grid_Covariance.Row, Grid_Covariance.Col))

    End If

  End Sub

  Private Sub Grid_Covariance_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Covariance.KeyDown
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************
    Try
      If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False)
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Grid_Covariance_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Covariance.LostFocus
    ' ***************************************************************************************************
    '
    ' ***************************************************************************************************

    If (Radio_Covariance_EditCovMatrix.Checked) Then

      Grid_Covariance.Select(0, 0)

    End If

  End Sub

  Private Sub Button_SaveCovMatrix_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SaveCovMatrix.Click
    ' ***************************************************************************************************
    '
    ' ***************************************************************************************************

    Dim CovListID As Integer = 0

    Try

      If (List_CovarianceMatrices.SelectedIndex >= 0) AndAlso (IsNumeric(List_CovarianceMatrices.SelectedValue)) Then
        CovListID = CInt(List_CovarianceMatrices.SelectedValue)
      End If

      If SaveCovarianceGridData(CovListID, False) Then
        Me.Button_SaveAsCovMatrix.Enabled = False
        Me.Button_SaveCovMatrix.Enabled = False
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Button_SaveAsCovMatrix_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SaveAsCovMatrix.Click
    ' ***************************************************************************************************
    '
    ' ***************************************************************************************************

    Try

      If SaveCovarianceGridData(0, False) Then
        Me.Button_SaveAsCovMatrix.Enabled = False
        Me.Button_SaveCovMatrix.Enabled = False
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Function SaveCovarianceGridData(ByVal pCovListID As Integer, Optional ByVal pConfirm As Boolean = True) As Boolean
    ' *************************************************************
    '
    ' *************************************************************

    Dim ErrMessage As String
    Dim ErrFlag As Boolean
    Dim ErrStack As String
    Dim ProtectedItem As Boolean = False

    ErrMessage = ""
    ErrStack = ""

    ' *************************************************************
    ' Appropriate Save permission :-
    ' *************************************************************

    Try

      Dim Permissions As Integer

      Permissions = MainForm.CheckPermissions(GenoaFormID.frmGroupCovariance.ToString, RenaissanceGlobals.PermissionFeatureType.TypeForm)

      If ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) = 0) AndAlso ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) = 0) Then

        MainForm.LogError(Me.Name & ", SaveCovarianceGridData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
        Return False
        Exit Function

      End If

    Catch ex As Exception
    End Try

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    ' Confirm Save, if required.

    If (pConfirm = True) Then
      If MessageBox.Show("Save Covariance Matrix ?", "Save Matrix ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
        Return True
        Exit Function
      End If
    End If

    ' *************************************************************
    ' Procedure to Save the current form information.
    ' *************************************************************

    If (FormIsValid = False) Then
      Return False
      Exit Function
    End If


    ' *************************************************************
    '		
    ' *************************************************************

    Dim ThisGroupListID As Integer
    Dim ThisCovListID As Integer
    Dim ThisGroupStartDate As Date
    Dim ThisGroupEndDate As Date

    Try
      If (Combo_SelectGroupID.SelectedValue Is Nothing) OrElse (IsNumeric(Combo_SelectGroupID.SelectedValue) = False) Then
        MainForm.LogError(Me.Name & ", SaveCovarianceGridData()", LOG_LEVELS.Warning, "", "No Group appears to be selected, Saving as Group Zero.", "", True)
        ThisGroupListID = 0
      Else
        ThisGroupListID = CInt(Combo_SelectGroupID.SelectedValue)
      End If

      ThisCovListID = pCovListID

      If (IsDate(Label_MatrixDateFrom.Text) = False) Then
        ThisGroupStartDate = Renaissance_BaseDate
      Else
        ThisGroupStartDate = CDate(Label_MatrixDateFrom.Text)
      End If
      If (IsDate(Label_MatrixDateTo.Text) = False) Then
        ThisGroupEndDate = Renaissance_EndDate_Data
      Else
        ThisGroupEndDate = CDate(Label_MatrixDateTo.Text)
      End If

      ' Add / Edit CovarianceList

      Try
        Dim CovListDS As DSCovarianceList
        Dim CovListTbl As DSCovarianceList.tblCovarianceListDataTable
        Dim CovListRow As DSCovarianceList.tblCovarianceListRow
        Dim SelectedCovListRows() As DSCovarianceList.tblCovarianceListRow = Nothing

        CovListDS = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblCovarianceList, False)
        If (CovListDS IsNot Nothing) Then
          CovListTbl = CovListDS.tblCovarianceList

          SyncLock CovListTbl
            CovListRow = Nothing

            If (ThisCovListID > 0) Then
              ' Edit
              SelectedCovListRows = CovListTbl.Select("CovListID=" & ThisCovListID.ToString)

              If (SelectedCovListRows.Length > 0) Then
                CovListRow = SelectedCovListRows(0)
              End If
            End If

            If (CovListRow Is Nothing) OrElse (ThisCovListID <= 0) Then
              ' Add New
              CovListRow = CovListTbl.NewtblCovarianceListRow

              CovListRow.CovListID = 0
            End If

            If (SelectedCovListRows Is Nothing) OrElse (SelectedCovListRows.Length <= 0) Then
              CovListRow.ListName = InputBox("Please enter new Matrix Name", "New Covariance Matrix", "Cov Matrix " & Now.ToString(DISPLAYMEMBER_LONGDATEFORMAT))
            End If

            If (CovListRow.ListName.Length <= 0) Then
              CovListRow.ListName = "Cov Matrix " & Now.ToString(DISPLAYMEMBER_LONGDATEFORMAT)
            End If

            CovListRow.GroupListID = ThisGroupListID
            CovListRow.DateFrom = ThisGroupStartDate
            CovListRow.DateTo = ThisGroupEndDate
            If (IsNumeric(Label_MatrixLamda.Text)) Then
              CovListRow.Lamda = CInt(Label_MatrixLamda.Text)
            Else
              CovListRow.Lamda = 1
            End If
            CovListRow.IsAnnualised = 0
            CovListRow.DataPeriod = CInt(DefaultStatsDatePeriod)

            If (CovListRow.RowState And DataRowState.Detached) = DataRowState.Detached Then
              CovListTbl.Rows.Add(CovListRow)
            End If

            MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblCovarianceList, New DataRow() {CovListRow})

            ThisCovListID = CovListRow.CovListID
          End SyncLock

        Else
          MainForm.LogError(Me.Name & ", SaveCovarianceGridData()", LOG_LEVELS.Warning, "", "Failed to get tblCovarianceList (Mainform.LoadTable())", "", True)
          Return False
          Exit Function
        End If

      Catch ex As Exception
        MainForm.LogError(Me.Name & ", SaveCovarianceGridData()", LOG_LEVELS.Warning, ex.Message, "Failed to Update tblCovarianceList.", ex.StackTrace, True)
        Return False
        Exit Function
      End Try

      ' Add / Edit tblCovariance

      If (ThisCovListID <= 0) Then
        MainForm.LogError(Me.Name & ", SaveCovarianceGridData()", LOG_LEVELS.Warning, "", "Covariance ListID is Zero, Failed to update Covariance List ?. Save Aborted.", "", True)
        Return False
        Exit Function
      End If



      Try
        Dim CovDS As DSCovariance
        Dim CovTbl As DSCovariance.tblCovarianceDataTable
        Dim CovRow As DSCovariance.tblCovarianceRow
        Dim SelectedCovRows() As DSCovariance.tblCovarianceRow
        Dim ItemCounter As Integer
        Dim GridIDs() As Integer
        Dim RowIndex As Integer
        Dim ColIndex As Integer
        Dim PertracIDCol As Integer
        Dim PertracID1 As Integer
        Dim PertracID2 As Integer
        Dim ThisCovarianceValue As Object

        CovDS = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblCovariance, False)
        If (CovDS IsNot Nothing) Then
          ' Get Grid IDs
          ReDim GridIDs(Me.Grid_Covariance.Rows.Count - 1)

          CovTbl = CovDS.tblCovariance
          CovRow = Nothing

          SyncLock CovTbl
            SelectedCovRows = CovTbl.Select("CovListID=" & ThisCovListID.ToString, "PertracID1,PertracID2")

            For ItemCounter = 0 To (SelectedCovRows.Length - 1)
              SelectedCovRows(ItemCounter).PertracID1 = 0
              SelectedCovRows(ItemCounter).PertracID2 = 0
              SelectedCovRows(ItemCounter).Covariance = 0
              SelectedCovRows(ItemCounter).Correlation = 0
            Next

            ItemCounter = 0
            PertracIDCol = Grid_Covariance.Cols.Count - 1

            For RowIndex = 1 To (Grid_Covariance.Rows.Count - 1)
              If IsNumeric(Grid_Covariance.Item(RowIndex, PertracIDCol)) Then
                PertracID1 = CInt(Grid_Covariance.Item(RowIndex, PertracIDCol))

                For ColIndex = 1 To RowIndex
                  If IsNumeric(Grid_Covariance.Item(ColIndex, PertracIDCol)) Then

                    PertracID2 = CInt(Grid_Covariance.Item(ColIndex, PertracIDCol))

                    ThisCovarianceValue = Grid_Covariance.Item(RowIndex, ColIndex)

                    ' ThisComparison = MainForm.StatFunctions.GetComparisonStatsItem(Math.Min(PertracID1, PertracID2), Math.Max(PertracID1, PertracID2), Math.Min(PertracID1, PertracID2), ThisGroupStartDate, ThisGroupEndDate, Text_Lamda.Value, MainForm.StatFunctions.AnnualPeriodCount)

                    If (ThisCovarianceValue IsNot Nothing) AndAlso (IsNumeric(ThisCovarianceValue)) Then
                      ' Save Correlation
                      If (ItemCounter < SelectedCovRows.Length) Then
                        SelectedCovRows(ItemCounter).BeginEdit()

                        SelectedCovRows(ItemCounter).PertracID1 = Math.Min(PertracID1, PertracID2)
                        SelectedCovRows(ItemCounter).PertracID2 = Math.Max(PertracID1, PertracID2)
                        SelectedCovRows(ItemCounter).Covariance = CDbl(ThisCovarianceValue)
                        SelectedCovRows(ItemCounter).Correlation = 0

                        SelectedCovRows(ItemCounter).EndEdit()

                      Else
                        CovRow = CovTbl.NewtblCovarianceRow

                        CovRow.CovID = 0
                        CovRow.CovListID = ThisCovListID
                        CovRow.PertracID1 = Math.Min(PertracID1, PertracID2)
                        CovRow.PertracID2 = Math.Max(PertracID1, PertracID2)
                        CovRow.Covariance = CDbl(ThisCovarianceValue)
                        CovRow.Correlation = 0

                        CovTbl.Rows.Add(CovRow)
                      End If

                      ItemCounter += 1
                    End If
                  End If
                Next
              End If
            Next

            ' Delete any un-used rows.

            While ItemCounter < SelectedCovRows.Length
              SelectedCovRows(ItemCounter).Delete()
            End While

            ' Update Rows

            MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblCovariance, CovTbl)

          End SyncLock

        End If

      Catch ex As Exception
        MainForm.LogError(Me.Name & ", SaveCovarianceGridData()", LOG_LEVELS.Warning, ex.Message, "Failed to update tblCovariance.", ex.StackTrace, True)
        Return False
        Exit Function
      End Try

    Catch ex As Exception
      ErrMessage = ex.Message
      ErrFlag = True
      ErrStack = ex.StackTrace
    End Try

    ' *************************************************************
    ' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
    ' *************************************************************

    If (ErrFlag = True) Then
      Call MainForm.LogError(Me.Name & ", SaveCovarianceGridData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
    End If

    ' Finish off

    Me.THIS_FORM_SelectingCombo.Enabled = True

    ' Propagate changes

    Try
      Dim ThisEventArgs As New RenaissanceGlobals.RenaissanceUpdateEventArgs
      ThisEventArgs.TableChanged(RenaissanceChangeID.tblCovarianceList) = True
      ThisEventArgs.TableChanged(RenaissanceChangeID.tblCovariance) = True

      Call MainForm.Main_RaiseEvent(ThisEventArgs)
    Catch ex As Exception
    End Try

    Return True

  End Function


  ' ---------------------------------------------------------------------------------
  ' Tab Constraints
  ' ---------------------------------------------------------------------------------



  ' ---------------------------------------------------------------------------------
  ' Tab Optimisation Input / Output
  ' ---------------------------------------------------------------------------------

  Private Sub InitialiseOptimisationReturnsGrid()

    Dim ThisStyle As C1.Win.C1FlexGrid.CellStyle

    Try

      ThisStyle = Me.Grid_OptimisationResults.Styles("Positive")
      If (ThisStyle Is Nothing) Then
        ThisStyle = Me.Grid_OptimisationResults.Styles.Add("Positive")
      End If
      ThisStyle.ForeColor = Color.Blue
      ThisStyle.Format = "#,##0.0000"

      ThisStyle = Me.Grid_OptimisationResults.Styles("Negative")
      If (ThisStyle Is Nothing) Then
        ThisStyle = Me.Grid_OptimisationResults.Styles.Add("Negative")
      End If
      ThisStyle.ForeColor = Color.Red
      ThisStyle.Format = "#,##0.0000"

      ThisStyle = Me.Grid_OptimisationResults.Styles("Black")
      If (ThisStyle Is Nothing) Then
        ThisStyle = Me.Grid_OptimisationResults.Styles.Add("Black")
      End If
      ThisStyle.ForeColor = Color.Blue
      ThisStyle.Format = "#,##0.0000"
      ThisStyle.Font = New Font(ThisStyle.Font, FontStyle.Bold)

    Catch ex As Exception

    End Try

    Try

      Grid_OptimisationResults.Cols.Count = 4
      Grid_OptimisationResults.Rows.Count = 1

      Grid_OptimisationResults.Cols(0).Caption = "CP"
      Grid_OptimisationResults.Cols(1).Caption = "Return"
      Grid_OptimisationResults.Cols(2).Caption = "SDev"
      Grid_OptimisationResults.Cols(3).Caption = "LambdaE"

    Catch ex As Exception

    End Try
  End Sub


#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' *****************************************************************************************
    ' Cancel Changes, redisplay form.
    ' *****************************************************************************************

    InstrumentsGridChanged = False

    Me.THIS_FORM_SelectingCombo.Enabled = True

    If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
      thisDataRow = Me.SortedRows(thisPosition)
      Call LoadInstrumentGrid(thisDataRow)
    Else
      Call btnNavFirst_Click(Me, New System.EventArgs)
    End If

  End Sub

  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' *****************************************************************************************
    ' Save Changes, if any, without prompting.
    '
    ' 25 Mar 2008, Code updated to make more robust the process of saving constraints if the Group is saved.
    ' I have also elected to preserve the currently selected Constraint set, rather than revert to the default
    ' set for this group.
    ' *****************************************************************************************

    Try
      Dim ExistingConstraintID As Integer = 0

      If (InstrumentsGridChanged = True) Then

        If (ConstrainsGridChanged = True) Then
          ExistingConstraintID = SaveConstraintsGrid()
        End If

        Call SaveInstrumentGrid(False)

        Application.DoEvents()

        If (ExistingConstraintID > 0) Then
          Constraints_ComboConstraintsGoup.SelectedValue = ExistingConstraintID
        End If

      ElseIf btnSave.Enabled Then
        Me.btnCancel.Enabled = False
        Me.btnSave.Enabled = False

        SetButtonStatus()
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Saving Group", ex.StackTrace, True)
    End Try

  End Sub

  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' *****************************************************************************************
    ' Close Form
    ' *****************************************************************************************

    If (InstrumentsGridChanged = True) Then
      Call SaveInstrumentGrid()
    End If

    If (ConstrainsGridChanged = True) Then
      Call SaveConstraintsGrid()
    End If

    Me.Close()

  End Sub

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region

#Region " Instruments Grid : Create / Paint Custom Field(s)"

  Private Sub CreateCustomFieldColumn(ByRef CustomFieldID As Integer)
    ' ********************************************************************************************
    ' Create a column in the Returns grid for the given Custom Field ID.
    '
    '
    ' ********************************************************************************************

    Dim ThisColumnName As String
    Dim ThisColumn As C1.Win.C1FlexGrid.Column
    Dim GroupListID As Integer

    ThisColumnName = "Custom_" & CustomFieldID.ToString

    Dim thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow

    ThisColumn = Nothing

    If (CustomFieldID > 0) Then

      ' Check that Custom Data is present for the selected field(s)
      If (Combo_SelectGroupID.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_SelectGroupID.SelectedValue)) Then
        GroupListID = CInt(Combo_SelectGroupID.SelectedValue)
      Else
        GroupListID = (0)
      End If

      If CustomFieldDataCache.GetCustomFieldDataStatus(CustomFieldID, GroupListID) <> CustomFieldDataCacheClass.CacheStatus.FullyPopulated Then
        CustomFieldDataCache.LoadCustomFieldData(CustomFieldID, GroupListID)
      End If

      ' Add Column if necessary

      If Not Grid_Returns.Cols.Contains(ThisColumnName) Then
        ThisColumn = Grid_Returns.Cols.Add()
        ThisColumn.Name = ThisColumnName
        ThisColumn.Caption = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, CustomFieldID, "FieldName")
        ThisColumn.Visible = True
        ThisColumn.DataType = GetType(String)
        ThisColumn.AllowDragging = True
        ThisColumn.AllowEditing = False
        ThisColumn.AllowResizing = True
        ThisColumn.AllowSorting = True
      End If

      ' Format the New column

      If (ThisColumn IsNot Nothing) Then ' A newly added column
        thisCustomFieldDefinition = Nothing
        Try
          thisCustomFieldDefinition = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, CustomFieldID, "")

          If (thisCustomFieldDefinition IsNot Nothing) Then
            If (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.TextType) = RenaissanceDataType.TextType Then
              ThisColumn.DataType = GetType(String)

            ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.DateType) = RenaissanceDataType.DateType Then
              ThisColumn.DataType = GetType(Date)
              ThisColumn.Format = DISPLAYMEMBER_DATEFORMAT

            ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.BooleanType) = RenaissanceDataType.BooleanType Then
              ThisColumn.DataType = GetType(Boolean)

            ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.PercentageType) = RenaissanceDataType.PercentageType Then
              ThisColumn.DataType = GetType(Double)
              ThisColumn.Format = "#,##0.00%"

            ElseIf (thisCustomFieldDefinition.FieldDataType And RenaissanceDataType.NumericType) = RenaissanceDataType.NumericType Then
              ThisColumn.DataType = GetType(Double)
              ThisColumn.Format = "#,##0.00"
            End If

            ' Editable Column ?

            If (Me.HasUpdatePermission) Then
              Select Case CType(thisCustomFieldDefinition.CustomFieldType, PertracCustomFieldTypes)
                Case PertracCustomFieldTypes.CustomBoolean
                  ThisColumn.AllowEditing = True

                Case PertracCustomFieldTypes.CustomDate
                  ThisColumn.AllowEditing = True

                Case PertracCustomFieldTypes.CustomNumeric
                  ThisColumn.AllowEditing = True

                Case PertracCustomFieldTypes.CustomString
                  ThisColumn.AllowEditing = True

              End Select
            End If

          End If
        Catch ex As Exception
        End Try
      End If
    End If

  End Sub

  Private Sub PaintCustomFieldColumn(ByRef pCustomColumnName As String)
    Try
      If (pCustomColumnName.StartsWith("Custom_")) AndAlso (IsNumeric(pCustomColumnName.Substring(7))) Then
        PaintCustomFieldColumn(CInt(pCustomColumnName.Substring(7)))
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub PaintCustomFieldColumn(ByRef pCustomFieldID As Integer)
    Dim ThisColumnName As String
    Dim ThisColumnOrdinal As Integer
    Dim PertracColumnOrdinal As Integer
    Dim ThisCustomValue As Object = Nothing
    Dim OrgInPaint As Boolean
    Dim RowCount As Integer
    Dim PertracID As Integer
    Dim GroupListID As Integer

    ThisColumnName = "Custom_" & pCustomFieldID.ToString

    If Grid_Returns.Cols.Contains(ThisColumnName) = False Then
      Exit Sub
    Else
      ThisColumnOrdinal = Grid_Returns.Cols.IndexOf(ThisColumnName)
    End If
    PertracColumnOrdinal = Grid_Returns.Cols.IndexOf("GroupPertracCode")

    If (Combo_SelectGroupID.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_SelectGroupID.SelectedValue)) Then
      GroupListID = CInt(Combo_SelectGroupID.SelectedValue)
    Else
      GroupListID = (0)
    End If

    ' OK...

    Try
      OrgInPaint = InPaint
      InPaint = True

      If CustomFieldDataCache.GetCustomFieldDataStatus(pCustomFieldID, GroupListID) <> CustomFieldDataCacheClass.CacheStatus.FullyPopulated Then
        CustomFieldDataCache.LoadCustomFieldData(pCustomFieldID, GroupListID)
      End If

      For RowCount = 1 To (Grid_Returns.Rows.Count - 1)

        If (IsNumeric(Grid_Returns.Item(RowCount, PertracColumnOrdinal))) Then
          PertracID = CInt(Grid_Returns.Item(RowCount, PertracColumnOrdinal))
        Else
          PertracID = (-1)
        End If

        If (PertracID > 0) Then

          Try
            Try
              ThisCustomValue = Nothing

              ThisCustomValue = CustomFieldDataCache.GetDataPoint(pCustomFieldID, GroupListID, PertracID)

              If (ThisCustomValue Is Nothing) OrElse (ThisCustomValue Is DBNull.Value) Then
                ThisCustomValue = Nz(ThisCustomValue, Grid_Returns.Cols(ThisColumnOrdinal).DataType)
              End If
            Catch ex As Exception
            End Try

            Grid_Returns.Item(RowCount, ThisColumnOrdinal) = ThisCustomValue

          Catch ex As Exception
            ' Set Zero value, appropriate to column type.
            Grid_Returns.Item(RowCount, ThisColumnOrdinal) = Nz(Nothing, Grid_Returns.Cols(ThisColumnOrdinal).DataType)
          End Try

        Else
          Grid_Returns.Item(RowCount, ThisColumnOrdinal) = Nz(Nothing, Grid_Returns.Cols(ThisColumnOrdinal).DataType)
        End If
      Next


    Catch ex As Exception
    Finally
      InPaint = OrgInPaint
    End Try

  End Sub

#End Region

#Region " Marginals Tab Code"

  Private Sub CalculateLiveCovarianceMatrix(ByVal StatsDatePeriod As DealingPeriod)
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    If (Me.Radio_Covariance_UseLiveMatrix.Checked = False) Then
      Exit Sub
    End If

    Dim OrgInpaint As Boolean
    Dim CovarianceStartDate As Date
    Dim CovarianceEndDate As Date

    ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
    OrgInpaint = InPaint

    Dim tmpCommand As New SqlCommand
    Dim ListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable = Nothing

    Try
      InPaint = True

      Me.Grid_Covariance.Rows.Count = 1
      Me.Grid_Covariance.Cols.Count = 1

      If (Combo_SelectGroupID.SelectedIndex < 0) OrElse (IsNumeric(Combo_SelectGroupID.SelectedValue) = False) Then
        Exit Sub
      End If

      Label_MatrixName.Text = "<Live>"

      If IsDate(Label_GroupStartDate.Text) Then
        Label_MatrixDateFrom.Text = Label_GroupStartDate.Text
      Else
        Label_MatrixDateFrom.Text = "1 Jan 1970"
      End If
      If IsDate(Label_GroupEndDate.Text) Then
        Label_MatrixDateTo.Text = Label_GroupEndDate.Text
      Else
        Label_MatrixDateTo.Text = "1 Jan 2100"
      End If

      CovarianceStartDate = CDate(Label_MatrixDateFrom.Text)
      CovarianceEndDate = CDate(Label_MatrixDateTo.Text)
      If (CovarianceEndDate.CompareTo(FitDateToPeriod(StatsDatePeriod, CovarianceEndDate, True)) < 0) Then ' If the Day is not at the end of the period then set to end of previous period.
        CovarianceEndDate = FitDateToPeriod(StatsDatePeriod, AddPeriodToDate(StatsDatePeriod, CovarianceEndDate, -1), True)
      End If
      Label_MatrixLamda.Text = "1.0"
      Label_MatrixStatus.Text = "<Loading>"

      ' Get Covariance Data

      tmpCommand.CommandType = CommandType.StoredProcedure
      tmpCommand.CommandText = "adp_tblGroupItems_SelectGroupPertracCodes"
      tmpCommand.Connection = MainForm.GetGenoaConnection
      tmpCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = CInt(Combo_SelectGroupID.SelectedValue)
      tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
      tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

      ListTable = New RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable
      ListTable.Columns.Add("ListDescription", GetType(String))
      ListTable.Columns.Add("PertracName", GetType(String))
      ListTable.Columns.Add("PertracProvider", GetType(String))
      ListTable.Columns.Add("IndexName", GetType(String))
      ListTable.Columns.Add("IndexProvider", GetType(String))

      ListTable.Load(tmpCommand.ExecuteReader)

      If (ListTable IsNot Nothing) Then
        GetFullCovarianceMatrix(MainForm, StatsDatePeriod, Grid_Covariance, ListTable, ScalingFactorHash, Check_Covariance_BackfillData.Checked, True, CDbl(Label_MatrixLamda.Text), CovarianceStartDate, CovarianceEndDate, True, False, 0, StatusLabel_Optimisation)
      Else
        Me.Grid_Covariance.Rows.Count = 1
        Me.Grid_Covariance.Cols.Count = 1
      End If

      ' Allow Field events to trigger before 'InPaint' Is re-set. 
      ' (Should) Prevent Validation errors during Form Draw.
      Application.DoEvents()

      InPaint = OrgInpaint

      Label_MatrixStatus.Text = ""

    Catch ex As Exception
      ListTable = Nothing
      Label_MatrixStatus.Text = "<Error>"

    Finally

      Try
        If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
          tmpCommand.Connection.Close()
          tmpCommand.Connection = Nothing
        End If
      Catch ex As Exception
      End Try

      InPaint = OrgInpaint

      Update_Group_RiskAndReturn(True)

    End Try

  End Sub

  Private Sub UpdateMarginalsGrid(Optional ByVal pForceDisplay As Boolean = False)
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    If (pForceDisplay = False) AndAlso (Grid_Marginals.Visible = False) Then
      Exit Sub
    End If

    UpdateMarginalsFlag = True

  End Sub

  Private Sub CalculateMarginals(ByRef ResultsTable As DSMarginals.tblMarginalsDataTable)
    ' ***************************************************************************************************
    '
    '
    '
    ' Marginals calculation is performed relative to the 'NewHolding' of the Instruments Grid. This is determined
    ' when the temporary Grid is built.
    '
    ' Re : 'Look for Substitute Trades Only'. This causes the Marginals calculation to only replace existing Trades.
    ' The intention is that the Contra Instrument is the Instrument that you really want to transact in exchange for
    ' existing transactions.
    ' i.e. For the 'Buy' cycle, only Instruments that already have a Sale transaction are considered (i.e. Buy Instrument, Sell Contra).
    ' For the 'Sell' cycle, only Instruments that already have a Buy transaction are considered (i.e. Sell Instrument, Buy Contra).
    '
    '
    ' ***************************************************************************************************

    Dim RVal As DSMarginals.tblMarginalsDataTable = ResultsTable
    Dim NewMarginalRow As DSMarginals.tblMarginalsRow

    Try
      Me.Cursor = Cursors.WaitCursor

      StatusLabel_Optimisation.Text = ""

      If RVal Is Nothing Then
        RVal = New DSMarginals.tblMarginalsDataTable
      Else
        Try
          RVal.Rows.Clear()
        Catch ex As Exception
        End Try
      End If

      ' Check Parameters

      ' Get Essential Parameters

      Dim TradeSize As Double = Math.Abs(Edit_Marginals_TradeSize.Value)
      Dim RiskLimit As Double = Math.Abs(Edit_Marginals_RiskLimit.Value)
      Dim ReturnTarget As Double = Edit_Marginals_ReturnTarget.Value
      Dim RiskFreeRate As Double = Math.Abs(Edit_Marginals_RiskFreeRate.Value)

      If (TradeSize <= 0) Then
        ' MessageBox.Show("Trade Size must be entered", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
        StatusLabel_Optimisation.Text = "Trade Size must be entered"
        ' Return RVal
      End If
      If (RiskLimit <= 0) Then
        ' MessageBox.Show("Risk Limit must be entered", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation, MessageBoxDefaultButton.Button1)
        StatusLabel_Optimisation.Text = "Risk Limit must be entered"
        ' Return RVal
      End If

      ' Get non-essential parameters

      Dim AllowBuys As Boolean = Check_Marginals_Buy.Checked
      Dim AllowSells As Boolean = Check_Marginals_Sell.Checked
      Dim SellWholeHolding As Boolean = Check_Marginals_SellAll.Checked
      Dim ContraTrade As Boolean = Check_Marginals_ContraTrade.Checked
      Dim ContraInstrument As Integer = (-1)
      Dim ContraTradeIndex As Integer = (-1)
      Dim SaveHolding As Double
      Dim SaveContraHolding As Double
      Dim InitialRisk As Double = 0
      Dim InitialReturn As Double = 0
      Dim ThisRisk As Double
      Dim ThisReturn As Double
      Dim ThisWorstDrawDown As Double
      Dim SharpeRatio As Double

      If (ContraTrade) AndAlso (Combo_Marginals_ContraInstrument.SelectedIndex > 0) AndAlso (IsNumeric(Combo_Marginals_ContraInstrument.SelectedValue)) Then
        ContraInstrument = CInt(Combo_Marginals_ContraInstrument.SelectedValue)
      Else
        ContraTrade = False
      End If

      Dim SubstituteTradesOnly As Boolean = Check_Marginals_SubstituteTradesOnly.Checked
      Dim ImproveReturn As Boolean = Radio_ImproveReturn.Checked
      Dim ImproveRisk As Boolean = Radio_ImproveRisk.Checked
      Dim ImproveSharpeRatio As Boolean = Radio_ImproveSharpeRatio.Checked
      Dim ImproveRiskReturn As Boolean = Radio_ImproveTargetRiskReturn.Checked
      Dim ImproveDrawDown As Boolean = Radio_ImproveWorstDrawDown.Checked
      Dim ImproveLeastSquares As Boolean = Radio_ImproveLeastSquares.Checked
      Dim ImproveStdError As Boolean = Radio_ImproveStdError.Checked
      Dim ImproveVsInstrument As Integer = 0
      Dim ImproveVsInstrument_Dates() As Date
      Dim ImproveVsInstrument_Returns() As Double
      Dim ImproveVsInstrument_NAVs() As Double

      Dim ApplyBounds As Boolean = Check_Marginals_ApplyBounds.Checked

      Dim ExcludeInstrumentBreaks As Boolean = Check_Marginals_ExcludeInstrumentBreaks.Checked
      Dim ExcludeSectorBreaks As Boolean = Check_Marginals_ExcludeSectorBreaks.Checked
      Dim ExcludeLiquidityBreaks As Boolean = Check_Marginals_ExcludeLiquidityBreaks.Checked
      Dim ExcludeCustomLimitBreaks As Boolean = Check_Marginals_ExcludeCustomLimitBreaks.Checked

      ' Validate MarginalsInstrument
      Try
        If (Combo_MarginalIndex.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_MarginalIndex.SelectedValue)) Then
          ImproveVsInstrument = CInt(Combo_MarginalIndex.SelectedValue)
        End If
      Catch ex As Exception
        ImproveVsInstrument = 0
      End Try
      If (ImproveVsInstrument <= 0) Then
        ImproveLeastSquares = False
        ImproveStdError = False
      End If

      ' Get MarginalsInstrument NAVs

      If (ImproveLeastSquares OrElse ImproveStdError) Then
        ImproveVsInstrument_Dates = MainForm.StatFunctions.DateSeries(DefaultStatsDatePeriod, CULng(ImproveVsInstrument), False, MainForm.StatFunctions.AnnualPeriodCount(DefaultStatsDatePeriod), 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data)
        ImproveVsInstrument_Returns = MainForm.StatFunctions.ReturnSeries(DefaultStatsDatePeriod, CULng(ImproveVsInstrument), False, MainForm.StatFunctions.AnnualPeriodCount(DefaultStatsDatePeriod), 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data)
        ImproveVsInstrument_NAVs = MainForm.StatFunctions.NAVSeries(DefaultStatsDatePeriod, CULng(ImproveVsInstrument), False, MainForm.StatFunctions.AnnualPeriodCount(DefaultStatsDatePeriod), 1.0#, True, Renaissance_BaseDate, Renaissance_EndDate_Data)

        If ((ImproveVsInstrument_Dates Is Nothing) OrElse (ImproveVsInstrument_Returns Is Nothing) OrElse (ImproveVsInstrument_NAVs Is Nothing)) OrElse ((ImproveVsInstrument_Dates.Length <= 0) OrElse (ImproveVsInstrument_NAVs.Length <= 0) OrElse (ImproveVsInstrument_Returns.Length <= 0)) Then
          ImproveLeastSquares = False
          ImproveStdError = False
        End If

      Else
        ImproveVsInstrument_Dates = Nothing
        ImproveVsInstrument_Returns = Nothing
        ImproveVsInstrument_NAVs = Nothing
      End If

      ' Set Temporary Returns Grid

      Dim RowCount As Integer
      Dim SumValue As Double
      Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
      Dim Col_PertracName As Integer = Grid_Returns.Cols("PertracName").SafeIndex
      Dim Col_GroupTrade As Integer = Grid_Returns.Cols("GroupTrade").SafeIndex
      Dim Col_GroupNewHolding As Integer = Grid_Returns.Cols("GroupNewHolding").SafeIndex
      Dim Col_GroupReturn As Integer = Grid_Returns.Cols("GroupExpectedReturn").SafeIndex

      Grid_CalculateMarginals.Rows.Count = 0
      Grid_CalculateMarginals.Rows.Count = Me.Grid_Returns.Rows.Count
      If (Grid_CalculateMarginals.Cols.Count <> (TmpMarginalsCols.MAX_Count + 1)) Then
        Grid_CalculateMarginals.Cols.Count = TmpMarginalsCols.MAX_Count + 1
        Grid_CalculateMarginals.Cols(TmpMarginalsCols.PertracID).DataType = GetType(Integer)
        Grid_CalculateMarginals.Cols(TmpMarginalsCols.Holding).DataType = GetType(Double)
        Grid_CalculateMarginals.Cols(TmpMarginalsCols.Weighting).DataType = GetType(Double)
      End If

      Grid_CalculateMarginals.Item(0, TmpMarginalsCols.PertracID) = 0
      Grid_CalculateMarginals.Item(0, TmpMarginalsCols.Holding) = 0.0#
      Grid_CalculateMarginals.Item(0, TmpMarginalsCols.Trade) = 0.0#
      Grid_CalculateMarginals.Item(RowCount, TmpMarginalsCols.InstrumentReturn) = 0.0#
      Grid_CalculateMarginals.Item(0, TmpMarginalsCols.Weighting) = 0.0#

      For RowCount = 1 To (Grid_Returns.Rows.Count - 1)
        Try
          Grid_CalculateMarginals.Item(RowCount, TmpMarginalsCols.PertracID) = CInt(Grid_Returns.Item(RowCount, Col_GroupPertracCode))
          Grid_CalculateMarginals.Item(RowCount, TmpMarginalsCols.Holding) = CDbl(Grid_Returns.Item(RowCount, Col_GroupNewHolding))
          Grid_CalculateMarginals.Item(RowCount, TmpMarginalsCols.Trade) = CDbl(Grid_Returns.Item(RowCount, Col_GroupTrade))
          Grid_CalculateMarginals.Item(RowCount, TmpMarginalsCols.InstrumentReturn) = CDbl(Grid_Returns.Item(RowCount, Col_GroupReturn))
          Grid_CalculateMarginals.Item(RowCount, TmpMarginalsCols.Weighting) = 0.0#

          If (ContraInstrument > 0) AndAlso (ContraTradeIndex < 0) Then
            If (CInt(Grid_Returns.Item(RowCount, Col_GroupPertracCode)) = ContraInstrument) Then
              ContraTradeIndex = RowCount
            End If
          End If
        Catch ex As Exception
        End Try
      Next

      InitialRisk = CDbl(ConvertValue(Label_NewHoldingRisk.Text, GetType(Double)))
      InitialReturn = CDbl(ConvertValue(Label_NewHoldingReturn.Text, GetType(Double)))

      SumValue = Update_Grid_Percentages(Grid_CalculateMarginals, TmpMarginalsCols.Holding, TmpMarginalsCols.Weighting, 0, 0, 0, 0, False)

      ' Create Marginals

      Dim MarginalTradeType As MarginalTradeType
      Dim DoMarginals As Boolean
      Dim IncludeRow As Boolean
      Dim Trade As Double
      Dim Status As String
      Dim ConstraintStatusArray() As String = Nothing
      Dim StatusFlag As Boolean
      Dim BelowRisk As Boolean
      Dim AboveReturn As Boolean
      Dim Comment As String
      Dim Utility As Double
      Dim ExistingTrade As Double

      Dim ConstraintResult As MarginalsStatusEnum

      Dim ExistingDistance As Double ' Used for Risk/Return optimisation
      Dim NewDistance As Double
      Dim RiskMultiplier As Double

      RiskMultiplier = Math.Abs((InitialReturn - ReturnTarget) / Math.Max(0.001, Math.Abs(InitialRisk - RiskLimit))) ^ 0.5#

      ' Prep the Constraints Worker 

      ConstraintWorker.RefreshAllData() ' No Force Refresh.

      ' Calculate Marginals

      For MarginalTradeType = MarginalTradeType.Buy To MarginalTradeType.Sell

        DoMarginals = True

        ' Arrange to Omit 'Buy' or 'Sell' Marginals appropriate to the selected options.

        If (MarginalTradeType = MarginalTradeType.Buy) And (Not AllowBuys) Then DoMarginals = False
        If (MarginalTradeType = MarginalTradeType.Sell) And (Not AllowSells) Then DoMarginals = False

        ' Calculate Marginals :

        If DoMarginals Then

          For RowCount = 1 To (Grid_CalculateMarginals.Rows.Count - 1)

            ' Initialise this Pass.

            SaveHolding = 0
            SaveContraHolding = 0
            ExistingTrade = 0
            NewMarginalRow = Nothing
            StatusFlag = True
            Comment = ""

            ' If we are not doing 'Contra' trades, or this is not the 'Contra' Instrument, then ...

            If (Not ContraTrade) OrElse (RowCount <> ContraTradeIndex) Then

              NewMarginalRow = RVal.NewtblMarginalsRow

              NewMarginalRow.PertracID = Grid_CalculateMarginals.Item(RowCount, TmpMarginalsCols.PertracID)
              NewMarginalRow.ContraID = 0
              NewMarginalRow.InstrumentName = Grid_Returns.Item(RowCount, Col_PertracName)
              NewMarginalRow.TradeAmount = 0
              NewMarginalRow.NewWeight = 0

              ExistingTrade = CDbl(Grid_CalculateMarginals.Item(RowCount, TmpMarginalsCols.Trade))
              Trade = TradeSize

              'Increment holding and update percentages and exposures against limits

              If MarginalTradeType = MarginalTradeType.Buy Then
                NewMarginalRow.BuySell = "Buy"

                If ContraTrade Then
                  ' Limit Contra 'Sale' to the existing Contra Position.
                  Trade = Math.Max(0.0#, Math.Min(TradeSize, CDbl(Grid_CalculateMarginals.Item(ContraTradeIndex, TmpMarginalsCols.Holding))))
                End If

                If SubstituteTradesOnly Then
                  If ExistingTrade < 0.0# Then
                    Trade = Math.Max(0.0#, Math.Min(Math.Abs(ExistingTrade), Trade))
                  Else
                    Trade = 0
                  End If
                End If

                NewMarginalRow.TradeAmount = Trade

                SaveHolding = CDbl(Grid_CalculateMarginals.Item(RowCount, TmpMarginalsCols.Holding))

                Grid_CalculateMarginals.Item(RowCount, TmpMarginalsCols.Holding) = SaveHolding + Trade

                NewMarginalRow.NewHolding = Grid_CalculateMarginals.Item(RowCount, TmpMarginalsCols.Holding)

                If ContraTrade Then
                  SaveContraHolding = CDbl(Grid_CalculateMarginals.Item(ContraTradeIndex, TmpMarginalsCols.Holding))
                  Grid_CalculateMarginals.Item(ContraTradeIndex, TmpMarginalsCols.Holding) = SaveContraHolding - Trade
                  NewMarginalRow.ContraID = ContraInstrument
                End If

              Else
                NewMarginalRow.BuySell = "Sell"

                If SubstituteTradesOnly Then
                  If ExistingTrade <= 0 Then
                    Trade = 0
                  Else
                    Trade = Math.Max(0.0#, Math.Min(ExistingTrade, Trade))
                  End If
                End If

                If SellWholeHolding Then
                  ' Set Trade to Current holding

                  Trade = Math.Max(0.0#, Grid_CalculateMarginals.Item(RowCount, TmpMarginalsCols.Holding))
                Else
                  ' Limit the sale to no more than the current holding.

                  Trade = Math.Max(0.0#, Math.Min(Trade, Grid_CalculateMarginals.Item(RowCount, TmpMarginalsCols.Holding)))
                End If

                ' Apply Trades to Instrument & Contra (if necessary)

                NewMarginalRow.TradeAmount = Trade
                SaveHolding = CDbl(Grid_CalculateMarginals.Item(RowCount, TmpMarginalsCols.Holding))
                Grid_CalculateMarginals.Item(RowCount, TmpMarginalsCols.Holding) = SaveHolding - Trade
                NewMarginalRow.NewHolding = Grid_CalculateMarginals.Item(RowCount, TmpMarginalsCols.Holding)

                If ContraTrade Then
                  SaveContraHolding = CDbl(Grid_CalculateMarginals.Item(ContraTradeIndex, TmpMarginalsCols.Holding))
                  Grid_CalculateMarginals.Item(ContraTradeIndex, TmpMarginalsCols.Holding) = SaveContraHolding + Trade
                  NewMarginalRow.ContraID = ContraInstrument
                End If

              End If

              ' Calculate New weights.

              Update_Grid_Percentages(Grid_CalculateMarginals, TmpMarginalsCols.Holding, TmpMarginalsCols.Weighting, 0, 0, 0, 0, False)

              ' Save New Weighting

              NewMarginalRow.NewWeight = Grid_CalculateMarginals.Item(RowCount, TmpMarginalsCols.Weighting)

              ' Calculate new portfolio Risk & return

              ThisRisk = CalculateGroupRisk(Grid_CalculateMarginals, Grid_Covariance, TmpMarginalsCols.PertracID, TmpMarginalsCols.Weighting)
              ThisReturn = CalculateGroupReturn(Grid_CalculateMarginals, TmpMarginalsCols.Weighting, TmpMarginalsCols.InstrumentReturn)

              If (ImproveDrawDown) Then
                ThisWorstDrawDown = CalculateGroupWorstDrawdown(Me.DefaultStatsDatePeriod, Grid_CalculateMarginals, TmpMarginalsCols.PertracID, TmpMarginalsCols.Weighting, False)
              Else
                ThisWorstDrawDown = 0.0#
              End If

              ' Calculate Sharpe.

              If (ThisRisk = 0) Then
                SharpeRatio = 0
              Else
                SharpeRatio = (ThisReturn - RiskFreeRate) / ThisRisk
              End If

              ' Save Details to new Marginals row.

              NewMarginalRow.PortfolioRisk = ThisRisk
              NewMarginalRow.PortfolioReturn = ThisReturn

              NewMarginalRow.LimitStatus = "OK"
              NewMarginalRow.LimitComment = ""

              ' CheckLimits_Marginals()

              If Trade = 0 Then

                IncludeRow = False
                Status = "OK"

              Else

                IncludeRow = True

                ConstraintResult = ConstraintWorker.CheckMarginalsLimits(ConstraintStatusArray, NewMarginalRow.PertracID, NewMarginalRow.ContraID, Grid_CalculateMarginals, TmpMarginalsCols.PertracID, TmpMarginalsCols.Weighting, ExcludeInstrumentBreaks, ExcludeSectorBreaks, ExcludeLiquidityBreaks, ExcludeCustomLimitBreaks)

                If (ConstraintResult = MarginalsStatusEnum.OK) Then
                  Status = "OK"
                Else
                  If (ConstraintStatusArray IsNot Nothing) AndAlso (ConstraintStatusArray.Length > 0) Then
                    Comment = ConstraintStatusArray(0)
                  Else
                    Comment = "BREAK"
                  End If

                  Status = "BREAK"
                  StatusFlag = False

                  ' Check ExcludeInstrumentBreaks, ExcludeSectorBreaks, ExcludeLiquidityBreaks, ExcludeCustomLimitBreaks

                  If (ExcludeInstrumentBreaks) AndAlso (CBool(ConstraintResult And MarginalsStatusEnum.InstrumentBreak)) Then
                    IncludeRow = False
                  ElseIf (ExcludeSectorBreaks) AndAlso (CBool(ConstraintResult And MarginalsStatusEnum.SectorBreak)) Then
                    IncludeRow = False
                  ElseIf (ExcludeLiquidityBreaks) AndAlso (CBool(ConstraintResult And MarginalsStatusEnum.LiquidityBreak)) Then
                    IncludeRow = False
                  ElseIf (ExcludeCustomLimitBreaks) AndAlso (CBool(ConstraintResult And MarginalsStatusEnum.CustomFieldBreak)) Then
                    IncludeRow = False
                  End If

                End If

              End If

              'Restore holdings and Exposures or original settings

              Grid_CalculateMarginals.Item(RowCount, TmpMarginalsCols.Holding) = SaveHolding
              If ContraTrade Then
                Grid_CalculateMarginals.Item(ContraTradeIndex, TmpMarginalsCols.Holding) = SaveContraHolding
              End If

              ' Set Risk / Return OK Flags

              BelowRisk = True
              AboveReturn = True

              If (Not ImproveRiskReturn) Then

                If ThisReturn >= ReturnTarget Then
                  AboveReturn = True
                Else
                  AboveReturn = False
                End If

                If ThisRisk <= RiskLimit Then
                  BelowRisk = True
                Else
                  BelowRisk = False
                End If

              End If

              ' Set Utility Value.

              If ImproveRisk Then

                'Set utility if minimising risk subject to return constraint

                If ThisReturn >= ReturnTarget Then
                  Utility = 1 - ThisRisk
                Else
                  Utility = ThisReturn
                End If

              ElseIf ImproveReturn = True Then

                'Set utility if maximising return subject to risk constraint

                If ThisRisk <= RiskLimit Then
                  Utility = ThisReturn
                Else
                  Utility = 0 - ThisRisk
                End If

              ElseIf ImproveSharpeRatio Then

                'Set utility if maximising Sharpe Ratio

                Utility = SharpeRatio

              ElseIf ImproveRiskReturn Then

                'Set utility if maximising Risk / Return Ratio

                ExistingDistance = ((((InitialReturn - ReturnTarget) ^ 2.0#) + (((InitialRisk - RiskLimit) * RiskMultiplier) ^ 2.0#))) ' ^ 0.5#)
                NewDistance = ((((ThisReturn - ReturnTarget) ^ 2.0#) + (((ThisRisk - RiskLimit) * RiskMultiplier) ^ 2.0#)))  '  ^ 0.5#)

                Utility = -NewDistance

                If (NewDistance > ExistingDistance) Then
                  BelowRisk = True
                  AboveReturn = True
                End If

              ElseIf ImproveDrawDown Then

                Utility = -ThisWorstDrawDown

              ElseIf ImproveLeastSquares Then

                Utility = -CalculateLeastSquares(DefaultStatsDatePeriod, Grid_CalculateMarginals, TmpMarginalsCols.PertracID, TmpMarginalsCols.Weighting, False, ImproveVsInstrument_Dates, ImproveVsInstrument_NAVs, True, Check_Marginals_AnalysisPeriod.Checked, Edit_Marginals_AnalysisPeriod.Value)

              ElseIf ImproveStdError Then

                Utility = -CalculateStdError(DefaultStatsDatePeriod, Grid_CalculateMarginals, TmpMarginalsCols.PertracID, TmpMarginalsCols.Weighting, False, ImproveVsInstrument_Dates, ImproveVsInstrument_Returns, Check_Marginals_AnalysisPeriod.Checked, Edit_Marginals_AnalysisPeriod.Value)

              End If

              'Finish updating new row and add to marginals dataset

              NewMarginalRow.SharpeRatio = SharpeRatio
              NewMarginalRow.WorstDrawdown = ThisWorstDrawDown
              NewMarginalRow.LimitStatus = Status
              NewMarginalRow.LimitFlag = StatusFlag
              NewMarginalRow.LimitComment = Comment
              NewMarginalRow.AboveTargetReturn = AboveReturn
              NewMarginalRow.BelowTargetRisk = BelowRisk
              NewMarginalRow.Utility = Utility

              If IncludeRow AndAlso (NewMarginalRow.RowState = DataRowState.Detached) Then
                Try
                  RVal.Rows.Add(NewMarginalRow)
                Catch ex As Exception
                End Try
              End If

            End If

          Next ' For RowCount = 1 To (Grid_CalculateMarginals.Rows.Count - 1)

        End If ' If DoMarginals Then

      Next ' For MarginalTradeType = MarginalTradeType.Buy To MarginalTradeType.Sell

    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try

    Try
      RVal.DefaultView.Sort = "Utility Desc"
    Catch ex As Exception
    End Try

    ' Return RVal

  End Sub

  Sub Grid_Marginals_cms_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************
    Dim thisContextMenu As ContextMenuStrip
    Dim thisMarginalsGridDetails As MarginalsGridDetails

    Try

      thisContextMenu = CType(sender, ContextMenuStrip)
      thisMarginalsGridDetails = thisContextMenu.Tag

      ' Clear the ContextMenuStrip control's 
      ' Items collection.
      thisContextMenu.Items.Clear()

      If (Grid_Marginals.MouseRow <= 0) Then
        Exit Sub
      End If

      ' Save Mouse Location

      thisMarginalsGridDetails.GridClickMouseRow = Grid_Marginals.MouseRow
      thisMarginalsGridDetails.GridClickMouseCol = Grid_Marginals.MouseCol

      If (Grid_Marginals.Selection.IsValid) AndAlso (thisMarginalsGridDetails.GridClickMouseRow >= Grid_Marginals.Selection.r1) AndAlso (thisMarginalsGridDetails.GridClickMouseRow <= Grid_Marginals.Selection.r2) Then
        Try

          Dim NewItem As System.Windows.Forms.ToolStripItem

          ' Trade Selection ?
          NewItem = thisContextMenu.Items.Add("Enter Trades for the selected rows", Nothing, AddressOf Marginals_TradeSelection)
          NewItem.Tag = thisMarginalsGridDetails

        Catch ex As Exception
        End Try
      End If
      If (thisMarginalsGridDetails.GridClickMouseRow >= 0) AndAlso (thisMarginalsGridDetails.GridClickMouseRow < Grid_Marginals.Rows.Count) Then
        Try

          Dim NewItem As System.Windows.Forms.ToolStripItem
          Dim Col_InstrumentName As Integer = Grid_Marginals.Cols("InstrumentName").SafeIndex
          Dim Col_BuySell As Integer = Grid_Marginals.Cols("BuySell").SafeIndex

          ' Trade Item ?
          NewItem = thisContextMenu.Items.Add("Enter Trade to " & Grid_Marginals.Item(thisMarginalsGridDetails.GridClickMouseRow, Col_BuySell).ToString & " " & Grid_Marginals.Item(thisMarginalsGridDetails.GridClickMouseRow, Col_InstrumentName).ToString, Nothing, AddressOf Marginals_TradeRow)
          NewItem.Tag = thisMarginalsGridDetails

        Catch ex As Exception
        End Try
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Marginals_TradeSelection(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim GridRowCounter As Integer
    Dim ThisMarginalsRow As C1.Win.C1FlexGrid.Row
    Dim SelectedRowCount As Integer = 0

    Dim MarginalTrades(-1) As MarginalTrade

    Try
      If (Not Grid_Marginals.Selection.IsValid) Then
        Exit Sub
      End If

      For GridRowCounter = Grid_Marginals.Selection.r1 To Grid_Marginals.Selection.r2
        ThisMarginalsRow = Grid_Marginals.Rows(GridRowCounter)

        If (Not ThisMarginalsRow.IsNew) Then
          SelectedRowCount += 1
        End If
      Next

      ' Redim Arrays
      If (SelectedRowCount <= 0) Then
        Exit Sub
      End If

      ReDim MarginalTrades(SelectedRowCount - 1)

      ' Record Trades

      SelectedRowCount = 0
      For GridRowCounter = Grid_Marginals.Selection.r1 To Grid_Marginals.Selection.r2
        ThisMarginalsRow = Grid_Marginals.Rows(GridRowCounter)

        If (Not ThisMarginalsRow.IsNew) Then
          MarginalTrades(SelectedRowCount) = New MarginalTrade(ThisMarginalsRow)

          SelectedRowCount += 1
        End If
      Next

      ' Process Trades

      Try
        InPaint = True

        For SelectedRowCount = 0 To (MarginalTrades.Length - 1)
          ProcessMarginalsTrade(MarginalTrades(SelectedRowCount))
        Next

      Catch ex As Exception
      Finally
        InPaint = False

        Call Update_Group_Percentages(True)
        Call Update_Group_RiskAndReturn(False)

        Call PaintGroupReturnChart()

        Call UpdateMarginalsGrid()
        Call CalculateConstraintExposures()

      End Try

      ' Update Portfolio Chart

      Try
        If (Label_NewHoldingReturn.Text.Length > 0) AndAlso (Label_NewHoldingRisk.Text.Length > 0) Then
          RiskReturnChartPoints.Add(New DecimalPoint(CDbl(ConvertValue(Label_NewHoldingRisk.Text, GetType(Double))), CDbl(ConvertValue(Label_NewHoldingReturn.Text, GetType(Double)))))

          PaintResultsCharts()
        End If
      Catch ex As Exception
      End Try

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Marginals_TradeRow(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************
    Dim ThisItem As System.Windows.Forms.ToolStripItem
    Dim thisMarginalsGridDetails As MarginalsGridDetails

    Try
      ThisItem = CType(sender, System.Windows.Forms.ToolStripItem)
      thisMarginalsGridDetails = CType(ThisItem.Tag, MarginalsGridDetails)

      ProcessMarginalsTrade(Grid_Marginals.Rows(thisMarginalsGridDetails.GridClickMouseRow))

      ' Update Portfolio Chart

      If (Label_NewHoldingReturn.Text.Length > 0) AndAlso (Label_NewHoldingRisk.Text.Length > 0) Then
        RiskReturnChartPoints.Add(New DecimalPoint(CDbl(ConvertValue(Label_NewHoldingRisk.Text, GetType(Double))), CDbl(ConvertValue(Label_NewHoldingReturn.Text, GetType(Double)))))

        PaintResultsCharts()
      End If

    Catch ex As Exception
    End Try

  End Sub


#End Region

#Region " Constraints Tab Code"

  Private Sub Constraints_ComboConstraintsGoup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Constraints_ComboConstraintsGoup.SelectedIndexChanged
    ' *****************************************************************
    '
    '
    ' *****************************************************************

    Try
      If (InPaint = False) AndAlso (Me.Created) AndAlso (Me.IsDisposed = False) Then

        If (ConstrainsGridChanged = True) Then
          Call SaveConstraintsGrid()
          ConstrainsGridChanged = False
        End If

        If (Constraints_ComboConstraintsGoup.SelectedIndex <= 0) OrElse (Constraints_ComboConstraintsGoup.SelectedValue Is Nothing) OrElse (IsNumeric(Constraints_ComboConstraintsGoup.SelectedValue) = False) Then
          Constraints_ComboConstraintsGoup.Tag = 0
          Constraints_ComboConstraintsGoup.SelectedValue = 0
          LoadConstraintsGroup(0)
        Else
          Constraints_ComboConstraintsGoup.Tag = Constraints_ComboConstraintsGoup.SelectedIndex
          LoadConstraintsGroup(CInt(Constraints_ComboConstraintsGoup.SelectedValue))
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Constraints_Grid_Defined_AfterDeleteRow(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Constraints_Grid_Defined.AfterDeleteRow
    ' *****************************************************************
    '
    '
    ' *****************************************************************

    Try
      If (Me.Created) AndAlso (Me.IsDisposed = False) AndAlso (InPaint = False) Then
        ConstrainsGridChanged = False

        Constraint_Button_SaveGroup.Enabled = False
        Constraint_Button_CancelGroup.Enabled = False
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Constraints_Grid_Defined_CellChanged(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs) Handles Constraints_Grid_Defined.CellChanged
    ' *****************************************************************
    '
    '
    ' *****************************************************************

    Try
      If e.Row <= 0 Then
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    Try
      If (Me.Created) AndAlso (Me.IsDisposed = False) Then

        Dim Col_CustomFieldID As Integer = Constraints_Grid_Defined.Cols("Col_CustomID").SafeIndex
        Dim Col_CustomFieldDataType As Integer = Constraints_Grid_Defined.Cols("Col_CustomFieldDataType").SafeIndex
        Dim Col_IsDefined As Integer = Constraints_Grid_Defined.Cols("Col_IsDefined").SafeIndex
        Dim Col_IsApplied As Integer = Constraints_Grid_Defined.Cols("Col_IsApplied").SafeIndex
        Dim Col_CustomFieldName As Integer = Constraints_Grid_Defined.Cols("Col_CustomName").SafeIndex

        Dim GroupID As Integer = 0
        Dim ThisCustomFieldID As Integer
        Dim ThisCustomFieldDataType As Integer
        Dim ThisCustomFieldName As String

        Dim TabName As String
        Dim NewTabPage As TabPage

        If (Constraints_ComboConstraintsGoup.SelectedIndex > 0) AndAlso (IsNumeric(Constraints_ComboConstraintsGoup.SelectedValue)) Then
          GroupID = CInt(Constraints_ComboConstraintsGoup.SelectedValue)
        End If
        ThisCustomFieldID = CInt(Nz(Constraints_Grid_Defined.Rows(e.Row).Item(Col_CustomFieldID), 0))
        ThisCustomFieldDataType = CInt(Nz(Constraints_Grid_Defined.Rows(e.Row).Item(Col_CustomFieldDataType), RenaissanceDataType.NumericType))
        ThisCustomFieldName = Nz(Constraints_Grid_Defined.Rows(e.Row).Item(Col_CustomFieldName), ThisCustomFieldID.ToString).ToString

        Select Case e.Col
          Case Col_IsDefined
            Try
              If (ThisCustomFieldID < 0) Then
                TabName = "Tab_" & CType(ThisCustomFieldID, SpecialConstraintFields).ToString
              Else
                TabName = "Tab_" & ThisCustomFieldID.ToString
              End If

              If CBool(Constraints_Grid_Defined.Item(e.Row, Col_IsDefined)) Then
                If Constraints_Grid_Defined.Focused Then
                  Constraints_Grid_Defined.Item(e.Row, Col_IsApplied) = True
                End If

                If Not (Me.Constraints_TabConstraints.TabPages.ContainsKey(TabName)) Then
                  ' Only create new tab it it does not already exist.

                  NewTabPage = ConstructConstraintsTabPage(GroupID, ThisCustomFieldID, ThisCustomFieldDataType, TabName, ThisCustomFieldName, False)
                  ' More_Code_Here(" Thinking about the Un-Check -> Check Scenario. ")
                  ' RefreshConstraintsTabPage(NewTabPage, GroupID, ThisCustomFieldID)

                End If

                ' Constraints_Grid_Defined.Sort(New ConstraintFieldGridComparer)

                If Constraints_Grid_Defined.Focused Then
                  ReOrderConstraintTabs()
                End If

              Else
                Constraints_Grid_Defined.Item(e.Row, Col_IsApplied) = False

                ' Hide TabPage

                If Constraints_TabConstraints.TabPages.ContainsKey(TabName) Then
                  NewTabPage = Constraints_TabConstraints.TabPages(TabName)

                  Constraints_TabConstraints.TabPages.Remove(NewTabPage)

                  ' Save This TabPage for later

                  'If (NewTabPage.Tag IsNot Nothing) AndAlso (TypeOf NewTabPage.Tag Is ConstraintsTabDetails) Then
                  '	CType(NewTabPage.Tag, ConstraintsTabDetails).ConstraintsGrid.Rows.Count = 1
                  'End If

                  ConstraintsTabPageStack.Push(NewTabPage)

                  ' Constraints_Grid_Defined.Sort(New ConstraintFieldGridComparer)

                End If

              End If

              ' If this is, or appears to be, a user action then Re-Sort and set Changed Flags etc.
              ' If this update is part of another process, such as 'Load Grid' then you do not
              ' want to do this stuff.

              If (Constraints_Grid_Defined.Focused) OrElse ((Constraints_Grid_Defined.Row = e.Row) And (Constraints_Grid_Defined.Col = e.Col)) Then
                Constraints_Grid_Defined.Sort(New ConstraintFieldGridComparer)

                Set_ConstrainsGridChanged()
              End If

            Catch ex As Exception
            End Try

          Case Col_IsApplied
            Try
              If (CBool(Constraints_Grid_Defined.Item(e.Row, Col_IsApplied))) Then
                If (Not CBool(Constraints_Grid_Defined.Item(e.Row, Col_IsDefined))) Then

                  Constraints_Grid_Defined.Item(e.Row, Col_IsApplied) = False

                End If
              End If

              ConstraintWorker.InvalidateConstraintDetails()

            Catch ex As Exception
            End Try

        End Select

      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub LoadConstraintsGroup(ByVal pGroupID As Integer, Optional ByVal pforceUpdate As Boolean = False, Optional ByVal pSetAllActive As Boolean = True)
    ' *****************************************************************
    '
    '
    ' *****************************************************************
    Static LastLoadedGroup As Integer = (-1)
    Static LastLoadedTime As Date

    ' Simple Get Out logic. The idea is to avoid repeatedly loading the same constraints grid
    ' if this function is repeatedly called in quick succession. This can happen when selecting off
    ' the Constraints Set combo for example.

    If (Not pforceUpdate) AndAlso (LastLoadedGroup = pGroupID) AndAlso ((Now - LastLoadedTime).TotalSeconds < 2.0) Then
      LastLoadedTime = Now()
      Exit Sub
    End If

    CurrentConstraintGroupID = pGroupID

    Dim GridRowCounter As Integer
    Dim SelectedConstraintRows() As DSGenoaConstraintItems.tblGenoaConstraintItemsRow
    Dim ThisCustomFieldID As Integer
    Dim TabName As String

    Try
      Dim Col_CustomFieldID As Integer = Constraints_Grid_Defined.Cols("Col_CustomID").SafeIndex
      Dim Col_CustomFieldDataType As Integer = Constraints_Grid_Defined.Cols("Col_CustomFieldDataType").SafeIndex
      Dim Col_CustomFieldName As Integer = Constraints_Grid_Defined.Cols("Col_CustomName").SafeIndex
      Dim Col_IsDefined As Integer = Constraints_Grid_Defined.Cols("Col_IsDefined").SafeIndex
      Dim Col_IsApplied As Integer = Constraints_Grid_Defined.Cols("Col_IsApplied").SafeIndex

      ' Check the Field Select Grid is up to date

      Call SetConstraints_FieldSelectGrid()

      Try
        Constraints_Grid_Defined.Select(0, 0)
      Catch ex As Exception
      End Try

      ' Get Current Constraints for this Group

      If (tbl_CurrentConstraintsGroup <> pGroupID) OrElse (tbl_CurrentConstraints Is Nothing) Then
        Call LoadConstraintsTable(tbl_CurrentConstraints, pGroupID)
      End If

      ' Build List of Unique Custom Field IDs in Use.

      Dim FieldIDsInUse As New Stack(Of Integer)
      Dim LastFieldID As Integer
      Dim ItemCounter As Integer

      SelectedConstraintRows = Nothing

      Try
        If (tbl_CurrentConstraints IsNot Nothing) Then
          SyncLock tbl_CurrentConstraints
            SelectedConstraintRows = tbl_CurrentConstraints.Select("ConstraintGroupID=" & pGroupID.ToString, "CustomFieldID")
          End SyncLock
        Else
          SelectedConstraintRows = Nothing
        End If

        If (SelectedConstraintRows IsNot Nothing) AndAlso (SelectedConstraintRows.Length > 0) Then
          LastFieldID = SelectedConstraintRows(0).CustomFieldID
          FieldIDsInUse.Push(LastFieldID)

          For ItemCounter = 1 To (SelectedConstraintRows.Length - 1)
            If (LastFieldID <> SelectedConstraintRows(ItemCounter).CustomFieldID) Then
              LastFieldID = SelectedConstraintRows(ItemCounter).CustomFieldID

              If (Not FieldIDsInUse.Contains(LastFieldID)) Then
                FieldIDsInUse.Push(LastFieldID)
              End If
            End If
          Next
        End If
      Catch ex As Exception
      End Try

      ' Uncheck any Custom fields which are not now in use.
      ' Grid must be Un-Checked from the bottom up because the grid is re-sorted each time the 
      ' check value is changed.

      For GridRowCounter = (Constraints_Grid_Defined.Rows.Count - 1) To 1 Step -1
        Try
          If (CBool(Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_IsDefined))) Then
            ThisCustomFieldID = CInt(Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_CustomFieldID))

            If (Not FieldIDsInUse.Contains(ThisCustomFieldID)) Then
              ' This Field is marked as defined, but is not in the Constraints Group.

              Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_IsDefined) = False

            End If
          ElseIf (CBool(Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_IsApplied))) Then
            ' If it's applied but not defined, then de-apply it.

            Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_IsApplied) = False
          End If

        Catch ex As Exception
        End Try
      Next

      ' OK, Now Work through the Defined Custom Fields Grid Setting 'Defined' & 'Applied' check marks 
      ' as necessary and refreshing existing tabs as necessary.

      While FieldIDsInUse.Count > 0
        ThisCustomFieldID = FieldIDsInUse.Pop

        For GridRowCounter = 1 To (Constraints_Grid_Defined.Rows.Count - 1)

          Try
            If ThisCustomFieldID = CInt(Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_CustomFieldID)) Then

              If (ThisCustomFieldID < 0) Then
                TabName = "Tab_" & CType(ThisCustomFieldID, SpecialConstraintFields).ToString
              Else
                TabName = "Tab_" & ThisCustomFieldID.ToString
              End If

              If (CBool(Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_IsDefined))) AndAlso (Constraints_TabConstraints.TabPages.ContainsKey(TabName)) Then
                RefreshConstraintsTabPage(Constraints_TabConstraints.TabPages(TabName), pGroupID, ThisCustomFieldID)

                If (pSetAllActive) Then
                  Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_IsApplied) = True
                End If
              Else
                ' Create New Page
                Dim NewTabPage As TabPage

                NewTabPage = ConstructConstraintsTabPage(pGroupID, ThisCustomFieldID, CInt(Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_CustomFieldDataType)), TabName, CStr(Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_CustomFieldName)))

                Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_IsDefined) = True
                Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_IsApplied) = True

                RefreshConstraintsTabPage(NewTabPage, pGroupID, ThisCustomFieldID)

                If (pSetAllActive) Then
                  Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_IsApplied) = True
                End If

              End If

              Exit For
            End If
          Catch ex As Exception
          End Try
        Next

      End While

      ' Set Tab Order

      Constraints_Grid_Defined.Sort(New ConstraintFieldGridComparer)

      ReOrderConstraintTabs()

      LastLoadedGroup = pGroupID

    Catch ex As Exception
    Finally

      ConstrainsGridChanged = False

      Constraint_Button_SaveGroup.Enabled = False
      Constraint_Button_CancelGroup.Enabled = False

      LastLoadedTime = Now()

      ' Refresh other areas. (Heineken)
      Try

        ConstraintWorker.InvalidateConstraintDetails()
        Call UpdateMarginalsGrid()

      Catch ex As Exception
      End Try

    End Try

  End Sub

  Private Function SaveConstraintsGrid(Optional ByVal pConfirm As Boolean = True) As Integer
    ' *****************************************************************
    '
    ' Notes :
    ' tbl_CurrentConstraints initially contains all existing Constraints for this Group.
    ' For Each Custom Field :
    '		Unchanged constraint records are removed from 'tbl_CurrentConstraints'
    '		Modified constraints are changed in 'tbl_CurrentConstraints'
    '		New constraints are added to 'tblNewConstraints'
    ' Finally, any unmodified record in 'tbl_CurrentConstraints' is deemed to be obsolete and deleted.
    '
    ' *****************************************************************

    Dim ConstraintHasChanged As Boolean = False
    Dim NewConstraintSetID As Integer = 0
    Dim RVal As Integer = 0

    Try

      If (ConstrainsGridChanged = False) Or (FormIsValid = False) Or (Me.IsDisposed) Then
        Return 0
        Exit Function
      End If

      ' *************************************************************
      ' Appropriate Save permission :-
      ' *************************************************************

      If (Me.HasUpdatePermission = False) And (Me.HasInsertPermission = False) Then
        MainForm.LogError(Me.Name & ", SaveConstraintsGrid()", LOG_LEVELS.Warning, "", "You do not have permission to save Constraint Sets.", "", True)
        Call btnCancel_Click(Me, New System.EventArgs)
        Return 0
        Exit Function
      End If

      ' *************************************************************
      ' If Save button is disabled then should not be able to save, exit silently.
      ' *************************************************************
      If Me.Constraint_Button_SaveGroup.Enabled = False Then
        Call Constraint_Button_CancelGroup_Click(Me, New System.EventArgs)
        Return 0
        Exit Function
      End If

      ' *************************************************************
      ' KnowledgeDate OK :-
      ' *************************************************************

      If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
        MainForm.LogError(Me.Name & ", SaveConstraintsGrid()", LOG_LEVELS.Warning, "", "Saving changes to Genoa are not allowed when the Knowledgedate is not set to `NOW`", "", True)
        Call btnCancel_Click(Me, New System.EventArgs)
        Return 0
        Exit Function
      End If

      ' Confirm Save, if required.

      If (pConfirm) AndAlso (ConstrainsGridChanged) Then
        Dim UserAnswer As System.Windows.Forms.DialogResult

        UserAnswer = MessageBox.Show("Save Constraint Changes ?", "Save Constraint Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1)

        If UserAnswer <> Windows.Forms.DialogResult.Yes Then
          ConstrainsGridChanged = False

          Return 0
          Exit Function
        End If

      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name & ", SaveConstraintsGrid()", LOG_LEVELS.Error, ex.Message, "Error in SaveConstraintsGrid().", ex.StackTrace, True)
      Return 0
    End Try

    ' *************************************************************
    ' Procedure to Save the current constraint set information.
    '
    ' 1) Get Constraint Group ID
    ' 2) Work through Selected Custom Field IDs, Saving Constraint Data
    ' 3) Contrive to delete Constraint records that are no longer in use.
    ' *************************************************************

    Try
      Dim thisConstraintGroupID As Integer
      Dim tblNewConstraints As New DSGenoaConstraintItems.tblGenoaConstraintItemsDataTable
      Dim newConstraintRow As DSGenoaConstraintItems.tblGenoaConstraintItemsRow
      Dim thisConstraintRow As DSGenoaConstraintItems.tblGenoaConstraintItemsRow

      Dim SelectedConstraints() As DSGenoaConstraintItems.tblGenoaConstraintItemsRow
      Dim DefinedGridCounter As Integer
      Dim ConstraintGridCounter As Integer
      Dim ConstraintRowCounter As Integer
      Dim thisDefinedGridRow As C1.Win.C1FlexGrid.Row
      Dim thisConstraintGridRow As C1.Win.C1FlexGrid.Row
      Dim TabName As String
      Dim ConstraintsTab As TabPage
      Dim ConstraintsGrid As C1.Win.C1FlexGrid.C1FlexGrid

      Dim ThisCustomFieldID As Integer
      Dim ThisCustomFieldDataType As Integer

      ' Custom Field Grid Columns

      Dim Col_CustomFieldID As Integer = Constraints_Grid_Defined.Cols("Col_CustomID").SafeIndex
      Dim Col_CustomFieldDataType As Integer = Constraints_Grid_Defined.Cols("Col_CustomFieldDataType").SafeIndex
      Dim Col_CustomFieldName As Integer = Constraints_Grid_Defined.Cols("Col_CustomName").SafeIndex
      Dim Col_IsDefined As Integer = Constraints_Grid_Defined.Cols("Col_IsDefined").SafeIndex
      Dim Col_IsApplied As Integer = Constraints_Grid_Defined.Cols("Col_IsApplied").SafeIndex

      thisConstraintGroupID = CurrentConstraintGroupID
      'If (Constraints_ComboConstraintsGoup.SelectedIndex > 0) AndAlso (IsNumeric(Constraints_ComboConstraintsGoup.SelectedValue)) Then
      '	thisConstraintGroupID = CInt(Constraints_ComboConstraintsGoup.SelectedValue)
      'End If


      If (thisConstraintGroupID <= 0) Then
        ' There is no Constraints Set selected.
        ' Give the user the option of creating a new one, or cancelling.

        Dim Result As DialogResult
        Result = MessageBox.Show("There is no Constraint Set selected. Save Constraints as a new set ?", "Save as New Set", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2)

        If Result = Windows.Forms.DialogResult.Yes Then
          Dim NewSetName As String = ""
          NewSetName = InputBox("Enter new set name :", "Add Constraint Set", "")

          If (NewSetName.Length > 0) Then
            NewConstraintSetID = MainForm.CreateNewConstraintSet(NewSetName)
          Else
            NewConstraintSetID = 0
          End If

          If (NewConstraintSetID > 0) Then
            thisConstraintGroupID = NewConstraintSetID
          Else
            MainForm.LogError(Me.Name & ", SaveConstraintsGrid()", LOG_LEVELS.Warning, "", "Failed to create new constraint Set.", "", False)
            Return 0
          End If

        ElseIf Result = Windows.Forms.DialogResult.Cancel Then
          Return False
        Else
          MainForm.LogError(Me.Name & ", SaveConstraintsGrid()", LOG_LEVELS.Warning, "", "Constraints Group ID appears to be Zero, unable to save Constraints.", "", False)
          ConstrainsGridChanged = False
          Call btnCancel_Click(Me, New System.EventArgs)
          Return 0
        End If

      End If

      RVal = thisConstraintGroupID

      ' Get Current Constraints for this Group

      If (tbl_CurrentConstraintsGroup <> thisConstraintGroupID) OrElse (tbl_CurrentConstraints Is Nothing) Then
        Call LoadConstraintsTable(tbl_CurrentConstraints, thisConstraintGroupID)
      End If

      If (tbl_CurrentConstraints Is Nothing) Then
        tbl_CurrentConstraints = New DSGenoaConstraintItems.tblGenoaConstraintItemsDataTable
      End If

      SyncLock tbl_CurrentConstraints

        For DefinedGridCounter = 1 To (Constraints_Grid_Defined.Rows.Count - 1)
          ConstraintsGrid = Nothing
          thisDefinedGridRow = Constraints_Grid_Defined.Rows(DefinedGridCounter)

          ' Get CustomField ID

          ThisCustomFieldID = CInt(thisDefinedGridRow(Col_CustomFieldID))
          ThisCustomFieldDataType = CInt(thisDefinedGridRow(Col_CustomFieldDataType))

          ' Select Existng constraints for this Constraint Field
          ' The rows in 'tbl_CurrentConstraints' have already been limited to this Constraint Group.

          If (ThisCustomFieldID <> 0) Then
            SelectedConstraints = tbl_CurrentConstraints.Select("CustomFieldID=" & ThisCustomFieldID.ToString, "ConstraintID")
          Else
            SelectedConstraints = Nothing
          End If

          ' 

          If (CBool(thisDefinedGridRow(Col_IsDefined))) Then

            ' RemoveDataSetRows(tbl_CurrentConstraints, SelectedConstraints)

            ' Get Constraints Tab / Grid.

            If (ThisCustomFieldID < 0) Then
              TabName = "Tab_" & CType(ThisCustomFieldID, SpecialConstraintFields).ToString
            Else
              TabName = "Tab_" & ThisCustomFieldID.ToString
            End If

            If Me.Constraints_TabConstraints.TabPages.ContainsKey(TabName) Then
              ConstraintsTab = Constraints_TabConstraints.TabPages(TabName)

              If (ConstraintsTab.Tag IsNot Nothing) Then
                Dim thisConstraintsTabDetails As ConstraintsTabDetails

                thisConstraintsTabDetails = CType(ConstraintsTab.Tag, ConstraintsTabDetails)
                ConstraintsGrid = thisConstraintsTabDetails.ConstraintsGrid
              End If
            End If

            ' OK, Process Constraints Grid.

            If (ConstraintsGrid IsNot Nothing) Then
              ' Constraints Grid Columns
              Dim ConstraintFound As Boolean

              Dim Col_Where As Integer = ConstraintsGrid.Cols("Col_Where").SafeIndex
              Dim Col_LessThan As Integer = ConstraintsGrid.Cols("Col_LessThan").SafeIndex
              Dim Col_Equal As Integer = ConstraintsGrid.Cols("Col_Equal").SafeIndex
              Dim Col_GreaterThan As Integer = ConstraintsGrid.Cols("Col_GreaterThan").SafeIndex
              Dim Col_Value As Integer = ConstraintsGrid.Cols("Col_Value").SafeIndex
              Dim Col_LowerLimit As Integer = ConstraintsGrid.Cols("Col_LowerLimit").SafeIndex
              Dim Col_UpperLimit As Integer = ConstraintsGrid.Cols("Col_UpperLimit").SafeIndex
              Dim Col_ConstraintID As Integer = ConstraintsGrid.Cols("Col_ConstraintID").SafeIndex
              Dim SelectedRowCounter As Integer

              Dim ThisConstraintID As Integer
              Dim IsLessThan As Boolean
              Dim IsEqualTo As Boolean
              Dim IsGreaterThan As Boolean
              Dim ValueString As String
              Dim ValueNumeric As Double
              Dim ValueDate As Date
              Dim LowerLimit As Double
              Dim UpperLimit As Double

              If (ConstraintsGrid.Rows.Count > 1) Then

                For ConstraintGridCounter = 1 To (ConstraintsGrid.Rows.Count - 1)
                  thisConstraintGridRow = ConstraintsGrid.Rows(ConstraintGridCounter)

                  If Not thisConstraintGridRow.IsNew Then

                    ThisConstraintID = CInt(Nz(thisConstraintGridRow(Col_ConstraintID), 0))
                    IsLessThan = Nz(thisConstraintGridRow(Col_LessThan), False)
                    IsEqualTo = Nz(thisConstraintGridRow(Col_Equal), False)
                    IsGreaterThan = Nz(thisConstraintGridRow(Col_GreaterThan), False)
                    ValueString = ""
                    ValueNumeric = 0
                    ValueDate = Renaissance_BaseDate
                    LowerLimit = Nz(thisConstraintGridRow(Col_LowerLimit), 0)
                    UpperLimit = Nz(thisConstraintGridRow(Col_UpperLimit), 0)

                    Try
                      If (ThisCustomFieldDataType And RenaissanceDataType.BooleanType) Then
                        ValueNumeric = CBool(Nz(thisConstraintGridRow(Col_Value), False))

                      ElseIf (ThisCustomFieldDataType And RenaissanceDataType.DateType) Then
                        If IsDate(thisConstraintGridRow(Col_Value)) Then
                          ValueDate = CDate(Nz(thisConstraintGridRow(Col_Value), Renaissance_BaseDate))
                        End If

                      ElseIf (ThisCustomFieldDataType And RenaissanceDataType.TextType) Then
                        ValueString = Nz(thisConstraintGridRow(Col_Value), "").ToString

                      ElseIf (ThisCustomFieldDataType And RenaissanceDataType.PercentageType) Then
                        If IsNumeric(thisConstraintGridRow(Col_Value)) Then
                          ValueNumeric = CDbl(Nz(thisConstraintGridRow(Col_Value), 0))
                        End If

                      ElseIf (ThisCustomFieldDataType And RenaissanceDataType.NumericType) Then
                        If IsNumeric(thisConstraintGridRow(Col_Value)) Then
                          ValueNumeric = CDbl(Nz(thisConstraintGridRow(Col_Value), 0))
                        End If
                      End If
                    Catch ex As Exception
                    End Try

                    ' Is this Constraint in the Existing 'Selected' Row set

                    ConstraintFound = False
                    thisConstraintRow = Nothing

                    If (SelectedConstraints IsNot Nothing) AndAlso (SelectedConstraints.Length > 0) Then
                      For SelectedRowCounter = 0 To (SelectedConstraints.Length - 1)
                        thisConstraintRow = SelectedConstraints(SelectedRowCounter)

                        If (thisConstraintRow IsNot Nothing) AndAlso (thisConstraintRow.ConstraintID = ThisConstraintID) Then
                          ConstraintFound = True
                          Exit For
                        End If
                      Next
                    End If

                    ' If this Constraint has changed then add it to 'tblNewConstraints', else Null it.

                    If ConstraintFound Then
                      If (thisConstraintRow.TestFieldLessThan = IsLessThan) AndAlso _
                       (thisConstraintRow.TestFieldEqualTo = IsEqualTo) AndAlso _
                       (thisConstraintRow.TestFieldGreaterThan = IsGreaterThan) AndAlso _
                       (thisConstraintRow.TestValueString = ValueString) AndAlso _
                       (thisConstraintRow.TestValueNumeric = ValueNumeric) AndAlso _
                       (thisConstraintRow.TestValueDate = ValueDate) AndAlso _
                       (Math.Abs(thisConstraintRow.LowerLimit - LowerLimit) < 0.0000001) AndAlso _
                       (Math.Abs(thisConstraintRow.UpperLimit - UpperLimit) < 0.0000001) Then

                        ' Constraint is Unchanged

                        tbl_CurrentConstraints.Rows.Remove(thisConstraintRow)
                        thisConstraintRow = Nothing
                        SelectedConstraints(SelectedRowCounter) = Nothing

                      Else

                        ' Constraint is Changed

                        thisConstraintRow.TestFieldLessThan = IsLessThan
                        thisConstraintRow.TestFieldEqualTo = IsEqualTo
                        thisConstraintRow.TestFieldGreaterThan = IsGreaterThan
                        thisConstraintRow.TestValueString = ValueString
                        thisConstraintRow.TestValueNumeric = ValueNumeric
                        thisConstraintRow.TestValueDate = ValueDate
                        thisConstraintRow.LowerLimit = LowerLimit
                        thisConstraintRow.UpperLimit = UpperLimit

                        SelectedConstraints(SelectedRowCounter) = Nothing
                        ' tblNewConstraints.Rows.Add(thisConstraintRow)

                        ConstraintHasChanged = True
                      End If

                    Else

                      ' New Constraint.
                      newConstraintRow = tblNewConstraints.NewtblGenoaConstraintItemsRow

                      newConstraintRow.ConstraintID = 0
                      newConstraintRow.CustomFieldID = ThisCustomFieldID
                      newConstraintRow.ConstraintGroupID = thisConstraintGroupID

                      newConstraintRow.TestFieldLessThan = IsLessThan
                      newConstraintRow.TestFieldEqualTo = IsEqualTo
                      newConstraintRow.TestFieldGreaterThan = IsGreaterThan
                      newConstraintRow.TestValueString = ValueString
                      newConstraintRow.TestValueNumeric = ValueNumeric
                      newConstraintRow.TestValueDate = ValueDate
                      newConstraintRow.LowerLimit = LowerLimit
                      newConstraintRow.UpperLimit = UpperLimit

                      tblNewConstraints.Rows.Add(newConstraintRow)

                      ConstraintHasChanged = True
                    End If

                  End If ' Not thisConstraintGridRow.IsNew

                Next ' ConstraintGridCounter

              End If ' (ConstraintsGrid.Rows.Count > 1) 

            End If ' (ConstraintsGrid IsNot Nothing)

            ' Delete Orphaned Constraints.

            If (SelectedConstraints IsNot Nothing) AndAlso (SelectedConstraints.Length > 0) Then
              For ConstraintRowCounter = 0 To (SelectedConstraints.Length - 1)
                If (SelectedConstraints(ConstraintRowCounter) IsNot Nothing) Then
                  SelectedConstraints(ConstraintRowCounter).Delete()

                  ConstraintHasChanged = True
                End If
              Next
            End If

          Else

            ' No Constraints are defined for this Custom Field within this Constraint Group.
            ' Delete any existing constraints.

            If (SelectedConstraints IsNot Nothing) AndAlso (SelectedConstraints.Length > 0) Then
              For ConstraintRowCounter = 0 To (SelectedConstraints.Length - 1)
                If (SelectedConstraints(ConstraintRowCounter) IsNot Nothing) Then
                  SelectedConstraints(ConstraintRowCounter).Delete()

                  ConstraintHasChanged = True
                End If
              Next
            End If

          End If ' CBool(thisDefinedGridRow(Col_IsDefined))

        Next

        ' Delete Redundant Constraints

        If (tbl_CurrentConstraints.Rows.Count > 0) Then
          For ConstraintRowCounter = (tbl_CurrentConstraints.Rows.Count - 1) To 0 Step -1
            If Not CBool(tbl_CurrentConstraints.Rows(ConstraintRowCounter).RowState And DataRowState.Modified) Then
              tbl_CurrentConstraints.Rows(ConstraintRowCounter).Delete()

              ConstraintHasChanged = True
            End If
          Next

          MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblGenoaConstraintItems, tbl_CurrentConstraints)
        End If

        ' Update 'tblNewConstraints'

        If (tblNewConstraints IsNot Nothing) AndAlso (tblNewConstraints.Rows.Count > 0) Then
          MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblGenoaConstraintItems, tblNewConstraints)

          ConstraintHasChanged = True
        End If

      End SyncLock

    Catch ex As Exception
    Finally
      tbl_CurrentConstraintsGroup = 0
      Try
        If (tbl_CurrentConstraints IsNot Nothing) Then
          SyncLock tbl_CurrentConstraints
            tbl_CurrentConstraints.Clear()
          End SyncLock
          tbl_CurrentConstraints = Nothing
        End If
      Catch ex As Exception
      End Try
    End Try

    ' 

    ConstrainsGridChanged = False
    Constraint_Button_SaveGroup.Enabled = False
    Constraint_Button_CancelGroup.Enabled = False

    ' Trigger AutoUpdate.

    If ConstraintHasChanged Then
      Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblGenoaConstraintItems))
    End If

    ' Move to New Constrsaint Set ?

    If (NewConstraintSetID > 0) Then
      If (Constraints_ComboConstraintsGoup.SelectedIndex <= 0) Then
        Constraints_ComboConstraintsGoup.SelectedValue = NewConstraintSetID
      End If
    End If

    Return RVal

  End Function

  Private Function ConstructConstraintsTabPage(ByVal pGroupID As Integer, ByVal pCustomFieldID As Integer, ByVal pCustomFieldDataType As Integer, ByVal pTabName As String, ByVal pTabText As String, Optional ByVal pForceToClear As Boolean = True) As TabPage
    ' *****************************************************************************
    ' 
    '
    ' *****************************************************************************

    Dim RVal As TabPage = Nothing
    Dim thisConstraintsTabDetails As ConstraintsTabDetails

    Try
      Dim NewGrid As C1.Win.C1FlexGrid.C1FlexGrid

      ' Look For Stored tab Pages
      If (ConstraintsTabPageStack.Count > 0) Then
        Dim TemplateGrid As C1.Win.C1FlexGrid.C1FlexGrid = TemplateConstraintsTabPage.Controls("Constraints_Grid_Template")

        RVal = ConstraintsTabPageStack.Pop

        If Not pForceToClear Then
          If (RVal.Name = pTabName) Then
            thisConstraintsTabDetails = RVal.Tag

            If (thisConstraintsTabDetails.ConstraintsGroupID = pGroupID) AndAlso (thisConstraintsTabDetails.CustomFieldID = pCustomFieldID) AndAlso (thisConstraintsTabDetails.CustomFieldDataType = pCustomFieldDataType) Then
              RVal.Text = pTabText
              thisConstraintsTabDetails.TabText = pTabText

              NewGrid = thisConstraintsTabDetails.ConstraintsGrid

              ' Add to TabControl

              Constraints_TabConstraints.TabPages.Add(RVal)

              NewGrid.Size = New System.Drawing.Size(RVal.ClientSize.Width - (TemplateGrid.Left + TemplateConstraintsTabPage.Padding.Right), RVal.ClientSize.Height - (TemplateGrid.Top + TemplateConstraintsTabPage.Padding.Bottom))

              Return RVal

            End If
          End If
        End If

        RVal.Name = pTabName
        RVal.Text = pTabText
        thisConstraintsTabDetails = RVal.Tag
        thisConstraintsTabDetails.TabText = pTabText
        thisConstraintsTabDetails.ConstraintsGroupID = pGroupID
        thisConstraintsTabDetails.CustomFieldID = pCustomFieldID
        thisConstraintsTabDetails.CustomFieldDataType = pCustomFieldDataType

        ' Check Grid position

        NewGrid = thisConstraintsTabDetails.ConstraintsGrid

        ' Clear Grid ?

        NewGrid.Rows.Count = 1

        ' Add to TabControl

        Constraints_TabConstraints.TabPages.Add(RVal)

        NewGrid.Size = New System.Drawing.Size(RVal.ClientSize.Width - (TemplateGrid.Left + TemplateConstraintsTabPage.Padding.Right), RVal.ClientSize.Height - (TemplateGrid.Top + TemplateConstraintsTabPage.Padding.Bottom))



      ElseIf (Constraints_TabConstraints.TabPages.ContainsKey(pTabName)) Then

        RVal = Constraints_TabConstraints.TabPages(pTabName)
        RVal.Text = pTabText

        ' Clear Grid ?

        thisConstraintsTabDetails = RVal.Tag
        thisConstraintsTabDetails.TabText = pTabText
        thisConstraintsTabDetails.ConstraintsGroupID = pGroupID
        thisConstraintsTabDetails.CustomFieldID = pCustomFieldID
        thisConstraintsTabDetails.CustomFieldDataType = pCustomFieldDataType

        NewGrid = thisConstraintsTabDetails.ConstraintsGrid
        NewGrid.Rows.Count = 1

      Else

        ' Construct New Tab Page
        NewGrid = New C1.Win.C1FlexGrid.C1FlexGrid
        Dim TemplateGrid As C1.Win.C1FlexGrid.C1FlexGrid

        Constraints_TabConstraints.TabPages.Add(pTabName, pTabName)
        RVal = Constraints_TabConstraints.TabPages(pTabName)
        RVal.Text = pTabText

        thisConstraintsTabDetails = New ConstraintsTabDetails()
        thisConstraintsTabDetails.TabText = pTabText
        thisConstraintsTabDetails.ConstraintsGroupID = pGroupID
        thisConstraintsTabDetails.CustomFieldID = pCustomFieldID
        thisConstraintsTabDetails.CustomFieldDataType = pCustomFieldDataType
        thisConstraintsTabDetails.ConstraintsGrid = NewGrid

        NewGrid.Tag = thisConstraintsTabDetails
        RVal.Tag = thisConstraintsTabDetails

        TemplateGrid = TemplateConstraintsTabPage.Controls("Constraints_Grid_Template")

        If (RVal IsNot Nothing) Then
          ' 
          TemplateGrid.Text = pTabName
          RVal.AutoScroll = TemplateConstraintsTabPage.AutoScroll
          RVal.Padding = New System.Windows.Forms.Padding(TemplateConstraintsTabPage.Padding.Top)
          RVal.UseVisualStyleBackColor = TemplateConstraintsTabPage.UseVisualStyleBackColor

          Try
            NewGrid.BeginInit()

            NewGrid.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
             Or System.Windows.Forms.AnchorStyles.Left) _
             Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
            NewGrid.Cursor = System.Windows.Forms.Cursors.Default
            NewGrid.Location = New System.Drawing.Point(TemplateGrid.Left, TemplateGrid.Top)
            NewGrid.Name = "Grid_" & pTabName
            NewGrid.Rows.DefaultSize = TemplateGrid.Rows.DefaultSize

            NewGrid.Size = New System.Drawing.Size(RVal.ClientSize.Width - (TemplateGrid.Left + TemplateConstraintsTabPage.Padding.Right), RVal.ClientSize.Height - (TemplateGrid.Top + TemplateConstraintsTabPage.Padding.Bottom))

            NewGrid.TabIndex = 0
            NewGrid.Cols.Count = TemplateGrid.Cols.Count
            NewGrid.Rows.Count = 1
            NewGrid.AutoClipboard = False
            NewGrid.AllowAddNew = TemplateGrid.AllowAddNew
            NewGrid.AllowDelete = TemplateGrid.AllowDelete
            NewGrid.AllowDragging = TemplateGrid.AllowDragging
            NewGrid.AllowSorting = TemplateGrid.AllowSorting
            NewGrid.AutoGenerateColumns = TemplateGrid.AutoGenerateColumns
            NewGrid.AutoResize = TemplateGrid.AutoResize
            NewGrid.SelectionMode = TemplateGrid.SelectionMode
            NewGrid.KeyActionTab = TemplateGrid.KeyActionTab
            NewGrid.KeyActionEnter = TemplateGrid.KeyActionEnter

            AddHandler NewGrid.KeyDown, AddressOf Grid_Constraints_KeyDown
            AddHandler NewGrid.EnterCell, AddressOf Grid_Constraints_EnterCell
            AddHandler NewGrid.LeaveCell, AddressOf Grid_Constraints_LeaveCell
            AddHandler NewGrid.AfterDeleteRow, AddressOf Grid_Constraints_AfterDeleteRow
            AddHandler NewGrid.VisibleChanged, AddressOf Grid_Constraints_VisibleChanged

            AddHandler NewGrid.CellChanged, AddressOf Grid_Constraints_CellChanged
            NewGrid.ContextMenuStrip = New ContextMenuStrip
            NewGrid.ContextMenuStrip.Tag = thisConstraintsTabDetails
            AddHandler NewGrid.ContextMenuStrip.Opening, AddressOf Grid_Constraints_cms_Opening
            NewGrid.ContextMenuStrip.Items.Add(New ToolStripMenuItem(" "))

            Try
              Dim ColCounter As Integer

              For ColCounter = 0 To (TemplateGrid.Cols.Count - 1)
                NewGrid.Cols(ColCounter).Name = TemplateGrid.Cols(ColCounter).Name
                NewGrid.Cols(ColCounter).AllowDragging = TemplateGrid.Cols(ColCounter).AllowDragging
                NewGrid.Cols(ColCounter).AllowEditing = TemplateGrid.Cols(ColCounter).AllowEditing
                NewGrid.Cols(ColCounter).AllowResizing = TemplateGrid.Cols(ColCounter).AllowResizing
                NewGrid.Cols(ColCounter).AllowSorting = TemplateGrid.Cols(ColCounter).AllowSorting
                NewGrid.Cols(ColCounter).Caption = TemplateGrid.Cols(ColCounter).Caption
                NewGrid.Cols(ColCounter).DataType = TemplateGrid.Cols(ColCounter).DataType
                NewGrid.Cols(ColCounter).Format = TemplateGrid.Cols(ColCounter).Format
                NewGrid.Cols(ColCounter).TextAlign = TemplateGrid.Cols(ColCounter).TextAlign
                NewGrid.Cols(ColCounter).Visible = TemplateGrid.Cols(ColCounter).Visible
                NewGrid.Cols(ColCounter).Width = TemplateGrid.Cols(ColCounter).Width
              Next
            Catch ex As Exception
            End Try

          Catch ex As Exception

          Finally
            NewGrid.EndInit()
          End Try

          RVal.Controls.Add(NewGrid)

        End If

      End If

      Try
        ' Set 'Value' column format consistent with the CustomFieldDataType

        NewGrid.Cols("Col_Value").DataType = DefaultDataTypeType(pCustomFieldDataType)
        NewGrid.Cols("Col_Value").Format = DefaultDataTypeFormat(pCustomFieldDataType)

      Catch ex As Exception
      End Try

    Catch ex As Exception
    End Try

    Return RVal

  End Function

  Private Sub DestructConstraintsTabPage(ByRef pTabPage As TabPage)
    ' *****************************************************************************
    ' De-Construct a Constraints Tab Page.
    '
    ' This may not be necessary, but it may help the garbage collection and is probably
    ' good practice.
    ' *****************************************************************************

    Dim NewGrid As C1.Win.C1FlexGrid.C1FlexGrid
    Dim thisConstraintsTabDetails As ConstraintsTabDetails

    Try
      If (pTabPage.Tag IsNot Nothing) Then
        thisConstraintsTabDetails = CType(pTabPage.Tag, ConstraintsTabDetails)

        NewGrid = thisConstraintsTabDetails.ConstraintsGrid
        NewGrid.Tag = Nothing
        pTabPage.Tag = Nothing

        Try
          RemoveHandler NewGrid.KeyDown, AddressOf Grid_Constraints_KeyDown
          RemoveHandler NewGrid.CellChanged, AddressOf Grid_Constraints_CellChanged
          RemoveHandler NewGrid.EnterCell, AddressOf Grid_Constraints_EnterCell
          RemoveHandler NewGrid.LeaveCell, AddressOf Grid_Constraints_LeaveCell
          RemoveHandler NewGrid.AfterDeleteRow, AddressOf Grid_Constraints_AfterDeleteRow

          RemoveHandler NewGrid.ContextMenuStrip.Opening, AddressOf Grid_Constraints_cms_Opening
          NewGrid.ContextMenuStrip = Nothing
        Catch ex As Exception
        End Try
        Try
          pTabPage.Controls.Remove(NewGrid)
        Catch ex As Exception
        End Try
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub RefreshConstraintsTabPage(ByRef pConstraintsTab As TabPage, ByVal pGroupID As Integer, ByVal pCustomFieldID As Integer)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************
    Dim thisConstraintsTabDetails As ConstraintsTabDetails
    Dim thisGrid As C1.Win.C1FlexGrid.C1FlexGrid = Nothing
    Dim SelectedConstraintRows() As DSGenoaConstraintItems.tblGenoaConstraintItemsRow
    Dim thisTableRow As DSGenoaConstraintItems.tblGenoaConstraintItemsRow
    Dim thisGridRow As C1.Win.C1FlexGrid.Row
    Dim RowCount As Integer
    Dim OrgAllowAddNew As Boolean = False

    Try

      ' Get Current Constraints for this Group

      If (tbl_CurrentConstraintsGroup <> pGroupID) OrElse (tbl_CurrentConstraints Is Nothing) Then
        Call LoadConstraintsTable(tbl_CurrentConstraints, pGroupID)
      End If

      thisConstraintsTabDetails = pConstraintsTab.Tag
      thisGrid = thisConstraintsTabDetails.ConstraintsGrid
      OrgAllowAddNew = thisGrid.AllowAddNew
      thisGrid.AllowAddNew = False

      If (tbl_CurrentConstraints IsNot Nothing) Then
        SyncLock tbl_CurrentConstraints
          SelectedConstraintRows = tbl_CurrentConstraints.Select("CustomFieldID=" & pCustomFieldID.ToString, "TestValueString,TestValueNumeric,TestValueDate")
        End SyncLock
      Else
        SelectedConstraintRows = Nothing
      End If

      ' Exit if there are no constraints to display.

      If (SelectedConstraintRows Is Nothing) OrElse (SelectedConstraintRows.Length <= 0) Then
        thisGrid.Rows.Count = 1
        Exit Sub
      End If

      ' OK, Set data

      Dim Col_Where As Integer = thisGrid.Cols("Col_Where").SafeIndex
      Dim Col_LessThan As Integer = thisGrid.Cols("Col_LessThan").SafeIndex
      Dim Col_Equal As Integer = thisGrid.Cols("Col_Equal").SafeIndex
      Dim Col_GreaterThan As Integer = thisGrid.Cols("Col_GreaterThan").SafeIndex
      Dim Col_Value As Integer = thisGrid.Cols("Col_Value").SafeIndex
      Dim Col_LowerLimit As Integer = thisGrid.Cols("Col_LowerLimit").SafeIndex
      Dim Col_UpperLimit As Integer = thisGrid.Cols("Col_UpperLimit").SafeIndex
      Dim Col_ConstraintID As Integer = thisGrid.Cols("Col_ConstraintID").SafeIndex

      thisGrid.Rows.Count = SelectedConstraintRows.Length + 1

      For RowCount = 0 To (SelectedConstraintRows.Length - 1)
        thisTableRow = SelectedConstraintRows(RowCount)
        thisGridRow = thisGrid.Rows(RowCount + 1)

        thisGridRow(Col_Where) = thisConstraintsTabDetails.TabText
        thisGridRow(Col_LessThan) = thisTableRow.TestFieldLessThan
        thisGridRow(Col_Equal) = thisTableRow.TestFieldEqualTo
        thisGridRow(Col_GreaterThan) = thisTableRow.TestFieldGreaterThan

        Try
          If (thisConstraintsTabDetails.CustomFieldDataType And RenaissanceDataType.BooleanType) Then
            thisGridRow(Col_Value) = CBool(thisTableRow.TestValueNumeric)

          ElseIf (thisConstraintsTabDetails.CustomFieldDataType And RenaissanceDataType.DateType) Then
            thisGridRow(Col_Value) = thisTableRow.TestValueDate

          ElseIf (thisConstraintsTabDetails.CustomFieldDataType And RenaissanceDataType.TextType) Then
            thisGridRow(Col_Value) = thisTableRow.TestValueString

          ElseIf (thisConstraintsTabDetails.CustomFieldDataType And RenaissanceDataType.PercentageType) Then
            thisGridRow(Col_Value) = thisTableRow.TestValueNumeric

          ElseIf (thisConstraintsTabDetails.CustomFieldDataType And RenaissanceDataType.NumericType) Then
            thisGridRow(Col_Value) = thisTableRow.TestValueNumeric

          Else
            thisGridRow(Col_Value) = Nz(Nothing, thisGrid.Cols(Col_Value).DataType)
          End If

        Catch ex As Exception
        End Try

        thisGridRow(Col_LowerLimit) = thisTableRow.LowerLimit
        thisGridRow(Col_UpperLimit) = thisTableRow.UpperLimit
        thisGridRow(Col_ConstraintID) = thisTableRow.ConstraintID

      Next

    Catch ex As Exception
    Finally
      Try
        If (thisGrid IsNot Nothing) Then
          thisGrid.AllowAddNew = OrgAllowAddNew
        End If
      Catch ex As Exception
      End Try
    End Try

  End Sub

  Private Sub ReOrderConstraintTabs()
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try
      If (Constraints_TabConstraints.TabPages.Count <= 0) Then
        Exit Sub
      End If
    Catch ex As Exception
    End Try

    Try
      Dim Col_CustomFieldID As Integer = Constraints_Grid_Defined.Cols("Col_CustomID").SafeIndex
      Dim Col_IsDefined As Integer = Constraints_Grid_Defined.Cols("Col_IsDefined").SafeIndex
      Dim ThisCustomFieldID As Integer
      Dim GridRowCounter As Integer
      Dim TabName As String
      Dim TabCount As Integer = 0
      Dim TempTabArray(Constraints_TabConstraints.TabPages.Count - 1) As TabPage
      Dim SelectedTab As TabPage

      SelectedTab = Constraints_TabConstraints.SelectedTab

      For GridRowCounter = 1 To (Constraints_Grid_Defined.Rows.Count - 1)
        Try
          If (CBool(Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_IsDefined))) Then
            ThisCustomFieldID = CInt(Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_CustomFieldID))

            If (ThisCustomFieldID < 0) Then
              TabName = "Tab_" & CType(ThisCustomFieldID, SpecialConstraintFields).ToString
            Else
              TabName = "Tab_" & ThisCustomFieldID.ToString
            End If

            If (Constraints_TabConstraints.TabPages.ContainsKey(TabName)) Then
              If (TabCount < TempTabArray.Length) Then
                TempTabArray(TabCount) = Constraints_TabConstraints.TabPages(TabName)
                TabCount += 1
              End If
            End If

          End If
        Catch ex As Exception
        End Try
      Next

      '
      If (TabCount = Constraints_TabConstraints.TabPages.Count) Then
        For TabCount = 0 To (TempTabArray.Length - 1)
          Constraints_TabConstraints.TabPages.Item(TabCount) = TempTabArray(TabCount)
        Next
      End If

      If (SelectedTab Is Nothing) Then
        If (Constraints_TabConstraints.TabPages.Count > 0) Then
          Constraints_TabConstraints.SelectedIndex = 0
        End If
      Else
        Constraints_TabConstraints.SelectedTab = SelectedTab
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub LoadConstraintsTable(ByRef pConstraintsTable As DSGenoaConstraintItems.tblGenoaConstraintItemsDataTable, ByVal pGroupID As Integer)
    ' *****************************************************************************
    '
    '
    ' *****************************************************************************

    Try
      If (Not Me.Created) OrElse (Me.IsDisposed) Then
        Exit Sub
      End If
    Catch ex As Exception
      Exit Sub
    End Try

    If (pConstraintsTable Is Nothing) Then
      pConstraintsTable = New DSGenoaConstraintItems.tblGenoaConstraintItemsDataTable
    End If

    Try
      Dim tmpCommand As New SqlCommand

      SyncLock pConstraintsTable
        Try

          tmpCommand.CommandType = CommandType.StoredProcedure
          tmpCommand.CommandText = "adp_tblGenoaConstraintItems_SelectGroup"
          tmpCommand.Connection = myConnection ' Main Genoa Connection (Better performance?)
          tmpCommand.Parameters.Add("@ConstraintGroupID", SqlDbType.Int).Value = pGroupID
          tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
          tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

          pConstraintsTable.Clear()

          If (tmpCommand.Connection IsNot Nothing) Then
            SyncLock tmpCommand.Connection
              pConstraintsTable.Load(tmpCommand.ExecuteReader)
            End SyncLock
          End If

        Catch ex As Exception

          Call MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error loading Constraint Items", ex.StackTrace, True)

        Finally
          tmpCommand.Connection = Nothing
          tmpCommand = Nothing
        End Try
      End SyncLock

      tbl_CurrentConstraintsGroup = pGroupID

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Grid_Constraints_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) ' Handles Grid_Performance_0.KeyDown
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************
    Try
      If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False)
      End If
    Catch ex As Exception
    End Try
  End Sub

  Private Sub Grid_Constraints_EnterCell(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      Dim ThisGrid As C1.Win.C1FlexGrid.C1FlexGrid = CType(sender, C1.Win.C1FlexGrid.C1FlexGrid)

      If (ThisGrid.Row > 0) AndAlso (ThisGrid.Col >= 0) AndAlso (ThisGrid.Row < ThisGrid.Rows.Count) AndAlso (ThisGrid.Col < ThisGrid.Cols.Count) Then
        If ThisGrid.Cols(ThisGrid.Col).AllowEditing Then
          ThisGrid.SetCellStyle(ThisGrid.Row, ThisGrid.Col, Grid_Returns.Styles("Genoa_PlainEdit"))
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Grid_Constraints_LeaveCell(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      Dim ThisGrid As C1.Win.C1FlexGrid.C1FlexGrid = CType(sender, C1.Win.C1FlexGrid.C1FlexGrid)

      If (ThisGrid.Row > 0) AndAlso (ThisGrid.Col >= 0) AndAlso (ThisGrid.Row < ThisGrid.Rows.Count) AndAlso (ThisGrid.Col < ThisGrid.Cols.Count) Then
        If ThisGrid.Cols(ThisGrid.Col).AllowEditing Then
          ThisGrid.SetCellStyle(ThisGrid.Row, ThisGrid.Col, ThisGrid.Cols(ThisGrid.Col).Style)
        End If
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Grid_Constraints_AfterDeleteRow(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      Dim ThisGrid As C1.Win.C1FlexGrid.C1FlexGrid = CType(sender, C1.Win.C1FlexGrid.C1FlexGrid)

      If (ThisGrid.Focused) Then
        Set_ConstrainsGridChanged()
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Grid_Constraints_VisibleChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      Dim ThisGrid As C1.Win.C1FlexGrid.C1FlexGrid = CType(sender, C1.Win.C1FlexGrid.C1FlexGrid)

      If (ThisGrid.Focused) Then

        CalculateConstraintExposures()

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Grid_Constraints_CellChanged(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************
    Dim ReCalculateConstraintExposures As Boolean = False

    If (e.Row <= 0) Then
      Exit Sub
    End If

    Dim ThisGrid As C1.Win.C1FlexGrid.C1FlexGrid = Nothing
    Dim thisConstraintsTabDetails As ConstraintsTabDetails = Nothing
    Dim ThisRow As C1.Win.C1FlexGrid.Row = Nothing

    Try
      ThisGrid = CType(sender, C1.Win.C1FlexGrid.C1FlexGrid)

      If (Not ThisGrid.Focused) Then
        Exit Sub
      End If

      Dim Col_Where As Integer = ThisGrid.Cols("Col_Where").SafeIndex
      Dim Col_LessThan As Integer = ThisGrid.Cols("Col_LessThan").SafeIndex
      Dim Col_Equal As Integer = ThisGrid.Cols("Col_Equal").SafeIndex
      Dim Col_GreaterThan As Integer = ThisGrid.Cols("Col_GreaterThan").SafeIndex
      Dim Col_Value As Integer = ThisGrid.Cols("Col_Value").SafeIndex
      Dim Col_LowerLimit As Integer = ThisGrid.Cols("Col_LowerLimit").SafeIndex
      Dim Col_UpperLimit As Integer = ThisGrid.Cols("Col_UpperLimit").SafeIndex
      Dim Col_ConstraintID As Integer = ThisGrid.Cols("Col_ConstraintID").SafeIndex

      thisConstraintsTabDetails = CType(ThisGrid.Tag, ConstraintsTabDetails)

      ThisRow = ThisGrid.Rows(e.Row)

      ' Will we need to Re Calculate the constraint exposure ?

      Select Case e.Col
        Case Col_Value, Col_LessThan, Col_Equal, Col_GreaterThan
          ReCalculateConstraintExposures = True

      End Select

      ' Check Default ConstraintID

      If (Not IsNumeric(ThisRow(Col_ConstraintID))) Then
        ThisRow(Col_ConstraintID) = 0
      End If

      ' Check 'Where' is present.

      If (Nz(ThisRow(Col_Where), "").ToString <> thisConstraintsTabDetails.TabText) Then
        ThisRow(Col_Where) = thisConstraintsTabDetails.TabText
      End If

      ' Check Default Values

      If (Not IsNumeric(ThisRow(Col_LowerLimit))) Then
        ThisRow(Col_LowerLimit) = 0
      End If

      If (Not IsNumeric(ThisRow(Col_UpperLimit))) Then
        ThisRow(Col_UpperLimit) = 1
      End If

      If (ThisRow(Col_Value) Is Nothing) Then
        Select Case thisConstraintsTabDetails.CustomFieldDataType
          Case RenaissanceDataType.BooleanType
            ThisRow(Col_Value) = False

          Case RenaissanceDataType.DateType
            ThisRow(Col_Value) = Now.Date

          Case RenaissanceDataType.NumericType
            ThisRow(Col_Value) = 0

          Case RenaissanceDataType.PercentageType
            ThisRow(Col_Value) = 0

          Case RenaissanceDataType.TextType
            ThisRow(Col_Value) = ""

        End Select
      End If

    Catch ex As Exception
    Finally

      Set_ConstrainsGridChanged()

      Try
        If (ReCalculateConstraintExposures) AndAlso (ThisGrid IsNot Nothing) AndAlso (ThisRow IsNot Nothing) Then
          CalculateConstraintExposures(ThisGrid, ThisRow, False)
        End If
      Catch ex As Exception
      End Try
    End Try

  End Sub


  Private Sub Constraints_TabConstraints_VisibleChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Constraints_TabConstraints.VisibleChanged
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      If (Constraints_TabConstraints.Visible) Then

        ConstraintWorker.RefreshAllData()

        CalculateConstraintExposures()

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Constraints_TabConstraints_TabIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Constraints_TabConstraints.TabIndexChanged
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      If (Constraints_TabConstraints.Visible) Then

        CalculateConstraintExposures()

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Constraints_TabConstraints_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Constraints_TabConstraints.SelectedIndexChanged
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      If (Constraints_TabConstraints.Visible) Then

        CalculateConstraintExposures()

      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Constraint_Button_AllActive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Constraint_Button_AllActive.Click
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      Dim Col_IsDefined As Integer = Constraints_Grid_Defined.Cols("Col_IsDefined").SafeIndex
      Dim Col_IsApplied As Integer = Constraints_Grid_Defined.Cols("Col_IsApplied").SafeIndex
      Dim GridRowCounter As Integer

      For GridRowCounter = 1 To (Constraints_Grid_Defined.Rows.Count - 1)
        Try
          If CBool(Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_IsDefined)) Then
            If Not CBool(Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_IsApplied)) Then
              Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_IsApplied) = True
            End If
          End If
        Catch ex As Exception
        End Try
      Next

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Constraint_Button_NoneActive_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Constraint_Button_NoneActive.Click
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      Dim Col_IsDefined As Integer = Constraints_Grid_Defined.Cols("Col_IsDefined").SafeIndex
      Dim Col_IsApplied As Integer = Constraints_Grid_Defined.Cols("Col_IsApplied").SafeIndex
      Dim GridRowCounter As Integer

      For GridRowCounter = 1 To (Constraints_Grid_Defined.Rows.Count - 1)
        Try
          If CBool(Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_IsDefined)) Then
            If CBool(Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_IsApplied)) Then
              Constraints_Grid_Defined.Rows(GridRowCounter).Item(Col_IsApplied) = False
            End If
          End If
        Catch ex As Exception
        End Try
      Next
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Constraint_Button_NewGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Constraint_Button_NewGroup.Click
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      Dim NewSetName As String = ""

      If MessageBox.Show("Create a new Constraints set ?", "Create Constraints Set", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
        NewSetName = InputBox("Enter new set name :", "Add Constraint Set", "")

        If (NewSetName.Length > 0) Then

          Dim NewConstraintSetID As Integer

          NewConstraintSetID = MainForm.CreateNewConstraintSet(NewSetName)

          SetConstraintSetsCombo()

          Constraints_ComboConstraintsGoup.SelectedValue = NewConstraintSetID

        End If
      End If

    Catch ex As Exception

    End Try

  End Sub

  Private Sub Constraint_Button_NewGroup_Copy_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Constraint_Button_NewGroup_Copy.Click
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      Dim NewSetName As String = ""
      Dim ThisConstraintsGroupID As Integer = 0

      If MessageBox.Show("Create a new Constraints set ?", "Create Constraints Set", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
        NewSetName = InputBox("Enter new set name :", "Add Constraint Set", "")

        If (NewSetName.Length > 0) Then

          ' Get Existing Group ID

          If (Constraints_ComboConstraintsGoup.SelectedIndex > 0) AndAlso (IsNumeric(Constraints_ComboConstraintsGoup.SelectedValue)) Then
            ThisConstraintsGroupID = CInt(Constraints_ComboConstraintsGoup.SelectedValue)
          End If

          ' Create New Constraint Set

          Dim NewConstraintSetID As Integer

          NewConstraintSetID = MainForm.CreateNewConstraintSet(NewSetName)

          SetConstraintSetsCombo()

          Constraints_ComboConstraintsGoup.SelectedValue = NewConstraintSetID

          ' Copy Data

          If (ThisConstraintsGroupID > 0) Then
            MainForm.CopyConstraintSet(ThisConstraintsGroupID, NewConstraintSetID)
          End If

          Constraints_ComboConstraintsGoup.SelectedValue = NewConstraintSetID
        End If
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Constraint_Button_RenameGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Constraint_Button_RenameGroup.Click
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      Dim NewSetName As String = ""
      Dim ThisConstraintsGroupID As Integer = 0

      ' Get Existing Group ID

      If (Constraints_ComboConstraintsGoup.SelectedIndex > 0) AndAlso (IsNumeric(Constraints_ComboConstraintsGoup.SelectedValue)) Then
        ThisConstraintsGroupID = CInt(Constraints_ComboConstraintsGoup.SelectedValue)
      End If

      If (ThisConstraintsGroupID <= 0) Then
        MessageBox.Show("No Constraint set is currently selected", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
      Else
        If MessageBox.Show("Rename the current Constraints set ?", "Rename Constraints Set", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then
          NewSetName = InputBox("Enter new set name :", "Rename Constraint Set", "")

          If (NewSetName.Length > 0) Then

            MainForm.RenameConstraintSet(ThisConstraintsGroupID, NewSetName)

          End If
        End If
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Constraint_Button_DeleteGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Constraint_Button_DeleteGroup.Click
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      Dim ThisConstraintsGroupID As Integer = 0

      ' Get Existing Group ID

      If (Constraints_ComboConstraintsGoup.SelectedIndex > 0) AndAlso (IsNumeric(Constraints_ComboConstraintsGoup.SelectedValue)) Then
        ThisConstraintsGroupID = CInt(Constraints_ComboConstraintsGoup.SelectedValue)
      End If

      If (ThisConstraintsGroupID <= 0) Then
        MessageBox.Show("No Constraint set is currently selected", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information)
      Else
        If MessageBox.Show("Delete the current Constraints set ?", "Delete Constraints Set", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.Yes Then

          MainForm.DeleteConstraintSet(ThisConstraintsGroupID)

          Constraints_ComboConstraintsGoup.SelectedIndex = 0
          LoadConstraintsGroup(0)
        End If
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub Constraint_Button_SaveGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Constraint_Button_SaveGroup.Click
    ' *****************************************************************************************
    ' Save Changed Constraints, if any, without prompting.
    '
    ' *****************************************************************************************

    If (ConstrainsGridChanged = True) Then
      Call SaveConstraintsGrid(False)
    ElseIf Constraint_Button_SaveGroup.Enabled Then
      ConstrainsGridChanged = False
      Constraint_Button_SaveGroup.Enabled = False
      Constraint_Button_CancelGroup.Enabled = False
    End If

  End Sub

  Private Sub Constraint_Button_CancelGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Constraint_Button_CancelGroup.Click
    ' *****************************************************************************************
    ' Cancel Changes, Redisplay form.
    '
    ' *****************************************************************************************

    ConstrainsGridChanged = False

    Try
      If (Me.Created) AndAlso (Me.IsDisposed = False) Then

        If (Constraints_ComboConstraintsGoup.SelectedIndex < 0) OrElse (Constraints_ComboConstraintsGoup.SelectedValue Is Nothing) OrElse (IsNumeric(Constraints_ComboConstraintsGoup.SelectedValue) = False) Then
          LoadConstraintsGroup(0)
        Else
          LoadConstraintsGroup(CInt(Constraints_ComboConstraintsGoup.SelectedValue))
        End If
      End If
    Catch ex As Exception
    End Try

  End Sub

  Sub Grid_Constraints_cms_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************
    Dim thisContextMenu As ContextMenuStrip
    Dim thisConstraintsTabDetails As ConstraintsTabDetails
    Dim ThisGrid As C1.Win.C1FlexGrid.C1FlexGrid

    Try
      thisContextMenu = CType(sender, ContextMenuStrip)
      thisConstraintsTabDetails = thisContextMenu.Tag
      ThisGrid = thisConstraintsTabDetails.ConstraintsGrid

      ' Clear the ContextMenuStrip control's 
      ' Items collection.
      thisContextMenu.Items.Clear()

      If (ThisGrid.MouseRow <= 0) Then
        Exit Sub
      End If

      ' Save Mouse Location

      thisConstraintsTabDetails.GridClickMouseRow = ThisGrid.MouseRow
      thisConstraintsTabDetails.GridClickMouseCol = ThisGrid.MouseCol

      Dim Col_Value As Integer = ThisGrid.Cols("Col_Value").SafeIndex

      Select Case thisConstraintsTabDetails.GridClickMouseCol
        Case Col_Value
          ' If this Custom Field is of type 'String', Present the user with a drop-Down list of values

          If thisConstraintsTabDetails.CustomFieldDataType = RenaissanceDataType.TextType Then
            ' Show DrowDown List of values

            Dim StringValueArray() As String = Nothing

            If (thisConstraintsTabDetails.CustomFieldID = SpecialConstraintFields.Sector) Then
              StringValueArray = GetDistinctSectorValues()
            ElseIf thisConstraintsTabDetails.CustomFieldID > 0 Then
              StringValueArray = GetDistinctCustomFieldStrings(thisConstraintsTabDetails.CustomFieldID)
            End If

            If (StringValueArray IsNot Nothing) AndAlso (StringValueArray.Length > 0) Then
              Dim StringCounter As Integer
              Dim NewItem As System.Windows.Forms.ToolStripItem

              For StringCounter = 0 To (StringValueArray.Length - 1)
                NewItem = thisContextMenu.Items.Add(StringValueArray(StringCounter), Nothing, AddressOf Constraint_SetValueString)
                NewItem.Tag = thisConstraintsTabDetails
              Next
            End If

            e.Cancel = False
          End If

        Case Else


      End Select

    Catch ex As Exception

    End Try


  End Sub

  Private Sub Constraint_SetValueString(ByVal sender As Object, ByVal e As System.EventArgs)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try
      Dim ThisItem As System.Windows.Forms.ToolStripItem
      Dim thisConstraintsTabDetails As ConstraintsTabDetails

      ThisItem = CType(sender, System.Windows.Forms.ToolStripItem)
      thisConstraintsTabDetails = CType(ThisItem.Tag, ConstraintsTabDetails)

      If (thisConstraintsTabDetails.GridClickMouseRow > 0) AndAlso (thisConstraintsTabDetails.GridClickMouseCol >= 0) AndAlso (thisConstraintsTabDetails.GridClickMouseRow < thisConstraintsTabDetails.ConstraintsGrid.Rows.Count) AndAlso (thisConstraintsTabDetails.GridClickMouseCol < thisConstraintsTabDetails.ConstraintsGrid.Cols.Count) Then
        thisConstraintsTabDetails.ConstraintsGrid.Item(thisConstraintsTabDetails.GridClickMouseRow, thisConstraintsTabDetails.GridClickMouseCol) = ThisItem.Text
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Function GetDistinctSectorValues() As String()
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************
    Dim RVal() As String = Nothing

    Try
      Dim TmpStringList As New Stack(Of String)
      Dim GridRowCount As Integer
      Dim ThisRow As C1.Win.C1FlexGrid.Row
      Dim ThisSector As String
      Dim Col_GroupSector As Integer = Grid_Returns.Cols("GroupSector").SafeIndex

      For GridRowCount = 1 To (Me.Grid_Returns.Rows.Count - 1)
        ThisRow = Grid_Returns.Rows(GridRowCount)
        ThisSector = Nz(ThisRow(Col_GroupSector), "").ToString
        If Not TmpStringList.Contains(ThisSector) Then
          TmpStringList.Push(ThisSector)
        End If
      Next

      RVal = TmpStringList.ToArray

      If (RVal IsNot Nothing) AndAlso (RVal.Length > 0) Then
        Array.Sort(RVal)
      End If

    Catch ex As Exception
    End Try

    Return RVal

  End Function

  Private Function GetDistinctCustomFieldStrings(ByVal pCustomFieldID As Integer) As String()
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim RVal() As String = Nothing
    Dim SelectString As String = ""
    Dim tblCustomSelection As New DataTable
    Dim RowCount As Integer
    Dim ThisItem As String

    Try
      SelectString = "SELECT DISTINCT [FieldTextData] AS DataField FROM tblPertracCustomFieldData WHERE (CustomFieldID=@CustomFieldID) AND (NOT ([FieldTextData] = ''))"

      Dim tmpCommand As New SqlCommand
      Dim TmpStringList As New Stack(Of String)
      TmpStringList.Push("")

      Try
        tmpCommand.CommandType = CommandType.Text
        tmpCommand.CommandText = SelectString
        tmpCommand.Parameters.Add("@CustomFieldID", SqlDbType.Int).Value = pCustomFieldID

        tmpCommand.Connection = myConnection
        tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

        SyncLock tmpCommand.Connection
          tblCustomSelection.Load(tmpCommand.ExecuteReader)
        End SyncLock

      Catch ex As Exception
      Finally
        tmpCommand.Connection = Nothing
      End Try

      If (tblCustomSelection.Rows.Count > 0) Then
        For RowCount = 0 To (tblCustomSelection.Rows.Count - 1)

          ThisItem = Nz(tblCustomSelection.Rows(RowCount)(0), "").ToString

          If Not TmpStringList.Contains(ThisItem) Then
            TmpStringList.Push(ThisItem)
          End If
        Next
      End If

      RVal = TmpStringList.ToArray

      If (RVal IsNot Nothing) AndAlso (RVal.Length > 0) Then
        Array.Sort(RVal)
      End If

    Catch ex As Exception
    End Try

    Return RVal

  End Function

  Private Function DefaultDataTypeType(ByVal pCustomFieldDataType As RenaissanceDataType) As Type
    ' *****************************************************************************************
    '
    ' Used to format the 'Value' column on the Constraints Grids.
    ' *****************************************************************************************

    If (pCustomFieldDataType And RenaissanceDataType.BooleanType) Then
      Return GetType(Boolean)

    ElseIf (pCustomFieldDataType And RenaissanceDataType.DateType) Then
      Return GetType(Date)

    ElseIf (pCustomFieldDataType And RenaissanceDataType.TextType) Then
      Return GetType(String)

    ElseIf (pCustomFieldDataType And RenaissanceDataType.PercentageType) Then
      Return GetType(Double)

    ElseIf (pCustomFieldDataType And RenaissanceDataType.NumericType) Then
      Return GetType(Double)

    Else
      Return GetType(Object)

    End If

    Return GetType(Object)

  End Function

  Private Function DefaultDataTypeFormat(ByVal pCustomFieldDataType As RenaissanceDataType) As String
    ' *****************************************************************************************
    '
    ' Used to format the 'Value' column on the Constraints Grids.
    ' *****************************************************************************************

    If (pCustomFieldDataType And RenaissanceDataType.BooleanType) Then
      Return ""

    ElseIf (pCustomFieldDataType And RenaissanceDataType.DateType) Then
      Return DISPLAYMEMBER_DATEFORMAT

    ElseIf (pCustomFieldDataType And RenaissanceDataType.TextType) Then
      Return ""

    ElseIf (pCustomFieldDataType And RenaissanceDataType.PercentageType) Then
      Return "#,##0.00##%"

    ElseIf (pCustomFieldDataType And RenaissanceDataType.NumericType) Then
      Return "#,##0.0###"

    Else
      Return ""

    End If

    Return ""

  End Function

#End Region

#Region " Optimisation Tab Code"

  Private Sub Grid_OptimisationResults_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_OptimisationResults.KeyDown
    ' *************************************************************************************
    '
    '
    '
    ' *************************************************************************************
    Try
      If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False)
      End If
    Catch ex As Exception
    End Try

  End Sub

  Private Sub Btn_RunOptimisation_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_RunOptimisation.Click
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Try

      Me.Grid_OptimisationResults.Rows.Count = 1
      Grid_OptimisationResults.Tag = Nothing

      thisOptimiser.SaveDebugInfo = False

      If thisOptimiser.InitialiseOptimiser(Me.ConstraintWorker, CheckBox_RunUnconstrained.Checked) Then

        thisOptimiser.SaveDebugInfo = False
        thisOptimiser.UseZeroWeightFix = CheckBox_ZeroWeightOptimiserFix.Checked
        thisOptimiser.UseMarginalsIteration = checkbox_UseMarginalsIteration.Checked
        If (checkbox_UseMarginalsIteration.Checked) Then
          thisOptimiser.MAX_MarginalsMoveCount = Edit_MarginalsMoveCount.Value
        End If

        ' Set Max Corner Portfolio Count & ELambdaE value.

        If (Optimisation_CpLimit.Value > 0) Then
          thisOptimiser.MaxCPs = CInt(Optimisation_CpLimit.Value)
        End If

        If (Optimisation_ELambdaE.Value > 0) Then
          thisOptimiser.ELambdaE = CDbl(Optimisation_ELambdaE.Value)
        End If

        ' Run Optimiser

        thisOptimiser.Optimise()

        ' Get Results

        Dim OptimiserSnapshot As OptimiserDataClass
        OptimiserSnapshot = thisOptimiser.GetOptimisationData

        Grid_OptimisationResults.Tag = OptimiserSnapshot

        DisplayOptimisationResults(OptimiserSnapshot)

        AddOptimisationChartArray(OptimiserSnapshot)

        PaintResultsCharts()

        If (Optimisation_Check_SaveResultsToFile.Checked) Then
          thisOptimiser.ExportOptimisationParameters(OptimiserSnapshot)
        End If

      Else

        MainForm.LogError(Me.Name & ", Btn_RunOptimisation_Click()", LOG_LEVELS.Warning, "", "Optimiser Initialisation failed." & vbCrLf & thisOptimiser.StatusString, "", True)

      End If

    Catch ex As Exception

      MainForm.LogError(Me.Name & ", Btn_RunOptimisation_Click()", LOG_LEVELS.Warning, ex.Message, "Error in Optimisation process." & vbCrLf & thisOptimiser.StatusString, ex.StackTrace, True)
      If (Grid_OptimisationResults.Rows.Count > 1) Then
        Grid_OptimisationResults.Rows.Count = 1
      End If

    End Try


  End Sub

  Private Sub AddOptimisationChartArray(Optional ByVal pOptimiserSnapshot As OptimiserDataClass = Nothing)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim OptimiserSnapshot As OptimiserDataClass
    Dim OptimisationChartPoints As New ArrayList
    Dim RowIndex As Integer
    Dim ThisPoint As DecimalPoint

    Try

      If (pOptimiserSnapshot Is Nothing) OrElse (pOptimiserSnapshot.ResultsSet = False) Then
        OptimiserSnapshot = thisOptimiser.GetOptimisationData
      Else
        OptimiserSnapshot = pOptimiserSnapshot
      End If

      ' 
      For RowIndex = 1 To OptimiserSnapshot.CornerPortfolioCount
        ThisPoint = New DecimalPoint(OptimiserSnapshot.CP_StandardDeviation(RowIndex) * MainForm.StatFunctions.Sqrt12(DefaultStatsDatePeriod), OptimiserSnapshot.CP_ExpectedReturn(RowIndex))

        OptimisationChartPoints.Add(ThisPoint)
      Next

      OptimisationArrayLists.Add(OptimisationChartPoints)

    Catch ex As Exception
    End Try

  End Sub

  Private Sub DisplayOptimisationResults(Optional ByVal pOptimiserSnapshot As OptimiserDataClass = Nothing)
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    Dim OptimiserSnapshot As OptimiserDataClass

    Try

      If (pOptimiserSnapshot Is Nothing) OrElse (pOptimiserSnapshot.ResultsSet = False) Then
        OptimiserSnapshot = thisOptimiser.GetOptimisationData
      Else
        OptimiserSnapshot = pOptimiserSnapshot
      End If

      ' Initialise Grid

      Dim InstrumentIndex As Integer
      Dim ColumnIndex As Integer
      Dim RowIndex As Integer
      Dim ThisRow As C1.Win.C1FlexGrid.Row

      ' Set Column and Row Counts

      If (Grid_OptimisationResults.Cols.Count <> (OptimiserSnapshot.NumberOfRealSecurities + 4)) Then
        Grid_OptimisationResults.Cols.Count = (OptimiserSnapshot.NumberOfRealSecurities + 4)
      End If

      If (Grid_OptimisationResults.Rows.Count <> (OptimiserSnapshot.CornerPortfolioCount + 1)) Then
        Grid_OptimisationResults.Rows.Count = (OptimiserSnapshot.CornerPortfolioCount + 1)
      End If

      ' Set Column Headings.

      Grid_OptimisationResults.Cols(0).Caption = "CP"
      Grid_OptimisationResults.Cols(1).Caption = "Return"
      Grid_OptimisationResults.Cols(2).Caption = "SDev"
      Grid_OptimisationResults.Cols(3).Caption = "LambdaE"

      For ColumnIndex = 0 To 3
        Grid_OptimisationResults.Cols(ColumnIndex).AllowDragging = False
        Grid_OptimisationResults.Cols(ColumnIndex).AllowEditing = False
        Grid_OptimisationResults.Cols(ColumnIndex).AllowMerging = False
        Grid_OptimisationResults.Cols(ColumnIndex).AllowResizing = True
        Grid_OptimisationResults.Cols(ColumnIndex).AllowSorting = True
      Next

      For InstrumentIndex = 1 To OptimiserSnapshot.NumberOfRealSecurities
        ColumnIndex = InstrumentIndex + 3

        Grid_OptimisationResults.Cols(ColumnIndex).Caption = "S" & InstrumentIndex.ToString
        Grid_OptimisationResults.Cols(ColumnIndex).AllowDragging = False
        Grid_OptimisationResults.Cols(ColumnIndex).AllowEditing = False
        Grid_OptimisationResults.Cols(ColumnIndex).AllowMerging = False
        Grid_OptimisationResults.Cols(ColumnIndex).AllowResizing = True
        Grid_OptimisationResults.Cols(ColumnIndex).AllowSorting = True
      Next

      ' Set Column Data 
      Dim ThisWeightsArray() As Double

      For RowIndex = 1 To OptimiserSnapshot.CornerPortfolioCount
        ThisRow = Grid_OptimisationResults.Rows(RowIndex)

        Grid_OptimisationResults.Item(RowIndex, 0) = OptimiserSnapshot.CP_Number(RowIndex)
        Grid_OptimisationResults.Item(RowIndex, 1) = OptimiserSnapshot.CP_ExpectedReturn(RowIndex)
        Grid_OptimisationResults.Item(RowIndex, 2) = OptimiserSnapshot.CP_StandardDeviation(RowIndex)
        Grid_OptimisationResults.Item(RowIndex, 3) = OptimiserSnapshot.CP_LambdaE(RowIndex)
        ThisWeightsArray = OptimiserSnapshot.CP_WeightingArray(RowIndex)

        For InstrumentIndex = 1 To OptimiserSnapshot.NumberOfRealSecurities
          ColumnIndex = InstrumentIndex + 3

          If (ThisWeightsArray Is Nothing) OrElse (ThisWeightsArray.Length <= InstrumentIndex) Then
            Grid_OptimisationResults.Clear(C1.Win.C1FlexGrid.ClearFlags.Content, RowIndex, ColumnIndex)
          Else
            Grid_OptimisationResults.Item(RowIndex, ColumnIndex) = ThisWeightsArray(InstrumentIndex)

            If (ThisWeightsArray(InstrumentIndex) < 0) Then
              Grid_OptimisationResults.SetCellStyle(RowIndex, ColumnIndex, Grid_OptimisationResults.Styles("Negative"))
            Else
              Grid_OptimisationResults.SetCellStyle(RowIndex, ColumnIndex, Grid_OptimisationResults.Styles("Positive"))
            End If
          End If
        Next
      Next

      Grid_OptimisationResults.Sort(C1.Win.C1FlexGrid.SortFlags.Ascending, 0)

    Catch ex As Exception

      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error displaying optimisation results", ex.StackTrace, True)

      Me.Grid_OptimisationResults.Rows.Count = 1

    End Try


  End Sub

#End Region

#Region " Supporting functions"

  Private Sub SetScalingFactorHash()

    ' *****************************************************************************************
    '
    ' *****************************************************************************************

    If (ScalingFactorHash Is Nothing) Then
      ScalingFactorHash = New Dictionary(Of Integer, Double)
    Else
      ScalingFactorHash.Clear()
    End If

    Try
      Dim Col_GroupPertracCode As Integer = Grid_Returns.Cols("GroupPertracCode").SafeIndex
      Dim Col_GroupScalingFactor As Integer = Grid_Returns.Cols("GroupScalingFactor").SafeIndex
      Dim RowIndex As Integer

      For RowIndex = 1 To (Me.Grid_Returns.Rows.Count - 1)
        If (IsNumeric(Grid_Returns.Item(RowIndex, Col_GroupPertracCode))) Then
          If (IsNumeric(Grid_Returns.Item(RowIndex, Col_GroupScalingFactor))) Then
            ScalingFactorHash.Add(CInt(Grid_Returns.Item(RowIndex, Col_GroupPertracCode)), CDbl(Grid_Returns.Item(RowIndex, Col_GroupScalingFactor)))
          Else
            ScalingFactorHash.Add(CInt(Grid_Returns.Item(RowIndex, Col_GroupPertracCode)), 1.0#)
          End If
        End If
      Next

    Catch ex As Exception
    End Try

  End Sub

#End Region











End Class
