Option Strict On

Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass


Public Class frmMasterName

	Inherits System.Windows.Forms.Form
	Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	Friend WithEvents editAuditID As System.Windows.Forms.TextBox
	Friend WithEvents edit_MasterName As System.Windows.Forms.TextBox
	Friend WithEvents btnNavFirst As System.Windows.Forms.Button
	Friend WithEvents btnNavPrev As System.Windows.Forms.Button
	Friend WithEvents btnNavNext As System.Windows.Forms.Button
	Friend WithEvents btnLast As System.Windows.Forms.Button
	Friend WithEvents btnAdd As System.Windows.Forms.Button
	Friend WithEvents btnDelete As System.Windows.Forms.Button
	Friend WithEvents btnCancel As System.Windows.Forms.Button
	Friend WithEvents btnSave As System.Windows.Forms.Button
  Friend WithEvents Combo_SelectMasterName As FCP_TelerikControls.FCP_RadComboBox
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
	Friend WithEvents lbl_EntityName As System.Windows.Forms.Label
  Friend WithEvents Check_IsUsed As System.Windows.Forms.CheckBox
  Friend WithEvents Combo_DataPeriod As System.Windows.Forms.ComboBox
  Friend WithEvents lbl_GroupID As System.Windows.Forms.Label
  Friend WithEvents Check_CaptureFromMorningstar As System.Windows.Forms.CheckBox
  Friend WithEvents btnClose As System.Windows.Forms.Button
  <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.editAuditID = New System.Windows.Forms.TextBox
    Me.edit_MasterName = New System.Windows.Forms.TextBox
    Me.btnNavFirst = New System.Windows.Forms.Button
    Me.btnNavPrev = New System.Windows.Forms.Button
    Me.btnNavNext = New System.Windows.Forms.Button
    Me.btnLast = New System.Windows.Forms.Button
    Me.btnAdd = New System.Windows.Forms.Button
    Me.btnDelete = New System.Windows.Forms.Button
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.Combo_SelectMasterName = New FCP_TelerikControls.FCP_RadComboBox
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Label1 = New System.Windows.Forms.Label
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.btnClose = New System.Windows.Forms.Button
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.lbl_EntityName = New System.Windows.Forms.Label
    Me.Check_IsUsed = New System.Windows.Forms.CheckBox
    Me.Combo_DataPeriod = New System.Windows.Forms.ComboBox
    Me.lbl_GroupID = New System.Windows.Forms.Label
    Me.Check_CaptureFromMorningstar = New System.Windows.Forms.CheckBox
    CType(Me.Combo_SelectMasterName, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel1.SuspendLayout()
    Me.SuspendLayout()
    '
    'editAuditID
    '
    Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.editAuditID.Enabled = False
    Me.editAuditID.Location = New System.Drawing.Point(555, 33)
    Me.editAuditID.Name = "editAuditID"
    Me.editAuditID.Size = New System.Drawing.Size(50, 20)
    Me.editAuditID.TabIndex = 1
    '
    'edit_MasterName
    '
    Me.edit_MasterName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.edit_MasterName.Location = New System.Drawing.Point(121, 76)
    Me.edit_MasterName.MaxLength = 100
    Me.edit_MasterName.Name = "edit_MasterName"
    Me.edit_MasterName.Size = New System.Drawing.Size(484, 20)
    Me.edit_MasterName.TabIndex = 2
    '
    'btnNavFirst
    '
    Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavFirst.Location = New System.Drawing.Point(8, 8)
    Me.btnNavFirst.Name = "btnNavFirst"
    Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
    Me.btnNavFirst.TabIndex = 0
    Me.btnNavFirst.Text = "<<"
    '
    'btnNavPrev
    '
    Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavPrev.Location = New System.Drawing.Point(50, 8)
    Me.btnNavPrev.Name = "btnNavPrev"
    Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
    Me.btnNavPrev.TabIndex = 1
    Me.btnNavPrev.Text = "<"
    '
    'btnNavNext
    '
    Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavNext.Location = New System.Drawing.Point(88, 8)
    Me.btnNavNext.Name = "btnNavNext"
    Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
    Me.btnNavNext.TabIndex = 2
    Me.btnNavNext.Text = ">"
    '
    'btnLast
    '
    Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnLast.Location = New System.Drawing.Point(124, 8)
    Me.btnLast.Name = "btnLast"
    Me.btnLast.Size = New System.Drawing.Size(40, 28)
    Me.btnLast.TabIndex = 3
    Me.btnLast.Text = ">>"
    '
    'btnAdd
    '
    Me.btnAdd.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnAdd.Location = New System.Drawing.Point(176, 8)
    Me.btnAdd.Name = "btnAdd"
    Me.btnAdd.Size = New System.Drawing.Size(75, 28)
    Me.btnAdd.TabIndex = 4
    Me.btnAdd.Text = "&New"
    '
    'btnDelete
    '
    Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnDelete.Location = New System.Drawing.Point(237, 263)
    Me.btnDelete.Name = "btnDelete"
    Me.btnDelete.Size = New System.Drawing.Size(75, 28)
    Me.btnDelete.TabIndex = 7
    Me.btnDelete.Text = "&Delete"
    '
    'btnCancel
    '
    Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(321, 263)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 8
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSave
    '
    Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(149, 263)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 28)
    Me.btnSave.TabIndex = 6
    Me.btnSave.Text = "&Save"
    '
    'Combo_SelectMasterName
    '
    Me.Combo_SelectMasterName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SelectMasterName.FormattingEnabled = True
    Me.Combo_SelectMasterName.Location = New System.Drawing.Point(121, 33)
    Me.Combo_SelectMasterName.MasternameCollection = Nothing
    Me.Combo_SelectMasterName.Name = "Combo_SelectMasterName"
    '
    '
    '
    Me.Combo_SelectMasterName.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
    Me.Combo_SelectMasterName.SelectNoMatch = False
    Me.Combo_SelectMasterName.Size = New System.Drawing.Size(428, 20)
    Me.Combo_SelectMasterName.TabIndex = 0
    Me.Combo_SelectMasterName.ThemeName = "ControlDefault"
    '
    'Panel1
    '
    Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel1.Controls.Add(Me.btnNavFirst)
    Me.Panel1.Controls.Add(Me.btnNavPrev)
    Me.Panel1.Controls.Add(Me.btnNavNext)
    Me.Panel1.Controls.Add(Me.btnLast)
    Me.Panel1.Controls.Add(Me.btnAdd)
    Me.Panel1.Location = New System.Drawing.Point(185, 207)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(260, 48)
    Me.Panel1.TabIndex = 5
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(6, 33)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(100, 23)
    Me.Label1.TabIndex = 18
    Me.Label1.Text = "Select"
    '
    'GroupBox1
    '
    Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox1.Location = New System.Drawing.Point(5, 59)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(600, 10)
    Me.GroupBox1.TabIndex = 77
    Me.GroupBox1.TabStop = False
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(405, 263)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 9
    Me.btnClose.Text = "&Close"
    '
    'RootMenu
    '
    Me.RootMenu.AllowMerge = False
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(616, 24)
    Me.RootMenu.TabIndex = 10
    Me.RootMenu.Text = "MenuStrip1"
    '
    'lbl_EntityName
    '
    Me.lbl_EntityName.Location = New System.Drawing.Point(6, 79)
    Me.lbl_EntityName.Name = "lbl_EntityName"
    Me.lbl_EntityName.Size = New System.Drawing.Size(111, 17)
    Me.lbl_EntityName.TabIndex = 86
    Me.lbl_EntityName.Text = "Instrument Name"
    '
    'Check_IsUsed
    '
    Me.Check_IsUsed.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Check_IsUsed.Location = New System.Drawing.Point(5, 106)
    Me.Check_IsUsed.Name = "Check_IsUsed"
    Me.Check_IsUsed.Size = New System.Drawing.Size(130, 18)
    Me.Check_IsUsed.TabIndex = 3
    Me.Check_IsUsed.Text = "Is Used"
    Me.Check_IsUsed.UseVisualStyleBackColor = True
    Me.Check_IsUsed.Visible = False
    '
    'Combo_DataPeriod
    '
    Me.Combo_DataPeriod.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_DataPeriod.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_DataPeriod.FormattingEnabled = True
    Me.Combo_DataPeriod.Location = New System.Drawing.Point(121, 130)
    Me.Combo_DataPeriod.Name = "Combo_DataPeriod"
    Me.Combo_DataPeriod.Size = New System.Drawing.Size(484, 21)
    Me.Combo_DataPeriod.TabIndex = 4
    '
    'lbl_GroupID
    '
    Me.lbl_GroupID.AutoSize = True
    Me.lbl_GroupID.Location = New System.Drawing.Point(6, 133)
    Me.lbl_GroupID.Name = "lbl_GroupID"
    Me.lbl_GroupID.Size = New System.Drawing.Size(112, 13)
    Me.lbl_GroupID.TabIndex = 88
    Me.lbl_GroupID.Text = "Price / Returns Period"
    '
    'Check_CaptureFromMorningstar
    '
    Me.Check_CaptureFromMorningstar.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
    Me.Check_CaptureFromMorningstar.Location = New System.Drawing.Point(5, 155)
    Me.Check_CaptureFromMorningstar.Name = "Check_CaptureFromMorningstar"
    Me.Check_CaptureFromMorningstar.Size = New System.Drawing.Size(130, 32)
    Me.Check_CaptureFromMorningstar.TabIndex = 89
    Me.Check_CaptureFromMorningstar.Text = "Capture from Morningstar"
    Me.Check_CaptureFromMorningstar.UseVisualStyleBackColor = True
    '
    'frmMasterName
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(616, 303)
    Me.Controls.Add(Me.Check_CaptureFromMorningstar)
    Me.Controls.Add(Me.Combo_DataPeriod)
    Me.Controls.Add(Me.lbl_GroupID)
    Me.Controls.Add(Me.Check_IsUsed)
    Me.Controls.Add(Me.lbl_EntityName)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.Combo_SelectMasterName)
    Me.Controls.Add(Me.btnSave)
    Me.Controls.Add(Me.editAuditID)
    Me.Controls.Add(Me.edit_MasterName)
    Me.Controls.Add(Me.btnDelete)
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.RootMenu)
    Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
    Me.MainMenuStrip = Me.RootMenu
    Me.MinimumSize = New System.Drawing.Size(458, 265)
    Me.Name = "frmMasterName"
    Me.Text = "Add/Edit Mastername"
    CType(Me.Combo_SelectMasterName, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel1.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


  ' Form 'Parent', the Main Venice form.
  ' Generally only accessed through the 'MainForm' property.
  Private WithEvents _MainForm As GenoaMain

  ' Form ToolTip
  Private FormTooltip As New ToolTip()

  ' Form Constants, specific to the table being updated.

  Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

  ' Form Locals, initialised on 'New' defining what standard data items to use
  Private THIS_TABLENAME As String
  Private THIS_ADAPTORNAME As String
  Private THIS_DATASETNAME As String

  Private GenericUpdateObject As RenaissanceTimerUpdateClass

  ' The standard ChangeID for this form. e.g. tblGroupList
  Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  Private THIS_FORM_SelectingCombo As FCP_TelerikControls.FCP_RadComboBox
  Private THIS_FORM_NewMoveToControl As Control

  ' Form Specific Order fields
  Private THIS_FORM_SelectBy As String
  Private THIS_FORM_OrderBy As String

  Private THIS_FORM_ValueMember As String

  ' Form specific Permissioning variables
  Private THIS_FORM_PermissionArea As String
  Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

  ' Form specific Form type 
  Private THIS_FORM_FormID As GenoaFormID

  ' Data Structures

  Private myConnection As SqlConnection
  Private myAdaptor As SqlDataAdapter

  Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


  ' Active Element.

  Private thisAuditID As Integer
  Private thisPosition As Integer
  Private _IsOverCancelButton As Boolean
  Private _InUse As Boolean

  ' Form Status Flags

  Private FormIsValid As Boolean
  Private FormChanged As Boolean
  Private _FormOpenFailed As Boolean
  Private InPaint As Boolean
  Private InGetFormData_Tick As Boolean
  Private AddNewRecord As Boolean

  ' User Permission Flags

  Private HasReadPermission As Boolean
  Private HasUpdatePermission As Boolean
  Private HasInsertPermission As Boolean
  Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

  Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
    ' Public property to return handle to the 'Main' Venice form, where in reside most of the 
    ' data structures and many common utilities.
    Get
      Return _MainForm
    End Get
  End Property

  Public ReadOnly Property FormChangedFlag() As Boolean
    Get
      Return FormChanged
    End Get
  End Property

  Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
    ' Public property maintaining a value indicating if the cursor is over the 'Cancel'
    ' Button on this form.
    ' This property is specifically designed for use by the field formating Event functions
    ' In order that they do not impose format restrictions if the user is about to click the 
    ' 'Cancel' button.
    '
    Get
      Return _IsOverCancelButton
    End Get
    Set(ByVal Value As Boolean)
      _IsOverCancelButton = Value
    End Set
  End Property

  Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
    Get
      Return InPaint
    End Get
  End Property

  Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
    Get
      Return _InUse
    End Get
  End Property

  Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
    Get
      Return _FormOpenFailed
    End Get
  End Property

#End Region

  Public Sub New(ByVal pMainForm As GenoaMain)
    ' *************************************************************
    ' Custom 'New'. 
    ' Passes in the reference to the parent form.
    ' 
    ' Establishes form specific variables.
    ' Establishes Form specific Data connection / data structures.
    '
    ' *************************************************************

    Me.New()

    _MainForm = pMainForm
    AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

    _FormOpenFailed = False
    _InUse = True

    ' ******************************************************
    ' Form Specific Settings :
    ' ******************************************************

    THIS_FORM_SelectingCombo = Me.Combo_SelectMasterName
    THIS_FORM_NewMoveToControl = Me.edit_MasterName

    ' Default Select and Order fields.

    THIS_FORM_SelectBy = "Mastername"
    THIS_FORM_OrderBy = "Mastername"

    THIS_FORM_ValueMember = "ID"

    ' Form Permissioning :-

    THIS_FORM_PermissionArea = Me.Name
    THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

    ' 'This' form ID

    THIS_FORM_FormID = GenoaFormID.frmMasterName

    ' This form's dataset type.

    ThisStandardDataset = RenaissanceStandardDatasets.Mastername  ' This Defines the Form Data !!! 

    ' Format Event Handlers for form controls

    AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
    AddHandler edit_MasterName.LostFocus, AddressOf MainForm.Text_NotNull

    ' Form Control Changed events

    AddHandler edit_MasterName.TextChanged, AddressOf Me.FormControlChanged
    AddHandler Check_IsUsed.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Check_CaptureFromMorningstar.CheckedChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_DataPeriod.SelectedValueChanged, AddressOf Me.FormControlChanged

    AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
    'AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

    AddHandler Combo_DataPeriod.SelectedValueChanged, AddressOf Me.FormControlChanged
    AddHandler Combo_DataPeriod.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
    AddHandler Combo_DataPeriod.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
    AddHandler Combo_DataPeriod.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
    AddHandler Combo_DataPeriod.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

    ' Set up the ToolTip
    MainForm.SetFormToolTip(Me, FormTooltip)

    ' ******************************************************
    ' End Form Specific.
    ' ******************************************************

    ' Data object names standard to this Form type.

    THIS_TABLENAME = ThisStandardDataset.TableName
    THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
    THIS_DATASETNAME = ThisStandardDataset.DatasetName

    THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

    ' Establish / Retrieve data objects for this form.

    myConnection = MainForm.MainDataHandler.Get_Connection(Genoa_CONNECTION)
    myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, Genoa_CONNECTION, THIS_TABLENAME)

    Me.RootMenu.PerformLayout()

    Try
      InPaint = True

      SetDealingPeriodCombo()

    Catch ex As Exception
    Finally
      InPaint = False
    End Try

  End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

  ' Form Initialisation code.
  '
  Public Sub ResetForm() Implements StandardGenoaForm.ResetForm
    THIS_FORM_SelectBy = "Mastername"
    THIS_FORM_OrderBy = "Mastername"

    Call Form_Load(Me, New System.EventArgs)
  End Sub

  Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
    ' *****************************************************************************************
    '
    '
    ' *****************************************************************************************

    ALWAYS_CLOSE_THIS_FORM = True
    Me.Close()
  End Sub


  Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
    _FormOpenFailed = False
    _InUse = True

    ' Initialise Data structures. Connection, Adaptor and Dataset.

    If Not (MainForm Is Nothing) Then
      FormIsValid = True
    Else
      MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myConnection Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    If (myAdaptor Is Nothing) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    ' Initialse form

    InPaint = True
    IsOverCancelButton = False

    ' Check User permissions
    Call CheckPermissions()
    If (HasReadPermission = False) Then
      Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

      FormIsValid = False
      _FormOpenFailed = True
      Exit Sub
    End If

    Call SetSortedRows()

    ' Initialise Timer

    GenericUpdateObject = MainForm.AddFormUpdate(Me, AddressOf GetFormData_Tick, 300)

    ' Display initial record.

    thisPosition = 0
    If MainForm.MasternameDictionary.Count > 0 Then
      Call GetFormData(CInt(MainForm.MasternameDictionary.KeyValue(0)))
    Else
      Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
      Call GetFormData(Nothing)
    End If

    InPaint = False


  End Sub

  Private Sub frmEntity_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
    Dim HideForm As Boolean

    ' Hide or Close this form ?
    ' All depends on how many of this form type are Open or in Cache...

    _InUse = False
    MainForm.RemoveFormUpdate(Me)

    If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
      HideForm = False
    Else
      If (FormChanged = True) Then
        Call SetFormData()
      End If

      HideForm = True
      If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
        HideForm = False
      End If
    End If

    If HideForm = True Then
      MainForm.HideInFormsCollection(Me)
      Me.Hide() ' NPP Fix

      e.Cancel = True
    Else
      Try
        MainForm.RemoveFromFormsCollection(Me)
        RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

        RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
        RemoveHandler edit_MasterName.LostFocus, AddressOf MainForm.Text_NotNull

        ' Form Control Changed events

        RemoveHandler edit_MasterName.TextChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_IsUsed.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Check_CaptureFromMorningstar.CheckedChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_DataPeriod.SelectedValueChanged, AddressOf Me.FormControlChanged

        RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
        'RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

        RemoveHandler Combo_DataPeriod.SelectedValueChanged, AddressOf Me.FormControlChanged
        RemoveHandler Combo_DataPeriod.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
        RemoveHandler Combo_DataPeriod.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
        RemoveHandler Combo_DataPeriod.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
        RemoveHandler Combo_DataPeriod.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

      Catch ex As Exception
      End Try
    End If

  End Sub


#End Region

  ' Routine to handle changes / updates to tables by this and other windows.
  ' If this, or any other, form posts a change to a table, then it will invoke an update event 
  ' detailing what tables have been altered.
  ' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
  ' the 'VeniceAutoUpdate' event of the main Venice form.
  ' Each form may them react as appropriate to changes in any table that might impact it.
  '
  Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
    Dim OrgInPaint As Boolean
    Dim KnowledgeDateChanged As Boolean
    Dim SetButtonStatus_Flag As Boolean
    Dim RefreshForm As Boolean = False

    If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

    OrgInPaint = InPaint
    InPaint = True
    KnowledgeDateChanged = False
    SetButtonStatus_Flag = False

    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
      KnowledgeDateChanged = True
      RefreshForm = True
    End If

    ' ****************************************************************
    ' Check for changes relevant to this form
    ' ****************************************************************

    ' Changes to the KnowledgeDate :-
    If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
      SetButtonStatus_Flag = True
    End If

    ' Changes to the tblUserPermissions table :-
    If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

      ' Check ongoing permissions.

      Call CheckPermissions()
      If (HasReadPermission = False) Then
        Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

        FormIsValid = False
        Me.Close()
        Exit Sub
      End If

      SetButtonStatus_Flag = True

    End If



    ' ****************************************************************
    ' Changes to the Main FORM table :-
    ' ****************************************************************

    If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then
      RefreshForm = True

      ' Re-Set Controls etc.
      Call SetSortedRows()

      ' Move again to the correct item
      thisPosition = MainForm.MasternameDictionary.IndexOfKey(thisAuditID)

      ' Set SelectingCombo Index.
      If (Me.MainForm.MasternameDictionary.Count <= 0) Then
        Try
          Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
        Catch ex As Exception
        End Try
      ElseIf (thisPosition < 0) Then
        Try
          MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
        Catch ex As Exception
        End Try
      Else
        Try
          Me.THIS_FORM_SelectingCombo.SelectedValue = thisAuditID
        Catch ex As Exception
        End Try
      End If

    End If

    ' ****************************************************************
    ' Repaint if not currently in Edit Mode
    '
    ' ****************************************************************

    InPaint = OrgInPaint

    ' Retrieve form data if appropriate, i.e. this data item is not already being edited.
    If (RefreshForm = True) AndAlso (FormChanged = False) AndAlso (AddNewRecord = False) Then
      GetFormData(thisAuditID) ' Includes a call to 'SetButtonStatus()'
    Else
      If SetButtonStatus_Flag Then
        Call SetButtonStatus()
      End If
    End If

  End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

  ' Build Sorted list from the Source dataset and update the Select Combo
  Private Sub SetSortedRows()

    THIS_FORM_SelectingCombo.MasternameCollection = MainForm.MasternameDictionary

    'Dim OrgInPaint As Boolean

    'Dim thisrow As DataRow
    'Dim thisDrowDownWidth As Integer
    'Dim SizingBitmap As Bitmap
    'Dim SizingGraphics As Graphics
    'Dim SelectString As String = "True"

    '' Form Specific Selection Combo :-
    'If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

    '' Code is pretty Generic from here on...

    '' Set paint local so that changes to the selection combo do not trigger form updates.

    'OrgInPaint = InPaint
    'InPaint = True

    '' Get selected Row sets, or exit if no data is present.
    'If myDataset Is Nothing Then
    '  ReDim SortedRows(0)
    '  ReDim SelectBySortedRows(0)
    'Else
    '  SortedRows = myTable.Select(SelectString, THIS_FORM_OrderBy)
    '  SelectBySortedRows = myTable.Select(SelectString, THIS_FORM_SelectBy)
    'End If

    '' Set Combo data source
    'THIS_FORM_SelectingCombo.DataSource = SortedRows
    'THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
    'THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

    '' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

    'thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
    'For Each thisrow In SortedRows

    '  ' Compute the string dimensions in the given font
    '  SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
    '  SizingGraphics = Graphics.FromImage(SizingBitmap)
    '  Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
    '  If (stringSize.Width) > thisDrowDownWidth Then
    '    thisDrowDownWidth = CInt(stringSize.Width)
    '  End If
    'Next

    'THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

    'InPaint = OrgInPaint
  End Sub


  ' Check User permissions
  Private Sub CheckPermissions()

    Dim Permissions As Integer

    Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

    HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
    HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
    HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
    HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

  End Sub

  ' Flag changes to form controls, Event Associations made in 'New' routine.
  Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

    If InPaint = False Then
      If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
        FormChanged = True
        Me.btnSave.Enabled = True
        Me.btnCancel.Enabled = True
      End If
    End If

  End Sub


  Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
    ' Event handler for the Audit Report Menu
    ' Event association is made during the dynamic menu creation

    Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
      Case 0 ' This Record
        If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
          Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID)
        End If

      Case 1 ' All Records
        Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset)

    End Select

  End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

  Private Sub SetDealingPeriodCombo()
    ' ***************************************************************
    '
    ' ***************************************************************

    Call MainForm.SetTblGenericCombo( _
    Me.Combo_DataPeriod, GetType(RenaissanceGlobals.DealingPeriod), False)   ' 


  End Sub


#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

  Private Function GetFormData_Tick() As Boolean
    ' *******************************************************************************
    '
    ' Callback process for the Generic Form Update Process.
    '
    ' Function MUST return True / False on Update.
    ' True will clear the update request. False will not.
    '
    ' *******************************************************************************
    Dim RVal As Boolean = False

    If (InGetFormData_Tick) OrElse (Not _InUse) Then
      Return False
      Exit Function
    End If

    Try
      InGetFormData_Tick = True

      ' Find the correct data row, then show it...
      thisPosition = MainForm.MasternameDictionary.IndexOfKey(THIS_FORM_SelectingCombo.SelectedValue)

      GetFormData(CInt(THIS_FORM_SelectingCombo.SelectedValue))

      RVal = True

    Catch ex As Exception
      RVal = False
    Finally
      InGetFormData_Tick = False
    End Try

    Return RVal

  End Function

  Private Sub GetFormData(ByVal pPertracID As Integer)
    ' **********************************************************************
    '
    ' Routine to populate form Controls given a data row.
    ' Form is cleared for invalid Datarows or Invalid Form.
    '
    ' **********************************************************************

    Dim OrgInpaint As Boolean

    Try
      ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
      OrgInpaint = InPaint
      InPaint = True


      If (pPertracID <= 0) Or (FormIsValid = False) Or (Me.InUse = False) Then
        ' Bad / New Datarow - Clear Form.

        thisAuditID = (-1)

        Me.editAuditID.Text = ""
        Me.edit_MasterName.Text = ""

        Me.Check_IsUsed.Checked = False
        If (Combo_DataPeriod.Items.Count > 0) Then
          Me.Combo_DataPeriod.SelectedValue = CInt(DealingPeriod.Monthly)
        End If

        Check_CaptureFromMorningstar.Checked = False

        If AddNewRecord = True Then
          Me.btnCancel.Enabled = True
          Me.THIS_FORM_SelectingCombo.Enabled = False
        Else
          Me.btnCancel.Enabled = False
          Me.THIS_FORM_SelectingCombo.Enabled = True
        End If

      Else

        Dim ThisTable As DSMastername.tblMasternameDataTable
        Dim thisDataRow As DSMastername.tblMasternameRow = Nothing

        ThisTable = MainForm.GetMasternameItem(pPertracID)

        If (ThisTable IsNot Nothing) AndAlso (ThisTable.Rows.Count > 0) Then
          thisDataRow = CType(ThisTable.Rows(0), DSMastername.tblMasternameRow)
        End If

        If thisDataRow Is Nothing Then
          GetFormData(0)
        Else

          ' Populate Form with given data.
          Try
            thisAuditID = thisDataRow.ID

            Me.editAuditID.Text = thisAuditID.ToString

            Me.edit_MasterName.Text = thisDataRow.Mastername
            If (thisDataRow.IsUsed) Then
              Me.Check_IsUsed.Checked = True
            Else
              Me.Check_IsUsed.Checked = False
            End If

            Check_CaptureFromMorningstar.Checked = thisDataRow.CaptureFromMorningstar

            If (Combo_DataPeriod.Items.Count > 0) Then
              Me.Combo_DataPeriod.SelectedValue = thisDataRow.DataPeriod
            End If

            AddNewRecord = False
            ' MainForm.SetComboSelectionLengths(Me, THIS_FORM_SelectingCombo)

            Me.btnCancel.Enabled = False
            Me.THIS_FORM_SelectingCombo.Enabled = True

          Catch ex As Exception

            Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
            Call GetFormData(Nothing)

          End Try

        End If

      End If

      ' Allow Field events to trigger before 'InPaint' Is re-set. 
      ' (Should) Prevent Validation errors during Form Draw.
      Application.DoEvents()

    Catch ex As Exception
    Finally
      InPaint = OrgInpaint
      FormChanged = False
    End Try

    ' Restore 'Paint' flag.
    Me.btnSave.Enabled = False

    ' As it says on the can....
    Call SetButtonStatus()

  End Sub

  Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
    ' *************************************************************
    '
    ' Note that GroupList Entries are also added from the FundSearch Form. Changes
    ' to the GroupList table should be reflected there also.
    '
    ' *************************************************************
    Dim ErrMessage As String
    Dim ErrFlag As Boolean
    Dim ErrStack As String
    Dim ProtectedItem As Boolean = False

    Dim ThisTable As DSMastername.tblMasternameDataTable
    Dim thisDataRow As DSMastername.tblMasternameRow = Nothing

    ErrMessage = ""
    ErrStack = ""

    ' *************************************************************
    ' Appropriate Save permission :-
    ' *************************************************************

    If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' If Save button is disabled then should not be able to save, exit silently.
    ' *************************************************************
    If Me.btnSave.Enabled = False Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Genoa are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' Confirm Save, if required.

    If (pConfirm = True) Then
      If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
        Return True
        Exit Function
      End If
    End If

    ' *************************************************************
    ' Procedure to Save the current form information.
    ' *************************************************************

    Dim StatusString As String
    Dim LogString As String
    Dim UpdateRows(0) As DataRow
    Dim Position As Integer

    If (FormChanged = False) Or (FormIsValid = False) Then
      Return False
      Exit Function
    End If

    ' Validation
    StatusString = ""
    If ValidateForm(StatusString) = False Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
      Return False
      Exit Function
    End If

    ' Check current position in the table.
    Position = MainForm.MasternameDictionary.IndexOfKey(thisAuditID)

    ' Allow for new or missing ID.
    If Position < 0 Then
      If (AddNewRecord) Then
        LogString = "Add : "
        AddNewRecord = True
        ThisTable = New DSMastername.tblMasternameDataTable
        thisDataRow = ThisTable.NewtblMasternameRow
      Else
        Return False
        Exit Function
      End If
    Else
      ' Row found OK.
      LogString = "Edit : AuditID = " & thisAuditID.ToString

      ThisTable = MainForm.GetMasternameItem(thisAuditID)

      If (ThisTable IsNot Nothing) Then
        If (ThisTable.Rows.Count > 0) Then
          thisDataRow = CType(ThisTable.Rows(0), DSMastername.tblMasternameRow)
        Else
          thisDataRow = ThisTable.NewtblMasternameRow
          AddNewRecord = True
        End If
      Else
        ThisTable = New DSMastername.tblMasternameDataTable
        thisDataRow = ThisTable.NewtblMasternameRow
        AddNewRecord = True
      End If

    End If


    Try
      ' Set 'Paint' flag.
      InPaint = True

      ' *************************************************************
      ' Lock the Data Table, to prevent update conflicts.
      ' *************************************************************

      ' Initiate Edit,
      thisDataRow.BeginEdit()

      ' Set Data Values

      thisDataRow.Mastername = edit_MasterName.Text
      LogString &= ", Mastername = " & edit_MasterName.Text

      thisDataRow.IsUsed = Check_IsUsed.Checked

      If (Combo_DataPeriod.Items.Count > 0) AndAlso (IsNumeric(Combo_DataPeriod.SelectedValue)) Then
        thisDataRow.DataPeriod = CInt(Me.Combo_DataPeriod.SelectedValue)
      Else
        thisDataRow.DataPeriod = CInt(DealingPeriod.Monthly)
      End If

      thisDataRow.CaptureFromMorningstar = Check_CaptureFromMorningstar.Checked

      thisDataRow.EndEdit()
      InPaint = False

      ' Add and Update DataRow. 

      ErrFlag = False

      If CBool(thisDataRow.RowState And DataRowState.Detached) Then
        Try
          ThisTable.Rows.Add(thisDataRow)
        Catch ex As Exception
          ErrFlag = True
          ErrMessage = ex.Message
          ErrStack = ex.StackTrace
        End Try
      End If

      UpdateRows(0) = thisDataRow

      ' Post Additions / Updates to the underlying table :-
      Dim temp As Integer
      Try
        If (ErrFlag = False) Then
          myAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
          myAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

          temp = MainForm.AdaptorUpdate(Me.Name, myAdaptor, UpdateRows)
        End If

      Catch ex As Exception

        ErrMessage = ex.Message
        ErrFlag = True
        ErrStack = ex.StackTrace

      End Try

      thisAuditID = thisDataRow.ID

    Catch ex As Exception
      ErrFlag = True
      ErrMessage = ex.Message
      ErrStack = ex.StackTrace
    Finally
      InPaint = False
    End Try


    ' *************************************************************
    ' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
    ' *************************************************************

    If (ErrFlag = True) Then
      Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
    End If

    ' Finish off

    AddNewRecord = False
    FormChanged = False

    Me.THIS_FORM_SelectingCombo.Enabled = True

    ' Propagate changes

    Dim UpdateMessage As New RenaissanceGlobals.RenaissanceUpdateEventArgs
    UpdateMessage.TableChanged(THIS_FORM_ChangeID) = True
    UpdateMessage.UpdateDetail(THIS_FORM_ChangeID) = thisAuditID.ToString
    UpdateMessage.TableChanged(RenaissanceChangeID.Information) = True
    UpdateMessage.UpdateDetail(RenaissanceChangeID.Information) = thisAuditID.ToString

    Call MainForm.Main_RaiseEvent(UpdateMessage)

  End Function

  Private Sub SetButtonStatus()
    ' Sets the status of the form controlls appropriate to the current users 
    'permissions and the 'Changed' or 'New' status of the form.

    ' No Read permission :-

    If Me.HasReadPermission = False Then
      MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
      Me.Close()
      Exit Sub
    End If

    ' Has Insert Permission.
    If Me.HasInsertPermission Then
      Me.btnAdd.Enabled = True
    Else
      Me.btnAdd.Enabled = False
    End If

    ' Has Delete permission. 
    If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
      Me.btnDelete.Enabled = True
    Else
      Me.btnDelete.Enabled = False
    End If

    If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) And ((AddNewRecord) Or (thisAuditID > 0)) Then

      Me.edit_MasterName.Enabled = True
      Me.Check_IsUsed.Enabled = True
      Me.Check_CaptureFromMorningstar.Enabled = True
    Else

      Me.edit_MasterName.Enabled = False
      Me.Check_IsUsed.Enabled = False
      Me.Check_CaptureFromMorningstar.Enabled = False

    End If

  End Sub

  Private Function ValidateForm(ByRef pReturnString As String) As Boolean
    ' Form Validation code.
    ' 
    ' This code should be the final arbiter of what is allowed. no assumptions regarding 
    ' prior validation should be made.
    ' 
    ' This Code is called by the SetFormData routine before position changes.
    '
    Dim RVal As Boolean

    RVal = True
    pReturnString = ""

    ' Validate 

    If Me.edit_MasterName.Text.Length <= 0 Then
      pReturnString = "Instrument must not be left blank."
      RVal = False
    End If

    Return RVal

  End Function

  Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = True
  End Sub

  Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
    ' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
    Me.IsOverCancelButton = False
  End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

  Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    ' ***************************************************************
    ' Selection Combo. SelectedItem changed.
    '
    ' ***************************************************************

    ' Don't react to changes made in paint routines etc.

    If InPaint = True Then Exit Sub

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    ' Find the correct data row, then show it...
    thisPosition = MainForm.MasternameDictionary.IndexOfKey(THIS_FORM_SelectingCombo.SelectedValue)

    If (GenericUpdateObject IsNot Nothing) Then
      Try
        GenericUpdateObject.FormToUpdate = True  '	GetFormDataToUpdate = True
      Catch ex As Exception
      End Try
    Else
      Call GetFormData(CInt(THIS_FORM_SelectingCombo.SelectedValue))
    End If


  End Sub


  Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
    ' 'Previous' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition > 0 Then thisPosition -= 1
    If thisPosition >= MainForm.MasternameDictionary.Count Then
      thisPosition = (MainForm.MasternameDictionary.Count - 1)
    End If

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

  End Sub

  Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
    ' 'Next' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    If thisPosition < (MainForm.MasternameDictionary.Count - 1) Then thisPosition += 1
    If thisPosition >= MainForm.MasternameDictionary.Count Then
      thisPosition = (MainForm.MasternameDictionary.Count - 1)
    End If

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

  End Sub

  Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
    ' 'First' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = 0
    If thisPosition >= MainForm.MasternameDictionary.Count Then
      thisPosition = (MainForm.MasternameDictionary.Count - 1)
    End If

    If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
      Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
    Else
      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    End If

  End Sub

  Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
    ' 'Last' Button.

    If (FormChanged = True) Then
      Call SetFormData()
    End If

    thisPosition = MainForm.MasternameDictionary.Count - 1

    THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

  End Sub

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

  Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
    ' Cancel Changes, redisplay form.

    FormChanged = False
    AddNewRecord = False

    Me.THIS_FORM_SelectingCombo.Enabled = True

    If (thisPosition >= 0) And (thisPosition < MainForm.MasternameDictionary.Count) Then
      Call GetFormData(CInt(MainForm.MasternameDictionary.KeyValue(thisPosition)))
    Else
      Call btnNavFirst_Click(Me, New System.EventArgs)
    End If

  End Sub

  Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
    ' Save Changes, if any, without prompting.

    If (FormChanged = True) Then
      Call SetFormData(False)
    End If

  End Sub

  Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
    Dim Position As Integer
    Dim NextAuditID As Integer

    If (AddNewRecord = True) Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' No Appropriate Save permission :-

    If (Me.HasDeletePermission = False) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Genoa are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Sub
    End If

    ' *************************************************************
    ' Confirm :-
    ' *************************************************************
    If MsgBox("Are you Sure ?", MsgBoxStyle.OkCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
      Exit Sub
    End If

    ' Resolve row to show after deleting this one.

    NextAuditID = (-1)
    If (Position + 1) < MainForm.MasternameDictionary.Count Then
      NextAuditID = CInt(MainForm.MasternameDictionary.KeyValue(Position + 1))
    ElseIf (Position - 1) >= 0 Then
      NextAuditID = CInt(MainForm.MasternameDictionary.KeyValue(Position - 1))
    Else
      NextAuditID = (-1)
    End If

    ' Check Data position.

    Dim UpdateMessage As New RenaissanceGlobals.RenaissanceUpdateEventArgs
    UpdateMessage.TableChanged(THIS_FORM_ChangeID) = True
    UpdateMessage.UpdateDetail(THIS_FORM_ChangeID) = thisAuditID.ToString
    UpdateMessage.TableChanged(RenaissanceChangeID.Information) = True
    UpdateMessage.UpdateDetail(RenaissanceChangeID.Information) = thisAuditID.ToString

    Position = MainForm.MasternameDictionary.IndexOfKey(thisAuditID)

    If Position < 0 Then Exit Sub

    Dim ThisTable As DSMastername.tblMasternameDataTable
    Dim thisDataRow As DSMastername.tblMasternameRow = Nothing

    ThisTable = MainForm.GetMasternameItem(thisAuditID)
    If (ThisTable IsNot Nothing) AndAlso (ThisTable.Rows.Count > 0) Then

      thisDataRow = CType(ThisTable.Rows(0), DSMastername.tblMasternameRow)

      ' Check Referential Integrity 
      If CheckReferentialIntegrity(_MainForm, thisDataRow) = False Then
        MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Deleting this record would voilate referential integrity.", "", True)
        Call btnCancel_Click(Me, New System.EventArgs)
        Exit Sub
      End If

      ' Delete this row

      Try
        thisDataRow.Delete()

        MainForm.AdaptorUpdate(Me.Name, myAdaptor, New DataRow() {thisDataRow})

      Catch ex As Exception

        Call MainForm.LogError(Me.Name & ", Delete()", LOG_LEVELS.Error, ex.Message, "Error Deleting Record.", ex.StackTrace, True)

      Finally

        ' Tidy Up.

        FormChanged = False
        thisAuditID = NextAuditID
        Call MainForm.Main_RaiseEvent(UpdateMessage)

      End Try

    Else

      thisAuditID = NextAuditID

    End If

    ' Tidy Up.

    FormChanged = False

  End Sub

  Public Sub AddNewGroup()
    ' *************************************************************
    '
    ' *************************************************************

    Call btnAdd_Click(Me.btnAdd, New System.EventArgs)
  End Sub

  Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
    ' *************************************************************
    ' Prepare form to Add a new record.
    ' *************************************************************


    If (FormChanged = True) Then
      Call SetFormData()
    End If

    Application.DoEvents()

    thisPosition = -1
    InPaint = True
    MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
    InPaint = False

    GetFormData(Nothing)
    AddNewRecord = True
    Me.btnCancel.Enabled = True
    Me.THIS_FORM_SelectingCombo.Enabled = False

    Call SetButtonStatus()

    THIS_FORM_NewMoveToControl.Focus()

  End Sub

  Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
    ' *************************************************************
    ' Close Form
    ' *************************************************************


    If (FormChanged = True) Then
      Call SetFormData()
    End If

    Me.Close()

  End Sub

#End Region

#Region " Form Control Event Code"

#End Region

#Region " Bug hunting "

  ' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
  ' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
  ' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

  Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
    Dim a As Integer

    If Not e.Command Is Nothing Then
      a = 1
    End If

    If Not e.Errors Is Nothing Then
      a = 2
    End If
  End Sub

  Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
    Dim a As Integer

    a = 1
  End Sub

#End Region




  Private Sub Combo_Charts_CompareSeriesPertrac_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

  End Sub
End Class
