Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass
Imports RenaissanceStatFunctions

Imports Genoa.MaxFunctions

Public Class frmGroupCovariance

	Inherits System.Windows.Forms.Form
	Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	Friend WithEvents btnNavFirst As System.Windows.Forms.Button
	Friend WithEvents btnNavPrev As System.Windows.Forms.Button
	Friend WithEvents btnNavNext As System.Windows.Forms.Button
	Friend WithEvents btnLast As System.Windows.Forms.Button
	Friend WithEvents btnSave As System.Windows.Forms.Button
	Friend WithEvents Combo_SelectGroupID As System.Windows.Forms.ComboBox
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
	Friend WithEvents Label6 As System.Windows.Forms.Label
	Friend WithEvents Combo_SelectGroup As System.Windows.Forms.ComboBox
	Friend WithEvents StatusStrip_GroupCovariance As System.Windows.Forms.StatusStrip
	Friend WithEvents StatusLabel_GroupReturns As System.Windows.Forms.ToolStripStatusLabel
	Friend WithEvents Label2 As System.Windows.Forms.Label
	Friend WithEvents Label_GroupStartDate As System.Windows.Forms.Label
	Friend WithEvents Label_GroupEndDate As System.Windows.Forms.Label
	Friend WithEvents Label4 As System.Windows.Forms.Label
	Friend WithEvents List_SelectItems As System.Windows.Forms.ListBox
	Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
	Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents TabControl_Grids As System.Windows.Forms.TabControl
	Friend WithEvents Tab_FullMatrix As System.Windows.Forms.TabPage
	Friend WithEvents Tab_SelectedMatrix As System.Windows.Forms.TabPage
	Friend WithEvents Text_Lamda As RenaissanceControls.NumericTextBox
	Friend WithEvents Label38 As System.Windows.Forms.Label
	Friend WithEvents Grid_FullMatrix As C1.Win.C1FlexGrid.C1FlexGrid
	Friend WithEvents Grid_Selective As C1.Win.C1FlexGrid.C1FlexGrid
	Friend WithEvents Radio_Covariance As System.Windows.Forms.RadioButton
	Friend WithEvents Radio_Correlation As System.Windows.Forms.RadioButton
	Friend WithEvents Chart_Prices As Dundas.Charting.WinControl.Chart
	Friend WithEvents Chart_Scatter As Dundas.Charting.WinControl.Chart
	Friend WithEvents SplitContainer2 As System.Windows.Forms.SplitContainer
	Friend WithEvents Combo_CovarianceSave As System.Windows.Forms.ComboBox
	Friend WithEvents btnView As System.Windows.Forms.Button
	Friend WithEvents btnDelete As System.Windows.Forms.Button
	Friend WithEvents Check_Covariance_BackfillData As System.Windows.Forms.CheckBox
	Friend WithEvents btnClose As System.Windows.Forms.Button
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim ChartArea1 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
		Dim Legend1 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
		Dim Series1 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
		Dim Series2 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
		Dim ChartArea2 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
		Dim Legend2 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
		Dim Series3 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
		Dim Series4 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
		Me.btnNavFirst = New System.Windows.Forms.Button
		Me.btnNavPrev = New System.Windows.Forms.Button
		Me.btnNavNext = New System.Windows.Forms.Button
		Me.btnLast = New System.Windows.Forms.Button
		Me.btnSave = New System.Windows.Forms.Button
		Me.Combo_SelectGroupID = New System.Windows.Forms.ComboBox
		Me.Label1 = New System.Windows.Forms.Label
		Me.btnClose = New System.Windows.Forms.Button
		Me.RootMenu = New System.Windows.Forms.MenuStrip
		Me.Label6 = New System.Windows.Forms.Label
		Me.Combo_SelectGroup = New System.Windows.Forms.ComboBox
		Me.StatusStrip_GroupCovariance = New System.Windows.Forms.StatusStrip
		Me.StatusLabel_GroupReturns = New System.Windows.Forms.ToolStripStatusLabel
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label_GroupStartDate = New System.Windows.Forms.Label
		Me.Label_GroupEndDate = New System.Windows.Forms.Label
		Me.Label4 = New System.Windows.Forms.Label
		Me.List_SelectItems = New System.Windows.Forms.ListBox
		Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
		Me.TabControl_Grids = New System.Windows.Forms.TabControl
		Me.Tab_FullMatrix = New System.Windows.Forms.TabPage
		Me.Grid_FullMatrix = New C1.Win.C1FlexGrid.C1FlexGrid
		Me.Tab_SelectedMatrix = New System.Windows.Forms.TabPage
		Me.Grid_Selective = New C1.Win.C1FlexGrid.C1FlexGrid
		Me.Panel1 = New System.Windows.Forms.Panel
		Me.Check_Covariance_BackfillData = New System.Windows.Forms.CheckBox
		Me.Radio_Covariance = New System.Windows.Forms.RadioButton
		Me.Radio_Correlation = New System.Windows.Forms.RadioButton
		Me.Text_Lamda = New RenaissanceControls.NumericTextBox
		Me.Label38 = New System.Windows.Forms.Label
		Me.Chart_Prices = New Dundas.Charting.WinControl.Chart
		Me.Chart_Scatter = New Dundas.Charting.WinControl.Chart
		Me.SplitContainer2 = New System.Windows.Forms.SplitContainer
		Me.Combo_CovarianceSave = New System.Windows.Forms.ComboBox
		Me.btnView = New System.Windows.Forms.Button
		Me.btnDelete = New System.Windows.Forms.Button
		Me.StatusStrip_GroupCovariance.SuspendLayout()
		Me.SplitContainer1.Panel1.SuspendLayout()
		Me.SplitContainer1.Panel2.SuspendLayout()
		Me.SplitContainer1.SuspendLayout()
		Me.TabControl_Grids.SuspendLayout()
		Me.Tab_FullMatrix.SuspendLayout()
		CType(Me.Grid_FullMatrix, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Tab_SelectedMatrix.SuspendLayout()
		CType(Me.Grid_Selective, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Panel1.SuspendLayout()
		CType(Me.Chart_Prices, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.Chart_Scatter, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.SplitContainer2.Panel1.SuspendLayout()
		Me.SplitContainer2.Panel2.SuspendLayout()
		Me.SplitContainer2.SuspendLayout()
		Me.SuspendLayout()
		'
		'btnNavFirst
		'
		Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavFirst.Location = New System.Drawing.Point(118, 57)
		Me.btnNavFirst.Name = "btnNavFirst"
		Me.btnNavFirst.Size = New System.Drawing.Size(40, 23)
		Me.btnNavFirst.TabIndex = 2
		Me.btnNavFirst.Text = "<<"
		'
		'btnNavPrev
		'
		Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavPrev.Location = New System.Drawing.Point(160, 57)
		Me.btnNavPrev.Name = "btnNavPrev"
		Me.btnNavPrev.Size = New System.Drawing.Size(35, 23)
		Me.btnNavPrev.TabIndex = 3
		Me.btnNavPrev.Text = "<"
		'
		'btnNavNext
		'
		Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnNavNext.Location = New System.Drawing.Point(201, 57)
		Me.btnNavNext.Name = "btnNavNext"
		Me.btnNavNext.Size = New System.Drawing.Size(35, 23)
		Me.btnNavNext.TabIndex = 4
		Me.btnNavNext.Text = ">"
		'
		'btnLast
		'
		Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnLast.Location = New System.Drawing.Point(237, 57)
		Me.btnLast.Name = "btnLast"
		Me.btnLast.Size = New System.Drawing.Size(40, 23)
		Me.btnLast.TabIndex = 5
		Me.btnLast.Text = ">>"
		'
		'btnSave
		'
		Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnSave.Location = New System.Drawing.Point(2, 583)
		Me.btnSave.Name = "btnSave"
		Me.btnSave.Size = New System.Drawing.Size(75, 21)
		Me.btnSave.TabIndex = 2
		Me.btnSave.Text = "&Save"
		'
		'Combo_SelectGroupID
		'
		Me.Combo_SelectGroupID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_SelectGroupID.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_SelectGroupID.Location = New System.Drawing.Point(117, 30)
		Me.Combo_SelectGroupID.Name = "Combo_SelectGroupID"
		Me.Combo_SelectGroupID.Size = New System.Drawing.Size(236, 21)
		Me.Combo_SelectGroupID.TabIndex = 1
		'
		'Label1
		'
		Me.Label1.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label1.Location = New System.Drawing.Point(5, 33)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(87, 14)
		Me.Label1.TabIndex = 13
		Me.Label1.Text = "Select"
		'
		'btnClose
		'
		Me.btnClose.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnClose.Location = New System.Drawing.Point(847, 583)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 21)
		Me.btnClose.TabIndex = 6
		Me.btnClose.Text = "&Close"
		'
		'RootMenu
		'
		Me.RootMenu.AllowMerge = False
		Me.RootMenu.Location = New System.Drawing.Point(0, 0)
		Me.RootMenu.Name = "RootMenu"
		Me.RootMenu.Size = New System.Drawing.Size(930, 24)
		Me.RootMenu.TabIndex = 12
		Me.RootMenu.Text = "MenuStrip1"
		'
		'Label6
		'
		Me.Label6.AutoSize = True
		Me.Label6.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label6.Location = New System.Drawing.Point(5, 6)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(69, 13)
		Me.Label6.TabIndex = 12
		Me.Label6.Text = "Select Group"
		'
		'Combo_SelectGroup
		'
		Me.Combo_SelectGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_SelectGroup.Location = New System.Drawing.Point(117, 3)
		Me.Combo_SelectGroup.Name = "Combo_SelectGroup"
		Me.Combo_SelectGroup.Size = New System.Drawing.Size(236, 21)
		Me.Combo_SelectGroup.TabIndex = 0
		'
		'StatusStrip_GroupCovariance
		'
		Me.StatusStrip_GroupCovariance.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.StatusLabel_GroupReturns})
		Me.StatusStrip_GroupCovariance.Location = New System.Drawing.Point(0, 609)
		Me.StatusStrip_GroupCovariance.Name = "StatusStrip_GroupCovariance"
		Me.StatusStrip_GroupCovariance.Size = New System.Drawing.Size(930, 22)
		Me.StatusStrip_GroupCovariance.TabIndex = 7
		Me.StatusStrip_GroupCovariance.Text = "StatusStrip1"
		'
		'StatusLabel_GroupReturns
		'
		Me.StatusLabel_GroupReturns.Name = "StatusLabel_GroupReturns"
		Me.StatusLabel_GroupReturns.Size = New System.Drawing.Size(111, 17)
		Me.StatusLabel_GroupReturns.Text = "ToolStripStatusLabel1"
		'
		'Label2
		'
		Me.Label2.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label2.Location = New System.Drawing.Point(3, 86)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(101, 17)
		Me.Label2.TabIndex = 14
		Me.Label2.Text = "Group Start Date"
		'
		'Label_GroupStartDate
		'
		Me.Label_GroupStartDate.BackColor = System.Drawing.SystemColors.Window
		Me.Label_GroupStartDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Label_GroupStartDate.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_GroupStartDate.Location = New System.Drawing.Point(118, 86)
		Me.Label_GroupStartDate.Name = "Label_GroupStartDate"
		Me.Label_GroupStartDate.Size = New System.Drawing.Size(149, 19)
		Me.Label_GroupStartDate.TabIndex = 6
		Me.Label_GroupStartDate.Text = "MgmtFee"
		'
		'Label_GroupEndDate
		'
		Me.Label_GroupEndDate.BackColor = System.Drawing.SystemColors.Window
		Me.Label_GroupEndDate.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Label_GroupEndDate.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label_GroupEndDate.Location = New System.Drawing.Point(118, 112)
		Me.Label_GroupEndDate.Name = "Label_GroupEndDate"
		Me.Label_GroupEndDate.Size = New System.Drawing.Size(149, 19)
		Me.Label_GroupEndDate.TabIndex = 7
		Me.Label_GroupEndDate.Text = "MgmtFee"
		'
		'Label4
		'
		Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Label4.Location = New System.Drawing.Point(3, 113)
		Me.Label4.Name = "Label4"
		Me.Label4.Size = New System.Drawing.Size(101, 16)
		Me.Label4.TabIndex = 15
		Me.Label4.Text = "Group End Date"
		'
		'List_SelectItems
		'
		Me.List_SelectItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.List_SelectItems.FormattingEnabled = True
		Me.List_SelectItems.Location = New System.Drawing.Point(1, 1)
		Me.List_SelectItems.Name = "List_SelectItems"
		Me.List_SelectItems.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
		Me.List_SelectItems.Size = New System.Drawing.Size(354, 329)
		Me.List_SelectItems.Sorted = True
		Me.List_SelectItems.TabIndex = 0
		'
		'SplitContainer1
		'
		Me.SplitContainer1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.SplitContainer1.Location = New System.Drawing.Point(2, 245)
		Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(1)
		Me.SplitContainer1.Name = "SplitContainer1"
		'
		'SplitContainer1.Panel1
		'
		Me.SplitContainer1.Panel1.Controls.Add(Me.List_SelectItems)
		'
		'SplitContainer1.Panel2
		'
		Me.SplitContainer1.Panel2.Controls.Add(Me.TabControl_Grids)
		Me.SplitContainer1.Size = New System.Drawing.Size(923, 335)
		Me.SplitContainer1.SplitterDistance = 361
		Me.SplitContainer1.TabIndex = 1
		'
		'TabControl_Grids
		'
		Me.TabControl_Grids.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.TabControl_Grids.Controls.Add(Me.Tab_FullMatrix)
		Me.TabControl_Grids.Controls.Add(Me.Tab_SelectedMatrix)
		Me.TabControl_Grids.Location = New System.Drawing.Point(1, 1)
		Me.TabControl_Grids.Margin = New System.Windows.Forms.Padding(1)
		Me.TabControl_Grids.Name = "TabControl_Grids"
		Me.TabControl_Grids.SelectedIndex = 0
		Me.TabControl_Grids.Size = New System.Drawing.Size(552, 329)
		Me.TabControl_Grids.TabIndex = 0
		'
		'Tab_FullMatrix
		'
		Me.Tab_FullMatrix.Controls.Add(Me.Grid_FullMatrix)
		Me.Tab_FullMatrix.Location = New System.Drawing.Point(4, 22)
		Me.Tab_FullMatrix.Name = "Tab_FullMatrix"
		Me.Tab_FullMatrix.Size = New System.Drawing.Size(544, 303)
		Me.Tab_FullMatrix.TabIndex = 0
		Me.Tab_FullMatrix.Text = "Full Matrix"
		Me.Tab_FullMatrix.UseVisualStyleBackColor = True
		'
		'Grid_FullMatrix
		'
		Me.Grid_FullMatrix.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Grid_FullMatrix.ColumnInfo = "1,1,0,0,0,85,Columns:0{Width:101;Name:""Col_Name"";AllowDragging:False;AllowEditing" & _
				":False;Style:""Format:""""0000"""";DataType:System.String;Font:Microsoft Sans Serif, " & _
				"8.25pt;TextAlign:LeftCenter;"";}" & Global.Microsoft.VisualBasic.ChrW(9)
		Me.Grid_FullMatrix.Cursor = System.Windows.Forms.Cursors.Default
		Me.Grid_FullMatrix.Location = New System.Drawing.Point(0, 0)
		Me.Grid_FullMatrix.Margin = New System.Windows.Forms.Padding(1)
		Me.Grid_FullMatrix.Name = "Grid_FullMatrix"
		Me.Grid_FullMatrix.Rows.Count = 1
		Me.Grid_FullMatrix.Rows.DefaultSize = 17
		Me.Grid_FullMatrix.Size = New System.Drawing.Size(544, 303)
		Me.Grid_FullMatrix.TabIndex = 19
		'
		'Tab_SelectedMatrix
		'
		Me.Tab_SelectedMatrix.Controls.Add(Me.Grid_Selective)
		Me.Tab_SelectedMatrix.Location = New System.Drawing.Point(4, 22)
		Me.Tab_SelectedMatrix.Name = "Tab_SelectedMatrix"
		Me.Tab_SelectedMatrix.Padding = New System.Windows.Forms.Padding(3)
		Me.Tab_SelectedMatrix.Size = New System.Drawing.Size(544, 303)
		Me.Tab_SelectedMatrix.TabIndex = 1
		Me.Tab_SelectedMatrix.Text = "Selective Matrix"
		Me.Tab_SelectedMatrix.UseVisualStyleBackColor = True
		'
		'Grid_Selective
		'
		Me.Grid_Selective.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Grid_Selective.ColumnInfo = "1,1,0,0,0,85,Columns:0{Width:101;Name:""Col_Name"";AllowDragging:False;AllowEditing" & _
				":False;Style:""Format:""""0000"""";DataType:System.String;Font:Microsoft Sans Serif, " & _
				"8.25pt;TextAlign:LeftCenter;"";}" & Global.Microsoft.VisualBasic.ChrW(9)
		Me.Grid_Selective.Location = New System.Drawing.Point(0, 0)
		Me.Grid_Selective.Margin = New System.Windows.Forms.Padding(1)
		Me.Grid_Selective.Name = "Grid_Selective"
		Me.Grid_Selective.Rows.Count = 1
		Me.Grid_Selective.Rows.DefaultSize = 17
		Me.Grid_Selective.Size = New System.Drawing.Size(543, 302)
		Me.Grid_Selective.TabIndex = 20
		'
		'Panel1
		'
		Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Panel1.Controls.Add(Me.Check_Covariance_BackfillData)
		Me.Panel1.Controls.Add(Me.Radio_Covariance)
		Me.Panel1.Controls.Add(Me.Radio_Correlation)
		Me.Panel1.Controls.Add(Me.Text_Lamda)
		Me.Panel1.Controls.Add(Me.Label38)
		Me.Panel1.Controls.Add(Me.Combo_SelectGroup)
		Me.Panel1.Controls.Add(Me.Combo_SelectGroupID)
		Me.Panel1.Controls.Add(Me.Label_GroupEndDate)
		Me.Panel1.Controls.Add(Me.Label1)
		Me.Panel1.Controls.Add(Me.Label4)
		Me.Panel1.Controls.Add(Me.btnLast)
		Me.Panel1.Controls.Add(Me.Label_GroupStartDate)
		Me.Panel1.Controls.Add(Me.btnNavNext)
		Me.Panel1.Controls.Add(Me.Label2)
		Me.Panel1.Controls.Add(Me.Label6)
		Me.Panel1.Controls.Add(Me.btnNavPrev)
		Me.Panel1.Controls.Add(Me.btnNavFirst)
		Me.Panel1.Location = New System.Drawing.Point(2, 27)
		Me.Panel1.Name = "Panel1"
		Me.Panel1.Size = New System.Drawing.Size(361, 214)
		Me.Panel1.TabIndex = 0
		'
		'Check_Covariance_BackfillData
		'
		Me.Check_Covariance_BackfillData.AutoSize = True
		Me.Check_Covariance_BackfillData.Location = New System.Drawing.Point(117, 187)
		Me.Check_Covariance_BackfillData.Name = "Check_Covariance_BackfillData"
		Me.Check_Covariance_BackfillData.Size = New System.Drawing.Size(120, 17)
		Me.Check_Covariance_BackfillData.TabIndex = 11
		Me.Check_Covariance_BackfillData.Text = "Backfill Price history"
		Me.Check_Covariance_BackfillData.UseVisualStyleBackColor = True
		'
		'Radio_Covariance
		'
		Me.Radio_Covariance.AutoSize = True
		Me.Radio_Covariance.Location = New System.Drawing.Point(215, 164)
		Me.Radio_Covariance.Name = "Radio_Covariance"
		Me.Radio_Covariance.Size = New System.Drawing.Size(79, 17)
		Me.Radio_Covariance.TabIndex = 10
		Me.Radio_Covariance.TabStop = True
		Me.Radio_Covariance.Text = "Covariance"
		Me.Radio_Covariance.UseVisualStyleBackColor = True
		'
		'Radio_Correlation
		'
		Me.Radio_Correlation.AutoSize = True
		Me.Radio_Correlation.Location = New System.Drawing.Point(117, 164)
		Me.Radio_Correlation.Name = "Radio_Correlation"
		Me.Radio_Correlation.Size = New System.Drawing.Size(75, 17)
		Me.Radio_Correlation.TabIndex = 9
		Me.Radio_Correlation.TabStop = True
		Me.Radio_Correlation.Text = "Correlation"
		Me.Radio_Correlation.UseVisualStyleBackColor = True
		'
		'Text_Lamda
		'
		Me.Text_Lamda.Location = New System.Drawing.Point(118, 138)
		Me.Text_Lamda.Name = "Text_Lamda"
		Me.Text_Lamda.RenaissanceTag = Nothing
		Me.Text_Lamda.Size = New System.Drawing.Size(149, 20)
		Me.Text_Lamda.TabIndex = 8
		Me.Text_Lamda.Text = "1.00"
		Me.Text_Lamda.TextFormat = "#,##0.00"
		Me.Text_Lamda.Value = 1
		'
		'Label38
		'
		Me.Label38.AutoSize = True
		Me.Label38.Location = New System.Drawing.Point(3, 141)
		Me.Label38.Name = "Label38"
		Me.Label38.Size = New System.Drawing.Size(39, 13)
		Me.Label38.TabIndex = 16
		Me.Label38.Text = "Lamda"
		'
		'Chart_Prices
		'
		Me.Chart_Prices.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Chart_Prices.BackColor = System.Drawing.Color.Azure
		Me.Chart_Prices.BackGradientEndColor = System.Drawing.Color.SkyBlue
		Me.Chart_Prices.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
		Me.Chart_Prices.BorderLineColor = System.Drawing.Color.LightGray
		Me.Chart_Prices.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
		Me.Chart_Prices.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
		ChartArea1.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
		ChartArea1.AxisX.LabelsAutoFit = False
		ChartArea1.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
								Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
								Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
		ChartArea1.AxisX.LabelStyle.Format = "Y"
		ChartArea1.AxisX.LineColor = System.Drawing.Color.DimGray
		ChartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
		ChartArea1.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
		ChartArea1.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
		ChartArea1.AxisX2.LineColor = System.Drawing.Color.DimGray
		ChartArea1.AxisY.LabelsAutoFit = False
		ChartArea1.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
								Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
								Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
		ChartArea1.AxisY.LabelStyle.Format = "N0"
		ChartArea1.AxisY.LineColor = System.Drawing.Color.DimGray
		ChartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
		ChartArea1.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
		ChartArea1.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
		ChartArea1.AxisY.StartFromZero = False
		ChartArea1.AxisY2.LineColor = System.Drawing.Color.DimGray
		ChartArea1.BackColor = System.Drawing.Color.Transparent
		ChartArea1.BorderColor = System.Drawing.Color.DimGray
		ChartArea1.Name = "Default"
		Me.Chart_Prices.ChartAreas.Add(ChartArea1)
		Legend1.BackColor = System.Drawing.Color.Transparent
		Legend1.BorderColor = System.Drawing.Color.Transparent
		Legend1.Docking = Dundas.Charting.WinControl.LegendDocking.Left
		Legend1.DockToChartArea = "Default"
		Legend1.Enabled = False
		Legend1.Name = "Default"
		Me.Chart_Prices.Legends.Add(Legend1)
		Me.Chart_Prices.Location = New System.Drawing.Point(0, 0)
		Me.Chart_Prices.Margin = New System.Windows.Forms.Padding(1)
		Me.Chart_Prices.Name = "Chart_Prices"
		Me.Chart_Prices.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
		Series1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
		Series1.BorderWidth = 2
		Series1.ChartType = "Line"
		Series1.CustomAttributes = "LabelStyle=Top"
		Series1.Name = "Series1"
		Series1.ShadowOffset = 1
		Series1.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
		Series1.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
		Series2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
		Series2.BorderWidth = 2
		Series2.ChartType = "Line"
		Series2.CustomAttributes = "LabelStyle=Top"
		Series2.Name = "Series2"
		Series2.ShadowOffset = 1
		Series2.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
		Series2.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
		Me.Chart_Prices.Series.Add(Series1)
		Me.Chart_Prices.Series.Add(Series2)
		Me.Chart_Prices.Size = New System.Drawing.Size(276, 214)
		Me.Chart_Prices.TabIndex = 0
		Me.Chart_Prices.Text = "Chart2"
		'
		'Chart_Scatter
		'
		Me.Chart_Scatter.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Chart_Scatter.BackColor = System.Drawing.Color.Azure
		Me.Chart_Scatter.BackGradientEndColor = System.Drawing.Color.SkyBlue
		Me.Chart_Scatter.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
		Me.Chart_Scatter.BorderLineColor = System.Drawing.Color.LightGray
		Me.Chart_Scatter.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
		Me.Chart_Scatter.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
		ChartArea2.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
		ChartArea2.AxisX.Crossing = 0
		ChartArea2.AxisX.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
								Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
								Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
		ChartArea2.AxisX.LabelStyle.Format = "P1"
		ChartArea2.AxisX.LabelStyle.Interval = 0
		ChartArea2.AxisX.LabelStyle.IntervalOffset = 0
		ChartArea2.AxisX.LabelStyle.IntervalOffsetType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisX.LabelStyle.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisX.LineColor = System.Drawing.Color.DimGray
		ChartArea2.AxisX.MajorGrid.Interval = 0
		ChartArea2.AxisX.MajorGrid.IntervalOffset = 0
		ChartArea2.AxisX.MajorGrid.IntervalOffsetType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisX.MajorGrid.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
		ChartArea2.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
		ChartArea2.AxisX.MajorTickMark.Interval = 0
		ChartArea2.AxisX.MajorTickMark.IntervalOffset = 0
		ChartArea2.AxisX.MajorTickMark.IntervalOffsetType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisX.MajorTickMark.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
		ChartArea2.AxisX2.LabelStyle.Interval = 0
		ChartArea2.AxisX2.LabelStyle.IntervalOffset = 0
		ChartArea2.AxisX2.LabelStyle.IntervalOffsetType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisX2.LabelStyle.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisX2.LineColor = System.Drawing.Color.DimGray
		ChartArea2.AxisX2.MajorGrid.Interval = 0
		ChartArea2.AxisX2.MajorGrid.IntervalOffset = 0
		ChartArea2.AxisX2.MajorGrid.IntervalOffsetType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisX2.MajorGrid.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisX2.MajorTickMark.Interval = 0
		ChartArea2.AxisX2.MajorTickMark.IntervalOffset = 0
		ChartArea2.AxisX2.MajorTickMark.IntervalOffsetType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisX2.MajorTickMark.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisY.Crossing = 0
		ChartArea2.AxisY.LabelsAutoFit = False
		ChartArea2.AxisY.LabelsAutoFitStyle = CType((((Dundas.Charting.WinControl.LabelsAutoFitStyle.DecreaseFont Or Dundas.Charting.WinControl.LabelsAutoFitStyle.OffsetLabels) _
								Or Dundas.Charting.WinControl.LabelsAutoFitStyle.LabelsAngleStep30) _
								Or Dundas.Charting.WinControl.LabelsAutoFitStyle.WordWrap), Dundas.Charting.WinControl.LabelsAutoFitStyle)
		ChartArea2.AxisY.LabelStyle.Format = "P1"
		ChartArea2.AxisY.LabelStyle.Interval = 0
		ChartArea2.AxisY.LabelStyle.IntervalOffset = 0
		ChartArea2.AxisY.LabelStyle.IntervalOffsetType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisY.LabelStyle.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisY.LineColor = System.Drawing.Color.DimGray
		ChartArea2.AxisY.MajorGrid.Interval = 0
		ChartArea2.AxisY.MajorGrid.IntervalOffset = 0
		ChartArea2.AxisY.MajorGrid.IntervalOffsetType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisY.MajorGrid.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
		ChartArea2.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
		ChartArea2.AxisY.MajorTickMark.Interval = 0
		ChartArea2.AxisY.MajorTickMark.IntervalOffset = 0
		ChartArea2.AxisY.MajorTickMark.IntervalOffsetType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisY.MajorTickMark.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
		ChartArea2.AxisY.StartFromZero = False
		ChartArea2.AxisY2.LabelStyle.Interval = 0
		ChartArea2.AxisY2.LabelStyle.IntervalOffset = 0
		ChartArea2.AxisY2.LabelStyle.IntervalOffsetType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisY2.LabelStyle.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisY2.LineColor = System.Drawing.Color.DimGray
		ChartArea2.AxisY2.MajorGrid.Interval = 0
		ChartArea2.AxisY2.MajorGrid.IntervalOffset = 0
		ChartArea2.AxisY2.MajorGrid.IntervalOffsetType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisY2.MajorGrid.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisY2.MajorTickMark.Interval = 0
		ChartArea2.AxisY2.MajorTickMark.IntervalOffset = 0
		ChartArea2.AxisY2.MajorTickMark.IntervalOffsetType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.AxisY2.MajorTickMark.IntervalType = Dundas.Charting.WinControl.DateTimeIntervalType.[Auto]
		ChartArea2.BackColor = System.Drawing.Color.Transparent
		ChartArea2.BorderColor = System.Drawing.Color.DimGray
		ChartArea2.Name = "Default"
		Me.Chart_Scatter.ChartAreas.Add(ChartArea2)
		Legend2.BackColor = System.Drawing.Color.Transparent
		Legend2.BorderColor = System.Drawing.Color.Transparent
		Legend2.Docking = Dundas.Charting.WinControl.LegendDocking.Left
		Legend2.DockToChartArea = "Default"
		Legend2.Enabled = False
		Legend2.Name = "Default"
		Me.Chart_Scatter.Legends.Add(Legend2)
		Me.Chart_Scatter.Location = New System.Drawing.Point(0, 0)
		Me.Chart_Scatter.Margin = New System.Windows.Forms.Padding(1)
		Me.Chart_Scatter.Name = "Chart_Scatter"
		Me.Chart_Scatter.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
		Series3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
		Series3.ChartType = "Point"
		Series3.CustomAttributes = "LabelStyle=Top"
		Series3.MarkerBorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
		Series3.Name = "Series1"
		Series3.ShadowOffset = 1
		Series3.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
		Series3.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
		Series4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
		Series4.ChartType = "Point"
		Series4.CustomAttributes = "LabelStyle=Top"
		Series4.MarkerBorderColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
		Series4.Name = "Series2"
		Series4.ShadowOffset = 1
		Series4.XValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
		Series4.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
		Me.Chart_Scatter.Series.Add(Series3)
		Me.Chart_Scatter.Series.Add(Series4)
		Me.Chart_Scatter.Size = New System.Drawing.Size(276, 214)
		Me.Chart_Scatter.TabIndex = 0
		'
		'SplitContainer2
		'
		Me.SplitContainer2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.SplitContainer2.Location = New System.Drawing.Point(369, 27)
		Me.SplitContainer2.Margin = New System.Windows.Forms.Padding(0)
		Me.SplitContainer2.Name = "SplitContainer2"
		'
		'SplitContainer2.Panel1
		'
		Me.SplitContainer2.Panel1.Controls.Add(Me.Chart_Prices)
		'
		'SplitContainer2.Panel2
		'
		Me.SplitContainer2.Panel2.Controls.Add(Me.Chart_Scatter)
		Me.SplitContainer2.Size = New System.Drawing.Size(553, 214)
		Me.SplitContainer2.SplitterDistance = 277
		Me.SplitContainer2.TabIndex = 113
		'
		'Combo_CovarianceSave
		'
		Me.Combo_CovarianceSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.Combo_CovarianceSave.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_CovarianceSave.Location = New System.Drawing.Point(83, 583)
		Me.Combo_CovarianceSave.Name = "Combo_CovarianceSave"
		Me.Combo_CovarianceSave.Size = New System.Drawing.Size(280, 21)
		Me.Combo_CovarianceSave.TabIndex = 3
		'
		'btnView
		'
		Me.btnView.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnView.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnView.Location = New System.Drawing.Point(370, 583)
		Me.btnView.Name = "btnView"
		Me.btnView.Size = New System.Drawing.Size(75, 21)
		Me.btnView.TabIndex = 4
		Me.btnView.Text = "&View"
		'
		'btnDelete
		'
		Me.btnDelete.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btnDelete.Location = New System.Drawing.Point(463, 583)
		Me.btnDelete.Name = "btnDelete"
		Me.btnDelete.Size = New System.Drawing.Size(75, 21)
		Me.btnDelete.TabIndex = 5
		Me.btnDelete.Text = "&Delete"
		'
		'frmGroupCovariance
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(930, 631)
		Me.Controls.Add(Me.btnDelete)
		Me.Controls.Add(Me.btnView)
		Me.Controls.Add(Me.Combo_CovarianceSave)
		Me.Controls.Add(Me.SplitContainer2)
		Me.Controls.Add(Me.Panel1)
		Me.Controls.Add(Me.SplitContainer1)
		Me.Controls.Add(Me.StatusStrip_GroupCovariance)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.btnSave)
		Me.Controls.Add(Me.RootMenu)
		Me.MainMenuStrip = Me.RootMenu
		Me.MinimumSize = New System.Drawing.Size(458, 339)
		Me.Name = "frmGroupCovariance"
		Me.Text = "View/Edit Group Covariances"
		Me.StatusStrip_GroupCovariance.ResumeLayout(False)
		Me.StatusStrip_GroupCovariance.PerformLayout()
		Me.SplitContainer1.Panel1.ResumeLayout(False)
		Me.SplitContainer1.Panel2.ResumeLayout(False)
		Me.SplitContainer1.ResumeLayout(False)
		Me.TabControl_Grids.ResumeLayout(False)
		Me.Tab_FullMatrix.ResumeLayout(False)
		CType(Me.Grid_FullMatrix, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Tab_SelectedMatrix.ResumeLayout(False)
		CType(Me.Grid_Selective, System.ComponentModel.ISupportInitialize).EndInit()
		Me.Panel1.ResumeLayout(False)
		Me.Panel1.PerformLayout()
		CType(Me.Chart_Prices, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.Chart_Scatter, System.ComponentModel.ISupportInitialize).EndInit()
		Me.SplitContainer2.Panel1.ResumeLayout(False)
		Me.SplitContainer2.Panel2.ResumeLayout(False)
		Me.SplitContainer2.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
	Private WithEvents _MainForm As GenoaMain

	' Apply Normalise Volatility for Backfilled series.
	' This is really just an identifier for areas that might need to be updated later.
	Private CONST_ChartsBackfillVolatility As Boolean = True
	Private CONST_MatrixScalingFactor As Double = 1.0#

	' Form ToolTip
	Private FormTooltip As New ToolTip()
	Dim FieldsMenu As ToolStripMenuItem = Nothing

	' Form Constants, specific to the table being updated.

	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form Locals, initialised on 'New' defining what standard data items to use
	Private THIS_TABLENAME As String
	Private THIS_ADAPTORNAME As String
	Private THIS_DATASETNAME As String


	' The standard ChangeID for this form. e.g. tblGroupList
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	Private THIS_FORM_SelectingCombo As ComboBox
	Private THIS_FORM_NewMoveToControl As Control

	' Form Specific Order fields
	Private THIS_FORM_SelectBy As String
	Private THIS_FORM_OrderBy As String

	Private THIS_FORM_ValueMember As String

	' Form specific Permissioning variables
	Private THIS_FORM_PermissionArea As String
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
	Private THIS_FORM_FormID As GenoaFormID

	' Data Structures

	Private myDataset As RenaissanceDataClass.DSGroupList			 ' Form Specific !!!!
	Private myTable As RenaissanceDataClass.DSGroupList.tblGroupListDataTable
	Private myConnection As SqlConnection
	Private myAdaptor As SqlDataAdapter

	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

	Private CovListIDToDisplay As Integer = 0

  Private _DefaultStatsDatePeriod As DealingPeriod = DealingPeriod.Monthly

	' Active Element.

	Private SortedRows() As DataRow
	Private SelectBySortedRows() As DataRow
	Private thisDataRow As RenaissanceDataClass.DSGroupList.tblGroupListRow		' Form Specific !!!!
	Private thisAuditID As Integer
	Private thisPosition As Integer
	Private _IsOverCancelButton As Boolean
	Private _InUse As Boolean

	' Form Status Flags

	Private FormIsValid As Boolean
	Private _FormOpenFailed As Boolean
	Private InPaint As Boolean

	' User Permission Flags

	Private HasReadPermission As Boolean
	Private HasUpdatePermission As Boolean
	Private HasInsertPermission As Boolean
	Private HasDeletePermission As Boolean

#End Region

#Region " Form 'Properties' "

	Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

	Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return _IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			_IsOverCancelButton = Value
		End Set
	End Property

	Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

	Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
		Get
			Return _InUse
		End Get
	End Property

	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

  Public Property DefaultStatsDatePeriod() As DealingPeriod
    Get
      Return _DefaultStatsDatePeriod
    End Get
    Set(ByVal value As DealingPeriod)
      _DefaultStatsDatePeriod = value

      StatusLabel_GroupReturns.Text = "Data Period set to " & _DefaultStatsDatePeriod.ToString
    End Set
  End Property

#End Region

	Public Sub New(ByVal pMainForm As GenoaMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True
    DefaultStatsDatePeriod = MainForm.DefaultStatsDatePeriod

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectGroupID

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "GroupListName"
		THIS_FORM_OrderBy = "GroupListName"

		THIS_FORM_ValueMember = "GroupListID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = GenoaFormID.frmGroupCovariance

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblGroupList	 ' This Defines the Form Data !!! 

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		' Form Control Changed events

		AddHandler Combo_SelectGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_SelectGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_SelectGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_SelectGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_SelectGroupID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_SelectGroupID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_SelectGroupID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_SelectGroupID.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(Genoa_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, Genoa_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		' Configure Grid
		Try
			Dim ThisStyle As C1.Win.C1FlexGrid.CellStyle

			ThisStyle = Me.Grid_FullMatrix.Styles.Add("Positive")
			ThisStyle.ForeColor = Color.Blue
			ThisStyle.Format = "#,##0.0000"

			ThisStyle = Me.Grid_FullMatrix.Styles.Add("Negative")
			ThisStyle.ForeColor = Color.Red
			ThisStyle.Format = "#,##0.0000"

			ThisStyle = Me.Grid_FullMatrix.Styles.Add("Black")
			ThisStyle.ForeColor = Color.Blue
			ThisStyle.Format = "#,##0.0000"
			ThisStyle.Font = New Font(ThisStyle.Font, FontStyle.Bold)

			ThisStyle = Me.Grid_Selective.Styles.Add("Positive")
			ThisStyle.ForeColor = Color.Blue
			ThisStyle.Format = "#,##0.0000"

			ThisStyle = Me.Grid_Selective.Styles.Add("Negative")
			ThisStyle.ForeColor = Color.Red
			ThisStyle.Format = "#,##0.0000"

			ThisStyle = Me.Grid_Selective.Styles.Add("Black")
			ThisStyle.ForeColor = Color.Blue
			ThisStyle.Format = "#,##0.0000"
			ThisStyle.Font = New Font(ThisStyle.Font, FontStyle.Bold)

		Catch ex As Exception
		End Try

		Try
			InPaint = True

			Me.Radio_Correlation.Checked = True

			Call SetGroupCombo()

		Catch ex As Exception
		Finally
			InPaint = False
		End Try

		MainForm.AddCopyMenuToChart(Me.Chart_Prices)
		MainForm.AddCopyMenuToChart(Me.Chart_Scatter)

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
	Public Sub ResetForm() Implements StandardGenoaForm.ResetForm
		' ***************************************************************************************************
		'
		'
		' ***************************************************************************************************

		THIS_FORM_SelectBy = "GroupListName"
		THIS_FORM_OrderBy = "GroupListName"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

	Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
		' ***************************************************************************************************
		'
		'
		' ***************************************************************************************************

		Me.Close()
	End Sub

	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		' ***************************************************************************************************
		'
		'
		' ***************************************************************************************************

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Sorted data list from which this form operates
		If (Combo_SelectGroup.Items.Count > 0) Then
			Combo_SelectGroup.SelectedIndex = 0
		End If

		' 

		Call SetCovarianceSaveCombo()

		Call SetSortedRows()

		Check_Covariance_BackfillData.Checked = False

		' Display initial record.

		thisPosition = 0
		If THIS_FORM_SelectingCombo.Items.Count > 0 Then
			Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
			thisDataRow = SortedRows(thisPosition)
      Call GetFormData(thisDataRow)
		Else
			Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
      Call GetFormData(Nothing)
		End If

		InPaint = False

		SetSelectiveMatrix(Me.List_SelectItems)

		Me.Chart_Prices.Series.Clear()
		Me.Chart_Scatter.Series.Clear()

	End Sub

	Private Sub frmEntity_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		' ***************************************************************************************************
		'
		'
		' ***************************************************************************************************

		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler
			Catch ex As Exception
			End Try

			Try
				' Form Control Changed events

				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
				RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_SelectGroup.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_SelectGroup.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_SelectGroup.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_SelectGroup.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_SelectGroupID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_SelectGroupID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_SelectGroupID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_SelectGroupID.KeyUp, AddressOf MainForm.ComboSelectAsYouType

			Catch ex As Exception
			End Try


		End If

	End Sub

#End Region

	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		' ***************************************************************************************************
		' Routine to handle changes / updates to tables by this and other windows.
		' If this, or any other, form posts a change to a table, then it will invoke an update event 
		' detailing what tables have been altered.
		' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
		' the 'VeniceAutoUpdate' event of the main Venice form.
		' Each form may them react as appropriate to changes in any table that might impact it.
		'
		' ***************************************************************************************************

		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True
		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
			RefreshForm = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************


		' SetGroupCombo
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call SetGroupCombo()
		End If

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If

		' tblCovarianceList
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblCovarianceList) = True) Or KnowledgeDateChanged Then
			Call SetCovarianceSaveCombo()
		End If

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupItems) = True) Or KnowledgeDateChanged Then
			If (Me.Combo_SelectGroupID.SelectedIndex >= 0) Then
				RefreshForm = True
			End If
		End If
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupItemData) = True) Or KnowledgeDateChanged Then
			If (Me.Combo_SelectGroupID.SelectedIndex >= 0) Then
				RefreshForm = True
			End If
		End If


		' ****************************************************************
		' Changes to the Main FORM table :-
		' ****************************************************************

		If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then
			If (Me.THIS_FORM_SelectingCombo.SelectedIndex >= 0) Then
				RefreshForm = True
			End If

			' Re-Set Controls etc.
			Call SetSortedRows()

			' Move again to the correct item
			thisPosition = Get_Position(thisAuditID)

			' Validate current position.
			If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
				thisDataRow = Me.SortedRows(thisPosition)
			Else
				If (Me.SortedRows.GetLength(0) > 0) Then
					thisPosition = 0
					thisDataRow = Me.SortedRows(thisPosition)
				Else
					thisDataRow = Nothing
				End If
			End If

			' Set SelectingCombo Index.
			If (Me.THIS_FORM_SelectingCombo.SelectedIndex >= 0) Then
				If (Me.THIS_FORM_SelectingCombo.Items.Count <= 0) Then
					Try
						Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
					Catch ex As Exception
					End Try
				ElseIf (thisPosition < 0) Then
					Try
						MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
					Catch ex As Exception
					End Try
				Else
					Try
						Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
					Catch ex As Exception
					End Try
				End If
			End If

		End If

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		InPaint = OrgInPaint

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm = True) Then
      GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
	Private Sub SetSortedRows()
		' ***************************************************************************************************
		'
		'
		' ***************************************************************************************************

		Dim OrgInPaint As Boolean

		Dim thisrow As DataRow
		Dim thisDrowDownWidth As Integer
		Dim SizingBitmap As Bitmap
		Dim SizingGraphics As Graphics
		Dim SelectString As String = "true"

		' Form Specific Selection Combo :-
		If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

		' Code is pretty Generic from here on...

		' Set paint local so that changes to the selection combo do not trigger form updates.

		OrgInPaint = InPaint
		InPaint = True

		If (Me.Combo_SelectGroup.SelectedIndex > 0) Then
			SelectString = "GroupGroup='" & Combo_SelectGroup.SelectedValue.ToString & "'"
		End If

		' Get selected Row sets, or exit if no data is present.
		If myDataset Is Nothing Then
			ReDim SortedRows(0)
			ReDim SelectBySortedRows(0)
		Else
			SortedRows = myTable.Select(SelectString, THIS_FORM_OrderBy)
			SelectBySortedRows = myTable.Select(SelectString, THIS_FORM_SelectBy)
		End If

		' Set Combo data source
		THIS_FORM_SelectingCombo.DataSource = SortedRows
		THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
		THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

		' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

		thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
		For Each thisrow In SortedRows

			' Compute the string dimensions in the given font
			SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
			SizingGraphics = Graphics.FromImage(SizingBitmap)
			Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
			If (stringSize.Width) > thisDrowDownWidth Then
				thisDrowDownWidth = CInt(stringSize.Width)
			End If
		Next

		THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

		If (THIS_FORM_SelectingCombo.Items.Count > 0) AndAlso (THIS_FORM_SelectingCombo.SelectedIndex < 0) Then
			THIS_FORM_SelectingCombo.SelectedIndex = 0
		End If

		InPaint = OrgInPaint
	End Sub


	' Check User permissions
	Private Sub CheckPermissions()
		' ***************************************************************************************************
		'
		'
		' ***************************************************************************************************

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.

	Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************************
		' Event handler for the Select Menu
		' Event association is made during the dynamic menu creation
		' ***************************************************************************************************

		THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub

	Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************************
		' Event handler for the Order Menu
		' Event association is made during the dynamic menu creation
		' ***************************************************************************************************

		THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub


	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' ***************************************************************************************************
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation
		' ***************************************************************************************************

		Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
			Case 0 ' This Record
				If (thisAuditID >= 0) Then
					Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID)
				End If

			Case 1 ' All Records
				Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset)

		End Select

	End Sub

#End Region

#Region " Menu Code"



#End Region

#Region " Set Form Combos (Form Specific Code) "

	Private Sub SetGroupCombo()
		' ***************************************************************************************************
		'
		'
		' ***************************************************************************************************

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_SelectGroup, _
		RenaissanceStandardDatasets.tblGroupList, _
		"GroupGroup", _
		"GroupGroup", _
		"", True, True, True, "", "All Groups")		' 

	End Sub

	Private Sub SetCovarianceSaveCombo()
		' ***************************************************************************************************
		'
		'
		' ***************************************************************************************************

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_CovarianceSave, _
		RenaissanceStandardDatasets.tblCovarianceList, _
		"ListName", _
		"CovListID", _
		"", True, True, True, 0, "")		' 

	End Sub


#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

  Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSGroupList.tblGroupListRow)
    ' ***************************************************************************************************
    ' Routine to populate form Controls given a data row.
    ' Form is cleared for invalid Datarows or Invalid Form.
    '
    ' Note, 'DefaultStatsDatePeriod' is set according to the lowest-common-denominator of the Group being loaded
    '
    ' ***************************************************************************************************

    Dim OrgInpaint As Boolean

    ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
    OrgInpaint = InPaint
    InPaint = True

    Try
      thisAuditID = (-1)

      If (ThisRow Is Nothing) OrElse (FormIsValid = False) OrElse (Me.InUse = False) OrElse (IsNumeric(Combo_SelectGroupID.SelectedValue) = False) Then
        ' Bad / New Datarow - Clear Form.


        Me.Label_GroupStartDate.Text = ""
        Me.Label_GroupEndDate.Text = ""
        Text_Lamda.Value = 1

        List_SelectItems.DataSource = Nothing
        Me.List_SelectItems.Items.Clear()

        Me.Grid_FullMatrix.Rows.Count = 1
        Me.Grid_FullMatrix.Cols.Count = 1

        Me.Grid_Selective.Rows.Count = 1
        Me.Grid_Selective.Cols.Count = 1

        Me.THIS_FORM_SelectingCombo.Enabled = True

      Else
        ' Populate List with Group Items

        ' Populate Form with given data.

        Me.Label_GroupStartDate.Text = ThisRow.GroupDateFrom.ToString(DISPLAYMEMBER_DATEFORMAT)
        Me.Label_GroupEndDate.Text = ThisRow.GroupDateTo.ToString(DISPLAYMEMBER_DATEFORMAT)

        Dim tmpCommand As New SqlCommand
        Dim ListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable = Nothing
        Dim ListRow As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow

        Try

          tmpCommand.CommandType = CommandType.StoredProcedure
          tmpCommand.CommandText = "adp_tblGroupItems_SelectGroupPertracCodes"
          tmpCommand.Connection = MainForm.GetGenoaConnection
          tmpCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = ThisRow.GroupListID
          tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
          tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

          List_SelectItems.SuspendLayout()
          thisAuditID = (ThisRow.GroupListID)

          If (List_SelectItems.DataSource Is Nothing) OrElse (Not (TypeOf List_SelectItems.DataSource Is RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)) Then
            ListTable = New RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable
            ListTable.Columns.Add("ListDescription", GetType(String))
            ListTable.Columns.Add("PertracName", GetType(String))
            ListTable.Columns.Add("PertracProvider", GetType(String))
            ListTable.Columns.Add("IndexName", GetType(String))
            ListTable.Columns.Add("IndexProvider", GetType(String))

            ' List_SelectItems.DataSource = ListTable
          Else
            ListTable = CType(List_SelectItems.DataSource, RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)
          End If

          ListTable.Rows.Clear()
          ListTable.Load(tmpCommand.ExecuteReader)

          Try
            If (ListTable IsNot List_SelectItems.DataSource) Then
              List_SelectItems.DataSource = Nothing
            End If
            If (List_SelectItems.DisplayMember <> "ListDescription") Then
              List_SelectItems.DisplayMember = "ListDescription"
            End If
            If (List_SelectItems.ValueMember <> "GroupPertracCode") Then
              List_SelectItems.ValueMember = "GroupPertracCode"
            End If
          Catch ex As Exception
          End Try

          ' Get Common StatsDatePeriod.

          If (ListTable.Rows.Count > 0) Then
            Dim newStatsDatePeriod As DealingPeriod = DealingPeriod.Daily
            Dim IDArray((ListTable.Rows.Count * 2) - 1) As Integer
            Dim PeriodArray() As DealingPeriod

            Dim thisIndex As Integer = 0

            ' Build Array of Pertrac IDs

            For Each ListRow In ListTable.Rows
              IDArray((thisIndex * 2)) = ListRow.GroupPertracCode
              IDArray((thisIndex * 2) + 1) = ListRow.GroupIndexCode
              thisIndex += 1
            Next

            ' Remove Duplicates, simply to improve performance in the call to GetPertracDataPeriods()

            Array.Sort(IDArray)
            For thisIndex = 1 To (IDArray.Length - 1)
              If (IDArray(thisIndex) > 0) AndAlso (IDArray(thisIndex) = IDArray(thisIndex - 1)) Then
                IDArray(thisIndex - 1) = 0
              End If
            Next

            ' Get Array of Native Date Periods for these instruments

            PeriodArray = MainForm.PertracData.GetPertracDataPeriods(IDArray)

            ' Resolve Coarsest Date Period

            For thisIndex = 0 To (IDArray.Length - 1)
              If (IDArray(thisIndex) > 0) Then
                If (MainForm.StatFunctions.AnnualPeriodCount(PeriodArray(thisIndex)) < MainForm.StatFunctions.AnnualPeriodCount(newStatsDatePeriod)) Then
                  newStatsDatePeriod = PeriodArray(thisIndex)
                End If
              End If
            Next

            Me.DefaultStatsDatePeriod = newStatsDatePeriod
          End If

          '

          List_SelectItems.DataSource = ListTable
          List_SelectItems.ResumeLayout()

        Catch ex As Exception

          Call MainForm.LogError(Me.Name, 0, ex.Message, "Error building Select List", ex.StackTrace, True)
          DefaultStatsDatePeriod = MainForm.DefaultStatsDatePeriod

        Finally
          Try
            If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
              tmpCommand.Connection.Close()
              tmpCommand.Connection = Nothing
            End If
          Catch ex As Exception
          End Try
        End Try


        ' Load Matrix Data

        Try
          If (ListTable IsNot Nothing) Then
            GetFullMatrix(DefaultStatsDatePeriod, ListTable, ThisRow.GroupDateFrom, ThisRow.GroupDateTo, StatusLabel_GroupReturns)
            SetSelectiveMatrix(List_SelectItems)
          Else
            Me.Grid_FullMatrix.Rows.Count = 1
            Me.Grid_FullMatrix.Cols.Count = 1

            Me.Grid_Selective.Rows.Count = 1
            Me.Grid_Selective.Cols.Count = 1
          End If

        Catch ex As Exception
          Call MainForm.LogError(Me.Name, 0, ex.Message, "Error building correlation grids", ex.StackTrace, True)
        End Try
      End If
    Catch ex As Exception
    Finally
      StatusLabel_GroupReturns.Text = ""
    End Try

    ' Allow Field events to trigger before 'InPaint' Is re-set. 
    ' (Should) Prevent Validation errors during Form Draw.
    Application.DoEvents()

    ' Restore 'Paint' flag.
    InPaint = OrgInpaint

    ' As it says on the can....
    Call SetButtonStatus()

  End Sub

  Private Sub GetFullMatrix(ByRef StatsDatePeriod As DealingPeriod, ByRef pListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable, ByVal DFrom As Date, ByVal DTo As Date, ByRef pStatusLabel As ToolStripLabel)
    ' ***************************************************************************************************
    '
    '
    ' ***************************************************************************************************

    Dim LoadFromTable As Boolean = False

    If (Combo_SelectGroupID.SelectedIndex < 0) Then
      LoadFromTable = True
    End If

    GetFullCovarianceMatrix(MainForm, StatsDatePeriod, Grid_FullMatrix, pListTable, Nothing, Check_Covariance_BackfillData.Checked, CONST_ChartsBackfillVolatility, Text_Lamda.Value, DFrom, DTo, Me.Radio_Covariance.Checked, LoadFromTable, CovListIDToDisplay, StatusLabel_GroupReturns)

  End Sub


	Private Sub SetSelectiveMatrix(ByVal pListSelect As ListBox)
		' ***************************************************************************************************
		'
		'
		' ***************************************************************************************************

		Try
			If (pListSelect Is Nothing) OrElse (pListSelect.SelectedItems.Count <= 0) OrElse (InPaint = True) Then
				Grid_Selective.Rows.Count = 1
				Grid_Selective.Cols.Count = 1
				Exit Sub
			End If

			Dim SelectedPertracIDs(pListSelect.SelectedItems.Count - 1) As Integer
			Dim FullMatrixRowMap(pListSelect.SelectedItems.Count - 1) As Integer

			Dim ItemCounter As Integer
			Dim RowIndex As Integer
			Dim ColIndex As Integer

			Grid_Selective.Rows.Count = pListSelect.SelectedItems.Count + 1
			Grid_Selective.Cols.Count = pListSelect.SelectedItems.Count + 2

			Grid_Selective.Cols(Grid_Selective.Cols.Count - 1).Visible = False
			Grid_Selective.Cols(Grid_Selective.Cols.Count - 1).DataType = GetType(Integer)

			For ItemCounter = 0 To (pListSelect.SelectedItems.Count - 1)
				SelectedPertracIDs(ItemCounter) = CType(CType(pListSelect.SelectedItems(ItemCounter), DataRowView).Row, RenaissanceDataClass.DSGroupItems.tblGroupItemsRow).GroupPertracCode

				Grid_Selective.Item(0, ItemCounter + 1) = ""
				Grid_Selective.Item(ItemCounter + 1, 0) = ""

				If (ItemCounter > 0) Then
					If (Radio_Covariance.Checked) Then
						Grid_Selective.Cols(ItemCounter).Width = 65
					Else
						Grid_Selective.Cols(ItemCounter).Width = 50
					End If
				End If

				FullMatrixRowMap(ItemCounter) = (-1)

				For RowIndex = 1 To (Grid_FullMatrix.Rows.Count - 1)
					If CInt(Grid_FullMatrix.Item(RowIndex, Grid_FullMatrix.Cols.Count - 1)) = SelectedPertracIDs(ItemCounter) Then
						FullMatrixRowMap(ItemCounter) = RowIndex

						Grid_Selective.Item(0, ItemCounter + 1) = Grid_FullMatrix.Item(RowIndex, 0)
						Grid_Selective.Item(ItemCounter + 1, 0) = Grid_FullMatrix.Item(RowIndex, 0)
						Exit For
					End If
				Next
			Next

			Dim SourceRow As Integer
			Dim SourceCol As Integer

			For RowIndex = 1 To pListSelect.SelectedItems.Count
				SourceRow = FullMatrixRowMap(RowIndex - 1)

				Grid_Selective.Cols(RowIndex).AllowEditing = False
				Grid_Selective.Cols(RowIndex).AllowMerging = False
				Grid_Selective.Cols(RowIndex).Width = 50
				Grid_Selective.Cols(RowIndex).Visible = True
				Grid_Selective.Cols(RowIndex).DataType = GetType(Double)

				Grid_Selective.Item(RowIndex, Grid_Selective.Cols.Count - 1) = Grid_FullMatrix.Item(SourceRow, Grid_FullMatrix.Cols.Count - 1)

				For ColIndex = 1 To RowIndex
					SourceCol = FullMatrixRowMap(ColIndex - 1)

					If (SourceRow > 0) AndAlso (SourceCol > 0) Then

						Grid_Selective.Item(RowIndex, ColIndex) = Grid_FullMatrix.Item(SourceRow, SourceCol)
						Grid_Selective.Item(ColIndex, RowIndex) = Grid_FullMatrix.Item(SourceRow, SourceCol)

						Grid_Selective.SetCellStyle(RowIndex, ColIndex, Grid_FullMatrix.GetCellStyle(SourceRow, SourceCol))
						Grid_Selective.SetCellStyle(ColIndex, RowIndex, Grid_FullMatrix.GetCellStyle(SourceRow, SourceCol))

					Else
						Grid_Selective.Item(RowIndex, ColIndex) = 0
						Grid_Selective.Item(ColIndex, RowIndex) = 0

						Grid_Selective.SetCellStyle(RowIndex, ColIndex, Grid_FullMatrix.GetCellStyle(1, 1))
						Grid_Selective.SetCellStyle(ColIndex, RowIndex, Grid_FullMatrix.GetCellStyle(1, 1))
					End If

				Next
			Next

		Catch ex As Exception
			Grid_Selective.Rows.Count = 1
			Grid_Selective.Cols.Count = 1
		End Try



	End Sub

  Private Function SetFormData(ByVal StatsDatePeriod As DealingPeriod, ByVal pConfirm As Boolean) As Boolean
    ' *************************************************************
    '
    ' *************************************************************

    Dim ErrMessage As String
    Dim ErrFlag As Boolean
    Dim ErrStack As String
    Dim ProtectedItem As Boolean = False

    ErrMessage = ""
    ErrStack = ""

    ' *************************************************************
    ' Appropriate Save permission :-
    ' *************************************************************

    If (Me.HasUpdatePermission = False) And (Me.HasInsertPermission = False) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
      Return True
      Exit Function
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    ' Confirm Save, if required.

    If (pConfirm = True) Then
      If MessageBox.Show("Save Covariance Matrix ?", "Save Matrix ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
        Return True
        Exit Function
      End If
    End If

    ' *************************************************************
    ' Procedure to Save the current form information.
    ' *************************************************************

    Dim StatusString As String

    If (FormIsValid = False) Then
      Return False
      Exit Function
    End If

    ' Validation
    StatusString = ""
    If ValidateForm(StatusString) = False Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
      Return False
      Exit Function
    End If


    ' *************************************************************
    '		
    ' *************************************************************

    Dim ThisGroupListID As Integer
    Dim ThisCovListID As Integer
    Dim ThisGroupStartDate As Date
    Dim ThisGroupEndDate As Date
    Dim ListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable

    Try
      If (Combo_SelectGroupID.SelectedValue Is Nothing) OrElse (IsNumeric(Combo_SelectGroupID.SelectedValue) = False) Then
        MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "No Group appears to be selected, Saving as Group Zero.", "", True)
        ThisGroupListID = 0
      Else
        ThisGroupListID = CInt(Combo_SelectGroupID.SelectedValue)
      End If

      If (Combo_CovarianceSave.SelectedValue Is Nothing) OrElse (IsNumeric(Combo_CovarianceSave.SelectedValue) = False) Then
        ThisCovListID = 0
      Else
        ThisCovListID = CInt(Combo_CovarianceSave.SelectedValue)
      End If

      If (IsDate(Label_GroupStartDate.Text) = False) Then
        ThisGroupStartDate = Renaissance_BaseDate
      Else
        ThisGroupStartDate = CDate(Label_GroupStartDate.Text)
      End If
      If (IsDate(Label_GroupEndDate.Text) = False) Then
        ThisGroupEndDate = Renaissance_EndDate_Data
      Else
        ThisGroupEndDate = CDate(Label_GroupEndDate.Text)
      End If

      ListTable = List_SelectItems.DataSource

      ' Set 'Paint' flag.
      InPaint = True

      ' Add / Edit CovarianceList

      Try
        Dim CovListDS As DSCovarianceList
        Dim CovListTbl As DSCovarianceList.tblCovarianceListDataTable
        Dim CovListRow As DSCovarianceList.tblCovarianceListRow
        Dim SelectedCovListRows() As DSCovarianceList.tblCovarianceListRow

        CovListDS = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblCovarianceList, False)
        If (CovListDS IsNot Nothing) Then
          CovListTbl = CovListDS.tblCovarianceList

          SyncLock CovListTbl
            CovListRow = Nothing

            If (Me.Combo_CovarianceSave.SelectedIndex > 0) Then
              ' Edit
              SelectedCovListRows = CovListTbl.Select("CovListID=" & ThisCovListID.ToString)

              If (SelectedCovListRows.Length > 0) Then
                CovListRow = SelectedCovListRows(0)
              End If
            End If

            If (CovListRow Is Nothing) OrElse (ThisCovListID <= 0) Then
              ' Add New
              CovListRow = CovListTbl.NewtblCovarianceListRow

              CovListRow.CovListID = 0
            End If

            CovListRow.ListName = Me.Combo_CovarianceSave.Text
            CovListRow.GroupListID = ThisGroupListID
            CovListRow.DateFrom = ThisGroupStartDate
            CovListRow.DateTo = ThisGroupEndDate
            CovListRow.Lamda = Text_Lamda.Value
            CovListRow.IsAnnualised = 0
            CovListRow.DataPeriod = CInt(DefaultStatsDatePeriod)

            If (CovListRow.RowState And DataRowState.Detached) = DataRowState.Detached Then
              CovListTbl.Rows.Add(CovListRow)
            End If

            MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblCovarianceList, New DataRow() {CovListRow})

            ThisCovListID = CovListRow.CovListID
          End SyncLock

        Else
          MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Failed to get tblCovarianceList (Mainform.LoadTable())", "", True)
          Return False
          Exit Function
        End If

      Catch ex As Exception
        MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, ex.Message, "Failed to Update tblCovarianceList.", ex.StackTrace, True)
        Return False
        Exit Function
      End Try

      ' Add / Edit tblCovariance

      If (ThisCovListID <= 0) Then
        MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Covariance ListID is Zero, Failed to update Covariance List ?. Save Aborted.", "", True)
        Return False
        Exit Function
      End If

      Try
        Dim CovDS As DSCovariance
        Dim CovTbl As DSCovariance.tblCovarianceDataTable
        Dim CovRow As DSCovariance.tblCovarianceRow
        Dim SelectedCovRows() As DSCovariance.tblCovarianceRow
        Dim ItemCounter As Integer

        Dim RowIndex As Integer
        Dim ColIndex As Integer
        Dim PertracID1 As Integer
        Dim PertracID1_BackFill As Integer
        Dim PertracID2 As Integer
        Dim PertracID2_BackFill As Integer
        Dim ThisComparison As StatFunctions.ComparisonStatsClass = Nothing

        CovDS = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblCovariance, False)

        If (CovDS IsNot Nothing) Then
          ' Get Grid IDs

          CovTbl = CovDS.tblCovariance
          CovRow = Nothing

          SyncLock CovTbl
            SelectedCovRows = CovTbl.Select("CovListID=" & ThisCovListID.ToString, "PertracID1,PertracID2")

            For ItemCounter = 0 To (SelectedCovRows.Length - 1)
              SelectedCovRows(ItemCounter).PertracID1 = 0
              SelectedCovRows(ItemCounter).PertracID2 = 0
              SelectedCovRows(ItemCounter).Covariance = 0
              SelectedCovRows(ItemCounter).Correlation = 0
            Next

            If (ListTable IsNot Nothing) AndAlso (ListTable.Rows.Count > 0) Then
              Dim ThisListRow As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow
              Dim SelectedPertracIDs(ListTable.Rows.Count - 1) As Integer
              Dim SelectedIndexPertracIDs(ListTable.Rows.Count - 1) As Integer
              Dim PertracCount As Integer = ListTable.Rows.Count

              For RowIndex = 0 To (PertracCount - 1)
                ThisListRow = ListTable.Rows(RowIndex)

                SelectedPertracIDs(RowIndex) = ThisListRow.GroupPertracCode
                SelectedIndexPertracIDs(RowIndex) = ThisListRow.GroupIndexCode
              Next

              ItemCounter = 0

              For RowIndex = 0 To (PertracCount - 1)
                PertracID1 = SelectedPertracIDs(RowIndex)
                PertracID1_BackFill = SelectedIndexPertracIDs(RowIndex)

                For ColIndex = 1 To RowIndex
                  PertracID2 = SelectedPertracIDs(ColIndex)
                  PertracID2_BackFill = SelectedIndexPertracIDs(ColIndex)

                  If (Me.Check_Covariance_BackfillData.Checked) Then
                    Dim CombinedPertrac1 As ULong = MainForm.StatFunctions.CombinePertracIDs(PertracID1, PertracID1_BackFill)
                    Dim CombinedPertrac2 As ULong = MainForm.StatFunctions.CombinePertracIDs(PertracID2, PertracID2_BackFill)

                    If (PertracID1 <= PertracID2) Then
                      ThisComparison = MainForm.StatFunctions.GetComparisonStatsItem(StatsDatePeriod, CombinedPertrac1, CombinedPertrac2, True, CombinedPertrac1, ThisGroupStartDate, ThisGroupEndDate, Text_Lamda.Value, MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), -1)
                    Else
                      ThisComparison = MainForm.StatFunctions.GetComparisonStatsItem(StatsDatePeriod, CombinedPertrac2, CombinedPertrac1, True, CombinedPertrac2, ThisGroupStartDate, ThisGroupEndDate, Text_Lamda.Value, MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), -1)
                    End If

                  Else
                    ThisComparison = MainForm.StatFunctions.GetComparisonStatsItem(StatsDatePeriod, CULng(MIN(PertracID1, PertracID2)), CULng(MAX(PertracID1, PertracID2)), False, CULng(MIN(PertracID1, PertracID2)), ThisGroupStartDate, ThisGroupEndDate, Text_Lamda.Value, MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), -1)
                  End If


                  If (ThisComparison IsNot Nothing) Then
                    ' Save Correlation
                    If (ItemCounter < SelectedCovRows.Length) Then
                      SelectedCovRows(ItemCounter).BeginEdit()

                      SelectedCovRows(ItemCounter).PertracID1 = MIN(PertracID1, PertracID2)
                      SelectedCovRows(ItemCounter).PertracID2 = MAX(PertracID1, PertracID2)
                      SelectedCovRows(ItemCounter).Covariance = ThisComparison.Covariance
                      SelectedCovRows(ItemCounter).Correlation = ThisComparison.Correlation

                      SelectedCovRows(ItemCounter).EndEdit()

                    Else
                      CovRow = CovTbl.NewtblCovarianceRow

                      CovRow.CovID = 0
                      CovRow.CovListID = ThisCovListID
                      CovRow.PertracID1 = MIN(PertracID1, PertracID2)
                      CovRow.PertracID2 = MAX(PertracID1, PertracID2)
                      CovRow.Covariance = ThisComparison.Covariance
                      CovRow.Correlation = ThisComparison.Correlation

                      CovTbl.Rows.Add(CovRow)
                    End If

                    ItemCounter += 1
                  End If
                Next
              Next

            End If

            ' Delete any un-used rows.

            While ItemCounter < SelectedCovRows.Length
              SelectedCovRows(ItemCounter).Delete()
            End While

            ' Update Rows

            MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblCovariance, CovTbl)

          End SyncLock

        End If

      Catch ex As Exception
        MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, ex.Message, "Failed to update tblCovariance.", ex.StackTrace, True)
        Return False
        Exit Function
      End Try

    Catch ex As Exception
      ErrMessage = ex.Message
      ErrFlag = True
      ErrStack = ex.StackTrace
    Finally
      InPaint = False
    End Try

    ' *************************************************************
    ' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
    ' *************************************************************

    If (ErrFlag = True) Then
      Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
    End If

    ' Finish off

    Me.THIS_FORM_SelectingCombo.Enabled = True

    ' Propagate changes

    Try
      Dim ThisEventArgs As New RenaissanceGlobals.RenaissanceUpdateEventArgs
      ThisEventArgs.TableChanged(RenaissanceChangeID.tblCovarianceList) = True
      ThisEventArgs.TableChanged(RenaissanceChangeID.tblCovariance) = True

      Call MainForm.Main_RaiseEvent(ThisEventArgs)
    Catch ex As Exception
    End Try

  End Function

	Private Sub SetButtonStatus()
		' ***********************************************************************************
		' Sets the status of the form controls appropriate to the current users 
		' permissions and the 'Changed' or 'New' status of the form.
		' ***********************************************************************************

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		If ((Me.HasUpdatePermission) Or (Me.HasInsertPermission)) And (Combo_SelectGroupID.SelectedIndex >= 0) Then
			Me.btnSave.Enabled = True
		Else
			Me.btnSave.Enabled = False
		End If

		If (Me.HasDeletePermission) Then
			btnDelete.Enabled = True
		Else
			btnDelete.Enabled = False
		End If

	End Sub

	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 


		Return RVal

	End Function

	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs)
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub

	Private Sub Combo_SelectGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectGroup.SelectedIndexChanged
		' ********************************************************************************************
		' This form allows the user to display only those Entities from a given group.
		' This combo controls this.
		'
		' ********************************************************************************************

		If (Me.Created = False) OrElse (InPaint = True) Then
			Exit Sub
		End If

		Call SetSortedRows()

		thisPosition = 0
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		If (thisPosition >= 0) Then
			thisDataRow = Me.SortedRows(thisPosition)
		Else
			thisDataRow = Nothing
		End If

    Call GetFormData(thisDataRow)
	End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

	Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' ***************************************************************************************************
		' Selection Combo. SelectedItem changed.
		'
		' ***************************************************************************************************

		' Don't react to changes made in paint routines etc.
		If InPaint = True Then Exit Sub

		' Find the correct data row, then show it...
		thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

		If (thisPosition >= 0) Then
			thisDataRow = Me.SortedRows(thisPosition)
		Else
			thisDataRow = Nothing
		End If

		Try
			Me.Cursor = Cursors.WaitCursor

      Call GetFormData(thisDataRow)
			btnSave.Enabled = True

		Catch ex As Exception
		Finally
			Me.Cursor = Cursors.Default
		End Try

		Me.Chart_Prices.Series.Clear()
		Me.Chart_Scatter.Series.Clear()

	End Sub


	Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
		' ***************************************************************************************************
		' 'Previous' Button.
		'
		' ***************************************************************************************************

		If thisPosition > 0 Then thisPosition -= 1
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		Exit Sub

		If myTable.Rows.Count > thisPosition Then
			thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
		Else
      Call GetFormData(Nothing)
		End If

	End Sub

	Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
		' ***************************************************************************************************
		' 'Next' Button.
		'
		' ***************************************************************************************************

		If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		Exit Sub

		If myTable.Rows.Count > thisPosition Then
			thisDataRow = myTable.Rows(thisPosition)
      Call GetFormData(thisDataRow)
		Else
      Call GetFormData(Nothing)
		End If

	End Sub

	Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
		' ***************************************************************************************************
		' 'First' Button.
		' ***************************************************************************************************

		thisPosition = 0
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

    If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
      Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
    Else
      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    End If

	End Sub

	Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
		' ***************************************************************************************************
		' 'Last' Button.
		' ***************************************************************************************************

		thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

	End Sub

	Private Function Get_Position(ByVal pAuditID As Integer) As Integer
		' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
		' AudidID.

		Dim MinIndex As Integer
		Dim MaxIndex As Integer
		Dim CurrentMin As Integer
		Dim CurrentMax As Integer
		Dim searchDataRow As DataRow

		Try
			' SortedRows exists ?

			If SortedRows Is Nothing Then
				Return (-1)
				Exit Function
			End If

			' Return (-1) if there are no rows in the 'SortedRows'

			If (SortedRows.GetLength(0) <= 0) Then
				Return (-1)
				Exit Function
			End If


			' Use a modified Search moving outwards from the last returned value to locate the required value.
			' Reflecting the fact that for the most part One looks for closely located records.

			MinIndex = 0
			MaxIndex = SortedRows.GetLength(0) - 1


			' Check First and Last records (Incase 'First' or 'Last' was pressed).

			searchDataRow = SortedRows(MinIndex)

			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
				Return MinIndex
				Exit Function
			End If

			searchDataRow = SortedRows(MaxIndex)
			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
				Return MaxIndex
				Exit Function
			End If

			' now search outwards from the last returned value.

			If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
				CurrentMin = thisPosition
				CurrentMax = CurrentMin + 1
			Else
				CurrentMin = CInt((MinIndex + MaxIndex) / 2)
				CurrentMax = CurrentMin + 1
			End If

			While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
				If (CurrentMin >= MinIndex) Then
					searchDataRow = SortedRows(CurrentMin)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
						Return CurrentMin
						Exit Function
					End If

					CurrentMin -= 1
				End If

				If (CurrentMax <= MaxIndex) Then
					searchDataRow = SortedRows(CurrentMax)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
						Return CurrentMax
						Exit Function
					End If

					CurrentMax += 1
				End If

			End While

			Return (-1)
			Exit Function

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
			Return (-1)
			Exit Function
		End Try

	End Function

#End Region

#Region " Form User Control Event Code (inc Grid Events)."

	Private Sub Radio_Correlation_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Correlation.CheckedChanged
		' *************************************************************
		'
		'
		' *************************************************************

		Radio_Covariance.Checked = Not Radio_Correlation.Checked

		If (Me.Created) And (InPaint = False) Then
      GetFullMatrix(DefaultStatsDatePeriod, List_SelectItems.DataSource, thisDataRow.GroupDateFrom, thisDataRow.GroupDateTo, StatusLabel_GroupReturns)
			SetSelectiveMatrix(List_SelectItems)
		End If
	End Sub

	Private Sub Radio_Covariance_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Covariance.CheckedChanged
		' *************************************************************
		'
		'
		' *************************************************************

		Radio_Correlation.Checked = Not Radio_Covariance.Checked

		If (Me.Created) And (InPaint = False) Then
      GetFullMatrix(DefaultStatsDatePeriod, List_SelectItems.DataSource, thisDataRow.GroupDateFrom, thisDataRow.GroupDateTo, StatusLabel_GroupReturns)
			SetSelectiveMatrix(List_SelectItems)
		End If
	End Sub

	Private Sub Check_Covariance_BackfillData_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_Covariance_BackfillData.CheckedChanged
		' ***************************************************************************
		'
		'
		' ***************************************************************************

		If (Me.Created) And (InPaint = False) Then
			If (Check_Covariance_BackfillData.Enabled) AndAlso (Check_Covariance_BackfillData.Focused) Then
        GetFullMatrix(DefaultStatsDatePeriod, List_SelectItems.DataSource, thisDataRow.GroupDateFrom, thisDataRow.GroupDateTo, StatusLabel_GroupReturns)
				SetSelectiveMatrix(List_SelectItems)
			End If
		End If

	End Sub

	Private Sub List_SelectItems_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles List_SelectItems.SelectedIndexChanged
		' *************************************************************
		'
		'
		' *************************************************************

		SetSelectiveMatrix(List_SelectItems)

		If Me.Grid_Selective.Focused Then
			Me.Chart_Prices.Series.Clear()
			Me.Chart_Scatter.Series.Clear()
		End If

	End Sub

	Private Sub Text_Lamda_ValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Text_Lamda.ValueChanged
		' *************************************************************
		'
		'
		' *************************************************************

		If (Me.Created) And (InPaint = False) Then
      GetFullMatrix(DefaultStatsDatePeriod, List_SelectItems.DataSource, thisDataRow.GroupDateFrom, thisDataRow.GroupDateTo, StatusLabel_GroupReturns)
			SetSelectiveMatrix(List_SelectItems)
		End If
	End Sub

	Private Sub Grid_FullMatrix_EnterCell(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_FullMatrix.EnterCell
		Call Grid_FullMatrix_Click(sender, e)
	End Sub

	Private Sub Grid_FullMatrix_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Grid_FullMatrix.VisibleChanged ' , Grid_FullMatrix.Click
		' *************************************************************
		'
		'
		' *************************************************************

		If (Me.Created) AndAlso (Me.InPaint = False) AndAlso (Grid_FullMatrix.Visible) Then
			If (Grid_FullMatrix.Row > 0) AndAlso (Grid_FullMatrix.Col > 0) Then
				Dim StartDate As Date = Now.AddYears(-3).Date
				Dim EndDate As Date = Now.Date
        Dim PertracID As Integer
        Dim StatsDatePeriod1 As DealingPeriod = DealingPeriod.Monthly
        Dim StatsDatePeriod2 As DealingPeriod = DealingPeriod.Monthly
        Dim StatsDatePeriodScatter As DealingPeriod = DealingPeriod.Monthly

				' Vami Chart
				Dim LabelString As String
				LabelString = Grid_FullMatrix.Item(Grid_FullMatrix.Row, 0)
				If (LabelString.Length > 30) Then
					LabelString = LabelString.Substring(0, 30)
        End If

        PertracID = CInt(Grid_FullMatrix.Item(Grid_FullMatrix.Row, Grid_FullMatrix.Cols.Count - 1))
        StatsDatePeriod1 = MainForm.PertracData.GetPertracDataPeriod(PertracID)

        Set_LineChart(MainForm, StatsDatePeriod1, PertracID, Chart_Prices, 0, LabelString, MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod1), 1, CONST_MatrixScalingFactor, StartDate, EndDate)

				LabelString = Grid_FullMatrix.Item(Grid_FullMatrix.Col, 0)
				If (LabelString.Length > 30) Then
					LabelString = LabelString.Substring(0, 30)
        End If

        PertracID = CInt(Grid_FullMatrix.Item(Grid_FullMatrix.Col, Grid_FullMatrix.Cols.Count - 1))
        StatsDatePeriod2 = MainForm.PertracData.GetPertracDataPeriod(PertracID)
        Set_LineChart(MainForm, StatsDatePeriod2, PertracID, Chart_Prices, 1, LabelString, MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod2), 1, CONST_MatrixScalingFactor, StartDate, EndDate)

        If (MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod1) < MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod2)) Then
          StatsDatePeriodScatter = StatsDatePeriod1
        Else
          StatsDatePeriodScatter = StatsDatePeriod2
        End If
        Set_ReturnScatterChart(MainForm, StatsDatePeriodScatter, CInt(Grid_FullMatrix.Item(Grid_FullMatrix.Row, Grid_FullMatrix.Cols.Count - 1)), CInt(Grid_FullMatrix.Item(Grid_FullMatrix.Col, Grid_FullMatrix.Cols.Count - 1)), Chart_Scatter, 0, Grid_FullMatrix.Item(Grid_FullMatrix.Row, 0), 1, 1, CONST_MatrixScalingFactor, CONST_MatrixScalingFactor, StartDate, EndDate)

      Else
        Me.Chart_Prices.Series.Clear()
        Me.Chart_Scatter.Series.Clear()
      End If
    End If
	End Sub

	Private Sub Grid_Selective_EnterCell(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Selective.EnterCell
		Call Grid_Selective_Click(sender, e)
	End Sub

	Private Sub Grid_Selective_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Grid_Selective.VisibleChanged ' Grid_Selective.Click, 
		' *************************************************************
		'
		'
		' *************************************************************

		If (Me.Created) AndAlso (Me.InPaint = False) AndAlso (Grid_Selective.Visible) Then
			If (Grid_Selective.Row > 0) AndAlso (Grid_Selective.Col > 0) Then
				Dim StartDate As Date = Now.AddYears(-3).Date
				Dim EndDate As Date = Now.Date
        Dim PertracID As Integer
        Dim StatsDatePeriod1 As DealingPeriod = DealingPeriod.Monthly
        Dim StatsDatePeriod2 As DealingPeriod = DealingPeriod.Monthly
        Dim StatsDatePeriodScatter As DealingPeriod = DealingPeriod.Monthly

        PertracID = CInt(Grid_Selective.Item(Grid_Selective.Row, Grid_Selective.Cols.Count - 1))
        StatsDatePeriod1 = MainForm.PertracData.GetPertracDataPeriod(PertracID)

				' Vami Chart
        Set_LineChart(MainForm, StatsDatePeriod1, PertracID, Chart_Prices, 0, Grid_Selective.Item(Grid_Selective.Row, 0), MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod1), 1, CONST_MatrixScalingFactor, StartDate, EndDate)

        PertracID = CInt(Grid_Selective.Item(Grid_Selective.Col, Grid_Selective.Cols.Count - 1))
        StatsDatePeriod2 = MainForm.PertracData.GetPertracDataPeriod(PertracID)

        Set_LineChart(MainForm, StatsDatePeriod2, PertracID, Chart_Prices, 1, Grid_Selective.Item(Grid_Selective.Col, 0), MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod2), 1, CONST_MatrixScalingFactor, StartDate, EndDate)

        If (MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod1) < MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod2)) Then
          StatsDatePeriodScatter = StatsDatePeriod1
        Else
          StatsDatePeriodScatter = StatsDatePeriod2
        End If

        Set_ReturnScatterChart(MainForm, StatsDatePeriodScatter, CInt(Grid_Selective.Item(Grid_Selective.Row, Grid_Selective.Cols.Count - 1)), CInt(Grid_Selective.Item(Grid_Selective.Col, Grid_Selective.Cols.Count - 1)), Chart_Scatter, 0, Grid_Selective.Item(Grid_Selective.Row, 0), 1, 1, CONST_MatrixScalingFactor, CONST_MatrixScalingFactor, StartDate, EndDate)

			Else
				Me.Chart_Prices.Series.Clear()
				Me.Chart_Scatter.Series.Clear()
			End If
		End If
	End Sub

	Private Sub Grid_FullMatrix_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_FullMatrix.KeyDown
		' *************************************************************************************
		' Clipboard 'Copy' functionality
		'
		'
		' *************************************************************************************
		Try
			If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False)
			End If
		Catch ex As Exception
		End Try
	End Sub

	Private Sub Grid_Selective_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Selective.KeyDown
		' *************************************************************************************
		' Clipboard 'Copy' functionality
		'
		'
		' *************************************************************************************
		Try
			If e.Control And (e.KeyCode = Keys.C) Then
        Call MainForm.CopyGridSelection(CType(sender, C1.Win.C1FlexGrid.C1FlexGrid), True, False)
			End If
		Catch ex As Exception
		End Try
	End Sub

	Private Sub List_SelectItems_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles List_SelectItems.KeyDown
		' *************************************************************************************
		' Clipboard 'Copy' functionality
		'
		'
		' *************************************************************************************
		Try
			If e.Control And (e.KeyCode = Keys.C) Then
				Call MainForm.CopyListSelection(CType(sender, ListBox))
			End If
		Catch ex As Exception
		End Try
	End Sub

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

	Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
		' *****************************************************************************************
		' Save Changes, if any, without prompting.
		' *****************************************************************************************

    Call SetFormData(DefaultStatsDatePeriod, False)

	End Sub

	Private Sub btnView_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnView.Click
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		btnSave.Enabled = False

		Try
			InPaint = True

			MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
		Catch ex As Exception
		Finally
			InPaint = False
		End Try

    GetFormData(Nothing)

		CovListIDToDisplay = 0
		If Combo_CovarianceSave.SelectedIndex > 0 Then
			If IsNumeric(Combo_CovarianceSave.SelectedValue) Then
				CovListIDToDisplay = CInt(Combo_CovarianceSave.SelectedValue)

				' Build List_SelectItems

        Build_ListSelectItems_FromCovListID(DefaultStatsDatePeriod, CovListIDToDisplay)

			End If
		End If

	End Sub

  Private Sub Build_ListSelectItems_FromCovListID(ByVal StatsDatePeriod As DealingPeriod, ByVal CovListIDToDisplay As Integer)
    ' ***************************************************************************************************
    ' Routine to populate form Controls given a CovListID.
    ' Form is cleared for invalid IDs or Invalid Form.
    ' ***************************************************************************************************

    Dim OrgInpaint As Boolean

    ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
    OrgInpaint = InPaint
    InPaint = True

    Try
      If (CovListIDToDisplay <= 0) OrElse (FormIsValid = False) OrElse (Me.InUse = False) Then
        ' Bad / New Datarow - Clear Form.

        thisAuditID = (-1)

        Me.Label_GroupStartDate.Text = ""
        Me.Label_GroupEndDate.Text = ""
        Text_Lamda.Value = 1

        List_SelectItems.DataSource = Nothing
        Me.List_SelectItems.Items.Clear()

        Me.Grid_FullMatrix.Rows.Count = 1
        Me.Grid_FullMatrix.Cols.Count = 1

        Me.Grid_Selective.Rows.Count = 1
        Me.Grid_Selective.Cols.Count = 1

        Me.THIS_FORM_SelectingCombo.Enabled = True

      Else
        ' Populate List with Group Items
        List_SelectItems.DataSource = Nothing
        Me.List_SelectItems.Items.Clear()

        ' First, Get CovList
        Dim CovListDS As DSCovarianceList
        Dim CovListTbl As DSCovarianceList.tblCovarianceListDataTable = Nothing
        Dim SelectedCovListRows(-1) As DSCovarianceList.tblCovarianceListRow

        CovListDS = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblCovarianceList, False)
        If (CovListDS IsNot Nothing) Then
          CovListTbl = CovListDS.tblCovarianceList
          SelectedCovListRows = CovListTbl.Select("CovListID=" & CovListIDToDisplay.ToString)
        End If

        If (SelectedCovListRows.Length > 0) Then
          Label_GroupStartDate.Text = SelectedCovListRows(0).DateFrom.ToString(DISPLAYMEMBER_DATEFORMAT)
          Label_GroupEndDate.Text = SelectedCovListRows(0).DateTo.ToString(DISPLAYMEMBER_DATEFORMAT)
          Text_Lamda.Value = SelectedCovListRows(0).Lamda
        Else
          Build_ListSelectItems_FromCovListID(StatsDatePeriod, -1)
          Exit Sub
        End If

        ' Populate List

        Dim tmpCommand As New SqlCommand
        Dim ListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable = Nothing

        Try

          tmpCommand.CommandType = CommandType.StoredProcedure
          tmpCommand.CommandText = "spu_CovariancePertracDetails"
          tmpCommand.Connection = MainForm.GetGenoaConnection
          tmpCommand.Parameters.Add("@CovListID", SqlDbType.Int).Value = CovListIDToDisplay
          tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
          tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

          List_SelectItems.SuspendLayout()

          If (List_SelectItems.DataSource Is Nothing) OrElse (Not (TypeOf List_SelectItems.DataSource Is RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)) Then
            ListTable = New RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable
            ListTable.Columns.Add("ListDescription", GetType(String))
            ListTable.Columns.Add("PertracName", GetType(String))
            ListTable.Columns.Add("PertracProvider", GetType(String))
            ListTable.Columns.Add("IndexName", GetType(String))
            ListTable.Columns.Add("IndexProvider", GetType(String))

            ' List_SelectItems.DataSource = ListTable
          Else
            ListTable = CType(List_SelectItems.DataSource, RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)
          End If

          ListTable.Rows.Clear()
          ListTable.Load(tmpCommand.ExecuteReader)

          Try
            If (ListTable IsNot List_SelectItems.DataSource) Then
              List_SelectItems.DataSource = Nothing
            End If
            If (List_SelectItems.DisplayMember <> "ListDescription") Then
              List_SelectItems.DisplayMember = "ListDescription"
            End If
            If (List_SelectItems.ValueMember <> "GroupPertracCode") Then
              List_SelectItems.ValueMember = "GroupPertracCode"
            End If
          Catch ex As Exception
          End Try

          List_SelectItems.DataSource = ListTable
          List_SelectItems.ResumeLayout()

        Catch ex As Exception

          Call MainForm.LogError(Me.Name, 0, ex.Message, "Error building Select List", ex.StackTrace, True)

        Finally
          Try
            If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
              tmpCommand.Connection.Close()
              tmpCommand.Connection = Nothing
            End If
          Catch ex As Exception
          End Try
        End Try

        ' Load Matrix Data

        Try
          If (ListTable IsNot Nothing) Then
            GetFullMatrix(StatsDatePeriod, ListTable, SelectedCovListRows(0).DateFrom, SelectedCovListRows(0).DateTo, StatusLabel_GroupReturns)
            SetSelectiveMatrix(List_SelectItems)
          Else
            Me.Grid_FullMatrix.Rows.Count = 1
            Me.Grid_FullMatrix.Cols.Count = 1

            Me.Grid_Selective.Rows.Count = 1
            Me.Grid_Selective.Cols.Count = 1
          End If

        Catch ex As Exception
          Call MainForm.LogError(Me.Name, 0, ex.Message, "Error building correlation grids", ex.StackTrace, True)
        End Try
      End If
    Catch ex As Exception
    Finally
      StatusLabel_GroupReturns.Text = ""

      ' Allow Field events to trigger before 'InPaint' Is re-set. 
      ' (Should) Prevent Validation errors during Form Draw.
      Application.DoEvents()

      ' Restore 'Paint' flag.
      InPaint = OrgInpaint
    End Try

    ' As it says on the can....
    Call SetButtonStatus()

  End Sub

	Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim ThisCovListID As Integer

		' No Appropriate Save permission :-

		If (Me.HasDeletePermission = False) Then
			MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "You do not have permission to Delete this Data Set.", "", True)
			Exit Sub
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", btnDelete()", LOG_LEVELS.Warning, "", "Changes to Genoa are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Exit Sub
		End If

		' *************************************************************
		' Confirm :-
		' *************************************************************
		If MsgBox("Are you Sure ?", MsgBoxStyle.OkCancel, "Confirm Delete") = MsgBoxResult.Cancel Then
			Exit Sub
		End If

		If (Combo_CovarianceSave.SelectedValue Is Nothing) OrElse (IsNumeric(Combo_CovarianceSave.SelectedValue) = False) Then
			Exit Sub
		Else
			ThisCovListID = CInt(Combo_CovarianceSave.SelectedValue)
		End If

		If (ThisCovListID <= 0) Then
			Exit Sub
		End If

		Dim CovListDS As DSCovarianceList
		Dim CovListTbl As DSCovarianceList.tblCovarianceListDataTable
		Dim SelectedCovListRows() As DSCovarianceList.tblCovarianceListRow

		CovListDS = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblCovarianceList, False)
		If (CovListDS IsNot Nothing) Then
			CovListTbl = CovListDS.tblCovarianceList

			SelectedCovListRows = CovListTbl.Select("CovListID=" & ThisCovListID.ToString)

			If (SelectedCovListRows.Length > 0) Then
				SelectedCovListRows(0).Delete()

				MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblCovarianceList, New DataRow() {SelectedCovListRows(0)})
			End If
		End If

		Try
			MainForm.ReloadTable(RenaissanceChangeID.tblCovarianceList, True)
			MainForm.ReloadTable(RenaissanceChangeID.tblCovariance, True)
		Catch ex As Exception
		End Try

	End Sub

	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' *****************************************************************************************
		' Close Form
		' *****************************************************************************************

		Me.Close()

	End Sub

#End Region

#Region " Bug hunting "

	' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
	' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
	' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

#End Region





End Class


