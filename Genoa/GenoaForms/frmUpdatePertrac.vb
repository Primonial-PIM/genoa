Imports System.IO
Imports System.Data.SqlClient
Imports RenaissanceGlobals



Public Class frmUpdatePertrac

	Inherits System.Windows.Forms.Form
	Implements StandardGenoaForm
	' Implements DTS.PackageEvents


#Region " Windows Form Designer generated code "

	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	Friend WithEvents Status1 As System.Windows.Forms.StatusStrip
	Friend WithEvents StatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
	Friend WithEvents lbl_EntityName As System.Windows.Forms.Label
	Friend WithEvents edit_PertracDBName As System.Windows.Forms.TextBox
	Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
	Friend WithEvents Button_SelectDBFile As System.Windows.Forms.Button
	Friend WithEvents Label6 As System.Windows.Forms.Label
	Friend WithEvents Combo_SelectInstrument As System.Windows.Forms.ComboBox
	Friend WithEvents Check_PertracPrecedence As System.Windows.Forms.CheckBox
	Friend WithEvents Check_DeleteGenoaOrphans As System.Windows.Forms.CheckBox
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents Label2 As System.Windows.Forms.Label
	Friend WithEvents Label3 As System.Windows.Forms.Label
	Friend WithEvents Check_JustCountUpdates As System.Windows.Forms.CheckBox
	Friend WithEvents btn_Update As System.Windows.Forms.Button
	Friend WithEvents editDataVendor As System.Windows.Forms.TextBox
	Friend WithEvents ToolStripProgressBar1 As System.Windows.Forms.ToolStripProgressBar
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.Status1 = New System.Windows.Forms.StatusStrip
		Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar
		Me.StatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
		Me.lbl_EntityName = New System.Windows.Forms.Label
		Me.edit_PertracDBName = New System.Windows.Forms.TextBox
		Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
		Me.Button_SelectDBFile = New System.Windows.Forms.Button
		Me.Label6 = New System.Windows.Forms.Label
		Me.Combo_SelectInstrument = New System.Windows.Forms.ComboBox
		Me.Check_PertracPrecedence = New System.Windows.Forms.CheckBox
		Me.Check_DeleteGenoaOrphans = New System.Windows.Forms.CheckBox
		Me.Label1 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Check_JustCountUpdates = New System.Windows.Forms.CheckBox
		Me.btn_Update = New System.Windows.Forms.Button
		Me.editDataVendor = New System.Windows.Forms.TextBox
		Me.Status1.SuspendLayout()
		Me.SuspendLayout()
		'
		'Status1
		'
		Me.Status1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripProgressBar1, Me.StatusLabel1})
		Me.Status1.Location = New System.Drawing.Point(0, 252)
		Me.Status1.Name = "Status1"
		Me.Status1.Size = New System.Drawing.Size(444, 22)
		Me.Status1.TabIndex = 69
		Me.Status1.Text = "StatusStrip1"
		'
		'ToolStripProgressBar1
		'
		Me.ToolStripProgressBar1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
		Me.ToolStripProgressBar1.Maximum = 20
		Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
		Me.ToolStripProgressBar1.Size = New System.Drawing.Size(150, 16)
		Me.ToolStripProgressBar1.Step = 1
		Me.ToolStripProgressBar1.Visible = False
		'
		'StatusLabel1
		'
		Me.StatusLabel1.Name = "StatusLabel1"
		Me.StatusLabel1.Size = New System.Drawing.Size(16, 17)
		Me.StatusLabel1.Text = "   "
		'
		'lbl_EntityName
		'
		Me.lbl_EntityName.Location = New System.Drawing.Point(3, 54)
		Me.lbl_EntityName.Name = "lbl_EntityName"
		Me.lbl_EntityName.Size = New System.Drawing.Size(145, 17)
		Me.lbl_EntityName.TabIndex = 88
		Me.lbl_EntityName.Text = "Import From Pertrac DB :"
		'
		'edit_PertracDBName
		'
		Me.edit_PertracDBName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.edit_PertracDBName.Location = New System.Drawing.Point(150, 50)
		Me.edit_PertracDBName.MaxLength = 100
		Me.edit_PertracDBName.Name = "edit_PertracDBName"
		Me.edit_PertracDBName.Size = New System.Drawing.Size(261, 20)
		Me.edit_PertracDBName.TabIndex = 87
		Me.edit_PertracDBName.Text = "M:\Pertrac\Master.mdb"
		'
		'OpenFileDialog1
		'
		Me.OpenFileDialog1.FileName = "OpenFileDialog1"
		'
		'Button_SelectDBFile
		'
		Me.Button_SelectDBFile.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Button_SelectDBFile.Location = New System.Drawing.Point(417, 50)
		Me.Button_SelectDBFile.Name = "Button_SelectDBFile"
		Me.Button_SelectDBFile.Size = New System.Drawing.Size(21, 20)
		Me.Button_SelectDBFile.TabIndex = 89
		Me.Button_SelectDBFile.Text = "..."
		Me.Button_SelectDBFile.UseVisualStyleBackColor = True
		'
		'Label6
		'
		Me.Label6.AutoSize = True
		Me.Label6.Location = New System.Drawing.Point(3, 84)
		Me.Label6.Name = "Label6"
		Me.Label6.Size = New System.Drawing.Size(100, 13)
		Me.Label6.TabIndex = 102
		Me.Label6.Text = "Update Instrument :"
		'
		'Combo_SelectInstrument
		'
		Me.Combo_SelectInstrument.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_SelectInstrument.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_SelectInstrument.Location = New System.Drawing.Point(150, 81)
		Me.Combo_SelectInstrument.Name = "Combo_SelectInstrument"
		Me.Combo_SelectInstrument.Size = New System.Drawing.Size(234, 21)
		Me.Combo_SelectInstrument.TabIndex = 101
		'
		'Check_PertracPrecedence
		'
		Me.Check_PertracPrecedence.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_PertracPrecedence.Location = New System.Drawing.Point(2, 117)
		Me.Check_PertracPrecedence.Name = "Check_PertracPrecedence"
		Me.Check_PertracPrecedence.Size = New System.Drawing.Size(162, 17)
		Me.Check_PertracPrecedence.TabIndex = 103
		Me.Check_PertracPrecedence.Text = "Pertrac has precedence"
		Me.Check_PertracPrecedence.UseVisualStyleBackColor = True
		'
		'Check_DeleteGenoaOrphans
		'
		Me.Check_DeleteGenoaOrphans.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_DeleteGenoaOrphans.Location = New System.Drawing.Point(2, 142)
		Me.Check_DeleteGenoaOrphans.Name = "Check_DeleteGenoaOrphans"
		Me.Check_DeleteGenoaOrphans.Size = New System.Drawing.Size(162, 17)
		Me.Check_DeleteGenoaOrphans.TabIndex = 104
		Me.Check_DeleteGenoaOrphans.Text = "Delete Genoa orphans"
		Me.Check_DeleteGenoaOrphans.UseVisualStyleBackColor = True
		'
		'Label1
		'
		Me.Label1.AutoSize = True
		Me.Label1.Location = New System.Drawing.Point(170, 118)
		Me.Label1.Name = "Label1"
		Me.Label1.Size = New System.Drawing.Size(208, 13)
		Me.Label1.TabIndex = 105
		Me.Label1.Text = "Overwrite Genoa prices with Pertrac prices"
		'
		'Label2
		'
		Me.Label2.AutoSize = True
		Me.Label2.Location = New System.Drawing.Point(170, 143)
		Me.Label2.Name = "Label2"
		Me.Label2.Size = New System.Drawing.Size(248, 13)
		Me.Label2.TabIndex = 106
		Me.Label2.Text = "Delete Genoa prices that do not exist in Pertrac DB"
		'
		'Label3
		'
		Me.Label3.Location = New System.Drawing.Point(3, 9)
		Me.Label3.Name = "Label3"
		Me.Label3.Size = New System.Drawing.Size(429, 29)
		Me.Label3.TabIndex = 107
		Me.Label3.Text = "Update Genoa prices with those from a Pertrac Database. Note that this does not a" & _
				"ffect 'User' series."
		'
		'Check_JustCountUpdates
		'
		Me.Check_JustCountUpdates.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_JustCountUpdates.Location = New System.Drawing.Point(2, 167)
		Me.Check_JustCountUpdates.Name = "Check_JustCountUpdates"
		Me.Check_JustCountUpdates.Size = New System.Drawing.Size(162, 17)
		Me.Check_JustCountUpdates.TabIndex = 108
		Me.Check_JustCountUpdates.Text = "Don't Update, Just count."
		Me.Check_JustCountUpdates.UseVisualStyleBackColor = True
		'
		'btn_Update
		'
		Me.btn_Update.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.btn_Update.Location = New System.Drawing.Point(150, 203)
		Me.btn_Update.Name = "btn_Update"
		Me.btn_Update.Size = New System.Drawing.Size(158, 28)
		Me.btn_Update.TabIndex = 109
		Me.btn_Update.Text = "&Update"
		'
		'editDataVendor
		'
		Me.editDataVendor.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.editDataVendor.Enabled = False
		Me.editDataVendor.Location = New System.Drawing.Point(390, 81)
		Me.editDataVendor.Name = "editDataVendor"
		Me.editDataVendor.Size = New System.Drawing.Size(48, 20)
		Me.editDataVendor.TabIndex = 110
		'
		'frmUpdatePertrac
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(444, 274)
		Me.Controls.Add(Me.editDataVendor)
		Me.Controls.Add(Me.btn_Update)
		Me.Controls.Add(Me.Check_JustCountUpdates)
		Me.Controls.Add(Me.Label3)
		Me.Controls.Add(Me.Label2)
		Me.Controls.Add(Me.Label1)
		Me.Controls.Add(Me.Check_DeleteGenoaOrphans)
		Me.Controls.Add(Me.Check_PertracPrecedence)
		Me.Controls.Add(Me.Label6)
		Me.Controls.Add(Me.Combo_SelectInstrument)
		Me.Controls.Add(Me.Button_SelectDBFile)
		Me.Controls.Add(Me.edit_PertracDBName)
		Me.Controls.Add(Me.Status1)
		Me.Controls.Add(Me.lbl_EntityName)
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.MinimumSize = New System.Drawing.Size(450, 250)
		Me.Name = "frmUpdatePertrac"
		Me.Text = "Update Genoa Pertrac Data"
		Me.Status1.ResumeLayout(False)
		Me.Status1.PerformLayout()
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
	Private WithEvents _MainForm As GenoaMain

	' Form ToolTip
	Private FormTooltip As New ToolTip()

	Private FormControls As ArrayList = Nothing
	Private WithEvents PackageTimer As New Windows.Forms.Timer()

	' Form Constants, specific to the table being updated.

	Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

	' The standard ChangeID for this form. e.g. tblPerson
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	' Form specific Permissioning variables
	Private THIS_FORM_PermissionArea As String
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
	Private THIS_FORM_FormID As GenoaFormID

	' Form Status Flags

	Private FormIsValid As Boolean
	Private FormChanged As Boolean
	Private _FormOpenFailed As Boolean

	' User Permission Flags

	Private HasReadPermission As Boolean
	Private HasUpdatePermission As Boolean
	Private HasInsertPermission As Boolean
	Private HasDeletePermission As Boolean

	' Data Structures
	Private PertracInstruments As DataView = Nothing ' Manages Select List for Pertrac Instruments.

#End Region

#Region " Form 'Properties' "

	Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

	Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)
		End Set
	End Property

	Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
		Get
			Return False
		End Get
	End Property

	Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
		Get
			Return True
		End Get
	End Property

	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

	Public Sub New(ByVal pMainForm As GenoaMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate
		AddHandler PackageTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

		_FormOpenFailed = False

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = GenoaFormID.frmInformationReport

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		AddHandler Combo_SelectInstrument.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_SelectInstrument.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_SelectInstrument.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_SelectInstrument.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
	Public Sub ResetForm() Implements StandardGenoaForm.ResetForm

		Call Form_Load(Me, New System.EventArgs)
	End Sub

	Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If


		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Combos

		Try

			SetPertracMasternameCombo()
			MainForm.SetComboSelectionLengths(Me)

			If Me.Combo_SelectInstrument.Items.Count > 0 Then
				Me.Combo_SelectInstrument.SelectedIndex = 0
			End If

		Catch ex As Exception
		End Try

		Try

			Me.Check_PertracPrecedence.Checked = True
			Me.Check_DeleteGenoaOrphans.Checked = False
			Me.Check_JustCountUpdates.Checked = False

		Catch ex As Exception
		End Try

	End Sub

	Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				'RemoveHandler Combo_SelectFrom.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				'RemoveHandler Combo_SelectFrom.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				'RemoveHandler Combo_SelectFrom.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				'RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler PackageTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'GenoaAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		'' Changes to the tblGenoaItems table :-
		'If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGenoaItems) = True) Or KnowledgeDateChanged Then

		'	' Re-Set combo.
		'	Call SetEntityCombo()
		'End If


		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If

		' ****************************************************************
		'
		' ****************************************************************


	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Check User permissions
	Private Sub CheckPermissions()
		' *****************************************************************************
		'
		' *****************************************************************************

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

	Private Sub SetPertracMasternameCombo()

		Call MainForm.SetMasterNameCombo( _
		 Me.Combo_SelectInstrument, _
		 "ID<1000000", True, 0, "All Instruments")	 ' 

	End Sub

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' *****************************************************************************
		' Close Form
		' *****************************************************************************

		Me.Close()

	End Sub

#End Region

#Region " Form Control Event Code"

	Private Sub Combo_SelectInstrument_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_SelectInstrument.SelectedIndexChanged
		' ****************************************************************
		'
		' ****************************************************************

		Try
			If (Me.Created) AndAlso (Not Me.IsDisposed) Then
				If (Combo_SelectInstrument.SelectedIndex > 0) AndAlso (Combo_SelectInstrument.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_SelectInstrument.SelectedValue)) Then

					Dim ThisInformationRow As RenaissanceDataClass.DSInformation.tblInformationRow
					ThisInformationRow = MainForm.PertracData.GetInformationRow(CInt(Combo_SelectInstrument.SelectedValue))

					If (editDataVendor.Text <> Nz(ThisInformationRow.DataVendorName, ".")) Then
						editDataVendor.Text = Nz(ThisInformationRow.DataVendorName, ".")
					End If

				Else

					editDataVendor.Text = ""

				End If
			End If
		Catch ex As Exception
		End Try

	End Sub

	Private Sub btn_Update_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_Update.Click
		' ****************************************************************
		'
		' ****************************************************************

    Try
      FormControls = MainForm.DisableFormControls(Me)

      If (Combo_SelectInstrument.SelectedValue IsNot Nothing) AndAlso IsNumeric(Combo_SelectInstrument.SelectedValue) Then
        UpdatePertracPerformance(MainForm, Me.edit_PertracDBName.Text, CInt(Me.Combo_SelectInstrument.SelectedValue), Me.Check_PertracPrecedence.Checked, Me.Check_DeleteGenoaOrphans.Checked, Me.Check_JustCountUpdates.Checked, Me.StatusLabel1)
      Else
        UpdatePertracPerformance(MainForm, Me.edit_PertracDBName.Text, 0, Me.Check_PertracPrecedence.Checked, Me.Check_DeleteGenoaOrphans.Checked, Me.Check_JustCountUpdates.Checked, Me.StatusLabel1)
      End If

    Catch ex As Exception
      MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error updating Pertrac prices.", ex.StackTrace, True)
    Finally
      MainForm.EnableFormControls(FormControls)
    End Try
	End Sub

	Private Sub Button_SelectDBFile_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_SelectDBFile.Click
		' ****************************************************************
		'
		' ****************************************************************

		Try
			OpenFileDialog1.CheckFileExists = True
			OpenFileDialog1.CheckPathExists = True
			OpenFileDialog1.DefaultExt = "mdb"
			OpenFileDialog1.FileName = edit_PertracDBName.Text

			If (edit_PertracDBName.Text.Length > 0) Then
				OpenFileDialog1.InitialDirectory = Path.GetDirectoryName(edit_PertracDBName.Text)
			Else
				OpenFileDialog1.InitialDirectory = Application.ExecutablePath
			End If
			OpenFileDialog1.Multiselect = False
			OpenFileDialog1.Title = "Select Pertrac DB"

			If OpenFileDialog1.ShowDialog = Windows.Forms.DialogResult.OK Then
				edit_PertracDBName.Text = OpenFileDialog1.FileName
			End If

		Catch ex As Exception
		End Try

	End Sub

#End Region







End Class



