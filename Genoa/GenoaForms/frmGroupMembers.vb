Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass

Imports Dundas.Charting.WinControl

Public Class frmGroupMembers

  Inherits System.Windows.Forms.Form
  Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

  Private Sub New()

    MyBase.New()

    'This call is required by the Windows Form Designer.

    InitializeComponent()
    
    'Add any initialization after the InitializeComponent() call

  End Sub

  'Form overrides dispose to clean up the component list.
  Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
    If disposing Then
      If Not (components Is Nothing) Then
        components.Dispose()
      End If
    End If
    MyBase.Dispose(disposing)
  End Sub

  'Required by the Windows Form Designer
  Private components As System.ComponentModel.IContainer

  'NOTE: The following procedure is required by the Windows Form Designer
  'It can be modified using the Windows Form Designer.  
  'Do not modify it using the code editor.
  Friend WithEvents editAuditID As System.Windows.Forms.TextBox
  Friend WithEvents btnNavFirst As System.Windows.Forms.Button
  Friend WithEvents btnNavPrev As System.Windows.Forms.Button
  Friend WithEvents btnNavNext As System.Windows.Forms.Button
  Friend WithEvents btnLast As System.Windows.Forms.Button
  Friend WithEvents btnCancel As System.Windows.Forms.Button
  Friend WithEvents btnSave As System.Windows.Forms.Button
  Friend WithEvents Combo_SelectGroupID As System.Windows.Forms.ComboBox
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
  Friend WithEvents Label1 As System.Windows.Forms.Label
  Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
  Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
  Friend WithEvents lbl_GroupName As System.Windows.Forms.Label
  Friend WithEvents Label6 As System.Windows.Forms.Label
  Friend WithEvents Combo_SelectGroup As System.Windows.Forms.ComboBox
  Friend WithEvents Split_GroupEdit As System.Windows.Forms.SplitContainer
  Friend WithEvents List_GroupItems As System.Windows.Forms.ListBox
  Friend WithEvents Button_RemoveAllItems As System.Windows.Forms.Button
  Friend WithEvents Button_RemoveItem As System.Windows.Forms.Button
  Friend WithEvents Button_AddItem As System.Windows.Forms.Button
  Friend WithEvents Button_AddAllItems As System.Windows.Forms.Button
  Friend WithEvents List_SelectItems As System.Windows.Forms.ListBox
  Friend WithEvents Label2 As System.Windows.Forms.Label
  Friend WithEvents Radio_ExistingGroups As System.Windows.Forms.RadioButton
  Friend WithEvents Radio_Pertrac As System.Windows.Forms.RadioButton
  Friend WithEvents Combo_SelectFrom As System.Windows.Forms.ComboBox
  Friend WithEvents Radio_ExistingFunds As System.Windows.Forms.RadioButton
  Friend WithEvents Btn_ShowCarts As System.Windows.Forms.Button
  Friend WithEvents Split_Charts As System.Windows.Forms.SplitContainer
  Friend WithEvents Chart_Returns As Dundas.Charting.WinControl.Chart
  Friend WithEvents Chart_Prices As Dundas.Charting.WinControl.Chart
  Friend WithEvents Split_All As System.Windows.Forms.SplitContainer
  Friend WithEvents Check_TransferDetails As System.Windows.Forms.CheckBox
	Friend WithEvents MenuPeriods As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents btn_NewGroup As System.Windows.Forms.Button
	Friend WithEvents Date_ValueDate As System.Windows.Forms.DateTimePicker
	Friend WithEvents btnClose As System.Windows.Forms.Button
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim ChartArea1 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend1 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series1 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series2 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim ChartArea2 As Dundas.Charting.WinControl.ChartArea = New Dundas.Charting.WinControl.ChartArea
    Dim Legend2 As Dundas.Charting.WinControl.Legend = New Dundas.Charting.WinControl.Legend
    Dim Series3 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Dim Series4 As Dundas.Charting.WinControl.Series = New Dundas.Charting.WinControl.Series
    Me.editAuditID = New System.Windows.Forms.TextBox
    Me.btnNavFirst = New System.Windows.Forms.Button
    Me.btnNavPrev = New System.Windows.Forms.Button
    Me.btnNavNext = New System.Windows.Forms.Button
    Me.btnLast = New System.Windows.Forms.Button
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.Combo_SelectGroupID = New System.Windows.Forms.ComboBox
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.btn_NewGroup = New System.Windows.Forms.Button
    Me.Label1 = New System.Windows.Forms.Label
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.btnClose = New System.Windows.Forms.Button
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.MenuPeriods = New System.Windows.Forms.ToolStripMenuItem
    Me.lbl_GroupName = New System.Windows.Forms.Label
    Me.Label6 = New System.Windows.Forms.Label
    Me.Combo_SelectGroup = New System.Windows.Forms.ComboBox
    Me.Split_GroupEdit = New System.Windows.Forms.SplitContainer
    Me.Button_RemoveAllItems = New System.Windows.Forms.Button
    Me.Button_RemoveItem = New System.Windows.Forms.Button
    Me.Button_AddItem = New System.Windows.Forms.Button
    Me.Button_AddAllItems = New System.Windows.Forms.Button
    Me.List_GroupItems = New System.Windows.Forms.ListBox
    Me.Date_ValueDate = New System.Windows.Forms.DateTimePicker
    Me.Check_TransferDetails = New System.Windows.Forms.CheckBox
    Me.Radio_ExistingFunds = New System.Windows.Forms.RadioButton
    Me.Radio_ExistingGroups = New System.Windows.Forms.RadioButton
    Me.Radio_Pertrac = New System.Windows.Forms.RadioButton
    Me.Combo_SelectFrom = New System.Windows.Forms.ComboBox
    Me.List_SelectItems = New System.Windows.Forms.ListBox
    Me.Label2 = New System.Windows.Forms.Label
    Me.Btn_ShowCarts = New System.Windows.Forms.Button
    Me.Split_Charts = New System.Windows.Forms.SplitContainer
    Me.Chart_Returns = New Dundas.Charting.WinControl.Chart
    Me.Chart_Prices = New Dundas.Charting.WinControl.Chart
    Me.Split_All = New System.Windows.Forms.SplitContainer
    Me.Panel1.SuspendLayout()
    Me.RootMenu.SuspendLayout()
    Me.Split_GroupEdit.Panel1.SuspendLayout()
    Me.Split_GroupEdit.Panel2.SuspendLayout()
    Me.Split_GroupEdit.SuspendLayout()
    Me.Split_Charts.Panel1.SuspendLayout()
    Me.Split_Charts.Panel2.SuspendLayout()
    Me.Split_Charts.SuspendLayout()
    CType(Me.Chart_Returns, System.ComponentModel.ISupportInitialize).BeginInit()
    CType(Me.Chart_Prices, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Split_All.Panel1.SuspendLayout()
    Me.Split_All.Panel2.SuspendLayout()
    Me.Split_All.SuspendLayout()
    Me.SuspendLayout()
    '
    'editAuditID
    '
    Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.editAuditID.Enabled = False
    Me.editAuditID.Location = New System.Drawing.Point(907, 36)
    Me.editAuditID.Name = "editAuditID"
    Me.editAuditID.Size = New System.Drawing.Size(50, 20)
    Me.editAuditID.TabIndex = 10
    Me.editAuditID.Visible = False
    '
    'btnNavFirst
    '
    Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavFirst.Location = New System.Drawing.Point(3, 3)
    Me.btnNavFirst.Name = "btnNavFirst"
    Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
    Me.btnNavFirst.TabIndex = 0
    Me.btnNavFirst.Text = "<<"
    '
    'btnNavPrev
    '
    Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavPrev.Location = New System.Drawing.Point(45, 3)
    Me.btnNavPrev.Name = "btnNavPrev"
    Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
    Me.btnNavPrev.TabIndex = 1
    Me.btnNavPrev.Text = "<"
    '
    'btnNavNext
    '
    Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavNext.Location = New System.Drawing.Point(83, 3)
    Me.btnNavNext.Name = "btnNavNext"
    Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
    Me.btnNavNext.TabIndex = 2
    Me.btnNavNext.Text = ">"
    '
    'btnLast
    '
    Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnLast.Location = New System.Drawing.Point(119, 3)
    Me.btnLast.Name = "btnLast"
    Me.btnLast.Size = New System.Drawing.Size(40, 28)
    Me.btnLast.TabIndex = 3
    Me.btnLast.Text = ">>"
    '
    'btnCancel
    '
    Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(177, 694)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 7
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSave
    '
    Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(5, 694)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 28)
    Me.btnSave.TabIndex = 6
    Me.btnSave.Text = "&Save"
    '
    'Combo_SelectGroupID
    '
    Me.Combo_SelectGroupID.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectGroupID.Location = New System.Drawing.Point(121, 60)
    Me.Combo_SelectGroupID.Name = "Combo_SelectGroupID"
    Me.Combo_SelectGroupID.Size = New System.Drawing.Size(318, 21)
    Me.Combo_SelectGroupID.TabIndex = 1
    '
    'Panel1
    '
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel1.Controls.Add(Me.btn_NewGroup)
    Me.Panel1.Controls.Add(Me.btnNavFirst)
    Me.Panel1.Controls.Add(Me.btnNavPrev)
    Me.Panel1.Controls.Add(Me.btnNavNext)
    Me.Panel1.Controls.Add(Me.btnLast)
    Me.Panel1.Location = New System.Drawing.Point(457, 36)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(245, 36)
    Me.Panel1.TabIndex = 2
    '
    'btn_NewGroup
    '
    Me.btn_NewGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btn_NewGroup.Location = New System.Drawing.Point(166, 3)
    Me.btn_NewGroup.Name = "btn_NewGroup"
    Me.btn_NewGroup.Size = New System.Drawing.Size(74, 28)
    Me.btn_NewGroup.TabIndex = 4
    Me.btn_NewGroup.Text = "New Group"
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(6, 63)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(69, 18)
    Me.Label1.TabIndex = 12
    Me.Label1.Text = "Select"
    '
    'GroupBox1
    '
    Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox1.Location = New System.Drawing.Point(5, 86)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(943, 10)
    Me.GroupBox1.TabIndex = 13
    Me.GroupBox1.TabStop = False
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(261, 694)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 8
    Me.btnClose.Text = "&Close"
    '
    'RootMenu
    '
    Me.RootMenu.AllowMerge = False
    Me.RootMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MenuPeriods})
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(959, 24)
    Me.RootMenu.TabIndex = 9
    Me.RootMenu.Text = "MenuStrip1"
    '
    'MenuPeriods
    '
    Me.MenuPeriods.Name = "MenuPeriods"
    Me.MenuPeriods.Size = New System.Drawing.Size(88, 20)
    Me.MenuPeriods.Text = "Graph Period"
    '
    'lbl_GroupName
    '
    Me.lbl_GroupName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.lbl_GroupName.Location = New System.Drawing.Point(6, 99)
    Me.lbl_GroupName.Name = "lbl_GroupName"
    Me.lbl_GroupName.Size = New System.Drawing.Size(942, 19)
    Me.lbl_GroupName.TabIndex = 4
    Me.lbl_GroupName.Text = "Group Name"
    '
    'Label6
    '
    Me.Label6.AutoSize = True
    Me.Label6.Location = New System.Drawing.Point(6, 36)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(69, 13)
    Me.Label6.TabIndex = 11
    Me.Label6.Text = "Select Group"
    '
    'Combo_SelectGroup
    '
    Me.Combo_SelectGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectGroup.Location = New System.Drawing.Point(121, 33)
    Me.Combo_SelectGroup.Name = "Combo_SelectGroup"
    Me.Combo_SelectGroup.Size = New System.Drawing.Size(318, 21)
    Me.Combo_SelectGroup.TabIndex = 0
    '
    'Split_GroupEdit
    '
    Me.Split_GroupEdit.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Split_GroupEdit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Split_GroupEdit.Location = New System.Drawing.Point(0, 0)
    Me.Split_GroupEdit.Name = "Split_GroupEdit"
    '
    'Split_GroupEdit.Panel1
    '
    Me.Split_GroupEdit.Panel1.Controls.Add(Me.Button_RemoveAllItems)
    Me.Split_GroupEdit.Panel1.Controls.Add(Me.Button_RemoveItem)
    Me.Split_GroupEdit.Panel1.Controls.Add(Me.Button_AddItem)
    Me.Split_GroupEdit.Panel1.Controls.Add(Me.Button_AddAllItems)
    Me.Split_GroupEdit.Panel1.Controls.Add(Me.List_GroupItems)
    Me.Split_GroupEdit.Panel1MinSize = 100
    '
    'Split_GroupEdit.Panel2
    '
    Me.Split_GroupEdit.Panel2.Controls.Add(Me.Date_ValueDate)
    Me.Split_GroupEdit.Panel2.Controls.Add(Me.Check_TransferDetails)
    Me.Split_GroupEdit.Panel2.Controls.Add(Me.Radio_ExistingFunds)
    Me.Split_GroupEdit.Panel2.Controls.Add(Me.Radio_ExistingGroups)
    Me.Split_GroupEdit.Panel2.Controls.Add(Me.Radio_Pertrac)
    Me.Split_GroupEdit.Panel2.Controls.Add(Me.Combo_SelectFrom)
    Me.Split_GroupEdit.Panel2.Controls.Add(Me.List_SelectItems)
    Me.Split_GroupEdit.Panel2.Controls.Add(Me.Label2)
    Me.Split_GroupEdit.Panel2MinSize = 100
    Me.Split_GroupEdit.Size = New System.Drawing.Size(618, 562)
    Me.Split_GroupEdit.SplitterDistance = 327
    Me.Split_GroupEdit.SplitterWidth = 2
    Me.Split_GroupEdit.TabIndex = 0
    '
    'Button_RemoveAllItems
    '
    Me.Button_RemoveAllItems.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Button_RemoveAllItems.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_RemoveAllItems.Location = New System.Drawing.Point(280, 131)
    Me.Button_RemoveAllItems.Name = "Button_RemoveAllItems"
    Me.Button_RemoveAllItems.Size = New System.Drawing.Size(40, 28)
    Me.Button_RemoveAllItems.TabIndex = 4
    Me.Button_RemoveAllItems.Text = ">>"
    '
    'Button_RemoveItem
    '
    Me.Button_RemoveItem.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Button_RemoveItem.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_RemoveItem.Location = New System.Drawing.Point(280, 97)
    Me.Button_RemoveItem.Name = "Button_RemoveItem"
    Me.Button_RemoveItem.Size = New System.Drawing.Size(40, 28)
    Me.Button_RemoveItem.TabIndex = 3
    Me.Button_RemoveItem.Text = ">"
    '
    'Button_AddItem
    '
    Me.Button_AddItem.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Button_AddItem.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_AddItem.Location = New System.Drawing.Point(280, 53)
    Me.Button_AddItem.Name = "Button_AddItem"
    Me.Button_AddItem.Size = New System.Drawing.Size(40, 28)
    Me.Button_AddItem.TabIndex = 2
    Me.Button_AddItem.Text = "<"
    '
    'Button_AddAllItems
    '
    Me.Button_AddAllItems.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Button_AddAllItems.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_AddAllItems.Location = New System.Drawing.Point(280, 19)
    Me.Button_AddAllItems.Name = "Button_AddAllItems"
    Me.Button_AddAllItems.Size = New System.Drawing.Size(40, 28)
    Me.Button_AddAllItems.TabIndex = 1
    Me.Button_AddAllItems.Text = "<<"
    '
    'List_GroupItems
    '
    Me.List_GroupItems.AllowDrop = True
    Me.List_GroupItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.List_GroupItems.FormattingEnabled = True
    Me.List_GroupItems.Location = New System.Drawing.Point(0, 0)
    Me.List_GroupItems.Name = "List_GroupItems"
    Me.List_GroupItems.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
    Me.List_GroupItems.Size = New System.Drawing.Size(274, 563)
    Me.List_GroupItems.Sorted = True
    Me.List_GroupItems.TabIndex = 0
    '
    'Date_ValueDate
    '
    Me.Date_ValueDate.CustomFormat = "dd MMM yyyy"
    Me.Date_ValueDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
    Me.Date_ValueDate.Location = New System.Drawing.Point(162, 47)
    Me.Date_ValueDate.Name = "Date_ValueDate"
    Me.Date_ValueDate.Size = New System.Drawing.Size(120, 20)
    Me.Date_ValueDate.TabIndex = 5
    '
    'Check_TransferDetails
    '
    Me.Check_TransferDetails.AutoSize = True
    Me.Check_TransferDetails.Location = New System.Drawing.Point(162, 4)
    Me.Check_TransferDetails.Name = "Check_TransferDetails"
    Me.Check_TransferDetails.Size = New System.Drawing.Size(100, 17)
    Me.Check_TransferDetails.TabIndex = 2
    Me.Check_TransferDetails.Text = "Transfer Details"
    Me.Check_TransferDetails.UseVisualStyleBackColor = True
    Me.Check_TransferDetails.Visible = False
    '
    'Radio_ExistingFunds
    '
    Me.Radio_ExistingFunds.AutoSize = True
    Me.Radio_ExistingFunds.Location = New System.Drawing.Point(51, 49)
    Me.Radio_ExistingFunds.Name = "Radio_ExistingFunds"
    Me.Radio_ExistingFunds.Size = New System.Drawing.Size(106, 17)
    Me.Radio_ExistingFunds.TabIndex = 4
    Me.Radio_ExistingFunds.TabStop = True
    Me.Radio_ExistingFunds.Text = "Existing Positions"
    Me.Radio_ExistingFunds.UseVisualStyleBackColor = True
    '
    'Radio_ExistingGroups
    '
    Me.Radio_ExistingGroups.AutoSize = True
    Me.Radio_ExistingGroups.Location = New System.Drawing.Point(51, 3)
    Me.Radio_ExistingGroups.Name = "Radio_ExistingGroups"
    Me.Radio_ExistingGroups.Size = New System.Drawing.Size(98, 17)
    Me.Radio_ExistingGroups.TabIndex = 1
    Me.Radio_ExistingGroups.TabStop = True
    Me.Radio_ExistingGroups.Text = "Existing Groups"
    Me.Radio_ExistingGroups.UseVisualStyleBackColor = True
    '
    'Radio_Pertrac
    '
    Me.Radio_Pertrac.AutoSize = True
    Me.Radio_Pertrac.Location = New System.Drawing.Point(51, 26)
    Me.Radio_Pertrac.Name = "Radio_Pertrac"
    Me.Radio_Pertrac.Size = New System.Drawing.Size(59, 17)
    Me.Radio_Pertrac.TabIndex = 3
    Me.Radio_Pertrac.TabStop = True
    Me.Radio_Pertrac.Text = "Pertrac"
    Me.Radio_Pertrac.UseVisualStyleBackColor = True
    '
    'Combo_SelectFrom
    '
    Me.Combo_SelectFrom.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SelectFrom.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectFrom.Location = New System.Drawing.Point(3, 70)
    Me.Combo_SelectFrom.Name = "Combo_SelectFrom"
    Me.Combo_SelectFrom.Size = New System.Drawing.Size(279, 21)
    Me.Combo_SelectFrom.TabIndex = 6
    '
    'List_SelectItems
    '
    Me.List_SelectItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.List_SelectItems.FormattingEnabled = True
    Me.List_SelectItems.Location = New System.Drawing.Point(0, 97)
    Me.List_SelectItems.Name = "List_SelectItems"
    Me.List_SelectItems.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
    Me.List_SelectItems.Size = New System.Drawing.Size(283, 485)
    Me.List_SelectItems.Sorted = True
    Me.List_SelectItems.TabIndex = 7
    '
    'Label2
    '
    Me.Label2.Location = New System.Drawing.Point(0, 5)
    Me.Label2.Name = "Label2"
    Me.Label2.Size = New System.Drawing.Size(46, 61)
    Me.Label2.TabIndex = 0
    Me.Label2.Text = "Select from :"
    '
    'Btn_ShowCarts
    '
    Me.Btn_ShowCarts.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Btn_ShowCarts.Location = New System.Drawing.Point(736, 40)
    Me.Btn_ShowCarts.Name = "Btn_ShowCarts"
    Me.Btn_ShowCarts.Size = New System.Drawing.Size(96, 28)
    Me.Btn_ShowCarts.TabIndex = 3
    Me.Btn_ShowCarts.Text = "Hide Charts"
    '
    'Split_Charts
    '
    Me.Split_Charts.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Split_Charts.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Split_Charts.Location = New System.Drawing.Point(0, 0)
    Me.Split_Charts.Name = "Split_Charts"
    Me.Split_Charts.Orientation = System.Windows.Forms.Orientation.Horizontal
    '
    'Split_Charts.Panel1
    '
    Me.Split_Charts.Panel1.Controls.Add(Me.Chart_Returns)
    Me.Split_Charts.Panel1MinSize = 100
    '
    'Split_Charts.Panel2
    '
    Me.Split_Charts.Panel2.Controls.Add(Me.Chart_Prices)
    Me.Split_Charts.Panel2MinSize = 100
    Me.Split_Charts.Size = New System.Drawing.Size(337, 565)
    Me.Split_Charts.SplitterDistance = 279
    Me.Split_Charts.SplitterWidth = 2
    Me.Split_Charts.TabIndex = 0
    '
    'Chart_Returns
    '
    Me.Chart_Returns.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Returns.BackColor = System.Drawing.Color.Azure
    Me.Chart_Returns.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Returns.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Returns.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Returns.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Returns.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea1.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea1.AxisX.LabelStyle.Format = "Y"
    ChartArea1.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea1.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.LabelStyle.Format = "P1"
    ChartArea1.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea1.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea1.AxisY.StartFromZero = False
    ChartArea1.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea1.BackColor = System.Drawing.Color.Transparent
    ChartArea1.BorderColor = System.Drawing.Color.DimGray
    ChartArea1.Name = "Default"
    Me.Chart_Returns.ChartAreas.Add(ChartArea1)
    Legend1.BackColor = System.Drawing.Color.Transparent
    Legend1.BorderColor = System.Drawing.Color.Transparent
    Legend1.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend1.DockToChartArea = "Default"
    Legend1.Name = "Default"
    Me.Chart_Returns.Legends.Add(Legend1)
    Me.Chart_Returns.Location = New System.Drawing.Point(0, 0)
    Me.Chart_Returns.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Returns.Name = "Chart_Returns"
    Me.Chart_Returns.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series1.Name = "Series1"
    Series1.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series1.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series2.Name = "Series2"
    Series2.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series2.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Returns.Series.Add(Series1)
    Me.Chart_Returns.Series.Add(Series2)
    Me.Chart_Returns.Size = New System.Drawing.Size(335, 274)
    Me.Chart_Returns.TabIndex = 0
    Me.Chart_Returns.Text = "Chart1"
    '
    'Chart_Prices
    '
    Me.Chart_Prices.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Chart_Prices.BackColor = System.Drawing.Color.Azure
    Me.Chart_Prices.BackGradientEndColor = System.Drawing.Color.SkyBlue
    Me.Chart_Prices.BackGradientType = Dundas.Charting.WinControl.GradientType.DiagonalLeft
    Me.Chart_Prices.BorderLineColor = System.Drawing.Color.LightGray
    Me.Chart_Prices.BorderSkin.FrameBackColor = System.Drawing.Color.LightSkyBlue
    Me.Chart_Prices.BorderSkin.FrameBackGradientEndColor = System.Drawing.Color.DodgerBlue
    ChartArea2.Area3DStyle.Light = Dundas.Charting.WinControl.LightStyle.Realistic
    ChartArea2.AxisX.LabelStyle.Format = "Y"
    ChartArea2.AxisX.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisX.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisX.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea2.AxisX.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisX2.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.LabelStyle.Format = "N0"
    ChartArea2.AxisY.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.MajorGrid.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.MajorGrid.LineStyle = Dundas.Charting.WinControl.ChartDashStyle.Dot
    ChartArea2.AxisY.MajorTickMark.LineColor = System.Drawing.Color.DimGray
    ChartArea2.AxisY.StartFromZero = False
    ChartArea2.AxisY2.LineColor = System.Drawing.Color.DimGray
    ChartArea2.BackColor = System.Drawing.Color.Transparent
    ChartArea2.BorderColor = System.Drawing.Color.DimGray
    ChartArea2.Name = "Default"
    Me.Chart_Prices.ChartAreas.Add(ChartArea2)
    Legend2.BackColor = System.Drawing.Color.Transparent
    Legend2.BorderColor = System.Drawing.Color.Transparent
    Legend2.Docking = Dundas.Charting.WinControl.LegendDocking.Left
    Legend2.DockToChartArea = "Default"
    Legend2.Enabled = False
    Legend2.Name = "Default"
    Me.Chart_Prices.Legends.Add(Legend2)
    Me.Chart_Prices.Location = New System.Drawing.Point(0, 0)
    Me.Chart_Prices.Margin = New System.Windows.Forms.Padding(1)
    Me.Chart_Prices.Name = "Chart_Prices"
    Me.Chart_Prices.Palette = Dundas.Charting.WinControl.ChartColorPalette.SemiTransparent
    Series3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series3.BorderWidth = 2
    Series3.ChartType = "Line"
    Series3.CustomAttributes = "LabelStyle=Top"
    Series3.Name = "Series1"
    Series3.ShadowOffset = 1
    Series3.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series3.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Series4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(120, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer), CType(CType(50, Byte), Integer))
    Series4.BorderWidth = 2
    Series4.ChartType = "Line"
    Series4.CustomAttributes = "LabelStyle=Top"
    Series4.Name = "Series2"
    Series4.ShadowOffset = 1
    Series4.XValueType = Dundas.Charting.WinControl.ChartValueTypes.DateTime
    Series4.YValueType = Dundas.Charting.WinControl.ChartValueTypes.[Double]
    Me.Chart_Prices.Series.Add(Series3)
    Me.Chart_Prices.Series.Add(Series4)
    Me.Chart_Prices.Size = New System.Drawing.Size(335, 283)
    Me.Chart_Prices.TabIndex = 1
    Me.Chart_Prices.Text = "Chart2"
    '
    'Split_All
    '
    Me.Split_All.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Split_All.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Split_All.Location = New System.Drawing.Point(0, 121)
    Me.Split_All.Margin = New System.Windows.Forms.Padding(1)
    Me.Split_All.MinimumSize = New System.Drawing.Size(600, 400)
    Me.Split_All.Name = "Split_All"
    '
    'Split_All.Panel1
    '
    Me.Split_All.Panel1.Controls.Add(Me.Split_GroupEdit)
    Me.Split_All.Panel1MinSize = 200
    '
    'Split_All.Panel2
    '
    Me.Split_All.Panel2.Controls.Add(Me.Split_Charts)
    Me.Split_All.Panel2MinSize = 300
    Me.Split_All.Size = New System.Drawing.Size(959, 567)
    Me.Split_All.SplitterDistance = 619
    Me.Split_All.SplitterWidth = 2
    Me.Split_All.TabIndex = 5
    '
    'frmGroupMembers
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(959, 727)
    Me.Controls.Add(Me.Split_All)
    Me.Controls.Add(Me.Btn_ShowCarts)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.Combo_SelectGroup)
    Me.Controls.Add(Me.lbl_GroupName)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.Combo_SelectGroupID)
    Me.Controls.Add(Me.btnSave)
    Me.Controls.Add(Me.editAuditID)
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.RootMenu)
    Me.MainMenuStrip = Me.RootMenu
    Me.MinimumSize = New System.Drawing.Size(720, 500)
    Me.Name = "frmGroupMembers"
    Me.Text = "Add/Edit Group Members"
    Me.Panel1.ResumeLayout(False)
    Me.RootMenu.ResumeLayout(False)
    Me.RootMenu.PerformLayout()
    Me.Split_GroupEdit.Panel1.ResumeLayout(False)
    Me.Split_GroupEdit.Panel2.ResumeLayout(False)
    Me.Split_GroupEdit.Panel2.PerformLayout()
    Me.Split_GroupEdit.ResumeLayout(False)
    Me.Split_Charts.Panel1.ResumeLayout(False)
    Me.Split_Charts.Panel2.ResumeLayout(False)
    Me.Split_Charts.ResumeLayout(False)
    CType(Me.Chart_Returns, System.ComponentModel.ISupportInitialize).EndInit()
    CType(Me.Chart_Prices, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Split_All.Panel1.ResumeLayout(False)
    Me.Split_All.Panel2.ResumeLayout(False)
    Me.Split_All.ResumeLayout(False)
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
	Private WithEvents _MainForm As GenoaMain

	' Form ToolTip
	Private FormTooltip As New ToolTip()

	' Form Constants, specific to the table being updated.

	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form Locals, initialised on 'New' defining what standard data items to use
	Private THIS_TABLENAME As String
	Private THIS_ADAPTORNAME As String
	Private THIS_DATASETNAME As String

	Private GenericUpdateObject As RenaissanceTimerUpdateClass
	Private Date_ValueDate_ValueDateChanged As Boolean

	' The standard ChangeID for this form. e.g. tblGroupList
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	Private THIS_FORM_SelectingCombo As ComboBox
	Private THIS_FORM_NewMoveToControl As Control

	' Form Specific Order fields
	Private THIS_FORM_SelectBy As String
	Private THIS_FORM_OrderBy As String

	Private THIS_FORM_ValueMember As String

	' Form specific Permissioning variables
	Private THIS_FORM_PermissionArea As String
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
	Private THIS_FORM_FormID As GenoaFormID

	' Data Structures

	Private myDataset As RenaissanceDataClass.DSGroupList			 ' Form Specific !!!!
	Private myTable As RenaissanceDataClass.DSGroupList.tblGroupListDataTable
	Private myConnection As SqlConnection
	Private myAdaptor As SqlDataAdapter

	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset

	Private myDSItems As RenaissanceDataClass.DSGroupItems				 ' Form Specific !!!!
	Private myTblItems As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable
	Private myAdpItems As SqlDataAdapter

	Private ItemsStandardDataset As RenaissanceGlobals.StandardDataset

	Private PertracInstruments As DataView = Nothing

	' Active Element.

	Private SortedRows() As DataRow
	Private SelectBySortedRows() As DataRow
	Private thisDataRow As RenaissanceDataClass.DSGroupList.tblGroupListRow		' Form Specific !!!!
	Private thisAuditID As Integer
	Private thisPosition As Integer
	Private _IsOverCancelButton As Boolean
	Private _InUse As Boolean

	' Form Status Flags

	Private FormIsValid As Boolean
	Private FormChanged As Boolean
	Private _FormOpenFailed As Boolean
	Private InPaint As Boolean
	Private InFormUpdate_Tick As Boolean
	' Private AddNewRecord As Boolean

	' User Permission Flags

	Private HasReadPermission As Boolean
	Private HasUpdatePermission As Boolean
	Private HasInsertPermission As Boolean
	Private HasDeletePermission As Boolean

	' Chart Management Code
	Private PriceChartArrayList As New ArrayList
	Private ReturnsChartArrayList As New ArrayList

#End Region

#Region " Form 'Properties' "

	Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

	Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return _IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			_IsOverCancelButton = Value
		End Set
	End Property

	Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

	Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
		Get
			Return _InUse
		End Get
	End Property

	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

	Public Sub New(ByVal pMainForm As GenoaMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectGroupID
		THIS_FORM_NewMoveToControl = Me.Combo_SelectGroupID

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "GroupListName"
		THIS_FORM_OrderBy = "GroupListName"

		THIS_FORM_ValueMember = "GroupListID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = GenoaFormID.frmGroupMembers

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.tblGroupList	' This Defines the Form Data !!! 
		ItemsStandardDataset = RenaissanceStandardDatasets.tblGroupItems

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		' Form Control Changed events

		AddHandler Button_AddAllItems.Click, AddressOf Me.FormControlChanged
		AddHandler Button_AddItem.Click, AddressOf Me.FormControlChanged
		AddHandler Button_RemoveAllItems.Click, AddressOf Me.FormControlChanged
		AddHandler Button_RemoveItem.Click, AddressOf Me.FormControlChanged

		AddHandler Combo_SelectFrom.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_SelectFrom.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_SelectFrom.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
		AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		MainForm.AddCopyMenuToChart(Me.Chart_Prices)
		MainForm.AddCopyMenuToChart(Me.Chart_Returns)

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.

		myConnection = MainForm.MainDataHandler.Get_Connection(Genoa_CONNECTION)
		myAdaptor = MainForm.MainDataHandler.Get_Adaptor(THIS_ADAPTORNAME, Genoa_CONNECTION, THIS_TABLENAME)
		myDataset = MainForm.Load_Table(ThisStandardDataset, False)
		myTable = myDataset.Tables(0)

		myAdpItems = MainForm.MainDataHandler.Get_Adaptor(ItemsStandardDataset.Adaptorname, Genoa_CONNECTION, ItemsStandardDataset.TableName)
		myDSItems = MainForm.Load_Table(ItemsStandardDataset, False)
		myTblItems = myDSItems.Tables(0)

		MainForm.BuildStandardFormMenu(Me.RootMenu, myDataset.Tables(0), AddressOf SelectMenuEvent, AddressOf OrderMenuEvent, AddressOf AuditReportMenuEvent)
		Me.RootMenu.PerformLayout()

		Try
			InPaint = True

			Call SetGroupCombo()
		Catch ex As Exception
		Finally
			InPaint = False
		End Try

		' Initialise List_GroupList
		Dim ListTable As New RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable
		ListTable.Columns.Add("ListDescription", GetType(String))
		ListTable.Columns.Add("PertracName", GetType(String))
		ListTable.Columns.Add("PertracProvider", GetType(String))
		ListTable.Columns.Add("IndexName", GetType(String))
		ListTable.Columns.Add("IndexProvider", GetType(String))
		ListTable.Columns.Add("TransferDetails", GetType(Boolean)).DefaultValue = False
		ListTable.Columns.Add("TransferItemID", GetType(Integer)).DefaultValue = 0

		Me.List_GroupItems.DataSource = ListTable
		List_GroupItems.DisplayMember = "ListDescription"
		List_GroupItems.ValueMember = "GroupPertracCode"

		' Initialise Chart Arraylists
		PriceChartArrayList.Add(Me.Chart_Prices)
		ReturnsChartArrayList.Add(Me.Chart_Returns)
	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
	Public Sub ResetForm() Implements StandardGenoaForm.ResetForm
		THIS_FORM_SelectBy = "GroupListName"
		THIS_FORM_OrderBy = "GroupListName"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

	Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myConnection Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Database connection is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myAdaptor Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Data Adaptor is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		If (myDataset Is Nothing) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "Form Dataset is not established. Closing form.", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Initialse form

		Try
			InPaint = True
			IsOverCancelButton = False

			' Check User permissions
			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			' Build Sorted data list from which this form operates
			If (Combo_SelectGroup.Items.Count > 0) Then
				Combo_SelectGroup.SelectedIndex = 0
			End If

			Call SetSortedRows()

			' Initialise Timer

			Date_ValueDate_ValueDateChanged = False
			GenericUpdateObject = MainForm.AddFormUpdate(Me, AddressOf FormUpdate_Tick)

			' Display initial record.
			Me.Radio_Pertrac.Checked = True
			Date_ValueDate.Value = RenaissanceGlobals.Globals.Renaissance_BaseDate

			thisPosition = 0
			If THIS_FORM_SelectingCombo.Items.Count > 0 Then
				Me.THIS_FORM_SelectingCombo.SelectedIndex = 0
				thisDataRow = SortedRows(thisPosition)
				Call GetFormData(thisDataRow)
			Else
				Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
				Call GetFormData(Nothing)
			End If

			' Set Period Combo
			SetPeriodCombo()

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in Form Load", ex.StackTrace, True)
		Finally
			InPaint = False
		End Try

    Combo_SelectFrom.Text = "A"
    Call Radio_Pertrac_CheckedChanged(Nothing, New EventArgs)

	End Sub

	Private Sub frmEntity_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False
		MainForm.RemoveFormUpdate(Me)	' Remove Form Update reference, will be re-established in Load() if this form is cached.

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				' Call GetActivePertracInstruments(MainForm, True, -999)
				Call MainForm.PertracData.GetActivePertracInstruments(True, -999)

				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Combo_SelectFrom.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_SelectFrom.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_SelectFrom.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Button_AddAllItems.Click, AddressOf Me.FormControlChanged
				RemoveHandler Button_AddItem.Click, AddressOf Me.FormControlChanged
				RemoveHandler Button_RemoveAllItems.Click, AddressOf Me.FormControlChanged
				RemoveHandler Button_RemoveItem.Click, AddressOf Me.FormControlChanged

				' Form Control Changed events
				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
				RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType


			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True

		Try
			KnowledgeDateChanged = False
			SetButtonStatus_Flag = False

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
				KnowledgeDateChanged = True
				RefreshForm = True
			End If

			' ****************************************************************
			' Check for changes relevant to this form
			' ****************************************************************

			' SetGroupCombo
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList) = True) Or KnowledgeDateChanged Then

				' Re-Set combo.
				Call SetGroupCombo()
      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList)) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Mastername)) Or KnowledgeDateChanged Then

        ' Clear Pertrac Instruments 'Cache'

        If (PertracInstruments IsNot Nothing) Then
          Try
            PertracInstruments.Dispose()
          Catch ex As Exception
          Finally
            PertracInstruments = Nothing
          End Try
        End If

        ' ReBuild Group Select-From List.

        If Radio_Pertrac.Checked Then
          InPaint = False

          If (Combo_SelectFrom.Text.Length > 0) Then ' Default (to improve performance)
            Call Combo_SelectFrom_TextChanged(Nothing, New EventArgs)
          Else
            Combo_SelectFrom.Text = "A"
          End If
        Else
          If Combo_SelectFrom.Items.Count > 0 Then
            Combo_SelectFrom.SelectedIndex = 0
          Else
            Call Combo_SelectFrom_SelectedIndexChanged(Nothing, New EventArgs)
          End If

          InPaint = True
        End If

      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupItems) = True) Or KnowledgeDateChanged Then
        ' If Knowledgedate has changed, Get_Position() will fail until SetSortedRows() is called.
        'If (FormChanged = False) Then
        '  thisPosition = Get_Position(thisAuditID)

        '  If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
        '    thisDataRow = Me.SortedRows(thisPosition)

        '    GetFormData(thisDataRow)
        '  End If
        'End If

        If Me.Radio_ExistingGroups.Checked Then
          Call Combo_SelectFrom_SelectedIndexChanged(Nothing, New EventArgs)
        End If
      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblTransaction) = True) Or KnowledgeDateChanged Then

        ' Re-Set combo.
        If Me.Radio_ExistingFunds.Checked Then
          ' Call GetActivePertracInstruments(MainForm, True)
          Call MainForm.PertracData.GetActivePertracInstruments(True)
          Call Combo_SelectFrom_SelectedIndexChanged(Nothing, New EventArgs)
        End If

      End If

      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPeriod) = True) Or KnowledgeDateChanged Then

        ' Re-Set combo.
        Call Me.SetPeriodCombo()

      End If

      ' Changes to the KnowledgeDate :-
      If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
        SetButtonStatus_Flag = True
      End If

      ' Changes to the tblUserPermissions table :-
      If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

        ' Check ongoing permissions.

        Call CheckPermissions()
        If (HasReadPermission = False) Then
          Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

          FormIsValid = False
          Me.Close()
          Exit Sub
        End If

        SetButtonStatus_Flag = True

      End If

      ' ****************************************************************
      ' Changes to the Main FORM table :-
      ' ****************************************************************

      If e.TableChanged(ThisStandardDataset.ChangeID) OrElse e.TableChanged(ItemsStandardDataset.ChangeID) OrElse KnowledgeDateChanged Then
        RefreshForm = True

        ' Re-Set Controls etc.
        Call SetSortedRows()

        ' Move again to the correct item
        thisPosition = Get_Position(thisAuditID)

        ' Validate current position.
        If thisPosition >= 0 And thisPosition < Me.SortedRows.GetLength(0) Then
          thisDataRow = Me.SortedRows(thisPosition)
        Else
          If (Me.SortedRows.GetLength(0) > 0) Then
            thisPosition = 0
            thisDataRow = Me.SortedRows(thisPosition)
            FormChanged = False
          Else
            thisDataRow = Nothing
          End If
        End If

        ' Set SelectingCombo Index.
        If (Me.THIS_FORM_SelectingCombo.Items.Count <= 0) Then
          Try
            Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
          Catch ex As Exception
          End Try
        ElseIf (thisPosition < 0) Then
          Try
            MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
          Catch ex As Exception
          End Try
        Else
          Try
            Me.THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
          Catch ex As Exception
          End Try
        End If

      End If
    Catch ex As Exception
    Finally
      InPaint = OrgInPaint
    End Try

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm = True) AndAlso (FormChanged = False) Then
			GetFormData(thisDataRow) ' Includes a call to 'SetButtonStatus()'
		Else
			If SetButtonStatus_Flag Then
				Call SetButtonStatus()
			End If
		End If

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
	Private Sub SetSortedRows()

		Dim OrgInPaint As Boolean

		Dim thisrow As DataRow
		Dim thisDrowDownWidth As Integer
		Dim SizingBitmap As Bitmap
		Dim SizingGraphics As Graphics
		Dim SelectString As String = "RN >= 0"

		' Form Specific Selection Combo :-
		If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

		' Code is pretty Generic from here on...

		' Set paint local so that changes to the selection combo do not trigger form updates.

		OrgInPaint = InPaint
		InPaint = True

		Try
			If (Me.Combo_SelectGroup.SelectedIndex > 0) Then
				SelectString = "GroupGroup='" & Combo_SelectGroup.SelectedValue.ToString & "'"
			End If

			' Get selected Row sets, or exit if no data is present.
			If myDataset Is Nothing Then
				ReDim SortedRows(0)
				ReDim SelectBySortedRows(0)
			Else
				SortedRows = myTable.Select(SelectString, THIS_FORM_OrderBy)
				SelectBySortedRows = myTable.Select(SelectString, THIS_FORM_SelectBy)
			End If

			' Set Combo data source
			THIS_FORM_SelectingCombo.DataSource = SortedRows
			THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
			THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

			' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

			thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
			For Each thisrow In SortedRows

				' Compute the string dimensions in the given font
				SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
				SizingGraphics = Graphics.FromImage(SizingBitmap)
				Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
				If (stringSize.Width) > thisDrowDownWidth Then
					thisDrowDownWidth = CInt(stringSize.Width)
				End If
			Next

			THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in SetSortedRows()", ex.StackTrace, True)
		Finally
			InPaint = OrgInPaint
		End Try

	End Sub


	' Check User permissions
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		If InPaint = False Then
			If (Me.HasUpdatePermission) Then
				FormChanged = True
				Me.btnSave.Enabled = True
				Me.btnCancel.Enabled = True
			End If
		End If

	End Sub

	Public Sub SelectMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Select Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_SelectBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub

	Public Sub OrderMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Order Menu
		' Event association is made during the dynamic menu creation

		THIS_FORM_OrderBy = CType(sender, System.Windows.Forms.ToolStripMenuItem).Text

		Call SetSortedRows()

	End Sub

	Private Sub SetPeriodCombo()
		Dim PeriodDS As RenaissanceDataClass.DSPeriod
		Dim PeriodTbl As RenaissanceDataClass.DSPeriod.tblPeriodDataTable
		Dim SelectedRows() As RenaissanceDataClass.DSPeriod.tblPeriodRow
		Dim thisRow As RenaissanceDataClass.DSPeriod.tblPeriodRow

		Dim ParentMenuItem As ToolStripMenuItem
		Dim newMenuItem As ToolStripMenuItem

		PeriodDS = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPeriod)
		PeriodTbl = PeriodDS.tblPeriod
		SelectedRows = PeriodTbl.Select("True", "PeriodTitle")

		ParentMenuItem = Me.MenuPeriods
		ParentMenuItem.DropDownItems.Clear()

		For Each thisRow In SelectedRows
			newMenuItem = ParentMenuItem.DropDownItems.Add(thisRow.PeriodTitle, Nothing, AddressOf ToolStripCombo_Periods_Click)
			newMenuItem.Tag = thisRow.PeriodID
		Next

	End Sub

	Private Sub ToolStripCombo_Periods_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' *********************************************************************************
		'
		'
		'
		' *********************************************************************************

		If InPaint = False Then
			Try
				Dim thisPeriodID As Integer
				Dim SelectedMenuItem As ToolStripMenuItem
				Dim StartDate As Date
				Dim EndDate As Date
				Dim NewChartPosition As Double
				Dim NewChartZoomSize As Double

				Dim PeriodDS As RenaissanceDataClass.DSPeriod
				Dim PeriodTbl As RenaissanceDataClass.DSPeriod.tblPeriodDataTable
				Dim SelectedRows() As RenaissanceDataClass.DSPeriod.tblPeriodRow

				SelectedMenuItem = CType(sender, System.Windows.Forms.ToolStripMenuItem)
				If (SelectedMenuItem IsNot Nothing) AndAlso (IsNumeric(SelectedMenuItem.Tag)) Then
					thisPeriodID = CInt(SelectedMenuItem.Tag)

					If (thisPeriodID > 0) Then
						PeriodDS = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPeriod)
						PeriodTbl = PeriodDS.tblPeriod
						SelectedRows = PeriodTbl.Select("PeriodID=" & thisPeriodID.ToString)

						If (SelectedRows.Length > 0) Then
							Dim StartPosition As Double
							Dim EndPosition As Double

							StartDate = SelectedRows(0).PeriodDateFrom
							EndDate = SelectedRows(0).PeriodDateTo

							Chart_Returns.ChartAreas(0).AxisX.IntervalType = DateTimeIntervalType.Months
							Chart_Returns.ChartAreas(0).AxisX.View.ZoomReset(0)
							StartPosition = Me.Chart_Returns.ChartAreas(0).AxisX.ValueToPosition(StartDate.ToOADate)
							EndPosition = Me.Chart_Returns.ChartAreas(0).AxisX.ValueToPosition(EndDate.ToOADate)
							NewChartPosition = StartDate.ToOADate
							NewChartZoomSize = (EndPosition - StartPosition) + 1
							Chart_Returns.ChartAreas(0).AxisX.View.Zoom(NewChartPosition, NewChartZoomSize, DateTimeIntervalType.Auto)

							StartPosition = Me.Chart_Returns.ChartAreas(0).AxisX.ValueToPosition(StartDate.ToOADate)
							EndPosition = Me.Chart_Returns.ChartAreas(0).AxisX.ValueToPosition(EndDate.ToOADate)
							NewChartPosition = StartDate.ToOADate
							NewChartZoomSize = (EndPosition - StartPosition) + 1
							Chart_Returns.ChartAreas(0).AxisX.View.Zoom(NewChartPosition, NewChartZoomSize, DateTimeIntervalType.Auto)

							Chart_Returns.ChartAreas(0).ReCalc()
							Chart_Returns.ChartAreas(0).AxisY.RoundAxisValues()

							Chart_Returns.Invalidate()

							Call Chart_Returns_AxisViewChanged(Chart_Returns, New ViewEventArgs(Chart_Returns.ChartAreas(0).AxisX, StartPosition))

						End If
					End If
				End If
			Catch ex As Exception
			End Try
		End If
	End Sub

	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation

		Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
			Case 0 ' This Record
				If (thisAuditID >= 0) Then
					Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID)
				End If

			Case 1 ' All Records
				Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset)

		End Select

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

	Private Sub SetGroupCombo()

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_SelectGroup, _
		RenaissanceStandardDatasets.tblGroupList, _
		"GroupGroup", _
		"GroupGroup", _
		"", True, True, True, "", "All Groups")		' 

	End Sub


#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

	Private Function FormUpdate_Tick() As Boolean
		' *******************************************************************************
		'
		' Callback process for the Generic Form Update Process.
		'
		' Function MUST return True / False on Update.
		' True will clear the update request. False will not.
		'
		' *******************************************************************************
		Dim RVal As Boolean = False

		If (InFormUpdate_Tick) OrElse (Not _InUse) Then
			Return False
			Exit Function
		End If

		Try
			InFormUpdate_Tick = True

			' Update Form :
			' Not Implemented


			' Update After ValueDate.Value Changed

			If (Date_ValueDate_ValueDateChanged) Then
				Date_ValueDate_ValueDateChanged = False

				' Call GetActivePertracInstruments(MainForm, True, 0)
				Call MainForm.PertracData.GetActivePertracInstruments(True, 0, Me.Date_ValueDate.Value)

				' Trigger  SelectList Rebuild.

				If (Combo_SelectFrom.Items.Count > 0) AndAlso (Combo_SelectFrom.SelectedIndex <> 0) Then
					Combo_SelectFrom.SelectedIndex = 0
				Else
					Call Combo_SelectFrom_SelectedIndexChanged(Nothing, New EventArgs)
				End If

				RVal = True

			End If

		Catch ex As Exception
			RVal = False
		Finally
			InFormUpdate_Tick = False
		End Try

		Return RVal

	End Function

	Private Sub GetFormData(ByRef ThisRow As RenaissanceDataClass.DSGroupList.tblGroupListRow)
		' Routine to populate form Controls given a data row.
		' Form is cleared for invalid Datarows or Invalid Form.

		Dim OrgInpaint As Boolean

		' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
		OrgInpaint = InPaint
		InPaint = True

		Try
			If (ThisRow Is Nothing) Or (FormIsValid = False) Or (Me.InUse = False) Then
				' Bad / New Datarow - Clear Form.

				thisAuditID = (-1)

				Me.editAuditID.Text = ""
				Me.lbl_GroupName.Text = ""

				CType(List_GroupItems.DataSource, RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable).Rows.Clear()

				Me.btnCancel.Enabled = False
				Me.THIS_FORM_SelectingCombo.Enabled = True

			Else

				' Populate Form with given data.
				Dim tmpCommand As New SqlCommand
				Dim ListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable

				Try

					thisAuditID = thisDataRow.AuditID

					Me.editAuditID.Text = thisDataRow.AuditID.ToString

					lbl_GroupName.Text = thisDataRow.GroupListName

					tmpCommand.CommandType = CommandType.StoredProcedure
					tmpCommand.CommandText = "adp_tblGroupItems_SelectGroup"
					tmpCommand.Connection = MainForm.GetGenoaConnection
					tmpCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = thisDataRow.GroupListID
					tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
					tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

					List_GroupItems.SuspendLayout()
					ListTable = CType(List_GroupItems.DataSource, RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)
					ListTable.Rows.Clear()
					ListTable.Load(tmpCommand.ExecuteReader)
					List_GroupItems.ResumeLayout()

					' MainForm.SetComboSelectionLengths(Me, THIS_FORM_SelectingCombo)

					Me.btnCancel.Enabled = False
					Me.THIS_FORM_SelectingCombo.Enabled = True

				Catch ex As Exception

					Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
					Call GetFormData(Nothing)

				Finally
					Try
						If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
							tmpCommand.Connection.Close()
							tmpCommand.Connection = Nothing
						End If
					Catch ex As Exception
					End Try
				End Try

			End If

			' Allow Field events to trigger before 'InPaint' Is re-set. 
			' (Should) Prevent Validation errors during Form Draw.
			Application.DoEvents()

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in GetFormData()", ex.StackTrace, True)
		Finally
			' Restore 'Paint' flag.
			InPaint = OrgInpaint
		End Try

		FormChanged = False
		Me.btnSave.Enabled = False

		List_SelectItems.SelectedIndex = (-1)
		List_GroupItems.SelectedIndex = (-1)

		' As it says on the can....
		Call SetButtonStatus()

	End Sub

	Private Function SetFormData(Optional ByVal pConfirm As Boolean = True) As Boolean
		' *************************************************************
		'
		' Note that GroupItems Entries are also added from the FundSearch Form. Changes
		' to the GroupItems table should be reflected there also.
		'
		' *************************************************************
		Dim ErrMessage As String
    Dim ErrFlag As Boolean = False
		Dim ErrStack As String
		Dim ProtectedItem As Boolean = False
		Dim UpdatedItemData As Boolean = False
    Dim UpdateMessage As String = ""
    Dim UpdateMessageItemData As String = ""

		ErrMessage = ""
		ErrStack = ""

		' *************************************************************
		' Appropriate Save permission :-
		' *************************************************************

		If (Me.HasUpdatePermission = False) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' If Save button is disabled then should not be able to save, exit silently.
		' *************************************************************
		If Me.btnSave.Enabled = False Then
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' *************************************************************
		' KnowledgeDate OK :-
		' *************************************************************

		If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Genoa are not allowed when the Knowledgedate is not set to `NOW`", "", True)
			Call btnCancel_Click(Me, New System.EventArgs)
			Exit Function
		End If

		' Confirm Save, if required.

		If (pConfirm = True) Then
			If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
				Return True
				Exit Function
			End If
		End If

		' *************************************************************
		' Procedure to Save the current form information.
		' *************************************************************

		Dim StatusString As String
		Dim LogString As String
		Dim UpdateRows(0) As DataRow
		Dim Position As Integer

		If (FormChanged = False) Or (FormIsValid = False) Then
			Return False
			Exit Function
		End If

		' Validation
		StatusString = ""
		If ValidateForm(StatusString) = False Then
			MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
			Return False
			Exit Function
		End If

		' Check current position in the table.
		Position = Get_Position(thisAuditID)

		' Exit on new or missing ID.
		If Position < 0 Then
			Return False
			Exit Function
		Else
			' Row found OK.
			LogString = "Edit : AuditID = " & thisAuditID.ToString

			thisDataRow = Me.SortedRows(Position)

		End If


		Try
			' Set 'Paint' flag.
			InPaint = True

			' *************************************************************
			' Lock the Data Table, to prevent update conflicts.
			' *************************************************************

			' Get Existing Data Items

			Dim SelectedGroupID As Integer
			Dim SelectedRows() As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow
			Dim tblItemRow As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow
			Dim GroupItemRow As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow
			Dim ItemCount As Integer
			Dim ListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable
			Dim TransferDataRows As New ArrayList

			ListTable = CType(List_GroupItems.DataSource, RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)

			myDSItems = MainForm.Load_Table(ItemsStandardDataset, False)
			myTblItems = myDSItems.Tables(0)

			SelectedGroupID = thisDataRow.GroupListID
			SelectedRows = myTblItems.Select("GroupID=" & SelectedGroupID.ToString, "GroupID")

			SyncLock myTblItems
				Dim temp As Integer
				Try
					' Delete any rows that are no longer in the Group

					If (SelectedRows IsNot Nothing) AndAlso (SelectedRows.Length > 0) Then
						For ItemCount = 0 To (SelectedRows.Length - 1)
							tblItemRow = SelectedRows(ItemCount)

              If (GroupContainsID(tblItemRow.GroupPertracCode) = False) Then

                UpdateMessage &= tblItemRow.GroupItemID.ToString & ","
                tblItemRow.Delete()
              End If
						Next
					End If

					' Add / Update Group Items to the table

					Dim FoundFlag As Boolean = False
					Dim TransferDetailsFlag As Boolean = False
					Dim TransferDetailsItemID As Integer
					Dim TransferDetailsHashTable As New Hashtable

					For Each GroupItemRow In ListTable.Rows
						FoundFlag = False
						TransferDetailsFlag = False
						TransferDetailsItemID = 0
						tblItemRow = Nothing

						If (CBool(GroupItemRow("TransferDetails")) = True) Then
							If IsNumeric(GroupItemRow("TransferItemID")) AndAlso (CInt(GroupItemRow("TransferItemID")) > 0) Then
								TransferDetailsFlag = True
								TransferDetailsItemID = CInt(GroupItemRow("TransferItemID"))
							End If

							GroupItemRow("TransferDetails") = False
						End If

						If (SelectedRows.Length > 0) Then
							For ItemCount = 0 To (SelectedRows.Length - 1)
								tblItemRow = SelectedRows(ItemCount)

								If tblItemRow.RowState <> DataRowState.Deleted Then
									If (tblItemRow.GroupPertracCode = GroupItemRow.GroupPertracCode) Then
										FoundFlag = True

										Exit For
									End If
								End If
							Next
						End If

						If (FoundFlag = False) Or (TransferDetailsFlag = True) Then
							If (FoundFlag = False) Then
								' Add New Row

								tblItemRow = myTblItems.NewRow
								tblItemRow.GroupID = SelectedGroupID
								tblItemRow.GroupItemID = 0
								tblItemRow.GroupPertracCode = GroupItemRow.GroupPertracCode
								tblItemRow.GroupIndexCode = 0
								tblItemRow.GroupSector = ""

								myTblItems.Rows.Add(tblItemRow)

							End If

							If (TransferDetailsFlag = True) And (tblItemRow IsNot Nothing) Then
								tblItemRow.GroupIndexCode = GroupItemRow.GroupIndexCode
								tblItemRow.GroupSector = GroupItemRow.GroupSector

								TransferDataRows.Add(tblItemRow)
								TransferDetailsHashTable.Add(tblItemRow, TransferDetailsItemID)
							End If
						End If
					Next

					' Post Additions / Updates to the underlying table :-

          If (ErrFlag = False) Then
            myAdpItems.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
            myAdpItems.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

            SelectedRows = myTblItems.Select("GroupItemID=0", "GroupID")

            temp = MainForm.AdaptorUpdate(Me.Name, myAdpItems, myTblItems)

            For Each tblItemRow In SelectedRows
              UpdateMessage &= tblItemRow.GroupItemID.ToString & ","
            Next

          End If

          ' Transfer Item Data as requested.

          If TransferDataRows.Count > 0 Then
            Dim TransferDataCount As Integer
            Dim ItemDataDataset As RenaissanceDataClass.DSGroupItemData       ' Form Specific !!!!
            Dim ItemDataTable As RenaissanceDataClass.DSGroupItemData.tblGroupItemDataDataTable
            Dim ItemDataAdaptor As SqlDataAdapter
            Dim ItemDataStandardDataset As RenaissanceGlobals.StandardDataset
            Dim SelectedItemDataRows() As RenaissanceDataClass.DSGroupItemData.tblGroupItemDataRow
            Dim SourceRow As RenaissanceDataClass.DSGroupItemData.tblGroupItemDataRow
            Dim TargetRow As RenaissanceDataClass.DSGroupItemData.tblGroupItemDataRow

            ItemDataStandardDataset = RenaissanceStandardDatasets.tblGroupItemData
            ItemDataAdaptor = MainForm.MainDataHandler.Get_Adaptor(ItemDataStandardDataset.Adaptorname, Genoa_CONNECTION, ItemDataStandardDataset.TableName)
            ItemDataDataset = MainForm.Load_Table(ItemDataStandardDataset, False)
            ItemDataTable = ItemDataDataset.Tables(0)

            SyncLock ItemDataTable
              For TransferDataCount = 0 To (TransferDataRows.Count - 1)
                tblItemRow = TransferDataRows(TransferDataCount)

                TransferDetailsItemID = 0
                If TransferDetailsHashTable.Contains(tblItemRow) Then
                  TransferDetailsItemID = CInt(TransferDetailsHashTable(tblItemRow))
                End If

                ' Source Row
                SelectedItemDataRows = ItemDataTable.Select("GroupItemID=" & TransferDetailsItemID)

                If (SelectedItemDataRows.Length > 0) Then
                  SourceRow = SelectedItemDataRows(0)

                  ' Target Row

                  SelectedItemDataRows = ItemDataTable.Select("GroupItemID=" & tblItemRow.GroupItemID)

                  If (SelectedItemDataRows.Length > 0) Then
                    TargetRow = SelectedItemDataRows(0)
                  Else
                    TargetRow = ItemDataTable.NewtblGroupItemDataRow
                  End If

                  TargetRow.GroupItemID = tblItemRow.GroupItemID
                  TargetRow.GroupLiquidity = SourceRow.GroupLiquidity
                  TargetRow.GroupHolding = SourceRow.GroupHolding
                  TargetRow.GroupNewHolding = SourceRow.GroupNewHolding
                  TargetRow.GroupPercent = SourceRow.GroupPercent
                  TargetRow.GroupExpectedReturn = SourceRow.GroupExpectedReturn
                  TargetRow.GroupUpperBound = SourceRow.GroupUpperBound
                  TargetRow.GroupLowerBound = SourceRow.GroupLowerBound
                  TargetRow.GroupFloor = SourceRow.GroupFloor
                  TargetRow.GroupCap = SourceRow.GroupCap
                  TargetRow.GroupTradeSize = SourceRow.GroupTradeSize
                  TargetRow.GroupBeta = SourceRow.GroupBeta
                  TargetRow.GroupAlpha = SourceRow.GroupAlpha
                  TargetRow.GroupIndexE = SourceRow.GroupIndexE
                  TargetRow.GroupAlphaE = SourceRow.GroupAlphaE
                  TargetRow.GroupStdErr = SourceRow.GroupStdErr

                  If (TargetRow.RowState And DataRowState.Detached) Then
                    ItemDataTable.Rows.Add(TargetRow)
                  End If

                  ItemDataAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
                  ItemDataAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

                  temp = MainForm.AdaptorUpdate(Me.Name, ItemDataAdaptor, New DataRow() {TargetRow})
                  UpdatedItemData = True

                  UpdateMessageItemData &= TargetRow.GroupItemID.ToString & ","
                End If

              Next
            End SyncLock

            TransferDataRows.Clear()
            TransferDetailsHashTable.Clear()

          End If

        Catch ex As Exception

          ErrMessage = ex.Message
          ErrFlag = True
          ErrStack = ex.StackTrace

        End Try

			End SyncLock

			' *************************************************************
			' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
			' *************************************************************

			If (ErrFlag = True) Then
        Call MainForm.LogError(Me.Name & ", SetFormData", LOG_LEVELS.Error, ErrMessage, "Error Saving Data", ErrStack, True)
			End If

		Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", SetFormData", LOG_LEVELS.Error, ErrMessage, "Error in SetFormData()", ErrStack, True)
    Finally
      InPaint = False
		End Try

		' Finish off

		FormChanged = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		' Propagate changes

    Try

      Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(ItemsStandardDataset.ChangeID, UpdateMessage))

      If (UpdatedItemData) Then
        Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceStandardDatasets.tblGroupItemData.ChangeID, UpdateMessageItemData))
      End If

    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", SetFormData", LOG_LEVELS.Error, ex.Message, "Error Saving Data", ex.StackTrace, True)
    End Try

  End Function

	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		'' Has Insert Permission.
		'If Me.HasInsertPermission Then
		'  Me.Button_AddAllItems.Enabled = True
		'  Me.Button_AddItem.Enabled = True
		'  Me.Button_RemoveAllItems.Enabled = True
		'  Me.Button_RemoveItem.Enabled = True
		'Else
		'  Me.Button_AddAllItems.Enabled = False
		'  Me.Button_AddItem.Enabled = False
		'  Me.Button_RemoveAllItems.Enabled = False
		'  Me.Button_RemoveItem.Enabled = False
		'End If

		'' Has Delete permission. 
		'If (Me.HasDeletePermission) And (Me.AddNewRecord = False) And (thisPosition >= 0) Then
		'  Me.btnDelete.Enabled = True
		'Else
		'  Me.btnDelete.Enabled = False
		'End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And (Me.HasUpdatePermission) Then

			Me.List_GroupItems.Enabled = True
			Me.List_GroupItems.Enabled = True
			Me.Combo_SelectGroupID.Enabled = True
			Me.Radio_Pertrac.Enabled = True
			Me.Radio_ExistingGroups.Enabled = True
			Me.Radio_ExistingFunds.Enabled = True

			Me.Button_AddAllItems.Enabled = True
			Me.Button_AddItem.Enabled = True
			Me.Button_RemoveAllItems.Enabled = True
			Me.Button_RemoveItem.Enabled = True

		Else

			Me.List_GroupItems.Enabled = False
			Me.List_GroupItems.Enabled = False

			Me.Combo_SelectGroupID.Enabled = False
			Me.Radio_Pertrac.Enabled = False
			Me.Radio_ExistingGroups.Enabled = False
			Me.Radio_ExistingFunds.Enabled = False

			Me.Button_AddAllItems.Enabled = False
			Me.Button_AddItem.Enabled = False
			Me.Button_RemoveAllItems.Enabled = False
			Me.Button_RemoveItem.Enabled = False


		End If

	End Sub

	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 



		Return RVal

	End Function

	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub

	Private Sub Combo_SelectGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectGroup.SelectedIndexChanged
		' ********************************************************************************************
		' This form allows the user to display only those Entities from a given group.
		' This combo controls this.
		'
		' ********************************************************************************************

		If InPaint Then
			Exit Sub
		End If

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Call SetSortedRows()

		thisPosition = 0
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		If (thisPosition >= 0) Then
			thisDataRow = Me.SortedRows(thisPosition)
		Else
			thisDataRow = Nothing
		End If

		Call GetFormData(thisDataRow)
	End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

	Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' Selection Combo. SelectedItem changed.
		'

		' Don't react to changes made in paint routines etc.
		If InPaint = True Then Exit Sub

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		' Find the correct data row, then show it...
		thisPosition = THIS_FORM_SelectingCombo.SelectedIndex

		If (thisPosition >= 0) Then
			thisDataRow = Me.SortedRows(thisPosition)
		Else
			thisDataRow = Nothing
		End If

		Call GetFormData(thisDataRow)

	End Sub


	Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
		' 'Previous' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If thisPosition > 0 Then thisPosition -= 1
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		Exit Sub

		If myTable.Rows.Count > thisPosition Then
			thisDataRow = myTable.Rows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call GetFormData(Nothing)
		End If

	End Sub

	Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
		' 'Next' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If thisPosition < (myTable.Rows.Count - 1) Then thisPosition += 1
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

		Exit Sub

		If myTable.Rows.Count > thisPosition Then
			thisDataRow = myTable.Rows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call GetFormData(Nothing)
		End If

	End Sub

	Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
		' 'First' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = 0
		If thisPosition >= THIS_FORM_SelectingCombo.Items.Count Then
			thisPosition = (THIS_FORM_SelectingCombo.Items.Count - 1)
		End If

    If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
      Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
    Else
      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    End If

	End Sub

	Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
		' 'Last' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = THIS_FORM_SelectingCombo.Items.Count - 1

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

	End Sub

	Private Sub btn_NewGroup_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn_NewGroup.Click

		Dim NewGroupName As String

		NewGroupName = InputBox("Please Enter a New Group Name", "New Group")
		If (NewGroupName.Length <= 0) Then
			Exit Sub
		End If

		Dim GroupDataset As RenaissanceDataClass.DSGroupList
		Dim GroupTable As RenaissanceDataClass.DSGroupList.tblGroupListDataTable
		Dim GroupAdaptor As SqlDataAdapter
		Dim GroupStandardDataset As RenaissanceGlobals.StandardDataset
		Dim thisGroupRow As RenaissanceDataClass.DSGroupList.tblGroupListRow = Nothing

		GroupStandardDataset = RenaissanceStandardDatasets.tblGroupList

		GroupAdaptor = MainForm.MainDataHandler.Get_Adaptor(GroupStandardDataset.Adaptorname, Genoa_CONNECTION, GroupStandardDataset.TableName)
		GroupDataset = MainForm.Load_Table(GroupStandardDataset, False)
		GroupTable = myDataset.Tables(0)

		Try
			thisGroupRow = GroupTable.NewRow

			SyncLock GroupTable
				thisGroupRow.GroupDateFrom = #1/1/1900#
				thisGroupRow.GroupDateTo = #1/1/3000#
				thisGroupRow.GroupGroup = ""
				thisGroupRow.GroupListID = 0
				thisGroupRow.GroupListName = NewGroupName

				GroupTable.Rows.Add(thisGroupRow)

				GroupAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
				GroupAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

				MainForm.AdaptorUpdate(Me.Name, GroupAdaptor, New DataRow() {thisGroupRow})
			End SyncLock

      Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(GroupStandardDataset.ChangeID, thisGroupRow.GroupListID.ToString))

		Catch ex As Exception
		End Try

		Try
			Application.DoEvents()

			InPaint = True

			If (Combo_SelectGroup.Items.Count > 0) Then
				Combo_SelectGroup.SelectedIndex = 0
			End If

			Application.DoEvents()

			'			Call SetSortedRows()
			InPaint = False

			If (thisGroupRow.GroupListID > 0) Then
				Me.Combo_SelectGroupID.SelectedValue = thisGroupRow.GroupListID
			End If

		Catch ex As Exception
		Finally
			InPaint = False
		End Try

		GroupAdaptor = Nothing
		GroupDataset = Nothing
		GroupTable = Nothing

		' MainForm.ShowForm_AddNew(GenoaFormID.frmGroupList)

	End Sub

	Private Function Get_Position(ByVal pAuditID As Integer) As Integer
		' Returns the position in the current 'SortedRows' array of the entry corresponding to the given 
		' AudidID.

		Dim MinIndex As Integer
		Dim MaxIndex As Integer
		Dim CurrentMin As Integer
		Dim CurrentMax As Integer
		Dim searchDataRow As DataRow

		Try
			' SortedRows exists ?

			If SortedRows Is Nothing Then
				Return (-1)
				Exit Function
			End If

			' Return (-1) if there are no rows in the 'SortedRows'

			If (SortedRows.GetLength(0) <= 0) Then
				Return (-1)
				Exit Function
			End If


			' Use a modified Search moving outwards from the last returned value to locate the required value.
			' Reflecting the fact that for the most part One looks for closely located records.

			MinIndex = 0
			MaxIndex = SortedRows.GetLength(0) - 1


			' Check First and Last records (Incase 'First' or 'Last' was pressed).

			searchDataRow = SortedRows(MinIndex)

			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
				Return MinIndex
				Exit Function
			End If

			searchDataRow = SortedRows(MaxIndex)
			If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
				Return MaxIndex
				Exit Function
			End If

			' now search outwards from the last returned value.

			If (Me.thisPosition >= MinIndex) And (Me.thisPosition <= MaxIndex) Then
				CurrentMin = thisPosition
				CurrentMax = CurrentMin + 1
			Else
				CurrentMin = CInt((MinIndex + MaxIndex) / 2)
				CurrentMax = CurrentMin + 1
			End If

			While (CurrentMin >= MinIndex) Or (CurrentMax <= MaxIndex)
				If (CurrentMin >= MinIndex) Then
					searchDataRow = SortedRows(CurrentMin)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
						Return CurrentMin
						Exit Function
					End If

					CurrentMin -= 1
				End If

				If (CurrentMax <= MaxIndex) Then
					searchDataRow = SortedRows(CurrentMax)

					If (searchDataRow.IsNull("AuditID") = False) AndAlso (CInt(CInt(searchDataRow("AuditID"))) = pAuditID) Then
						Return CurrentMax
						Exit Function
					End If

					CurrentMax += 1
				End If

			End While

			Return (-1)
			Exit Function

		Catch ex As Exception
			MainForm.LogError(Me.Name & ", Get_Position()", LOG_LEVELS.Warning, ex.Message, "", ex.StackTrace, True)
			Return (-1)
			Exit Function
		End Try

	End Function

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
		' Cancel Changes, redisplay form.

		FormChanged = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

		If (thisPosition >= 0) And (thisPosition < Me.SortedRows.GetLength(0)) Then
			thisDataRow = Me.SortedRows(thisPosition)
			Call GetFormData(thisDataRow)
		Else
			Call btnNavFirst_Click(Me, New System.EventArgs)
		End If

	End Sub

	Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
		' Save Changes, if any, without prompting.

		If (FormChanged = True) Then
			Call SetFormData(False)
		End If

	End Sub


	Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' Prepare form to Add a new record.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If (Combo_SelectGroup.Items.Count > 0) Then
			Combo_SelectGroup.SelectedIndex = 0
		End If

		Application.DoEvents()

		'thisPosition = -1
		'InPaint = True
		'MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
		'InPaint = False

		'GetFormData(Nothing)
		'AddNewRecord = True
		'Me.btnCancel.Enabled = True
		'Me.THIS_FORM_SelectingCombo.Enabled = False
		'If (Me.Combo_SelectGroup.SelectedIndex > 0) AndAlso (IsNumeric(Combo_SelectGroup.SelectedValue)) Then
		'  Me.Combo_GroupGroup.SelectedValue = Combo_SelectGroup.SelectedValue
		'End If

		Call SetButtonStatus()

		THIS_FORM_NewMoveToControl.Focus()

	End Sub

	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' Close Form

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Me.Close()

	End Sub

#End Region

#Region " Radio Events, Build Select List Combo"

	Private Sub Radio_Pertrac_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Pertrac.CheckedChanged
		' ***********************************************************************************
		' React to the Pertrac Radio button being selected.
		'
		' The Select combo becomes a Select-As-You-Type edit box for the Select List control.
		' The SelectList control contains All or Selected Pertrac Instruments.
		' ***********************************************************************************

		If Radio_Pertrac.Checked Then

			Check_TransferDetails.Visible = False
			Date_ValueDate.Visible = False

			' Re-Configure Select Combo events.

			Try
				RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType
			Catch ex As Exception
			End Try
			Try
				RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
			Catch ex As Exception
			End Try

			AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

      Dim OrgInpaint As Boolean = InPaint
      Try
        InPaint = True

        Combo_SelectFrom.DataSource = Nothing
        Combo_SelectFrom.Items.Clear()

      Catch ex As Exception
      Finally
        InPaint = OrgInpaint
      End Try

      ' Trigger Select List re-Build.

      If (Combo_SelectFrom.Text.Length > 0) Then ' Default (to improve performance)
        Call Combo_SelectFrom_TextChanged(Nothing, New EventArgs)
      Else
        Combo_SelectFrom.Text = "A"
      End If
    End If

	End Sub

	Private Sub Radio_ExistingGroups_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ExistingGroups.CheckedChanged
		' ***********************************************************************************
		' React to the ExistingGroups radio.
		'
		' The Select Combo becomes a Combo to select an existing Group,
		' The SelectList contains all Instruments in the selected Group(s).
		' ***********************************************************************************

		If Radio_ExistingGroups.Checked Then

			' Re-Configure Select Combo events.

			Try
				RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType
			Catch ex As Exception
			End Try
			Try
				RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
			Catch ex As Exception
			End Try

			AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

			' Populate Select Combo.

			Call MainForm.SetTblGenericCombo( _
			Me.Combo_SelectFrom, _
			RenaissanceStandardDatasets.tblGroupList, _
			"GroupListName", _
			"GroupListID", _
			"", False, True, True, 0, "All Groups")		' 

			Check_TransferDetails.Visible = True
			Date_ValueDate.Visible = False

			' Trigger  SelectList Rebuild.

			If Combo_SelectFrom.Items.Count > 0 Then
				Combo_SelectFrom.SelectedIndex = 0
			End If
			Call Combo_SelectFrom_SelectedIndexChanged(Nothing, New EventArgs)

		End If

	End Sub

	Private Sub Radio_ExistingFunds_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ExistingFunds.CheckedChanged
		' ***********************************************************************************
		' React to the ExistingFunds radio.
		'
		' The Select Combo becomes a Combo to select an existing Venice Fund,
		' The SelectList contains all Instruments, with associated Pertrac Instruments, in the selected Fund(s).
		' ***********************************************************************************

		If Radio_ExistingFunds.Checked Then

			Check_TransferDetails.Visible = False
			Date_ValueDate.Visible = True

			' Re-Configure Select Combo events.

			Try
				RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType
			Catch ex As Exception
			End Try
			Try
				RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
			Catch ex As Exception
			End Try

			AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

			' Call GetActivePertracInstruments(MainForm, True, 0)
			Call MainForm.PertracData.GetActivePertracInstruments(True, 0, Me.Date_ValueDate.Value)

			' Populate Select Combo.

			Call MainForm.SetTblGenericCombo( _
			Me.Combo_SelectFrom, _
			RenaissanceStandardDatasets.tblFund, _
			"FundCode", _
			"FundID", _
			"", False, True, True, 0, "All Funds")	 ' 

			' Trigger  SelectList Rebuild.

			If Combo_SelectFrom.Items.Count > 0 Then
				Combo_SelectFrom.SelectedIndex = 0
			Else
				Call Combo_SelectFrom_SelectedIndexChanged(Nothing, New EventArgs)
			End If
		End If

	End Sub


	Private Sub Date_ValueDate_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Date_ValueDate.ValueChanged
		' ***********************************************************************************
		'
		'
		' ***********************************************************************************

		Try
			If (Not Me.Created) OrElse (Me.IsDisposed) OrElse (Me.InPaint) Then
				Exit Sub
			End If

			If Radio_ExistingFunds.Checked Then

				If (GenericUpdateObject IsNot Nothing) Then
					' Trigger periodic Update

					Try
						GenericUpdateObject.FormToUpdate = True
						Date_ValueDate_ValueDateChanged = True
					Catch ex As Exception
					End Try
				Else

					' Call GetActivePertracInstruments(MainForm, True, 0)
					Call MainForm.PertracData.GetActivePertracInstruments(True, 0, Me.Date_ValueDate.Value)

					' Trigger  SelectList Rebuild.

					If (Combo_SelectFrom.Items.Count > 0) AndAlso (Combo_SelectFrom.SelectedIndex <> 0) Then
						Combo_SelectFrom.SelectedIndex = 0
					Else
						Call Combo_SelectFrom_SelectedIndexChanged(Nothing, New EventArgs)
					End If

				End If

			End If

		Catch ex As Exception

		End Try

	End Sub

	Private Sub Combo_SelectFrom_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectFrom.TextChanged
		' ***********************************************************************************
		' This Event handles the Select-As-You-Type functionality associated with the Pertrac Radio choice.
		'
		' The List is managed by applying a select critera to the PertracInstruments DataView.
		' ***********************************************************************************

    Try

      If Me.InPaint = False Then
        If (Combo_SelectFrom.SelectedIndex < 0) Then
          If Me.Radio_Pertrac.Checked Then

            Try

              List_SelectItems.SuspendLayout()

              ' Ensure the DataView is populated.
              If (PertracInstruments Is Nothing) Then
                PertracInstruments = MainForm.PertracData.GetPertracInstruments()
              End If

              ' Set Display and View Members

              Try
                If (List_SelectItems.DisplayMember <> "ListDescription") Then
                  List_SelectItems.DataSource = Nothing
                  List_SelectItems.DisplayMember = "ListDescription"
                End If

                If (List_SelectItems.ValueMember <> "PertracCode") Then
                  List_SelectItems.DataSource = Nothing
                  List_SelectItems.ValueMember = "PertracCode"
                End If
              Catch ex As Exception
              End Try

              ' Set Selection criteria.

              If (Combo_SelectFrom.Text.Length <= 0) OrElse (Combo_SelectFrom.Text = "%") OrElse (Combo_SelectFrom.Text = "*") Then
                PertracInstruments.RowFilter = "false"
              Else
                If (PertracInstruments.RowFilter <> "Mastername LIKE '" & Combo_SelectFrom.Text & "%'") Then
                  PertracInstruments.RowFilter = "Mastername LIKE '" & Combo_SelectFrom.Text & "%'"
                End If
              End If

              ' Set DataSource

              If (List_SelectItems.DataSource IsNot PertracInstruments) Then
                List_SelectItems.DataSource = PertracInstruments
              End If

              ' Set Display and View Members (Double check).

              Try
                If (List_SelectItems.DisplayMember <> "ListDescription") Then
                  List_SelectItems.DisplayMember = "ListDescription"
                End If

                If (List_SelectItems.ValueMember <> "PertracCode") Then
                  List_SelectItems.ValueMember = "PertracCode"
                End If
              Catch ex As Exception
              End Try


            Catch ex As Exception
              PertracInstruments.RowFilter = "false"
            Finally
              List_SelectItems.ResumeLayout()
            End Try


          End If
        End If
      End If

    Catch ex As Exception

    End Try


	End Sub

	Private Sub Combo_SelectFrom_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectFrom.SelectedIndexChanged
		' ***********************************************************************************
		'
		'
		'
		' ***********************************************************************************

		If Me.InPaint = False Then

			Check_TransferDetails.Enabled = False

			If (Combo_SelectFrom.SelectedIndex >= 0) Then

				If Me.Radio_Pertrac.Checked Then
					' 

					List_SelectItems.SuspendLayout()

					If (PertracInstruments Is Nothing) Then
						PertracInstruments = MainForm.PertracData.GetPertracInstruments()
					End If

					Try
						If (List_SelectItems.DisplayMember <> "ListDescription") Then
							List_SelectItems.DataSource = Nothing
							List_SelectItems.DisplayMember = "ListDescription"
						End If

						If (List_SelectItems.ValueMember <> "PertracCode") Then
							List_SelectItems.DataSource = Nothing
							List_SelectItems.ValueMember = "PertracCode"
						End If
					Catch ex As Exception
					End Try

					PertracInstruments.RowFilter = "Mastername LIKE '" & Combo_SelectFrom.SelectedText & "%'"

					If (List_SelectItems.DataSource IsNot PertracInstruments) Then
						List_SelectItems.DataSource = PertracInstruments
					End If

					List_SelectItems.ResumeLayout()

				ElseIf (Me.Radio_ExistingGroups.Checked) AndAlso (IsNumeric(Combo_SelectFrom.SelectedIndex)) Then
					' Populate Form with given data.

					Dim tmpCommand As New SqlCommand
					Dim ListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable

					' Enable 'Transfer Details' Check box ?
					Try
						If CInt(Combo_SelectFrom.SelectedIndex) > 0 Then
							Check_TransferDetails.Enabled = True
						End If
					Catch ex As Exception
					End Try

					Try

						tmpCommand.CommandType = CommandType.StoredProcedure
						tmpCommand.CommandText = "adp_tblGroupItems_SelectGroupPertracCodes"
						tmpCommand.Connection = MainForm.GetGenoaConnection
						tmpCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = CInt(Combo_SelectFrom.SelectedValue)
						tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
						tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

						List_SelectItems.SuspendLayout()

						If (List_SelectItems.DataSource Is Nothing) OrElse (Not (TypeOf List_SelectItems.DataSource Is RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)) Then
							ListTable = New RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable
							ListTable.Columns.Add("ListDescription", GetType(String))
							ListTable.Columns.Add("PertracName", GetType(String))
							ListTable.Columns.Add("PertracProvider", GetType(String))
							ListTable.Columns.Add("IndexName", GetType(String))
							ListTable.Columns.Add("IndexProvider", GetType(String))

							List_SelectItems.DataSource = ListTable
						Else
							ListTable = CType(List_SelectItems.DataSource, RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)
						End If

						ListTable.Rows.Clear()
						ListTable.Load(tmpCommand.ExecuteReader)

						Try
							List_SelectItems.DisplayMember = "ListDescription"
							List_SelectItems.ValueMember = "GroupPertracCode"
						Catch ex As Exception
						End Try

						List_SelectItems.DataSource = ListTable
						List_SelectItems.ResumeLayout()

					Catch ex As Exception

						Call MainForm.LogError(Me.Name, 0, ex.Message, "Error building Select List", ex.StackTrace, True)
						Call GetFormData(Nothing)

					Finally
						Try
							If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
								tmpCommand.Connection.Close()
								tmpCommand.Connection = Nothing
							End If
						Catch ex As Exception
						End Try
					End Try

				ElseIf Me.Radio_ExistingFunds.Checked Then

					List_SelectItems.SuspendLayout()

					Try
						If (List_SelectItems.DisplayMember <> "ListDescription") Then
							List_SelectItems.DataSource = Nothing
							List_SelectItems.DisplayMember = "ListDescription"
						End If

						If (List_SelectItems.ValueMember <> "PertracCode") Then
							List_SelectItems.DataSource = Nothing
							List_SelectItems.ValueMember = "PertracCode"
						End If
					Catch ex As Exception
					End Try

					' List_SelectItems.DataSource = GetActivePertracInstruments(MainForm, False, CInt(Combo_SelectFrom.SelectedValue))
					List_SelectItems.DataSource = MainForm.PertracData.GetActivePertracInstruments(False, CInt(Combo_SelectFrom.SelectedValue), Date_ValueDate.Value)

					Try
						If (List_SelectItems.DisplayMember <> "ListDescription") Then
							List_SelectItems.DisplayMember = "ListDescription"
						End If

						If (List_SelectItems.ValueMember <> "PertracCode") Then
							List_SelectItems.ValueMember = "PertracCode"
						End If
					Catch ex As Exception
					End Try

					List_SelectItems.ResumeLayout()

				End If

			End If
		End If
	End Sub



#End Region

#Region " Member Add and Remove button events"

	Private Sub Button_AddAllItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_AddAllItems.Click
		' ***********************************************************************************
		' Add All Items From the SelectItems List to the GroupItems List
		'
		' Do not Duplicate Items.
		' ***********************************************************************************
		Dim ListTableRow As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow

		Try
			' If there is something to add....
			If (Me.List_SelectItems.Items.Count > 0) Then

				' Don't accidentally add 10,000 pertrac instruments. I tried it and it takes forever !
				If Me.Radio_Pertrac.Checked Then
					If MessageBox.Show("Are you sure that you want to add ALL " & List_SelectItems.Items.Count.ToString & " Pertrac instruments ?", "Add Instruments", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) = Windows.Forms.DialogResult.No Then
						Exit Sub
					End If
				End If

				Me.FormChanged = True

				Dim ListIndex As Integer
				Dim ListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable

				' Get pointer to the GroupItems List Table
				ListTable = CType(List_GroupItems.DataSource, RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)

				' Cycle through the Select Items, adding each one (if it does not already exist) to the GroupItems List

				For ListIndex = 0 To (List_SelectItems.Items.Count - 1)

					' If it this Items is not already in the Group...
					If GroupContainsID(CInt(List_SelectItems.Items(ListIndex)(List_SelectItems.ValueMember))) = False Then
						Dim ThisRow As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow

						' Construct a new Item Row.

						ThisRow = ListTable.NewRow
						ThisRow.GroupID = 0
						ThisRow.GroupItemID = 0
						ThisRow.GroupPertracCode = CInt(List_SelectItems.Items(ListIndex)(List_SelectItems.ValueMember))
						ThisRow.GroupIndexCode = 0
						ThisRow.GroupSector = ""

						If Me.Radio_Pertrac.Checked Then
							ThisRow("ListDescription") = CStr(List_SelectItems.Items(ListIndex)(List_SelectItems.DisplayMember))
							ThisRow("PertracName") = CStr(List_SelectItems.Items(ListIndex)("Mastername")) ' Mastername
							ThisRow("PertracProvider") = CStr(List_SelectItems.Items(ListIndex)("DataVendorName")) ' DataVendorName

						ElseIf Me.Radio_ExistingGroups.Checked Then
							ThisRow("ListDescription") = CStr(List_SelectItems.Items(ListIndex)(List_SelectItems.DisplayMember))
							ThisRow("PertracName") = CStr(List_SelectItems.Items(ListIndex)("PertracName"))	' Mastername
							ThisRow("PertracProvider") = CStr(List_SelectItems.Items(ListIndex)("PertracProvider"))	' DataVendorName

							If (Check_TransferDetails.Checked) And (Check_TransferDetails.Enabled) Then
								ListTableRow = CType(List_SelectItems.Items(ListIndex), DataRowView).Row

								ThisRow("TransferDetails") = Me.Check_TransferDetails.Checked
								ThisRow("TransferItemID") = ListTableRow.GroupItemID
								ThisRow.GroupIndexCode = ListTableRow.GroupIndexCode
								ThisRow.GroupSector = ListTableRow.GroupSector
							End If

						ElseIf Me.Radio_ExistingFunds.Checked Then
							ThisRow("ListDescription") = CStr(List_SelectItems.Items(ListIndex)(List_SelectItems.DisplayMember))
							ThisRow("PertracName") = CStr(List_SelectItems.Items(ListIndex)("Mastername")) ' Mastername
							ThisRow("PertracProvider") = CStr(List_SelectItems.Items(ListIndex)("DataVendorName")) ' DataVendorName

						End If

						ThisRow("IndexName") = ""
						ThisRow("IndexProvider") = ""

            Dim InstrumentFlags As RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags
            InstrumentFlags = RenaissancePertracDataClass.PertracDataClass.GetFlagsFromID(CUInt(ThisRow.GroupPertracCode))

            If (InstrumentFlags = RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Group_Mean) OrElse _
            (InstrumentFlags = RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Group_Median) OrElse _
            (InstrumentFlags = RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Group_Weighted) Then

              If (RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(CUInt(ThisRow.GroupPertracCode)) <> thisAuditID) Then
                ' Not this group, add it.

                ListTable.Rows.Add(ThisRow)

              End If

            Else
              ' Not a Group, Add it.

              ListTable.Rows.Add(ThisRow)

            End If

          End If
				Next

				List_SelectItems.SelectedIndex = (-1)
				List_GroupItems.SelectedIndex = (-1)

				SetButtonStatus()
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Adding All items to Group Items List.", ex.StackTrace, True)
		End Try

	End Sub

	Private Sub Button_AddItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_AddItem.Click
		' ***********************************************************************************
		' Add Selected Items From the SelectItems List to the GroupItems List
		'
		' Do not Duplicate Items.
		' Operates like 'Add All' except that it utilises the SelectedIndices() property of the SelectItems List.
		' ***********************************************************************************
		Dim ListTableRow As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow

		Try
			If Me.List_SelectItems.SelectedIndices.Count > 0 Then
				Me.FormChanged = True

				Dim ListIndex As Integer
				Dim ListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable

				' Get pointer to the GroupItems List Table

				ListTable = CType(List_GroupItems.DataSource, RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)

				' Cycle through the Selected Items, adding each one (if it does not already exist) to the GroupItems List

				For Each ListIndex In List_SelectItems.SelectedIndices
          If GroupContainsID(CInt(List_SelectItems.Items(ListIndex)(List_SelectItems.ValueMember))) = False Then

            Dim ThisRow As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow

            ' Construct a new Item Row.

            ThisRow = ListTable.NewRow
            ThisRow.GroupID = 0
            ThisRow.GroupItemID = 0
            ThisRow.GroupPertracCode = CInt(List_SelectItems.Items(ListIndex)(List_SelectItems.ValueMember))
            ThisRow.GroupIndexCode = 0
            ThisRow.GroupSector = ""

            If Me.Radio_Pertrac.Checked Then
              ThisRow("ListDescription") = CStr(List_SelectItems.Items(ListIndex)(List_SelectItems.DisplayMember))
              ThisRow("PertracName") = CStr(List_SelectItems.Items(ListIndex)("Mastername")) ' Mastername
              ThisRow("PertracProvider") = CStr(List_SelectItems.Items(ListIndex)("DataVendorName")) ' DataVendorName

            ElseIf Me.Radio_ExistingGroups.Checked Then
              ThisRow("ListDescription") = CStr(List_SelectItems.Items(ListIndex)(List_SelectItems.DisplayMember))
              ThisRow("PertracName") = CStr(List_SelectItems.Items(ListIndex)("PertracName")) ' Mastername
              ThisRow("PertracProvider") = CStr(List_SelectItems.Items(ListIndex)("PertracProvider")) ' DataVendorName

              If (Check_TransferDetails.Checked) And (Check_TransferDetails.Enabled) Then
                ListTableRow = CType(List_SelectItems.Items(ListIndex), DataRowView).Row

                ThisRow("TransferDetails") = Me.Check_TransferDetails.Checked
                ThisRow("TransferItemID") = ListTableRow.GroupItemID
                ThisRow.GroupIndexCode = ListTableRow.GroupIndexCode
                ThisRow.GroupSector = ListTableRow.GroupSector
              End If

            ElseIf Me.Radio_ExistingFunds.Checked Then
              ThisRow("ListDescription") = CStr(List_SelectItems.Items(ListIndex)(List_SelectItems.DisplayMember))
              ThisRow("PertracName") = CStr(List_SelectItems.Items(ListIndex)("Mastername")) ' Mastername
              ThisRow("PertracProvider") = CStr(List_SelectItems.Items(ListIndex)("DataVendorName")) ' DataVendorName

            End If

            ThisRow("IndexName") = ""
            ThisRow("IndexProvider") = ""

            ' Validate against adding a group to itself.

            Dim InstrumentFlags As RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags
            InstrumentFlags = RenaissancePertracDataClass.PertracDataClass.GetFlagsFromID(CUInt(ThisRow.GroupPertracCode))

            If (InstrumentFlags = RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Group_Mean) OrElse _
            (InstrumentFlags = RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Group_Median) OrElse _
            (InstrumentFlags = RenaissancePertracDataClass.PertracDataClass.PertracInstrumentFlags.Group_Weighted) Then

              If (RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(CUInt(ThisRow.GroupPertracCode)) <> thisAuditID) Then
                ' Not this group, add it.

                ListTable.Rows.Add(ThisRow)

              End If

            Else
              ' Not a Group, Add it.

              ListTable.Rows.Add(ThisRow)

            End If
          End If
				Next

				List_SelectItems.SelectedIndex = (-1)
				List_GroupItems.SelectedIndex = (-1)

				SetButtonStatus()
			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Adding to Group Items List.", ex.StackTrace, True)
		End Try

	End Sub

	Private Sub Button_RemoveItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_RemoveItem.Click
		' ***********************************************************************************
		' Remove Selected Items from the Group List
		'
		' A temporary array of Selected Rows must be created because of the way collections work.
		' ***********************************************************************************

		Try
			If Me.List_GroupItems.SelectedItems.Count > 0 Then
				Dim ListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable
				Dim SelectedRow As DataRowView
				Dim SelectedRows(List_GroupItems.SelectedItems.Count - 1) As DataRow
				Dim RowCount As Integer = 0

				ListTable = CType(List_GroupItems.DataSource, RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)

				For Each SelectedRow In List_GroupItems.SelectedItems
					SelectedRows(RowCount) = (SelectedRow.Row)
					RowCount += 1
				Next

				For RowCount = 0 To (SelectedRows.Length - 1)
					ListTable.Rows.Remove(SelectedRows(RowCount))
				Next

				List_SelectItems.SelectedIndex = (-1)
				List_GroupItems.SelectedIndex = (-1)

			End If
		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error removing Item(s) from Group Items.", ex.StackTrace, True)
		End Try
	End Sub

	Private Sub Button_RemoveAllItems_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_RemoveAllItems.Click
		' ***********************************************************************************
		' Remove All Items from the Group List
		'
		' ***********************************************************************************

		Try
			Dim ListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable

			ListTable = CType(List_GroupItems.DataSource, RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)
			ListTable.Rows.Clear()

			List_SelectItems.SelectedIndex = (-1)
			List_GroupItems.SelectedIndex = (-1)

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error clearing Group Items List.", ex.StackTrace, True)
		End Try

	End Sub

	Private Function GroupContainsID(ByVal pPertracCode As Integer) As Boolean
		' ***********************************************************************************
		' Function to check if the GroupItems List contains a given Instrument.
		'
		'
		' ***********************************************************************************

		Dim thisRow As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow
		Dim ListTable As New RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable

		Try
			If (List_GroupItems.DataSource Is Nothing) Then
				Return False
			End If

			ListTable = CType(List_GroupItems.DataSource, RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)

			For Each thisRow In ListTable
				If thisRow.GroupPertracCode = pPertracCode Then
					Return True
				End If
			Next

		Catch ex As Exception

		End Try

		Return False
	End Function

#End Region

#Region " Show Charts / List Select Event code"

	Private Sub Btn_ShowCarts_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Btn_ShowCarts.Click
		' *****************************************************************************
		' Button to toggle the display of the Instrument Charts.
		'
		' *****************************************************************************

		If (Split_All.Panel2Collapsed = False) Then
			' Hide Charts

			Me.Split_All.Panel2Collapsed = True
			Me.Split_Charts.SplitterDistance = Split_Charts.Width / 2

			Btn_ShowCarts.Text = "Show Charts"
		Else
			' Show Charts

			Me.Split_All.Panel2Collapsed = False
			Split_All.SplitterDistance = Split_All.Width * 2 / 3
			Me.Split_GroupEdit.SplitterDistance = Split_All.Width / 3
			Me.Split_Charts.SplitterDistance = Split_Charts.Height / 2

			Btn_ShowCarts.Text = "Hide Charts"
		End If

	End Sub

	Private _validData As Boolean

	Private Sub List_GroupItems_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles List_GroupItems.DragEnter
		'get the data
		Dim DragString As String = CType(e.Data.GetData(GetType(String)), String)

		'no empty data
		If DragString Is Nothing OrElse DragString.Length = 0 Then
			_validData = False
			Return
		End If

		If (DragString IsNot Nothing) AndAlso (DragString.StartsWith(GENOA_DRAG_IDs_HEADER)) Then
			If DragString.Split(",").Length > 1 Then
				_validData = True
				Return
			End If
		End If

		_validData = False

	End Sub

	Private Sub List_GroupItems_DragLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles List_GroupItems.DragLeave
		_validData = False
	End Sub

	Private Sub List_GroupItems_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles List_GroupItems.DragOver
		If _validData Then
			e.Effect = DragDropEffects.Copy
		Else
			e.Effect = DragDropEffects.None
		End If

	End Sub

	Private Sub List_GroupItems_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles List_GroupItems.DragDrop
		' ***********************************************************************************
		'
		'
		'
		' ***********************************************************************************

		'get the data
		Dim DragString As String = CType(e.Data.GetData(GetType(String)), String)

		'no empty data
		If (DragString Is Nothing) OrElse (DragString.StartsWith(GENOA_DRAG_IDs_HEADER) = False) Then
			Return
		End If

		' Process
		Dim ListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable
		Dim ListTableRow As RenaissanceDataClass.DSGroupItems.tblGroupItemsRow
		Dim InformationArray() As Object

		Dim PertracID As Integer
		Dim ID_Strings() As String
		Dim ID_Counter As Integer

		Try
			If (Me.HasUpdatePermission = False) Then
				Return
			End If

			' Get Dragged IDs

			ID_Strings = DragString.Split(",")
			' Note, the first element should be the Header String, so don't count it as an ID.
			If (ID_Strings Is Nothing) OrElse (ID_Strings.Length <= 1) Then
				Return
			End If

			' Get pointer to the GroupItems List Table

			ListTable = CType(List_GroupItems.DataSource, RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)

			' Cycle through the Selected Items, adding each one (if it does not already exist) to the GroupItems List

			For ID_Counter = 1 To (ID_Strings.Length - 1)
				If IsNumeric(ID_Strings(ID_Counter)) Then
					PertracID = CInt(ID_Strings(ID_Counter))

					If GroupContainsID(PertracID) = False Then
						If (PertracID > 0) Then
							ListTableRow = ListTable.NewRow
							ListTableRow.GroupID = 0
							ListTableRow.GroupItemID = 0
							ListTableRow.GroupPertracCode = PertracID
							ListTableRow.GroupIndexCode = 0
							ListTableRow.GroupSector = ""

							InformationArray = MainForm.PertracData.GetInformationValues(PertracID)

							Try
								ListTableRow("PertracName") = Nz(InformationArray(PertracInformationFields.FundName), "<No Name>").ToString
								ListTableRow("PertracProvider") = Nz(InformationArray(PertracInformationFields.DataVendorName), "<No Name>").ToString
								ListTableRow("ListDescription") = ListTableRow("PertracName").ToString & " (" & ListTableRow("PertracProvider").ToString & ")"
								ListTableRow("IndexName") = ""
								ListTableRow("IndexProvider") = ""

							Catch ex As Exception
							End Try

							ListTable.Rows.Add(ListTableRow)

						End If
					End If

				End If
			Next

			List_GroupItems.SelectedIndex = (-1)
			Call FormControlChanged(List_GroupItems, New System.EventArgs)
			SetButtonStatus()

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error Droping Instruments on the Group Items List.", ex.StackTrace, True)
		End Try

	End Sub

	Private Sub List_GroupItems_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles List_GroupItems.GotFocus
		' *****************************************************************************
		' 
		'
		' *****************************************************************************

		' Call List_GroupItems_SelectedIndexChanged(Nothing, New EventArgs)
	End Sub

	Private Sub List_GroupItems_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles List_GroupItems.SelectedIndexChanged
		' *****************************************************************************
		' Update the Instrument charts when Instruments are selected / Unselected
		'
		' *****************************************************************************

		Dim InstrumentCounter As Integer
		Dim PertracID As Integer
		Dim PertracName As String
		Dim SelectedRow As DataRowView
		Dim thisChart As Dundas.Charting.WinControl.Chart


		If (Me.List_GroupItems.Focused = True) Then

			SyncLock PriceChartArrayList
				For Each thisChart In Me.PriceChartArrayList
					If (thisChart.Visible) Then
						' Set Prices Chart Series Count

						Try
							While (thisChart.Series.Count > List_GroupItems.SelectedItems.Count)
								thisChart.Series.RemoveAt(thisChart.Series.Count - 1)
							End While

							While (thisChart.Series.Count < List_GroupItems.SelectedItems.Count)
								thisChart.Series.Add("PS" & thisChart.Series.Count.ToString)
							End While
						Catch ex As Exception
						End Try

						For InstrumentCounter = 0 To (List_GroupItems.SelectedItems.Count - 1)
							Try
								PertracID = CInt(List_GroupItems.SelectedItems(InstrumentCounter)(List_GroupItems.ValueMember))
                PertracName = CStr(Nz(List_GroupItems.SelectedItems(InstrumentCounter)(List_GroupItems.DisplayMember), PertracID.ToString))
							Catch ex As Exception
								PertracID = 0
								PertracName = "<Error>"
							End Try

							Try
                Set_LineChart(MainForm, MainForm.PertracData.GetPertracDataPeriod(PertracID), PertracID, thisChart, InstrumentCounter, PertracName, -1, -1, 1.0#, Renaissance_BaseDate, Renaissance_EndDate_Data)
							Catch ex As Exception
							End Try
						Next
					End If
				Next
			End SyncLock

			SyncLock ReturnsChartArrayList
				For Each thisChart In Me.ReturnsChartArrayList
					If (thisChart.Visible) Then
						' Set Prices Chart Series Count

						Try
							While (thisChart.Series.Count > List_GroupItems.SelectedItems.Count)
								thisChart.Series.RemoveAt(thisChart.Series.Count - 1)
							End While

							While (thisChart.Series.Count < List_GroupItems.SelectedItems.Count)
								thisChart.Series.Add("PS" & thisChart.Series.Count.ToString)
							End While
						Catch ex As Exception
						End Try

						For InstrumentCounter = 0 To (List_GroupItems.SelectedItems.Count - 1)
							Try
								SelectedRow = CType(List_GroupItems.SelectedItems(InstrumentCounter), DataRowView)
								PertracID = CInt(SelectedRow.Row("GroupPertracCode"))
                PertracName = CStr(Nz(SelectedRow.Row(List_GroupItems.DisplayMember), PertracID.ToString))
							Catch ex As Exception
								PertracID = 0
								PertracName = "<Error>"
							End Try

							Try
                Set_ReturnsBarChart(MainForm, MainForm.PertracData.GetPertracDataPeriod(PertracID), PertracID, thisChart, InstrumentCounter, PertracName, -1, -1, 1.0#, Renaissance_BaseDate, Renaissance_EndDate_Data)
							Catch ex As Exception
							End Try
						Next
					End If
				Next
			End SyncLock

		End If

	End Sub

	Private Sub List_SelectItems_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles List_SelectItems.GotFocus
		' *****************************************************************************
		' 
		'
		' *****************************************************************************

		' List_SelectItems_SelectedIndexChanged(Nothing, New EventArgs)
	End Sub


	Private Sub List_SelectItems_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles List_SelectItems.SelectedIndexChanged
		' *****************************************************************************
		' Update the Instrument charts when Instruments are selected / Unselected
		'
		' *****************************************************************************

		Dim InstrumentCounter As Integer
		Dim PertracID As Integer
		Dim PertracName As String
		Dim thisChart As Dundas.Charting.WinControl.Chart

		If (Me.List_GroupItems.Focused = False) Then

			SyncLock PriceChartArrayList
				For Each thisChart In Me.PriceChartArrayList
					If (thisChart.Visible) Then
						' Set Prices Chart Series Count

						Try
							While (thisChart.Series.Count > List_SelectItems.SelectedItems.Count)
								thisChart.Series.RemoveAt(thisChart.Series.Count - 1)
							End While

							While (thisChart.Series.Count < List_SelectItems.SelectedItems.Count)
								thisChart.Series.Add("PS" & thisChart.Series.Count.ToString)
							End While
						Catch ex As Exception
						End Try

						For InstrumentCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)
							Try
								PertracID = CInt(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.ValueMember))
								PertracName = CStr(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.DisplayMember))
							Catch ex As Exception
								PertracID = 0
								PertracName = "<Error>"
							End Try

							Try
                Set_LineChart(MainForm, MainForm.PertracData.GetPertracDataPeriod(PertracID), PertracID, thisChart, InstrumentCounter, PertracName, -1, -1, 1.0#, Renaissance_BaseDate, Renaissance_EndDate_Data)
							Catch ex As Exception
							End Try
						Next
					End If
				Next
			End SyncLock

			SyncLock ReturnsChartArrayList
				For Each thisChart In Me.ReturnsChartArrayList
					If (thisChart.Visible) Then
						' Set Prices Chart Series Count

						Try
							While (thisChart.Series.Count > List_SelectItems.SelectedItems.Count)
								thisChart.Series.RemoveAt(thisChart.Series.Count - 1)
							End While

							While (thisChart.Series.Count < List_SelectItems.SelectedItems.Count)
								thisChart.Series.Add("PS" & thisChart.Series.Count.ToString)
							End While
						Catch ex As Exception
						End Try

						For InstrumentCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)
							Try
								PertracID = CInt(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.ValueMember))
								PertracName = CStr(List_SelectItems.SelectedItems(InstrumentCounter)(List_SelectItems.DisplayMember))
							Catch ex As Exception
								PertracID = 0
								PertracName = "<Error>"
							End Try

							Try
                Set_ReturnsBarChart(MainForm, MainForm.PertracData.GetPertracDataPeriod(PertracID), PertracID, thisChart, InstrumentCounter, PertracName, -1, -1, 1.0#, Renaissance_BaseDate, Renaissance_EndDate_Data)
							Catch ex As Exception
							End Try
						Next
					End If
				Next
			End SyncLock

		End If

	End Sub

#End Region

#Region " Bug hunting "

	' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
	' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
	' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

#End Region

	Private Sub Chart_Returns_AxisViewChanged(ByVal sender As Object, ByVal e As Dundas.Charting.WinControl.ViewEventArgs) Handles Chart_Returns.AxisViewChanged
		' *******************************************************************************
		' If an Axis selection is made, reflect that to the other charts.
		' *******************************************************************************

		Dim thisChart As Dundas.Charting.WinControl.Chart

		If (Me.List_GroupItems.Focused = False) Then
			SyncLock PriceChartArrayList

				For Each thisChart In Me.PriceChartArrayList
					If (thisChart.Visible) AndAlso (thisChart IsNot sender) Then

						thisChart.ChartAreas(0).Axes(e.Axis.Type).View.Zoom(e.Axis.View.Position, e.Axis.View.Size, e.Axis.View.SizeType)
						'thisChart.ResetAutoValues()

					End If
				Next

				For Each thisChart In Me.ReturnsChartArrayList
					If (thisChart.Visible) AndAlso (thisChart IsNot sender) Then

						thisChart.ChartAreas(0).Axes(e.Axis.Type).View.Zoom(e.Axis.View.Position, e.Axis.View.Size, e.Axis.View.SizeType)
						'thisChart.ResetAutoValues()

					End If
				Next
			End SyncLock
		End If


		'Chart_Prices.ChartAreas(0).Axes(e.Axis.Type).View.Zoom(e.Axis.View.Position, e.Axis.View.Size, e.Axis.View.SizeType)
		'Chart_Prices.ResetAutoValues()

	End Sub

	Private Sub Chart_Prices_AxisViewChanged(ByVal sender As Object, ByVal e As Dundas.Charting.WinControl.ViewEventArgs) Handles Chart_Prices.AxisViewChanged
		' *******************************************************************************
		' If an Axis selection is made, reflect that to the other charts.
		' *******************************************************************************

		Dim thisChart As Dundas.Charting.WinControl.Chart

		If (Me.List_GroupItems.Focused = False) Then
			SyncLock PriceChartArrayList

				For Each thisChart In Me.PriceChartArrayList
					If (thisChart.Visible) AndAlso (thisChart IsNot sender) Then

						thisChart.ChartAreas(0).Axes(e.Axis.Type).View.Zoom(e.Axis.View.Position, e.Axis.View.Size, e.Axis.View.SizeType)
						'thisChart.ResetAutoValues()

					End If
				Next

				For Each thisChart In Me.ReturnsChartArrayList
					If (thisChart.Visible) AndAlso (thisChart IsNot sender) Then

						thisChart.ChartAreas(0).Axes(e.Axis.Type).View.Zoom(e.Axis.View.Position, e.Axis.View.Size, e.Axis.View.SizeType)
						'thisChart.ResetAutoValues()

					End If
				Next
			End SyncLock
		End If

	End Sub

	Private Sub Chart_Returns_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Returns.DoubleClick
		' *******************************************************************************
		' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
		' with this chart.
		' *******************************************************************************

		Dim newChartForm As frmViewChart

		newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
		newChartForm.GenoaParentForm = Me
		newChartForm.FormChart = Chart_Returns
		newChartForm.ParentChartList = ReturnsChartArrayList

		SyncLock ReturnsChartArrayList
			ReturnsChartArrayList.Add(newChartForm.DisplayedChart)
		End SyncLock

	End Sub

	Private Sub Chart_Prices_DClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Chart_Prices.DoubleClick
		' *******************************************************************************
		' If the chart is Double-Clicked on, Spawn a new Chart Form and update the contents
		' with this chart.
		' *******************************************************************************

		Dim newChartForm As frmViewChart

		newChartForm = MainForm.New_GenoaForm(GenoaFormID.frmViewChart).Form
		newChartForm.GenoaParentForm = Me
		newChartForm.FormChart = Chart_Prices
		newChartForm.ParentChartList = PriceChartArrayList

		SyncLock PriceChartArrayList
			PriceChartArrayList.Add(newChartForm.DisplayedChart)
		End SyncLock

	End Sub


	Private Sub List_SelectItems_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles List_SelectItems.KeyDown
		' *************************************************************************************
		' Clipboard 'Copy' functionality
		'
		'
		' *************************************************************************************
		Try
			If e.Control And (e.KeyCode = Keys.C) Then
				Call MainForm.CopyListSelection(CType(sender, ListBox))
				e.SuppressKeyPress = True
			End If
		Catch ex As Exception
		End Try
	End Sub

	Private Sub List_GroupItems_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles List_GroupItems.KeyDown
		' *************************************************************************************
		' Clipboard 'Copy' functionality
		'
		'
		' *************************************************************************************
		Try
			If e.Control And (e.KeyCode = Keys.C) Then
				Call MainForm.CopyListSelection(CType(sender, ListBox))
			End If
		Catch ex As Exception
		End Try
	End Sub


End Class
