Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceControls
Imports RenaissanceDataClass

Imports Genoa.MaxFunctions
Imports C1.Win.C1FlexGrid

Public Class frmCustomDataMaintenance

	Inherits System.Windows.Forms.Form
	Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Friend WithEvents StatusStrip_Search As System.Windows.Forms.StatusStrip
  Friend WithEvents Form_ProgressBar As System.Windows.Forms.ToolStripProgressBar
	Friend WithEvents StatusLabel_DataMaintenance As System.Windows.Forms.ToolStripStatusLabel
	Friend WithEvents Label6 As System.Windows.Forms.Label
	Friend WithEvents Combo_CustomField As System.Windows.Forms.ComboBox
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents Combo_SelectGroupID As System.Windows.Forms.ComboBox
	Friend WithEvents Grid_CustomFieldStatus As C1.Win.C1FlexGrid.C1FlexGrid
	Friend WithEvents Btn_CancelCalculation As System.Windows.Forms.Button
	Friend WithEvents ProgressBar_Calculation As System.Windows.Forms.ProgressBar
	Friend WithEvents Menu_Options As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents Menu_Options_UpdateStatusGrid As System.Windows.Forms.ToolStripMenuItem
	Friend WithEvents Menu_Options_PostUpdateEvents As System.Windows.Forms.ToolStripMenuItem

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCustomDataMaintenance))
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Menu_Options = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Options_UpdateStatusGrid = New System.Windows.Forms.ToolStripMenuItem
    Me.Menu_Options_PostUpdateEvents = New System.Windows.Forms.ToolStripMenuItem
    Me.StatusStrip_Search = New System.Windows.Forms.StatusStrip
    Me.Form_ProgressBar = New System.Windows.Forms.ToolStripProgressBar
    Me.StatusLabel_DataMaintenance = New System.Windows.Forms.ToolStripStatusLabel
    Me.Label6 = New System.Windows.Forms.Label
    Me.Combo_CustomField = New System.Windows.Forms.ComboBox
    Me.Label1 = New System.Windows.Forms.Label
    Me.Combo_SelectGroupID = New System.Windows.Forms.ComboBox
    Me.Grid_CustomFieldStatus = New C1.Win.C1FlexGrid.C1FlexGrid
    Me.Btn_CancelCalculation = New System.Windows.Forms.Button
    Me.ProgressBar_Calculation = New System.Windows.Forms.ProgressBar
    Me.RootMenu.SuspendLayout()
    Me.StatusStrip_Search.SuspendLayout()
    CType(Me.Grid_CustomFieldStatus, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'RootMenu
    '
    Me.RootMenu.AllowMerge = False
    Me.RootMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Options})
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(875, 26)
    Me.RootMenu.TabIndex = 12
    Me.RootMenu.Text = "MenuStrip1"
    '
    'Menu_Options
    '
    Me.Menu_Options.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Menu_Options_UpdateStatusGrid, Me.Menu_Options_PostUpdateEvents})
    Me.Menu_Options.Name = "Menu_Options"
    Me.Menu_Options.Size = New System.Drawing.Size(69, 22)
    Me.Menu_Options.Text = "&Options"
    '
    'Menu_Options_UpdateStatusGrid
    '
    Me.Menu_Options_UpdateStatusGrid.CheckOnClick = True
    Me.Menu_Options_UpdateStatusGrid.Name = "Menu_Options_UpdateStatusGrid"
    Me.Menu_Options_UpdateStatusGrid.Size = New System.Drawing.Size(468, 22)
    Me.Menu_Options_UpdateStatusGrid.Text = "Update Status Grid while calculating Field Data"
    '
    'Menu_Options_PostUpdateEvents
    '
    Me.Menu_Options_PostUpdateEvents.Checked = True
    Me.Menu_Options_PostUpdateEvents.CheckOnClick = True
    Me.Menu_Options_PostUpdateEvents.CheckState = System.Windows.Forms.CheckState.Checked
    Me.Menu_Options_PostUpdateEvents.Name = "Menu_Options_PostUpdateEvents"
    Me.Menu_Options_PostUpdateEvents.Size = New System.Drawing.Size(468, 22)
    Me.Menu_Options_PostUpdateEvents.Text = "Post Interim Update messages while calculating Field Data"
    '
    'StatusStrip_Search
    '
    Me.StatusStrip_Search.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form_ProgressBar, Me.StatusLabel_DataMaintenance})
    Me.StatusStrip_Search.Location = New System.Drawing.Point(0, 635)
    Me.StatusStrip_Search.Name = "StatusStrip_Search"
    Me.StatusStrip_Search.Size = New System.Drawing.Size(875, 23)
    Me.StatusStrip_Search.TabIndex = 16
    Me.StatusStrip_Search.Text = "StatusStrip1"
    '
    'Form_ProgressBar
    '
    Me.Form_ProgressBar.Name = "Form_ProgressBar"
    Me.Form_ProgressBar.Size = New System.Drawing.Size(200, 17)
    Me.Form_ProgressBar.Visible = False
    '
    'StatusLabel_DataMaintenance
    '
    Me.StatusLabel_DataMaintenance.Name = "StatusLabel_DataMaintenance"
    Me.StatusLabel_DataMaintenance.Size = New System.Drawing.Size(33, 18)
    Me.StatusLabel_DataMaintenance.Text = "     "
    '
    'Label6
    '
    Me.Label6.AutoSize = True
    Me.Label6.Location = New System.Drawing.Point(1, 32)
    Me.Label6.Name = "Label6"
    Me.Label6.Size = New System.Drawing.Size(67, 13)
    Me.Label6.TabIndex = 19
    Me.Label6.Text = "Custom Field"
    '
    'Combo_CustomField
    '
    Me.Combo_CustomField.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_CustomField.Location = New System.Drawing.Point(116, 29)
    Me.Combo_CustomField.Name = "Combo_CustomField"
    Me.Combo_CustomField.Size = New System.Drawing.Size(318, 21)
    Me.Combo_CustomField.TabIndex = 17
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(1, 59)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(69, 18)
    Me.Label1.TabIndex = 20
    Me.Label1.Text = "Group"
    '
    'Combo_SelectGroupID
    '
    Me.Combo_SelectGroupID.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Combo_SelectGroupID.Location = New System.Drawing.Point(116, 56)
    Me.Combo_SelectGroupID.Name = "Combo_SelectGroupID"
    Me.Combo_SelectGroupID.Size = New System.Drawing.Size(318, 21)
    Me.Combo_SelectGroupID.TabIndex = 18
    '
    'Grid_CustomFieldStatus
    '
    Me.Grid_CustomFieldStatus.AllowEditing = False
    Me.Grid_CustomFieldStatus.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_CustomFieldStatus.AutoGenerateColumns = False
    Me.Grid_CustomFieldStatus.ColumnInfo = resources.GetString("Grid_CustomFieldStatus.ColumnInfo")
    Me.Grid_CustomFieldStatus.Cursor = System.Windows.Forms.Cursors.Default
    Me.Grid_CustomFieldStatus.Location = New System.Drawing.Point(4, 95)
    Me.Grid_CustomFieldStatus.Name = "Grid_CustomFieldStatus"
    Me.Grid_CustomFieldStatus.Rows.Count = 13
    Me.Grid_CustomFieldStatus.Rows.DefaultSize = 17
    Me.Grid_CustomFieldStatus.SelectionMode = C1.Win.C1FlexGrid.SelectionModeEnum.ListBox
    Me.Grid_CustomFieldStatus.Size = New System.Drawing.Size(865, 535)
    Me.Grid_CustomFieldStatus.TabIndex = 105
    Me.Grid_CustomFieldStatus.Tree.Column = 0
    '
    'Btn_CancelCalculation
    '
    Me.Btn_CancelCalculation.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Btn_CancelCalculation.Location = New System.Drawing.Point(440, 29)
    Me.Btn_CancelCalculation.Name = "Btn_CancelCalculation"
    Me.Btn_CancelCalculation.Size = New System.Drawing.Size(429, 31)
    Me.Btn_CancelCalculation.TabIndex = 106
    Me.Btn_CancelCalculation.Text = "Cancel Field Calculation"
    Me.Btn_CancelCalculation.UseVisualStyleBackColor = True
    Me.Btn_CancelCalculation.Visible = False
    '
    'ProgressBar_Calculation
    '
    Me.ProgressBar_Calculation.Location = New System.Drawing.Point(440, 66)
    Me.ProgressBar_Calculation.Name = "ProgressBar_Calculation"
    Me.ProgressBar_Calculation.Size = New System.Drawing.Size(429, 21)
    Me.ProgressBar_Calculation.Step = 1
    Me.ProgressBar_Calculation.TabIndex = 107
    Me.ProgressBar_Calculation.Visible = False
    '
    'frmCustomDataMaintenance
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.ClientSize = New System.Drawing.Size(875, 658)
    Me.Controls.Add(Me.ProgressBar_Calculation)
    Me.Controls.Add(Me.Btn_CancelCalculation)
    Me.Controls.Add(Me.Grid_CustomFieldStatus)
    Me.Controls.Add(Me.Label6)
    Me.Controls.Add(Me.Combo_CustomField)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Combo_SelectGroupID)
    Me.Controls.Add(Me.StatusStrip_Search)
    Me.Controls.Add(Me.RootMenu)
    Me.MainMenuStrip = Me.RootMenu
    Me.MinimumSize = New System.Drawing.Size(500, 400)
    Me.Name = "frmCustomDataMaintenance"
    Me.Text = "Custom Field Maintenance"
    Me.RootMenu.ResumeLayout(False)
    Me.RootMenu.PerformLayout()
    Me.StatusStrip_Search.ResumeLayout(False)
    Me.StatusStrip_Search.PerformLayout()
    CType(Me.Grid_CustomFieldStatus, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "

	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
	Private WithEvents _MainForm As GenoaMain

	' Form ToolTip
	Private FormTooltip As New ToolTip()

	' Form Constants, specific to the table being updated.

	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form specific Permissioning variables
	Private THIS_FORM_PermissionArea As String
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
	Private THIS_FORM_FormID As GenoaFormID

	' Form Status Flags

	Private FormIsValid As Boolean
	Private _FormOpenFailed As Boolean
	Private InPaint As Boolean
	Private In_PaintTimer_Tick As Boolean
	Private RequestPaint As Boolean = False

	Private RequestGetData As Boolean = False
	Private RequestGetDataTime As Date


	Private _InUse As Boolean
	Private FormChanged As Boolean

	Private WithEvents PaintTimer As System.Windows.Forms.Timer

	' User Permission Flags

	Private HasReadPermission As Boolean
	Private HasUpdatePermission As Boolean
	Private HasInsertPermission As Boolean
	Private HasDeletePermission As Boolean


	' Private GridContextMenuStrip As ContextMenuStrip
	Private GridClickMouseRow As Integer = 0
	Private GridClickMouseCol As Integer = 0
	Private GridClickCustomFieldID As Integer = 0
	Private GridClickPertracID As Integer = 0

	' Custom Field Stuff

	Private FieldCalculationObject As CustomFieldCalculationClass

	Private WithEvents CustomStatusWorker As System.ComponentModel.BackgroundWorker = Nothing
	Private FormControls As ArrayList = Nothing
	Private WithEvents SQLFetchTimer As New Windows.Forms.Timer()

	Private WithEvents CustomCalculationWorker As System.ComponentModel.BackgroundWorker = Nothing
	Private WithEvents CustomCalculationTimer As New Windows.Forms.Timer()

	Private tblCustomFieldStatusSummary As DataTable
	Private tblCustomFieldStatus As DataTable

	Private CustomStatus_GettingData As Boolean = False
	Private CustomStatus_GettingData_LockObject As New Object
	Private CustomStatus_DataReady As Boolean = False
	Private CustomStatusWorker_GroupID As Integer
	Private CustomStatusWorker_CustomFieldID As Integer
	Private CustomStatusWorker_PertracID As Integer

	Private CustomFieldCalc_StartLock As New Object
	Private CustomFieldCalc_CustomFieldID As Integer
	Private CustomFieldCalc_PertracID As Integer
	Private CustomFieldCalc_ForceRecalc As Boolean
	Private CustomFieldCalc_CancelFlag As Boolean

  Private _DefaultStatsDatePeriod As DealingPeriod = DealingPeriod.Monthly



#End Region

#Region " Form 'Properties' "

	Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

	Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)
		End Set
	End Property

	Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

	Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
		Get
			Return _InUse
		End Get
	End Property

	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

  Public Property DefaultStatsDatePeriod() As DealingPeriod
    Get
      Return _DefaultStatsDatePeriod
    End Get
    Set(ByVal value As DealingPeriod)
      _DefaultStatsDatePeriod = value
    End Set
  End Property

#End Region

#Region " New(), Field Menu Functions"

	Public Sub New(ByVal pMainForm As GenoaMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		Try
			Me.Cursor = Cursors.WaitCursor

			_MainForm = pMainForm
			AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate
			AddHandler SQLFetchTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent
			AddHandler CustomCalculationTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

			_FormOpenFailed = False
			_InUse = True
      _DefaultStatsDatePeriod = pMainForm.DefaultStatsDatePeriod

			' ******************************************************
			' Form Specific Settings :
			' ******************************************************

			' Form Permissioning :-

			THIS_FORM_PermissionArea = Me.Name
			THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

			' 'This' form ID

			THIS_FORM_FormID = GenoaFormID.frmCustomDataMaintenance

			' Custom Field calculation class

			FieldCalculationObject = New CustomFieldCalculationClass(pMainForm)

			' Format Event Handlers for form controls

			AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

			AddHandler Combo_CustomField.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
			AddHandler Combo_CustomField.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
			AddHandler Combo_CustomField.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
			AddHandler Combo_CustomField.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

			AddHandler Combo_SelectGroupID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
			AddHandler Combo_SelectGroupID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
			AddHandler Combo_SelectGroupID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
			AddHandler Combo_SelectGroupID.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

			' Form Control Changed events

			' Set up the ToolTip
			MainForm.SetFormToolTip(Me, FormTooltip)

			' Grid :- Grid_CustomFieldStatus

			Grid_CustomFieldStatus.Rows.Count = 1
			Grid_CustomFieldStatus.Cols.Fixed = 0

			' Data Tables 
			tblCustomFieldStatusSummary = New DataTable
			tblCustomFieldStatus = New DataTable

			'styles
			Dim cs As CellStyle = Grid_CustomFieldStatus.Styles.Normal
			cs.Border.Direction = BorderDirEnum.Vertical
			cs.WordWrap = False

			cs = Grid_CustomFieldStatus.Styles.Add("Data")
			cs.BackColor = SystemColors.Info
			cs.ForeColor = Color.Black

			cs = Grid_CustomFieldStatus.Styles.Add("DataNegative")
			cs.BackColor = SystemColors.Info
			cs.ForeColor = Color.Red

			cs = Grid_CustomFieldStatus.Styles.Add("SourceNode")
			cs.BackColor = Color.Yellow
			cs.Font = New Font(Grid_CustomFieldStatus.Font, FontStyle.Bold)

			cs = Grid_CustomFieldStatus.Styles.Add("PositionPositive")
			cs.ForeColor = Color.Black
			cs.Font = New Font(Grid_CustomFieldStatus.Font, FontStyle.Bold)

			cs = Grid_CustomFieldStatus.Styles.Add("PositionNegative")
			cs.ForeColor = Color.Red
			cs.Font = New Font(Grid_CustomFieldStatus.Font, FontStyle.Bold)

			'outline tree

			Grid_CustomFieldStatus.Tree.Column = 0
			Grid_CustomFieldStatus.Tree.Style = TreeStyleFlags.Simple
			Grid_CustomFieldStatus.AllowMerging = AllowMergingEnum.Nodes

			'other
			Grid_CustomFieldStatus.AllowResizing = AllowResizingEnum.Columns
			Grid_CustomFieldStatus.SelectionMode = SelectionModeEnum.RowRange

			' ******************************************************
			' End Form Specific.
			' ******************************************************

			Try
				InPaint = True

				Grid_CustomFieldStatus.ContextMenuStrip = New ContextMenuStrip
				AddHandler Grid_CustomFieldStatus.ContextMenuStrip.Opening, AddressOf cms_Opening
				Grid_CustomFieldStatus.ContextMenuStrip.Items.Add(New ToolStripMenuItem(" "))

			Catch ex As Exception
			Finally
				InPaint = False
			End Try

		Catch ex As Exception
		Finally
			Me.Cursor = Cursors.Default
		End Try

	End Sub

#End Region

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
	Public Sub ResetForm() Implements StandardGenoaForm.ResetForm
		' *****************************************************************************************
		' Sub called when a form is re-used, after being hidden but not destroyed when closed
		' at an earlier point.
		' *****************************************************************************************

		Call Form_Load(Me, New System.EventArgs)
	End Sub

	Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
		' *****************************************************************************************
		' Forced closure of this form.
		'
		' *****************************************************************************************

		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			Me.Cursor = Cursors.WaitCursor

			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
			_FormOpenFailed = False
			_InUse = True

			' Initialise Data structures. Connection, Adaptor and Dataset.

			If Not (MainForm Is Nothing) Then
				FormIsValid = True
			Else
				MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
				FormIsValid = False
				_FormOpenFailed = True
				Exit Sub
			End If

			' Initialise Paint Timer

			If (PaintTimer IsNot Nothing) Then
				Try
					PaintTimer.Stop()
				Catch ex As Exception
				Finally
					PaintTimer = Nothing
				End Try
			End If
			Try
				PaintTimer = New System.Windows.Forms.Timer
				In_PaintTimer_Tick = False
				PaintTimer.Interval = 750
				AddHandler PaintTimer.Tick, AddressOf PaintTimer_Tick
				PaintTimer.Start()
			Catch ex As Exception
			End Try

			' Initialse form

			InPaint = True
			IsOverCancelButton = False
			Try
				' Check User permissions

				Call CheckPermissions()
				If (HasReadPermission = False) Then
					Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

					FormIsValid = False
					_FormOpenFailed = True
					Exit Sub
				End If

				' Initialise Combos

				Call SetGroupCombo()
				Call SetCustomFieldCombo()

				' Display initial record.

				Grid_CustomFieldStatus.Rows.Count = 1
				Form_ProgressBar.Visible = False
				CustomStatus_DataReady = False
				CustomStatus_GettingData = False

				Dim ColumnCount As Integer
				For ColumnCount = 1 To (Grid_CustomFieldStatus.Cols.Count - 1)
					Grid_CustomFieldStatus.Cols(ColumnCount).Visible = False
				Next

				' Clear Target Tables

				If (tblCustomFieldStatusSummary Is Nothing) Then
					tblCustomFieldStatusSummary = New DataTable
				Else
					SyncLock tblCustomFieldStatusSummary
						tblCustomFieldStatusSummary.Clear()
					End SyncLock
				End If

				If (tblCustomFieldStatus Is Nothing) Then
					tblCustomFieldStatus = New DataTable
				Else
					SyncLock tblCustomFieldStatus
						tblCustomFieldStatus.Clear()
					End SyncLock
				End If

				Call GetCustomFieldStatus(0, 0)

			Catch ex As Exception
			Finally
				InPaint = False
			End Try


		Catch ex As Exception
		Finally
			Me.Cursor = Cursors.Default
		End Try

	End Sub

	Private Sub frmEntity_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False

		If (PaintTimer IsNot Nothing) Then
			Try
				PaintTimer.Stop()
			Catch ex As Exception
			Finally
				PaintTimer = Nothing
			End Try
		End If

		' Tidy up the Status Worker object.

		Try
			If (CustomStatusWorker IsNot Nothing) Then
				If (CustomStatusWorker.IsBusy) Then
					CustomStatusWorker.CancelAsync()
				End If

				CustomStatus_DataReady = False
				CustomStatus_GettingData = False
				Form_ProgressBar.Visible = False
				StatusLabel_DataMaintenance.Text = ""

				If (FormControls IsNot Nothing) AndAlso (FormControls.Count > 0) Then
					MainForm.EnableFormControls(FormControls)
				End If

			End If
		Catch ex As Exception
		Finally
			CustomStatusWorker = Nothing
		End Try

		Try
			SQLFetchTimer.Stop()
		Catch ex As Exception
		End Try

		' Tidy up the Custom field calculation worker.

		If (CustomCalculationWorker IsNot Nothing) Then
			If (CustomCalculationWorker.IsBusy = False) Then
				CustomCalculationWorker = Nothing
			Else
				CustomCalculationWorker.CancelAsync()
				CustomCalculationWorker = Nothing
			End If
		End If

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else

			HideForm = True
			If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate
				RemoveHandler SQLFetchTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent
				RemoveHandler CustomCalculationTimer.Tick, AddressOf MainForm.IncrementStatusBarTimerEvent

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Combo_CustomField.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_CustomField.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_CustomField.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_CustomField.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

				RemoveHandler Combo_SelectGroupID.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_SelectGroupID.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_SelectGroupID.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_SelectGroupID.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

				FieldCalculationObject.Finalise()
				FieldCalculationObject = Nothing

			Catch ex As Exception
			End Try
		End If

	End Sub

	Private Sub PaintTimer_Tick(ByVal Sender As Object, ByVal e As EventArgs)
		' *******************************************************************************
		' As with other forms where the refresh process is lengthly, this form refreshes the
		' form data using a timer and an update flag.
		' This is so that the selection criteria can change rapidly without an update being 
		' processed, and the process initiates once the selection criteria stop changing.
		'
		' *******************************************************************************

		If (RequestGetData) AndAlso ((Now() - RequestGetDataTime).TotalSeconds >= 1.0) Then
			RequestGetData = False

			Dim CustomFieldID As Integer = 0
			Dim GroupID As Integer = 0

			If (Combo_CustomField.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_CustomField.SelectedValue)) Then
				CustomFieldID = CInt(Combo_CustomField.SelectedValue)
			End If

			If (Combo_SelectGroupID.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_SelectGroupID.SelectedValue)) Then
				GroupID = CInt(Combo_SelectGroupID.SelectedValue)
			End If

			GetCustomFieldStatus(GroupID, CustomFieldID)
			Exit Sub
		End If

		If (RequestPaint = False) Then
			Exit Sub
		End If

		SyncLock CustomStatus_GettingData_LockObject
			If (CustomStatus_GettingData) OrElse (CustomStatus_DataReady = False) Then
				Exit Sub
			End If
		End SyncLock

		Try
			If (In_PaintTimer_Tick) Then
				Exit Sub
			End If
		Catch ex As Exception
		End Try

		Try
			In_PaintTimer_Tick = True

			Form_ProgressBar.Visible = False
			StatusLabel_DataMaintenance.Text = ""

			Grid_CustomFieldStatus.Rows.Count = 1

			PaintGrid()

		Catch ex As Exception
		Finally
			In_PaintTimer_Tick = False
		End Try

	End Sub

#End Region

	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		' *****************************************************************************************
		' Routine to handle changes / updates to tables by this and other windows.
		' If this, or any other, form posts a change to a table, then it will invoke an update event 
		' detailing what tables have been altered.
		' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
		' the 'VeniceAutoUpdate' event of the main Venice form.
		' Each form may them react as appropriate to changes in any table that might impact it.
		'
		' *****************************************************************************************

		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		Try

			InPaint = True
			KnowledgeDateChanged = False
			SetButtonStatus_Flag = False

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
				KnowledgeDateChanged = True
				RefreshForm = True
			End If

			' ****************************************************************
			' Check for changes relevant to this form
			' ****************************************************************


			' Changes to the KnowledgeDate :-
			If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
				SetButtonStatus_Flag = True
			End If

			' Changes to the tblUserPermissions table :-
			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

				' Check ongoing permissions.

				Call CheckPermissions()
				If (HasReadPermission = False) Then
					Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

					FormIsValid = False
					Me.Close()
					Exit Sub
				End If

				SetButtonStatus_Flag = True

			End If

			' tblPertracCustomFields

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPertracCustomFields) = True) Or KnowledgeDateChanged Then

			End If

			' Information

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Information) = True) Then

			End If

			' tblPertracCustomFieldData

			If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPertracCustomFieldData) = True) Or KnowledgeDateChanged Then

			End If

		Catch ex As Exception
		Finally
			InPaint = OrgInPaint
		End Try

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
		If (RefreshForm) OrElse (SetButtonStatus_Flag) Then
			If (FormChanged = False) Then
				PaintGrid()
			End If

			Call SetButtonStatus()
		End If

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "


	' Check User permissions
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

#End Region

#Region " Form Control Events"


#End Region

#Region " Context Menu Code."

	Sub cms_Opening(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************
		Dim CustomFieldID As Integer = 0
		Dim PertracID As Integer = 0
		Dim GroupID As Integer = 0
		Dim ThisGrid As C1.Win.C1FlexGrid.C1FlexGrid
		Dim ThisMenuStrip As ContextMenuStrip

		If (TypeOf (sender) Is ContextMenuStrip) Then

			ThisMenuStrip = CType(sender, ContextMenuStrip)
			ThisGrid = CType(ThisMenuStrip.SourceControl, C1FlexGrid)

		ElseIf (TypeOf (sender) Is C1FlexGrid) Then

			ThisGrid = CType(sender, C1FlexGrid)
			ThisMenuStrip = ThisGrid.ContextMenuStrip

		Else

			Exit Sub

		End If

		If (ThisMenuStrip Is Nothing) Then
			Exit Sub
		End If

		Try
			If (Combo_SelectGroupID.SelectedValue IsNot Nothing) AndAlso (IsNumeric(Combo_SelectGroupID.SelectedValue)) Then
				GroupID = CInt(Combo_SelectGroupID.SelectedValue)
			End If
		Catch ex As Exception
		End Try

		Try
			' Acquire references to the owning control and item.

			Dim Grid_CustomFieldID_Ordinal As Integer = ThisGrid.Cols.IndexOf("CustomFieldID")
			Dim Grid_PertracID_Ordinal As Integer = ThisGrid.Cols.IndexOf("PertracID")
			Dim ThisGridRow As C1.Win.C1FlexGrid.Row
			Dim thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow

			' Clear the ContextMenuStrip control's 
			' Items collection.
			ThisMenuStrip.Items.Clear()

			Dim Col_Name As String = ""
			Dim Col_Is_Custom As Boolean = False

			GridClickMouseRow = ThisGrid.MouseRow
			GridClickMouseCol = ThisGrid.MouseCol

			ThisGrid.Select(GridClickMouseRow, GridClickMouseCol)
			ThisGrid.Focus()

			ThisGridRow = ThisGrid.Rows(GridClickMouseRow)
			CustomFieldID = ThisGridRow.Item(Grid_CustomFieldID_Ordinal)
			PertracID = ThisGridRow.Item(Grid_PertracID_Ordinal)

			GridClickCustomFieldID = ThisGridRow.Item(Grid_CustomFieldID_Ordinal)
			GridClickPertracID = ThisGridRow.Item(Grid_PertracID_Ordinal)

			If (CustomFieldID > 0) Then
				thisCustomFieldDefinition = LookupTableRow(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, CustomFieldID, "")

				If (thisCustomFieldDefinition IsNot Nothing) Then
					If (thisCustomFieldDefinition.FieldIsCalculated = False) Then
						ThisMenuStrip.Items.Add("This Custom field is not a calculated field. ", Nothing, Nothing)

					Else
						If (PertracID > 0) Then
							ThisMenuStrip.Items.Add("Update THIS Single Data Item.", Nothing, AddressOf Menu_RecalcSinglePertracID_SingleField)
							ThisMenuStrip.Items.Add("Update THIS Item for ALL Fields.", Nothing, AddressOf Menu_RecalcSinglePertracID_ALLFields)
							ThisMenuStrip.Items.Add(New ToolStripSeparator)
						End If

						ThisMenuStrip.Items.Add("Update Out-Of-Date Data Items for this Custom Field.", Nothing, AddressOf Menu_RecalcALLPertracID_SingleField)
						ThisMenuStrip.Items.Add("Update ALL Data Items for this Custom Field.", Nothing, AddressOf Menu_RecalcALLPertracID_SingleField_ForcedUpdate)
						ThisMenuStrip.Items.Add(New ToolStripSeparator)
						ThisMenuStrip.Items.Add("Update Out-Of-Date Data Items for ALL Custom Fields.", Nothing, AddressOf Menu_RecalcALLPertracID_ALLFields)
						ThisMenuStrip.Items.Add("Update ALL Data Items for ALL Custom Fields.", Nothing, AddressOf Menu_RecalcALLPertracID_ALLFields_ForcedUpdate)
					End If

					e.Cancel = False
				End If
			End If

		Catch ex As Exception
		End Try

	End Sub

#End Region

#Region " Custom Field Update Code."

	Private Sub Menu_RecalcSinglePertracID_SingleField(ByVal sender As Object, ByVal e As System.EventArgs)
		StartCalculateCustomFields(GridClickCustomFieldID, GridClickPertracID, True)
	End Sub

	Private Sub Menu_RecalcSinglePertracID_ALLFields(ByVal sender As Object, ByVal e As System.EventArgs)
		StartCalculateCustomFields(0, GridClickPertracID, True)
	End Sub

	Private Sub Menu_RecalcALLPertracID_SingleField(ByVal sender As Object, ByVal e As System.EventArgs)
		StartCalculateCustomFields(GridClickCustomFieldID, 0, False)
	End Sub

	Private Sub Menu_RecalcALLPertracID_SingleField_ForcedUpdate(ByVal sender As Object, ByVal e As System.EventArgs)
		StartCalculateCustomFields(GridClickCustomFieldID, 0, True)
	End Sub

	Private Sub Menu_RecalcALLPertracID_ALLFields(ByVal sender As Object, ByVal e As System.EventArgs)
		StartCalculateCustomFields(0, 0, False)
	End Sub

	Private Sub Menu_RecalcALLPertracID_ALLFields_ForcedUpdate(ByVal sender As Object, ByVal e As System.EventArgs)
		StartCalculateCustomFields(0, 0, True)
	End Sub

	Private Sub StartCalculateCustomFields(ByVal CustomFieldID As Integer, ByVal PertracID As Integer, ByVal ForceRecalc As Boolean)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			SyncLock CustomFieldCalc_StartLock
				If (CustomCalculationWorker IsNot Nothing) AndAlso (CustomCalculationWorker.IsBusy) Then
					Exit Sub
				End If

				If (CustomCalculationWorker Is Nothing) Then
					CustomCalculationWorker = New System.ComponentModel.BackgroundWorker
					CustomCalculationWorker.WorkerSupportsCancellation = True
					CustomCalculationWorker.WorkerReportsProgress = True

					AddHandler CustomCalculationWorker.DoWork, AddressOf CustomCalculationWorker_DoWork
					AddHandler CustomCalculationWorker.RunWorkerCompleted, AddressOf CustomCalculationWorkerCompleted
					AddHandler CustomCalculationWorker.ProgressChanged, AddressOf CustomCalculationWorkerProgressChanged
				End If

				CustomFieldCalc_CustomFieldID = CustomFieldID
				CustomFieldCalc_PertracID = PertracID
				CustomFieldCalc_ForceRecalc = ForceRecalc
				CustomFieldCalc_CancelFlag = False

				CustomCalculationTimer.Interval = 500
				CustomCalculationTimer.Tag = Me.ProgressBar_Calculation
				CustomCalculationTimer.Start()

				StatusLabel_DataMaintenance.Text = "Calculating Custom Fields."

				' Disable Controls

				Me.Combo_CustomField.Enabled = False
				Me.Combo_SelectGroupID.Enabled = False
				Me.Grid_CustomFieldStatus.Enabled = False
				Me.Btn_CancelCalculation.Enabled = True
				Me.Btn_CancelCalculation.Visible = True
				Me.ProgressBar_Calculation.Visible = True

				Me.Cursor = Cursors.WaitCursor
				Application.DoEvents()

				' Change Celect Combos, if necessary
				If (Me.Combo_SelectGroupID.Items.Count <= 0) OrElse (Combo_SelectGroupID.SelectedIndex <= 0) Then
					If (Me.Combo_CustomField.Items.Count > 0) AndAlso (Me.Combo_CustomField.SelectedIndex > 0) Then

						InPaint = True
						Me.Combo_CustomField.SelectedIndex = 0
						Application.DoEvents()
					End If
				End If

				CustomCalculationWorker.RunWorkerAsync()

			End SyncLock

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error starting the Field calculation worker.", ex.StackTrace, True)

			Me.Combo_CustomField.Enabled = True
			Me.Combo_SelectGroupID.Enabled = True
			Me.Grid_CustomFieldStatus.Enabled = True
			Me.Btn_CancelCalculation.Enabled = False
			Me.Btn_CancelCalculation.Visible = False
			Me.ProgressBar_Calculation.Visible = False

			StatusLabel_DataMaintenance.Text = ""
			Me.Cursor = Cursors.Default
		Finally
			InPaint = False
		End Try

	End Sub

	Private Sub CustomCalculationWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Dim ThisWorker As System.ComponentModel.BackgroundWorker
		Dim RowCount As Integer
		Dim ResultSet As RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataDataTable

		If (FieldCalculationObject Is Nothing) Then
			Exit Sub
		End If

		Try
			ThisWorker = CustomCalculationWorker
			RowCount = 0

			SyncLock FieldCalculationObject

				Try
					FieldCalculationObject.Initialise_CalculateFields(CustomFieldCalc_CustomFieldID, 0, CustomFieldCalc_PertracID, CustomFieldCalc_ForceRecalc)

					While (FieldCalculationObject.MoreFieldsToProcess) AndAlso (ThisWorker.CancellationPending = False)
            ResultSet = FieldCalculationObject.Process_CalculateFields(DefaultStatsDatePeriod, CustomFieldCalculationClass.DEFAULT_CALCULATION_BITE)

						If (Me.IsDisposed) OrElse (Me.Disposing) Then
							Exit While
						Else
							If (ResultSet IsNot Nothing) AndAlso (ResultSet.Rows.Count > 0) Then
								RowCount += ResultSet.Rows.Count

								MainForm.AdaptorUpdate(Me.Name, RenaissanceStandardDatasets.tblPertracCustomFieldData, ResultSet)

							End If

							ThisWorker.ReportProgress(0, RowCount)

						End If
					End While

				Catch ex As Exception
					If (Me.IsDisposed = False) AndAlso (Me.Disposing = False) Then
						MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in CustomCalculationWorker_DoWork()", ex.StackTrace, True)
					End If
				Finally
					If (FieldCalculationObject IsNot Nothing) Then
						FieldCalculationObject.Finish_CalculateFields()
					End If
				End Try

			End SyncLock

		Catch ex As Exception
			If (Me.IsDisposed = False) AndAlso (Me.Disposing = False) Then
				MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in CustomCalculationWorker_DoWork()", ex.StackTrace, True)
			End If
		End Try

	End Sub

	Private Sub CustomCalculationWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			If (CustomCalculationTimer IsNot Nothing) Then
				CustomCalculationTimer.Stop()
			End If
		Catch ex As Exception
		End Try

		Try
			Me.Cursor = Cursors.Default
		Catch ex As Exception
		End Try

		Try
			Me.Combo_CustomField.Enabled = True
			Me.Combo_SelectGroupID.Enabled = True
			Me.Grid_CustomFieldStatus.Enabled = True
			Me.Btn_CancelCalculation.Enabled = False
			Me.Btn_CancelCalculation.Visible = False
			Me.ProgressBar_Calculation.Visible = False
		Catch ex As Exception
		End Try

		Try
			StatusLabel_DataMaintenance.Text = "Updating Status Grid and dependant forms."
			Application.DoEvents()
		Catch ex As Exception
		End Try

		Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblPertracCustomFieldData))

		CustomCalculationWorker = Nothing
		RequestGetData = True
		RequestGetDataTime = Now()

		Try
			StatusLabel_DataMaintenance.Text = ""
		Catch ex As Exception
		End Try

	End Sub

	Private Sub CustomCalculationWorkerProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs)
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************
		Static EventCounter As Integer = 0

		StatusLabel_DataMaintenance.Text = "Custom Fields calculated : " & CInt(e.UserState).ToString

		If (EventCounter > CInt(e.UserState)) Then
			EventCounter = 0
		End If

		' Interim Update Status Grid.
		If (Menu_Options_UpdateStatusGrid.Checked) Then
			If ((Now() - RequestGetDataTime).TotalSeconds > GENOA_UpdateStatusGrid_Period) Then
				RequestGetDataTime = Now()
				RequestGetData = True
			End If
		End If

		' Post interim Update Event.
		If (Menu_Options_PostUpdateEvents.Checked) Then
			If ((CInt(e.UserState) - EventCounter) >= GENOA_CalculateCustomField_EventTrigger) Then
				EventCounter = CInt(e.UserState)
				Call MainForm.Main_RaiseEvent(New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.tblPertracCustomFieldData))
			End If
		End If
	End Sub

#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		If (FormChanged) Then

		Else

		End If

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

	End Sub

	Private Sub PaintGrid()
		' *************************************************************
		'
		'
		'
		' *************************************************************

		Dim ItemStatus As String = ""

		RequestPaint = False

		Me.StatusLabel_DataMaintenance.Text = ""

		' Paint Grid

		Try
			InPaint = True

			Dim ColumnCount As Integer
			Dim RowCount As Integer

			Dim Grid_FirstCol_Ordinal As Integer = Grid_CustomFieldStatus.Cols.IndexOf("FirstCol")
			Dim Grid_CustomFieldID_Ordinal As Integer = Grid_CustomFieldStatus.Cols.IndexOf("CustomFieldID")
			Dim Grid_PertracID_Ordinal As Integer = Grid_CustomFieldStatus.Cols.IndexOf("PertracID")
			Dim Grid_GroupID_Ordinal As Integer = Grid_CustomFieldStatus.Cols.IndexOf("GroupID")
			Dim Grid_UpdateRequired_Ordinal As Integer = Grid_CustomFieldStatus.Cols.IndexOf("UpdateRequired")
			Dim Grid_ItemCount_Ordinal As Integer = Grid_CustomFieldStatus.Cols.IndexOf("ItemCount")
			Dim Grid_UpdateCount_Ordinal As Integer = Grid_CustomFieldStatus.Cols.IndexOf("UpdateCount")

			Dim ThisGridRow As C1.Win.C1FlexGrid.Row
			Dim ThisDataRow As DataRow

			Grid_CustomFieldStatus.Rows.Count = 1

			For ColumnCount = 1 To (Grid_CustomFieldStatus.Cols.Count - 1)
				Grid_CustomFieldStatus.Cols(ColumnCount).Visible = False
			Next

			If (tblCustomFieldStatusSummary IsNot Nothing) AndAlso (tblCustomFieldStatusSummary.Rows.Count > 0) Then
				Dim DS_CustomFieldID_Ordinal As Integer = tblCustomFieldStatusSummary.Columns.IndexOf("CustomFieldID")
				Dim DS_ItemCount_Ordinal As Integer = tblCustomFieldStatusSummary.Columns.IndexOf("ItemCount")
				Dim DS_UpdateCount_Ordinal As Integer = tblCustomFieldStatusSummary.Columns.IndexOf("UpdateCount")

				Grid_CustomFieldStatus.Cols(Grid_ItemCount_Ordinal).Visible = True
				Grid_CustomFieldStatus.Cols(Grid_UpdateCount_Ordinal).Visible = True

				SyncLock tblCustomFieldStatusSummary
					For RowCount = 0 To (tblCustomFieldStatusSummary.Rows.Count - 1)
						ThisDataRow = tblCustomFieldStatusSummary.Rows(RowCount)

						ThisGridRow = Grid_CustomFieldStatus.Rows.Add()
						ThisGridRow.Item(Grid_FirstCol_Ordinal) = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, CInt(ThisDataRow(DS_CustomFieldID_Ordinal)), "FieldName")
						ThisGridRow.IsNode = False

						ThisGridRow.Item(Grid_CustomFieldID_Ordinal) = CInt(ThisDataRow(DS_CustomFieldID_Ordinal))
						ThisGridRow.Item(Grid_PertracID_Ordinal) = 0
						ThisGridRow.Item(Grid_GroupID_Ordinal) = 0
						ThisGridRow.Item(Grid_UpdateRequired_Ordinal) = False
						ThisGridRow.Item(Grid_ItemCount_Ordinal) = ThisDataRow(DS_ItemCount_Ordinal)
						ThisGridRow.Item(Grid_UpdateCount_Ordinal) = ThisDataRow(DS_UpdateCount_Ordinal)
					Next
				End SyncLock

			ElseIf (tblCustomFieldStatus IsNot Nothing) AndAlso (tblCustomFieldStatus.Rows.Count > 0) Then

				Dim DS_FieldDataID_Ordinal As Integer = tblCustomFieldStatus.Columns.IndexOf("FieldDataID")
				Dim DS_CustomFieldID_Ordinal As Integer = tblCustomFieldStatus.Columns.IndexOf("CustomFieldID")
				Dim DS_PertracID_Ordinal As Integer = tblCustomFieldStatus.Columns.IndexOf("PertracID")
				Dim DS_GroupID_Ordinal As Integer = tblCustomFieldStatus.Columns.IndexOf("GroupID")
				Dim DS_UpdateRequired_Ordinal As Integer = tblCustomFieldStatus.Columns.IndexOf("UpdateRequired")
				Dim DS_LatestReturnDate_Ordinal As Integer = tblCustomFieldStatus.Columns.IndexOf("LatestReturnDate")
				Dim DS_DateEntered_Ordinal As Integer = tblCustomFieldStatus.Columns.IndexOf("DateEntered")
				Dim LastCustomID As Integer = (-1)
				Dim ThisCustomID As Integer = (-1)
				Dim ThisPertracID As Integer = (-1)
				Dim SummaryGridRow As C1.Win.C1FlexGrid.Row = Nothing
				Dim ItemCount As Integer
				Dim UpdateCount As Integer

				Grid_CustomFieldStatus.Cols(Grid_ItemCount_Ordinal).Visible = True
				Grid_CustomFieldStatus.Cols(Grid_UpdateCount_Ordinal).Visible = True
				Grid_CustomFieldStatus.Cols(Grid_UpdateRequired_Ordinal).Visible = True

				SyncLock tblCustomFieldStatus
					For RowCount = 0 To (tblCustomFieldStatus.Rows.Count - 1)
						ThisDataRow = tblCustomFieldStatus.Rows(RowCount)

						ThisCustomID = CInt(ThisDataRow(DS_CustomFieldID_Ordinal))
						ThisPertracID = CInt(ThisDataRow(DS_PertracID_Ordinal))

						If (ThisCustomID <> LastCustomID) Then
							If (SummaryGridRow IsNot Nothing) Then
								SummaryGridRow.Item(Grid_ItemCount_Ordinal) = ItemCount
								SummaryGridRow.Item(Grid_UpdateCount_Ordinal) = UpdateCount
								If (UpdateCount > 0) Then
									SummaryGridRow.Item(Grid_UpdateRequired_Ordinal) = True
								Else
									SummaryGridRow.Item(Grid_UpdateRequired_Ordinal) = False
								End If
								SummaryGridRow.Node.Collapsed = True
							End If

							ThisGridRow = Grid_CustomFieldStatus.Rows.Add()
							SummaryGridRow = ThisGridRow

							ThisGridRow.Item(Grid_FirstCol_Ordinal) = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, ThisCustomID, "FieldName")
							ThisGridRow.IsNode = True
							ThisGridRow.Node.Level = 0

							ThisGridRow.Item(Grid_CustomFieldID_Ordinal) = ThisCustomID
							ThisGridRow.Item(Grid_PertracID_Ordinal) = 0
							ThisGridRow.Item(Grid_GroupID_Ordinal) = 0
							ThisGridRow.Item(Grid_UpdateRequired_Ordinal) = False
							ThisGridRow.Item(Grid_ItemCount_Ordinal) = 0
							ThisGridRow.Item(Grid_UpdateCount_Ordinal) = 0

							LastCustomID = ThisCustomID
							ItemCount = 0
							UpdateCount = 0

						End If

						ItemCount += 1

						If CBool(ThisDataRow(DS_UpdateRequired_Ordinal)) Then
							UpdateCount += 1
						End If

						ThisGridRow = Grid_CustomFieldStatus.Rows.Add()

						'ThisGridRow.Item(Grid_FirstCol_Ordinal) = LookupTableValue(MainForm, RenaissanceStandardDatasets.Mastername, 0, "MasterName", "ID", "ID=" & ThisPertracID.ToString)
						ThisGridRow.Item(Grid_FirstCol_Ordinal) = MainForm.MasternameDictionary(ThisPertracID.ToString)
						ThisGridRow.IsNode = False

						ThisGridRow.Item(Grid_CustomFieldID_Ordinal) = ThisCustomID
						ThisGridRow.Item(Grid_PertracID_Ordinal) = ThisPertracID
						ThisGridRow.Item(Grid_GroupID_Ordinal) = CInt(ThisDataRow(DS_GroupID_Ordinal))
						ThisGridRow.Item(Grid_UpdateRequired_Ordinal) = CBool(ThisDataRow(DS_UpdateRequired_Ordinal))
						ThisGridRow.Item(Grid_ItemCount_Ordinal) = Nothing
						ThisGridRow.Item(Grid_UpdateCount_Ordinal) = Nothing

					Next
				End SyncLock

				If (SummaryGridRow IsNot Nothing) Then
					SummaryGridRow.Item(Grid_ItemCount_Ordinal) = ItemCount
					SummaryGridRow.Item(Grid_UpdateCount_Ordinal) = UpdateCount
					If (UpdateCount > 0) Then
						SummaryGridRow.Item(Grid_UpdateRequired_Ordinal) = True
					Else
						SummaryGridRow.Item(Grid_UpdateRequired_Ordinal) = False
					End If
					SummaryGridRow.Node.Collapsed = True
				End If

			End If

		Catch ex As Exception
		Finally
			InPaint = False
		End Try

	End Sub

#End Region

#Region " Set Combos"

	Private Sub SetGroupCombo()
		' ***************************************************************************************************
		'
		'
		' ***************************************************************************************************

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_SelectGroupID, _
		RenaissanceStandardDatasets.tblGroupList, _
		"GroupListName", _
		"GroupListID", _
		"", False, True, True, 0, "All Groups")		' 

	End Sub

	Private Sub SetCustomFieldCombo()
		' ***************************************************************************************************
		'
		'
		' ***************************************************************************************************

		Call MainForm.SetTblGenericCombo( _
		Me.Combo_CustomField, _
		RenaissanceStandardDatasets.tblPertracCustomFields, _
		"FieldName", _
		"FieldID", _
		"", False, True, True, 0, "All Custom Fields")	 ' 

	End Sub

#End Region

#Region " Refresh Update List code "

	Private Sub GetCustomFieldStatus(ByVal GroupID As Integer, ByVal CustomFieldID As Integer, Optional ByVal PertracID As Integer = 0)
		' *****************************************************************************************
		' Code to refresh the Status Grid.
		'
		' The Field Status information is retrieved using a background worker thread : 'CustomStatusWorker'.
		' This method is used so that the Genoa application will continue to respons to user activity.
		'
		' *****************************************************************************************

		' Exit if update is already in progress.

		SyncLock CustomStatus_GettingData_LockObject
			If (CustomStatus_GettingData) OrElse ((CustomStatusWorker IsNot Nothing) AndAlso (CustomStatusWorker.IsBusy)) Then
				Exit Sub
			End If
			CustomStatus_GettingData = True
		End SyncLock
		CustomStatus_DataReady = False

		' Initialise CustomWorkerThread.

		Try
			If (CustomStatusWorker Is Nothing) Then
				CustomStatusWorker = New System.ComponentModel.BackgroundWorker
				CustomStatusWorker.WorkerSupportsCancellation = True
        CustomStatusWorker.WorkerReportsProgress = True

				AddHandler CustomStatusWorker.DoWork, AddressOf CustomStatusWorker_DoWork
				AddHandler CustomStatusWorker.RunWorkerCompleted, AddressOf CustomStatusWorkerCompleted
				AddHandler CustomStatusWorker.ProgressChanged, AddressOf CustomStatusWorkerProgressChanged
			End If

			CustomStatusWorker_GroupID = GroupID
			CustomStatusWorker_CustomFieldID = CustomFieldID
			CustomStatusWorker_PertracID = PertracID

			SQLFetchTimer.Interval = 500
			SQLFetchTimer.Tag = Me.Form_ProgressBar
			SQLFetchTimer.Start()

			StatusLabel_DataMaintenance.Text = "Loading Custom Field Status..."

      ' Disable Form Controls

			If (FormControls IsNot Nothing) AndAlso (FormControls.Count > 0) Then
				FormControls.Clear()
			End If
			' Don't hide all the form controls if it appears to already be hidden.
			If (Me.Combo_CustomField.Enabled) Then
				FormControls = MainForm.DisableFormControls(Me)
			End If

			' Clear Target Tables
			If (tblCustomFieldStatusSummary Is Nothing) Then
				tblCustomFieldStatusSummary = New DataTable
			Else
				SyncLock tblCustomFieldStatusSummary
					tblCustomFieldStatusSummary.Clear()
				End SyncLock
			End If

			If (tblCustomFieldStatus Is Nothing) Then
				tblCustomFieldStatus = New DataTable
			Else
				SyncLock tblCustomFieldStatus
					tblCustomFieldStatus.Clear()
				End SyncLock
			End If

			' Start worker thread.

			CustomStatusWorker.RunWorkerAsync()

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
			CustomStatus_GettingData = False
			MainForm.EnableFormControls(FormControls)
			StatusLabel_DataMaintenance.Text = ""
		End Try

	End Sub

	Private Sub CustomStatusWorker_DoWork(ByVal sender As Object, ByVal e As System.ComponentModel.DoWorkEventArgs)
		' *****************************************************************************************
		' Retrieve Field Status information.
		'
		' The retrieved data is contingent on what is selected. If ALL Fields and Instruments are to be
		' retrieved, then a summary is fetched, if a specific Group, Field or Instrument is to be
		' fetched, then all data items relating to the selection are retrieved.
		'
		' *****************************************************************************************

		Dim ThisWorker As System.ComponentModel.BackgroundWorker

		Try
			ThisWorker = CustomStatusWorker

			Dim JustASummary As Boolean = False

			Dim tmpCommand As New SqlCommand
			Dim result As IAsyncResult = Nothing
			Dim reader As SqlDataReader = Nothing

			' Just a Summary Query ?

			If (CustomStatusWorker_GroupID = 0) AndAlso (CustomStatusWorker_CustomFieldID = 0) AndAlso (CustomStatusWorker_PertracID = 0) Then
				JustASummary = True
			End If

			' Fetch Status :-

			Try
        Dim CustomFieldTable As DSPertracCustomFields.tblPertracCustomFieldsDataTable
        Dim ThisCustomField As DSPertracCustomFields.tblPertracCustomFieldsRow

        ' Build SQL Command to fetch status data.

        tmpCommand.CommandType = CommandType.StoredProcedure
        If (JustASummary) Then
          tmpCommand.CommandText = "spu_PertracCustomFieldData_StatusSummary"

        Else
          tmpCommand.CommandText = "spu_PertracCustomFieldData_Status"

        End If

        tmpCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = CustomStatusWorker_GroupID
        tmpCommand.Parameters.Add("@CustomFieldID", SqlDbType.Int).Value = CustomStatusWorker_CustomFieldID
        tmpCommand.Parameters.Add("@PertracID", SqlDbType.Int).Value = CustomStatusWorker_PertracID
        tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate

        tmpCommand.Connection = MainForm.GetGenoaAsynchConnection
        tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

        ' Initiate query as an Asynchronous query. This allows the Thread to be cancelled if required.

        If (JustASummary) Then
          CustomFieldTable = CType(MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblPertracCustomFields).Tables(0), DSPertracCustomFields.tblPertracCustomFieldsDataTable)

          For Each ThisCustomField In CustomFieldTable.Rows

            tmpCommand.Parameters("@CustomFieldID").Value = ThisCustomField.FieldID

            Try
              result = tmpCommand.BeginExecuteReader()
              While Not result.IsCompleted
                If (ThisWorker.CancellationPending) Then
                  tmpCommand.Cancel()
                  Exit While
                End If

                Threading.Thread.Sleep(100)
              End While
            Catch ex As Exception
            Finally
              If (result IsNot Nothing) Then
                reader = tmpCommand.EndExecuteReader(result)
              End If
              If (ThisWorker.CancellationPending) Then
                reader.Close()
                reader = Nothing
              End If
            End Try
            If (ThisWorker.CancellationPending) Then
              Exit For
            End If

            If (reader IsNot Nothing) Then
              tblCustomFieldStatusSummary.Load(reader)

              If (tblCustomFieldStatusSummary.Rows.Count > 0) Then
                ThisWorker.ReportProgress(ThisCustomField.FieldID, tblCustomFieldStatusSummary.Rows(tblCustomFieldStatusSummary.Rows.Count - 1))
              End If

            End If

          Next

        Else

          Try
            result = tmpCommand.BeginExecuteReader()
            While Not result.IsCompleted
              If (ThisWorker.CancellationPending) Then
                tmpCommand.Cancel()
                Exit While
              End If

              Threading.Thread.Sleep(100)
            End While
          Catch ex As Exception
          Finally
            If (result IsNot Nothing) Then
              reader = tmpCommand.EndExecuteReader(result)
            End If
            If (ThisWorker.CancellationPending) Then
              reader.Close()
              reader = Nothing
            End If
          End Try

          ' Fetch data if query was successful.

          If (reader IsNot Nothing) Then
            If (JustASummary) Then
              SyncLock tblCustomFieldStatusSummary
                tblCustomFieldStatusSummary.Load(reader)
              End SyncLock
            Else
              SyncLock tblCustomFieldStatus
                tblCustomFieldStatus.Load(reader)
              End SyncLock
            End If

            RequestPaint = True
          End If
        End If

      Catch ex As Exception
        If (Me.IsDisposed = False) AndAlso (Me.Disposing = False) Then
          If (ThisWorker.CancellationPending = False) Then
            MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in CustomStatusWorker_DoWork()", ex.StackTrace, True)
          End If
        End If

      Finally
        ' Tidy DB Connection.

        Try
          If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
            tmpCommand.Connection.Close()
            tmpCommand.Connection = Nothing
          End If
        Catch ex As Exception
        End Try
      End Try

		Catch ex As Exception
			If (Me.IsDisposed = False) AndAlso (Me.Disposing = False) Then
				MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "Error in CustomStatusWorker_DoWork()", ex.StackTrace, True)
			End If
		Finally
			ThisWorker = Nothing
		End Try

	End Sub

	Private Sub CustomStatusWorkerProgressChanged(ByVal sender As Object, ByVal e As System.ComponentModel.ProgressChangedEventArgs)
		' *****************************************************************************************
    ' 
		'
		' *****************************************************************************************
    Try

      If (Me.IsDisposed) OrElse (Me.Disposing) Then
        Exit Sub
      End If

      If (CustomStatusWorker_GroupID = 0) AndAlso (CustomStatusWorker_CustomFieldID = 0) AndAlso (CustomStatusWorker_PertracID = 0) Then
        ' JustASummary 

        If (e.ProgressPercentage > 0) AndAlso (e.UserState IsNot Nothing) Then

          Dim ThisGridRow As C1.Win.C1FlexGrid.Row
          Dim ThisDataRow As DataRow

          ThisDataRow = CType(e.UserState, DataRow)

          Dim Grid_FirstCol_Ordinal As Integer = Grid_CustomFieldStatus.Cols.IndexOf("FirstCol")
          Dim Grid_CustomFieldID_Ordinal As Integer = Grid_CustomFieldStatus.Cols.IndexOf("CustomFieldID")
          Dim Grid_PertracID_Ordinal As Integer = Grid_CustomFieldStatus.Cols.IndexOf("PertracID")
          Dim Grid_GroupID_Ordinal As Integer = Grid_CustomFieldStatus.Cols.IndexOf("GroupID")
          Dim Grid_UpdateRequired_Ordinal As Integer = Grid_CustomFieldStatus.Cols.IndexOf("UpdateRequired")
          Dim Grid_ItemCount_Ordinal As Integer = Grid_CustomFieldStatus.Cols.IndexOf("ItemCount")
          Dim Grid_UpdateCount_Ordinal As Integer = Grid_CustomFieldStatus.Cols.IndexOf("UpdateCount")

          ' Set Column Visibility

          For ColumnCount As Integer = 1 To (Grid_CustomFieldStatus.Cols.Count - 1)
            Grid_CustomFieldStatus.Cols(ColumnCount).Visible = False
          Next

          Grid_CustomFieldStatus.Cols(Grid_ItemCount_Ordinal).Visible = True
          Grid_CustomFieldStatus.Cols(Grid_UpdateCount_Ordinal).Visible = True

          ' Add New Row.

          Dim DS_CustomFieldID_Ordinal As Integer = ThisDataRow.Table.Columns.IndexOf("CustomFieldID")
          Dim DS_ItemCount_Ordinal As Integer = ThisDataRow.Table.Columns.IndexOf("ItemCount")
          Dim DS_UpdateCount_Ordinal As Integer = ThisDataRow.Table.Columns.IndexOf("UpdateCount")

          ThisGridRow = Grid_CustomFieldStatus.Rows.Add()
          ThisGridRow.Item(Grid_FirstCol_Ordinal) = LookupTableValue(MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, CInt(ThisDataRow(DS_CustomFieldID_Ordinal)), "FieldName")
          ThisGridRow.IsNode = False

          ThisGridRow.Item(Grid_CustomFieldID_Ordinal) = CInt(ThisDataRow(DS_CustomFieldID_Ordinal))
          ThisGridRow.Item(Grid_PertracID_Ordinal) = 0
          ThisGridRow.Item(Grid_GroupID_Ordinal) = 0
          ThisGridRow.Item(Grid_UpdateRequired_Ordinal) = False
          ThisGridRow.Item(Grid_ItemCount_Ordinal) = ThisDataRow(DS_ItemCount_Ordinal)
          ThisGridRow.Item(Grid_UpdateCount_Ordinal) = ThisDataRow(DS_UpdateCount_Ordinal)

        End If

      End If

    Catch ex As Exception
    End Try

	End Sub

	Private Sub CustomStatusWorkerCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.RunWorkerCompletedEventArgs)
		' *****************************************************************************************
		' On completion, set the status flags to allow the Grid to be re-painted by the Paint timer. 
		'
		' *****************************************************************************************

		If (Me.IsDisposed) OrElse (Me.Disposing) Then
			Exit Sub
		End If

		Try
			SQLFetchTimer.Stop()
		Catch ex As Exception
		End Try

		If (Me.IsDisposed) Then
			CustomStatusWorker = Nothing
			Exit Sub
		End If

		Try
			StatusLabel_DataMaintenance.Text = ""
      Form_ProgressBar.Visible = False

			If (FormControls IsNot Nothing) AndAlso (FormControls.Count > 0) Then
				MainForm.EnableFormControls(FormControls)
				FormControls.Clear()
			End If

		Catch ex As Exception
		End Try

		If (Me.Visible = False) Then
			Exit Sub
		End If

		Try
			CustomStatus_DataReady = True

		Catch ex As Exception
			MainForm.LogError(Me.Name, LOG_LEVELS.Error, ex.Message, "", ex.StackTrace, True)
		Finally
			SyncLock CustomStatus_GettingData_LockObject
				CustomStatus_GettingData = False
			End SyncLock
		End Try
	End Sub

#End Region

	Private Sub Combo_CustomField_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_CustomField.SelectedIndexChanged
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (InPaint = False) Then

			RequestGetData = True
			RequestGetDataTime = Now()

		End If

	End Sub

	Private Sub Combo_SelectGroupID_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectGroupID.SelectedIndexChanged
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		If (InPaint = False) Then

			RequestGetData = True
			RequestGetDataTime = Now()

		End If

	End Sub

	Private Sub Btn_CancelCalculation_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btn_CancelCalculation.Click
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		Try
			CustomFieldCalc_CancelFlag = True

			If (FieldCalculationObject IsNot Nothing) Then
				FieldCalculationObject.CancelOperation()
			End If

			If (CustomCalculationWorker IsNot Nothing) Then
				If (CustomCalculationWorker.IsBusy) Then
					CustomCalculationWorker.CancelAsync()
				End If
			End If
		Catch ex As Exception
		End Try

	End Sub



End Class
