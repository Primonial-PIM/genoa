Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass
Imports RenaissanceUtilities.DatePeriodFunctions

Public Class frmPerformance

	Inherits System.Windows.Forms.Form
	Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	Friend WithEvents editAuditID As System.Windows.Forms.TextBox
	Friend WithEvents btnNavFirst As System.Windows.Forms.Button
	Friend WithEvents btnNavPrev As System.Windows.Forms.Button
	Friend WithEvents btnNavNext As System.Windows.Forms.Button
	Friend WithEvents btnLast As System.Windows.Forms.Button
	Friend WithEvents btnCancel As System.Windows.Forms.Button
	Friend WithEvents btnSave As System.Windows.Forms.Button
  Friend WithEvents Combo_SelectMasterName As FCP_TelerikControls.FCP_RadComboBox
  Friend WithEvents Panel1 As System.Windows.Forms.Panel
	Friend WithEvents Label1 As System.Windows.Forms.Label
	Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
	Friend WithEvents RootMenu As System.Windows.Forms.MenuStrip
	Friend WithEvents Panel_InformationEdit As System.Windows.Forms.Panel
	Friend WithEvents Grid_Prices As System.Windows.Forms.DataGridView
	Friend WithEvents editDataVendor As System.Windows.Forms.TextBox
  Friend WithEvents Button_RecalcNAVs As System.Windows.Forms.Button
  Friend WithEvents Check_ShowRecent As System.Windows.Forms.CheckBox
	Friend WithEvents btnClose As System.Windows.Forms.Button
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
    Me.editAuditID = New System.Windows.Forms.TextBox
    Me.btnNavFirst = New System.Windows.Forms.Button
    Me.btnNavPrev = New System.Windows.Forms.Button
    Me.btnNavNext = New System.Windows.Forms.Button
    Me.btnLast = New System.Windows.Forms.Button
    Me.btnCancel = New System.Windows.Forms.Button
    Me.btnSave = New System.Windows.Forms.Button
    Me.Combo_SelectMasterName = New FCP_TelerikControls.FCP_RadComboBox
    Me.Panel1 = New System.Windows.Forms.Panel
    Me.Label1 = New System.Windows.Forms.Label
    Me.GroupBox1 = New System.Windows.Forms.GroupBox
    Me.btnClose = New System.Windows.Forms.Button
    Me.RootMenu = New System.Windows.Forms.MenuStrip
    Me.Panel_InformationEdit = New System.Windows.Forms.Panel
    Me.Button_RecalcNAVs = New System.Windows.Forms.Button
    Me.Grid_Prices = New System.Windows.Forms.DataGridView
    Me.editDataVendor = New System.Windows.Forms.TextBox
    Me.Check_ShowRecent = New System.Windows.Forms.CheckBox
    CType(Me.Combo_SelectMasterName, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.Panel1.SuspendLayout()
    Me.Panel_InformationEdit.SuspendLayout()
    CType(Me.Grid_Prices, System.ComponentModel.ISupportInitialize).BeginInit()
    Me.SuspendLayout()
    '
    'editAuditID
    '
    Me.editAuditID.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.editAuditID.Enabled = False
    Me.editAuditID.Location = New System.Drawing.Point(941, 33)
    Me.editAuditID.Name = "editAuditID"
    Me.editAuditID.Size = New System.Drawing.Size(50, 20)
    Me.editAuditID.TabIndex = 2
    '
    'btnNavFirst
    '
    Me.btnNavFirst.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavFirst.Location = New System.Drawing.Point(8, 8)
    Me.btnNavFirst.Name = "btnNavFirst"
    Me.btnNavFirst.Size = New System.Drawing.Size(40, 28)
    Me.btnNavFirst.TabIndex = 0
    Me.btnNavFirst.Text = "<<"
    '
    'btnNavPrev
    '
    Me.btnNavPrev.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavPrev.Location = New System.Drawing.Point(50, 8)
    Me.btnNavPrev.Name = "btnNavPrev"
    Me.btnNavPrev.Size = New System.Drawing.Size(35, 28)
    Me.btnNavPrev.TabIndex = 1
    Me.btnNavPrev.Text = "<"
    '
    'btnNavNext
    '
    Me.btnNavNext.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnNavNext.Location = New System.Drawing.Point(88, 8)
    Me.btnNavNext.Name = "btnNavNext"
    Me.btnNavNext.Size = New System.Drawing.Size(35, 28)
    Me.btnNavNext.TabIndex = 2
    Me.btnNavNext.Text = ">"
    '
    'btnLast
    '
    Me.btnLast.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnLast.Location = New System.Drawing.Point(124, 8)
    Me.btnLast.Name = "btnLast"
    Me.btnLast.Size = New System.Drawing.Size(40, 28)
    Me.btnLast.TabIndex = 3
    Me.btnLast.Text = ">>"
    '
    'btnCancel
    '
    Me.btnCancel.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
    Me.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnCancel.Location = New System.Drawing.Point(434, 632)
    Me.btnCancel.Name = "btnCancel"
    Me.btnCancel.Size = New System.Drawing.Size(75, 28)
    Me.btnCancel.TabIndex = 6
    Me.btnCancel.Text = "&Cancel"
    '
    'btnSave
    '
    Me.btnSave.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnSave.Location = New System.Drawing.Point(349, 632)
    Me.btnSave.Name = "btnSave"
    Me.btnSave.Size = New System.Drawing.Size(75, 28)
    Me.btnSave.TabIndex = 5
    Me.btnSave.Text = "&Save"
    '
    'Combo_SelectMasterName
    '
    Me.Combo_SelectMasterName.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Combo_SelectMasterName.FormattingEnabled = True
    Me.Combo_SelectMasterName.Location = New System.Drawing.Point(121, 33)
    Me.Combo_SelectMasterName.MasternameCollection = Nothing
    Me.Combo_SelectMasterName.Name = "Combo_SelectMasterName"
    '
    '
    '
    Me.Combo_SelectMasterName.RootElement.AutoSizeMode = Telerik.WinControls.RadAutoSizeMode.WrapAroundChildren
    Me.Combo_SelectMasterName.SelectNoMatch = False
    Me.Combo_SelectMasterName.Size = New System.Drawing.Size(669, 20)
    Me.Combo_SelectMasterName.TabIndex = 0
    Me.Combo_SelectMasterName.ThemeName = "ControlDefault"
    '
    'Panel1
    '
    Me.Panel1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
    Me.Panel1.Controls.Add(Me.btnNavFirst)
    Me.Panel1.Controls.Add(Me.btnNavPrev)
    Me.Panel1.Controls.Add(Me.btnNavNext)
    Me.Panel1.Controls.Add(Me.btnLast)
    Me.Panel1.Location = New System.Drawing.Point(131, 623)
    Me.Panel1.Name = "Panel1"
    Me.Panel1.Size = New System.Drawing.Size(176, 48)
    Me.Panel1.TabIndex = 4
    '
    'Label1
    '
    Me.Label1.Location = New System.Drawing.Point(6, 33)
    Me.Label1.Name = "Label1"
    Me.Label1.Size = New System.Drawing.Size(100, 23)
    Me.Label1.TabIndex = 18
    Me.Label1.Text = "Select"
    '
    'GroupBox1
    '
    Me.GroupBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.GroupBox1.Location = New System.Drawing.Point(5, 59)
    Me.GroupBox1.Name = "GroupBox1"
    Me.GroupBox1.Size = New System.Drawing.Size(988, 10)
    Me.GroupBox1.TabIndex = 77
    Me.GroupBox1.TabStop = False
    '
    'btnClose
    '
    Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
    Me.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.btnClose.Location = New System.Drawing.Point(518, 632)
    Me.btnClose.Name = "btnClose"
    Me.btnClose.Size = New System.Drawing.Size(75, 28)
    Me.btnClose.TabIndex = 7
    Me.btnClose.Text = "&Close"
    '
    'RootMenu
    '
    Me.RootMenu.AllowMerge = False
    Me.RootMenu.Location = New System.Drawing.Point(0, 0)
    Me.RootMenu.Name = "RootMenu"
    Me.RootMenu.Size = New System.Drawing.Size(1004, 24)
    Me.RootMenu.TabIndex = 8
    Me.RootMenu.Text = "MenuStrip1"
    '
    'Panel_InformationEdit
    '
    Me.Panel_InformationEdit.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Panel_InformationEdit.AutoScroll = True
    Me.Panel_InformationEdit.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
    Me.Panel_InformationEdit.Controls.Add(Me.Button_RecalcNAVs)
    Me.Panel_InformationEdit.Controls.Add(Me.Grid_Prices)
    Me.Panel_InformationEdit.Location = New System.Drawing.Point(9, 93)
    Me.Panel_InformationEdit.Name = "Panel_InformationEdit"
    Me.Panel_InformationEdit.Size = New System.Drawing.Size(988, 514)
    Me.Panel_InformationEdit.TabIndex = 3
    '
    'Button_RecalcNAVs
    '
    Me.Button_RecalcNAVs.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Button_RecalcNAVs.FlatStyle = System.Windows.Forms.FlatStyle.System
    Me.Button_RecalcNAVs.Location = New System.Drawing.Point(827, 3)
    Me.Button_RecalcNAVs.Name = "Button_RecalcNAVs"
    Me.Button_RecalcNAVs.Size = New System.Drawing.Size(153, 28)
    Me.Button_RecalcNAVs.TabIndex = 1
    Me.Button_RecalcNAVs.Text = "Re-Calc NAVs"
    '
    'Grid_Prices
    '
    Me.Grid_Prices.AllowDrop = True
    Me.Grid_Prices.AllowUserToOrderColumns = True
    Me.Grid_Prices.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                Or System.Windows.Forms.AnchorStyles.Left) _
                Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.Grid_Prices.Location = New System.Drawing.Point(3, 3)
    Me.Grid_Prices.Name = "Grid_Prices"
    Me.Grid_Prices.Size = New System.Drawing.Size(818, 504)
    Me.Grid_Prices.TabIndex = 0
    '
    'editDataVendor
    '
    Me.editDataVendor.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
    Me.editDataVendor.Enabled = False
    Me.editDataVendor.Location = New System.Drawing.Point(796, 33)
    Me.editDataVendor.Name = "editDataVendor"
    Me.editDataVendor.Size = New System.Drawing.Size(139, 20)
    Me.editDataVendor.TabIndex = 1
    '
    'Check_ShowRecent
    '
    Me.Check_ShowRecent.AutoSize = True
    Me.Check_ShowRecent.Location = New System.Drawing.Point(9, 70)
    Me.Check_ShowRecent.Name = "Check_ShowRecent"
    Me.Check_ShowRecent.Size = New System.Drawing.Size(336, 17)
    Me.Check_ShowRecent.TabIndex = 78
    Me.Check_ShowRecent.Text = "Display recent prices only. (May make it easier to add new prices)."
    Me.Check_ShowRecent.UseVisualStyleBackColor = True
    '
    'frmPerformance
    '
    Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
    Me.CancelButton = Me.btnCancel
    Me.ClientSize = New System.Drawing.Size(1004, 683)
    Me.Controls.Add(Me.Check_ShowRecent)
    Me.Controls.Add(Me.editDataVendor)
    Me.Controls.Add(Me.Panel_InformationEdit)
    Me.Controls.Add(Me.btnClose)
    Me.Controls.Add(Me.GroupBox1)
    Me.Controls.Add(Me.Label1)
    Me.Controls.Add(Me.Panel1)
    Me.Controls.Add(Me.Combo_SelectMasterName)
    Me.Controls.Add(Me.btnSave)
    Me.Controls.Add(Me.editAuditID)
    Me.Controls.Add(Me.btnCancel)
    Me.Controls.Add(Me.RootMenu)
    Me.MainMenuStrip = Me.RootMenu
    Me.MinimumSize = New System.Drawing.Size(800, 500)
    Me.Name = "frmPerformance"
    Me.Text = "Add/Edit Instrument Returns"
    CType(Me.Combo_SelectMasterName, System.ComponentModel.ISupportInitialize).EndInit()
    Me.Panel1.ResumeLayout(False)
    Me.Panel_InformationEdit.ResumeLayout(False)
    CType(Me.Grid_Prices, System.ComponentModel.ISupportInitialize).EndInit()
    Me.ResumeLayout(False)
    Me.PerformLayout()

  End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
	Private WithEvents _MainForm As GenoaMain

	' Form ToolTip
	Private FormTooltip As New ToolTip()

	' Form Constants, specific to the table being updated.

	Private ALWAYS_CLOSE_THIS_FORM As Boolean = False

	' Form Locals, initialised on 'New' defining what standard data items to use
	Private THIS_TABLENAME As String
	Private THIS_ADAPTORNAME As String
	Private THIS_DATASETNAME As String

	Private GenericUpdateObject As RenaissanceTimerUpdateClass

	' The standard ChangeID for this form. e.g. tblGroupList
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

  Private THIS_FORM_SelectingCombo As FCP_TelerikControls.FCP_RadComboBox
  Private THIS_FORM_NewMoveToControl As Control

	' Form Specific Order fields
	Private THIS_FORM_SelectBy As String
	Private THIS_FORM_OrderBy As String

	Private THIS_FORM_ValueMember As String

	' Form specific Permissioning variables
	Private THIS_FORM_PermissionArea As String
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
	Private THIS_FORM_FormID As GenoaFormID

	' Data Structures

	Private PerformanceDataset As RenaissanceDataClass.DSPerformance			' Form Specific !!!!
	Private PerformanceTable As RenaissanceDataClass.DSPerformance.tblPerformanceDataTable
	Private PerformanceAdaptor As SqlDataAdapter

	Private ThisStandardDataset As RenaissanceGlobals.StandardDataset


	' Active Element.

  Private thisAuditID As Integer
	Private thisPosition As Integer
	Private _IsOverCancelButton As Boolean
	Private _InUse As Boolean

	' Form Status Flags

	Private FormIsValid As Boolean
	Private FormChanged As Boolean
	Private _FormOpenFailed As Boolean
	Private InPaint As Boolean
	Private InGetFormData_Tick As Boolean
	Private AddNewRecord As Boolean

	' User Permission Flags

	Private HasReadPermission As Boolean
	Private HasUpdatePermission As Boolean
	Private HasInsertPermission As Boolean
	Private HasDeletePermission As Boolean

  Private _InSave As Boolean

#End Region

#Region " Form 'Properties' "

	Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

	Public ReadOnly Property FormChangedFlag() As Boolean
		Get
			Return FormChanged
		End Get
	End Property

	Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return _IsOverCancelButton
		End Get
		Set(ByVal Value As Boolean)
			_IsOverCancelButton = Value
		End Set
	End Property

	Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
		Get
			Return InPaint
		End Get
	End Property

	Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
		Get
			Return _InUse
		End Get
	End Property

	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

#Region " Local Classes"


#End Region

	Public Sub New(ByVal pMainForm As GenoaMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False
		_InUse = True
    _InSave = False

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		THIS_FORM_SelectingCombo = Me.Combo_SelectMasterName
		THIS_FORM_NewMoveToControl = Me.Grid_Prices

		' Default Select and Order fields.

		THIS_FORM_SelectBy = "Mastername"
		THIS_FORM_OrderBy = "Mastername"

		THIS_FORM_ValueMember = "ID"

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = GenoaFormID.frmPerformance

		' This form's dataset type.

		ThisStandardDataset = RenaissanceStandardDatasets.Mastername	' This Defines the Form Data !!! 

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		' Form Control Changed events

		AddHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
    'AddHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		' Data object names standard to this Form type.

		THIS_TABLENAME = ThisStandardDataset.TableName
		THIS_ADAPTORNAME = ThisStandardDataset.Adaptorname
		THIS_DATASETNAME = ThisStandardDataset.DatasetName

		THIS_FORM_ChangeID = ThisStandardDataset.ChangeID

		' Establish / Retrieve data objects for this form.


		PerformanceAdaptor = New SqlDataAdapter
    PerformanceDataset = New DSPerformance
		PerformanceTable = PerformanceDataset.tblPerformance
		If (PerformanceTable.Columns.Contains("ID")) Then
			PerformanceTable.Columns("ID").AllowDBNull = True
		End If

    MainForm.MainAdaptorHandler.Set_AdaptorCommands(MainForm.MainDataHandler.Get_Connection(Genoa_CONNECTION), PerformanceAdaptor, RenaissanceStandardDatasets.Performance.TableName)

    Me.RootMenu.PerformLayout()

		Try
			InPaint = True


		Catch ex As Exception
		Finally
			InPaint = False
		End Try

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
	Public Sub ResetForm() Implements StandardGenoaForm.ResetForm
		THIS_FORM_SelectBy = "Mastername"
		THIS_FORM_OrderBy = "Mastername"

		Call Form_Load(Me, New System.EventArgs)
	End Sub

	Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
		' *****************************************************************************************
		'
		'
		' *****************************************************************************************

		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False
		_InUse = True

		' Initialise Data structures. Connection, Adaptor and Dataset.

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

    ' Initialse form

		InPaint = True
		IsOverCancelButton = False

		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		Call SetSortedRows()

		' Initialise Grid

		Grid_Prices.DataSource = Nothing
		Grid_Prices.Columns.Clear()

		Try
			Grid_Prices.AutoGenerateColumns = True
			Grid_Prices.DataSource = PerformanceTable
		Catch ex As Exception
		Finally
			Grid_Prices.AutoGenerateColumns = False
			Grid_Prices.DataSource = Nothing
		End Try

		Grid_Prices.Columns("ID").Visible = False
		Grid_Prices.Columns("AuditID").Visible = False
		Grid_Prices.Columns("LastUpdated").Visible = False

		Grid_Prices.Columns("PerformanceDate").DefaultCellStyle.Format = "dd MMM yyyy"
		Grid_Prices.Columns("PerformanceDate").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter
		Grid_Prices.Columns("PerformanceDate").HeaderText = "    Date    "
		Grid_Prices.Columns("PerformanceReturn").DefaultCellStyle.Format = "#,##0.00####%;-#,##0.00####%"
		Grid_Prices.Columns("PerformanceReturn").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
		Grid_Prices.Columns("PerformanceReturn").HeaderText = "    Return    "
		Grid_Prices.Columns("FundsManaged").DefaultCellStyle.Format = "#,##0.00;-#,##0.00"
		Grid_Prices.Columns("FundsManaged").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
		Grid_Prices.Columns("FundsManaged").HeaderText = "Funds Managed"
		Grid_Prices.Columns("NAV").DefaultCellStyle.Format = "#,##0.00;-#,##0.00"
		Grid_Prices.Columns("NAV").DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
		Grid_Prices.Columns("NAV").HeaderText = "   NAV   "
		Grid_Prices.Columns.Add("Modified", "Modified")
		Grid_Prices.Columns("Modified").ValueType = GetType(Boolean)
		Grid_Prices.Columns("Modified").DefaultCellStyle = Grid_Prices.Columns("Estimate").DefaultCellStyle
		Grid_Prices.Columns("Modified").Visible = False

		Dim DeleteColumn As DataGridViewCheckBoxColumn = New DataGridViewCheckBoxColumn(False)
		DeleteColumn.Name = "Delete"
		DeleteColumn.HeaderText = "Delete"
		Grid_Prices.Columns.Add(DeleteColumn)
		'Grid_Prices.Columns("Delete").ValueType = GetType(Boolean)
		'Grid_Prices.Columns("Delete").DefaultCellStyle = Grid_Prices.Columns("Estimate").DefaultCellStyle
		Grid_Prices.Columns("Delete").Visible = True
		Grid_Prices.AllowUserToAddRows = True
		Grid_Prices.AllowUserToDeleteRows = False
		Grid_Prices.Sort(Grid_Prices.Columns("PerformanceDate"), System.ComponentModel.ListSortDirection.Ascending)
		Grid_Prices.MultiSelect = False

		' Initialise Timer

		GenericUpdateObject = MainForm.AddFormUpdate(Me, AddressOf GetFormData_Tick)

		' Display initial record.

		thisPosition = 0
    If MainForm.MasternameDictionary.Count > 0 Then
      Call GetFormData(CInt(MainForm.MasternameDictionary.KeyValue(0)))
    Else
      Me.THIS_FORM_SelectingCombo.SelectedIndex = (-1)
      Call GetFormData(Nothing)
    End If

		InPaint = False

	End Sub

	Private Sub frmEntity_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...

		_InUse = False
		MainForm.RemoveFormUpdate(Me)

		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			If (FormChanged = True) Then
				Call SetFormData()
			End If

			HideForm = True
			If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.EntryForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				' Form Control Changed events

				RemoveHandler THIS_FORM_SelectingCombo.SelectedIndexChanged, AddressOf Combo_SelectComboChanged
        'RemoveHandler THIS_FORM_SelectingCombo.KeyUp, AddressOf MainForm.ComboSelectAsYouType

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'VeniceAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim OrgInPaint As Boolean
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean
		Dim RefreshForm As Boolean = False

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		OrgInPaint = InPaint
		InPaint = True
		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
			RefreshForm = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If

    ' ****************************************************************
		' Changes to the Main FORM table :-
		' ****************************************************************

    If (e.TableChanged(RenaissanceChangeID.Performance)) Then

      If (IsNumeric(e.UpdateDetail(RenaissanceChangeID.Performance))) AndAlso (CInt(e.UpdateDetail(RenaissanceChangeID.Performance)) = thisAuditID) Then
        RefreshForm = True
      End If

    End If

    If (e.TableChanged(THIS_FORM_ChangeID) = True) Or KnowledgeDateChanged Then
      RefreshForm = True

      ' Re-Set Controls etc.
      Call SetSortedRows()

      ' Move again to the correct item
      thisPosition = MainForm.MasternameDictionary.IndexOfKey(thisAuditID)

      ' Set SelectingCombo Index.
      If (Me.MainForm.MasternameDictionary.Count <= 0) Then
        Try
          Me.THIS_FORM_SelectingCombo.SelectedIndex = -1
        Catch ex As Exception
        End Try
      ElseIf (thisPosition < 0) Then
        Try
          MainForm.ClearComboSelection(THIS_FORM_SelectingCombo)
          thisAuditID = (-1)
        Catch ex As Exception
        End Try
      Else
        Try
          Me.THIS_FORM_SelectingCombo.SelectedValue = thisAuditID
        Catch ex As Exception
        End Try
      End If

    End If

		' ****************************************************************
		' Repaint if not currently in Edit Mode
		'
		' ****************************************************************

		InPaint = OrgInPaint

		' Retrieve form data if appropriate, i.e. this data item is not already being edited.
    If (RefreshForm = True) AndAlso (FormChanged = False) AndAlso (AddNewRecord = False) Then

      GetFormData(thisAuditID) ' Includes a call to 'SetButtonStatus()'

    Else
      If SetButtonStatus_Flag Then
        Call SetButtonStatus()
      End If
    End If

	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Build Sorted list from the Source dataset and update the Select Combo
	Private Sub SetSortedRows()

    THIS_FORM_SelectingCombo.MasternameCollection = MainForm.MasternameDictionary

    'Dim OrgInPaint As Boolean

    '  Dim SelectString As String = "True"

    '  ' Form Specific Selection Combo :-
    '  If (THIS_FORM_SelectingCombo Is Nothing) Then Exit Sub

    '  ' Code is pretty Generic from here on...

    '  ' Set paint local so that changes to the selection combo do not trigger form updates.

    '  OrgInPaint = InPaint
    '  InPaint = True

    '  ' Get selected Row sets, or exit if no data is present.
    '  If myDataset Is Nothing Then
    '    ReDim SortedRows(0)
    '    ReDim SelectBySortedRows(0)
    '  Else
    '    SortedRows = myTable.Select(SelectString, THIS_FORM_OrderBy)
    '    SelectBySortedRows = myTable.Select(SelectString, THIS_FORM_SelectBy)
    '  End If

    '  'Try

    '  '  Dim thisrow As DataRow
    '  '  Dim thisDrowDownWidth As Integer
    '  '  Dim SizingBitmap As Bitmap
    '  '  Dim SizingGraphics As Graphics

    '  '  ' Set Combo data source
    '  '  THIS_FORM_SelectingCombo.DataSource = SortedRows
    '  '  THIS_FORM_SelectingCombo.DisplayMember = THIS_FORM_SelectBy
    '  '  THIS_FORM_SelectingCombo.ValueMember = THIS_FORM_ValueMember

    '  '  ' Ensure that the Selection Combo's DrowDownWidth is wide enough to take the required strings

    '  '  thisDrowDownWidth = THIS_FORM_SelectingCombo.Width
    '  '  For Each thisrow In SortedRows

    '  '    ' Compute the string dimensions in the given font
    '  '    SizingBitmap = New Bitmap(1, 1, System.Drawing.Imaging.PixelFormat.Format32bppArgb)
    '  '    SizingGraphics = Graphics.FromImage(SizingBitmap)
    '  '    Dim stringSize As SizeF = SizingGraphics.MeasureString(thisrow(THIS_FORM_SelectBy).ToString, THIS_FORM_SelectingCombo.Font)
    '  '    If (stringSize.Width) > thisDrowDownWidth Then
    '  '      thisDrowDownWidth = CInt(stringSize.Width)
    '  '    End If
    '  '  Next

    '  '  THIS_FORM_SelectingCombo.DropDownWidth = thisDrowDownWidth

    '  'Catch ex As Exception
    '  'End Try

    'InPaint = OrgInPaint
	End Sub

  ' Check User permissions
	Private Sub CheckPermissions()

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub

	' Flag changes to form controls, Event Associations made in 'New' routine.
	Private Sub FormControlChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)

		If InPaint = False Then
			If (Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord) Then
				FormChanged = True
				Me.btnSave.Enabled = True
				Me.btnCancel.Enabled = True
        Me.Check_ShowRecent.Enabled = False
      End If
		End If

	End Sub

	Private Sub Grid_CellChanged(ByVal sender As Object, ByVal e As C1.Win.C1FlexGrid.RowColEventArgs)
		Call FormControlChanged(Grid_Prices, New System.EventArgs)
	End Sub

	Public Sub AuditReportMenuEvent(ByVal sender As Object, ByVal e As EventArgs)
		' Event handler for the Audit Report Menu
		' Event association is made during the dynamic menu creation

		Select Case CInt(CType(sender, System.Windows.Forms.ToolStripMenuItem).Tag)
			Case 0 ' This Record
				If (Me.AddNewRecord = False) AndAlso (thisAuditID >= 0) Then
					Run_AuditReport_ThisRecord(MainForm, ThisStandardDataset, thisAuditID)
				End If

			Case 1 ' All Records
				Run_AuditReport_ALLRecords(MainForm, ThisStandardDataset)

		End Select

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "



#End Region

#Region " Get & Set Form Data / SetButton / ValidateForm / btnCancel Events (Form Specific Code) "

	Private Function GetFormData_Tick() As Boolean
		' *******************************************************************************
		'
		' Callback process for the Generic Form Update Process.
		'
		' Function MUST return True / False on Update.
		' True will clear the update request. False will not.
		'
		' *******************************************************************************
		Dim RVal As Boolean = False

    MainForm.DebugPrint("GetFormData_Tick(), InGetFormData_Tick=" & InGetFormData_Tick.ToString)

		If (InGetFormData_Tick) OrElse (Not _InUse) Then
			Return False
			Exit Function
		End If

		Try
			InGetFormData_Tick = True

			' Find the correct data row, then show it...
      thisPosition = MainForm.MasternameDictionary.IndexOfKey(THIS_FORM_SelectingCombo.SelectedValue)

      MainForm.DebugPrint("GetFormData_Tick(), GetFormData()")

      GetFormData(CInt(THIS_FORM_SelectingCombo.SelectedValue))

			RVal = True

		Catch ex As Exception
			RVal = False
		Finally
			InGetFormData_Tick = False
		End Try

		Return RVal

	End Function


  Private Sub GetFormData(ByVal pPertracID As Integer)
    ' ******************************************************************************
    '
    ' Routine to populate form Controls given a data row.
    ' Form is cleared for invalid Datarows or Invalid Form.
    '
    ' ******************************************************************************

    Dim OrgInpaint As Boolean

    If (MainForm.MasternameDictionary.IndexOfKey(pPertracID) < 0) Then
      pPertracID = 0
      thisAuditID = 0
      btnNavFirst_Click(Nothing, New EventArgs)
      Exit Sub
    End If

    ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.

    Try
      ' Set 'Paint' flag to prevent event actions caused by changing the value of form fields.
      OrgInpaint = InPaint
      InPaint = True

      If (pPertracID <= 0) Or (FormIsValid = False) Or (Me.InUse = False) Then
        ' Bad / New Datarow - Clear Form.

        thisAuditID = (-1)

        Me.editAuditID.Text = ""
        editDataVendor.Text = ""

        Grid_Prices.DataSource = Nothing
        If (Grid_Prices.Rows.Count > 0) Then
          Grid_Prices.Rows.Clear()
        End If

        If AddNewRecord = True Then
          Me.btnCancel.Enabled = True
          Me.Check_ShowRecent.Enabled = False
          Me.THIS_FORM_SelectingCombo.Enabled = False
        Else
          Me.btnCancel.Enabled = False
          Me.Check_ShowRecent.Enabled = True
          Me.THIS_FORM_SelectingCombo.Enabled = True
        End If

      Else

        ' Populate Form with given data.
        Try

          thisAuditID = pPertracID

          Me.editAuditID.Text = thisAuditID.ToString

          Dim ThisInformationRow As DSInformation.tblInformationRow
          ThisInformationRow = MainForm.PertracData.GetInformationRow(pPertracID)

          If (editDataVendor.Text <> Nz(ThisInformationRow.DataVendorName, ".")) Then
            editDataVendor.Text = Nz(ThisInformationRow.DataVendorName, ".")
          End If

          If (_InSave) Then
            PerformanceTable = New DSPerformance.tblPerformanceDataTable
          Else
            If (PerformanceTable.Rows.Count > 0) Then
              PerformanceTable.Rows.Clear()
            End If
          End If

          'If (Grid_Prices.DataSource Is Nothing) Then
          '	Grid_Prices.DataSource = PerformanceTable
          'End If

          PerformanceAdaptor.SelectCommand.Parameters("@ID").Value = pPertracID
          PerformanceTable.Load(PerformanceAdaptor.SelectCommand.ExecuteReader)

          Dim RowCounter As Integer
          Dim StartCounter As Integer = 1
          Dim ThisTableRow As DSPerformance.tblPerformanceRow

          If (Check_ShowRecent.Checked) Then
            StartCounter = Math.Max(1, PerformanceTable.Rows.Count - 10)
          End If

          ' Populate Grid.

          If (Grid_Prices.Rows.Count > 0) Then
            Grid_Prices.Rows.Clear()
          End If
          Dim NewGridRow As DataGridViewRow

          For RowCounter = StartCounter To (PerformanceTable.Rows.Count - 1)
            ThisTableRow = PerformanceTable.Rows(RowCounter)

            NewGridRow = Grid_Prices.Rows(Grid_Prices.Rows.Add())

            NewGridRow.Cells("AuditID").Value = ThisTableRow.AuditID
            NewGridRow.Cells("ID").Value = ThisTableRow.ID
            NewGridRow.Cells("PerformanceDate").Value = ThisTableRow.PerformanceDate
            NewGridRow.Cells("PerformanceReturn").Value = ThisTableRow.PerformanceReturn
            NewGridRow.Cells("FundsManaged").Value = ThisTableRow.FundsManaged
            NewGridRow.Cells("NAV").Value = ThisTableRow.NAV
            NewGridRow.Cells("Estimate").Value = ThisTableRow.Estimate
            NewGridRow.Cells("Modified").Value = False
            NewGridRow.Cells("Delete").Value = False
          Next

          ' Format Grid.

          If (Grid_Prices.SortOrder = Windows.Forms.SortOrder.Descending) Then
            Grid_Prices.Sort(Grid_Prices.SortedColumn, System.ComponentModel.ListSortDirection.Descending)
          Else
            Grid_Prices.Sort(Grid_Prices.SortedColumn, System.ComponentModel.ListSortDirection.Ascending)
          End If
          FormatNumericGridColumn(Grid_Prices.Columns("PerformanceReturn").Index)

          AddNewRecord = False

          Me.btnCancel.Enabled = False
          Me.Check_ShowRecent.Enabled = True
          Me.THIS_FORM_SelectingCombo.Enabled = True


        Catch ex As Exception

          Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
          Call GetFormData(Nothing)

        End Try

      End If

    Catch ex As Exception
      Call MainForm.LogError(Me.Name & ", GetFormData", 0, ex.Message, "Error Showing Data", ex.StackTrace, True)
    Finally

      ' Allow Field events to trigger before 'InPaint' Is re-set. 
      ' (Should) Prevent Validation errors during Form Draw.
      Application.DoEvents()

      ' Restore 'Paint' flag.
      InPaint = OrgInpaint

    End Try

    FormChanged = False
    Me.btnSave.Enabled = False

    ' As it says on the can....
    Call SetButtonStatus()

  End Sub

  Private Function SetFormData(Optional ByVal pConfirm As Boolean = True, Optional ByVal pForceSave As Boolean = False) As Boolean
    ' *************************************************************
    '
    ' Note that GroupList Entries are also added from the FundSearch Form. Changes
    ' to the GroupList table should be reflected there also.
    '
    ' *************************************************************
    Dim ErrMessage As String
    Dim ErrFlag As Boolean
    Dim ErrStack As String
    Dim ProtectedItem As Boolean = False
    Dim UpdateArrayList As New ArrayList
    Dim ThisPerformanceTable As DSPerformance.tblPerformanceDataTable = Nothing

    ErrMessage = ""
    ErrStack = ""

    ' *************************************************************
    ' Appropriate Save permission :-
    ' *************************************************************

    If (Me.HasUpdatePermission = False) And ((Me.HasInsertPermission = False) Or (Me.AddNewRecord = False)) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "You do not have permission to save this record.", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' If Save button is disabled then should not be able to save, exit silently.
    ' *************************************************************
    If (Me.btnSave.Enabled = False) And (Not pForceSave) Then
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' *************************************************************
    ' KnowledgeDate OK :-
    ' *************************************************************

    If (MainForm.Main_Knowledgedate > KNOWLEDGEDATE_NOW) Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Changes to Genoa are not allowed when the Knowledgedate is not set to `NOW`", "", True)
      Call btnCancel_Click(Me, New System.EventArgs)
      Exit Function
    End If

    ' Confirm Save, if required.

    If (pConfirm = True) Then
      If MessageBox.Show("Save Changes ?", "Save Changes ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) = Windows.Forms.DialogResult.No Then
        Return True
        Exit Function
      End If
    End If

    ' *************************************************************
    ' Procedure to Save the current form information.
    ' *************************************************************

    Dim StatusString As String
    Dim LogString As String
    Dim Position As Integer

    If (FormChanged = False) Or (FormIsValid = False) Then
      Return False
      Exit Function
    End If

    ' Validation
    StatusString = ""
    If ValidateForm(StatusString) = False Then
      MainForm.LogError(Me.Name & ", SetFormData()", LOG_LEVELS.Warning, "", "Validation Error." & vbCrLf & StatusString, "", True)
      Return False
      Exit Function
    End If

    ' Check current position in the table.
    Position = MainForm.MasternameDictionary.IndexOfKey(thisAuditID)

    ' Allow for new or missing ID.
    If Position < 0 Then
      Return False
      Exit Function
    Else
      ' Row found OK.
      LogString = "Edit : AuditID = " & thisAuditID.ToString

    End If


    Try

      ' *************************************************************
      ' Lock the Data Table, to prevent update conflicts.
      ' *************************************************************

      ErrFlag = False
      _InSave = True

      ' Post Additions / Updates to the underlying table :-

      Dim temp As Integer
      Try
        Dim GridRowCount As Integer
        Dim ThisID As Integer
        Dim ThisDate As Date
        Dim SelectedRows() As DSPerformance.tblPerformanceRow
        Dim ThisTableRow As DSPerformance.tblPerformanceRow

        ThisPerformanceTable = PerformanceTable
        Me.Cursor = Cursors.WaitCursor

        ' Update Modified Rows (Deletions handled already)
        For GridRowCount = 0 To (Grid_Prices.Rows.Count - 1)
          If (Grid_Prices.Rows(GridRowCount).Cells("Delete").Value = True) Then

            ThisID = Grid_Prices.Rows(GridRowCount).Cells("ID").Value
            ThisDate = Grid_Prices.Rows(GridRowCount).Cells("PerformanceDate").Value

            For Each ThisTableRow In ThisPerformanceTable.Select("(ID=" & ThisID.ToString & ") AND (PerformanceDate='" & ThisDate.ToString(QUERY_SHORTDATEFORMAT) & "')")
              ThisTableRow.Delete()
            Next

          ElseIf (Grid_Prices.Rows(GridRowCount).Cells("Modified").Value = True) Then

            If (IsNumeric(Grid_Prices.Rows(GridRowCount).Cells("ID").Value)) AndAlso (IsDate(Grid_Prices.Rows(GridRowCount).Cells("PerformanceDate").Value)) Then
              ThisID = Grid_Prices.Rows(GridRowCount).Cells("ID").Value
              ThisDate = Grid_Prices.Rows(GridRowCount).Cells("PerformanceDate").Value
              SelectedRows = ThisPerformanceTable.Select("(ID=" & ThisID.ToString & ") AND (PerformanceDate='" & ThisDate.ToString(QUERY_SHORTDATEFORMAT) & "')")

              If (SelectedRows Is Nothing) OrElse (SelectedRows.Length <= 0) Then
                ' New Row.
                ThisTableRow = ThisPerformanceTable.NewtblPerformanceRow

                ThisTableRow.AuditID = ThisID
                ThisTableRow.ID = ThisID
                ThisTableRow.PerformanceDate = ThisDate
                ThisTableRow.PerformanceReturn = 0
                ThisTableRow.NAV = 0
                ThisTableRow.FundsManaged = 0
                ThisTableRow.Estimate = False

                If (IsNumeric(Grid_Prices.Rows(GridRowCount).Cells("PerformanceReturn").Value)) Then
                  ThisTableRow.PerformanceReturn = CDbl(Grid_Prices.Rows(GridRowCount).Cells("PerformanceReturn").Value)
                End If
                If (IsNumeric(Grid_Prices.Rows(GridRowCount).Cells("NAV").Value)) Then
                  ThisTableRow.NAV = CDbl(Grid_Prices.Rows(GridRowCount).Cells("NAV").Value)
                End If
                If (IsNumeric(Grid_Prices.Rows(GridRowCount).Cells("FundsManaged").Value)) Then
                  ThisTableRow.FundsManaged = CDbl(Grid_Prices.Rows(GridRowCount).Cells("FundsManaged").Value)
                End If

                Try
                  If (Grid_Prices.Rows(GridRowCount).Cells("Estimate").Value IsNot Nothing) Then
                    ThisTableRow.Estimate = CBool(Grid_Prices.Rows(GridRowCount).Cells("Estimate").Value)
                  End If
                Catch ex As Exception
                End Try

                ThisPerformanceTable.Rows.Add(ThisTableRow)
              Else
                ' Update Rows.

                For Each ThisTableRow In SelectedRows
                  ThisTableRow.PerformanceReturn = Grid_Prices.Rows(GridRowCount).Cells("PerformanceReturn").Value
                  ThisTableRow.FundsManaged = Grid_Prices.Rows(GridRowCount).Cells("FundsManaged").Value
                  ThisTableRow.NAV = Grid_Prices.Rows(GridRowCount).Cells("NAV").Value
                  ThisTableRow.Estimate = Grid_Prices.Rows(GridRowCount).Cells("Estimate").Value
                Next
              End If
            End If
          End If
        Next
        SelectedRows = Nothing

        ' Save Changes

        If (ErrFlag = False) Then
          Dim RowCounter As Integer
          temp = 0

          Me.Text = "Add/Edit Instrument Returns - Saving Prices"

          PerformanceAdaptor.InsertCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate
          PerformanceAdaptor.UpdateCommand.Parameters("@Knowledgedate").Value = MainForm.Main_Knowledgedate

          ' Save the changed Rows in chunks.

          For RowCounter = 0 To (ThisPerformanceTable.Rows.Count - 1)
            UpdateArrayList.Add(ThisPerformanceTable.Rows(RowCounter))

            If (UpdateArrayList.Count >= 100) Then
              temp += MainForm.AdaptorUpdate(Me.Name, PerformanceAdaptor, UpdateArrayList.ToArray(GetType(DataRow)))
              UpdateArrayList.Clear()
              Me.Text = "Add/Edit Instrument Returns - Saving Prices (" & temp.ToString & ")"

              Application.DoEvents()
            End If
          Next

          If (UpdateArrayList.Count > 0) Then
            temp += MainForm.AdaptorUpdate(Me.Name, PerformanceAdaptor, UpdateArrayList.ToArray(GetType(DataRow)))
          End If

        End If

      Catch ex As Exception

        ErrMessage = ex.Message
        ErrFlag = True
        ErrStack = ex.StackTrace

      Finally
        UpdateArrayList.Clear()
        Me.Cursor = Cursors.Default

        Me.Text = "Add/Edit Instrument Returns"

        If (ThisPerformanceTable Is PerformanceTable) Then
          ThisPerformanceTable = Nothing
        Else
          ThisPerformanceTable.Clear()
          ThisPerformanceTable = Nothing
        End If

      End Try

    Catch ex As Exception
      ErrFlag = True
      ErrMessage = ex.Message
      ErrStack = ex.StackTrace
    Finally
      InPaint = False
      _InSave = False
    End Try


    ' *************************************************************
    ' Error report. Not done in the Try-Catch as that would serve to persist the SyncLock.
    ' *************************************************************

    If (ErrFlag = True) Then
      Call MainForm.LogError(Me.Name & ", SetFormData", 0, ErrMessage, "Error Saving Data", ErrStack, True)
    End If

    ' Finish off

    AddNewRecord = False
    FormChanged = False

    Me.THIS_FORM_SelectingCombo.Enabled = True

    ' Propagate changes

    Dim UpdateMessage As New RenaissanceGlobals.RenaissanceUpdateEventArgs(RenaissanceChangeID.Performance, thisAuditID.ToString)

    Call MainForm.Main_RaiseEvent(UpdateMessage)

  End Function

	Private Sub SetButtonStatus()
		' Sets the status of the form controlls appropriate to the current users 
		'permissions and the 'Changed' or 'New' status of the form.

		' No Read permission :-

		If Me.HasReadPermission = False Then
			MainForm.LogError(Me.Name & ", SetButtonStatus()", 0, "", "You do not have Read permission for this Form", "", True)
			Me.Close()
			Exit Sub
		End If

		If (Me.MainForm.Main_Knowledgedate <= KNOWLEDGEDATE_NOW) And _
		 ((Me.HasUpdatePermission) Or (Me.HasInsertPermission And Me.AddNewRecord)) Then

			Me.Grid_Prices.Enabled = True

		Else

			Me.Grid_Prices.Enabled = False

		End If

	End Sub

	Private Function ValidateForm(ByRef pReturnString As String) As Boolean
		' Form Validation code.
		' 
		' This code should be the final arbiter of what is allowed. no assumptions regarding 
		' prior validation should be made.
		' 
		' This Code is called by the SetFormData routine before position changes.
		'
		Dim RVal As Boolean

		RVal = True
		pReturnString = ""

		' Validate 

		Return RVal

	End Function

	Private Sub btnCancel_MouseEnter(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseEnter
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = True
	End Sub

	Private Sub btnCancel_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.MouseLeave
		' Simple Events for the 'Cancel' Button to maintain the status of the 'IsOverCancelButton' flag
		Me.IsOverCancelButton = False
	End Sub

#End Region

#Region " Navigation Code / GetPosition() (Generic Code) "

	Private Sub Combo_SelectComboChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
		' ***************************************************************
		' Selection Combo. SelectedItem changed.
		'
		' ***************************************************************

		' Don't react to changes made in paint routines etc.

    MainForm.DebugPrint("Combo_SelectComboChanged(), InPaint=" & InPaint.ToString)

    If (InPaint) Then

      MainForm.DebugPrint("Combo_SelectComboChanged(), Focused=" & Combo_SelectMasterName.Focused.ToString)

      If (GenericUpdateObject IsNot Nothing) Then
        Try
          MainForm.DebugPrint("Combo_SelectComboChanged(), GenericUpdateObject.FormToUpdate = True")

          GenericUpdateObject.FormToUpdate = True
        Catch ex As Exception
        End Try
      End If

      Exit Sub

    End If

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		' Find the correct data row, then show it...
    thisPosition = MainForm.MasternameDictionary.IndexOfKey(THIS_FORM_SelectingCombo.SelectedValue)

		If (GenericUpdateObject IsNot Nothing) Then
      Try
        MainForm.DebugPrint("Combo_SelectComboChanged(), GenericUpdateObject.FormToUpdate = True")

        GenericUpdateObject.FormToUpdate = True  '	GetFormDataToUpdate = True
      Catch ex As Exception
      End Try
		Else
      MainForm.DebugPrint("Combo_SelectComboChanged(), GetFormData()")

      Call GetFormData(CInt(THIS_FORM_SelectingCombo.SelectedValue))
    End If

	End Sub


	Private Sub btnNavPrev_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavPrev.Click
		' 'Previous' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		If thisPosition > 0 Then thisPosition -= 1
    If thisPosition >= MainForm.MasternameDictionary.Count Then
      thisPosition = (MainForm.MasternameDictionary.Count - 1)
    End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

	End Sub

	Private Sub btnNavNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavNext.Click
		' 'Next' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

    If thisPosition < (MainForm.MasternameDictionary.Count - 1) Then thisPosition += 1
    If thisPosition >= MainForm.MasternameDictionary.Count Then
      thisPosition = (MainForm.MasternameDictionary.Count - 1)
    End If

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

  End Sub

	Private Sub btnNavFirst_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNavFirst.Click
		' 'First' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

		thisPosition = 0
    If thisPosition >= MainForm.MasternameDictionary.Count Then
      thisPosition = (MainForm.MasternameDictionary.Count - 1)
    End If

    If (THIS_FORM_SelectingCombo.SelectedIndex = thisPosition) Then
      Combo_SelectComboChanged(THIS_FORM_SelectingCombo, New EventArgs)
    Else
      THIS_FORM_SelectingCombo.SelectedIndex = thisPosition
    End If

	End Sub

	Private Sub btnLast_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLast.Click
		' 'Last' Button.

		If (FormChanged = True) Then
			Call SetFormData()
		End If

    thisPosition = MainForm.MasternameDictionary.Count - 1

		THIS_FORM_SelectingCombo.SelectedIndex = thisPosition

	End Sub

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "

	Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click
		' Cancel Changes, redisplay form.

		FormChanged = False
		AddNewRecord = False

		Me.THIS_FORM_SelectingCombo.Enabled = True

    If (thisPosition >= 0) And (thisPosition < MainForm.MasternameDictionary.Count) Then
      Call GetFormData(CInt(MainForm.MasternameDictionary.KeyValue(thisPosition)))
    Else
      Call btnNavFirst_Click(Me, New System.EventArgs)
    End If

	End Sub

	Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
		' Save Changes, if any, without prompting.

		If (FormChanged = True) Then
      Dim FormControls As ArrayList = Nothing

      Try
        FormControls = MainForm.DisableFormControls(Me)

        Call SetFormData(False, True)

      Catch ex As Exception
      Finally
        MainForm.EnableFormControls(FormControls)

        SetButtonStatus()
        If (Not FormChanged) Then
          btnSave.Enabled = False
          btnCancel.Enabled = False
        End If
      End Try

    End If

	End Sub

	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' *************************************************************
		' Close Form
		' *************************************************************


		If (FormChanged = True) Then
			Call SetFormData()
		End If

		Me.Close()

	End Sub

#End Region

#Region " Form Control Event Code"

  Private Sub Check_ShowRecent_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_ShowRecent.CheckedChanged
    ' ************************************************************************************
    '
    '
    ' ************************************************************************************

    Try
      If (Not InPaint) AndAlso (Not FormChangedFlag) Then
        Call GetFormData(thisAuditID)
      End If
    Catch ex As Exception
    End Try
  End Sub

	Private Sub Grid_Prices_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_Prices.CellEnter
		' ****************************************************************************************
		'
		'
		' ****************************************************************************************

		Try
			If (Me.IsDisposed) OrElse (Not Me.Created) Then
				Exit Sub
			End If

			If (Grid_Prices.Rows(e.RowIndex).IsNewRow) Then
				' Grid_Prices.Rows.Add(1)

        If IsNumeric(Grid_Prices.Item(Grid_Prices.Columns("ID").Index, e.RowIndex).Value) = False Then
          Dim StatsDatePeriod As DealingPeriod = DealingPeriod.Monthly

          Grid_Prices.Item(Grid_Prices.Columns("ID").Index, e.RowIndex).Value = thisAuditID
          Grid_Prices.Item(Grid_Prices.Columns("AuditID").Index, e.RowIndex).Value = 0
          Grid_Prices.Item(Grid_Prices.Columns("PerformanceReturn").Index, e.RowIndex).Value = 0
          Grid_Prices.Item(Grid_Prices.Columns("FundsManaged").Index, e.RowIndex).Value = 0
          Grid_Prices.Item(Grid_Prices.Columns("NAV").Index, e.RowIndex).Value = 0
          Grid_Prices.Item(Grid_Prices.Columns("Estimate").Index, e.RowIndex).Value = False

          If (thisAuditID > 0) Then
            StatsDatePeriod = MainForm.PertracData.GetPertracDataPeriod(thisAuditID)
          End If

          If (e.RowIndex > 0) AndAlso IsDate(Grid_Prices.Item(Grid_Prices.Columns("PerformanceDate").Index, e.RowIndex - 1).Value) Then
            Grid_Prices.Item(Grid_Prices.Columns("PerformanceDate").Index, e.RowIndex).Value = FitDateToPeriod(StatsDatePeriod, AddPeriodToDate(StatsDatePeriod, CDate(Grid_Prices.Item(Grid_Prices.Columns("PerformanceDate").Index, e.RowIndex - 1).Value), 1), True)
          Else
            Grid_Prices.Item(Grid_Prices.Columns("PerformanceDate").Index, e.RowIndex).Value = FitDateToPeriod(StatsDatePeriod, Now.Date, True)
          End If

          Grid_Prices.BeginEdit(True)

        End If

			End If

		Catch ex As Exception

		End Try
	End Sub

	Private Sub Grid_Prices_CellLeave(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_Prices.CellLeave
		' ****************************************************************************************
		'
		'
		' ****************************************************************************************

		Try
			If (Me.IsDisposed) OrElse (Not Me.Created) Then
				Exit Sub
			End If

			' If the row being left id the 'NewRow' row, then nothing has been entered on it and it should be cleared.

			If (Grid_Prices.Rows(e.RowIndex).IsNewRow) Then
				Grid_Prices.Item(Grid_Prices.Columns("ID").Index, e.RowIndex).Value = Nothing
				Grid_Prices.Item(Grid_Prices.Columns("AuditID").Index, e.RowIndex).Value = Nothing
				Grid_Prices.Item(Grid_Prices.Columns("PerformanceReturn").Index, e.RowIndex).Value = Nothing
				Grid_Prices.Item(Grid_Prices.Columns("FundsManaged").Index, e.RowIndex).Value = Nothing
				Grid_Prices.Item(Grid_Prices.Columns("NAV").Index, e.RowIndex).Value = Nothing
				Grid_Prices.Item(Grid_Prices.Columns("Estimate").Index, e.RowIndex).Value = Nothing
				Grid_Prices.Item(Grid_Prices.Columns("PerformanceDate").Index, e.RowIndex).Value = Nothing
			End If

		Catch ex As Exception

		End Try

	End Sub

	Private Sub Grid_Prices_CellValueChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_Prices.CellValueChanged
		' ****************************************************************************************
		'
		'
		' ****************************************************************************************

		Try
			If (Not InPaint) Then
				Call FormControlChanged(Grid_Prices, Nothing)
				Grid_Prices.Item(Grid_Prices.Columns("Modified").Index, e.RowIndex).Value = True
			End If

			' Format Cell

			If (e.ColumnIndex = Grid_Prices.Columns("PerformanceReturn").Index) AndAlso (e.RowIndex >= 0) Then
				If IsNumeric(Grid_Prices.Item(e.ColumnIndex, e.RowIndex).Value) Then
					If CDbl(Grid_Prices.Item(e.ColumnIndex, e.RowIndex).Value) < 0 Then
						Grid_Prices.Item(e.ColumnIndex, e.RowIndex).Style.ForeColor = Color.Red
					Else
						If Grid_Prices.Item(e.ColumnIndex, e.RowIndex).Style.ForeColor <> Grid_Prices.Columns(e.ColumnIndex).DefaultCellStyle.ForeColor Then
							Grid_Prices.Item(e.ColumnIndex, e.RowIndex).Style.ForeColor = Grid_Prices.Columns(e.ColumnIndex).DefaultCellStyle.ForeColor
						End If
					End If

				End If
			End If

		Catch ex As Exception
		End Try

	End Sub

	Private Sub Grid_Prices_CellContentClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles Grid_Prices.CellContentClick
		' ****************************************************************************************
		'
		'
		' ****************************************************************************************

		Try

			If (Not InPaint) Then
				Call FormControlChanged(Grid_Prices, Nothing)
				Grid_Prices.Item(Grid_Prices.Columns("Modified").Index, e.RowIndex).Value = True
			End If

		Catch ex As Exception
		End Try

	End Sub

	Private Sub Grid_Prices_CellParsing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellParsingEventArgs) Handles Grid_Prices.CellParsing
		' ****************************************************************************************
		'
		'
		' ****************************************************************************************

		Try
			e.Value = ConvertValue(e.Value, e.DesiredType)
			e.ParsingApplied = True
		Catch ex As Exception
			e.ParsingApplied = False
		End Try
	End Sub

	Private Sub Grid_Prices_RowsRemoved(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowsRemovedEventArgs) Handles Grid_Prices.RowsRemoved
		' ****************************************************************************************
		'
		'
		' ****************************************************************************************


		Try
			If (Not InPaint) Then
				If (Not Grid_Prices.Rows(e.RowIndex).IsNewRow) Then
					Try
						' Reflect deletion in data table

						'Dim ThisID As Integer
						'Dim ThisDate As Date

						'ThisID = Grid_Prices.Rows(e.RowIndex).Cells("ID").Value
						'ThisDate = Grid_Prices.Rows(e.RowIndex).Cells("PerformanceDate").Value

						'For Each ThisTableRow As DSPerformance.tblPerformanceRow In PerformanceTable.Select("(ID=" & ThisID.ToString & ") AND (PerformanceDate='" & ThisDate.ToString(QUERY_SHORTDATEFORMAT) & "')")
						'	ThisTableRow.Delete()
						'Next

					Catch ex As Exception
					End Try
				End If

				Call FormControlChanged(Grid_Prices, Nothing)
			End If
		Catch ex As Exception
		End Try

	End Sub

	Private Sub Grid_Prices_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles Grid_Prices.DataError

	End Sub

	Private _validData As Boolean

	Private Sub Grid_Prices_DragEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Grid_Prices.DragEnter
		' ***********************************************************************************
		'
		'
		' ***********************************************************************************

		Try
			' Get the data
			Dim DragString As String = CType(e.Data.GetData(GetType(String)), String)

			' No empty data
			If DragString Is Nothing OrElse DragString.Length = 0 Then
				_validData = False
				Return
			End If

			If (DragString IsNot Nothing) Then
				If DragString.Split(Chr(13)).Length > 1 Then
					_validData = True
					Return
				End If
			End If

			_validData = False
		Catch ex As Exception
		End Try
	End Sub

	Private Sub Grid_Prices_DragLeave(ByVal sender As Object, ByVal e As System.EventArgs) Handles Grid_Prices.DragLeave
		' ***********************************************************************************
		'
		'
		' ***********************************************************************************

		Try
			_validData = False
		Catch ex As Exception
		End Try
	End Sub

	Private Sub Grid_Prices_DragOver(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Grid_Prices.DragOver
		' ***********************************************************************************
		'
		'
		' ***********************************************************************************

		Try
			If _validData Then
				e.Effect = DragDropEffects.Copy
			Else
				e.Effect = DragDropEffects.None
			End If
		Catch ex As Exception
		End Try
	End Sub

	Private Sub Grid_Prices_DragDrop(ByVal sender As Object, ByVal e As System.Windows.Forms.DragEventArgs) Handles Grid_Prices.DragDrop
		' ***********************************************************************************
		'
		'
		' ***********************************************************************************

		Try
			'get the data
			Dim DragString As String = CType(e.Data.GetData(GetType(String)), String)

			Grid_Prices_ParseString(DragString)

		Catch ex As Exception
		End Try
	End Sub

	Private Sub Grid_Prices_ParseString(ByVal ReturnsString As String)
		' ***********************************************************************************
		'
		'
		' ***********************************************************************************

		Try
			' No data
			If (ReturnsString Is Nothing) OrElse (ReturnsString.Length <= 0) Then
				Exit Sub
			End If

			'get the data
			Dim DragString As String = ReturnsString ' CType(e.Data.GetData(GetType(String)), String)

			Dim DroppedRows() As String
			Dim DroppedCols() As String
			Dim RowCount As Integer
			Dim ColCount As Integer
			Dim IsInRows As Boolean = False
			Dim IsInCols As Boolean = False
      Dim IsNAV As Boolean = False
      Dim DateValues(-1) As Date
			Dim ReturnValue(-1) As Double
			Dim ReturnIndex As Integer
			Dim SourceIndex As Integer

      Dim GridDateOrdinal As Integer = Grid_Prices.Columns("PerformanceDate").Index
      Dim GridReturnOrdinal As Integer = Grid_Prices.Columns("PerformanceReturn").Index
      Dim GridNAVOrdinal As Integer = Grid_Prices.Columns("NAV").Index

			DroppedRows = DragString.Split(New String() {CStr(Chr(13)) & CStr(Chr(10))}, StringSplitOptions.RemoveEmptyEntries)

			If (DroppedRows Is Nothing) OrElse (DroppedRows.Length <= 0) Then
				Exit Sub
			End If
			RowCount = DroppedRows.Length

			DroppedCols = DroppedRows(0).Split(Chr(9))
			If (DroppedCols Is Nothing) OrElse (DroppedCols.Length <= 0) Then
				Exit Sub
			End If
			ColCount = DroppedCols.Length

			If (RowCount <= 1) And (ColCount <= 1) Then
				Exit Sub
			End If

			' OK, Now try to figure out what format the Data is in.

      If (RowCount = 1) Then
        ' Must be <Date>, <Value> [,<Date>, <Value>] .... on a single line to be valid.

        ReturnIndex = 0

        For SourceIndex = 0 To (CInt(ColCount / 2) - 1)
          If (ConvertIsNumeric(DroppedCols((SourceIndex * 2) + 1))) Then
            If IsDate(DroppedCols(SourceIndex * 2)) Then
              ReturnIndex += 1
            ElseIf IsNumeric(DroppedCols(SourceIndex * 2)) AndAlso (CDbl(DroppedCols(SourceIndex * 2)) > 10000) Then
              ReturnIndex += 1
            End If
          End If
        Next

        If (ReturnIndex > 0) Then
          ReDim DateValues(ReturnIndex - 1)
          ReDim ReturnValue(ReturnIndex - 1)

          ReturnIndex = 0

          For SourceIndex = 0 To (CInt(ColCount / 2) - 1)
            If (ConvertIsNumeric(DroppedCols((SourceIndex * 2) + 1))) Then
              If IsDate(DroppedCols(SourceIndex * 2)) Then
                DateValues(ReturnIndex) = Date.Parse(DroppedCols(SourceIndex * 2))
                ReturnValue(ReturnIndex) = ConvertValue(DroppedCols((SourceIndex * 2) + 1), GetType(Double))

                ReturnIndex += 1
              ElseIf IsNumeric(DroppedCols(SourceIndex * 2)) AndAlso (CDbl(DroppedCols(SourceIndex * 2)) > 10000) Then
                DateValues(ReturnIndex) = Date.FromOADate(CDbl(DroppedCols(SourceIndex * 2)))
                ReturnValue(ReturnIndex) = ConvertValue(DroppedCols((SourceIndex * 2) + 1), GetType(Double))

                ReturnIndex += 1
              End If
            End If

            If (ReturnIndex >= DateValues.Length) Then
              Exit For
            End If
          Next
        End If

      ElseIf (ColCount = 1) And (RowCount = 2) Then
        ' Must be in <Date>, <Value> to be valid.

        If (ConvertIsNumeric(DroppedRows(1))) Then
          If IsDate(DroppedRows(0)) Then
            ReDim DateValues(0)
            ReDim ReturnValue(0)

            DateValues(0) = Date.Parse(DroppedRows(0))
            ReturnValue(0) = ConvertValue(DroppedRows(1), GetType(Double))
          ElseIf IsNumeric(DroppedRows(0)) AndAlso (CDbl(IsNumeric(DroppedRows(0))) > 10000) Then
            ReDim DateValues(0)
            ReDim ReturnValue(0)

            DateValues(0) = Date.FromOADate(CDbl(DroppedRows(0)))
            ReturnValue(0) = ConvertValue(DroppedRows(1), GetType(Double))
          End If

        End If

      ElseIf (ColCount > 1) And (RowCount > 1) Then
        Dim FirstRowHeaders As Boolean = False

        If ((DroppedCols(0).ToUpper = "DATE") AndAlso (DroppedCols(1).ToUpper = "RETURN")) Then
          IsNAV = False
          IsInCols = True
          FirstRowHeaders = True

        ElseIf ((DroppedCols(0).ToUpper = "DATE") AndAlso (DroppedCols(1).ToUpper = "NAV")) Then
          IsNAV = True
          IsInCols = True
          FirstRowHeaders = True

        ElseIf (ColCount = 2) And (RowCount = 2) Then
          ' In Rows or Cols >

          If IsDate(DroppedCols(1)) Then
            IsInRows = True
          ElseIf IsNumeric(DroppedCols(1)) AndAlso (CDbl(DroppedCols(1)) > 10000) Then
            IsInRows = True
          Else
            IsInCols = True
          End If

        ElseIf (ColCount = 2) Then
          IsInCols = True

          If ((DroppedCols(0).ToUpper = "DATE") AndAlso (DroppedCols(1).ToUpper = "RETURN")) Then
            IsNAV = False
            IsInRows = True

          ElseIf ((DroppedCols(0).ToUpper = "DATE") AndAlso (DroppedCols(1).ToUpper = "NAV")) Then
            IsNAV = True
            IsInRows = True

          End If

        ElseIf (RowCount = 2) Then
          IsInRows = True
        End If

        If (IsInRows) Then
          Dim DroppedDates() As String = DroppedRows(0).Split(Chr(9))
          Dim DroppedReturns() As String = DroppedRows(1).Split(Chr(9))

          ReturnIndex = 0

          If (DroppedDates IsNot Nothing) AndAlso (DroppedReturns IsNot Nothing) AndAlso (DroppedDates.Length = DroppedReturns.Length) Then
            For SourceIndex = 0 To (ColCount - 1)
              If (ConvertIsNumeric(DroppedReturns(SourceIndex))) Then
                If (IsDate(DroppedDates(SourceIndex))) Then
                  ReturnIndex += 1
                ElseIf (IsNumeric(DroppedDates(SourceIndex))) AndAlso (CDbl(DroppedDates(SourceIndex)) > 10000) Then
                  ReturnIndex += 1
                End If
              End If
            Next

            If (ReturnIndex > 0) Then
              ReDim DateValues(ReturnIndex - 1)
              ReDim ReturnValue(ReturnIndex - 1)

              ReturnIndex = 0

              For SourceIndex = 0 To (ColCount - 1)
                If (ConvertIsNumeric(DroppedReturns(SourceIndex))) Then
                  If (IsDate(DroppedDates(SourceIndex))) Then
                    DateValues(ReturnIndex) = Date.Parse(DroppedDates(SourceIndex))
                    ReturnValue(ReturnIndex) = ConvertValue(DroppedReturns(SourceIndex), GetType(Double))

                    ReturnIndex += 1
                  ElseIf (IsNumeric(DroppedDates(SourceIndex))) AndAlso (CDbl(DroppedDates(SourceIndex)) > 10000) Then
                    DateValues(ReturnIndex) = Date.FromOADate(CDbl(DroppedDates(SourceIndex)))
                    ReturnValue(ReturnIndex) = ConvertValue(DroppedReturns(SourceIndex), GetType(Double))

                    ReturnIndex += 1
                  End If
                End If

                If (ReturnIndex >= DateValues.Length) Then
                  Exit For
                End If
              Next

            End If
          End If

        ElseIf (IsInCols) Then
          Dim SumOfReturns As Double = 0.0#
          ReturnIndex = 0

          For SourceIndex = 0 To (RowCount - 1)
            If (Not FirstRowHeaders) OrElse (SourceIndex > 0) Then

              DroppedCols = DroppedRows(SourceIndex).Split(Chr(9))

              If (DroppedCols.Length = 2) AndAlso (ConvertIsNumeric(DroppedCols(1))) Then
                If IsDate(DroppedCols(0)) Then
                  ReturnIndex += 1
                ElseIf IsNumeric(DroppedCols(0)) AndAlso (CDbl(DroppedCols(0)) > 10000) Then
                  ReturnIndex += 1
                End If
              End If

            End If

          Next

          If (ReturnIndex > 0) Then
            ReDim DateValues(ReturnIndex - 1)
            ReDim ReturnValue(ReturnIndex - 1)

            ReturnIndex = 0

            For SourceIndex = 0 To (RowCount - 1)
              If (Not FirstRowHeaders) OrElse (SourceIndex > 0) Then

                DroppedCols = DroppedRows(SourceIndex).Split(Chr(9))

                If (DroppedCols.Length = 2) AndAlso (ConvertIsNumeric(DroppedCols(1))) Then
                  If IsDate(DroppedCols(0)) Then

                    DateValues(ReturnIndex) = Date.Parse(DroppedCols(0))
                    ReturnValue(ReturnIndex) = ConvertValue(DroppedCols(1), GetType(Double))
                    SumOfReturns += ReturnValue(ReturnIndex)

                    ReturnIndex += 1
                  ElseIf IsNumeric(DroppedCols(0)) AndAlso (CDbl(DroppedCols(0)) > 10000) Then
                    DateValues(ReturnIndex) = Date.FromOADate(CDbl(DroppedCols(0)))
                    ReturnValue(ReturnIndex) = ConvertValue(DroppedCols(1), GetType(Double))
                    SumOfReturns += ReturnValue(ReturnIndex)

                    ReturnIndex += 1
                  End If
                End If

                If (ReturnIndex >= DateValues.Length) Then
                  Exit For
                End If

              End If
            Next

            ' If Average Return > 100%, assume it is a NAV.

            If (ReturnIndex > 0) AndAlso ((SumOfReturns / ReturnIndex) > 1.0#) Then
              IsNAV = True
            End If

          End If
        End If
      End If

      ' Merge in Data....
      ' Ahhhh, at last....

      If (DateValues Is Nothing) OrElse (DateValues.Length <= 0) Then
        Exit Sub
      End If
      If (ReturnValue Is Nothing) OrElse (ReturnValue.Length <= 0) Then
        Exit Sub
      End If

      Dim ThisDate As Date
      Dim ThisReturn As Double
      Dim GridCounter As Integer
      Dim NewGridRow As DataGridViewRow
      Dim FoundIt As Boolean

      For ReturnIndex = 0 To (DateValues.Length - 1)
        ThisDate = DateValues(ReturnIndex)
        ThisReturn = ReturnValue(ReturnIndex)
        FoundIt = False

        For GridCounter = 0 To (Grid_Prices.Rows.Count - 1)
          Try
            If (Grid_Prices.Item(GridDateOrdinal, GridCounter).Value = ThisDate) Then

              If (IsNAV) Then
                Grid_Prices.Item(GridNAVOrdinal, GridCounter).Value = ThisReturn
                Grid_Prices.UpdateCellValue(GridNAVOrdinal, GridCounter)
              Else
                Grid_Prices.Item(GridReturnOrdinal, GridCounter).Value = ThisReturn
                Grid_Prices.UpdateCellValue(GridReturnOrdinal, GridCounter)
              End If

              FoundIt = True
              Exit For
            End If
          Catch ex As Exception
          End Try
        Next

        If (Not FoundIt) Then
          NewGridRow = Grid_Prices.Rows(Grid_Prices.Rows.Add())

          NewGridRow.Cells("AuditID").Value = thisAuditID
          NewGridRow.Cells("ID").Value = thisAuditID
          NewGridRow.Cells("PerformanceDate").Value = ThisDate
          NewGridRow.Cells("FundsManaged").Value = 0
          NewGridRow.Cells("Estimate").Value = False
          NewGridRow.Cells("Modified").Value = True

          If (IsNAV) Then
            NewGridRow.Cells("NAV").Value = ThisReturn
            NewGridRow.Cells("PerformanceReturn").Value = 0
          Else
            NewGridRow.Cells("PerformanceReturn").Value = ThisReturn
            NewGridRow.Cells("NAV").Value = 0
          End If

        End If
      Next

      ' Format Grid.
      'If (IsNAV) Then
      '  Grid_Prices.Sort(Grid_Prices.Columns("PerformanceDate"), System.ComponentModel.ListSortDirection.Ascending)

      '  For GridCounter = 1 To (Grid_Prices.Rows.Count - 1)

      '  Next
      'End If

      If (Grid_Prices.SortOrder = Windows.Forms.SortOrder.Descending) Then
        Grid_Prices.Sort(Grid_Prices.SortedColumn, System.ComponentModel.ListSortDirection.Descending)
      Else
        Grid_Prices.Sort(Grid_Prices.SortedColumn, System.ComponentModel.ListSortDirection.Ascending)
      End If

      FormatNumericGridColumn(Grid_Prices.Columns("PerformanceReturn").Index)

    Catch ex As Exception
    End Try
	End Sub

	Private Sub Grid_Prices_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Grid_Prices.KeyDown
		' ***********************************************************************************
		'
		'
		' ***********************************************************************************

		Try
			If e.Control And (e.KeyCode = Keys.C) Then
        Me.Cursor = Cursors.WaitCursor

        Dim HeaderString As String = "ID" & Chr(9) & "Date" & Chr(9) & "Return" & Chr(9) & "FundsManaged" & Chr(9) & "NAV" & Chr(9) & "Estimate" & Chr(13) & Chr(10)
				Dim GridRow As Integer

				For GridRow = 0 To (Grid_Prices.Rows.Count - 1)
					Try
						If (GridRow <> Grid_Prices.NewRowIndex) Then
							HeaderString &= Grid_Prices.Item(Grid_Prices.Columns("ID").Index, GridRow).Value.ToString & Chr(9) & CDate(Grid_Prices.Item(Grid_Prices.Columns("PerformanceDate").Index, GridRow).Value).ToString(DISPLAYMEMBER_DATEFORMAT) & Chr(9) & Grid_Prices.Item(Grid_Prices.Columns("PerformanceReturn").Index, GridRow).Value.ToString & Chr(9) & Grid_Prices.Item(Grid_Prices.Columns("FundsManaged").Index, GridRow).Value.ToString() & Chr(9) & Grid_Prices.Item(Grid_Prices.Columns("NAV").Index, GridRow).Value.ToString & Chr(9) & Grid_Prices.Item(Grid_Prices.Columns("Estimate").Index, GridRow).Value.ToString & Chr(13) & Chr(10)
						End If
					Catch ex As Exception
					End Try
				Next

				Clipboard.Clear()
				Clipboard.SetData(System.Windows.Forms.DataFormats.Text, (HeaderString))

				e.Handled = True
      ElseIf e.Control And (e.KeyCode = Keys.V) Then
        Me.Cursor = Cursors.WaitCursor

        Dim ClipboardData As String

        ClipboardData = Clipboard.GetData(System.Windows.Forms.DataFormats.Text)

        Grid_Prices_ParseString(ClipboardData)

        e.Handled = True

      End If

    Catch ex As Exception
    Finally
      Me.Cursor = Cursors.Default
    End Try

	End Sub

	Private Sub FormatNumericGridColumn(ByVal pColumnID As Integer)
		' ****************************************************************************************
		'
		'
		' ****************************************************************************************
		Dim RowCount As Integer

		Try
			For RowCount = 0 To (Grid_Prices.Rows.Count - 1)
				If (Not Grid_Prices.Rows(RowCount).IsNewRow) Then
					If IsNumeric(Grid_Prices.Item(pColumnID, RowCount).Value) Then
						If CDbl(Grid_Prices.Item(pColumnID, RowCount).Value) < 0 Then
							Grid_Prices.Item(pColumnID, RowCount).Style.ForeColor = Color.Red
						Else
							If Grid_Prices.Item(pColumnID, RowCount).Style.ForeColor <> Grid_Prices.Columns(pColumnID).DefaultCellStyle.ForeColor Then
								Grid_Prices.Item(pColumnID, RowCount).Style.ForeColor = Grid_Prices.Columns(pColumnID).DefaultCellStyle.ForeColor
							End If
						End If

					End If
				End If
			Next
		Catch ex As Exception
		End Try

	End Sub

#End Region

#Region " Bug hunting "

	' AddHandler myAdaptor.RowUpdating, AddressOf OnRowUpdating
	' AddHandler myAdaptor.RowUpdated, AddressOf OnRowUpdated
	' AddHandler myAdaptor.FillError, AddressOf OnRowFillError

	Protected Shared Sub OnRowUpdating(ByVal Sender As Object, ByVal e As SqlRowUpdatingEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

	Protected Shared Sub OnRowUpdated(ByVal Sender As Object, ByVal e As SqlRowUpdatedEventArgs)
		Dim a As Integer

		If Not e.Command Is Nothing Then
			a = 1
		End If

		If Not e.Errors Is Nothing Then
			a = 2
		End If
	End Sub

	Protected Shared Sub OnRowFillError(ByVal Sender As Object, ByVal e As FillErrorEventArgs)
		Dim a As Integer

		a = 1
	End Sub

#End Region





	Private Sub Button_RecalcNAVs_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button_RecalcNAVs.Click
		' ****************************************************************
		'
		'
		' ****************************************************************

		Try
			Dim GridCounter As Integer
			Dim ReturnOrdinal As Integer = Grid_Prices.Columns("PerformanceReturn").Index
			Dim NAVOrdinal As Integer = Grid_Prices.Columns("NAV").Index
			Dim NAVsPresent As Boolean = False

			For GridCounter = 1 To (Grid_Prices.Rows.Count - 2)
				If IsNumeric(Grid_Prices.Item(NAVOrdinal, GridCounter - 1).Value) AndAlso (IsNumeric(Grid_Prices.Item(ReturnOrdinal, GridCounter).Value)) AndAlso (CDbl(Grid_Prices.Item(NAVOrdinal, GridCounter - 1).Value) <> 0) Then
					Grid_Prices.Item(NAVOrdinal, GridCounter).Value = CDbl(Grid_Prices.Item(NAVOrdinal, GridCounter - 1).Value) * (CDbl(Grid_Prices.Item(ReturnOrdinal, GridCounter).Value) + 1.0#)
					NAVsPresent = True
				End If
			Next
			For GridCounter = (Grid_Prices.Rows.Count - 2) To 1 Step -1
				If IsNumeric(Grid_Prices.Item(NAVOrdinal, GridCounter).Value) AndAlso (IsNumeric(Grid_Prices.Item(ReturnOrdinal, GridCounter).Value)) AndAlso (CDbl(Grid_Prices.Item(NAVOrdinal, GridCounter).Value) <> 0) Then
					Grid_Prices.Item(NAVOrdinal, GridCounter - 1).Value = CDbl(Grid_Prices.Item(NAVOrdinal, GridCounter).Value) / (CDbl(Grid_Prices.Item(ReturnOrdinal, GridCounter).Value) + 1.0#)
					NAVsPresent = True
				End If
			Next

			If (Not NAVsPresent) Then
				Grid_Prices.Item(NAVOrdinal, 0).Value = 100
				Call Button_RecalcNAVs_Click(Nothing, Nothing)
			End If
		Catch ex As Exception
		End Try

	End Sub

End Class
