Imports System.Data.SqlClient
Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass
Imports RenaissancePertracDataClass
Imports RenaissanceUtilities.DatePeriodFunctions
Imports RenaissanceStatFunctions

' Imports Genoa.DatePeriodFunctions

Public Class CustomFieldCalculationClass

	' *************************************************************************************************
	' Class to calculate Custom Field Data items.
	'
	' Given a combination of Custom Field ID, Group ID and Pertrac ID this class will calculate ALL or just
	' outdated Custom field data items.
	'
	' The Process flow is as follows :
	'		1) Initialise the process with a call to 'Initialise_CalculateFields()'. This will establish a 
	'		Database connection, discover what fields need to be calculated, and prepare any internal data 
	'		structures.
	'
	'		2) While 'MoreFieldsToProcess()' is True, Repeatedly call 'Process_CalculateFields()' to calculate
	'		batches of Custom Data Fields. The results are returned as a DataTable and the process is completed
	'		in a series of batches so that the results can be saved a bit at a time, rather than as one huge update
	'		when the process is completed.
	'		This approach also makes Cancelling the process easier.
	'
	'		3) Once all fields are calculated, tidy up with a call to 'Finish_CalculateFields()'
	'
	'		The calculation process can be interrupted by calling 'CancelOperation()'. The class will be reset by a
	'		subsequent call to 'Initialise_CalculateFields()'
	'
	' This process has been designed as a largely stand alone class to facilitate incorperation in a
	' 'Modena' calculation engine at a later date.
	'
	' *************************************************************************************************


#Region " Interfaces"

	Public Interface ICustomFieldCalculation
		' *************************************************************************************************
		' Interface detailing services required from the parent object.
		' *************************************************************************************************

		Function GetAsyncConnection() As SqlConnection
		Function PertracDataObject() As PertracDataClass
		Function LookupTableRow(ByVal StdTableDef As RenaissanceGlobals.StandardDataset, ByVal ThisAuditID As Integer, Optional ByVal ReturnField As String = "", Optional ByVal SortString As String = "RN", Optional ByVal SearchString As String = "") As DataRow

		ReadOnly Property StatFunctions() As StatFunctions

	End Interface

#End Region

#Region " Class Locals / Globals"

	' Number of Custom Field Data Items calculated during each call to 'Process_CalculateFields()'
	Public Const DEFAULT_CALCULATION_BITE As Integer = 100

	Public Const KNOWLEDGEDATE_NOW As Date = #1/1/1900#
	Public Const DEFAULT_SQLCOMMAND_TIMEOUT As Integer = 600

	' Risk Free rate used to calculate Sharpe Ratio.
	' It is envisaged that this constant will eventually be replaced with a Risk-Free rate series.
	Public Const SHARPE_RiskFreeRate As Double = 0.03


	Private _InstanceLock As Object
	Private _ObjectIsBusy As Boolean

	Private _MainForm As ICustomFieldCalculation
	Private _CancelFlag As Boolean

	Private CustomFieldCommand As SqlCommand
	Private CustomStatusReader As SqlDataReader

#End Region

#Region " Class Properties"

	Public ReadOnly Property CancelFlag() As Boolean
		Get
			Return _CancelFlag
		End Get
	End Property

	Public ReadOnly Property IsBusy() As Boolean
		Get
			Return _ObjectIsBusy
		End Get
	End Property

	Public ReadOnly Property MoreFieldsToProcess() As Boolean
		Get
			Try
				If (CustomStatusReader IsNot Nothing) AndAlso (CustomStatusReader.HasRows) Then
					Return True
				End If
			Catch ex As Exception
			End Try

			Return False
		End Get
	End Property

#End Region

#Region " Methods"

	Public Sub CancelOperation()
		_CancelFlag = True
	End Sub

#End Region

#Region " Constructors / Destructors."

	Private Sub New()
		' Insist on using the parameterised constructor

	End Sub

	Public Sub New(ByRef pGenoaMainForm As GenoaMain)
		' *************************************************************************************************
		' Initialise a new instance of the Calculation Class.
		'
		' *************************************************************************************************

		If (pGenoaMainForm Is Nothing) Then
			_MainForm = Nothing
			Exit Sub
		End If

		_MainForm = pGenoaMainForm
		_CancelFlag = False

		_InstanceLock = New Object
		_ObjectIsBusy = False
		InitialiseComplete = False

		CustomStatusReader = Nothing

	End Sub

	Public Sub Finalise()
		' *************************************************************************************************
		' Tidy up.
		'
		' Clear and Close SQL connections.
		' *************************************************************************************************

		ClearConnectionObjects()
		_MainForm = Nothing

	End Sub

	Private Sub ClearConnectionObjects()
		' *************************************************************************************************
		' 
		' *************************************************************************************************

		InitialiseComplete = False

		Try

			If (CustomStatusReader IsNot Nothing) Then
				Try
					If (CustomStatusReader.HasRows) Then
						While (CustomStatusReader.Read)
						End While
					End If
					CustomStatusReader.Close()
				Catch ex As Exception
				Finally
					CustomStatusReader = Nothing
				End Try
			End If

		Catch ex As Exception
		End Try

		Try
			If (CustomFieldCommand IsNot Nothing) Then
				If (CustomFieldCommand.Connection IsNot Nothing) Then
					Try
						CustomFieldCommand.Connection.Close()
					Catch ex As Exception
					End Try
				End If

				CustomFieldCommand.Connection = Nothing

			End If
		Catch ex As Exception
		Finally
			CustomFieldCommand = Nothing
		End Try

	End Sub

#End Region

#Region " Control Functions"

	' _CancelFlag = false

	Private InitialiseComplete As Boolean
	Private CurrentDataReader As Boolean
	Private _CustomFieldID As Integer
	Private _GroupID As Integer
	Private _PertracID As Integer
	Private _ForceRecalc As Boolean

	Public Sub Initialise_CalculateFields(ByVal pCustomFieldID As Integer, ByVal pGroupID As Integer, ByVal pPertracID As Integer, Optional ByVal pForceRecalc As Boolean = False)
		' *************************************************************************************************
		' 
		' 
		' *************************************************************************************************

		Try
			If (_MainForm Is Nothing) Then
				Exit Sub
			End If

			_ObjectIsBusy = True
			SyncLock _InstanceLock

				' Initialise

				_CustomFieldID = pCustomFieldID
				_GroupID = pGroupID
				_PertracID = pPertracID
				_ForceRecalc = pForceRecalc

				_CancelFlag = False
				InitialiseComplete = False

				' Flush Existing Reader if it exists

				ClearConnectionObjects()

				' Get Database Reader returning selected permutations of Custom Data items.

				CustomFieldCommand = New SqlCommand
				Dim result As IAsyncResult = Nothing

				Try
					CustomFieldCommand.CommandType = CommandType.StoredProcedure
					CustomFieldCommand.CommandText = "spu_PertracCustomFieldData_CalculationQuery"
					CustomFieldCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = _GroupID
					CustomFieldCommand.Parameters.Add("@CustomFieldID", SqlDbType.Int).Value = _CustomFieldID
					CustomFieldCommand.Parameters.Add("@PertracID", SqlDbType.Int).Value = _PertracID
					CustomFieldCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = KNOWLEDGEDATE_NOW

					CustomFieldCommand.Connection = _MainForm.GetAsyncConnection
					CustomFieldCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

					Try
						result = CustomFieldCommand.BeginExecuteReader()
						While Not result.IsCompleted
							If (Me.CancelFlag) Then
								CustomFieldCommand.Cancel()
								Exit While
							End If

							Threading.Thread.Sleep(100)
						End While
					Catch ex As Exception
					Finally
						If (result IsNot Nothing) Then
							Try
								CustomStatusReader = CustomFieldCommand.EndExecuteReader(result)
								InitialiseComplete = True
							Catch ex As Exception
							End Try
						End If

						If (Me.CancelFlag) Then
							ClearConnectionObjects()
						End If
					End Try

				Catch ex As Exception
					ClearConnectionObjects()
				End Try

				' Exit Initialisation

			End SyncLock
		Catch ex As Exception
		Finally
			_ObjectIsBusy = False
		End Try

	End Sub

  Public Function Process_CalculateFields(ByVal StatsDatePeriod As DealingPeriod, ByVal pRecordCount As Integer) As RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataDataTable
    ' Optional ByVal pRecordCount As Integer = DEFAULT_CALCULATION_BITE
    ' *****************************************************************************************
    ' Calculate and return the next batch of Custom Field Data Items while there are still items to 
    ' calculate.
    '
    ' Return value is a data table of Custom Field Data Items.
    ' *****************************************************************************************

    Dim RVal As RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataDataTable = Nothing
    Dim LimitRecordCount As Integer = pRecordCount

    Dim thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow = Nothing
    Dim ThisNewRow As RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataRow

    Dim ThisCustomFieldID As Integer
    Dim ThisCustomFieldID_Ordinal As Integer
    Dim ThisPertracID As Integer
    Dim ThisPertracID_Ordinal As Integer
    Dim ThisUpdateRequired As Boolean
    Dim ThisUpdateRequired_Ordinal As Integer

    Dim DateArray() As Date

    Try
      ' Check Status.

      If (_MainForm Is Nothing) Then
        Return RVal
      End If

      If (InitialiseComplete = False) OrElse (CustomStatusReader Is Nothing) OrElse (CustomStatusReader.HasRows = False) Then
        Return RVal
      End If

      If (Me.CancelFlag) Then
        Return RVal
      End If

      ' Initialise Return Table.

      RVal = New RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataDataTable

      ' Process Batch of Custom Fields.

      _ObjectIsBusy = True
      SyncLock _InstanceLock
        If (LimitRecordCount < 1) Then
          LimitRecordCount = 1
        End If

        Try
          ' For Performance uplift, retrieve ordinals for required fields.

          ThisCustomFieldID_Ordinal = CustomStatusReader.GetOrdinal("CustomFieldID")
          ThisPertracID_Ordinal = CustomStatusReader.GetOrdinal("PertracID")
          ThisUpdateRequired_Ordinal = CustomStatusReader.GetOrdinal("UpdateRequired")

        Catch ex As Exception
          ClearConnectionObjects()
          Return RVal
        End Try

        ' Process Batch of Custom Fields.

        While (RVal.Rows.Count < LimitRecordCount)

          ' Check cancel flag.

          If (Me.CancelFlag) Then
            ClearConnectionObjects()
            Return RVal
          End If

          ' Loop...

          If (CustomStatusReader.Read) Then

            ' Get Custom Field Data Item details.

            Try
              ThisCustomFieldID = CInt(CustomStatusReader.Item(ThisCustomFieldID_Ordinal))
              ThisPertracID = CInt(CustomStatusReader.Item(ThisPertracID_Ordinal))
              ThisUpdateRequired = CBool(CustomStatusReader.Item(ThisUpdateRequired_Ordinal))

            Catch ex As Exception
              ClearConnectionObjects()
              Return RVal
            End Try

            ' Get Custom Field definition.

            thisCustomFieldDefinition = _MainForm.LookupTableRow(RenaissanceStandardDatasets.tblPertracCustomFields, ThisCustomFieldID, "")

            ' If Update is required and the Field definition appears OK...

            If ((_ForceRecalc) Or (ThisUpdateRequired)) AndAlso (thisCustomFieldDefinition IsNot Nothing) Then

              ' Create New Field Data Row

              Try
                ThisNewRow = RVal.NewtblPertracCustomFieldDataRow

                ThisNewRow.FieldDataID = (-1) ' Force the Item to save
                ThisNewRow.CustomFieldID = ThisCustomFieldID
                ThisNewRow.PertracID = ThisPertracID
                ThisNewRow.GroupID = 0
                ThisNewRow.FieldNumericData = 0
                ThisNewRow.FieldTextData = ""
                ThisNewRow.FieldDateData = KNOWLEDGEDATE_NOW
                ThisNewRow.FieldBooleanData = False
                ThisNewRow.UpdateRequired = False
                ThisNewRow.LatestReturnDate = KNOWLEDGEDATE_NOW
              Catch ex As Exception
                ClearConnectionObjects()
                Return RVal
              End Try

              ' OK, Process Field Data

              Try

                Select Case CType(thisCustomFieldDefinition.CustomFieldType, PertracCustomFieldTypes)

                  Case PertracCustomFieldTypes.ReturnDate
                    CFCalc_ReturnDate(StatsDatePeriod, ThisNewRow, thisCustomFieldDefinition, ThisPertracID)

                  Case PertracCustomFieldTypes.SingleReturn
                    CFCalc_SingleReturn(StatsDatePeriod, ThisNewRow, thisCustomFieldDefinition, ThisPertracID)

                  Case PertracCustomFieldTypes.PeriodReturn
                    CFCalc_PeriodReturn(StatsDatePeriod, ThisNewRow, thisCustomFieldDefinition, ThisPertracID)

                  Case PertracCustomFieldTypes.DrawDown
                    CFCalc_DrawDown(StatsDatePeriod, ThisNewRow, thisCustomFieldDefinition, ThisPertracID)

                  Case PertracCustomFieldTypes.DrawUp
                    CFCalc_DrawUp(StatsDatePeriod, ThisNewRow, thisCustomFieldDefinition, ThisPertracID)

                  Case PertracCustomFieldTypes.SharpeRatio
                    CFCalc_SharpeRatio(StatsDatePeriod, ThisNewRow, thisCustomFieldDefinition, ThisPertracID)

                  Case PertracCustomFieldTypes.StandardDeviation
                    CFCalc_StDev(StatsDatePeriod, ThisNewRow, thisCustomFieldDefinition, ThisPertracID, False)

                  Case PertracCustomFieldTypes.Volatility
                    CFCalc_StDev(StatsDatePeriod, ThisNewRow, thisCustomFieldDefinition, ThisPertracID, True)

                  Case PertracCustomFieldTypes.Correlation

                  Case PertracCustomFieldTypes.Alpha

                  Case PertracCustomFieldTypes.Beta


                End Select

              Catch ex As Exception
              End Try

              ' Set LatestReturnDate value - Essential in subsequently evaluation Update status of the Data item.

              Try
                DateArray = _MainForm.StatFunctions.DateSeries(StatsDatePeriod, CULng(ThisPertracID), False, _MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), 1, False, Renaissance_BaseDate, Renaissance_EndDate_Data)
                If (DateArray IsNot Nothing) AndAlso (DateArray.Length > 0) Then
                  ThisNewRow.LatestReturnDate = FitDateToPeriod(StatsDatePeriod, DateArray(DateArray.Length - 1), True)
                End If
                DateArray = Nothing
              Catch ex As Exception
              End Try

              ' Add Data Field Item to the results set.

              Try
                If (ThisNewRow.RowState And DataRowState.Detached) Then
                  RVal.Rows.Add(ThisNewRow)
                End If
              Catch ex As Exception
              End Try

            End If

          Else
            ClearConnectionObjects()
            Exit While
          End If
        End While

      End SyncLock
    Catch ex As Exception
    Finally
      _ObjectIsBusy = False
    End Try

    ' Return.

    Return RVal

  End Function

	Public Sub Finish_CalculateFields()
		' *****************************************************************************************
		' Tidy Up data structures and DB connections.
		'
		' *****************************************************************************************

		Try
			If (_MainForm Is Nothing) Then
				Exit Sub
			End If

			_ObjectIsBusy = True
			SyncLock _InstanceLock

				ClearConnectionObjects()

			End SyncLock
		Catch ex As Exception
		Finally
			_ObjectIsBusy = False
		End Try

	End Sub


#End Region

#Region " Calculation Functions."

  Private Sub CFCalc_ReturnDate(ByVal StatsDatePeriod As DealingPeriod, ByRef ThisNewRow As RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataRow, ByRef thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow, ByVal ThisPertracID As Integer)
    ' *************************************************************************************************
    '
    '
    '
    ' *************************************************************************************************

    Dim DataTable As DataTable = Nothing
    Dim SortedRows(-1) As DataRow

    Try
      If ThisPertracID <= 0 Then
        DataTable = _MainForm.PertracDataObject.GetPertracTable(StatsDatePeriod, 0)
      Else
        DataTable = _MainForm.PertracDataObject.GetPertracTable(StatsDatePeriod, ThisPertracID)
      End If
      SortedRows = DataTable.Select("(PerformanceDate >= #" & thisCustomFieldDefinition.StartDate.ToString(QUERY_SHORTDATEFORMAT) & "#) AND (PerformanceDate <= #" & thisCustomFieldDefinition.EndDate.ToString(QUERY_SHORTDATEFORMAT) & "#)", "PerformanceDate")

      If (SortedRows Is Nothing) OrElse (SortedRows.Length <= 0) Then
        ThisNewRow.FieldDateData = KNOWLEDGEDATE_NOW
      Else

        If (thisCustomFieldDefinition.IsFirstValue) OrElse (thisCustomFieldDefinition.IsMinValue) Then
          ThisNewRow.FieldDateData = SortedRows(0)("PerformanceDate")
        ElseIf (thisCustomFieldDefinition.IsAverageValue) Then
          ' Use the Middle Date as the Average.
          ThisNewRow.FieldDateData = SortedRows(CInt((SortedRows.Length - 0.5) / 2))("PerformanceDate")
        Else ' Assume MAX / Last
          ThisNewRow.FieldDateData = SortedRows(SortedRows.Length - 1)("PerformanceDate")
        End If
      End If

      Try
        DataTable = Nothing
      Catch ex As Exception
      End Try

    Catch ex As Exception

    End Try

  End Sub

  Private Sub CFCalc_SingleReturn(ByVal StatsDatePeriod As DealingPeriod, ByRef ThisNewRow As RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataRow, ByRef thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow, ByVal ThisPertracID As Integer)
    ' *************************************************************************************************
    '
    '
    '
    ' *************************************************************************************************

    Dim ReturnArray() As Double
    Dim DateArray() As Date
    Dim StartingIndex As Integer = 0

    Try

      ReturnArray = _MainForm.StatFunctions.ReturnSeries(StatsDatePeriod, CULng(ThisPertracID), False, _MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), 1, False, thisCustomFieldDefinition.StartDate, thisCustomFieldDefinition.EndDate)
      DateArray = _MainForm.StatFunctions.DateSeries(StatsDatePeriod, CULng(ThisPertracID), False, _MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), 1, False, Renaissance_BaseDate, Renaissance_EndDate_Data)

      If (ReturnArray Is Nothing) OrElse (ReturnArray.Length <= 0) Then
        ThisNewRow.FieldNumericData = 0
      Else
        ' If all instrument data is returned, the first Return is Zero in order to provide room for the initial NAV (100) item.
        ' Thus we want to try to avoid this data item for the Single Return field.

        If (FitDateToPeriod(StatsDatePeriod, thisCustomFieldDefinition.StartDate, False) <= FitDateToPeriod(StatsDatePeriod, DateArray(0), False)) Then
          StartingIndex = 1
        End If

        If (thisCustomFieldDefinition.IsFirstValue) Then
          ThisNewRow.FieldNumericData = ReturnArray(0)
        ElseIf (thisCustomFieldDefinition.IsMinValue) Then
          ThisNewRow.FieldNumericData = GetMinValue(ReturnArray, StartingIndex)
        ElseIf (thisCustomFieldDefinition.IsAverageValue) Then
          ThisNewRow.FieldNumericData = GetAverageValue(ReturnArray, StartingIndex)
        ElseIf (thisCustomFieldDefinition.IsMaxValue) Then
          ThisNewRow.FieldNumericData = GetMaxValue(ReturnArray, StartingIndex)
        Else ' Assume Last
          ThisNewRow.FieldNumericData = ReturnArray(ReturnArray.Length - 1)
        End If
      End If

    Catch ex As Exception

    End Try

  End Sub

  Private Sub CFCalc_PeriodReturn(ByVal StatsDatePeriod As DealingPeriod, ByRef ThisNewRow As RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataRow, ByRef thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow, ByVal ThisPertracID As Integer)
    ' *************************************************************************************************
    '
    '
    '
    ' *************************************************************************************************

    Dim ReturnArray() As Double
    Dim TempReturnArray() As Double
    Dim ValidStart As Integer = 0
    Dim PeriodCount As Integer
    Dim IsIRR As Boolean = thisCustomFieldDefinition.IsIRR

    Try
      PeriodCount = thisCustomFieldDefinition.FieldPeriodCount
      If (PeriodCount <= 0) Then
        ' If PeriodCount = 0 then assume ITD
        PeriodCount = Integer.MaxValue
      End If

      TempReturnArray = _MainForm.StatFunctions.PeriodReturnSeries(StatsDatePeriod, CULng(ThisPertracID), False, thisCustomFieldDefinition.FieldPeriodCount, 1, False, thisCustomFieldDefinition.StartDate, thisCustomFieldDefinition.EndDate)
      If (TempReturnArray Is Nothing) OrElse (TempReturnArray.Length <= 0) Then
        Exit Sub
      End If
      If (PeriodCount >= TempReturnArray.Length) Then
        PeriodCount = (TempReturnArray.Length - 1)
      End If

      While (ValidStart < TempReturnArray.Length) AndAlso (Double.IsNaN(TempReturnArray(ValidStart)))
        ValidStart += 1
      End While

      If (ValidStart >= TempReturnArray.Length) Then
        Exit Sub
      End If

      ReDim ReturnArray((TempReturnArray.Length - ValidStart) - 1)
      Array.ConstrainedCopy(TempReturnArray, ValidStart, ReturnArray, 0, (TempReturnArray.Length - ValidStart))

      If (ReturnArray Is Nothing) OrElse (ReturnArray.Length <= 0) Then
        ThisNewRow.FieldNumericData = 0
      Else
        If (thisCustomFieldDefinition.IsFirstValue) Then
          ThisNewRow.FieldNumericData = ReturnArray(0)
        ElseIf (thisCustomFieldDefinition.IsMinValue) Then
          ThisNewRow.FieldNumericData = GetMinValue(ReturnArray)
        ElseIf (thisCustomFieldDefinition.IsAverageValue) Then
          ThisNewRow.FieldNumericData = GetAverageValue(ReturnArray)
        ElseIf (thisCustomFieldDefinition.IsMaxValue) Then
          ThisNewRow.FieldNumericData = GetMaxValue(ReturnArray)
        Else ' Assume Last
          ThisNewRow.FieldNumericData = ReturnArray(ReturnArray.Length - 1)
        End If
      End If

      If (IsIRR) Then
        ThisNewRow.FieldNumericData = ((1 + ThisNewRow.FieldNumericData) ^ (_MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod) / PeriodCount)) - 1
      End If

    Catch ex As Exception
    End Try

  End Sub

  Private Sub CFCalc_DrawDown(ByVal StatsDatePeriod As DealingPeriod, ByRef ThisNewRow As RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataRow, ByRef thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow, ByVal ThisPertracID As Integer)
    ' *************************************************************************************************
    '
    '
    '
    ' *************************************************************************************************

    Dim Drawdowns() As StatFunctions.DrawDownInstanceClass
    Dim DrawDownPercent() As Double
    Dim DrawDownCounter As Integer
    Dim DrawDownDate As Date
    Dim DrawDownIndex As Integer

    Try

      Drawdowns = _MainForm.StatFunctions.GetDrawDownDetails(StatsDatePeriod, CULng(ThisPertracID), False, thisCustomFieldDefinition.StartDate, thisCustomFieldDefinition.EndDate, 1.0#)

      If (Drawdowns Is Nothing) OrElse (Drawdowns.Length <= 0) Then
        ThisNewRow.FieldNumericData = 0
      Else

        If (thisCustomFieldDefinition.IsFirstValue) Then
          DrawDownDate = Drawdowns(0).DateFrom
          DrawDownIndex = 0

          For DrawDownCounter = 1 To (Drawdowns.Length - 1)
            If (DrawDownDate > Drawdowns(0).DateFrom) Then
              DrawDownDate = Drawdowns(DrawDownCounter).DateFrom
              DrawDownIndex = DrawDownCounter
            End If
          Next

          ThisNewRow.FieldNumericData = Drawdowns(DrawDownIndex).DrawDown
        ElseIf (thisCustomFieldDefinition.IsMinValue) Then
          ThisNewRow.FieldNumericData = Drawdowns(Drawdowns.Length - 1).DrawDown
        ElseIf (thisCustomFieldDefinition.IsAverageValue) Then
          ReDim DrawDownPercent(Drawdowns.Length - 1)
          For DrawDownCounter = 0 To (Drawdowns.Length - 1)
            DrawDownPercent(DrawDownCounter) = Drawdowns(DrawDownCounter).DrawDown
          Next

          ThisNewRow.FieldNumericData = GetAverageValue(DrawDownPercent)
        ElseIf (thisCustomFieldDefinition.IsMaxValue) Then
          ThisNewRow.FieldNumericData = Drawdowns(0).DrawDown
        Else ' Assume Last
          DrawDownDate = Drawdowns(0).DateFrom
          DrawDownIndex = 0

          For DrawDownCounter = 1 To (Drawdowns.Length - 1)
            If (DrawDownDate < Drawdowns(0).DateFrom) Then
              DrawDownDate = Drawdowns(DrawDownCounter).DateFrom
              DrawDownIndex = DrawDownCounter
            End If
          Next

          ThisNewRow.FieldNumericData = Drawdowns(DrawDownIndex).DrawDown
        End If
      End If

    Catch ex As Exception

    End Try

  End Sub

  Private Sub CFCalc_DrawUp(ByVal StatsDatePeriod As DealingPeriod, ByRef ThisNewRow As RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataRow, ByRef thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow, ByVal ThisPertracID As Integer)
    ' *************************************************************************************************
    '
    '
    '
    ' *************************************************************************************************

    Dim DrawUps() As StatFunctions.DrawDownInstanceClass
    Dim DrawUpPercent() As Double
    Dim DrawUpCounter As Integer
    Dim DrawUpDate As Date
    Dim DrawUpIndex As Integer

    Try

      DrawUps = _MainForm.StatFunctions.GetDrawUpDetails(StatsDatePeriod, CULng(ThisPertracID), False, thisCustomFieldDefinition.StartDate, thisCustomFieldDefinition.EndDate, 1.0#)

      If (DrawUps Is Nothing) OrElse (DrawUps.Length <= 0) Then
        ThisNewRow.FieldNumericData = 0
      Else

        If (thisCustomFieldDefinition.IsFirstValue) Then
          DrawUpDate = DrawUps(0).DateFrom
          DrawUpIndex = 0

          For DrawUpCounter = 1 To (DrawUps.Length - 1)
            If (DrawUpDate > DrawUps(0).DateFrom) Then
              DrawUpDate = DrawUps(DrawUpCounter).DateFrom
              DrawUpIndex = DrawUpCounter
            End If
          Next

          ThisNewRow.FieldNumericData = DrawUps(DrawUpIndex).DrawUp
        ElseIf (thisCustomFieldDefinition.IsMinValue) Then
          ThisNewRow.FieldNumericData = DrawUps(DrawUps.Length - 1).DrawUp
        ElseIf (thisCustomFieldDefinition.IsAverageValue) Then
          ReDim DrawUpPercent(DrawUps.Length - 1)
          For DrawUpCounter = 0 To (DrawUps.Length - 1)
            DrawUpPercent(DrawUpCounter) = DrawUps(DrawUpCounter).DrawUp
          Next

          ThisNewRow.FieldNumericData = GetAverageValue(DrawUpPercent)
        ElseIf (thisCustomFieldDefinition.IsMaxValue) Then
          ThisNewRow.FieldNumericData = DrawUps(0).DrawUp
        Else ' Assume Last
          DrawUpDate = DrawUps(0).DateFrom
          DrawUpIndex = 0

          For DrawUpCounter = 1 To (DrawUps.Length - 1)
            If (DrawUpDate < DrawUps(0).DateFrom) Then
              DrawUpDate = DrawUps(DrawUpCounter).DateFrom
              DrawUpIndex = DrawUpCounter
            End If
          Next

          ThisNewRow.FieldNumericData = DrawUps(DrawUpIndex).DrawUp
        End If
      End If

    Catch ex As Exception

    End Try

  End Sub

  Private Sub CFCalc_SharpeRatio(ByVal StatsDatePeriod As DealingPeriod, ByRef ThisNewRow As RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataRow, ByRef thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow, ByVal ThisPertracID As Integer)
    ' *************************************************************************************************
    '
    '
    '
    ' *************************************************************************************************
    Dim VolArray() As Double
    Dim PeriodReturn() As Double
    Dim DateArray() As Date
    Dim SharpeArray() As Double
    Dim IndexCounter As Integer

    Dim ThisPeriodCount As Integer

    Try
      ThisPeriodCount = thisCustomFieldDefinition.FieldPeriodCount

      If (ThisPeriodCount <= 0) Then
        DateArray = _MainForm.StatFunctions.DateSeries(StatsDatePeriod, CULng(ThisPertracID), False, _MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), 1, False, Renaissance_BaseDate, Renaissance_EndDate_Data)

        If (DateArray Is Nothing) OrElse (DateArray.Length <= 0) Then
          Exit Sub
        End If

        ThisPeriodCount = GetPeriodCount(StatsDatePeriod, DateArray(0), DateArray(DateArray.Length - 1)) - 1

        If (ThisPeriodCount <= 0) Then
          Exit Sub
        End If

      End If

      VolArray = _MainForm.StatFunctions.StdDevSeries(StatsDatePeriod, CULng(ThisPertracID), False, ThisPeriodCount, 1, True, Renaissance_BaseDate, Renaissance_EndDate_Data)
      DateArray = _MainForm.StatFunctions.DateSeries(StatsDatePeriod, CULng(ThisPertracID), False, ThisPeriodCount, 1, False, Renaissance_BaseDate, Renaissance_EndDate_Data)
      PeriodReturn = _MainForm.StatFunctions.PeriodReturnSeries(StatsDatePeriod, CULng(ThisPertracID), False, ThisPeriodCount, 1, False, Renaissance_BaseDate, Renaissance_EndDate_Data)

      Dim WorkingStartDate As Date
      Dim WorkingStartIndex As Integer
      Dim WorkingEndDate As Date
      Dim WorkingEndIndex As Integer

      WorkingStartDate = FitDateToPeriod(StatsDatePeriod, DateArray(ThisPeriodCount), True)
      WorkingStartIndex = ThisPeriodCount
      WorkingEndIndex = DateArray.Length - 1
      WorkingEndDate = FitDateToPeriod(StatsDatePeriod, DateArray(WorkingEndIndex), True)

      If (FitDateToPeriod(StatsDatePeriod, thisCustomFieldDefinition.StartDate, True) > WorkingStartDate) Then
        WorkingStartDate = FitDateToPeriod(StatsDatePeriod, thisCustomFieldDefinition.StartDate, True)
        WorkingStartIndex = GetPriceIndex(StatsDatePeriod, DateArray(0), WorkingStartDate)
      End If

      If (FitDateToPeriod(StatsDatePeriod, thisCustomFieldDefinition.EndDate, True) < WorkingEndDate) Then
        WorkingEndDate = FitDateToPeriod(StatsDatePeriod, thisCustomFieldDefinition.EndDate, True)
        WorkingEndIndex = GetPriceIndex(StatsDatePeriod, DateArray(0), WorkingStartDate)
      End If

      If (WorkingEndDate < WorkingStartDate) Then
        Exit Sub
      End If

      '        RVal.SharpeRatio.M12 = (RVal.AnnualisedReturn.M12 - pRiskFreeRate) / RVal.Volatility.M12

      ReDim SharpeArray(WorkingEndIndex - WorkingStartIndex)
      For IndexCounter = WorkingStartIndex To WorkingEndIndex
        Try
          If (VolArray(IndexCounter) <> 0) AndAlso (Double.IsNaN(PeriodReturn(IndexCounter)) = False) Then
            SharpeArray(IndexCounter - WorkingStartIndex) = (PeriodReturn(IndexCounter) - SHARPE_RiskFreeRate) / VolArray(IndexCounter)
          Else
            SharpeArray(IndexCounter - WorkingStartIndex) = Double.NaN
          End If
        Catch ex As Exception
        End Try
      Next

      If (SharpeArray Is Nothing) OrElse (SharpeArray.Length <= 0) Then
        ThisNewRow.FieldNumericData = 0
      Else
        If (thisCustomFieldDefinition.IsFirstValue) Then
          ThisNewRow.FieldNumericData = SharpeArray(0)
        ElseIf (thisCustomFieldDefinition.IsMinValue) Then
          ThisNewRow.FieldNumericData = GetMinValue(SharpeArray)
        ElseIf (thisCustomFieldDefinition.IsAverageValue) Then
          ThisNewRow.FieldNumericData = GetAverageValue(SharpeArray)
        ElseIf (thisCustomFieldDefinition.IsMaxValue) Then
          ThisNewRow.FieldNumericData = GetMaxValue(SharpeArray)
        Else ' Assume Last
          ThisNewRow.FieldNumericData = SharpeArray(SharpeArray.Length - 1)
        End If
      End If

      If Double.IsNaN(ThisNewRow.FieldNumericData) Then
        ThisNewRow.FieldNumericData = 0
      End If

    Catch ex As Exception

    End Try

  End Sub

  Private Sub CFCalc_StDev(ByVal StatsDatePeriod As DealingPeriod, ByRef ThisNewRow As RenaissanceDataClass.DSPertracCustomFieldData.tblPertracCustomFieldDataRow, ByRef thisCustomFieldDefinition As RenaissanceDataClass.DSPertracCustomFields.tblPertracCustomFieldsRow, ByVal ThisPertracID As Integer, ByVal Annualised As Boolean)
    ' *************************************************************************************************
    '
    '
    '
    ' *************************************************************************************************


    Dim VolArray() As Double
    Dim DateArray() As Date
    Dim WorkingStartDate As Date
    Dim WorkingStartIndex As Integer
    Dim WorkingEndDate As Date
    Dim WorkingEndIndex As Integer

    Dim ThisPeriodCount As Integer

    Try
      ThisPeriodCount = thisCustomFieldDefinition.FieldPeriodCount

      If (ThisPeriodCount <= 0) Then
        DateArray = _MainForm.StatFunctions.DateSeries(StatsDatePeriod, CULng(ThisPertracID), False, _MainForm.StatFunctions.AnnualPeriodCount(StatsDatePeriod), 1, False, Renaissance_BaseDate, Renaissance_EndDate_Data)

        If (DateArray Is Nothing) OrElse (DateArray.Length <= 0) Then
          Exit Sub
        End If

        ThisPeriodCount = GetPeriodCount(StatsDatePeriod, DateArray(0), DateArray(DateArray.Length - 1)) - 1

        If (ThisPeriodCount <= 0) Then
          Exit Sub
        End If

      End If

      ' Sore out start date stuff.

      VolArray = _MainForm.StatFunctions.StdDevSeries(StatsDatePeriod, CULng(ThisPertracID), False, ThisPeriodCount, 1, Annualised, Renaissance_BaseDate, Renaissance_EndDate_Data)
      DateArray = _MainForm.StatFunctions.DateSeries(StatsDatePeriod, CULng(ThisPertracID), False, ThisPeriodCount, 1, False, Renaissance_BaseDate, Renaissance_EndDate_Data)

      If (DateArray.Length <= ThisPeriodCount) Then
        Exit Sub
      End If

      WorkingStartDate = FitDateToPeriod(StatsDatePeriod, DateArray(ThisPeriodCount), True)
      WorkingStartIndex = ThisPeriodCount
      WorkingEndIndex = DateArray.Length - 1
      WorkingEndDate = FitDateToPeriod(StatsDatePeriod, DateArray(WorkingEndIndex), True)

      If (FitDateToPeriod(StatsDatePeriod, thisCustomFieldDefinition.StartDate, True) > WorkingStartDate) Then
        WorkingStartDate = FitDateToPeriod(StatsDatePeriod, thisCustomFieldDefinition.StartDate, True)
        WorkingStartIndex = GetPriceIndex(StatsDatePeriod, DateArray(0), WorkingStartDate)
      End If

      If (FitDateToPeriod(StatsDatePeriod, thisCustomFieldDefinition.EndDate, True) < WorkingEndDate) Then
        WorkingEndDate = FitDateToPeriod(StatsDatePeriod, thisCustomFieldDefinition.EndDate, True)
        WorkingEndIndex = GetPriceIndex(StatsDatePeriod, DateArray(0), WorkingStartDate)
      End If

      VolArray = _MainForm.StatFunctions.ReturnFittedDataArray(StatsDatePeriod, DateArray, VolArray, StatFunctions.DataItemEnum.Returns, WorkingStartDate, WorkingEndDate, 1.0#, False, 100.0#)

      If (VolArray Is Nothing) OrElse (VolArray.Length <= 0) OrElse (WorkingEndDate < WorkingStartDate) Then
        ThisNewRow.FieldNumericData = 0
      Else
        If (thisCustomFieldDefinition.IsFirstValue) Then
          ThisNewRow.FieldNumericData = VolArray(0)
        ElseIf (thisCustomFieldDefinition.IsMinValue) Then
          ThisNewRow.FieldNumericData = GetMinValue(VolArray)
        ElseIf (thisCustomFieldDefinition.IsAverageValue) Then
          ThisNewRow.FieldNumericData = GetAverageValue(VolArray)
        ElseIf (thisCustomFieldDefinition.IsMaxValue) Then
          ThisNewRow.FieldNumericData = GetMaxValue(VolArray)
        Else ' Assume Last
          ThisNewRow.FieldNumericData = VolArray(VolArray.Length - 1)
        End If
      End If

    Catch ex As Exception

    End Try

  End Sub

#End Region

#Region " Utility Functions."

	Private Function GetMinValue(ByRef pArray() As Double, Optional ByVal StartingIndex As Integer = 0) As Double
		' *************************************************************************************************
		'
		' *************************************************************************************************

		Dim RVal As Double = 0
		Dim ICount As Integer

		Try
			If (pArray Is Nothing) OrElse (pArray.Length <= 0) Then
				Return RVal
			End If

			RVal = Double.NaN

			For ICount = StartingIndex To (pArray.Length - 1)
				If Double.IsNaN(RVal) Then
					If Double.IsNaN(pArray(ICount)) = False Then
						RVal = pArray(ICount)
					End If
				Else
					If (Double.IsNaN(pArray(ICount)) = False) AndAlso (pArray(ICount) < RVal) Then
						RVal = pArray(ICount)
					End If
				End If

			Next

		Catch ex As Exception
		End Try

		If Double.IsNaN(RVal) Then
			Return 0
		Else
			Return RVal
		End If

	End Function

	Private Function GetMaxValue(ByRef pArray() As Double, Optional ByVal StartingIndex As Integer = 0) As Double
		' *************************************************************************************************
		'
		' *************************************************************************************************

		Dim RVal As Double = 0
		Dim ICount As Integer

		Try
			If (pArray Is Nothing) OrElse (pArray.Length <= 0) Then
				Return RVal
			End If

			RVal = Double.NaN

			For ICount = StartingIndex To (pArray.Length - 1)
				If Double.IsNaN(RVal) Then
					If Double.IsNaN(pArray(ICount)) = False Then
						RVal = pArray(ICount)
					End If
				Else
					If (Double.IsNaN(pArray(ICount)) = False) AndAlso (pArray(ICount) > RVal) Then
						RVal = pArray(ICount)
					End If
				End If

			Next

		Catch ex As Exception
		End Try

		If Double.IsNaN(RVal) Then
			Return 0
		Else
			Return RVal
		End If

	End Function

	Private Function GetAverageValue(ByRef pArray() As Double, Optional ByVal StartingIndex As Integer = 0) As Double
		' *************************************************************************************************
		'
		' *************************************************************************************************

		Dim RVal As Double = 0
		Dim ICount As Integer
		Dim SumTotal As Decimal = 0
		Dim SumCount As Decimal = 0

		Try
			If (pArray Is Nothing) OrElse (pArray.Length <= 0) Then
				Return RVal
			End If

			For ICount = StartingIndex To (pArray.Length - 1)
				If Double.IsNaN(pArray(ICount)) = False Then
					SumTotal += CDec(pArray(ICount))
					SumCount += 1.0
				End If
			Next

		Catch ex As Exception
		End Try

		If (SumCount > 0) Then
			Return CDbl(SumTotal / SumCount)
		Else
			Return 0
		End If

	End Function


#End Region


End Class
