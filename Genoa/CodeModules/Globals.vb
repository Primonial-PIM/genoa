Option Strict On

Module GenoaGlobals

#Region " Global Declarations"

  Public Const SPLASH_DISPLAY As Integer = 3 ' Minimum Duration of Splash Display

  ' Base registry key for program settings

	Public Const REGISTRY_BASE As String = "Software\Primonial\Genoa"

  ' Standard Connection Name, as used with the DataHandler object.

  Public Const Genoa_CONNECTION As String = "cnnGenoa"

  ' Standard Form Caching Counters

  Public Const STANDARD_EntryForm_CACHE_COUNT As Integer = 1
  Public Const STANDARD_ReportForm_CACHE_COUNT As Integer = 1
  Public Const STANDARD_SingleForm_CACHE_COUNT As Integer = 1

  Public Const EPSILON As Double = 0.00000001

  ' Standard reference Dates & timings.
  ' LAST_Second is used to convert KDs to a precise 'Whole Day' value, also used to
  ' evaluate the 'Whole Day' status of a given Date.

  'Public Const KNOWLEDGEDATE_NOW As Date = #1/1/1900#
  'Public Const Renaissance_BaseDate As Date = #1/1/1900#
  'Public Const Renaissance_BaseDateDATA As Date = #1/1/2000#
  'Public Const Renaissance_EndDate_Data As Date = #1/1/2100#
  'Public Const LAST_SECOND As Integer = 86399

  ' Standard SQL Command Timeouts

  Public Const DEFAULT_SQLCOMMAND_TIMEOUT As Integer = 600

  ' Const used to compare two doubles. Differences smaller than this are not deemed to be significant.
  Public Const GENOA_RoundingError As Double = 0.000001

  Public Const GENOA_HoldingPrecision As Integer = 6

  ' Paint Timer Pause - Seconds to delay after parameter change before repainting charts
  Public Const PAINT_TIMER_DELAY As Double = 0.75

  ' If Enabled, Trigger an Update Event after calculating this many Custom Field Data points.
  ' <Used on the 'frmCustomDataMaintenance' form>.
  Public Const GENOA_CalculateCustomField_EventTrigger As Integer = 1000

  ' If Enabled, Refresh the Status grid after this many seconds during a 
  ' Custom Field Calculation procedure.
  ' <Used on the 'frmCustomDataMaintenance' form>.
  Public Const GENOA_UpdateStatusGrid_Period As Integer = 300

  ' Genoa DragDrop Headers
  Public Const GENOA_DRAG_IDs_HEADER As String = "GENOA_DRAG_IDs:"

  ' Standard Genoa form interface.
  ' Must be implemented by any form that is cached.

  Public Interface StandardGenoaForm
    Property IsOverCancelButton() As Boolean
    ReadOnly Property IsInPaint() As Boolean
    ReadOnly Property InUse() As Boolean
    ReadOnly Property FormOpenFailed() As Boolean
    ReadOnly Property MainForm() As GenoaMain
    Sub ResetForm()
    Sub CloseForm()
  End Interface


#End Region

  Public Delegate Sub GroupStatsUpdateDelegate(ByVal sender As Object, ByVal GroupID As Integer, ByVal IsInterimUpdate As Boolean, ByVal UpdateResults As RenaissanceStatFunctions.StatFunctions.SeriesStatsClass)

  Public Class DynamicGroupUpdateTaskClass

    Private _GroupIDToUpdate As Integer
    Private _RelatedForm() As StandardGenoaForm = Nothing
    Private _UpdateSubs() As GroupStatsUpdateDelegate = Nothing
    Private _Cancelled As Boolean
    Private _SampleSize As Integer = (-1)
    Private _GroupSize As Integer = 0
    Private _InterimUpdate As Boolean = False
    Private _RescheduleThisUpdate As Boolean = False
    Private _IsChartUpdate As Boolean
    Private _CustomStatsStartDate As Date
    Private _CustomStatsEndDate As Date
    Private _StatsDatePeriod As RenaissanceGlobals.DealingPeriod

    Private Sub New()
    End Sub

    Public Sub New(ByVal UpdateGroupID As Integer, ByVal FormsToUpdate() As StandardGenoaForm, ByVal UpdateSubs() As GroupStatsUpdateDelegate, ByVal pGroupSize As Integer, ByVal pSampleSize As Integer, ByVal pInterimUpdate As Boolean, ByVal pIsChartUpdate As Boolean, ByVal pCustomStartDate As Date, ByVal pCustomEndDate As Date, ByVal pStatsDatePeriod As RenaissanceGlobals.DealingPeriod)

      Try

        If (UpdateGroupID > 0) Then
          _GroupIDToUpdate = UpdateGroupID
          _RelatedForm = FormsToUpdate
          _UpdateSubs = UpdateSubs
          _SampleSize = pSampleSize
          _GroupSize = pGroupSize
          _InterimUpdate = pInterimUpdate
          _RescheduleThisUpdate = False
          _IsChartUpdate = pIsChartUpdate
          _CustomStatsStartDate = pCustomStartDate ' RenaissanceGlobals.Globals.Renaissance_BaseDate
          _CustomStatsEndDate = pCustomEndDate '  RenaissanceGlobals.Globals.Renaissance_EndDate_Data
          _StatsDatePeriod = pStatsDatePeriod
        Else
          _GroupIDToUpdate = 0
        End If

        _Cancelled = False

      Catch ex As Exception
      End Try

    End Sub

    Public ReadOnly Property GroupID() As Integer
      Get
        Return _GroupIDToUpdate
      End Get
    End Property

    Public ReadOnly Property StrippedGroupID() As Integer
      Get
        Return CInt(RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(CUInt(_GroupIDToUpdate)))
      End Get
    End Property

    Public ReadOnly Property IsFormActive() As Boolean
      Get
        Dim RVal As Boolean = False

        If (_RelatedForm IsNot Nothing) AndAlso (_RelatedForm.Length > 0) Then
          For Index As Integer = 0 To (_RelatedForm.Length - 1)
            If (_RelatedForm(Index).InUse) Then
              RVal = True
              Exit For
            End If
          Next
        End If

        Return RVal
      End Get
    End Property

    Public ReadOnly Property SampleSize() As Integer
      Get
        Return _SampleSize
      End Get
    End Property

    Public ReadOnly Property GroupSize() As Integer
      Get
        Return _GroupSize
      End Get
    End Property

    Public ReadOnly Property InterimUpdate() As Boolean
      Get
        Return _InterimUpdate
      End Get
    End Property

    Public ReadOnly Property IsChartUpdate() As Boolean
      Get
        Return _IsChartUpdate
      End Get
    End Property

    Public ReadOnly Property CustomStatsStartDate() As Date
      Get
        Return _CustomStatsStartDate
      End Get
    End Property

    Public ReadOnly Property CustomStatsEndDate() As Date
      Get
        Return _CustomStatsEndDate
      End Get
    End Property

    Public ReadOnly Property StatsDatePeriod() As RenaissanceGlobals.DealingPeriod
      Get
        Return _StatsDatePeriod
      End Get
    End Property

    Public Property RescheduleThisUpdate() As Boolean
      Get
        Return _RescheduleThisUpdate
      End Get
      Set(ByVal value As Boolean)
        _RescheduleThisUpdate = value
      End Set
    End Property

    Public Property Cancelled() As Boolean
      Get
        Return _Cancelled
      End Get
      Set(ByVal value As Boolean)
        _Cancelled = value
      End Set
    End Property

    Public Sub PostUpdates(ByVal GroupID As Integer, ByVal IsInterimResult As Boolean, ByVal UpdateResults As RenaissanceStatFunctions.StatFunctions.SeriesStatsClass)
      Dim ThisUpdateDelegate As GroupStatsUpdateDelegate
      Dim UpdateIndex As Integer

      Try
        If (_RelatedForm IsNot Nothing) AndAlso (_UpdateSubs IsNot Nothing) AndAlso (GroupID = _GroupIDToUpdate) Then

          For UpdateIndex = 0 To (_RelatedForm.Length - 1)
            If (_UpdateSubs.Length > UpdateIndex) AndAlso (_RelatedForm(UpdateIndex) IsNot Nothing) AndAlso (_RelatedForm(UpdateIndex).InUse) AndAlso (_UpdateSubs(UpdateIndex) IsNot Nothing) Then
              Try
                ThisUpdateDelegate = _UpdateSubs(UpdateIndex)
                ThisUpdateDelegate(Me, _GroupIDToUpdate, IsInterimResult, UpdateResults)
              Catch ex As Exception
              End Try
            End If
          Next
        End If

      Catch ex As Exception
      End Try
    End Sub

    Public Sub Clear()
      ' ************************************************************************************************************************
      '
      ' Clear Class members as completely as possible.
      '
      ' ************************************************************************************************************************

      Dim Index As Integer

      Try
        _GroupIDToUpdate = 0

        If (_RelatedForm IsNot Nothing) AndAlso (_RelatedForm.Length > 0) Then
          For Index = 0 To (_RelatedForm.Length - 1)
            _RelatedForm(Index) = Nothing
          Next
          _RelatedForm = Nothing
        End If

        If (_UpdateSubs IsNot Nothing) AndAlso (_UpdateSubs.Length > 0) Then
          For Index = 0 To (_UpdateSubs.Length - 1)
            _UpdateSubs(Index) = Nothing
          Next
          _UpdateSubs = Nothing
        End If
      Catch ex As Exception
      End Try

    End Sub

  End Class

  ' Fund Search Enumerations

  Public Enum CustomDataCachePresentFlag As Integer
    AllItemsAdded = 1
    SelectiveItemsAdded = 2
  End Enum

  ' Genoa Constants

  Public Enum GenoaChartTypes As Integer
    ' Series Charts :-

    VAMI
    MonthlyReturns
    RollingReturn
    ReturnScatter
    StdDev
    Omega
    DrawDown
    Correlation
    Alpha
    Beta
    Quartile
    Ranking

    ' Single Point Charts
    Compare_Correlation
    Compare_CorrelationUpDown
    Compare_Volatility
    Compare_APR
    Compare_Alpha
    Compare_Beta
    Compare_BetaupDown

  End Enum

  Public Enum GenoaQuartileDisplayData As Integer
    ' Data Series to display on the Quartile and Ranking charts

    MonthlyReturns
    RollingReturns
    Volatility
    Alpha
    Beta
    Correlation

  End Enum

  ' Form ID Enumeration. Intended to uniquely identify each Form Type

  Public Enum GenoaFormID
    ' **************************************************************************************
    '
    ' Enumeration member name MUST be the same as the associated form object.
    ' this is so that the generic 'New_GenoaForm()' function in the Genoa Main form will 
    ' be able to instantiate the correct object.
    '
    ' **************************************************************************************

    None = 0
    frmGenoaMain

    frmAbout
    frmAttributionsReports

    frmChangeControlReview
    frmCompetitorGroupsReport
    frmCustomDataMaintenance
    frmCustomFields

    frmDrillDown

    frmFundBrowser
    frmFundSearch

    frmGroupCovariance
    frmGroupList
    frmGroupMembers
    frmGroupDetails
    frmGroupReturns

    frmInformation
    frmMasterName
    frmOptimise
    frmPerformance

    frmSelectChangeControl
    frmSetKnowledgeDate

    frmUpdatePertrac

    frmUserPermissions

    ' Reports :-

    frmViewReport
    frmViewChart
    frmViewComparisonGrid

    frmInformationReport
    frmGroupReport
    frmTradeHistory

    ' Non-Form permisioning for Change Control roles.

    'ChangeCtrlAddNew
    ChangeCtrlITReview
    ChangeCtrlAuthoriseIT
    ChangeCtrlAuthoriseOwner
    ChangeCtrlAuthoriseBusiness
    ChangeCtrlAcceptIT
    ChangeCtrlAcceptOwner
    ChangeCtrlAcceptBusiness

    MaxValue = 100
  End Enum

  Public Enum GenoaReportID
    ' **************************************************************************************
    '
    ' Enumeration member name MUST be the same as the associated report.
    ' this is so that report permissioning can be performed.
    '
    ' **************************************************************************************

    None = 0

    AttributionsReport
    rptAttribution
    rptSectorBips
    rptSectorReturns
    rptSectorWeights
    rptAttributionBestWorst

    InformationReport
    MonthlyTableReport
    DrawDownReport
    StatisticsReport

    ChartHeaderReport
    ChartPageReport
    ChartMultiPageReport

    rptGroupReport
    rptCompetitorGroupsReport
    rptOptimiserGroupReport

    ' End Stop

    MaxValue
  End Enum

  ' Marginals Status Enumeration.

  <FlagsAttribute()> _
  Public Enum MarginalsStatusEnum As UInteger
    OK = 0
    InstrumentBreak = 1
    SectorBreak = 2
    LiquidityBreak = 4
    CustomFieldBreak = 8
  End Enum

  ' Grid Selection Enum
  Public Enum SelectionEnum As Integer
    None = 0
    SingleRow = 1
    MultipleRows = 2
    AllRows = 3
  End Enum

End Module
