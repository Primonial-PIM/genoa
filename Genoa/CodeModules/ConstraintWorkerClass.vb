Imports RenaissanceGlobals
Imports RenaissanceGlobals.Globals
Imports RenaissanceDataClass
Imports MarkowitzClass

Public Class ConstraintWorkerClass
	Implements OptimiserClass.IConstraintWorkerInterface

	' ****************************************************************************************
	'
	'
	' ****************************************************************************************

#Region " locals"

	' Relate to the Optimiser...

	Public Const EPSILON As Double = 0.000000001
	Public Const INFINITY As Double = CDbl(Single.MaxValue)	' 3.4E38
	Public Const INVALID As Double = Double.NaN

	' Related optimisation form

	Private _OptimiseForm As frmOptimise

	Private InstrumentNumerator As Dictionary(Of Integer, Integer) ' Key is PertracId, Integer is Array Offset
	Private CustomFieldNumerator As Dictionary(Of Integer, Integer)	' Key is CustomFieldID, Integer is Array Offset
	Private CustomFieldDataCache As CustomFieldDataCacheClass	' New

	' Private InstrumentWeights(-1) As Double
	Private CustomFieldData(-1, -1) As Object	' Array (CustomField, Instrument )
	Private MarginalsConstraintArray(-1, -1) As Double	' Array (Constraint, Instrument)
	Private ConstraintDetailsArray(-1) As ConstraintDetailClass

	Private ConstraintDetailsAreInvalid As Boolean ' The User entered constraints
	Private ConstraintsAreInvalid As Boolean ' The Machine generated table of Constraint Mappings
	Private InstrumentsAreInvalid As Boolean ' 
	Private CustomFieldDataIsInvalid As Boolean	' Array Of Custom Field Data vs Instrument

	Private ValueComparer As System.Collections.CaseInsensitiveComparer

	Private _ConstraintTypeColumn As Integer
	Private _ConstraintLimitColumn As Integer

#End Region

#Region " Private Classes"

	Private Class ConstraintDetailClass
		' ****************************************************************************************
		' Used to store the details of individual constraint details for use within the worker class.
		' Each instance of this class relates to a single line on the Instrument grid or a single
		' line on a Constraint grid, thus each instance of this class will usually relate to TWO
		' Marginal or optimisation constraints - One for each of the Lower and Upper limits.
		'
		' ****************************************************************************************

		Public ConstraintID As Integer			' ConstraintId taken from the Constraint Grid, if present.
		Public ConstraintGroupID As Integer	' ConstraintGroupID taken from the Constraint Grid.
		Public CustomFieldID As Integer			' Custom Field ID that this constraint relates to. note hte 'SpecialCase' IDs for Sector, Liquidity and Instrument Limits.

		Public CustomFieldName As String		' 
		Public CustomFieldDataType As RenaissanceGlobals.RenaissanceDataType

		Public TestLessThan As Boolean			' 
		Public TestEqualTo As Boolean				' 
		Public TestGreaterThan As Boolean		' 

		Private _TestValue As Object				'

		Public LowerLimit As Double					' 
		Public UpperLimit As Double					' 

		Private Function ConvertValueToDataType(ByVal Value As Object, ByVal thisDataType As RenaissanceGlobals.RenaissanceDataType) As Object
			' **********************************************************************
			' Private function used for converting a given object to a value of the given data type.
			' The necessity for this function is principally driven by the way C1 grids operate.
			' within a grid, value are commonly stored as type 'Object', thus a routine to reliably
			' convert to other types is convenient.
			' Failure to convert will return a default value for the requested data type.
			'
			' **********************************************************************

			If (thisDataType And RenaissanceDataType.TextType) Then
				Try
					If (Value IsNot Nothing) Then
						If TypeOf (Value) Is Date Then
							Return CType(Value, Date).ToOADate.ToString
						Else
							Return Value.ToString
						End If
					End If

				Catch ex As Exception
				End Try

				Return ""

			ElseIf (thisDataType And (RenaissanceDataType.NumericType Or RenaissanceDataType.PercentageType)) Then
				Try
					If (Value IsNot Nothing) Then
						If IsNumeric(Value) Then
							Return CDbl(Value)
						ElseIf (Value.ToString.ToUpper.EndsWith("%")) AndAlso IsNumeric(Value.ToString.Substring(0, Value.ToString.Length - 1)) Then
							Return CDbl(Value.ToString.Substring(0, Value.ToString.Length - 1)) / 100.0#
						End If
					End If
				Catch ex As Exception
				End Try

				Return 0

			ElseIf (Value IsNot Nothing) AndAlso (thisDataType And RenaissanceDataType.DateType) Then
				Try
					If IsDate(Value) Then
						Return CDate(Value)
					End If
				Catch ex As Exception
				End Try

				Return #1/1/1900#

			ElseIf (Value IsNot Nothing) AndAlso (thisDataType And RenaissanceDataType.BooleanType) Then
				Try
					Return CBool(Value)
				Catch ex As Exception
				End Try

				Return False

			End If

			Return Nothing

		End Function

		Public Property TestValue() As Object
			' **********************************************************************
			' Gets or Sets the internal Constraint definition value in a type consistent with the 
			' Constraint Definition type
			'
			' **********************************************************************

			Get
				If (CustomFieldDataType And RenaissanceDataType.TextType) Then
					Try
						If (_TestValue IsNot Nothing) Then
							Return _TestValue.ToString
						End If

					Catch ex As Exception
					End Try

					Return ""

				ElseIf (CustomFieldDataType And (RenaissanceDataType.NumericType Or RenaissanceDataType.PercentageType)) Then
					Try
						If (_TestValue IsNot Nothing) AndAlso IsNumeric(_TestValue) Then
							Return CDbl(_TestValue)
						End If
					Catch ex As Exception

					End Try
					Return 0

				ElseIf (_TestValue IsNot Nothing) AndAlso (CustomFieldDataType And RenaissanceDataType.DateType) Then
					Try
						If IsDate(_TestValue) Then
							Return CDate(_TestValue)
						End If
					Catch ex As Exception
					End Try

					Return #1/1/1900#

				ElseIf (_TestValue IsNot Nothing) AndAlso (CustomFieldDataType And RenaissanceDataType.BooleanType) Then
					Try
						Return CBool(_TestValue)
					Catch ex As Exception
					End Try
					Return False

				End If

				Return Nothing
			End Get

			Set(ByVal value As Object)

				Try
					_TestValue = ConvertValueToDataType(value, CustomFieldDataType)
				Catch ex As Exception
				End Try


			End Set
		End Property

		Public Function TestConstraint(ByVal pTestObject As Object, ByRef thisValueComparer As System.Collections.CaseInsensitiveComparer) As Boolean
			' **********************************************************************
			' Compare a given value againts the Constraint definition vale to establish
			' whether or not the given value falls within the definition of the constraint.
			' e.g. For sector constraints, test to see if the given value equals the Constraint Sector.
			'
			' **********************************************************************

			Dim CompResult As Integer

			If (pTestObject Is Nothing) Then
				Return False
			End If

			CompResult = thisValueComparer.Compare(ConvertValueToDataType(pTestObject, CustomFieldDataType), TestValue)

			Try
				If Me.TestLessThan Then
					If (CompResult < 0) Then
						Return True
					End If
				End If
				If Me.TestEqualTo Then
					If (CompResult = 0) Then
						Return True
					End If
				End If
				If Me.TestGreaterThan Then
					If (CompResult > 0) Then
						Return True
					End If
				End If

			Catch ex As Exception

			End Try

			Return False

		End Function

		Private Sub New()
			' Prohibit default new(), insist on constructors.
		End Sub

		Public Sub New(ByVal pConstraintID As Integer, ByVal pConstraintGroupID As Integer, ByVal pCustomFieldID As Integer, ByVal pCustomFieldName As String, ByVal pCustomFieldDataType As RenaissanceGlobals.RenaissanceDataType, ByVal pTestLessThan As Boolean, ByVal pTestEqualTo As Boolean, ByVal pTestGreaterThan As Boolean, ByVal pTestValue As Object, ByVal pLowerLimit As Double, ByVal pUpperLimit As Double)
			' **********************************************************************
			' Constructor
			'
			' **********************************************************************

			ConstraintID = pConstraintID
			ConstraintGroupID = pConstraintGroupID
			CustomFieldID = pCustomFieldID

			CustomFieldName = pCustomFieldName
			CustomFieldDataType = pCustomFieldDataType

			TestLessThan = pTestLessThan
			TestEqualTo = pTestEqualTo
			TestGreaterThan = pTestGreaterThan

			TestValue = pTestValue

			LowerLimit = pLowerLimit
			UpperLimit = pUpperLimit

		End Sub

	End Class

#End Region

#Region " Constructors"

	Private Sub New()
		' **********************************************************************
		' Constructor
		'
		' Restrict New() with no parameters
		'
		' **********************************************************************

		ValueComparer = New System.Collections.CaseInsensitiveComparer

		InstrumentNumerator = New Dictionary(Of Integer, Integer)
		CustomFieldNumerator = New Dictionary(Of Integer, Integer)
		CustomFieldDataCache = Nothing

		ConstraintDetailsAreInvalid = True
		ConstraintsAreInvalid = True
		InstrumentsAreInvalid = True
		' WeightsAreInvalid = True
		CustomFieldDataIsInvalid = True

	End Sub

	Public Sub New(ByRef pOptimiseForm As frmOptimise)
		' **********************************************************************
		' Constructor
		'
		' Provides the necessary link to the originating optimisation form.
		' This link provides access to the MainForm (for DB access & Error reporting), but
		' more importantly provdes access to the controls that define all the constraints !
		'
		' **********************************************************************

		Me.New()

		_OptimiseForm = pOptimiseForm

		CustomFieldDataCache = pOptimiseForm.CustomFieldDataCache	' maps back to MainForm object.

	End Sub

#End Region

#Region " Properties"

	Public ReadOnly Property OptimiseForm() As frmOptimise
		Get
			Return _OptimiseForm
		End Get
	End Property

	Public ReadOnly Property ConstraintTypeColumn() As Integer
		Get
			Return _ConstraintTypeColumn
		End Get
	End Property

	Public ReadOnly Property ConstraintLimitcolumn() As Integer
		Get
			Return _ConstraintLimitColumn
		End Get
	End Property

	Public ReadOnly Property InstrumentCount() As Integer
		Get
			Try
				If (InstrumentNumerator IsNot Nothing) Then
					Return InstrumentNumerator.Count
				Else
					Return 0
				End If
			Catch ex As Exception
			End Try
			Return 0
		End Get
	End Property

	Public ReadOnly Property DefinedConstraintsCount() As Integer
		Get
			Try
				If (ConstraintDetailsArray IsNot Nothing) Then
					Return ConstraintDetailsArray.Length
				Else
					Return 0
				End If
			Catch ex As Exception
			End Try
			Return 0
		End Get
	End Property

	Public ReadOnly Property MarginalsConstraintArrayCount() As Integer
		Get
			Try
				If (MarginalsConstraintArray IsNot Nothing) Then
					Return MarginalsConstraintArray.GetLength(0)
				Else
					Return 0
				End If
			Catch ex As Exception
			End Try
			Return 0
		End Get
	End Property

	Public ReadOnly Property InstrumentWeights(ByVal pWeightsGrid As C1.Win.C1FlexGrid.C1FlexGrid, Optional ByVal pPertracCol As Integer = (-1), Optional ByVal pWeightCol As Integer = (-1)) As Double()
		Get
      ' *****************************************************************************************
      ' *****************************************************************************************

			Dim WeightsArray() As Double = Nothing

      If (InstrumentsAreInvalid) Then
        LoadInstrumentDetails(False)
      End If

			Try
				Dim PertracCol As Integer
				Dim WeightCol As Integer

				WeightsArray = Array.CreateInstance(GetType(Double), InstrumentNumerator.Count)

				If (pPertracCol < 0) Then
					If pWeightsGrid.Cols.Contains("GroupPertracCode") Then
						PertracCol = pWeightsGrid.Cols("GroupPertracCode").SafeIndex
					Else
						Return WeightsArray
						Exit Property
					End If
				Else
					PertracCol = pPertracCol
				End If

				If (pWeightCol < 0) Then
					If pWeightsGrid.Cols.Contains("GroupNewPercent") Then
						WeightCol = pWeightsGrid.Cols("GroupNewPercent").SafeIndex
					Else
						Return WeightsArray
						Exit Property
					End If
				Else
					WeightCol = pWeightCol
				End If

				Dim GridRowCounter As Integer
				Dim thisGridRow As C1.Win.C1FlexGrid.Row
				Dim ThisPertracID As Integer
				Dim ThisInstrumentNumerator As Integer

				For GridRowCounter = 0 To (pWeightsGrid.Rows.Count - 1)
					Try
						thisGridRow = pWeightsGrid.Rows(GridRowCounter)

						If (Not thisGridRow.IsNew) AndAlso (IsNumeric(thisGridRow(PertracCol))) Then
							ThisPertracID = CInt(thisGridRow(PertracCol))

							If InstrumentNumerator.ContainsKey(ThisPertracID) Then

								ThisInstrumentNumerator = InstrumentNumerator(ThisPertracID)

								WeightsArray(ThisInstrumentNumerator) = CDbl(thisGridRow(WeightCol))

							End If

						End If

					Catch ex As Exception

					End Try
				Next

			Catch ex As Exception
			End Try

			Return WeightsArray

		End Get
	End Property

	Public ReadOnly Property InstrumentIDs() As Integer()
		' *****************************************************************************************
		' Return Array of Pertrac IDs used in the Optimiser, in the order that they are represented 
		' internally.
		'
		' Note : Values returned are Base 0 as opposed to InstrumentID() - Base 1, returned from the Optimiser.
		' *****************************************************************************************

		Get
			Dim RVal() As Integer = Nothing

      If (InstrumentsAreInvalid) Then
        LoadInstrumentDetails(False)
      End If

			Try
				' Create return array of Pertrac IDs in the correct positions.

				If (InstrumentNumerator IsNot Nothing) AndAlso (InstrumentNumerator.Count > 0) Then
					RVal = Array.CreateInstance(GetType(Integer), InstrumentNumerator.Count)

					Dim ID_Array(InstrumentNumerator.Keys.Count - 1) As Integer
					Dim Counter As Integer

					InstrumentNumerator.Keys.CopyTo(ID_Array, 0)

					For Counter = 0 To (ID_Array.Length - 1)
						RVal(InstrumentNumerator(ID_Array(Counter))) = ID_Array(Counter)
					Next

				Else
					RVal = Array.CreateInstance(GetType(Integer), 1)
				End If

			Catch ex As Exception
			End Try

			Return RVal

		End Get
	End Property

#End Region

#Region " Methods" ' In Madness ?

	Public Sub InvalidateConstraintDetails()
		' **********************************************************************
		' Flag that Constraint Details have changed.
		'
		' **********************************************************************

		ConstraintsAreInvalid = True
		ConstraintDetailsAreInvalid = True
	End Sub

	Public Sub InvalidateInstruments()
		' **********************************************************************
		' Flag that Instrument Details have changed. this of course invalidates
		' the Constraints.
		'
		' **********************************************************************

		InstrumentsAreInvalid = True
		ConstraintsAreInvalid = True
		CustomFieldDataIsInvalid = True
	End Sub

	Public Sub InvalidateCustomFieldData()
		' **********************************************************************
		' Flag that Custom Data Details have changed. this of course invalidates
		' the Constraints.
		'
		' **********************************************************************

		CustomFieldDataIsInvalid = True
		ConstraintsAreInvalid = True
	End Sub

#End Region

#Region " Data Initialisation Code"

	Public Sub RefreshAllData(Optional ByVal pForceReload As Boolean = False) Implements MarkowitzClass.OptimiserClass.IConstraintWorkerInterface.RefreshAllData
		' ****************************************************************************************
		'
		'
		' ****************************************************************************************

		' Update order (?) :-
		' Instruments
		' ConstraintDetails
		' CustomFieldData
		' Constraints

		If (InstrumentsAreInvalid) Or (pForceReload) Then
			LoadInstrumentDetails(pForceReload)
		End If

		If (ConstraintDetailsAreInvalid) Or (pForceReload) Then
			LoadConstraintDetails(pForceReload)
		End If

		If (CustomFieldDataIsInvalid) Or (pForceReload) Then
			LoadCustomFieldData(pForceReload)
		End If

		If (ConstraintsAreInvalid) Or (pForceReload) Then
			UpdateConstraints(pForceReload)
		End If

	End Sub

	Public Sub LoadInstrumentDetails(Optional ByVal pForceRefresh As Boolean = False)
		' ****************************************************************************************
		' Load Instrument details from the Instruments (Returns) Grid.
		'
		' ****************************************************************************************

		If (Not pForceRefresh) AndAlso (Not InstrumentsAreInvalid) Then
			Exit Sub
		End If

		Try

			' Get InstrumentsGrid

			Dim GridInstruments As C1.Win.C1FlexGrid.C1FlexGrid

			GridInstruments = _OptimiseForm.Grid_Returns

			Dim NewInstruments As Boolean = False
			Dim InstrumentCount As Integer = 0
			Dim GridRowCounter As Integer
			Dim thisGridRow As C1.Win.C1FlexGrid.Row

			Dim Col_PertracCode As Integer = GridInstruments.Cols("GroupPertracCode").SafeIndex
			Dim ThisPertracCode As Integer

			' 1) Check for New Instruments

			For GridRowCounter = 1 To (GridInstruments.Rows.Count - 1)
				thisGridRow = GridInstruments.Rows(GridRowCounter)

				If (Not thisGridRow.IsNew) Then
					If (thisGridRow(Col_PertracCode) IsNot Nothing) AndAlso (IsNumeric(thisGridRow(Col_PertracCode))) Then
						ThisPertracCode = CInt(thisGridRow(Col_PertracCode))

						If (ThisPertracCode > 0) Then
							InstrumentCount += 1

							If (Not InstrumentNumerator.ContainsKey(ThisPertracCode)) Then
								NewInstruments = True
								Exit For
							End If
						End If
					End If
				End If
			Next

			' 2) If there are any new or removed Instruments, then re-load the appropriate data structures.

			If NewInstruments OrElse (InstrumentCount <> InstrumentNumerator.Count) Then

				InstrumentNumerator.Clear()

				CustomFieldDataIsInvalid = True
				' WeightsAreInvalid = True
				ConstraintsAreInvalid = True

				' Load Instrument Numerator 

				For GridRowCounter = 1 To (GridInstruments.Rows.Count - 1)
					thisGridRow = GridInstruments.Rows(GridRowCounter)

					If (Not thisGridRow.IsNew) Then
						If (thisGridRow(Col_PertracCode) IsNot Nothing) AndAlso (IsNumeric(thisGridRow(Col_PertracCode))) Then
							ThisPertracCode = CInt(thisGridRow(Col_PertracCode))

							If (ThisPertracCode > 0) Then
								InstrumentNumerator.Add(ThisPertracCode, InstrumentNumerator.Count)
							End If
						End If
					End If
				Next

			End If

			InstrumentsAreInvalid = False

		Catch ex As Exception

		End Try
	End Sub

	Public Sub LoadConstraintDetails(Optional ByVal pForceRefresh As Boolean = False)
		' ****************************************************************************************
		' Populate the ConstraintDetailsArray with ConstraintDetailsClass objects relating to the 
		' Defined constraints.
		'
		' Constraints come from Two Sources :-
		'		1) The Upper and Lower bounds defined for individual Instruments on the Instruments (Returns) Grid.
		'		2) The Constraints defined on the Constraints tab relating to limits based on Custom fields
		'				( and Sector & Liquidity).
		'		
		'	The ConstraintDetailsClass objects relating to the individual Instrument limits will have a Constraint
		' ID of SpecialConstraintFields.IndividualInstrumentLimit (-15 at present).
		'
		' ****************************************************************************************

		If (Not pForceRefresh) AndAlso (Not ConstraintDetailsAreInvalid) Then
			Exit Sub
		End If

		Try

			' Get ConstraintGroupID 

			Dim ConstraintGroupID As Integer = 0
			If (_OptimiseForm.Constraints_ComboConstraintsGoup.SelectedIndex > 0) AndAlso IsNumeric(_OptimiseForm.Constraints_ComboConstraintsGoup.SelectedValue) Then
				ConstraintGroupID = CInt(_OptimiseForm.Constraints_ComboConstraintsGoup.SelectedValue)
			End If

			' 1) Clear Constraint Details Array, Constraints Array
			' 2) Count Constraints and Custom Fields in Use
			' 3) Update CustomFieldNumerator if Necessary 
			' 4) Build ConstraintDetailsArray, Private ConstraintDetailsArray(-1) As ConstraintDetailClass)

			If (ConstraintDetailsArray IsNot Nothing) AndAlso (ConstraintDetailsArray.Length > 0) Then
				Array.Clear(ConstraintDetailsArray, 0, ConstraintDetailsArray.Length)
				ConstraintDetailsArray = Nothing
			End If
			If (MarginalsConstraintArray IsNot Nothing) Then
				MarginalsConstraintArray = Nothing
				ConstraintsAreInvalid = True
			End If

			' 2) Count Constraints and Custom Fields in Use

			Dim DefinedGrid As C1.Win.C1FlexGrid.C1FlexGrid = _OptimiseForm.Constraints_Grid_Defined
			Dim InstrumentsGrid As C1.Win.C1FlexGrid.C1FlexGrid = _OptimiseForm.Grid_Returns
			Dim ConstraintsTab As TabPage
			Dim TabName As String
			Dim ConstraintsGrid As C1.Win.C1FlexGrid.C1FlexGrid

			Dim GridRowCounter As Integer
			Dim ConstraintsGridRowCounter As Integer
			Dim thisGridRow As C1.Win.C1FlexGrid.Row
			Dim thisConstraintsGridRow As C1.Win.C1FlexGrid.Row
			Dim TempConstraintArray As New ArrayList

			' Constraint Grid Columns

			Dim Col_CustomFieldID As Integer = DefinedGrid.Cols("Col_CustomID").SafeIndex
			Dim Col_CustomFieldDataType As Integer = DefinedGrid.Cols("Col_CustomFieldDataType").SafeIndex
			Dim Col_CustomFieldName As Integer = DefinedGrid.Cols("Col_CustomName").SafeIndex
			Dim Col_IsDefined As Integer = DefinedGrid.Cols("Col_IsDefined").SafeIndex
			Dim Col_IsApplied As Integer = DefinedGrid.Cols("Col_IsApplied").SafeIndex

			' Instruments grid columns

			Dim Col_GroupPertracCode As Integer = InstrumentsGrid.Cols("GroupPertracCode").SafeIndex
			Dim Col_GroupLowerBound As Integer = InstrumentsGrid.Cols("GroupLowerBound").SafeIndex
			Dim Col_GroupUpperBound As Integer = InstrumentsGrid.Cols("GroupUpperBound").SafeIndex
			Dim Col_PertracName As Integer = InstrumentsGrid.Cols("PertracName").SafeIndex

			Dim ThisCustomFieldID As Integer
			Dim ThisCustomFieldDataType As Integer
			Dim ThisCustomFieldName As String

			Try

				' Cycle through the Constraints defined on the Instruments Grid.
				' InstrumentsGrid

				Dim thisPertracCode As Integer

				Dim newConstraintDetail As ConstraintDetailClass
				Dim pLowerLimit As Double
				Dim pUpperLimit As Double

				ThisCustomFieldID = SpecialConstraintFields.IndividualInstrumentLimit	' Lower and Upper Limits

				For GridRowCounter = 1 To (InstrumentsGrid.Rows.Count - 1)
					Try
						thisGridRow = InstrumentsGrid.Rows(GridRowCounter)
						thisPertracCode = CInt(thisGridRow(Col_GroupPertracCode))

						' Default Instrument limit of +/- 100% of portfolio.

						pLowerLimit = 0	' (-1)
						pUpperLimit = 0	' 1

						' If this Row is not 'New' and either a Floor or Cap are set...

						If (Not thisGridRow.IsNew) Then	' AndAlso (CBool(thisGridRow(Col_GroupFloor)) Or CBool(thisGridRow(Col_GroupCap))) Then

							'If CBool(thisGridRow(Col_GroupFloor)) AndAlso IsNumeric(thisGridRow(Col_GroupLowerBound)) Then
							pLowerLimit = CDbl(thisGridRow(Col_GroupLowerBound))
							'End If

							'If CBool(thisGridRow(Col_GroupCap)) AndAlso IsNumeric(thisGridRow(Col_GroupUpperBound)) Then
							pUpperLimit = CDbl(thisGridRow(Col_GroupUpperBound))
							'End If

              newConstraintDetail = New ConstraintDetailClass(0, 0, ThisCustomFieldID, Nz(thisGridRow(Col_PertracName), "Instrument Limit").ToString, RenaissanceDataType.NumericType, False, True, False, thisPertracCode, pLowerLimit, pUpperLimit)
							TempConstraintArray.Add(newConstraintDetail)

						End If

					Catch ex As Exception

					End Try

				Next

				' Cycle Through Conventionally defined constraints on the Constraints Tab

				For GridRowCounter = 1 To (DefinedGrid.Rows.Count - 1)
					Try
						thisGridRow = DefinedGrid.Rows(GridRowCounter)

						If (Not thisGridRow.IsNew) AndAlso CBool(thisGridRow(Col_IsDefined)) AndAlso CBool(thisGridRow(Col_IsApplied)) Then

							' OK, Resolve and Get TabPage

							ThisCustomFieldID = CInt(thisGridRow(Col_CustomFieldID))
							ThisCustomFieldDataType = CInt(thisGridRow(Col_CustomFieldDataType))
							ThisCustomFieldName = CStr(thisGridRow(Col_CustomFieldName))

							' Update CustomFieldNumerator

							If (Not CustomFieldNumerator.ContainsKey(ThisCustomFieldID)) Then
								CustomFieldNumerator.Add(ThisCustomFieldID, CustomFieldNumerator.Count)

								If (Not CustomFieldDataIsInvalid) Then
									CustomFieldDataIsInvalid = True

									ReDim CustomFieldData(-1, -1)
								End If
							End If

							' Get TabName

							If (ThisCustomFieldID < 0) Then
								TabName = "Tab_" & CType(ThisCustomFieldID, SpecialConstraintFields).ToString
							Else
								TabName = "Tab_" & ThisCustomFieldID.ToString
							End If

							ConstraintsTab = Nothing

							If _OptimiseForm.Constraints_TabConstraints.TabPages.ContainsKey(TabName) Then
								ConstraintsTab = _OptimiseForm.Constraints_TabConstraints.TabPages(TabName)
							End If

              If (ConstraintsTab IsNot Nothing) Then

                Dim thisConstraintsTabDetails As frmOptimise.ConstraintsTabDetails

                thisConstraintsTabDetails = ConstraintsTab.Tag
                ConstraintsGrid = thisConstraintsTabDetails.ConstraintsGrid

                'Dim Col_LessThan As Integer = ConstraintsGrid.Cols("Col_LessThan").SafeIndex
                'Dim Col_Equal As Integer = ConstraintsGrid.Cols("Col_Equal").SafeIndex
                'Dim Col_GreaterThan As Integer = ConstraintsGrid.Cols("Col_GreaterThan").SafeIndex
                'Dim Col_Value As Integer = ConstraintsGrid.Cols("Col_Value").SafeIndex
                'Dim Col_LowerLimit As Integer = ConstraintsGrid.Cols("Col_LowerLimit").SafeIndex
                'Dim Col_UpperLimit As Integer = ConstraintsGrid.Cols("Col_UpperLimit").SafeIndex
                'Dim Col_ConstraintID As Integer = ConstraintsGrid.Cols("Col_ConstraintID").SafeIndex
								Dim NewConstraintDetailClass As ConstraintDetailClass

                For ConstraintsGridRowCounter = 1 To (ConstraintsGrid.Rows.Count - 1)
                  Try
                    thisConstraintsGridRow = ConstraintsGrid.Rows(ConstraintsGridRowCounter)

										NewConstraintDetailClass = ReturnConstraintDetailFromConstraintGridRow(ConstraintsGrid, thisConstraintsGridRow, ConstraintGroupID, ThisCustomFieldID, ThisCustomFieldName, ThisCustomFieldDataType)

										If (NewConstraintDetailClass IsNot Nothing) Then
											TempConstraintArray.Add(NewConstraintDetailClass)
										End If

                    'If (Not thisConstraintsGridRow.IsNew) Then
                    '  ' If LessThan, Equal and GreaterThan are all set, Discard the constraint
                    '  ' as this will equait to another 'Sum(Weight) = 1' constraint.

                    '  If Not (CBool(thisConstraintsGridRow(Col_LessThan)) And CBool(thisConstraintsGridRow(Col_Equal)) And CBool(thisConstraintsGridRow(Col_GreaterThan))) Then
                    '    TempConstraintArray.Add(New ConstraintDetailClass(CInt(thisConstraintsGridRow(Col_ConstraintID)), ConstraintGroupID, ThisCustomFieldID, ThisCustomFieldName, ThisCustomFieldDataType, CBool(thisConstraintsGridRow(Col_LessThan)), CBool(thisConstraintsGridRow(Col_Equal)), CBool(thisConstraintsGridRow(Col_GreaterThan)), thisConstraintsGridRow(Col_Value), CDbl(thisConstraintsGridRow(Col_LowerLimit)), CDbl(thisConstraintsGridRow(Col_UpperLimit))))
                    '    CountOfConstraints += 1
                    '  End If

                    'End If
                  Catch ex As Exception
                  End Try
                Next

              End If

						End If
					Catch ex As Exception
					End Try
				Next

				' Redim and populate Arrays

				If (TempConstraintArray.Count > 0) Then
					ConstraintDetailsArray = TempConstraintArray.ToArray(GetType(ConstraintDetailClass))
				Else
					ReDim ConstraintDetailsArray(-1)
				End If

			Catch ex As Exception

			End Try

			ConstraintDetailsAreInvalid = False

		Catch ex As Exception

		End Try

	End Sub

  Private Function ReturnConstraintDetailFromConstraintGridRow(ByVal ConstraintsGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal thisConstraintsGridRow As C1.Win.C1FlexGrid.Row, ByVal ConstraintGroupID As Integer, ByVal ThisCustomFieldID As Integer, ByVal ThisCustomFieldName As String, ByVal ThisCustomFieldDataType As Integer, Optional ByVal AllowSumWeightConstraints As Boolean = False) As ConstraintDetailClass
    ' ****************************************************************************************
    '
    '
    '
    ' ****************************************************************************************

    Try

      Dim Col_LessThan As Integer = ConstraintsGrid.Cols("Col_LessThan").SafeIndex
      Dim Col_Equal As Integer = ConstraintsGrid.Cols("Col_Equal").SafeIndex
      Dim Col_GreaterThan As Integer = ConstraintsGrid.Cols("Col_GreaterThan").SafeIndex
      Dim Col_Value As Integer = ConstraintsGrid.Cols("Col_Value").SafeIndex
      Dim Col_LowerLimit As Integer = ConstraintsGrid.Cols("Col_LowerLimit").SafeIndex
      Dim Col_UpperLimit As Integer = ConstraintsGrid.Cols("Col_UpperLimit").SafeIndex
      Dim Col_ConstraintID As Integer = ConstraintsGrid.Cols("Col_ConstraintID").SafeIndex


      If (Not thisConstraintsGridRow.IsNew) Then
        ' If LessThan, Equal and GreaterThan are all set, Discard the constraint
        ' as this will equait to another 'Sum(Weight) = 1' constraint.

        If (AllowSumWeightConstraints) OrElse (Not (CBool(thisConstraintsGridRow(Col_LessThan)) And CBool(thisConstraintsGridRow(Col_Equal)) And CBool(thisConstraintsGridRow(Col_GreaterThan)))) Then
          Return (New ConstraintDetailClass(CInt(thisConstraintsGridRow(Col_ConstraintID)), ConstraintGroupID, ThisCustomFieldID, ThisCustomFieldName, ThisCustomFieldDataType, CBool(thisConstraintsGridRow(Col_LessThan)), CBool(thisConstraintsGridRow(Col_Equal)), CBool(thisConstraintsGridRow(Col_GreaterThan)), thisConstraintsGridRow(Col_Value), CDbl(thisConstraintsGridRow(Col_LowerLimit)), CDbl(thisConstraintsGridRow(Col_UpperLimit))))
        End If

      End If
    Catch ex As Exception
    End Try

    Return Nothing

  End Function

  Public Function ReturnMarginalsConstraintArrayFromConstraintGridRow(ByVal ConstraintsGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal thisConstraintsGridRow As C1.Win.C1FlexGrid.Row, ByVal ConstraintGroupID As Integer, ByVal ThisCustomFieldID As Integer, ByVal ThisCustomFieldName As String, ByVal ThisCustomFieldDataType As Integer) As Double()

    Dim RValConstraintArray(InstrumentNumerator.Count + 1) As Double
    Dim ThisConstraintDetail As ConstraintDetailClass
    Dim ThisConstraintTestValue As Object = Nothing
    Dim ThisCustomFieldValue As Object = Nothing
    Dim InstrumentCounter As Integer
    Dim ThisCustomFieldNumerator As Integer
    Dim ThisInstrumentID As Integer
    Dim ThisInstrumentNumerator As Integer

    ThisConstraintDetail = ReturnConstraintDetailFromConstraintGridRow(ConstraintsGrid, thisConstraintsGridRow, ConstraintGroupID, ThisCustomFieldID, ThisCustomFieldName, ThisCustomFieldDataType, True)
    If (ThisConstraintDetail Is Nothing) Then
      Return RValConstraintArray
    End If

    ' Get List Of Instrument IDs
    ' InstrumentNumerator should contain all Instrument IDs currently in use.

    Dim InstrumentIDs(InstrumentNumerator.Keys.Count - 1) As Integer
    InstrumentNumerator.Keys.CopyTo(InstrumentIDs, 0)

    ' Get Constraint Test Value

    ThisConstraintTestValue = ThisConstraintDetail.TestValue

    ' For General Constraints the Instrument weights are set to 1.0 if the Custom data value tests
    ' True, otherwise the weight is Zero.
    ' Update CustomFieldNumerator

    If (Not CustomFieldNumerator.ContainsKey(ThisCustomFieldID)) Then
      CustomFieldNumerator.Add(ThisCustomFieldID, CustomFieldNumerator.Count)

      If (Not CustomFieldDataIsInvalid) Then
        CustomFieldDataIsInvalid = True

        ReDim CustomFieldData(-1, -1)

        LoadCustomFieldData(False)
      End If

			If (ThisCustomFieldID = CInt(SpecialConstraintFields.Liquidity)) OrElse (ThisCustomFieldID = CInt(SpecialConstraintFields.Sector)) Then
				RefreshUserEnteredCustomData()
			End If

    ElseIf (CustomFieldDataIsInvalid) Then
      LoadCustomFieldData(False)
      RefreshUserEnteredCustomData()
    End If

    ThisCustomFieldNumerator = CustomFieldNumerator(ThisConstraintDetail.CustomFieldID)

    For InstrumentCounter = 0 To (InstrumentIDs.Length - 1)

      ThisInstrumentID = InstrumentIDs(InstrumentCounter)
      ThisInstrumentNumerator = InstrumentNumerator(ThisInstrumentID)

      ThisCustomFieldValue = CustomFieldData(ThisCustomFieldNumerator, ThisInstrumentNumerator)

      ' Now, Does this Instrument satisfy the demands of the constraint ?

      If ThisConstraintDetail.TestConstraint(ThisCustomFieldValue, ValueComparer) Then
        ' Test Value OK
        RValConstraintArray(ThisInstrumentNumerator) = 1.0
      Else
        RValConstraintArray(ThisInstrumentNumerator) = 0
      End If
    Next

    Return RValConstraintArray

  End Function

  Public Sub LoadCustomFieldData(Optional ByVal pForceRefresh As Boolean = False)
    ' ****************************************************************************************
    ' Routine to Construct the CustomFieldData Array
    '
    ' Private CustomFieldData(-1, -1) As Object
    ' An Array of Custom Field Data vs Instrument IDs
    '
    ' ****************************************************************************************

    If (Not pForceRefresh) AndAlso (Not CustomFieldDataIsInvalid) Then
      Exit Sub
    End If

    If (InstrumentsAreInvalid) Then
      LoadInstrumentDetails()
    End If

    If (ConstraintDetailsAreInvalid) Then
      LoadConstraintDetails()
    End If

    If (InstrumentNumerator Is Nothing) OrElse (InstrumentNumerator.Count <= 0) Then
      ' No Instruments, Exit OK.
      CustomFieldDataIsInvalid = False
      Exit Sub
    End If

    If (CustomFieldNumerator Is Nothing) OrElse (CustomFieldNumerator.Count <= 0) Then
      ' No Custom Fields (Constraints) in use, Exit OK.
      CustomFieldDataIsInvalid = False
      Exit Sub
    End If

    Dim GroupListID As Integer = 0

    ' Get GroupListID 

    Try
      If (_OptimiseForm.Combo_SelectGroupID.SelectedIndex > 0) AndAlso IsNumeric(_OptimiseForm.Combo_SelectGroupID.SelectedValue) Then
        GroupListID = CInt(_OptimiseForm.Combo_SelectGroupID.SelectedValue)
      End If
    Catch ex As Exception
    End Try

    ' Get List Of Custom Field IDs
    ' CustomFieldNumerator should contain all Custom Field IDs currently in use.

    Dim CustomFieldIDs(CustomFieldNumerator.Keys.Count - 1) As Integer
    CustomFieldNumerator.Keys.CopyTo(CustomFieldIDs, 0)

    ' Get List Of Instrument IDs
    ' InstrumentNumerator should contain all Instrument IDs currently in use.

    Dim InstrumentIDs(InstrumentNumerator.Keys.Count - 1) As Integer
    InstrumentNumerator.Keys.CopyTo(InstrumentIDs, 0)

    ' Get Custom Field Data 

    Try
      Dim InstrumentCounter As Integer
      Dim ThisInstrumentID As Integer
      Dim ThisInstrumentNumerator As Integer
      Dim CustomFieldCounter As Integer
      Dim ThisCustomFieldID As Integer
      Dim ThisCustomValue As Object = Nothing
      Dim ThisCustomFieldNumerator As Integer

      ReDim CustomFieldData(CustomFieldNumerator.Count - 1, InstrumentNumerator.Count - 1)

			For CustomFieldCounter = 0 To (CustomFieldIDs.Length - 1)
				ThisCustomFieldID = CustomFieldIDs(CustomFieldCounter)
				ThisCustomFieldNumerator = CustomFieldNumerator(ThisCustomFieldID)

				' Get Group Set of Custom Data for this Custom Field.

				If (ThisCustomFieldID > 0) Then

					If CustomFieldDataCache.GetCustomFieldDataStatus(ThisCustomFieldID, 0) <> CustomFieldDataCacheClass.CacheStatus.FullyPopulated Then
						CustomFieldDataCache.LoadCustomFieldData(ThisCustomFieldID, 0)
					End If

				End If

				For InstrumentCounter = 0 To (InstrumentIDs.Length - 1)
					ThisInstrumentID = InstrumentIDs(InstrumentCounter)
					ThisInstrumentNumerator = InstrumentNumerator(ThisInstrumentID)

					Try
						ThisCustomValue = Nothing

						If (ThisCustomFieldID > 0) Then
							' Standard Custom Field

							ThisCustomValue = CustomFieldDataCache.GetDataPoint(ThisCustomFieldID, GroupListID, ThisInstrumentID)

							CustomFieldData(ThisCustomFieldNumerator, ThisInstrumentNumerator) = ThisCustomValue

						End If
					Catch ex As Exception
					End Try
				Next

			Next

			CustomFieldDataIsInvalid = False

		Catch ex As Exception
		End Try

  End Sub

	Public Sub RefreshUserEnteredCustomData(Optional ByVal pUpdateUserEnteredCustomFields As Boolean = False, Optional ByVal pForceRefresh As Boolean = False)
		' ****************************************************************************************
		' This routine will check the Instruments grid and update the CustomDataArray for the 
		' two 'Special case' Custom fields : Sector & Liquidity
		'
		' Additionally, it will update any Custom Fields of type 'CustomXXX'.
		'
		' ****************************************************************************************

		Try
			' Get The Instrument (Returns) Grid.

			Dim GridInstruments As C1.Win.C1FlexGrid.C1FlexGrid
			Dim GridRowCounter As Integer
			Dim thisGridRow As C1.Win.C1FlexGrid.Row

			GridInstruments = _OptimiseForm.Grid_Returns

			Dim Col_PertracCode As Integer = GridInstruments.Cols("GroupPertracCode").SafeIndex
			Dim Col_Sector As Integer = GridInstruments.Cols("GroupSector").SafeIndex
			Dim Col_Liquidity As Integer = GridInstruments.Cols("GroupLiquidity").SafeIndex

			Dim ThisCustomFieldNumerator As Integer
			Dim ThisInstrumentNumerator As Integer
			Dim ThisPertracCode As Integer

			' Update Special fields

			' Sector

			If (CustomFieldNumerator.ContainsKey(CInt(SpecialConstraintFields.Sector))) Then
				ThisCustomFieldNumerator = CustomFieldNumerator(CInt(SpecialConstraintFields.Sector))

				' Clear 'Sector' Custom Field Data 
				For ThisInstrumentNumerator = 0 To (InstrumentNumerator.Count - 1)
					CustomFieldData(ThisCustomFieldNumerator, ThisInstrumentNumerator) = ""
				Next

				For GridRowCounter = 1 To (GridInstruments.Rows.Count - 1)
					thisGridRow = GridInstruments.Rows(GridRowCounter)

					If (thisGridRow(Col_PertracCode) IsNot Nothing) AndAlso (IsNumeric(thisGridRow(Col_PertracCode))) Then
						ThisPertracCode = CInt(thisGridRow(Col_PertracCode))

						If InstrumentNumerator.ContainsKey(ThisPertracCode) Then
							ThisInstrumentNumerator = InstrumentNumerator(ThisPertracCode)

							If (thisGridRow(Col_Sector) IsNot Nothing) Then
								CustomFieldData(ThisCustomFieldNumerator, ThisInstrumentNumerator) = CStr(thisGridRow(Col_Sector))
							End If

						End If

					End If

				Next
			End If

			' Liquidity

			If (CustomFieldNumerator.ContainsKey(CInt(SpecialConstraintFields.Liquidity))) Then
				ThisCustomFieldNumerator = CustomFieldNumerator(CInt(SpecialConstraintFields.Liquidity))

				' Clear 'Liquidity' Custom Field Data 
				For ThisInstrumentNumerator = 0 To (InstrumentNumerator.Count - 1)
					CustomFieldData(ThisCustomFieldNumerator, ThisInstrumentNumerator) = 0
				Next

				For GridRowCounter = 1 To (GridInstruments.Rows.Count - 1)
					thisGridRow = GridInstruments.Rows(GridRowCounter)

					If (Not thisGridRow.IsNew) AndAlso (thisGridRow(Col_PertracCode) IsNot Nothing) AndAlso (IsNumeric(thisGridRow(Col_PertracCode))) Then
						ThisPertracCode = CInt(thisGridRow(Col_PertracCode))

						If InstrumentNumerator.ContainsKey(ThisPertracCode) Then
							ThisInstrumentNumerator = InstrumentNumerator(ThisPertracCode)

							If (thisGridRow(Col_Liquidity) IsNot Nothing) AndAlso IsNumeric(thisGridRow(Col_Liquidity)) Then
								CustomFieldData(ThisCustomFieldNumerator, ThisInstrumentNumerator) = CDbl(thisGridRow(Col_Liquidity))
							End If

						End If

					End If

				Next
			End If

			' Update User-Entered Custom Fields

			If (pUpdateUserEnteredCustomFields) OrElse (pForceRefresh) Then
				Dim InstrumentsColumnCount As Integer
				Dim CustomFieldID As Integer
				Dim CustomFieldDefinition As DSPertracCustomFields.tblPertracCustomFieldsRow = Nothing
				Dim TmpObject As Object

				' Loop through Instruments grid columns

				For InstrumentsColumnCount = 2 To (GridInstruments.Cols.Count - 1)

					' If this Column is a custom Field...

					If (GridInstruments.Cols(InstrumentsColumnCount).Name.StartsWith("Custom_")) Then
						' Get Custom Field ID (From the column name)

						CustomFieldID = CInt(GridInstruments.Cols(InstrumentsColumnCount).Name.Substring(7))

						' Is this Custom Column required ? (Present in CustomFieldNumerator)

						If (CustomFieldNumerator.ContainsKey(CustomFieldID)) Then
							ThisCustomFieldNumerator = CustomFieldNumerator(CustomFieldID)
						Else
							ThisCustomFieldNumerator = (-1)
							CustomFieldID = 0
						End If

						' If this is a valid Custom Field (>0 and is required)

						If (CustomFieldID > 0) Then

							' Get Custom field details (Determine if this is a user-entered field)

							Try
								TmpObject = LookupTableRow(_OptimiseForm.MainForm, RenaissanceStandardDatasets.tblPertracCustomFields, CustomFieldID, "")
								If (TmpObject Is Nothing) Then
									CustomFieldDefinition = Nothing
								Else
									CustomFieldDefinition = CType(TmpObject, DSPertracCustomFields.tblPertracCustomFieldsRow)
								End If
							Catch ex As Exception
							End Try

							' Having got the Custom Field definition...

							If (CustomFieldDefinition IsNot Nothing) Then

								' If this is a User-Entered Custom Field then Get Custom Data off the Instruments form.

								' CustomFieldType is of type PertracCustomFieldTypes
								' FieldIsCalculated = False if CustomFieldType is defined in Enum 'PertracCustomFieldTypes_NotCalculated'

								If (Not CustomFieldDefinition.FieldIsCalculated) Then

									' Work through each Instrument Row, saving the Custom Field value from it...

									For GridRowCounter = 1 To (GridInstruments.Rows.Count - 1)
										thisGridRow = GridInstruments.Rows(GridRowCounter)

										' If this Row is not the blank one at the end (should not exist) and the Pertrac ID is OK, then...

										If (Not thisGridRow.IsNew) AndAlso (thisGridRow(Col_PertracCode) IsNot Nothing) AndAlso (IsNumeric(thisGridRow(Col_PertracCode))) Then
											ThisPertracCode = CInt(thisGridRow(Col_PertracCode))

											' If this Instrument exists in the Numerator (it should)

											If InstrumentNumerator.ContainsKey(ThisPertracCode) Then
												ThisInstrumentNumerator = InstrumentNumerator(ThisPertracCode)

												' Set Value in 'CustomFieldData'. NZ should return a zero style value for Null values.

												If (thisGridRow(InstrumentsColumnCount) IsNot Nothing) Then
													CustomFieldData(ThisCustomFieldNumerator, ThisInstrumentNumerator) = Nz(thisGridRow(InstrumentsColumnCount), GridInstruments.Cols(InstrumentsColumnCount).DataType)
												End If

											End If

										End If

									Next GridRowCounter

								End If

							End If

						End If

					End If

				Next InstrumentsColumnCount

			End If ' (pUpdateUserEnteredCustomFields) OrElse (pForceRefresh)

		Catch ex As Exception

		End Try

	End Sub

	Public Sub UpdateConstraints(Optional ByVal pForceRefresh As Boolean = False)
		' ****************************************************************************************
		' Routine to Construct the Marginals Constraint Array
		'
		' ****************************************************************************************

		If (Not pForceRefresh) AndAlso (Not ConstraintsAreInvalid) Then
			Exit Sub
		End If

		If (InstrumentsAreInvalid) Then
			LoadInstrumentDetails()
		End If

		If (ConstraintDetailsAreInvalid) Then
			LoadConstraintDetails()
		End If

		If (CustomFieldDataIsInvalid) Then
			LoadCustomFieldData()
		End If

		RefreshUserEnteredCustomData()

		Try

			' Initialise Constraints Grid
			' Private ConstraintArray(-1, -1) As Double

			Dim InstrumentCounter As Integer

			If (ConstraintDetailsArray.Length > 0) AndAlso (InstrumentNumerator.Count > 0) Then
				' Two Constraint lines per Constraint defined - One each for Upper and Lower Limits

				ReDim MarginalsConstraintArray((ConstraintDetailsArray.Length * 2), InstrumentNumerator.Count + 1)
				_ConstraintTypeColumn = InstrumentNumerator.Count
				_ConstraintLimitColumn = _ConstraintTypeColumn + 1

			Else
				' No Constraints Defined or no Instruments defined.

				ReDim MarginalsConstraintArray(0, Math.Max(InstrumentNumerator.Count, 0) + 1)
				_ConstraintTypeColumn = Math.Max(InstrumentNumerator.Count, 0)
				_ConstraintLimitColumn = _ConstraintTypeColumn + 1

				ConstraintsAreInvalid = False

				' Set Control Constraint : Sum(Weight) = 1.0
				' This is always on Row Zero

				For InstrumentCounter = 0 To (Math.Max(InstrumentNumerator.Count, 0) - 1)
					MarginalsConstraintArray(0, InstrumentCounter) = 1.0
				Next

				MarginalsConstraintArray(0, _ConstraintTypeColumn) = ConstraintType.ctEqualTo
				MarginalsConstraintArray(0, _ConstraintLimitColumn) = 1.0

				Exit Sub

			End If

			' Get List Of Custom Field IDs
			' CustomFieldNumerator should contain all Custom Field IDs currently in use.

			Dim CustomFieldIDs(CustomFieldNumerator.Keys.Count - 1) As Integer
			CustomFieldNumerator.Keys.CopyTo(CustomFieldIDs, 0)

			' Get List Of Instrument IDs
			' InstrumentNumerator should contain all Instrument IDs currently in use.

			Dim InstrumentIDs(InstrumentNumerator.Keys.Count - 1) As Integer
			InstrumentNumerator.Keys.CopyTo(InstrumentIDs, 0)

			' Cycle Through 

			Dim ConstraintDetailCounter As Integer
			Dim ThisConstraintDetail As ConstraintDetailClass

			Dim SpecificInstrumentID As Integer
			Dim ThisInstrumentID As Integer
			Dim ThisInstrumentNumerator As Integer
			Dim ThisCustomFieldNumerator As Integer

			Dim ThisConstraintTestValue As Object = Nothing
			Dim ThisCustomFieldValue As Object = Nothing

			Dim ConstraintIndexLowerLimit As Integer
			Dim ConstraintIndexUpperLimit As Integer

			' Set Control Constraint : Sum(Weight) = 1.0
			' This is always on Row Zero

			For InstrumentCounter = 0 To (_ConstraintTypeColumn - 1)
				MarginalsConstraintArray(0, InstrumentCounter) = 1.0
			Next

			MarginalsConstraintArray(0, _ConstraintTypeColumn) = ConstraintType.ctEqualTo
			MarginalsConstraintArray(0, _ConstraintLimitColumn) = 1.0

			' Loop Through the defined constraints.

			For ConstraintDetailCounter = 0 To (ConstraintDetailsArray.Length - 1)

				' Get Constraint Detail.

				ThisConstraintDetail = ConstraintDetailsArray(ConstraintDetailCounter)

				' Calculate ConstraintArray Indices.

				ConstraintIndexLowerLimit = (ConstraintDetailCounter * 2) + 1
				ConstraintIndexUpperLimit = ConstraintIndexLowerLimit + 1

				' Get Constraint Test Value

				ThisConstraintTestValue = ThisConstraintDetail.TestValue

				' Set Constraint Types (< or >) and Weight Limits.

				MarginalsConstraintArray(ConstraintIndexLowerLimit, _ConstraintTypeColumn) = ConstraintType.ctGreaterThan
				MarginalsConstraintArray(ConstraintIndexUpperLimit, _ConstraintTypeColumn) = ConstraintType.ctLessThan

				MarginalsConstraintArray(ConstraintIndexLowerLimit, _ConstraintLimitColumn) = ThisConstraintDetail.LowerLimit
				MarginalsConstraintArray(ConstraintIndexUpperLimit, _ConstraintLimitColumn) = ThisConstraintDetail.UpperLimit

				' Set Instrument Weightings (0 or 1)

				If (ThisConstraintDetail.CustomFieldID = CInt(SpecialConstraintFields.IndividualInstrumentLimit)) Then
					' Upper and Lower Limits...
					'
					' For specific Instrument Limits, the Constraint weights are Zero, except for the specified Instrument
					' which is 1.0

					SpecificInstrumentID = CInt(ThisConstraintTestValue)

					For InstrumentCounter = 0 To (InstrumentIDs.Length - 1)

						ThisInstrumentID = InstrumentIDs(InstrumentCounter)
						ThisInstrumentNumerator = InstrumentNumerator(ThisInstrumentID)

						If (ThisInstrumentID = SpecificInstrumentID) Then
							MarginalsConstraintArray(ConstraintIndexLowerLimit, ThisInstrumentNumerator) = 1.0
							MarginalsConstraintArray(ConstraintIndexUpperLimit, ThisInstrumentNumerator) = 1.0
						Else
							MarginalsConstraintArray(ConstraintIndexLowerLimit, ThisInstrumentNumerator) = 0
							MarginalsConstraintArray(ConstraintIndexUpperLimit, ThisInstrumentNumerator) = 0
						End If

					Next

				Else
					' For General Constraints the Instrument weights are set to 1.0 if the Custom data value tests
					' True, otherwise the weight is Zero.

					ThisCustomFieldNumerator = CustomFieldNumerator(ThisConstraintDetail.CustomFieldID)

					For InstrumentCounter = 0 To (InstrumentIDs.Length - 1)

						ThisInstrumentID = InstrumentIDs(InstrumentCounter)
						ThisInstrumentNumerator = InstrumentNumerator(ThisInstrumentID)

						ThisCustomFieldValue = CustomFieldData(ThisCustomFieldNumerator, ThisInstrumentNumerator)

						' Now, Does this Instrument satisfy the demands of the constraint ?

						If ThisConstraintDetail.TestConstraint(ThisCustomFieldValue, ValueComparer) Then
							' Test Value OK
							MarginalsConstraintArray(ConstraintIndexLowerLimit, ThisInstrumentNumerator) = 1.0
							MarginalsConstraintArray(ConstraintIndexUpperLimit, ThisInstrumentNumerator) = 1.0
						Else
							MarginalsConstraintArray(ConstraintIndexLowerLimit, ThisInstrumentNumerator) = 0
							MarginalsConstraintArray(ConstraintIndexUpperLimit, ThisInstrumentNumerator) = 0
						End If

					Next

				End If

			Next

		Catch ex As Exception
		End Try

	End Sub

#End Region

#Region " Marginals Code"

	Friend Function CheckMarginalsLimits(ByRef ConstraintStatusArray() As String, ByVal pMarginalsPertracID As Integer, ByVal pMarginalsContraID As Integer, ByRef pMarginalsGrid As C1.Win.C1FlexGrid.C1FlexGrid, ByVal pPertracCol As Integer, ByVal pWeightCol As Integer, Optional ByVal ExcludeInstrumentBreaks As Boolean = False, Optional ByVal ExcludeSectorBreaks As Boolean = False, Optional ByVal ExcludeLiquidityBreaks As Boolean = False, Optional ByVal ExcludeCustomLimitBreaks As Boolean = False) As MarginalsStatusEnum
		' ****************************************************************************************
		' Routine desined to test the given Marginals Grid against the established constraint set.
		'
		' Note :
		'		Specific Instrument Limits are only checked if they relate to the given InstrumentID or ContraID.
		'		The return value will indicate all limits broken but the 'ConstraintStatusArray()' will only 
		'		contain break messages for limits not Excluded by the ExcludeInstrumentBreaks, ExcludeSectorBreaks, 
		'		ExcludeLiquidityBreaks and ExcludeCustomLimitBreaks parameters.
		'		It is intended that this enables you to see limit breaks which have not been excluded.
		'
		' ****************************************************************************************

		Dim ReturnArrayList As New ArrayList
		Dim ReturnValue As MarginalsStatusEnum

		Dim MarginalsPertracID_Numerator As Integer
		Dim MarginalsContraID_Numerator As Integer

		ReturnValue = MarginalsStatusEnum.OK

		Try
			ConstraintStatusArray = Nothing

			'  Basic Checks...

			If (pMarginalsGrid Is Nothing) OrElse (pMarginalsGrid.Rows.Count <= 0) Then
				Return ReturnValue
			End If

			If (pPertracCol < 0) OrElse (pPertracCol >= pMarginalsGrid.Cols.Count) Then
				Return ReturnValue
			End If

			If (pWeightCol < 0) OrElse (pWeightCol >= pMarginalsGrid.Cols.Count) Then
				Return ReturnValue
			End If

			If (InstrumentNumerator Is Nothing) OrElse (InstrumentNumerator.Count <= 0) Then
				Return ReturnValue
			End If

			If (ConstraintDetailsArray Is Nothing) OrElse (ConstraintDetailsArray.Length <= 0) Then
				' This ignores the Control Constraint : Sum(Weight) = 1.0
				' not deemed to be necessary for the Marginals process.
				Return ReturnValue
			End If

			' This function assumes that the Constraint Worker is upto date.

			' OK...
			' 1) Build Array of Instrument Weights ordered to match the ConstraintsArray()
			' 2) Compare
			' 3) Return limit breaks (if any)

			If InstrumentNumerator.ContainsKey(pMarginalsPertracID) Then
				MarginalsPertracID_Numerator = InstrumentNumerator(pMarginalsPertracID)
			Else
				MarginalsPertracID_Numerator = (-1)
			End If

			If (pMarginalsContraID > 0) AndAlso (InstrumentNumerator.ContainsKey(pMarginalsContraID)) Then
				MarginalsContraID_Numerator = InstrumentNumerator(pMarginalsContraID)
			Else
				MarginalsContraID_Numerator = (-1)
			End If

			' 1) Get Weights

			Dim WeightsArray() As Double
			WeightsArray = InstrumentWeights(pMarginalsGrid, pPertracCol, pWeightCol)

			'Dim WeightsArray(InstrumentNumerator.Count - 1) As Double

			'Dim GridRowCounter As Integer
			'Dim thisGridRow As C1.Win.C1FlexGrid.Row
			'Dim ThisPertracID As Integer
			'Dim ThisInstrumentNumerator As Integer

			'For GridRowCounter = 0 To (pMarginalsGrid.Rows.Count - 1)
			'	Try
			'		thisGridRow = pMarginalsGrid.Rows(GridRowCounter)

			'		If (Not thisGridRow.IsNew) Then
			'			ThisPertracID = CInt(thisGridRow(pPertracCol))

			'			If InstrumentNumerator.ContainsKey(ThisPertracID) Then

			'				ThisInstrumentNumerator = InstrumentNumerator(ThisPertracID)

			'				WeightsArray(ThisInstrumentNumerator) = CDbl(thisGridRow(pWeightCol))

			'			End If

			'		End If
			'	Catch ex As Exception
			'	End Try
			'Next

			' 2) Compare

			Dim ConstraintCounter As Integer
			Dim InstrumentCounter As Integer
			Dim SumWeight As Double
			Dim ConstraintFailed As Boolean
			Dim OmitThisConstraint As Boolean

			Dim ConstraintDetailCounter As Integer
			Dim ThisConstraintDetail As ConstraintDetailClass
			Dim BreakText As String = ""
			Dim ConstraintDescriptionText As String = ""

			For ConstraintCounter = 1 To (MarginalsConstraintArray.GetLength(0) - 1)
				SumWeight = 0.0
				ConstraintFailed = False
				OmitThisConstraint = False

				' Get Constraint Detail.

				ConstraintDetailCounter = ((ConstraintCounter - 1) \ 2)
				ThisConstraintDetail = ConstraintDetailsArray(ConstraintDetailCounter)

				' Check Constraint Omisions

				' For Marginals, Only check the Constraint to see if it relates to the Marginals Instruments involved.

				'If (ThisConstraintDetail.CustomFieldID = SpecialConstraintFields.IndividualInstrumentLimit) Then
				OmitThisConstraint = True

				If (MarginalsConstraintArray(ConstraintCounter, MarginalsPertracID_Numerator) <> 0) Then
					OmitThisConstraint = False
				ElseIf (MarginalsContraID_Numerator >= 0) AndAlso (MarginalsConstraintArray(ConstraintCounter, MarginalsContraID_Numerator) <> 0) Then
					OmitThisConstraint = False
				End If

				'ElseIf (pIgnoreSectorLimits) AndAlso (ThisConstraintDetail.CustomFieldID = SpecialConstraintFields.Sector) Then
				'	OmitThisConstraint = True
				'ElseIf (pIgnoreLiquiditylimits) AndAlso (ThisConstraintDetail.CustomFieldID = SpecialConstraintFields.Liquidity) Then
				'	OmitThisConstraint = True
				'ElseIf (pIgnoreCustomFieldLimits) AndAlso (ThisConstraintDetail.CustomFieldID > 0) Then
				'	OmitThisConstraint = True
				'End If

				' Check Constraint 

				If Not OmitThisConstraint Then

					' Sum MatchingInstruments

					For InstrumentCounter = 0 To (MarginalsConstraintArray.GetLength(1) - 3)	' (-3 to Strip off Constraint Type and Constraint weight columns) '   InstrumentNumerator.Count - 1)
						SumWeight += WeightsArray(InstrumentCounter) * MarginalsConstraintArray(ConstraintCounter, InstrumentCounter)
					Next

					' Check Limit

					SumWeight = Math.Round(SumWeight, 8)

					If (MarginalsConstraintArray(ConstraintCounter, _ConstraintTypeColumn) = ConstraintType.ctGreaterThan) Then
						If (SumWeight < MarginalsConstraintArray(ConstraintCounter, _ConstraintLimitColumn)) Then
							BreakText = "< " & MarginalsConstraintArray(ConstraintCounter, _ConstraintLimitColumn).ToString("#,##0.00%")
							ConstraintFailed = True
						End If
					ElseIf (MarginalsConstraintArray(ConstraintCounter, _ConstraintTypeColumn) = ConstraintType.ctLessThan) Then
						If (SumWeight > MarginalsConstraintArray(ConstraintCounter, _ConstraintLimitColumn)) Then
							BreakText = "> " & MarginalsConstraintArray(ConstraintCounter, _ConstraintLimitColumn).ToString("#,##0.00%")
							ConstraintFailed = True
						End If
					ElseIf (MarginalsConstraintArray(ConstraintCounter, _ConstraintTypeColumn) = ConstraintType.ctEqualTo) Then
						If (Math.Abs(SumWeight - MarginalsConstraintArray(ConstraintCounter, _ConstraintLimitColumn)) > 0.0000001) Then
							' Allow for imprecision of Double type numbers.
							BreakText = "Not Equal to " & MarginalsConstraintArray(ConstraintCounter, _ConstraintLimitColumn).ToString("#,##0.00%")
							ConstraintFailed = True
						End If
					End If

					' Constraint Failed ?

					If ConstraintFailed Then

						ConstraintDescriptionText = " "
						If (ThisConstraintDetail.TestLessThan) Then
							ConstraintDescriptionText &= "<"
						End If
						If (ThisConstraintDetail.TestEqualTo) Then
							ConstraintDescriptionText &= "="
						End If
						If (ThisConstraintDetail.TestGreaterThan) Then
							ConstraintDescriptionText = ">"
						End If

						If ThisConstraintDetail.CustomFieldID = SpecialConstraintFields.IndividualInstrumentLimit Then
							If Not ExcludeInstrumentBreaks Then
								ReturnArrayList.Add(CStr(ThisConstraintDetail.CustomFieldName & " weight of " & SumWeight.ToString("#,##0.00%") & " is " & BreakText))
							End If

							ReturnValue = ReturnValue Or MarginalsStatusEnum.InstrumentBreak

						Else
							Dim TestValueString As String

							If (ThisConstraintDetail.CustomFieldDataType = RenaissanceDataType.TextType) Then
								TestValueString = ThisConstraintDetail.TestValue.ToString
							ElseIf (ThisConstraintDetail.CustomFieldDataType = RenaissanceDataType.DateType) Then
								TestValueString = CDate(ThisConstraintDetail.TestValue).ToString(DISPLAYMEMBER_DATEFORMAT)
							ElseIf (ThisConstraintDetail.CustomFieldDataType = RenaissanceDataType.PercentageType) Then
								TestValueString = CDbl(ThisConstraintDetail.TestValue).ToString("#,##0.00%")
							ElseIf (ThisConstraintDetail.CustomFieldDataType = RenaissanceDataType.NumericType) Then
								TestValueString = CDbl(ThisConstraintDetail.TestValue).ToString("#,##0.00")
							ElseIf (ThisConstraintDetail.CustomFieldDataType = RenaissanceDataType.BooleanType) Then
								Try
									If CBool(ThisConstraintDetail.TestValue) Then
										TestValueString = "True"
									Else
										TestValueString = "False"
									End If
								Catch ex As Exception
									TestValueString = ThisConstraintDetail.TestValue.ToString()
								End Try
							Else
								TestValueString = ThisConstraintDetail.TestValue.ToString
							End If

							If ThisConstraintDetail.CustomFieldID = SpecialConstraintFields.Sector Then
								ReturnValue = ReturnValue Or MarginalsStatusEnum.SectorBreak

								If Not ExcludeSectorBreaks Then
									ReturnArrayList.Add(CStr(ThisConstraintDetail.CustomFieldName & ConstraintDescriptionText & " " & TestValueString & ", weight of " & SumWeight.ToString("#,##0.00%") & " is " & BreakText))
								End If

							ElseIf ThisConstraintDetail.CustomFieldID = SpecialConstraintFields.Liquidity Then
								ReturnValue = ReturnValue Or MarginalsStatusEnum.LiquidityBreak

								If Not ExcludeLiquidityBreaks Then
									ReturnArrayList.Add(CStr(ThisConstraintDetail.CustomFieldName & ConstraintDescriptionText & " " & TestValueString & ", weight of " & SumWeight.ToString("#,##0.00%") & " is " & BreakText))
								End If
							Else
								ReturnValue = ReturnValue Or MarginalsStatusEnum.CustomFieldBreak

								If Not ExcludeCustomLimitBreaks Then
									ReturnArrayList.Add(CStr(ThisConstraintDetail.CustomFieldName & ConstraintDescriptionText & " " & TestValueString & ", weight of " & SumWeight.ToString("#,##0.00%") & " is " & BreakText))
								End If
							End If

						End If

					End If

				End If

			Next

			' Set Return Array

			If (ReturnArrayList.Count > 0) Then
				ConstraintStatusArray = ReturnArrayList.ToArray(GetType(String))
			End If

		Catch ex As Exception
		Finally

			If (ConstraintStatusArray Is Nothing) Then
				ConstraintStatusArray = Array.CreateInstance(GetType(String), 0)
			End If

		End Try

		Return ReturnValue

	End Function

#End Region

#Region " Optimiser Initialisation Code."

	Public Sub OptimiserConstraintArray(ByRef pConstraintsArray(,) As Double, ByRef pConstraintValues() As Double, ByRef pInstrumentCount As Integer, ByRef pOptimiserInstrumentNumerator As Dictionary(Of Integer, Integer), ByRef pSlackVariableCount As Integer, ByRef pOriginalConstraintCount As Integer, ByRef pRunUnconstrained As Boolean) Implements MarkowitzClass.OptimiserClass.IConstraintWorkerInterface.OptimiserConstraintArray
		' **********************************************************************
		'
		'
		' **********************************************************************

		' Refresh Constraint worker data if necessary...

		If (InstrumentsAreInvalid) Then
			LoadInstrumentDetails()
		End If

		If (ConstraintDetailsAreInvalid) Then
			LoadConstraintDetails()
		End If

		If (CustomFieldDataIsInvalid) Then
			LoadCustomFieldData()
		End If

		RefreshUserEnteredCustomData()

		' Validate Instrument Numerator.

		If (pOptimiserInstrumentNumerator Is Nothing) Then
			pOptimiserInstrumentNumerator = New Dictionary(Of Integer, Integer)
		End If

		' No Instruments, Exit

		If (InstrumentNumerator.Count <= 0) Then
			pConstraintsArray = Array.CreateInstance(GetType(Double), 0, 0)
			pConstraintValues = Array.CreateInstance(GetType(Double), 0)
			pInstrumentCount = 0
			pSlackVariableCount = 0
			pOriginalConstraintCount = 0

			Exit Sub
		End If

		' Declare Return values

		Dim ConstraintsArray(,) As Double
		Dim ConstraintValues() As Double

		' Get List Of Instrument IDs
		' InstrumentNumerator should contain all Instrument IDs currently in use.

		Dim InstrumentIDs(InstrumentNumerator.Keys.Count - 1) As Integer

		InstrumentNumerator.Keys.CopyTo(InstrumentIDs, 0)
		pInstrumentCount = Math.Max(InstrumentNumerator.Count, 0)

		' 

		Dim SlackVariableCount As Integer = 0
		Dim thisSlackVariableCount As Integer = 0
		Dim SlackIndex As Integer

		Dim OriginalConstraintCount As Integer = 0
		Dim thisOriginalConstraintCount As Integer = 0

		Dim ConstraintDetailIndex As Integer
		Dim ConstraintArrayIndex As Integer
		Dim ConstraintIndexLowerLimit As Integer
		Dim ConstraintIndexUpperLimit As Integer

		Dim InstrumentCounter As Integer
		Dim ThisInstrumentID As Integer
		Dim ThisInstrumentNumerator As Integer
		Dim ThisCustomFieldNumerator As Integer

		Dim ThisCustomFieldValue As Object = Nothing

		Dim ThisConstraintDetail As ConstraintDetailClass

		' Set pOptimiserInstrumentNumerator

		For InstrumentCounter = 0 To (InstrumentIDs.Length - 1)
			ThisInstrumentID = InstrumentIDs(InstrumentCounter)

			pOptimiserInstrumentNumerator.Add(ThisInstrumentID, InstrumentNumerator(ThisInstrumentID))
		Next

		' OK, Count the Number of 'Real' Constraints and the number of required slack variables
		' Set variables : thisOriginalConstraintCount, thisSlackVariableCount

		Try
			If (Not pRunUnconstrained) Then

				For ConstraintDetailIndex = 0 To (ConstraintDetailsArray.Length - 1)
					ThisConstraintDetail = ConstraintDetailsArray(ConstraintDetailIndex)

					' Ignore constraint details relating to Upper and Lower Limits, they
					' are recorded in the optimiser seperately.

					If (ThisConstraintDetail IsNot Nothing) AndAlso (Not (ThisConstraintDetail.CustomFieldID = CInt(SpecialConstraintFields.IndividualInstrumentLimit))) Then

						' What type of Constraint ?

						If (Math.Abs(ThisConstraintDetail.LowerLimit - ThisConstraintDetail.UpperLimit) < 0.000001) Then

							' This is an Equality Constraint.

							thisOriginalConstraintCount += 1

						Else

							' It is a range limit, i.e. a 'Greater Than XX' and a 'Less Than YY' Limit.
							' This counts as Two original limits plus Two slack variables (required to convert '<' or '>' limits to '=' limits)

							thisOriginalConstraintCount += 2
							thisSlackVariableCount += 2

						End If

					End If

				Next

			End If
		Catch ex As Exception
		End Try

		' OK, dimension constraint array.
		'
		' I am dimensioning it Intentionally large so it does not need to be re-dimensioned later.
		'
		' Consistent with the original Optimiser code, all arrays are deemed to be base '1'.

		ConstraintsArray = Array.CreateInstance(GetType(Double), (thisOriginalConstraintCount + 2), (pInstrumentCount + thisSlackVariableCount + thisOriginalConstraintCount + 2))
		ConstraintValues = Array.CreateInstance(GetType(Double), (thisOriginalConstraintCount + 2))
		SlackVariableCount = thisSlackVariableCount
		OriginalConstraintCount = thisOriginalConstraintCount

		' Now, Set Constraint Values

		Try
			ConstraintArrayIndex = 1 ' Starting Constraint Row - note '1'.
			thisSlackVariableCount = 0
			SlackIndex = (pInstrumentCount + 1)

			' Set Control Constraint : Sum(Weight) = 1.0

			For InstrumentCounter = 1 To pInstrumentCount
				ConstraintsArray(1, InstrumentCounter) = 1.0
			Next

			ConstraintValues(1) = 1

			ConstraintArrayIndex += 1

			' User Constraints 

			If (Not pRunUnconstrained) Then

				For ConstraintDetailIndex = 0 To (ConstraintDetailsArray.Length - 1)

					ThisConstraintDetail = ConstraintDetailsArray(ConstraintDetailIndex)

					'If Not (ThisConstraintDetail.CustomFieldID = CInt(SpecialConstraintFields.IndividualInstrumentLimit)) Then
					If (ThisConstraintDetail IsNot Nothing) AndAlso (Not (ThisConstraintDetail.CustomFieldID = CInt(SpecialConstraintFields.IndividualInstrumentLimit))) Then

						If (ThisConstraintDetail.CustomFieldID = CInt(SpecialConstraintFields.IndividualInstrumentLimit)) Then

							''Dim ThisConstraintTestValue As Object = Nothing
							''ThisConstraintTestValue = ThisConstraintDetail.TestValue
							'Dim SpecificInstrumentID As Integer
							'SpecificInstrumentID = CInt(ThisConstraintDetail.TestValue)

							'If (Math.Abs(ThisConstraintDetail.LowerLimit - ThisConstraintDetail.UpperLimit) < 0.000001) Then
							'	' This is an Equality Constraint.

							'	For InstrumentCounter = 0 To (InstrumentIDs.Length - 1)

							'		ThisInstrumentID = InstrumentIDs(InstrumentCounter)
							'		ThisInstrumentNumerator = InstrumentNumerator(ThisInstrumentID)

							'		If (ThisInstrumentID = SpecificInstrumentID) Then
							'			ConstraintsArray(ConstraintIndexLowerLimit, ThisInstrumentNumerator + 1) = 1.0
							'		Else
							'			ConstraintsArray(ConstraintIndexLowerLimit, ThisInstrumentNumerator + 1) = 0
							'		End If

							'	Next

							'	ConstraintValues(ConstraintIndexLowerLimit) = ThisConstraintDetail.LowerLimit

							'	ConstraintArrayIndex += 1

							'Else

							'	For InstrumentCounter = 0 To (InstrumentIDs.Length - 1)

							'		ThisInstrumentID = InstrumentIDs(InstrumentCounter)
							'		ThisInstrumentNumerator = InstrumentNumerator(ThisInstrumentID)

							'		If (ThisInstrumentID = SpecificInstrumentID) Then
							'			ConstraintsArray(ConstraintIndexLowerLimit, ThisInstrumentNumerator + 1) = (-1.0)	' Convert '>' to '<'
							'			ConstraintsArray(ConstraintIndexUpperLimit, ThisInstrumentNumerator + 1) = 1.0
							'		Else
							'			ConstraintsArray(ConstraintIndexLowerLimit, ThisInstrumentNumerator + 1) = 0
							'			ConstraintsArray(ConstraintIndexUpperLimit, ThisInstrumentNumerator + 1) = 0
							'		End If

							'	Next

							'	' Set Constraint Values for these two constraints.

							'	ConstraintValues(ConstraintIndexLowerLimit) = (-ThisConstraintDetail.LowerLimit)
							'	ConstraintValues(ConstraintIndexUpperLimit) = ThisConstraintDetail.UpperLimit

							'	' Set Constraint Weighting for the Two new slack variables.

							'	ConstraintsArray(ConstraintIndexLowerLimit, SlackIndex) = 1
							'	ConstraintsArray(ConstraintIndexUpperLimit, SlackIndex + 1) = 1

							'	' Increment Counters.

							'	ConstraintArrayIndex += 2
							'	thisSlackVariableCount += 2
							'	SlackIndex += 2

							'End If

						Else
							' Not Individual Limit

							ThisCustomFieldNumerator = CustomFieldNumerator(ThisConstraintDetail.CustomFieldID)

							' What type of Constraint ?

							If (Math.Abs(ThisConstraintDetail.LowerLimit - ThisConstraintDetail.UpperLimit) < 0.000001) Then
								' This is an Equality Constraint.

								ConstraintIndexLowerLimit = ConstraintArrayIndex

								For InstrumentCounter = 0 To (InstrumentIDs.Length - 1)

									ThisInstrumentID = InstrumentIDs(InstrumentCounter)
									ThisInstrumentNumerator = InstrumentNumerator(ThisInstrumentID)

									ThisCustomFieldValue = CustomFieldData(ThisCustomFieldNumerator, ThisInstrumentNumerator)

									' Now, Does this Instrument satisfy the demands of the constraint ?

									If ThisConstraintDetail.TestConstraint(ThisCustomFieldValue, ValueComparer) Then
										' Test Value OK
										ConstraintsArray(ConstraintIndexLowerLimit, ThisInstrumentNumerator + 1) = 1.0
									Else
										ConstraintsArray(ConstraintIndexLowerLimit, ThisInstrumentNumerator + 1) = 0
									End If

								Next

								ConstraintValues(ConstraintIndexLowerLimit) = ThisConstraintDetail.LowerLimit

								ConstraintArrayIndex += 1

							Else
								' It is a range limit, i.e. a 'Greater Than XX' and a 'Less Than YY' Limit.
								' This counts as Two original limits plus Two slack variables (required to convert '<' or '>' limits to '=' limits)
								' Also, Convert '<' and '>' constraints to Equality constraints.

								' thisOriginalConstraintCount += 2
								' thisSlackVariableCount += 2

								ConstraintIndexLowerLimit = ConstraintArrayIndex
								ConstraintIndexUpperLimit = ConstraintArrayIndex + 1

								For InstrumentCounter = 0 To (InstrumentIDs.Length - 1)

									ThisInstrumentID = InstrumentIDs(InstrumentCounter)
									ThisInstrumentNumerator = InstrumentNumerator(ThisInstrumentID)

									ThisCustomFieldValue = CustomFieldData(ThisCustomFieldNumerator, ThisInstrumentNumerator)

									' Now, Does this Instrument satisfy the demands of the constraint ?

									If ThisConstraintDetail.TestConstraint(ThisCustomFieldValue, ValueComparer) Then
										' Test Value OK

										ConstraintsArray(ConstraintIndexLowerLimit, ThisInstrumentNumerator + 1) = (-1.0)	' Convert '>' to '<'
										ConstraintsArray(ConstraintIndexUpperLimit, ThisInstrumentNumerator + 1) = 1.0

									Else
										' This instrument is not included in this constraint.

										ConstraintsArray(ConstraintIndexLowerLimit, ThisInstrumentNumerator + 1) = 0
										ConstraintsArray(ConstraintIndexUpperLimit, ThisInstrumentNumerator + 1) = 0

									End If

								Next

								' Set Constraint Values for these two constraints.

								ConstraintValues(ConstraintIndexLowerLimit) = (-ThisConstraintDetail.LowerLimit)
								ConstraintValues(ConstraintIndexUpperLimit) = ThisConstraintDetail.UpperLimit

								' Set Constraint Weighting for the Two new slack variables.

								ConstraintsArray(ConstraintIndexLowerLimit, SlackIndex) = 1
								ConstraintsArray(ConstraintIndexUpperLimit, SlackIndex + 1) = 1

								' Increment Counters.

								ConstraintArrayIndex += 2
								thisSlackVariableCount += 2
								SlackIndex += 2
							End If
						End If

					End If ' Not an Instrument Specific Constraint (Handled by Upper and Lower limits)

				Next ' Constraint

			End If ' (Not pRunUnconstrained) 

		Catch ex As Exception
		End Try

		' Set return Values.

		pConstraintsArray = ConstraintsArray
		pConstraintValues = ConstraintValues
		pSlackVariableCount = SlackVariableCount
		pOriginalConstraintCount = (OriginalConstraintCount + 1) ' +1 for the Default Constraint.

	End Sub

	Public Function OptimiserExpectedReturns(ByRef pExpectedReturns() As Double, ByRef pStdErrors() As Double, ByVal NumberOfRealSecurities As Integer, ByVal NumberOfSlackVariables As Integer, ByVal NumberOfConstraints As Integer) As Integer Implements MarkowitzClass.OptimiserClass.IConstraintWorkerInterface.OptimiserExpectedReturns
		' **********************************************************************
		' Return an array of expected returns.
		' 
		' The Array is dimensioned to be big enough for any use by the optimiser, the 
		' Order of returns is as dictated by the InstrumentNumerator.
		'
		' **********************************************************************

		Dim ExpectedReturns() As Double
		Dim StdErrors() As Double
		Dim GridInstruments As C1.Win.C1FlexGrid.C1FlexGrid
		Dim GridRowCounter As Integer
		Dim thisGridRow As C1.Win.C1FlexGrid.Row
		Dim ThisPertracCode As Integer
		Dim ThisReturn As Double
		Dim ThisStdError As Double

		GridInstruments = _OptimiseForm.Grid_Returns

		'Dim Col_GroupPercent As Integer = GridInstruments.Cols("GroupPercent").SafeIndex
		'Dim Col_GroupNewPercent As Integer = GridInstruments.Cols("GroupNewPercent").SafeIndex
		Dim Col_Return As Integer = GridInstruments.Cols("GroupExpectedReturn").SafeIndex
		Dim Col_StdError As Integer = GridInstruments.Cols("GroupStdErr").SafeIndex
		Dim Col_PertracCode As Integer = GridInstruments.Cols("GroupPertracCode").SafeIndex

		Try

			ExpectedReturns = Array.CreateInstance(GetType(Double), (NumberOfRealSecurities + NumberOfSlackVariables + NumberOfConstraints + 1))
			StdErrors = Array.CreateInstance(GetType(Double), (NumberOfRealSecurities + NumberOfSlackVariables + NumberOfConstraints + 1))

			For GridRowCounter = 1 To (GridInstruments.Rows.Count - 1)
				thisGridRow = GridInstruments.Rows(GridRowCounter)

				If (Not thisGridRow.IsNew) Then
					If (thisGridRow(Col_PertracCode) IsNot Nothing) AndAlso (IsNumeric(thisGridRow(Col_PertracCode))) Then
						ThisPertracCode = CInt(thisGridRow(Col_PertracCode))
						ThisReturn = CDbl(Nz(thisGridRow(Col_Return), 0))
						ThisStdError = CDbl(Nz(thisGridRow(Col_StdError), 0))

						If (InstrumentNumerator.ContainsKey(ThisPertracCode)) Then

							ExpectedReturns(InstrumentNumerator(ThisPertracCode) + 1) = ThisReturn
							StdErrors(InstrumentNumerator(ThisPertracCode) + 1) = ThisStdError

						Else
							' Error !

							Return (-1)
						End If

					End If
				End If

			Next

		Catch ex As Exception

			Return (-1)

		End Try

		' Set return parameters

		pExpectedReturns = ExpectedReturns
		pStdErrors = StdErrors

		If (ExpectedReturns IsNot Nothing) Then
			Return (ExpectedReturns.Length)
		End If

		Return (-1)

	End Function

	Public Function OptimiserUpperAndLowerLimits(ByRef pLowerLimit() As Double, ByRef pUpperLimit() As Double, ByRef NumberOfRealSecurities As Integer, ByVal NumberOfSlackVariables As Integer, ByVal NumberOfConstraints As Integer, ByVal pRunUnconstrained As Boolean) As Integer Implements MarkowitzClass.OptimiserClass.IConstraintWorkerInterface.OptimiserUpperAndLowerLimits
		' **********************************************************************
		'
		'
		' **********************************************************************

		Dim LowerLimits() As Double = Nothing
		Dim UpperLimits() As Double = Nothing

		Dim GridInstruments As C1.Win.C1FlexGrid.C1FlexGrid
		Dim GridRowCounter As Integer
		Dim thisGridRow As C1.Win.C1FlexGrid.Row
		Dim ThisPertracCode As Integer
		Dim ThisLowerLimit As Double
		Dim ThisUpperLimit As Double
		Dim LimitIndex As Integer

		GridInstruments = _OptimiseForm.Grid_Returns

		Dim Col_PertracCode As Integer = GridInstruments.Cols("GroupPertracCode").SafeIndex
		Dim Col_GroupUpperBound As Integer = GridInstruments.Cols("GroupUpperBound").SafeIndex
		Dim Col_GroupLowerBound As Integer = GridInstruments.Cols("GroupLowerBound").SafeIndex

		Try
			' Initialise Arrays

			LowerLimits = Array.CreateInstance(GetType(Double), NumberOfRealSecurities + NumberOfSlackVariables + NumberOfConstraints + 1)
			UpperLimits = Array.CreateInstance(GetType(Double), NumberOfRealSecurities + NumberOfSlackVariables + NumberOfConstraints + 1)

			If (pRunUnconstrained) Then
				' Set UnConstrained Instrument limits. Still limited to be >= 0 though !

				For LimitIndex = 1 To NumberOfRealSecurities
					LowerLimits(LimitIndex) = 0
					UpperLimits(LimitIndex) = INFINITY
				Next

			Else

				For GridRowCounter = 1 To (GridInstruments.Rows.Count - 1)
					thisGridRow = GridInstruments.Rows(GridRowCounter)

					If (Not thisGridRow.IsNew) Then
						If (thisGridRow(Col_PertracCode) IsNot Nothing) AndAlso (IsNumeric(thisGridRow(Col_PertracCode))) Then
							ThisPertracCode = CInt(thisGridRow(Col_PertracCode))

							If (InstrumentNumerator.ContainsKey(ThisPertracCode)) Then

								Try
									ThisLowerLimit = CDbl(Nz(thisGridRow(Col_GroupLowerBound), 0))
									LowerLimits(InstrumentNumerator(ThisPertracCode) + 1) = ThisLowerLimit
								Catch ex As Exception
									' Exit with Error 
									Return (-1)
								End Try

								Try
									ThisUpperLimit = CDbl(Nz(thisGridRow(Col_GroupUpperBound), 0))
									UpperLimits(InstrumentNumerator(ThisPertracCode) + 1) = ThisUpperLimit
								Catch ex As Exception
									' Exit with Error 
									Return (-1)
								End Try

							Else
								' Error !

								Return (-1)
							End If

						End If
					End If

				Next

			End If

			' Set Default Upper Limits for Slack Variables.

			For LimitIndex = (NumberOfRealSecurities + 1) To (NumberOfRealSecurities + NumberOfSlackVariables)
				LowerLimits(LimitIndex) = 0
				UpperLimits(LimitIndex) = INFINITY
			Next

		Catch ex As Exception

		End Try

		pLowerLimit = LowerLimits
		pUpperLimit = UpperLimits

		If (LowerLimits IsNot Nothing) Then
			Return (LowerLimits.Length)
		End If

		Return (-1)

	End Function

	Public Function OptimiserCovarianceMatrix(ByRef pCovarianceMatrix(,) As Double, ByRef NumberOfRealSecurities As Integer, ByVal NumberOfSlackVariables As Integer, ByVal NumberOfConstraints As Integer) As Integer Implements MarkowitzClass.OptimiserClass.IConstraintWorkerInterface.OptimiserCovarianceMatrix
		' **********************************************************************
		'
		' Read the Optimiser form Covariance Grid into the Optimiser covariance Matrix.
		' Allow for the fact that the Instruments may not be in the same order.
		'
		' **********************************************************************


		Dim CovarianceMatrix(,) As Double = Nothing
		Dim GridCovariance As C1.Win.C1FlexGrid.C1FlexGrid
		Dim GridMapping() As Integer
		Dim GridRowCounter As Integer
		Dim thisGridRow As C1.Win.C1FlexGrid.Row
		Dim ThisPertracCode As Integer
		Dim ThisPertracColumn As Integer
		Dim CovarianceMatrixSize As Integer

		GridCovariance = _OptimiseForm.Grid_Covariance

		If (GridCovariance Is Nothing) OrElse (GridCovariance.Rows.Count <= 1) Then
			Return (-1)
		End If

		Try
			' NumberOfRealSecurities + NumberOfSlackVariables + (2 *  NumberOfConstraints)
			' Remember Base offset of '1'.

			CovarianceMatrixSize = NumberOfRealSecurities + NumberOfSlackVariables + NumberOfConstraints + NumberOfConstraints + 1

			CovarianceMatrix = Array.CreateInstance(GetType(Double), CovarianceMatrixSize, CovarianceMatrixSize)

			ReDim GridMapping(GridCovariance.Rows.Count)

			ThisPertracColumn = (GridCovariance.Cols.Count - 1)
			For GridRowCounter = 1 To (GridCovariance.Rows.Count - 1)
				GridMapping(GridRowCounter) = 0

				thisGridRow = GridCovariance.Rows(GridRowCounter)

				If (Not thisGridRow.IsNew) Then
					If (thisGridRow(ThisPertracColumn) IsNot Nothing) AndAlso (IsNumeric(thisGridRow(ThisPertracColumn))) Then
						ThisPertracCode = CInt(thisGridRow(ThisPertracColumn))

						If (InstrumentNumerator.ContainsKey(ThisPertracCode)) Then
							GridMapping(GridRowCounter) = InstrumentNumerator(ThisPertracCode) + 1
						End If

					End If
				End If

			Next

			' Populate Matrix

			Dim GridColCounter As Integer

			For GridRowCounter = 1 To (GridCovariance.Rows.Count - 1)

				thisGridRow = GridCovariance.Rows(GridRowCounter)

				If (Not thisGridRow.IsNew) AndAlso (GridMapping(GridRowCounter) > 0) Then

					For GridColCounter = 1 To (GridCovariance.Cols.Count - 2)
						If (GridMapping(GridColCounter) > 0) Then

							CovarianceMatrix(GridMapping(GridRowCounter), GridMapping(GridColCounter)) = CDbl(Nz(thisGridRow.Item(GridColCounter), 0))

						End If
					Next
				End If
			Next

		Catch ex As Exception

		End Try

		pCovarianceMatrix = CovarianceMatrix

		If (CovarianceMatrix IsNot Nothing) Then
			Return (CovarianceMatrix.GetLength(0))
		End If

		Return (-1)

	End Function

	Public Function CopyMarginalsConstraintArray() As Double(,) Implements OptimiserClass.IConstraintWorkerInterface.CopyMarginalsConstraintArray
		' **********************************************************************
		'
		'
		' **********************************************************************

		Dim RVal As Double(,) = Nothing

		Try

			' MarginalsConstraintArray

			If (MarginalsConstraintArray IsNot Nothing) AndAlso (MarginalsConstraintArray.Rank = 2) Then
				If (MarginalsConstraintArray.GetLength(0) > 0) AndAlso (MarginalsConstraintArray.GetLength(1) > 0) Then
					ReDim RVal(MarginalsConstraintArray.GetLength(0) - 1, MarginalsConstraintArray.GetLength(1) - 1)

					Array.Copy(MarginalsConstraintArray, RVal, MarginalsConstraintArray.Length)
				End If
			End If

		Catch ex As Exception
		End Try

		Return RVal

	End Function

#End Region

End Class
