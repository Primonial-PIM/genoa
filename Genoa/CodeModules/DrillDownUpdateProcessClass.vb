﻿Option Strict On

Imports RenaissanceGlobals.Globals

Friend Class DrillDownUpdateProcessClass
  ' *****************************************************************************************************
  ' DrillDownUpdateProcessClass 
  ' ---------------------------
  '
  ' 
  '
  ' *****************************************************************************************************

  Inherits NaplesGlobals.ThreadWrapperBase

  Private _CloseDownFlag As Boolean
  Private _DrillDownUpdateQueue As Queue(Of DynamicGroupUpdateTaskClass)
  Private _DrillDownChartUpdateQueue As Queue(Of DynamicGroupUpdateTaskClass)
  Private _StatFunctions As RenaissanceStatFunctions.StatFunctions
  Private _SampleSize As Integer

  Private ActiveWorkItem As DynamicGroupUpdateTaskClass

  Private ActiveWorkItemLock As New Object

  Private Const DEFAULT_SAMPLE_SIZE As Integer = 1000
  Public Const BIG_GROUP_RESCHEDULE_THRESHOLD As Integer = 200

  ' Public Event GroupStatsUpdateEvent(ByVal sender As Object, ByVal UpdateTask As DynamicGroupUpdateTaskClass, ByVal UpdateResults As RenaissanceStatFunctions.StatFunctions.SeriesStatsClass)

  Private Sub New()
  End Sub

  Public Sub New(ByRef pDrillDownUpdateQueue As Queue(Of DynamicGroupUpdateTaskClass), ByRef pDrillDownChartQueue As Queue(Of DynamicGroupUpdateTaskClass), ByRef pMyStatFunctions As RenaissanceStatFunctions.StatFunctions)
    MyBase.New()
    Me.Thread.Name = "DrillDownUpdateProcess"

    _DrillDownUpdateQueue = pDrillDownUpdateQueue
    _DrillDownChartUpdateQueue = pDrillDownChartQueue
    _StatFunctions = pMyStatFunctions
    _SampleSize = DEFAULT_SAMPLE_SIZE

  End Sub

  Public Sub New(ByRef pDrillDownUpdateQueue As Queue(Of DynamicGroupUpdateTaskClass), ByRef pDrillDownChartQueue As Queue(Of DynamicGroupUpdateTaskClass), ByRef pMyStatFunctions As RenaissanceStatFunctions.StatFunctions, ByVal GroupSampleSize As Integer)
    MyBase.New()
    Me.Thread.Name = "DrillDownUpdateProcess"

    _DrillDownUpdateQueue = pDrillDownUpdateQueue
    _DrillDownChartUpdateQueue = pDrillDownChartQueue
    _StatFunctions = pMyStatFunctions
    If (GroupSampleSize > 0) Then
      _SampleSize = Math.Max(20, GroupSampleSize)
    Else
      _SampleSize = (-1)
    End If

  End Sub

  Public Property CloseDown() As Boolean
    Get
      Return _CloseDownFlag
    End Get
    Set(ByVal Value As Boolean)
      _CloseDownFlag = Value
    End Set
  End Property

  Protected Overrides Sub DoTask()

    Initialise()

    RunServer()

    TidyUp()

  End Sub

  Private Sub Initialise()
    ' Initialise Data Server

    _CloseDownFlag = False


  End Sub

  Private Sub TidyUp()

    ' On ending the Server

    Try



    Catch ex As Exception
    End Try


  End Sub

  Private Sub RunServer()

    ' ******************************************************
    ' Work loop.
    '
    ' Give priority to Chart Work Queue.
    ' ******************************************************

    Dim UpdateResults As RenaissanceStatFunctions.StatFunctions.SeriesStatsClass = Nothing
    Dim WorkItemRescheduled_DoNotClear As Boolean

    'Dim DebugPause As Boolean = False

    If (_DrillDownUpdateQueue Is Nothing) OrElse (_DrillDownChartUpdateQueue Is Nothing) OrElse (_StatFunctions Is Nothing) Then
      Exit Sub
    End If

    While (Not _CloseDownFlag)

      Try
        If (_DrillDownUpdateQueue.Count > 0) OrElse (_DrillDownChartUpdateQueue.Count > 0) Then

          SyncLock ActiveWorkItemLock

            ActiveWorkItem = Nothing

            SyncLock _DrillDownChartUpdateQueue
              If (_DrillDownChartUpdateQueue.Count > 0) Then

                ActiveWorkItem = _DrillDownChartUpdateQueue.Dequeue

                'While (DebugPause)
                '  Threading.Thread.Sleep(10000)
                'End While

              End If
            End SyncLock

            If (ActiveWorkItem Is Nothing) Then
              SyncLock _DrillDownUpdateQueue
                If (_DrillDownUpdateQueue.Count > 0) Then

                  ActiveWorkItem = _DrillDownUpdateQueue.Dequeue

                  'While (DebugPause)
                  '  Threading.Thread.Sleep(10000)
                  'End While

                End If
              End SyncLock
            End If

          End SyncLock

          ' Debug.Print("Processing : " & RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(CUInt(ThisWorkItem.GroupID)).ToString & ", Interim=" & ThisWorkItem.InterimUpdate.ToString & ", GroupSize=" & ThisWorkItem.GroupSize.ToString & ", SampleSize=" & ThisWorkItem.SampleSize.ToString)
          Dim this_IsFormActive As Boolean
          Dim this_IsChartUpdate As Boolean
          Dim this_IsCancelled As Boolean
          Dim this_WorkItemIsNull As Boolean
          Dim this_InterimUpdate As Boolean
          Dim this_GroupSize As Integer
          Dim this_SampleSize As Integer
          Dim this_RescheduleThisUpdate As Boolean
          Dim this_GroupID As Integer
          Dim this_StatsDatePeriod As RenaissanceGlobals.DealingPeriod

          SyncLock ActiveWorkItemLock
            If (ActiveWorkItem Is Nothing) Then
              this_WorkItemIsNull = True
            Else
              this_WorkItemIsNull = False

              this_IsFormActive = ActiveWorkItem.IsFormActive
              this_IsChartUpdate = ActiveWorkItem.IsChartUpdate
              this_IsCancelled = ActiveWorkItem.Cancelled
              this_InterimUpdate = ActiveWorkItem.InterimUpdate
              this_GroupSize = ActiveWorkItem.GroupSize
              this_SampleSize = ActiveWorkItem.SampleSize
              this_RescheduleThisUpdate = ActiveWorkItem.RescheduleThisUpdate
              this_GroupID = ActiveWorkItem.GroupID
              this_StatsDatePeriod = ActiveWorkItem.StatsDatePeriod

            End If

          End SyncLock


          If (Not this_WorkItemIsNull) Then

            Try

              ' Do Work
              WorkItemRescheduled_DoNotClear = False

              If (Not this_IsCancelled) Then

                If (this_IsFormActive) Then

                  If (Not this_InterimUpdate) AndAlso (this_GroupSize > BIG_GROUP_RESCHEDULE_THRESHOLD) AndAlso (Not this_RescheduleThisUpdate) Then
                    SyncLock ActiveWorkItemLock

                      this_RescheduleThisUpdate = SetRescheduleRequestFlagsIfRequired()
                      ActiveWorkItem.RescheduleThisUpdate = this_RescheduleThisUpdate

                    End SyncLock
                  End If

                  If (this_RescheduleThisUpdate) Then
                    ' Clear Update flag and re-queue.

                    ' Debug.Print("Reschedule : " & RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(CUInt(ThisWorkItem.GroupID)).ToString & ", Interim=" & ThisWorkItem.InterimUpdate.ToString & ", GroupSize=" & ThisWorkItem.GroupSize.ToString & ", SampleSize=" & ThisWorkItem.SampleSize.ToString)

                    SyncLock ActiveWorkItemLock

                      ActiveWorkItem.RescheduleThisUpdate = False
                      this_RescheduleThisUpdate = False
                      _DrillDownUpdateQueue.Enqueue(ActiveWorkItem)
                      WorkItemRescheduled_DoNotClear = True

                    End SyncLock


                  Else
                    ' Execute Task
                    ' If the given sample size is <= 0, then use the standard sample size.
                    ' Else Use the given sample size, subject to a minimum of 20.

                    If (this_SampleSize <= 0) Then

                      UpdateResults = _StatFunctions.GetSimpleStats(this_StatsDatePeriod, CUInt(this_GroupID), False, Renaissance_BaseDate, ActiveWorkItem.CustomStatsStartDate, ActiveWorkItem.CustomStatsEndDate, Now.Date.AddMonths(-1), 0.0#, 1.0#, _SampleSize)

                    Else

                      UpdateResults = _StatFunctions.GetSimpleStats(this_StatsDatePeriod, CUInt(this_GroupID), False, Renaissance_BaseDate, ActiveWorkItem.CustomStatsStartDate, ActiveWorkItem.CustomStatsEndDate, Now.Date.AddMonths(-1), 0.0#, 1.0#, Math.Max(20, this_SampleSize))

                    End If

                    ' Clear the Stats cache of Interim related Items.

                    If (this_InterimUpdate) Then
                      _StatFunctions.ClearCache(False, False, False, True, False, this_GroupID.ToString)
                    End If

                    ' Invoke Update, if Form is still active

                    SyncLock ActiveWorkItemLock
                      this_IsFormActive = ActiveWorkItem.IsFormActive
                      this_IsCancelled = ActiveWorkItem.Cancelled
                    End SyncLock

                    If (this_IsFormActive) AndAlso (Not this_IsCancelled) Then

                      Try

                        If (this_IsChartUpdate) Then
                          ' For Chart Updates, check there is no subsequent request.

                          If (Not ChartRequestExists(this_GroupID)) Then
                            ActiveWorkItem.PostUpdates(this_GroupID, this_InterimUpdate, UpdateResults)
                          End If

                        Else
                          ActiveWorkItem.PostUpdates(this_GroupID, this_InterimUpdate, UpdateResults)
                        End If

                      Catch ex As Exception
                      End Try

                    End If


                  End If

                End If


              Else

                ' Debug.Print("Omit Cancel: " & RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(CUInt(ThisWorkItem.GroupID)).ToString & ", Interim=" & ThisWorkItem.InterimUpdate.ToString & ", GroupSize=" & ThisWorkItem.GroupSize.ToString & ", SampleSize=" & ThisWorkItem.SampleSize.ToString)

              End If

            Catch ex As Exception
            Finally
              If (Not WorkItemRescheduled_DoNotClear) Then
                SyncLock ActiveWorkItemLock

                  ActiveWorkItem.Clear()

                End SyncLock
              End If

            End Try

          End If

        Else
          Threading.Thread.Sleep(100)
        End If

      Catch ex As Exception
      End Try

    End While

  End Sub

  Friend Function SetRescheduleRequestFlagsIfRequired() As Boolean
    ' ********************************************************************************
    ' Check the work queue to see if there are any small group work items being blocked
    ' by big work items.
    ' If there are, then flag the big work items for re-scheduling.
    ' ********************************************************************************
    Dim RescheduleRequired As Boolean = False
    Dim HaveFoundABigGroup As Boolean = False

    Try

      SyncLock _DrillDownUpdateQueue
        Dim ThisTask As DynamicGroupUpdateTaskClass
        Dim TaskArray() As DynamicGroupUpdateTaskClass

        TaskArray = _DrillDownUpdateQueue.ToArray()

        For Each ThisTask In TaskArray

          If (Not ThisTask.Cancelled) AndAlso (Not ThisTask.InterimUpdate) AndAlso (ThisTask.GroupSize >= BIG_GROUP_RESCHEDULE_THRESHOLD) Then
            HaveFoundABigGroup = True
          End If

          If (Not ThisTask.Cancelled) AndAlso (HaveFoundABigGroup) AndAlso (ThisTask.GroupSize < BIG_GROUP_RESCHEDULE_THRESHOLD) Then
            RescheduleRequired = True
          End If

        Next

        If (RescheduleRequired) Then

          For Each ThisTask In TaskArray

            If (Not ThisTask.Cancelled) AndAlso (Not ThisTask.InterimUpdate) AndAlso (ThisTask.GroupSize >= BIG_GROUP_RESCHEDULE_THRESHOLD) Then
              ThisTask.RescheduleThisUpdate = True
            End If

          Next

        End If

      End SyncLock

    Catch ex As Exception
    End Try

    Return RescheduleRequired

  End Function

  Friend Sub PostNewChartUpdateRequest(ByVal pStatsDatePeriod As RenaissanceGlobals.DealingPeriod, ByVal GroupID As Integer, ByRef UpdateForm As StandardGenoaForm, ByVal UpdateDelegate As GroupStatsUpdateDelegate, ByVal pGroupSize As Integer, ByVal pSampleSize As Integer, ByVal pInterimUpdate As Boolean)
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    SyncLock _DrillDownChartUpdateQueue
      _DrillDownChartUpdateQueue.Enqueue(New DynamicGroupUpdateTaskClass(GroupID, New StandardGenoaForm() {UpdateForm}, New GroupStatsUpdateDelegate() {UpdateDelegate}, pGroupSize, pSampleSize, pInterimUpdate, True, Renaissance_BaseDate, Renaissance_EndDate_Data, pStatsDatePeriod))
    End SyncLock

  End Sub

  Friend Sub ClearGroupChartUpdateRequest(ByVal GroupID() As Integer, ByRef UpdateForm As StandardGenoaForm)
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    SyncLock _DrillDownChartUpdateQueue

      Try

        SyncLock ActiveWorkItemLock
          If (ActiveWorkItem IsNot Nothing) AndAlso (GroupID.Contains(ActiveWorkItem.GroupID)) Then
            ActiveWorkItem.Cancelled = True
          End If
        End SyncLock

      Catch ex As Exception
      End Try

      Dim ThisTask As DynamicGroupUpdateTaskClass
      Dim TaskArray() As DynamicGroupUpdateTaskClass

      Try

        TaskArray = _DrillDownChartUpdateQueue.ToArray()

        For Each ThisTask In TaskArray
          If GroupID.Contains(ThisTask.StrippedGroupID) Then
            ThisTask.Cancelled = True

            ' Debug.Print("Mark Cancel: " & RenaissancePertracDataClass.PertracDataClass.GetInstrumentFromID(CUInt(ThisTask.GroupID)).ToString & ", Interim=" & ThisTask.InterimUpdate.ToString & ", GroupSize=" & ThisTask.GroupSize.ToString & ", SampleSize=" & ThisTask.SampleSize.ToString)

          End If
        Next

      Catch ex As Exception
      End Try

    End SyncLock

  End Sub

  Friend Function ChartRequestExists(ByVal GroupID As Integer) As Boolean
    ' ********************************************************************************
    '
    '
    ' ********************************************************************************

    Try

      SyncLock _DrillDownChartUpdateQueue

        Dim ThisTask As DynamicGroupUpdateTaskClass
        Dim TaskArray() As DynamicGroupUpdateTaskClass

        TaskArray = _DrillDownChartUpdateQueue.ToArray()

        For Each ThisTask In TaskArray
          If GroupID = ThisTask.StrippedGroupID Then
            Return True
          End If
        Next

      End SyncLock

    Catch ex As Exception
    End Try

    Return False

  End Function

End Class
