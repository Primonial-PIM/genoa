Imports System.Data.SqlClient
Imports RenaissanceGlobals


Public Class frmGroupReport

	Inherits System.Windows.Forms.Form
	Implements StandardGenoaForm

#Region " Windows Form Designer generated code "

	Private Sub New()
		MyBase.New()

		'This call is required by the Windows Form Designer.
		InitializeComponent()

		'Add any initialization after the InitializeComponent() call

	End Sub

	'Form overrides dispose to clean up the component list.
	Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
		If disposing Then
			If Not (components Is Nothing) Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(disposing)
	End Sub

	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer

	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.  
	'Do not modify it using the code editor.
	Friend WithEvents btnRunReport As System.Windows.Forms.Button
	Friend WithEvents btnClose As System.Windows.Forms.Button
	Friend WithEvents Status1 As System.Windows.Forms.StatusStrip
	Friend WithEvents StatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
	Friend WithEvents ToolStripProgressBar1 As System.Windows.Forms.ToolStripProgressBar
	Friend WithEvents Combo_SelectFrom As System.Windows.Forms.ComboBox
	Friend WithEvents Radio_Pertrac As System.Windows.Forms.RadioButton
	Friend WithEvents Radio_ExistingGroups As System.Windows.Forms.RadioButton
	Friend WithEvents List_SelectItems As System.Windows.Forms.ListBox
	Friend WithEvents Radio_ReportOnGroup As System.Windows.Forms.RadioButton
	Friend WithEvents Radio_ReportOnSelected As System.Windows.Forms.RadioButton
	Friend WithEvents Panel_SelectFrom As System.Windows.Forms.Panel
	Friend WithEvents List_SelectedGroups As System.Windows.Forms.ListBox
	Friend WithEvents Combo_SelectGroup As System.Windows.Forms.ComboBox
	Friend WithEvents Split_GroupReport As System.Windows.Forms.SplitContainer
	Friend WithEvents Check_CustomField As System.Windows.Forms.CheckBox
	Friend WithEvents Combo_CustomField As System.Windows.Forms.ComboBox
	Friend WithEvents Combo_GroupByCustomField As System.Windows.Forms.ComboBox
	Friend WithEvents Check_GroupByCustomField As System.Windows.Forms.CheckBox
	Friend WithEvents Check_OrderByName As System.Windows.Forms.CheckBox
	Friend WithEvents Radio_ExistingFunds As System.Windows.Forms.RadioButton
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Me.btnRunReport = New System.Windows.Forms.Button
		Me.btnClose = New System.Windows.Forms.Button
		Me.Status1 = New System.Windows.Forms.StatusStrip
		Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar
		Me.StatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel
		Me.Combo_SelectFrom = New System.Windows.Forms.ComboBox
		Me.Radio_Pertrac = New System.Windows.Forms.RadioButton
		Me.Radio_ExistingGroups = New System.Windows.Forms.RadioButton
		Me.Radio_ExistingFunds = New System.Windows.Forms.RadioButton
		Me.List_SelectItems = New System.Windows.Forms.ListBox
		Me.Radio_ReportOnGroup = New System.Windows.Forms.RadioButton
		Me.Radio_ReportOnSelected = New System.Windows.Forms.RadioButton
		Me.Panel_SelectFrom = New System.Windows.Forms.Panel
		Me.Combo_SelectGroup = New System.Windows.Forms.ComboBox
		Me.List_SelectedGroups = New System.Windows.Forms.ListBox
		Me.Split_GroupReport = New System.Windows.Forms.SplitContainer
		Me.Check_CustomField = New System.Windows.Forms.CheckBox
		Me.Combo_CustomField = New System.Windows.Forms.ComboBox
		Me.Combo_GroupByCustomField = New System.Windows.Forms.ComboBox
		Me.Check_GroupByCustomField = New System.Windows.Forms.CheckBox
		Me.Check_OrderByName = New System.Windows.Forms.CheckBox
		Me.Status1.SuspendLayout()
		Me.Panel_SelectFrom.SuspendLayout()
		Me.Split_GroupReport.Panel1.SuspendLayout()
		Me.Split_GroupReport.Panel2.SuspendLayout()
		Me.Split_GroupReport.SuspendLayout()
		Me.SuspendLayout()
		'
		'btnRunReport
		'
		Me.btnRunReport.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnRunReport.Location = New System.Drawing.Point(109, 510)
		Me.btnRunReport.Name = "btnRunReport"
		Me.btnRunReport.Size = New System.Drawing.Size(148, 28)
		Me.btnRunReport.TabIndex = 7
		Me.btnRunReport.Text = "Run Report"
		'
		'btnClose
		'
		Me.btnClose.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.btnClose.Location = New System.Drawing.Point(325, 510)
		Me.btnClose.Name = "btnClose"
		Me.btnClose.Size = New System.Drawing.Size(75, 28)
		Me.btnClose.TabIndex = 8
		Me.btnClose.Text = "&Close"
		'
		'Status1
		'
		Me.Status1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripProgressBar1, Me.StatusLabel1})
		Me.Status1.Location = New System.Drawing.Point(0, 546)
		Me.Status1.Name = "Status1"
		Me.Status1.Size = New System.Drawing.Size(594, 22)
		Me.Status1.TabIndex = 0
		Me.Status1.Text = "StatusStrip1"
		'
		'ToolStripProgressBar1
		'
		Me.ToolStripProgressBar1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right
		Me.ToolStripProgressBar1.Maximum = 20
		Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
		Me.ToolStripProgressBar1.Size = New System.Drawing.Size(150, 16)
		Me.ToolStripProgressBar1.Step = 1
		Me.ToolStripProgressBar1.Visible = False
		'
		'StatusLabel1
		'
		Me.StatusLabel1.Name = "StatusLabel1"
		Me.StatusLabel1.Size = New System.Drawing.Size(0, 17)
		'
		'Combo_SelectFrom
		'
		Me.Combo_SelectFrom.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_SelectFrom.Location = New System.Drawing.Point(191, 3)
		Me.Combo_SelectFrom.Name = "Combo_SelectFrom"
		Me.Combo_SelectFrom.Size = New System.Drawing.Size(390, 21)
		Me.Combo_SelectFrom.TabIndex = 1
		'
		'Radio_Pertrac
		'
		Me.Radio_Pertrac.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_Pertrac.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_Pertrac.Location = New System.Drawing.Point(3, 3)
		Me.Radio_Pertrac.Name = "Radio_Pertrac"
		Me.Radio_Pertrac.Size = New System.Drawing.Size(124, 21)
		Me.Radio_Pertrac.TabIndex = 0
		Me.Radio_Pertrac.TabStop = True
		Me.Radio_Pertrac.Text = "Pertrac Instrument"
		Me.Radio_Pertrac.UseVisualStyleBackColor = True
		'
		'Radio_ExistingGroups
		'
		Me.Radio_ExistingGroups.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_ExistingGroups.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_ExistingGroups.Location = New System.Drawing.Point(3, 30)
		Me.Radio_ExistingGroups.Name = "Radio_ExistingGroups"
		Me.Radio_ExistingGroups.Size = New System.Drawing.Size(124, 21)
		Me.Radio_ExistingGroups.TabIndex = 1
		Me.Radio_ExistingGroups.TabStop = True
		Me.Radio_ExistingGroups.Text = "Genoa Group"
		Me.Radio_ExistingGroups.UseVisualStyleBackColor = True
		'
		'Radio_ExistingFunds
		'
		Me.Radio_ExistingFunds.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_ExistingFunds.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_ExistingFunds.Location = New System.Drawing.Point(3, 57)
		Me.Radio_ExistingFunds.Name = "Radio_ExistingFunds"
		Me.Radio_ExistingFunds.Size = New System.Drawing.Size(124, 21)
		Me.Radio_ExistingFunds.TabIndex = 2
		Me.Radio_ExistingFunds.TabStop = True
		Me.Radio_ExistingFunds.Text = "Existing Position"
		Me.Radio_ExistingFunds.UseVisualStyleBackColor = True
		'
		'List_SelectItems
		'
		Me.List_SelectItems.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.List_SelectItems.FormattingEnabled = True
		Me.List_SelectItems.Location = New System.Drawing.Point(191, 30)
		Me.List_SelectItems.Name = "List_SelectItems"
		Me.List_SelectItems.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
		Me.List_SelectItems.Size = New System.Drawing.Size(390, 212)
		Me.List_SelectItems.Sorted = True
		Me.List_SelectItems.TabIndex = 3
		'
		'Radio_ReportOnGroup
		'
		Me.Radio_ReportOnGroup.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_ReportOnGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_ReportOnGroup.Location = New System.Drawing.Point(1, 2)
		Me.Radio_ReportOnGroup.Name = "Radio_ReportOnGroup"
		Me.Radio_ReportOnGroup.Size = New System.Drawing.Size(181, 21)
		Me.Radio_ReportOnGroup.TabIndex = 0
		Me.Radio_ReportOnGroup.TabStop = True
		Me.Radio_ReportOnGroup.Text = "Report on Group(s)"
		Me.Radio_ReportOnGroup.UseVisualStyleBackColor = True
		'
		'Radio_ReportOnSelected
		'
		Me.Radio_ReportOnSelected.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Radio_ReportOnSelected.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Radio_ReportOnSelected.Location = New System.Drawing.Point(1, 3)
		Me.Radio_ReportOnSelected.Name = "Radio_ReportOnSelected"
		Me.Radio_ReportOnSelected.Size = New System.Drawing.Size(181, 21)
		Me.Radio_ReportOnSelected.TabIndex = 0
		Me.Radio_ReportOnSelected.TabStop = True
		Me.Radio_ReportOnSelected.Text = "Report on Selected Instruments"
		Me.Radio_ReportOnSelected.UseVisualStyleBackColor = True
		'
		'Panel_SelectFrom
		'
		Me.Panel_SelectFrom.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Panel_SelectFrom.Controls.Add(Me.Radio_ExistingFunds)
		Me.Panel_SelectFrom.Controls.Add(Me.Radio_ExistingGroups)
		Me.Panel_SelectFrom.Controls.Add(Me.Radio_Pertrac)
		Me.Panel_SelectFrom.Location = New System.Drawing.Point(47, 30)
		Me.Panel_SelectFrom.Name = "Panel_SelectFrom"
		Me.Panel_SelectFrom.Size = New System.Drawing.Size(135, 89)
		Me.Panel_SelectFrom.TabIndex = 2
		'
		'Combo_SelectGroup
		'
		Me.Combo_SelectGroup.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_SelectGroup.FlatStyle = System.Windows.Forms.FlatStyle.System
		Me.Combo_SelectGroup.Location = New System.Drawing.Point(191, 2)
		Me.Combo_SelectGroup.Name = "Combo_SelectGroup"
		Me.Combo_SelectGroup.Size = New System.Drawing.Size(390, 21)
		Me.Combo_SelectGroup.TabIndex = 1
		'
		'List_SelectedGroups
		'
		Me.List_SelectedGroups.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.List_SelectedGroups.DisplayMember = "GroupListName"
		Me.List_SelectedGroups.FormattingEnabled = True
		Me.List_SelectedGroups.Location = New System.Drawing.Point(191, 28)
		Me.List_SelectedGroups.Name = "List_SelectedGroups"
		Me.List_SelectedGroups.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended
		Me.List_SelectedGroups.Size = New System.Drawing.Size(390, 108)
		Me.List_SelectedGroups.Sorted = True
		Me.List_SelectedGroups.TabIndex = 2
		Me.List_SelectedGroups.ValueMember = "GroupListID"
		'
		'Split_GroupReport
		'
		Me.Split_GroupReport.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
								Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Split_GroupReport.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.Split_GroupReport.Location = New System.Drawing.Point(2, 2)
		Me.Split_GroupReport.Margin = New System.Windows.Forms.Padding(1)
		Me.Split_GroupReport.Name = "Split_GroupReport"
		Me.Split_GroupReport.Orientation = System.Windows.Forms.Orientation.Horizontal
		'
		'Split_GroupReport.Panel1
		'
		Me.Split_GroupReport.Panel1.Controls.Add(Me.Combo_SelectGroup)
		Me.Split_GroupReport.Panel1.Controls.Add(Me.List_SelectedGroups)
		Me.Split_GroupReport.Panel1.Controls.Add(Me.Radio_ReportOnGroup)
		'
		'Split_GroupReport.Panel2
		'
		Me.Split_GroupReport.Panel2.Controls.Add(Me.Panel_SelectFrom)
		Me.Split_GroupReport.Panel2.Controls.Add(Me.List_SelectItems)
		Me.Split_GroupReport.Panel2.Controls.Add(Me.Radio_ReportOnSelected)
		Me.Split_GroupReport.Panel2.Controls.Add(Me.Combo_SelectFrom)
		Me.Split_GroupReport.Size = New System.Drawing.Size(588, 410)
		Me.Split_GroupReport.SplitterDistance = 146
		Me.Split_GroupReport.TabIndex = 1
		'
		'Check_CustomField
		'
		Me.Check_CustomField.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.Check_CustomField.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_CustomField.Location = New System.Drawing.Point(10, 428)
		Me.Check_CustomField.Name = "Check_CustomField"
		Me.Check_CustomField.Size = New System.Drawing.Size(176, 17)
		Me.Check_CustomField.TabIndex = 2
		Me.Check_CustomField.Text = "Show Custom Field"
		Me.Check_CustomField.UseVisualStyleBackColor = True
		'
		'Combo_CustomField
		'
		Me.Combo_CustomField.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_CustomField.Enabled = False
		Me.Combo_CustomField.Location = New System.Drawing.Point(195, 426)
		Me.Combo_CustomField.Name = "Combo_CustomField"
		Me.Combo_CustomField.Size = New System.Drawing.Size(390, 21)
		Me.Combo_CustomField.TabIndex = 3
		'
		'Combo_GroupByCustomField
		'
		Me.Combo_GroupByCustomField.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
								Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
		Me.Combo_GroupByCustomField.Enabled = False
		Me.Combo_GroupByCustomField.Location = New System.Drawing.Point(195, 453)
		Me.Combo_GroupByCustomField.Name = "Combo_GroupByCustomField"
		Me.Combo_GroupByCustomField.Size = New System.Drawing.Size(390, 21)
		Me.Combo_GroupByCustomField.TabIndex = 5
		'
		'Check_GroupByCustomField
		'
		Me.Check_GroupByCustomField.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.Check_GroupByCustomField.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_GroupByCustomField.Location = New System.Drawing.Point(10, 455)
		Me.Check_GroupByCustomField.Name = "Check_GroupByCustomField"
		Me.Check_GroupByCustomField.Size = New System.Drawing.Size(176, 17)
		Me.Check_GroupByCustomField.TabIndex = 4
		Me.Check_GroupByCustomField.Text = "Group By Custom Field"
		Me.Check_GroupByCustomField.UseVisualStyleBackColor = True
		'
		'Check_OrderByName
		'
		Me.Check_OrderByName.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
		Me.Check_OrderByName.CheckAlign = System.Drawing.ContentAlignment.MiddleRight
		Me.Check_OrderByName.Location = New System.Drawing.Point(10, 481)
		Me.Check_OrderByName.Name = "Check_OrderByName"
		Me.Check_OrderByName.Size = New System.Drawing.Size(176, 17)
		Me.Check_OrderByName.TabIndex = 6
		Me.Check_OrderByName.Text = "Order By Instrument Name"
		Me.Check_OrderByName.UseVisualStyleBackColor = True
		'
		'frmGroupReport
		'
		Me.AutoScaleBaseSize = New System.Drawing.Size(5, 13)
		Me.ClientSize = New System.Drawing.Size(594, 568)
		Me.Controls.Add(Me.Check_OrderByName)
		Me.Controls.Add(Me.Combo_GroupByCustomField)
		Me.Controls.Add(Me.Check_GroupByCustomField)
		Me.Controls.Add(Me.Combo_CustomField)
		Me.Controls.Add(Me.Check_CustomField)
		Me.Controls.Add(Me.Split_GroupReport)
		Me.Controls.Add(Me.Status1)
		Me.Controls.Add(Me.btnClose)
		Me.Controls.Add(Me.btnRunReport)
		Me.MaximizeBox = False
		Me.MinimizeBox = False
		Me.MinimumSize = New System.Drawing.Size(450, 250)
		Me.Name = "frmGroupReport"
		Me.Text = "Genoa Group Comparison Report"
		Me.Status1.ResumeLayout(False)
		Me.Status1.PerformLayout()
		Me.Panel_SelectFrom.ResumeLayout(False)
		Me.Split_GroupReport.Panel1.ResumeLayout(False)
		Me.Split_GroupReport.Panel2.ResumeLayout(False)
		Me.Split_GroupReport.ResumeLayout(False)
		Me.ResumeLayout(False)
		Me.PerformLayout()

	End Sub

#End Region

#Region " Form Locals and Constants "


	' Form 'Parent', the Main Venice form.
	' Generally only accessed through the 'MainForm' property.
	Private WithEvents _MainForm As GenoaMain

	' Form ToolTip
	Private FormTooltip As New ToolTip()

	' Form Constants, specific to the table being updated.

	Private ALWAYS_CLOSE_THIS_FORM As Boolean = True

	' The standard ChangeID for this form. e.g. tblPerson
	Private THIS_FORM_ChangeID As RenaissanceGlobals.RenaissanceChangeID

	' Form specific Permissioning variables
	Private THIS_FORM_PermissionArea As String
	Private THIS_FORM_PermissionType As RenaissanceGlobals.PermissionFeatureType

	' Form specific Form type 
	Private THIS_FORM_FormID As GenoaFormID

	' Form Status Flags

	Private FormIsValid As Boolean
	Private FormChanged As Boolean
	Private _FormOpenFailed As Boolean

	' User Permission Flags

	Private HasReadPermission As Boolean
	Private HasUpdatePermission As Boolean
	Private HasInsertPermission As Boolean
	Private HasDeletePermission As Boolean

	' Data Structures
	Private PertracInstruments As DataView = Nothing ' Manages Select List for Pertrac Instruments.

#End Region

#Region " Form Properties"

	Public ReadOnly Property MainForm() As GenoaMain Implements StandardGenoaForm.MainForm
		' Public property to return handle to the 'Main' Venice form, where in reside most of the 
		' data structures and many common utilities.
		Get
			Return _MainForm
		End Get
	End Property

	Public Property IsOverCancelButton() As Boolean Implements StandardGenoaForm.IsOverCancelButton
		' Public property maintaining a value indicating if the cursor is over the 'Cancel'
		' Button on this form.
		' This property is specifically designed for use by the field formating Event functions
		' In order that they do not impose format restrictions if the user is about to click the 
		' 'Cancel' button.
		'
		Get
			Return False
		End Get
		Set(ByVal Value As Boolean)
		End Set
	End Property

	Public ReadOnly Property IsInPaint() As Boolean Implements StandardGenoaForm.IsInPaint
		Get
			Return False
		End Get
	End Property

	Public ReadOnly Property InUse() As Boolean Implements StandardGenoaForm.InUse
		Get
			Return True
		End Get
	End Property

	Public ReadOnly Property FormOpenFailed() As Boolean Implements StandardGenoaForm.FormOpenFailed
		Get
			Return _FormOpenFailed
		End Get
	End Property

#End Region

	Public Sub New(ByVal pMainForm As GenoaMain)
		' *************************************************************
		' Custom 'New'. 
		' Passes in the reference to the parent form.
		' 
		' Establishes form specific variables.
		' Establishes Form specific Data connection / data structures.
		'
		' *************************************************************

		Me.New()

		_MainForm = pMainForm
		AddHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

		_FormOpenFailed = False

		' ******************************************************
		' Form Specific Settings :
		' ******************************************************

		' Form Permissioning :-

		THIS_FORM_PermissionArea = Me.Name
		THIS_FORM_PermissionType = RenaissanceGlobals.PermissionFeatureType.TypeForm

		' 'This' form ID

		THIS_FORM_FormID = GenoaFormID.frmGroupReport

		' Format Event Handlers for form controls

		AddHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

		AddHandler Combo_SelectFrom.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_SelectFrom.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_SelectFrom.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_CustomField.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_CustomField.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_CustomField.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_CustomField.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		AddHandler Combo_GroupByCustomField.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
		AddHandler Combo_GroupByCustomField.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
		AddHandler Combo_GroupByCustomField.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
		AddHandler Combo_GroupByCustomField.KeyUp, AddressOf MainForm.ComboSelectAsYouType

		' Set up the ToolTip
		MainForm.SetFormToolTip(Me, FormTooltip)

		' ******************************************************
		' End Form Specific.
		' ******************************************************

		THIS_FORM_ChangeID = RenaissanceChangeID.KnowledgeDate

	End Sub

#Region " This Form Event handlers : FormLoad / FormClose "

	' Form Initialisation code.
	'
	Public Sub ResetForm() Implements StandardGenoaForm.ResetForm

		Call Form_Load(Me, New System.EventArgs)
	End Sub

	Public Sub CloseForm() Implements StandardGenoaForm.CloseForm
		ALWAYS_CLOSE_THIS_FORM = True
		Me.Close()
	End Sub

	Private Sub Form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

		Me.MainForm.LogError(Me.Name, LOG_LEVELS.Audit, "", "Form Loaded", "", False)
		_FormOpenFailed = False

		If Not (MainForm Is Nothing) Then
			FormIsValid = True
		Else
			MessageBox.Show("'Main' form is not set. Closing form", "no Main Form", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If


		' Check User permissions
		Call CheckPermissions()
		If (HasReadPermission = False) Then
			Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User does not have Read permission for this Form..", "", True)

			FormIsValid = False
			_FormOpenFailed = True
			Exit Sub
		End If

		' Build Combos

		Try
			Set_ComboGroups()
			Set_ComboCustomFields()
			Check_CustomField.Checked = False

			If (Combo_CustomField.Items.Count > 0) Then
				Combo_CustomField.SelectedIndex = 0
			End If

			Me.Radio_ReportOnGroup.Checked = True
			If (Combo_SelectGroup.Items.Count > 0) Then
				Combo_SelectGroup.SelectedIndex = 0
			End If

			MainForm.SetComboSelectionLengths(Me)

			Me.Radio_ExistingGroups.Checked = True

		Catch ex As Exception
		End Try

	End Sub

	Private Sub Form_Closing(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles MyBase.Closing
		Dim HideForm As Boolean

		' Hide or Close this form ?
		' All depends on how many of this form type are Open or in Cache...


		If (ALWAYS_CLOSE_THIS_FORM = True) Or (FormIsValid = False) Then
			HideForm = False
		Else
			HideForm = True
			If MainForm.GenoaForms.CountOf(THIS_FORM_FormID) > MainForm.ReportForm_CacheCount Then
				HideForm = False
			End If
		End If

		If HideForm = True Then
			MainForm.HideInFormsCollection(Me)
			Me.Hide()	' NPP Fix

			e.Cancel = True
		Else
			Try
				MainForm.RemoveFromFormsCollection(Me)
				RemoveHandler _MainForm.GenoaAutoUpdate, AddressOf Me.AutoUpdate

				RemoveHandler MyBase.Resize, AddressOf MainForm.GenericFormResizeHandler

				RemoveHandler Combo_SelectFrom.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_SelectFrom.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_SelectFrom.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_CustomField.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_CustomField.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_CustomField.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_CustomField.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				RemoveHandler Combo_GroupByCustomField.GotFocus, AddressOf MainForm.GenericCombo_GotFocus
				RemoveHandler Combo_GroupByCustomField.SelectedIndexChanged, AddressOf MainForm.GenericCombo_SelectedIndexChanged
				RemoveHandler Combo_GroupByCustomField.LostFocus, AddressOf MainForm.GenericCombo_LostFocus
				RemoveHandler Combo_GroupByCustomField.KeyUp, AddressOf MainForm.ComboSelectAsYouType

			Catch ex As Exception
			End Try
		End If

	End Sub


#End Region

	' Routine to handle changes / updates to tables by this and other windows.
	' If this, or any other, form posts a change to a table, then it will invoke an update event 
	' detailing what tables have been altered.
	' Each Form will receive this event through this 'AutoUpdate' routine whice is configured to handle
	' the 'GenoaAutoUpdate' event of the main Venice form.
	' Each form may them react as appropriate to changes in any table that might impact it.
	'
	Private Sub AutoUpdate(ByVal sender As System.Object, ByVal e As RenaissanceGlobals.RenaissanceUpdateEventArgs)
		Dim KnowledgeDateChanged As Boolean
		Dim SetButtonStatus_Flag As Boolean

		If (Me.IsDisposed) OrElse (Me.Disposing) OrElse (Me.InUse = False) Then Exit Sub

		KnowledgeDateChanged = False
		SetButtonStatus_Flag = False

		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True) Or (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.Connection) = True) Then
			KnowledgeDateChanged = True
		End If

		' ****************************************************************
		' Check for changes relevant to this form
		' ****************************************************************

		' Changes to the KnowledgeDate :-
		If e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.KnowledgeDate) = True Then
			SetButtonStatus_Flag = True
		End If

		' Changes to the tblGroupList table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupList) = True) Or KnowledgeDateChanged Then

			' Re-Set combo.
			Call Set_ComboGroups()

			If Radio_ExistingGroups.Checked Then
				Call Radio_ExistingGroups_CheckedChanged(Radio_ExistingGroups, New EventArgs)
			End If
		End If


		' Changes to the tblGroupList table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblGroupItems) = True) Or KnowledgeDateChanged Then

			If Radio_ExistingGroups.Checked Then
				Call Radio_ExistingGroups_CheckedChanged(Radio_ExistingGroups, New EventArgs)
			End If
		End If

		' Changes to the tblGroupList table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblPertracCustomFields) = True) Or KnowledgeDateChanged Then
			Set_ComboCustomFields()
		End If

		' Changes to the tblUserPermissions table :-
		If (e.TableChanged(RenaissanceGlobals.RenaissanceChangeID.tblUserPermissions) = True) Or KnowledgeDateChanged Then

			' Check ongoing permissions.

			Call CheckPermissions()
			If (HasReadPermission = False) Then
				Me.MainForm.LogError(Me.Name, LOG_LEVELS.Warning, "", "User has lost Read permission for this Form.", "", True)

				FormIsValid = False
				Me.Close()
				Exit Sub
			End If

			SetButtonStatus_Flag = True

		End If

		' ****************************************************************
		'
		' ****************************************************************


	End Sub

#Region " Workhorse functions : SetSortedRows / CheckPermission / ControlsChanged / Menu Events "

	' Check User permissions
	Private Sub CheckPermissions()
		' *****************************************************************************
		'
		' *****************************************************************************

		Dim Permissions As Integer

		Permissions = MainForm.CheckPermissions(THIS_FORM_PermissionArea, THIS_FORM_PermissionType)

		HasReadPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermRead) > 0)
		HasUpdatePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermUpdate) > 0)
		HasInsertPermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermInsert) > 0)
		HasDeletePermission = ((Permissions And RenaissanceGlobals.PermissionBitmap.PermDelete) > 0)

	End Sub


#End Region

#Region " Set Form Combos (Form Specific Code) "

	Private Sub Radio_ExistingGroups_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ExistingGroups.CheckedChanged
		' ***********************************************************************************
		' React to the ExistingGroups radio.
		'
		' The Select Combo becomes a Combo to select an existing Group,
		' The SelectList contains all Instruments in the selected Group(s).
		' ***********************************************************************************

		If (Me.Created) AndAlso (Radio_ExistingGroups.Checked) Then

			Try
				Me.Cursor = Cursors.WaitCursor

				' Re-Configure Select Combo events.

				Try
					RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				Catch ex As Exception
				End Try
				Try
					RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
				Catch ex As Exception
				End Try

				AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				' Populate Select Combo.

				Call MainForm.SetTblGenericCombo( _
				Me.Combo_SelectFrom, _
				RenaissanceStandardDatasets.tblGroupList, _
				"GroupListName", _
				"GroupListID", _
				"", False, True, True, 0, "All Groups")		' 

				' Trigger  SelectList Rebuild.

				If Combo_SelectFrom.Items.Count > 0 Then
					Combo_SelectFrom.SelectedIndex = 0
				End If
				Call Combo_SelectFrom_SelectedIndexChanged(Combo_SelectFrom, New EventArgs)

			Catch ex As Exception
			Finally
				Me.Cursor = Cursors.Default
			End Try

		End If
	End Sub

	Private Sub Radio_Pertrac_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_Pertrac.CheckedChanged
		' ***********************************************************************************
		' React to the Pertrac Radio button being selected.
		'
		' The Select combo becomes a Select-As-You-Type edit box for the Select List control.
		' The SelectList control contains All or Selected Pertrac Instruments.
		' ***********************************************************************************

		If (Me.Created) AndAlso (Radio_Pertrac.Checked) Then

			' Re-Configure Select Combo events.

			Try
				RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType
			Catch ex As Exception
			End Try
			Try
				RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
			Catch ex As Exception
			End Try

			Try
				Me.Cursor = Cursors.WaitCursor

				AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch

				Combo_SelectFrom.DataSource = Nothing
				Combo_SelectFrom.Items.Clear()
				Combo_SelectFrom.Text = ""

				' Trigger Select List re-Build.

				Call Combo_SelectFrom_TextChanged(Combo_SelectFrom, New EventArgs)
			Catch ex As Exception
			Finally
				Me.Cursor = Cursors.Default
			End Try
		End If

	End Sub

	Private Sub Radio_ExistingFunds_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ExistingFunds.CheckedChanged
		' ***********************************************************************************
		' React to the ExistingFunds radio.
		'
		' The Select Combo becomes a Combo to select an existing Venice Fund,
		' The SelectList contains all Instruments, with associated Pertrac Instruments, in the selected Fund(s).
		' ***********************************************************************************

		If (Me.Created) AndAlso (Radio_ExistingFunds.Checked) Then

			Try
				Me.Cursor = Cursors.WaitCursor

				' Re-Configure Select Combo events.

				Try
					RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType
				Catch ex As Exception
				End Try
				Try
					RemoveHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType_NoMatch
				Catch ex As Exception
				End Try

				AddHandler Combo_SelectFrom.KeyUp, AddressOf MainForm.ComboSelectAsYouType

				Call MainForm.PertracData.GetActivePertracInstruments(True, 0) '  GetActivePertracInstruments(MainForm, True, 0)

				' Populate Select Combo.

				Call MainForm.SetTblGenericCombo( _
				Me.Combo_SelectFrom, _
				RenaissanceStandardDatasets.tblFund, _
				"FundCode", _
				"FundID", _
				"", False, True, True, 0, "All Funds")	 ' 

				' Trigger  SelectList Rebuild.

				If Combo_SelectFrom.Items.Count > 0 Then
					Combo_SelectFrom.SelectedIndex = 0
				Else
					Call Combo_SelectFrom_SelectedIndexChanged(Nothing, New EventArgs)
				End If

			Catch ex As Exception
			Finally
				Me.Cursor = Cursors.Default
			End Try
		End If
	End Sub

	Private Sub Set_ComboGroups()
		' *******************************************************************************
		'
		' *******************************************************************************

		Dim thisDSGroupList As RenaissanceDataClass.DSGroupList

		Try
			thisDSGroupList = MainForm.Load_Table(RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupList)

			List_SelectedGroups.DataSource = New DataView(thisDSGroupList.tblGroupList)

		Catch ex As Exception
		End Try

		MainForm.SetTblGenericCombo( _
		 Me.Combo_SelectGroup, _
		 RenaissanceGlobals.RenaissanceStandardDatasets.tblGroupList, _
		 "GroupGroup", _
		 "GroupGroup", _
		 "", _
		 True, True, True, 0, "")

	End Sub

	Private Sub Set_ComboCustomFields()
		' ***********************************************************************************
		'
		' ***********************************************************************************

		MainForm.SetTblGenericCombo( _
		Me.Combo_CustomField, _
		RenaissanceGlobals.RenaissanceStandardDatasets.tblPertracCustomFields, _
		"FieldName", _
		"FieldID", _
		"", _
		True, True, True, 0, "")

		MainForm.SetTblGenericCombo( _
		Me.Combo_GroupByCustomField, _
		RenaissanceGlobals.RenaissanceStandardDatasets.tblPertracCustomFields, _
		"FieldName", _
		"FieldID", _
		"", _
		True, True, True, 0, "")


	End Sub

	Private Sub Combo_SelectFrom_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectFrom.SelectedIndexChanged
		' ***********************************************************************************
		'
		' ***********************************************************************************

		If (Me.Created) Then

			If (Combo_SelectFrom.SelectedIndex >= 0) Then

				If Me.Radio_Pertrac.Checked Then
					' 

					List_SelectItems.SuspendLayout()

					' Ensure the DataView is populated.
					If (PertracInstruments Is Nothing) Then
						PertracInstruments = MainForm.PertracData.GetPertracInstruments()
					End If

					Try
						If (List_SelectItems.DisplayMember <> "ListDescription") Then
							List_SelectItems.DataSource = Nothing
							List_SelectItems.DisplayMember = "ListDescription"
						End If

						If (List_SelectItems.ValueMember <> "PertracCode") Then
							List_SelectItems.DataSource = Nothing
							List_SelectItems.ValueMember = "PertracCode"
						End If
					Catch ex As Exception
					End Try

					PertracInstruments.RowFilter = "Mastername LIKE '" & Combo_SelectFrom.SelectedText & "%'"

					If (List_SelectItems.DataSource IsNot PertracInstruments) Then
						List_SelectItems.DataSource = PertracInstruments
					End If

					List_SelectItems.ResumeLayout()

				ElseIf (Me.Radio_ExistingGroups.Checked) AndAlso (IsNumeric(Combo_SelectFrom.SelectedValue)) Then
					' Populate Form with given data.

					Dim tmpCommand As New SqlCommand
					Dim ListTable As RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable

					Try

						tmpCommand.CommandType = CommandType.StoredProcedure
						tmpCommand.CommandText = "adp_tblGroupItems_SelectGroupPertracCodes"
						tmpCommand.Connection = MainForm.GetGenoaConnection
						tmpCommand.Parameters.Add("@GroupID", SqlDbType.Int).Value = CInt(Combo_SelectFrom.SelectedValue)
						tmpCommand.Parameters.Add("@KnowledgeDate", SqlDbType.DateTime).Value = MainForm.Main_Knowledgedate
						tmpCommand.CommandTimeout = DEFAULT_SQLCOMMAND_TIMEOUT

						List_SelectItems.SuspendLayout()

						If (List_SelectItems.DataSource Is Nothing) OrElse (Not (TypeOf List_SelectItems.DataSource Is RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)) Then
							ListTable = New RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable
							ListTable.Columns.Add("ListDescription", GetType(String))
							ListTable.Columns.Add("PertracName", GetType(String))
							ListTable.Columns.Add("PertracProvider", GetType(String))
							ListTable.Columns.Add("IndexName", GetType(String))
							ListTable.Columns.Add("IndexProvider", GetType(String))

							List_SelectItems.DataSource = ListTable
						Else
							ListTable = CType(List_SelectItems.DataSource, RenaissanceDataClass.DSGroupItems.tblGroupItemsDataTable)
						End If

						ListTable.Rows.Clear()
						ListTable.Load(tmpCommand.ExecuteReader)

						Try
							List_SelectItems.DisplayMember = "ListDescription"
							List_SelectItems.ValueMember = "GroupPertracCode"
						Catch ex As Exception
						End Try

						List_SelectItems.DataSource = ListTable
						List_SelectItems.ResumeLayout()

					Catch ex As Exception

						Call MainForm.LogError(Me.Name, 0, ex.Message, "Error building Select List", ex.StackTrace, True)

					Finally
						Try
							If (tmpCommand IsNot Nothing) AndAlso (tmpCommand.Connection IsNot Nothing) Then
								tmpCommand.Connection.Close()
								tmpCommand.Connection = Nothing
							End If
						Catch ex As Exception
						End Try
					End Try

				ElseIf Me.Radio_ExistingFunds.Checked Then

					List_SelectItems.SuspendLayout()

					Try
						If (List_SelectItems.DisplayMember <> "ListDescription") Then
							List_SelectItems.DataSource = Nothing
							List_SelectItems.DisplayMember = "ListDescription"
						End If

						If (List_SelectItems.ValueMember <> "PertracCode") Then
							List_SelectItems.DataSource = Nothing
							List_SelectItems.ValueMember = "PertracCode"
						End If
					Catch ex As Exception
					End Try

					' List_SelectItems.DataSource = GetActivePertracInstruments(MainForm, False, CInt(Combo_SelectFrom.SelectedValue))
					List_SelectItems.DataSource = MainForm.PertracData.GetActivePertracInstruments(False, CInt(Combo_SelectFrom.SelectedValue))

					Try
						If (List_SelectItems.DisplayMember <> "ListDescription") Then
							List_SelectItems.DisplayMember = "ListDescription"
						End If

						If (List_SelectItems.ValueMember <> "PertracCode") Then
							List_SelectItems.ValueMember = "PertracCode"
						End If
					Catch ex As Exception
					End Try

					List_SelectItems.ResumeLayout()

				End If

			End If
		End If
	End Sub



	Private Sub Combo_SelectFrom_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles Combo_SelectFrom.TextChanged
		' ***********************************************************************************
		' This Event handles the Select-As-You-Type functionality associated with the Pertrac Radio choice.
		'
		' The List is managed by applying a select critera to the PertracInstruments DataView.
		' ***********************************************************************************

		If (Me.Created) Then
			If (Combo_SelectFrom.SelectedIndex < 0) Then
				If Me.Radio_Pertrac.Checked Then

					List_SelectItems.SuspendLayout()

					' Ensure the DataView is populated.
					If (PertracInstruments Is Nothing) Then
						PertracInstruments = MainForm.PertracData.GetPertracInstruments()
					End If

					' Set Display and View Members

					Try
						If (List_SelectItems.DisplayMember <> "ListDescription") Then
							List_SelectItems.DataSource = Nothing
							List_SelectItems.DisplayMember = "ListDescription"
						End If

						If (List_SelectItems.ValueMember <> "PertracCode") Then
							List_SelectItems.DataSource = Nothing
							List_SelectItems.ValueMember = "PertracCode"
						End If
					Catch ex As Exception
					End Try

					' Set Selection criteria.

					If (PertracInstruments.RowFilter <> "Mastername LIKE '" & Combo_SelectFrom.Text & "%'") Then
						PertracInstruments.RowFilter = "Mastername LIKE '" & Combo_SelectFrom.Text & "%'"
					End If

					' Set DataSource

					If (List_SelectItems.DataSource IsNot PertracInstruments) Then
						List_SelectItems.DataSource = PertracInstruments
					End If

					' Set Display and View Members (Double check).

					Try
						If (List_SelectItems.DisplayMember <> "ListDescription") Then
							List_SelectItems.DisplayMember = "ListDescription"
						End If

						If (List_SelectItems.ValueMember <> "PertracCode") Then
							List_SelectItems.ValueMember = "PertracCode"
						End If
					Catch ex As Exception
					End Try

					List_SelectItems.ResumeLayout()

				End If
			End If
		End If
	End Sub

	Private Sub Button_SelectAll_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
		Dim IndexCounter As Integer

		Try
			List_SelectItems.SuspendLayout()

			For IndexCounter = (List_SelectItems.Items.Count - 1) To 0 Step -1
				If (List_SelectItems.SelectedIndices.Contains(IndexCounter) = False) Then
					List_SelectItems.SelectedIndices.Add(IndexCounter)
				End If
			Next
		Catch ex As Exception
		Finally
			List_SelectItems.ResumeLayout()
		End Try

	End Sub

	Private Sub Button_SelectNone_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

		Try
			List_SelectItems.ClearSelected()
		Catch ex As Exception
		End Try

	End Sub

#End Region

#Region " Buttons : Add / Delete / Cancel / Save and Close (Generic Code) "


	Private Sub btnRunReport_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRunReport.Click
		' *****************************************************************************
		' Run Report
		' *****************************************************************************

		Dim SelectedIDs(-1) As Integer
		Dim IndexCounter As Integer
		Dim FormControls As ArrayList = Nothing
		Dim CustomFieldID As Integer = 0
		Dim GroupByCustomFieldID As Integer = 0

		Try
			FormControls = MainForm.DisableFormControls(Me)

			If (Combo_CustomField.Items.Count > 0) AndAlso (Combo_CustomField.SelectedIndex >= 0) Then
				CustomFieldID = CInt(Combo_CustomField.SelectedValue)
			End If

			If (Combo_GroupByCustomField.SelectedIndex > 0) AndAlso (IsNumeric(Combo_GroupByCustomField.SelectedValue)) Then
				GroupByCustomFieldID = CInt(Combo_GroupByCustomField.SelectedValue)
			End If

			If Me.Radio_ReportOnGroup.Checked Then
				If (List_SelectedGroups.SelectedItems.Count <= 0) Then
					Exit Sub
				End If

				ReDim SelectedIDs(List_SelectedGroups.SelectedItems.Count - 1)

				For IndexCounter = 0 To (List_SelectedGroups.SelectedItems.Count - 1)
					SelectedIDs(IndexCounter) = List_SelectedGroups.SelectedItems(IndexCounter)(List_SelectedGroups.ValueMember)
				Next

        MainForm.MainReportHandler.GroupReportOnGroups(SelectedIDs, CustomFieldID, GroupByCustomFieldID, Check_OrderByName.Checked, StatusLabel1)

			ElseIf Me.Radio_ReportOnSelected.Checked Then
				Dim GroupName As String

				If (Me.List_SelectItems.SelectedItems.Count <= 0) Then
					Exit Sub
				End If

				ReDim SelectedIDs(List_SelectItems.SelectedItems.Count - 1)

				For IndexCounter = 0 To (List_SelectItems.SelectedItems.Count - 1)
					SelectedIDs(IndexCounter) = List_SelectItems.SelectedItems(IndexCounter)(List_SelectItems.ValueMember)
				Next

				GroupName = "Custom Selection"
				If (Me.Radio_ExistingGroups.Checked) AndAlso (Me.Combo_SelectFrom.SelectedIndex >= 0) Then
					If (List_SelectItems.SelectedItems.Count = List_SelectItems.Items.Count) Then
						GroupName = Combo_SelectFrom.SelectedItem(Me.Combo_SelectFrom.DisplayMember).ToString
					Else
						GroupName = "SubSet of " & Combo_SelectFrom.SelectedItem(Combo_SelectFrom.DisplayMember).ToString
					End If
				End If

				Me.StatusLabel1.Text = "Processing Report"
				Me.Refresh()
				Application.DoEvents()

        MainForm.MainReportHandler.GroupReportSelectedIDs(GroupName, SelectedIDs, CustomFieldID, GroupByCustomFieldID, Check_OrderByName.Checked, StatusLabel1)

			End If

		Catch ex As Exception

		Finally
			MainForm.EnableFormControls(FormControls)
		End Try

		Me.StatusLabel1.Text = ""
		Me.btnRunReport.Enabled = True

	End Sub


	Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
		' *****************************************************************************
		' Close Form
		' *****************************************************************************

		Me.Close()

	End Sub

#End Region

#Region " Form Control Event Code"

	Private Sub Radio_ReportOnGroup_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ReportOnGroup.CheckedChanged
		' *****************************************************************************
		' 
		'
		' *****************************************************************************

		If (Me.Created) AndAlso (Radio_ReportOnGroup.Checked) Then
			Panel_SelectFrom.Enabled = False
			Combo_SelectFrom.Enabled = False
			List_SelectItems.Enabled = False

			List_SelectedGroups.Enabled = True
			Combo_SelectGroup.Enabled = True

			Me.Radio_ReportOnSelected.Checked = False

		End If
	End Sub

	Private Sub Radio_ReportOnSelected_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Radio_ReportOnSelected.CheckedChanged
		' *****************************************************************************
		' 
		'
		' *****************************************************************************

		If (Me.Created) AndAlso (Radio_ReportOnSelected.Checked) Then
			Panel_SelectFrom.Enabled = True
			Combo_SelectFrom.Enabled = True
			List_SelectItems.Enabled = True

			List_SelectedGroups.Enabled = False
			Combo_SelectGroup.Enabled = False

			Radio_ReportOnGroup.Checked = False
		End If
	End Sub

	Private Sub Combo_SelectGroup_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Combo_SelectGroup.SelectedIndexChanged

		Dim ListDataView As DataView

		If (Combo_SelectGroup.SelectedIndex <= 0) Then
			ListDataView = Me.List_SelectedGroups.DataSource
			ListDataView.RowFilter = "True"
		Else
			ListDataView = Me.List_SelectedGroups.DataSource
			ListDataView.RowFilter = "GroupGroup='" & Combo_SelectGroup.SelectedItem(Combo_SelectGroup.ValueMember).ToString & "'"
		End If

	End Sub

	Private Sub Check_CustomField_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_CustomField.CheckedChanged
		' *****************************************************************************
		' 
		' *****************************************************************************

		If (Check_CustomField.Checked) Then
			Combo_CustomField.Enabled = True
		Else
			Combo_CustomField.Enabled = False
			If (Combo_CustomField.Items.Count > 0) Then
				Combo_CustomField.SelectedIndex = 0
			End If
		End If

	End Sub

	Private Sub Check_GroupByCustomField_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Check_GroupByCustomField.CheckedChanged
		' *****************************************************************************
		' 
		' *****************************************************************************

		If (Check_GroupByCustomField.Checked) Then
			Combo_GroupByCustomField.Enabled = True
		Else
			Combo_GroupByCustomField.Enabled = False
			If (Combo_GroupByCustomField.Items.Count > 0) Then
				Combo_GroupByCustomField.SelectedIndex = 0
			End If
		End If
	End Sub

#End Region




End Class
